<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationxx');
      require_once("php/fungsi.php");
   }
   
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu272')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
      {
         $data['page_title'] = $this->lang->line('transkn');
         $data['iperiode']	= '';
         $this->load->view('transkn/vmainform', $data);
      }
      else
      {
         $this->load->view('awal/index.php');
      }
   }

   function view()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu272')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
      {
         $iperiode= $this->input->post('iperiode');
         if($iperiode=='')
         {
            $iperiode=$this->uri->segment(4);
         }
         $data['page_title'] = $this->lang->line('transkn');
         $data['iperiode']	= $iperiode;
         $tahun=substr($iperiode,0,4);      

         define ('BOOLEAN_FIELD',   'L');
         define ('CHARACTER_FIELD', 'C');
         define ('DATE_FIELD',      'D');
         define ('NUMBER_FIELD',    'N');
         define ('READ_ONLY',  '0');
         define ('WRITE_ONLY', '1');
         define ('READ_WRITE', '2');
         $db_file = 'spb/00/kn'.$tahun.'.dbf';
         $dbase_definition = array (
               array ('KODEAREA',  CHARACTER_FIELD,  2),
               array ('KODELANG',  CHARACTER_FIELD,  5),
               array ('KODESALES',  CHARACTER_FIELD,  2),
               array ('NOKN',  CHARACTER_FIELD,  8),
               array ('TGLKN',  DATE_FIELD),
               array ('NOBBM',  CHARACTER_FIELD,  6),
               array ('TGLBBM',  DATE_FIELD),
               array ('NOTTB',  CHARACTER_FIELD,  7),
               array ('TGLTTB',  DATE_FIELD),
               array ('SERIPAJAK',  CHARACTER_FIELD,  6),
               array ('TGLPAJAK',  DATE_FIELD),
               array ('KOTOR',  NUMBER_FIELD,  10,0),
               array ('BERSIH',  NUMBER_FIELD,  10,0),
               array ('DISCOUNT', NUMBER_FIELD, 7, 2),
               array ('SISA', NUMBER_FIELD, 10, 0),
               array ('BATAL', BOOLEAN_FIELD),
               array ('TGLBUAT', CHARACTER_FIELD,  20),
               array ('TGLUBAH', CHARACTER_FIELD,  20)
            );
         $create = @ dbase_create($db_file, $dbase_definition)
                   or die ("Could not create dbf file <i>$db_file</i>.");
         $id = @ dbase_open ($db_file, READ_WRITE)
               or die ("Could not open dbf file <i>$db_file</i>."); 
         $per=substr($iperiode,2,2);
         $dicari="K%".$per;
         $sql= " select a.i_customer, 
                     a.i_area,
                     a.i_kn,
                     a.d_kn,
                     a.i_refference,
                     a.i_salesman,
                     a.i_pajak,
                     a.d_pajak,
                     a.v_gross,
                     a.v_discount,
                     a.v_netto,
                     a.v_sisa,
                     a.f_kn_cancel,
                     a.d_entry,
                     a.d_update,
                     b.i_refference_document as i_ttb,
                     b.d_refference_document as d_ttb,
                     b.d_bbm
                     from tm_kn a,
                           tm_bbm b
                     where a.i_kn like '$dicari' 
                           and to_char(a.d_kn,'yyyymm')='$iperiode'
                           and a.i_refference=b.i_bbm
                     order by i_kn";
         $query=$this->db->query($sql);
         $jumrec=$query->num_rows();
         if ($query->num_rows() > 0)
         {
            foreach($query->result() as $rowsj)
            {
               $kodelang         = $rowsj->i_customer;
               $kodearea         = $rowsj->i_area;
               $nokn             = $rowsj->i_kn;
               $nobbm            = substr($rowsj->i_refference,9,6);
               $nottb            = $rowsj->i_ttb;
               $kodesales        = $rowsj->i_salesman;
               $tgldok           = substr($rowsj->d_kn,0,4).substr($rowsj->d_kn,5,2).substr($rowsj->d_kn,8,2);
               $tglttb           = substr($rowsj->d_ttb,0,4).substr($rowsj->d_ttb,5,2).substr($rowsj->d_ttb,8,2);
               $seripajak        = $rowsj->i_pajak;
               $tglpajak         = substr($rowsj->d_pajak,0,4).substr($rowsj->d_pajak,5,2).substr($rowsj->d_pajak,8,2);
               $tglbbm           = substr($rowsj->d_bbm,0,4).substr($rowsj->d_bbm,5,2).substr($rowsj->d_bbm,8,2);
               $kotor            = $rowsj->v_gross;
               $bersih           = $rowsj->v_netto;
               $discount         = $rowsj->v_discount;
               $sisa             = $rowsj->v_sisa;

               if($rowsj->f_kn_cancel=='f')
               {
                  $batal='F'; 
               }
               else
               { 
                  $batal='T';
               }
               
               $tglbuat          = $rowsj->d_entry;
               $tglubah          = $rowsj->d_update;
               $isi = array ($kodearea,
                              $kodelang,
                              $kodesales,
                              $nokn,
                              $tgldok,
                              $nobbm,
                              $tglbbm,
                              $nottb,
                              $tglttb,
                              $seripajak,
                              $tglpajak,
                              $kotor,
                              $bersih,
                              $discount,
                              $sisa,
                              $batal,
                              $tglbuat,
                              $tglubah);
               dbase_add_record ($id, $isi) or die ("Gagal transfer ke file <i>$db_file</i>."); 
            }
         }
         dbase_close($id);
         $sess=$this->session->userdata('session_id');
         $id=$this->session->userdata('user_id');
         $sql= "select * from dgu_session where session_id='$sess' and not user_data isnull";
         $rs= pg_query($sql);
         if(pg_num_rows($rs)>0)
         {
            while($row=pg_fetch_assoc($rs))
            {
               $ip_address	  = $row['ip_address'];
               break;
            }
         }
         else
         {
            $ip_address='kosong';
         }
         $query 	= pg_query("SELECT current_timestamp as c");
         while($row=pg_fetch_assoc($query))
         {
            $now	  = $row['c'];
         }
         $pesan='Transfer Data KN Retur ke file ' .$db_file.' Periode:'.$iperiode.' sebanyak :'.$jumrec.' records';
         $this->load->model('logger');
         $this->logger->write($id, $ip_address, $now , $pesan );
         $data['sukses']= true;
         $data['inomor']= $pesan;
         $this->load->view('nomor',$data);
      }
      else
      {
         $this->load->view('awal/index.php');
      }
   }
   
}
?>
