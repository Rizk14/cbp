<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('pelunasan-ap');
			$data['jmlitem'] = 0;
			$data['ipl'] = '';
			$data['idtap'] = '';
			$data['iarea']= '';
			$data['isupplier']= '';
			$data['isi']='';
			$data['detail']='';
	 		$this->load->view('pelunasan-ap/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listspb');
			$this->load->view('pelunasan-ap/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/pelunasan-ap/cform/area/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tr_area ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('pelunasan-ap/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_area
									   	where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('pelunasan-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('pelunasan-ap/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			( ($this->session->userdata('logged_in')) && ($this->session->userdata('menu135')=='t') ) 
			||
			( ($this->session->userdata('logged_in')) && ($this->session->userdata('allmenu')=='t') )
		   ){
			$ispb	= $this->input->post('ispbdelete', TRUE);
			$this->load->model('pelunasan-ap/mmaster');
			$this->mmaster->delete($ispb);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
		  $data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus Pelunasan AP No:'.$ispb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('listspb');
			$data['ispb']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('pelunasan-ap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/pelunasan-ap/cform/index/';
			$config['per_page'] = '10';
			$limo=$config['per_page'];
			$ofso=$this->uri->segment(4);
			if($ofso=='')
				$ofso=0;
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer
										and a.f_lunas = 'f'
										and (upper(a.i_customer) like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%'
										  or upper(a.i_spb) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('pelunasan-ap/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('pelunasan');
			$data['ispb']='';
			$data['ittb']='';
			$data['inota']='';
	 		$this->load->view('pelunasan-ap/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('pelunasan-ap/mmaster');
			$ipl 	= $this->input->post('ipelunasanap', TRUE);
			$dbukti	= $this->input->post('dbukti', TRUE);
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$thh=$tmp[2];
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
			}
			$djt 	= $this->input->post('djt', TRUE);
			if($djt!=''){
				$tmp=explode("-",$djt);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$djt=$th."-".$bl."-".$hr;
			}
			$dcair 	= $this->input->post('dcair', TRUE);
			if($dcair!=''){
				$tmp=explode("-",$dcair);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dcair=$th."-".$bl."-".$hr;
			}
			$dgiro 	= $this->input->post('dgiro', TRUE);
			if($dgiro!=''){
				$tmp=explode("-",$dgiro);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgiro=$th."-".$bl."-".$hr;
				$thku=$th;
			}
			$isupplier		= $this->input->post('isupplier', TRUE);
			$esuppliername	= $this->input->post('esuppliername', TRUE);
      $esuppliername = str_replace("'","\'",$esuppliername);
			$esupplieraddress= $this->input->post('esupplieraddress', TRUE);
      $esupplieraddress = str_replace("'","\'",$esupplieraddress);
			$esuppliercity	= $this->input->post('esuppliercity', TRUE);
			$iarea			= '00';#$this->input->post('iarea', TRUE);
#			$eareaname		= $this->input->post('eareaname', TRUE);
			$ijenisbayar	= $this->input->post('ijenisbayar',TRUE);
			$ejenisbayarname= $this->input->post('ejenisbayarname',TRUE);
			$egirobank		= $this->input->post('egirobank',TRUE);
			if(($egirobank==Null) && ($ijenisbayar!='05')){
				$egirobank	= '-';
				$igiro		= $this->input->post('igiro',TRUE);
			}else if(($egirobank==Null) && ($ijenisbayar=='05')){
				$egirobank	= $this->input->post('igiro',TRUE);
				$igiro		='';
			}else if(($egirobank!=Null) && ($ijenisbayar=='01')){
				$igiro		= $this->input->post('igiro',TRUE);
				$ipv		= $this->input->post('ipv',TRUE);
			}else if(($egirobank!=Null) && ($ijenisbayar=='03')){
				$igiro		= $this->input->post('igiro',TRUE);
			}else{
				$igiro		='';
			}
			$vjumlah		= $this->input->post('vjumlah',TRUE);
			$vjumlah		= str_replace(',','',$vjumlah);
			$vlebih			= $this->input->post('vlebih',TRUE);
			$vlebih			= str_replace(',','',$vlebih);
			$jml			= $this->input->post('jml', TRUE);
			if(($dbukti!='') && ($ipl!='') && ($vjumlah!='') && ($vjumlah!='0') && ($jml!='0')){
#				$ipl=$this->mmaster->runningnumberpl($thh,$iarea);
				switch($ijenisbayar){
				case '01':
					$ipl=$ipl.'B';
					break;
				case '02':
					$ipl=$ipl.'A';
					break;
				case '03':
					$ipl=$ipl.'C';
					break;
				case '04':
					$ipl=$ipl.'D';
					break;
				case '05':
					$ipl=$ipl.'E';
					break;
				}
				$this->db->trans_begin();
				$this->mmaster->insertheader(	$ipl,$iarea,$ijenisbayar,$igiro,$isupplier,$dgiro,$dbukti,$dcair,$egirobank,
              												$vjumlah,$vlebih);
				$asal=0;
				$pengurang=$vjumlah-$vlebih;
				if($ijenisbayar=='01'){
				  $this->mmaster->updategiro($igiro,$ipv,$pengurang,$asal);
				}elseif($ijenisbayar=='04'){
				  $this->mmaster->updatedn($iarea,$igiro,$pengurang,$asal);
				}elseif($ijenisbayar=='03'){
				  $this->mmaster->updateku($iarea,$igiro,$pengurang,$asal,$thku);
				}
				for($i=1;$i<=$jml;$i++){
				  $inota					= $this->input->post('inota'.$i, TRUE);
				  $dnota					= $this->input->post('dnota'.$i, TRUE);
				  if($dnota!=''){
					$tmp=explode("-",$dnota);
					$th=$tmp[2];
					$bl=$tmp[1];
					$hr=$tmp[0];
					$dnota=$th."-".$bl."-".$hr;
				  }
/*
b=giro/cek -- 01
a=tunai -- 02
c=transfer/ku -- 03
d=kn -- 04
e=lebih -- 05 
*/
				  $vjumlah	= $this->input->post('vjumlah'.$i, TRUE);
				  $vsisa	= $this->input->post('vsisa'.$i, TRUE);
				  $vjumlah	= str_replace(',','',$vjumlah);
  				  $vsisa	= str_replace(',','',$vsisa);
				  $vsisa	= $vsisa-$vjumlah;
				  $this->mmaster->insertdetail(	$ipl,$iarea,$inota,$dnota,$vjumlah,$vsisa,$dbukti);
				  $this->mmaster->updatedtap($inota,$iarea,$isupplier,$vsisa);	
				}
				$nilai=$this->mmaster->hitungsisapelunasan($ipl,$iarea,$dbukti);
				if($nilai==0){
					$this->mmaster->updatestatuspelunasan($ipl,$iarea,$dbukti);
				}
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		=  $this->db->query($sql);
			    if($rs->num_rows>0){
				    foreach($rs->result() as $tes){
					    $ip_address	  = $tes->ip_address;
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }

			    $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
			    $data['host']	= $ip_address;
			    $data['uri']	= $this->session->userdata('printeruri');
				  $query 	= pg_query("SELECT current_timestamp as c");
				  while($row=pg_fetch_assoc($query)){
					  $now	  = $row['c'];
				  }
				  $pesan='Update Pelunasan AP Area '.$iarea.' No:'.$ipl;
				  $this->load->model('logger');
				  $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses'] = true;
					$data['inomor']	= $ipl;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('pelunasan-ap')." update";
			if(
				($this->uri->segment(4)) && ($this->uri->segment(5))			
			  )
			{
				$ipl 			= $this->uri->segment(4);
				$iarea		= $this->uri->segment(5);
				$isupplier= $this->uri->segment(6);
				$dfrom		= $this->uri->segment(7);
				$dto 			= $this->uri->segment(8);
				$query 		= $this->db->query("select * from tm_pelunasanap_item 
													where i_pelunasanap = '$ipl' and i_area = '$iarea' ");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ipl'] 			= $ipl;
				$data['iarea']		= $iarea;
				$data['isupplier']= $isupplier;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				$this->load->model('pelunasan-ap/mmaster');
				$data['vsisa']=$this->mmaster->sisa($iarea,$ipl);
				$data['isi']=$this->mmaster->bacapl($iarea,$ipl);
				$data['detail']=$this->mmaster->bacadetailpl($iarea,$ipl);
		 		$this->load->view('pelunasan-ap/vmainform',$data);
			}else{
				$this->load->view('pelunasan-ap/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function updatepelunasan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('pelunasan-ap/mmaster');
			$ipl 	= $this->input->post('ipelunasanap', TRUE);
			$dbukti	= $this->input->post('dbukti', TRUE);
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$thh=$tmp[2];
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
			}
			$djt 	= $this->input->post('djt', TRUE);
			if($djt!=''){
				$tmp=explode("-",$djt);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$djt=$th."-".$bl."-".$hr;
			}
			$dcair 	= $this->input->post('dcair', TRUE);
			if($dcair!=''){
				$tmp=explode("-",$dcair);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dcair=$th."-".$bl."-".$hr;
			}
			$dgiro 	= $this->input->post('dgiro', TRUE);
			if($dgiro!=''){
				$tmp=explode("-",$dgiro);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dgiro=$th."-".$bl."-".$hr;
				$thku=$th;
			}
			$isupplier		= $this->input->post('isupplier', TRUE);
			$esuppliername	= $this->input->post('esuppliername', TRUE);
      $esuppliername = str_replace("'","\'",$esuppliername);
			$esupplieraddress= $this->input->post('esupplieraddress', TRUE);
      $esupplieraddress = str_replace("'","\'",$esupplieraddress);
			$esuppliercity	= $this->input->post('esuppliercity', TRUE);
			$iarea			= '00';#$this->input->post('iarea', TRUE);
#			$eareaname		= $this->input->post('eareaname', TRUE);
			$ijenisbayar	= $this->input->post('ijenisbayar',TRUE);
			$ejenisbayarname= $this->input->post('ejenisbayarname',TRUE);
			$egirobank		= $this->input->post('egirobank',TRUE);
			if(($egirobank==Null) && ($ijenisbayar!='05')){
				$egirobank	= '-';
				$igiro		= $this->input->post('igiro',TRUE);
			}else if(($egirobank==Null) && ($ijenisbayar=='05')){
				$egirobank	= $this->input->post('igiro',TRUE);
				$igiro		='';
			}else if(($egirobank!=Null) && ($ijenisbayar=='01')){
				$igiro		= $this->input->post('igiro',TRUE);
				$ipv		= $this->input->post('ipv',TRUE);
			}else if(($egirobank!=Null) && ($ijenisbayar=='03')){
				$igiro		= $this->input->post('igiro',TRUE);
			}else{
				$igiro		='';
			}
			$vjumlah		= $this->input->post('vjumlah',TRUE);
			$vjumlah		= str_replace(',','',$vjumlah);
			$vlebih			= $this->input->post('vlebih',TRUE);
			$vlebih			= str_replace(',','',$vlebih);
			$jml			= $this->input->post('jml', TRUE);


			if(($dbukti!='') && ($ipl!='')){
				$this->db->trans_begin();
				$asalkn	= $this->mmaster->jmlasalkn($ipl,$dbukti,$iarea);
				foreach($asalkn as $asl){
					$jmlpl	= $asl->v_jumlah;
					$lbhpl	= $asl->v_lebih;
				}
				$asal	= $jmlpl-$lbhpl;
				$this->mmaster->deleteheader(	$ipl,$dbukti,$iarea);
				$this->mmaster->insertheader(	$ipl,$iarea,$ijenisbayar,$igiro,$isupplier,$dgiro,$dbukti,$dcair,$egirobank,
												$vjumlah,$vlebih);
				$asal=0;
				$pengurang=$vjumlah-$vlebih;
				if($ijenisbayar=='01'){
				  $this->mmaster->updategiro($igiro,$ipv,$pengurang,$asal);
				}elseif($ijenisbayar=='04'){
				  $this->mmaster->updatedn($iarea,$igiro,$pengurang,$asal);
				}elseif($ijenisbayar=='03'){
				  $this->mmaster->updateku($iarea,$igiro,$pengurang,$asal,$thku);
				}
				for($i=1;$i<=$jml;$i++){
				  $inota					= $this->input->post('inota'.$i, TRUE);
				  $dnota					= $this->input->post('dnota'.$i, TRUE);
				  if($dnota!=''){
					$tmp=explode("-",$dnota);
					$th=$tmp[2];
					$bl=$tmp[1];
					$hr=$tmp[0];
					$dnota=$th."-".$bl."-".$hr;
				  }
/*
a=giro/cek -- 01
b=tunai -- 02
c=transfer/ku -- 03
d=kn -- 04
e=lebih -- 05 
*/
				  $vjumlah	= $this->input->post('vjumlah'.$i, TRUE);
				  $vsisa	= $this->input->post('vsisa'.$i, TRUE);
				  $vjumlah	= str_replace(',','',$vjumlah);
  				  $vsisa	= str_replace(',','',$vsisa);
				  $vsisa	= $vsisa-$vjumlah;
				  $this->mmaster->deletedetail(	$ipl,$iarea,$inota,$dbukti);
				  $this->mmaster->insertdetail(	$ipl,$iarea,$inota,$dnota,$vjumlah,$vsisa,$dbukti);
				  $this->mmaster->updatedtap($inota,$iarea,$isupplier,$vsisa);	
				}
				$nilai=$this->mmaster->hitungsisapelunasan($ipl,$iarea,$dbukti);
				if($nilai==0){
					$this->mmaster->updatestatuspelunasan($ipl,$iarea,$dbukti);
				}
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		=  $this->db->query($sql);
			    if($rs->num_rows>0){
				    foreach($rs->result() as $tes){
					    $ip_address	  = $tes->ip_address;
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }

			    $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
			    $data['host']	= $ip_address;
			    $data['uri']	= $this->session->userdata('printeruri');
				  $query 	= pg_query("SELECT current_timestamp as c");
				  while($row=pg_fetch_assoc($query)){
					  $now	  = $row['c'];
				  }
				  $pesan='Update Pelunasan AP Area '.$iarea.' No:'.$ipl;
				  $this->load->model('logger');
				  $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses'] = true;
					$data['inomor']	= $ipl;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 	= strtoupper($this->input->post("cari"));
			$config['base_url'] = base_url().'index.php/pelunasan-ap/cform/supplier/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select i_supplier from tr_supplier
										              where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

      $data['cari']=$cari;
			$this->load->model('pelunasan-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('pelunasan-ap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/spb/cform/customer/'.$iarea.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_customer
									   	where i_area='$iarea' 
										and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
			$this->load->view('spb/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function girocek()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/pelunasan-ap/cform/girocek/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_jenis_bayar",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_girocek');
			$data['isi']=$this->mmaster->bacagirocek($config['per_page'],$this->uri->segment(5));
			$this->load->view('pelunasan-ap/vlistgirocek', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function idtap()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari=strtoupper($this->input->post("cari"));
      $baris=strtoupper($this->input->post("baris"));
      $isupplier=strtoupper($this->input->post("isupplier"));
      $iarea=strtoupper($this->input->post("iarea"));
			$data['cari']	= $cari;
			if($baris=='') $baris = $this->uri->segment(4);
			if($isupplier=='') $isupplier	= $this->uri->segment(5);
			if($iarea=='') $iarea = $this->uri->segment(6);
			$data['baris']		= $baris;
			$data['isupplier']	= $isupplier;
			$data['iarea']		= $iarea;
			$config['base_url'] = base_url().'index.php/pelunasan-ap/cform/idtap/'.$baris.'/'.$isupplier.'/'.$iarea.'/';
			$query = $this->db->query(" select i_dtap from tm_dtap
										              where i_supplier='$isupplier' and v_sisa>0
                                  and (upper(i_dtap) like '%$cari%')" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('pelunasan-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_dtap');
			$data['isi']=$this->mmaster->bacadtap($cari,$iarea,$isupplier,$config['per_page'],$this->uri->segment(7));
			$this->load->view('pelunasan-ap/vlistdtap', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariidtap()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 				= $this->input->post('cari', FALSE);
			$cari				= strtoupper($cari);
			$data['baris']		= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$area				= $this->uri->segment(5);
			$data['area']		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/daftartagihan/cform/nota/'.$baris.'/'.$area.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name, c.e_salesman_name 
										from tm_nota a, tr_customer b, tr_salesman c 
										where a.i_customer=b.i_customer
										and a.i_salesman=c.i_salesman and a.i_nota like '%$cari%'
										and a.i_area='$area'" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->caridtap($cari,$area,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$this->load->view('daftartagihan/vlistnota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function giro()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isupplier 	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/pelunasan-ap/cform/giro/'.$isupplier.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tm_giro_dgu
										where i_supplier = '$isupplier' 
										and (f_giro_tolak='f' or f_giro_batal='f')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_girodgu');
			$data['isi']=$this->mmaster->bacagiro($isupplier,$config['per_page'],$this->uri->segment(6));
			$data['isupplier']=$isupplier;
			$this->load->view('pelunasan-ap/vlistgiro', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carigiro()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/spb/cform/customer/'.$iarea.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_customer
									   	where i_area='$iarea' 
										and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spb/mmaster');
			$data['page_title'] = $this->lang->line('list_giro');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
			$this->load->view('spb/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function lebihbayar()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/pelunasan-ap/cform/lebihbayar/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	SELECT 	i_dt, min(v_jumlah), min(v_lebih), i_area,
											d_bukti,i_dt||'-'||max(substr(i_pelunasan,9,2))
										from tm_pelunasan_lebih
										where i_customer = '$icustomer'
										and i_area='$iarea'
										and v_lebih>0
										group by i_dt, d_bukti, i_area",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_pelunasan');
			$data['isi']=$this->mmaster->bacapelunasan($icustomer,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('pelunasan-ap/vlistpelunasan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carilebihbayar()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/spb/cform/customer/'.$iarea.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_customer
									   	where i_area='$iarea' 
										and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spb/mmaster');
			$data['page_title'] = $this->lang->line('list_giro');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
			$this->load->view('spb/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function dn()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isupplier 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/pelunasan-ap/cform/dn/'.$isupplier.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tm_dn 
										where i_supplier = '$isupplier' 
										and i_area='$iarea'
										and v_sisa>0",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_dn');
			$data['isi']=$this->mmaster->bacadn($isupplier,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['isupplier']=$isupplier;
			$data['iarea']=$iarea;
			$this->load->view('pelunasan-ap/vlistdn', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caridn()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isupplier 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/pelunasan-ap/cform/dn/'.$isupplier.'/'.$iarea.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query = $this->db->query("	select * from tm_dn 
										where i_supplier = '$isupplier' 
										and i_area='$iarea' and i_dn like '$cari'
										and v_sisa>0",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_dn');
			$data['isi']=$this->mmaster->caridn($cari,$isupplier,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['isupplier']=$isupplier;
			$data['iarea']=$iarea;
			$this->load->view('pelunasan-ap/vlistdn', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function ku()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isupplier 	= $this->uri->segment(4);
			$th 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/pelunasan-ap/cform/ku/'.$isupplier.'/'.$th.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tm_kuk 
										              where i_supplier = '$isupplier' 
										              and v_sisa>0
										              and f_close='f' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_ku');
			$data['isi']=$this->mmaster->bacaku($isupplier,$th,$config['per_page'],$this->uri->segment(7));
			$data['isupplier']=$isupplier;
			$data['iarea']=$th;
			$this->load->view('pelunasan-ap/vlistku', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariku()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/spb/cform/customer/'.$iarea.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_customer
									   	where i_area='$iarea' 
										and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spb/mmaster');
			$data['page_title'] = $this->lang->line('list_giro');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
			$this->load->view('spb/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
