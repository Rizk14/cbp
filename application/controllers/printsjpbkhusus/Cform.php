<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu356')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/printsjpbkhusus/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('printsjpbkhusus');
      $data['dfrom']= '';
			$data['dto']  = '';
			$data['isi']  = '';
#			$data['area']	= $this->session->userdata('i_area');
			$this->load->view('printsjpbkhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
/*	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu306')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printsjpb/cform/customer/index/';
			$icustomer	= $this->input->post('i_customer');
			$query = $this->db->query(" select a.*, b.e_customer_name from tr_spg a, tr_customer b
                    where a.i_customer=b.i_customer order by a.i_customer",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('printsjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(5));
			$this->load->view('printsjpb/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu306')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printsjpb/cform/customer/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$query 	= $this->db->query("select a.*, b.e_customer_name from tr_spg a, tr_customer b
                          where a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                          or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
						              order by a.i_customer",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printsjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printsjpb/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}*/
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		   ($this->session->userdata('menu356')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			 ($this->session->userdata('allmenu')=='t'))
		   ){
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
      if($dfrom=='')$dfrom=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$cari = strtoupper($this->input->post('cari', FALSE));
      $area	= $this->session->userdata('i_area');
			$config['base_url'] = base_url().'index.php/printsjpbkhusus/cform/view/'.$dfrom.'/'.$dto.'/';
			if($area=='PB'){
			  $query = $this->db->query(" select a.i_sjpb
                                    from tm_sjpb a, tr_customer b
                                    where a.i_customer=b.i_customer
                                    and (upper(a.i_sjpb) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                                    or upper(b.e_customer_name) like '%$cari%')
                                    and a.d_sjpb >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjpb <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
			  $query = $this->db->query(" select a.i_sjpb
                                    from tm_sjpb a, tr_customer b
                                    where a.i_customer=b.i_customer
                                    and (upper(a.i_sjpb) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                                    or upper(b.e_customer_name) like '%$cari%') and a.i_area_entry='$area'
                                    and a.d_sjpb >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjpb <= to_date('$dto','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printsjpb');
			$this->load->model('printsjpbkhusus/mmaster');
      $data['dfrom']= $dfrom;
			$data['dto']  = $dto;
			$data['area'] = $area;
			$data['isi']  = $this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(6),$dfrom,$dto,$area);
			$this->load->view('printsjpbkhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
  function cetak(){
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu356')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isjpb  = $this->uri->segment(4);
			$this->load->model('printsjpbkhusus/mmaster');
			$data['isjpb']=$isjpb;
			$data['page_title'] = $this->lang->line('printsjpbkhusus');
			$data['isi']=$this->mmaster->baca($isjpb);
			$data['detail']=$this->mmaster->bacadetail($isjpb);
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak SJPB No:'.$isjpb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printsjpbkhusus/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu356')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printsjpbkhusus');
			$this->load->view('printsjpbkhusus/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
