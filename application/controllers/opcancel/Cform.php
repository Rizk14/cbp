<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu55')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opcancel/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query(" 	select * from (
        	select a.*, b.e_supplier_name from tm_op a, tr_supplier b
			where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.f_op_close='f'
			and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
			or upper(a.i_op) like '%$cari%')
			order by a.i_op desc
        ) as a
        where a.i_op not in (select i_op from tm_do where f_do_cancel = 'f') order by a.i_op desc",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('opcancel');
			$this->load->model('opcancel/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('opcancel/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cancel()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu55')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml = $this->input->post('jml', TRUE);
			$this->db->trans_begin();
			$this->load->model('opcancel/mmaster');
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$iop = $this->input->post('op'.$i, TRUE);
					$this->mmaster->updateop($iop);
				}
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			// $data['iarea']	= $area;
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cancel OP No:'.$iop;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			}

			$config['base_url'] = base_url().'index.php/opcancel/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query(" select * from (
        	select a.*, b.e_supplier_name from tm_op a, tr_supplier b
			where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.f_op_close='f'
			and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
			or upper(a.i_op) like '%$cari%')
			order by a.i_op desc
        ) as a
        where a.i_op not in (select i_op from tm_do where f_do_cancel = 'f') order by a.i_op desc",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('opcancel');
			$this->load->model('opcancel/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('opcancel/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu55')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('opcancel');
			$this->load->view('opcancel/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu55')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/opcancel/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);*/

			$cari=str_replace("%20", " ", $this->input->post("cari",false));
			if($cari=='')$cari=str_replace("%20", " ", $this->uri->segment(4));
			if($cari=='zxvf'){
				$cari='';
				$config['base_url'] = base_url().'index.php/opcancel/cform/cari/zxvf/';
			}else{
				$config['base_url'] = base_url().'index.php/opcancel/cform/cari/'.$cari.'/';
			}

			$query = $this->db->query(" select * from (
								        	select a.*, b.e_supplier_name from tm_op a, tr_supplier b
											where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.f_op_close='f'
											and (upper(a.i_reff) ilike '%$cari%' or upper(b.e_supplier_name) ilike '%$cari%' or upper(a.i_op) ilike '%$cari%')
											order by a.i_op desc
								        ) as a
								        where a.i_op not in (select i_op from tm_do where f_do_cancel = 'f') order by a.i_op desc",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('opcancel/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('opcancel');
			$data['iop']='';
	 		$this->load->view('opcancel/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
