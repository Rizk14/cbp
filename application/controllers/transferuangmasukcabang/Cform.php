<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu230')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transfermcab');
			$this->load->model('transferuangmasukcabang/mmaster');
			$data['ikum']='';
			$this->load->view('transferuangmasukcabang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu230')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transferm');
			$this->load->view('transferuangmasukcabang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu230')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transferm')." update";
			if(
				($this->uri->segment(4)!='')
			  ){
        $kum        	    = str_replace('|','/',$this->uri->segment(4));
				$data['ikum']     = str_replace('%20',' ',$kum);
				$data['nkumyear']	= $this->uri->segment(5);
				$data['iarea']		= $this->uri->segment(6);
				$data['dfrom']		= $this->uri->segment(7);
				$data['dto']		= $this->uri->segment(8);
				$ikum			      = str_replace('|','/',$this->uri->segment(4));
				$ikum			      = str_replace('%20',' ',$ikum);
				$nkumyear	      = $this->uri->segment(5);
				$iarea		      = $this->uri->segment(6);
				$dfrom		      = $this->uri->segment(7);
				$dto			      = $this->uri->segment(8);
        $data['pst']	= $this->session->userdata('i_area');
				$this->load->model("transferuangmasukcabang/mmaster");
				$data['isi']=$this->mmaster->baca($iarea,$ikum,$nkumyear);
		 		$this->load->view('transferuangmasukcabang/vformupdate',$data);
			}else{
				$this->load->view('transferuangmasukcabang/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu230')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikum 	= $this->input->post('ikum', TRUE);
			$dkum	= $this->input->post('dkum', TRUE);
			if($dkum!=''){
				$tmp=explode("-",$dkum);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkum=$th."-".$bl."-".$hr;
				$tahun=$th;
			}
			$iareaasal	  = $this->input->post('iareaasal', TRUE);
			$icustomer		= $this->input->post('icustomer', TRUE);
			$icustomergroupar	= $this->input->post('icustomergroupar', TRUE);
			$ecustomername		= $this->input->post('ecustomername', TRUE);
			if (
				($ikum != '') && ($iareaasal!='') && ($tahun!='')
			   )
			{
				$this->load->model('transferuangmasukcabang/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($ikum,$tahun,$icustomer,$icustomergroupar,$iareaasal);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
#					$this->db->trans_rollback();
					$this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update Transfer Uang Masuk Cabang No:'.$ikum;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ikum;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu230')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/transferuangmasukcabang/cform/customer/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
										left join tr_customer_groupar b on(a.i_customer=b.i_customer)
										left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
										left join tr_customer_owner d on(a.i_customer=d.i_customer)

										where a.i_area = '$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('transferuangmasukcabang/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
      $data['cari']='';
			$this->load->view('transferuangmasukcabang/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu230')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 	= $this->uri->segment(4);
			$cari 	= strtoupper($this->input->post('cari', FALSE));
      if( ($cari=='') && ($this->uri->segment(6)!='') )$cari=$this->uri->segment(5);
      if($cari!='')
  			$config['base_url'] = base_url().'index.php/transferuangmasukcabang/cform/caricustomer/'.$iarea.'/'.$cari.'/';
      else
  			$config['base_url'] = base_url().'index.php/transferuangmasukcabang/cform/caricustomer/'.$iarea.'/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
								left join tr_customer_groupar b on(a.i_customer=b.i_customer)
								left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
								left join tr_customer_owner d on(a.i_customer=d.i_customer)

								where a.i_area='$iarea' and ( 
									upper(a.i_customer) like '%$cari%' or 
									upper(a.e_customer_name) like '%$cari%' or 
									upper(d.e_customer_setor) like '%$cari%' )",false);
			$config['total_rows'] = $query->num_rows();
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
      if($cari!='')
  			$config['cur_page'] = $this->uri->segment(6);
      else
   			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transferuangmasukcabang/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
      if($cari!='')
  			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
      else
  			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
      $data['cari']=$cari;
			$this->load->view('transferuangmasukcabang/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
