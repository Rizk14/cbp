<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_ku_masuk');
			$data['dfrom']='';
			$data['dto']  ='';
			$data['ipl']  ='';
			$data['idt']  ='';
			$data['iarea']='';
			$data['status']='awal';
			$data['isi']  ='';
			$this->load->view('cekkumasuk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cekkumasuk/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query("select * from tm_kum 
			                          where d_kum >= to_date('$dfrom','dd-mm-yyyy') AND
			                          d_kum <= to_date('$dto','dd-mm-yyyy') and i_area ='$iarea'
			                          and f_kum_cancel='f'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_ku_masuk');
			$this->load->model('cekkumasuk/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
			$data['status'] = 'view';
			$data['idt']	= '';
			$data['ipl']	= '';
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('cekkumasuk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listpelunasan');
			$this->load->view('cekkumasuk/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
  			$data['page_title'] = $this->lang->line('cekkumasuk');
				$ikum = $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$dfrom= $this->uri->segment(6);
				$dto 	= $this->uri->segment(7);
				$tmp=explode("-",$dfrom);
		    $ikum = str_replace("spasi"," ",$ikum);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$y_kum=$th;
				$query = $this->db->query("select * from tm_kum 
													where i_kum = '$ikum' and i_area = '$iarea' 
													and d_kum >= to_date('$dfrom','dd-mm-yyyy') 
													AND d_kum <= to_date('$dto','dd-mm-yyyy')
													AND f_kum_cancel='f' AND n_kum_year='$y_kum'");
				$jml = $query->num_rows();
				$data['jmlitem'] = $query->num_rows();
				$data['ikum']    = $ikum;
				$data['iarea']	 = $iarea;
				$data['dfrom']   = $dfrom;
				$data['dto']	   = $dto;
				$data['status']  = 'edit';
				$status='edit';
				$this->load->model('cekkumasuk/mmaster');
				$data['isi']=$this->mmaster->bacakum($iarea,$ikum,$y_kum);
		 		$this->load->view('cekkumasuk/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cekkumasuk/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_kum
										where (upper(i_kum) like '%$cari%' or upper(i_customer) like '%$cari%')
										and i_area='$iarea' and f_kum_cancel='f'
										and d_kum >= to_date('$dfrom','dd-mm-yyyy')
										and d_kum <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_ku_masuk');

			$this->load->model('cekkumasuk/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['status'] = 'view';
			$data['iarea']	= $iarea;
			$data['idt']	= '';
			$data['ipl']	= '';
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('cekkumasuk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/cekkumasuk/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('cekkumasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('cekkumasuk/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                                    where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                    and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('cekkumasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('cekkumasuk/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function updatekum()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('cekkumasuk/mmaster');
			$ikum 	= $this->input->post('ikum', TRUE);
			$dkum 	= $this->input->post('dkum', TRUE);
			$iarea	= $this->input->post('iarea', TRUE);
			$ecek	= $this->input->post('ecek',TRUE);
			if($ecek=='')
				$ecek=null;
			$user		=$this->session->userdata('user_id');
			$this->mmaster->updatecekku($ecek,$user,$ikum,$dkum,$iarea);
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
	        while($row=pg_fetch_assoc($rs)){
		        $ip_address	  = $row['ip_address'];
		        break;
	        }
        }else{
	        $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
	        $now	  = $row['c'];
        }
        $pesan='Cek Ku Masuk No:'.$ikum.' Area:'.$iarea;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses'] = true;
				$data['inomor']	= $ikum;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   echo 'ini customer'.'<br>';
			$iarea 	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/cekkumasuk/cform/customer/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tr_customer
										where i_area = '$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('cekkumasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
			$this->load->view('cekkumasuk/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/cekkumasuk/cform/customer/'.$iarea.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_customer
									   	where i_area='$iarea' 
										and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
			$this->load->view('cekkumasuk/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function notaupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']		= $this->uri->segment(4);
			$data['iarea']		= $this->uri->segment(5);
			$data['idt']		= $this->uri->segment(6);
			$data['icustomer']	= $this->uri->segment(7);
			$baris				= $this->uri->segment(4);
			$iarea				= $this->uri->segment(5);
			$idt				= $this->uri->segment(6);
			$icustomer			= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/cekkumasuk/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
			$query = $this->db->query(" select i_customer_group from tr_customer where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_group;
      }
			$query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer b
										              where b.i_customer_group='$group' and a.i_customer=b.i_customer
										              and a.i_dt='$idt'
										              and a.i_area='$iarea'" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(9);
			$this->pagination->initialize($config);
			$this->load->model('cekkumasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
			$this->load->view('cekkumasuk/vlistnotaupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function nota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']		= $this->uri->segment(4);
			$data['iarea']		= $this->uri->segment(5);
			$data['idt']		= $this->uri->segment(6);
			$data['icustomer']	= $this->uri->segment(7);
			$baris				= $this->uri->segment(4);
			$iarea				= $this->uri->segment(5);
			$idt				= $this->uri->segment(6);
			$icustomer			= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/cekkumasuk/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
			$query = $this->db->query(" select i_customer_group from tr_customer where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_group;
      }
			$query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer b
										              where b.i_customer_group='$group' and a.i_customer=b.i_customer
										              and a.i_dt='$idt'
										              and a.i_area='$iarea'" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(9);
			$this->paginationxx->initialize($config);
			$this->load->model('cekkumasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
			$this->load->view('cekkumasuk/vlistnota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function giro()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekkumasuk/cform/giro/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tm_giro 
										where i_customer = '$icustomer' 
										and i_area='$iarea'
										and (f_giro_tolak='f' and f_giro_batal='f')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('cekkumasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_giro');
			$data['isi']=$this->mmaster->bacagiro($icustomer,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('cekkumasuk/vlistgiro', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kn()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekkumasuk/cform/kn/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tm_kn 
										where i_customer = '$icustomer' 
										and i_area='$iarea'
										and v_sisa>0",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_kn');
			$data['isi']=$this->mmaster->bacakn($icustomer,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('cekkumasuk/vlistkn', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function ku()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekkumasuk/cform/ku/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';

      $query = $this->db->query(" select i_customer_group from tr_customer where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_group;
      }
      if($group!='G0000'){
			  $query = $this->db->query("	select a.* from tm_kum a, tr_customer b
										  where b.i_customer_group='$group' and a.i_customer=b.i_customer
										  and a.i_area='$iarea'
										  and a.v_sisa>0
										  and a.f_close='f' ",false);
			}else{
        $query = $this->db->query("	select a.* from tm_kum a
										  where a.i_customer='$icustomer'
										  and a.i_area='$iarea'
										  and a.v_sisa>0
										  and a.f_close='f' ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('cekkumasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_ku');
      if($group!='G0000'){
  			$data['isi']=$this->mmaster->bacaku($icustomer,$iarea,$config['per_page'],$this->uri->segment(7),$group);
      }else{
  			$data['isi']=$this->mmaster->bacaku2($icustomer,$iarea,$config['per_page'],$this->uri->segment(7),$group);
      }
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('cekkumasuk/vlistku', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function lebihbayar()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekkumasuk/cform/lebihbayar/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	SELECT 	i_dt, min(v_jumlah), min(v_lebih), i_area,
											d_bukti,i_dt||'-'||max(substr(i_pelunasan,9,2))
										from tm_pelunasan_lebih
										where i_customer = '$icustomer'
										and i_area='$iarea'
										and v_lebih>0
										group by i_dt, d_bukti, i_area",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('cekkumasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_ku_masuk');
			$data['isi']=$this->mmaster->bacapelunasan($icustomer,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('cekkumasuk/vlistpelunasan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
