<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu344')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-lapgudperdiv');
			#$data['istore']='';
			$data['iperiode']='';
      $data['b']='';
      $data['nb']='';
      $data['r']='';
			$this->load->view('exp-lapgudperdivnas/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu344')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-lapgudperdiv');
			$this->load->view('exp-lapgudperdivnas/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu344')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-lapgudperdivnas/cform/store/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
				$query = $this->db->query("select distinct(c.i_store) as i_store
                                  from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store");
			} else {
				$query = $this->db->query("select distinct(c.i_store) as i_store
													         from tr_store_location a, tr_store b, tr_area c
													         where a.i_store = b.i_store and b.i_store=c.i_store
													         and (c.i_area = '$area1' or c.i_area = '$area2' or
													         c.i_area = '$area3' or c.i_area = '$area4' or
													         c.i_area = '$area5')");				
			}
			$config['total_rows'] = $query->num_rows(); 	
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-lapgudperdivnas/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('exp-lapgudperdivnas/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu344')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-lapgudperdivnas/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
        $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')",false);
      }else{
			  $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')
                                    and (c.i_area = '$area1' or c.i_area = '$area2' or
                                    c.i_area = '$area3' or c.i_area = '$area4' or
                                    c.i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-lapgudperdivnas/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('exp-lapgudperdivnas/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu344')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
         {
            $this->load->model('exp-lapgudperdivnas/mmaster');
            $iperiode	= $this->input->post('iperiode');
            #$istore = $this->input->post('istore');
            #$estorename= $this->input->post('estorename');
            #$istorelocation= $this->input->post('istorelocation');
            $b   = $this->input->post('chkbx');
            $nb   = $this->input->post('chknbx');
            $r   = $this->input->post('chkrx');
            #if($istore=='')$istore=$this->uri->segment(4);
            #if($istorelocation=='')$istore=$this->uri->segment(5);
            if($iperiode=='')$iperiode=$this->uri->segment(4);
            if($b=='') $b=$this->uri->segment(5);
            if($nb=='') $nb=$this->uri->segment(6);
            if($r=='') $r=$this->uri->segment(7);
            $c=substr($iperiode,0,4);
	          $d=substr($iperiode,4,2);
		        $peri=mbulan($d)." - ".$c;

            if($b=='qqq'){
              $judul='Bedding';
            } elseif($nb=='qqq'){
              $judul='Non Bedding';
            } elseif($r=='qqq'){
              $judul='Reguler'; 
            }
#            echo $nb;
#            die;
            $this->load->model('exp-lapgudperdivnas/mmaster');
                if($b=='qqq')
                {
/*
                  $this->db->select(" distinct(a.i_product),b.e_product_name
                                      from tm_mutasi a, tr_product b
                                      where  a.i_product=b.i_product
                                      and b.i_product_type='03' and a.e_mutasi_periode='$iperiode'
                                      order by b.e_product_name",false);
*/
                  $this->db->select(" distinct(a.i_product), a.e_product_name from
                                      (
                                      select distinct(a.i_product),b.e_product_name
                                      from f_mutasi_stock_daerah_all('$iperiode') a, tr_product b
                                      where  a.i_product=b.i_product and b.i_product_type='03'
                                      union all
                                      select distinct(a.i_product),b.e_product_name
                                      from f_mutasi_stock_pusat('$iperiode') a, tr_product b
                                      where  a.i_product=b.i_product and b.i_product_type='03'
                                      )
                                      as a
                                      order by a.e_product_name",false);
                }
                elseif($nb=='qqq')
                {
/*
                  $this->db->select(" distinct(a.i_product),b.e_product_name
                                      from tm_mutasi a, tr_product b
                                      where  a.i_product=b.i_product
                                      and b.i_product_type='08' and a.e_mutasi_periode='$iperiode'
                                      order by b.e_product_name",false);
*/
                  $this->db->select(" distinct(a.i_product), a.e_product_name from
                                      (
                                      select distinct(a.i_product),b.e_product_name
                                      from f_mutasi_stock_daerah_all('$iperiode') a, tr_product b
                                      where  a.i_product=b.i_product and b.i_product_type='08'
                                      union all
                                      select distinct(a.i_product),b.e_product_name
                                      from f_mutasi_stock_pusat('$iperiode') a, tr_product b
                                      where  a.i_product=b.i_product and b.i_product_type='08'
                                      )
                                      as a
                                      order by a.e_product_name",false);
                }
                elseif($r=='qqq')
                {
/*
                   $this->db->select("distinct(a.i_product),b.e_product_name
                                      from tm_mutasi a, tr_product b
                                      where  a.i_product=b.i_product
                                      and b.i_product_type<>'03' and b.i_product_type<>'08' 
                                      and a.e_mutasi_periode='$iperiode'
                                      order by b.e_product_name",false);
*/
                  $this->db->select(" distinct(a.i_product), a.e_product_name from
                                      (
                                      select distinct(a.i_product),b.e_product_name
                                      from f_mutasi_stock_daerah_all('$iperiode') a, tr_product b
                                      where  a.i_product=b.i_product and b.i_product_type<>'03' and b.i_product_type<>'08' 
                                      union all
                                      select distinct(a.i_product),b.e_product_name
                                      from f_mutasi_stock_pusat('$iperiode') a, tr_product b
                                      where  a.i_product=b.i_product and b.i_product_type<>'03' and b.i_product_type<>'08' 
                                      )
                                      as a
                                      order by a.e_product_name",false);
                }       
            $query = $this->db->get();
            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("Laporan Gudang Per Divisi Per Area")->setDescription(NmPerusahaan);
            $objPHPExcel->setActiveSheetIndex(0);
            if ($query->num_rows() > 0)
            {
               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'	=> 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A2:A4'
               );
               $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
               $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
               $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
               $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);

               $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan Gudang Per Divisi Per Area Periode '.$peri);
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,10,2);
               $objPHPExcel->getActiveSheet()->setCellValue('A3', "Gudang Nasional");
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,10,3);

               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'	=> 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A5:I5'
               );


               $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
               $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Kode');
               $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Nama');
               $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Harga');
               $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Penjualan');
               $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Saldo Akhir');
               $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Stock Opname');
               $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Nilai Penjualan');
               $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Nilai Saldo Akhir');
               $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $i=6;
               $j=6;
               $no=0;
               $totjual=0;
               $totsaldo=0;
               foreach($query->result() as $row)
               {
                  $no++;

                  $objPHPExcel->getActiveSheet()->duplicateStyleArray
                  (
                     array(
                        'font' => array
                        (
                           'name'   => 'Arial',
                           'bold'   => false,
                           'italic' => false,
                           'size'   => 10
                        )
                     ),
                     'A'.$i.':I'.$i
                  );

                $product=$row->i_product;
              if($b=='qqq')
              {
/*
                 $query = $this->db->query(" select distinct(a.i_product) as i_product,b.e_product_name,a.i_store, a.i_store_location, 
                                     b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                     sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                     sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                     from tm_mutasi a, tr_product b, tm_ic c
                                     where a.e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
                                     and b.i_product_type='03' and a.i_product=c.i_product and a.i_product_motif=c.i_product_motif
                                     and a.i_product_grade=c.i_product_grade and a.i_store=c.i_store 
                                     and a.i_store_location=c.i_store_location and b.i_product='$product'
                                     group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, b.e_product_name, 
                                     b.v_product_retail
                                     order by a.i_store, a.i_store_location, a.i_product",false);
*/
                  $query = $this->db->query(" select distinct(a.i_product) as i_product, a.e_product_name, a.i_store, a.i_store_location, 
                                              a.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(a.n_quantity_stock) as n_quantity_stock
                                              from (
                                              select distinct(a.i_product) as i_product,b.e_product_name,a.i_store, a.i_store_location, 
                                              b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                              from f_mutasi_stock_daerah_all('$iperiode') a, tr_product b, tm_ic c
                                              where a.i_product=b.i_product and b.i_product_type='03' and a.i_product=c.i_product 
                                              and a.i_product_motif=c.i_product_motif
                                              and a.i_product_grade=c.i_product_grade and a.i_store=c.i_store 
                                              and a.i_store_location=c.i_store_location and b.i_product='$product'
                                              group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, b.e_product_name, 
                                              b.v_product_retail
                                              union all
                                              select distinct(a.i_product) as i_product,b.e_product_name,a.i_store, a.i_store_location, 
                                              b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                              from f_mutasi_stock_pusat('$iperiode') a, tr_product b, tm_ic c
                                              where a.i_product=b.i_product and b.i_product_type='03' and a.i_product=c.i_product 
                                              and a.i_product_motif=c.i_product_motif
                                              and a.i_product_grade=c.i_product_grade and a.i_store=c.i_store 
                                              and a.i_store_location=c.i_store_location and b.i_product='$product'
                                              group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, b.e_product_name, 
                                              b.v_product_retail
                                              ) as a
                                              group by a.i_store, a.i_store_location, a.i_product, a.e_product_name, a.v_product_retail
                                              order by a.i_store, a.i_store_location, a.i_product",false);
              }
              elseif($nb=='qqq')
              {
/*
                $query = $this->db->query(" select distinct(a.i_product) as i_product,b.e_product_name,a.i_store, a.i_store_location, 
                                     b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                     sum(n_mutasi_penjualan) as n_mutasi_penjualan,sum(n_saldo_akhir) as n_saldo_akhir,
                                     sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                     from tm_mutasi a, tr_product b, tm_ic c
                                     where a.e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
                                     and b.i_product_type='08' and a.i_product=c.i_product and a.i_product_motif=c.i_product_motif
                                     and a.i_product_grade=c.i_product_grade and a.i_store=c.i_store 
                                     and a.i_store_location=c.i_store_location and b.i_product='$product'
                                     group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, b.e_product_name,   
                                     b.v_product_retail
                                     order by a.i_store, a.i_store_location, a.i_product",false);
*/
                  $query = $this->db->query(" select distinct(a.i_product) as i_product, a.e_product_name, a.i_store, a.i_store_location, 
                                              a.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(a.n_quantity_stock) as n_quantity_stock
                                              from (
                                              select distinct(a.i_product) as i_product,b.e_product_name,a.i_store, a.i_store_location, 
                                              b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                              from f_mutasi_stock_daerah_all('$iperiode') a, tr_product b, tm_ic c
                                              where a.i_product=b.i_product and b.i_product_type='08' and a.i_product=c.i_product 
                                              and a.i_product_motif=c.i_product_motif
                                              and a.i_product_grade=c.i_product_grade and a.i_store=c.i_store 
                                              and a.i_store_location=c.i_store_location and b.i_product='$product'
                                              group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, b.e_product_name, 
                                              b.v_product_retail
                                              union all
                                              select distinct(a.i_product) as i_product,b.e_product_name,a.i_store, a.i_store_location, 
                                              b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                              from f_mutasi_stock_pusat('$iperiode') a, tr_product b, tm_ic c
                                              where a.i_product=b.i_product and b.i_product_type='08' and a.i_product=c.i_product 
                                              and a.i_product_motif=c.i_product_motif
                                              and a.i_product_grade=c.i_product_grade and a.i_store=c.i_store 
                                              and a.i_store_location=c.i_store_location and b.i_product='$product'
                                              group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, b.e_product_name, 
                                              b.v_product_retail
                                              ) as a
                                              group by a.i_store, a.i_store_location, a.i_product, a.e_product_name, a.v_product_retail
                                              order by a.i_store, a.i_store_location, a.i_product",false);
              }
              elseif($r=='qqq')
              {
/*
                 $query = $this->db->query(" select distinct(a.i_product) as i_product,b.e_product_name,a.i_store, a.i_store_location, 
                                     b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                     sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                     sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                     from tm_mutasi a, tr_product b, tm_ic c
                                     where a.e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
                                     and b.i_product_type<>'08' and b.i_product_type<>'03' and a.i_product=c.i_product 
                                     and a.i_product_motif=c.i_product_motif and a.i_product_grade=c.i_product_grade
                                     and a.i_store=c.i_store and b.i_product='$product'
                                     and a.i_store_location=c.i_store_location
                                     group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, b.e_product_name,   
                                     b.v_product_retail
                                     order by a.i_store, a.i_store_location, a.i_product",false);
*/
                  $query = $this->db->query(" select distinct(a.i_product) as i_product, a.e_product_name, a.i_store, a.i_store_location, 
                                              a.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(a.n_quantity_stock) as n_quantity_stock
                                              from (
                                              select distinct(a.i_product) as i_product,b.e_product_name,a.i_store, a.i_store_location, 
                                              b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                              from f_mutasi_stock_daerah_all('$iperiode') a, tr_product b, tm_ic c
                                              where a.i_product=b.i_product and b.i_product_type<>'08' and b.i_product_type<>'03' 
                                              and a.i_product=c.i_product and a.i_product_motif=c.i_product_motif
                                              and a.i_product_grade=c.i_product_grade and a.i_store=c.i_store 
                                              and a.i_store_location=c.i_store_location and b.i_product='$product'
                                              group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, b.e_product_name, 
                                              b.v_product_retail
                                              union all
                                              select distinct(a.i_product) as i_product,b.e_product_name,a.i_store, a.i_store_location, 
                                              b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                              from f_mutasi_stock_pusat('$iperiode') a, tr_product b, tm_ic c
                                              where a.i_product=b.i_product and b.i_product_type<>'08' and b.i_product_type<>'03'
                                              and a.i_product=c.i_product and a.i_product_motif=c.i_product_motif
                                              and a.i_product_grade=c.i_product_grade and a.i_store=c.i_store 
                                              and a.i_store_location=c.i_store_location and b.i_product='$product'
                                              group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, b.e_product_name, 
                                              b.v_product_retail
                                              ) as a
                                              group by a.i_store, a.i_store_location, a.i_product, a.e_product_name, a.v_product_retail
                                              order by a.i_store, a.i_store_location, a.i_product",false);
              }

                if($query->num_rows()>0){
                  foreach($query->result() as $raw){
                    $prodname=$raw->e_product_name;
                    $mutasijual=$raw->n_mutasi_penjualan;
                    $saldoakhir=$raw->n_saldo_akhir-$raw->n_git_penjualan-$raw->n_mutasi_git;
                    $harga=$raw->v_product_retail;
                    $so=$raw->n_saldo_stockopname+$raw->n_git_penjualan+$raw->n_mutasi_git;
                  }
                }
                $nilaijual=$mutasijual*$harga;
                $nilaisaldo=$saldoakhir*$harga;
                $totjual=$totjual+$nilaijual;
                $totsaldo=$totsaldo+$nilaisaldo;

                  $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $no, Cell_DataType::TYPE_NUMERIC);
                  $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_product, Cell_DataType::TYPE_STRING);
                  $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->e_product_name, Cell_DataType::TYPE_STRING);
                  $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $harga, Cell_DataType::TYPE_NUMERIC);
                  $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $mutasijual, Cell_DataType::TYPE_NUMERIC);
                  $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $saldoakhir, Cell_DataType::TYPE_NUMERIC);
                  $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $so, Cell_DataType::TYPE_NUMERIC);
                  $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $nilaijual, Cell_DataType::TYPE_NUMERIC);
                  $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $nilaisaldo, Cell_DataType::TYPE_NUMERIC);

					        $i++;
					        $j++;
               }
               $x=$i-1;
            }
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'TOTAL');
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $totjual);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $totsaldo);
			      $objPHPExcel->getActiveSheet()->getStyle('H'.$i.':I'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER);
            #$objPHPExcel->getActiveSheet()->getStyle('G6:H'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $nama='lapgudnas-'.$judul.'-'.$iperiode.'.xls';
            if(file_exists('spb/00/'.$nama))
            {
               @chmod('excel/00/'.$nama, 0777);
               @unlink('excel/00/'.$nama);
            }
            $objWriter->save("excel/00/".$nama); 
            @chmod('excel/00/'.$nama, 0777);

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
	          while($row=pg_fetch_assoc($query)){
	          	$now	  = $row['c'];
			      }
			      $pesan='Export Laporan Gudang Per Divisi Nasional Periode:'.$iperiode;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan ); 

            $data['sukses']   = true;
            $data['inomor']   = "Ekspor Laporan Gudang Per Divisi Nasional berhasil ke file ".$nama;
            $this->load->view('nomor',$data);
         }
         else
         {
            $this->load->view('awal/index.php');
      }
   }
}
?>
