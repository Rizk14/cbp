<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu271')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transnota');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('transnota/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu271')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
#      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
#			if($iarea=='') $iarea=$this->uri->segment(5);
			$data['page_title'] = $this->lang->line('transnota');
			$data['iperiode']	= $iperiode;
      $tahun=substr($iperiode,0,4);      
#			$data['iarea']	= $iarea;

      define ('BOOLEAN_FIELD',   'L');
      define ('CHARACTER_FIELD', 'C');
      define ('DATE_FIELD',      'D');
      define ('NUMBER_FIELD',    'N');
      define ('READ_ONLY',  '0');
      define ('WRITE_ONLY', '1');
      define ('READ_WRITE', '2');
#      $db_file = 'nota/nt'.$iarea.substr($iperiode,2,4).'.dbf';
      $db_file = 'nota/nt'.$iperiode.'.dbf';
      $dbase_definition = array (
         array ('KODETRAN',  CHARACTER_FIELD,  2),
         array ('NODOK',  CHARACTER_FIELD,  7),
         array ('TGLDOK',  DATE_FIELD),
         array ('NOSJ',  CHARACTER_FIELD,  6),
         array ('TGLSJ',  DATE_FIELD),
         array ('NOSPB',  CHARACTER_FIELD,  6),
         array ('TGLSPB',  DATE_FIELD),
         array ('KODELANG',  CHARACTER_FIELD,  5),
         array ('KODESALES',  CHARACTER_FIELD,  2),
         array ('PLUSPPN', BOOLEAN_FIELD),
         array ('PLUSDISC', BOOLEAN_FIELD),
         array ('KOTOR',  NUMBER_FIELD,  10,0),
         array ('POTONGAN', NUMBER_FIELD, 10, 0),     
         array ('DISCOUNT', NUMBER_FIELD, 7, 2),
         array ('DISC2',  NUMBER_FIELD,  4, 1),  
         array ('DISC3',  NUMBER_FIELD,  4, 1),  
         array ('DISC4',  NUMBER_FIELD,  4, 1),  
         array ('BATAL', BOOLEAN_FIELD),
         array ('TGLBUAT', CHARACTER_FIELD,  20),
         array ('TGLUBAH', CHARACTER_FIELD,  20)
      );
      if(file_exists($db_file)){
        @chmod($db_file, 0777);
        @unlink($db_file);
      }
      $create = @ dbase_create($db_file, $dbase_definition)
                or die ("Could not create dbf file <i>$db_file</i>.");
      $id = @ dbase_open ($db_file, READ_WRITE)
            or die ("Could not open dbf file <i>$db_file</i>."); 
      $per=substr($iperiode,2,4);
#      $dicari='FP-'.$per.'-'.$iarea.'%';
      $dicari='FP-'.$per.'-%';
      $sql	  = " * from tm_nota where i_nota like '$dicari'";
      $this->db->select($sql);
      $query = $this->db->get();
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
          $kodetran         = '03';
	        $nodok            = substr($rowsj->i_nota,8,7);
	        $tgldok           = substr($rowsj->d_nota,0,4).substr($rowsj->d_nota,5,2).substr($rowsj->d_nota,8,2);
	        $nosj             = substr($rowsj->i_sj,8,6);
	        $tglsj            = substr($rowsj->d_sj,0,4).substr($rowsj->d_sj,5,2).substr($rowsj->d_sj,8,2);
	        $nospb            = substr($rowsj->i_spb,9,6);
	        $tglspb           = substr($rowsj->d_spb,0,4).substr($rowsj->d_spb,5,2).substr($rowsj->d_spb,8,2);
	        $kodelang         = $rowsj->i_customer;
	        $kodesales        = $rowsj->i_salesman;
	        $plusppn          = $rowsj->f_plus_ppn;
	        $plusdiscount     = $rowsj->f_plus_discount;
          $kotor            = $rowsj->v_nota_gross;
          $potongan         = $rowsj->v_nota_discounttotal;
	        $discount         = $rowsj->n_nota_discount1;
	        $disc2            = $rowsj->n_nota_discount2;
	        $disc3            = $rowsj->n_nota_discount3;
	        $disc4            = $rowsj->n_nota_discount4;
          if($rowsj->f_plus_ppn=='f'){ 
            $plusppn='F'; 
          }else{ 
            $plusppn='T';
          }
          if($rowsj->f_plus_discount=='f'){ 
            $plusdiscount='F'; 
          }else{ 
            $plusdiscount='T';
          }
          if($rowsj->f_nota_cancel=='f'){ 
            $batal='F'; 
          }else{ 
            $batal='T';
          }
          $tglbuat          = $rowsj->d_sj_entry;
          $tglubah          = $rowsj->d_sj_update;
          $isi = array ($kodetran,$nodok,$tgldok,$nosj,$tglsj,$nospb,$tglspb,$kodelang,$kodesales,$plusppn, $plusdiscount,
                        $kotor,$potongan,$discount,$disc2,$disc3,$disc4,$batal,$tglbuat,$tglubah);
          dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>."); 
        }
      }
      dbase_close($id);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
      @chmod($db_file, 0777);
#			$pesan='Transfer ke Nota lama Area '.$iarea.' Periode:'.$iperiode;
			$pesan='Transfer ke Nota lama Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses']			= true;
			$data['inomor']			= $pesan;
			$this->load->view('nomor',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu271')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listspmb/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspmb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu271')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listspmb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspmb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
