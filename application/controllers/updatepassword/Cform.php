<?php
class Cform extends CI_Controller
{
	public $title	= "Update Password";
	// public $menu	= "100C0013";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}

	function index()
	{
		$iuser 			= $this->session->userdata('user_id');
		$epasswordold	= $this->input->post('epasswordold', TRUE);
		$eusername	    = $this->session->userdata('user_name');
		$epasswordnew1	= $this->input->post('epasswordnew1', TRUE);
		$epasswordnew2	= $this->input->post('epasswordnew2', TRUE);

		if (
			(isset($epasswordold) && $epasswordold != '') &&
			(isset($epasswordnew1) && $epasswordnew1 != '') &&
			(isset($epasswordnew2) && $epasswordnew2 != '') &&
			($epasswordnew2 == $epasswordnew1)
		) {

			$this->load->model('updatepassword/mmaster');

			$pass  = $this->session->userdata('xxx');

			if (md5(md5($epasswordold)) == $pass) {
				if (md5(md5($epasswordold)) == md5(md5($epasswordnew1))) {
					$data['inomor']			= 'Password baru tidak boleh sama dengan password lama...';
					$data['page_title'] = $this->lang->line('updatepassword');

					$this->load->view('updatepassword/vinsert_fail', $data);
				} else {
					// $this->db->trans_begin();

					$this->mmaster->update($iuser, $eusername, md5(md5($epasswordnew1)));

					// if ($this->db->trans_status() === FALSE || $this->db->trans_status() === false) {
					// 	$this->db->trans_rollback();
					// } else {
					// 	$this->db->trans_commit();

					$this->logger->writenew("Update Password");
					// }
				}
			} else {
				$data['page_title'] = $this->lang->line('updatepassword');
				$data['iuser'] 			= $this->session->userdata('user_id');
				$data['password']		= $this->session->userdata('epassword');
				$data['eusername']	= $this->session->userdata('username');

				$this->load->model('updatepassword/mmaster');

				$data['isi'] = $this->mmaster->baca($iuser);

				$this->logger->writenew("Menu : Update Password Per 3 bulan");

				$this->load->view('updatepassword/vmainform', $data);
			}
		}
	}

	function insert_fail()
	{
		$data['page_title'] = $this->lang->line('updatepassword');
		$this->load->view('updatepassword/vinsert_fail', $data);
	}
	function sukses()
	{
		$this->load->view('main/index');
		/*			$data['page_title'] = $this->lang->line('updatepassword');
			$this->load->view('updatepassword/vsuksesform',$data);*/
		$data['sukses']			= true;
		$data['inomor']			= 'password berhasil dirubah';
		$this->load->view('nomor', $data);
	}
}
