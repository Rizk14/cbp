<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu146')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = 'SJPB Ke SJ';
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('sjpbsj/vform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function sjpb(){
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu146')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
     	if($cari=='' && $this->uri->segment(4)!='zxasqw') $cari=$this->uri->segment(4);
     	if($cari==''){
  			$config['base_url'] = base_url().'index.php/sjpbsj/cform/sjpb/zxasqw/';
  	  }else{
  			$config['base_url'] = base_url().'index.php/sjpbsj/cform/sjpb/'.$cari.'/';
		}
		$query	=  $this->db->query(" 	SELECT
											a.i_sjpb,
											a.i_customer,
											b.e_customer_name,
											a.d_sjpb,
											a.d_sjpb_receive,
											a.i_spb,
                            				a.i_area
										FROM
											tm_sjpb a,
											tr_customer b
										WHERE
											a.i_customer = b.i_customer
											AND b.e_customer_name LIKE 'CLANDY%'
											AND NOT a.d_sjpb_receive ISNULL
											AND a.d_sjpb >= '2019-05-01'
											AND (a.i_sjpb LIKE '%$cari%'
											OR a.i_customer LIKE '%$cari%'
											OR b.e_customer_name LIKE '%$cari%')
											/* AND a.i_spb isnull */
											ORDER BY a.d_sjpb_receive DESC ",false);
     		$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('sjpbsj/mmaster');
			$data['page_title'] = 'SJPB';
			$data['isi']=$this->mmaster->bacasjpb($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('sjpbsj/vlistsjpb', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kodeharga(){
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu146')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
     	if($cari=='' && $this->uri->segment(4)!='zxasqw') $cari=$this->uri->segment(4);
     	if($cari==''){
  			$config['base_url'] = base_url().'index.php/sjpbsj/cform/kodeharga/zxasqw/';
  	  }else{
  			$config['base_url'] = base_url().'index.php/sjpbsj/cform/kodeharga/'.$cari.'/';
		}
		$query =  $this->db->query(" select i_price_group from tr_product_priceco
		where i_price_group like '%$cari%'
		group by i_price_group
		order by i_price_group asc",false);
     	$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('sjpbsj/mmaster');
			$data['page_title'] = 'Kode Harga';
			$data['isi']=$this->mmaster->bacakodeharga($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('sjpbsj/vlistkodeharga', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function view(){
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu146')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
		){
			$i_sjpb = $this->input->post('i_sjpb', TRUE);
			$i_kode_harga = $this->input->post('i_kode_harga', TRUE);
			$pilihan = $this->input->post('pilihan', TRUE);

			$this->load->model('sjpbsj/mmaster');
			$nilaikotor = $this->mmaster->carinilaikotor($i_sjpb, $i_kode_harga,$pilihan);
			$data = array(
				'page_title' => 'SJPB Ke SJ',
				'header' => $this->mmaster->data_sjpb_header($i_sjpb),
				'detail' => $this->mmaster->data_sjpb_item($i_sjpb, $i_kode_harga),
				'nilaikotor' => $nilaikotor,
				'i_kode_harga' => $i_kode_harga,
				'i_sjpb' => $i_sjpb,
				'i_kode_harga' => $i_kode_harga,
				'pilihan' => $pilihan,
				'sj_nota' => $this->mmaster->cek_sj_nota($i_sjpb)
			);
			$this->load->view('sjpbsj/vformupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function simpan(){
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu146')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
		){
			$this->db->trans_begin();
			$this->load->model('sjpbsj/mmaster');
			$icustomer		= $this->input->post('icustomer', TRUE);
			$dspb    		= $this->input->post('dspb', TRUE);
			$i_sjpb 		= $this->input->post('i_sjpb', TRUE);
			$i_kode_harga 	= $this->input->post('i_kode_harga', TRUE);
			$iarea 			= $this->input->post('iarea', TRUE);
			$ecustomername 	= $this->input->post('ecustomername', TRUE);
			$pilihan 		= $this->input->post('pilihan', TRUE);

			if($dspb!=''){
				$tmp=explode("-",$dspb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspb=$th."-".$bl."-".$hr;
				$thbl=substr($th,2,2).$bl;
				$thblsj = $th.$bl;
			}

			$awalnama=substr($ecustomername,0,5);
			if($awalnama=='GRIYA' or $awalnama=='SUSAN' or $awalnama=='ASIA ' or $awalnama=='RUMAH'){
				$namagrup='YOG';
			}elseif($awalnama=='TIARA'){
				$namagrup='CLA';
			}elseif($awalnama=='PT PE'){
				$namagrup='SUZ';
			}elseif($awalnama=='PT. K'){
				$namagrup='PT.';
			}elseif($awalnama=='MACAD'){
				$namagrup='MAC';
			}elseif($awalnama=='CV AN'){
				$namagrup='ATH';
			}elseif($awalnama=='STOCK'){
				$namagrup='STO';
			}elseif(substr($awalnama,0,3)=='MDS'){
				$namagrup='MDS';
			}else{
				$namagrup=substr($ecustomername,0,3);
			}
			$enama_group =$ecustomername;

			 $ispb    = $this->mmaster->runningnumberspb($thblsj,$namagrup,$iarea,$enama_group);
			 $this->mmaster->insert_spb_header($ispb, $icustomer,$dspb);
			 $this->mmaster->update_sjpb($i_sjpb, $icustomer, $ispb);
			 $detail = $this->mmaster->data_sjpb_item($i_sjpb, $i_kode_harga);
			
			 $nitemno = 1;
			 
			 foreach ($detail as $row) {
				 $iproduct = $row->i_product;
				 $iproductgrade = $row->i_product_grade;
				 $iproductmotif = $row->i_product_motif;
				 $norder = $row->n_receive;
				 if($pilihan == 'biasa'){                               
					$vunitprice = $row->bersih;
				}else{
					$vunitprice = $row->v_product_retail;
				}
				 $eproductname = $row->e_product_name;

				$this->mmaster->insert_spb_item($ispb, $iproduct, $iproductgrade, $iproductmotif, $norder, $vunitprice, $eproductname, $nitemno);
				$nitemno++;
			 }
			 
			 $nilaikotor = $this->mmaster->carinilaikotor($i_sjpb, $i_kode_harga, $pilihan);
			 if($pilihan == 'biasa'){                               
				$diskon1persen = ($nilaikotor->nilaikotor * 0.01);
				$v_spb_after = ($nilaikotor->nilaikotor - $diskon1persen);
			}else{
				$diskon1persen = 0;
				$v_spb_after = $nilaikotor->nilaikotor;
			}

			 $this->mmaster->update_spb_header($ispb, $diskon1persen, $nilaikotor->nilaikotor, $v_spb_after, $pilihan);

			 $isj = $this->mmaster->runningnumbersj($thblsj);

			 $this->mmaster->insert_sj_header($isj, $ispb, $dspb, $icustomer, $diskon1persen, $nilaikotor->nilaikotor, $v_spb_after, $pilihan);
			 $this->mmaster->updatespbsj($ispb,$isj,$dspb);
			 $istore = 'PB';
			 $istorelocation = '00';
			 $istorelocationbin = '00';
			 $nitemno = 1;
			 foreach ($detail as $row) {
				$iproduct = $row->i_product;
				$iproductgrade = $row->i_product_grade;
				$iproductmotif = $row->i_product_motif;
				$norder = $row->n_receive;
				if($pilihan == 'biasa'){                               
					$vunitprice = $row->bersih;
				}else{
					$vunitprice = $row->v_product_retail;
				}
				$eproductname = $row->e_product_name;
				$ndeliver = $norder;

			   $this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$norder,
			   $vunitprice,$isj,$nitemno);
			   $nitemno++;

			   $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
			   if(isset($trans)){
				 foreach($trans as $itrans)
				 {
				   $q_aw =$itrans->n_quantity_stock;
				   $q_ak =$itrans->n_quantity_stock;
#                    $q_in =$itrans->n_quantity_in;
#                    $q_out=$itrans->n_quantity_out;
				   $q_in =0;
				   $q_out=0;
				   break;
				 }
			   }else{
				 $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
				 if(isset($trans)){
				   foreach($trans as $itrans)
				   {
					 $q_aw =$itrans->n_quantity_stock;
					 $q_ak =$itrans->n_quantity_stock;
					 $q_in =0;
					 $q_out=0;
					 break;
				   }
				 }else{
				   $q_aw=0;
				   $q_ak=0;
				   $q_in=0;
				   $q_out=0;
				 }
			   }
			   $this->mmaster->inserttrans04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$ndeliver,$q_aw,$q_ak);
			   $th=substr($dspb,0,4);
			   $bl=substr($dspb,5,2);
			   $emutasiperiode=$th.$bl;
	 
			   if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
			   {
				 $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
			   }else{
				 $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode,$q_aw);
			   }
			   if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
			   {
				 $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$q_ak);
			   }else{
				 $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ndeliver,$q_aw);
			   }
			}


			 if ( ($this->db->trans_status() == FALSE) )
            {
              $this->db->trans_rollback();
            }else{
				$this->db->trans_commit();

				$sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
                  $now    = $row['c'];
               }
			   $pesan='Input SJPB ke SJ SPB :  '.$ispb.' SJ :'.$isj;
			   
               $this->load->model('logger');
			   $this->logger->write($id, $ip_address, $now , $pesan );
			   
				$data['sukses']         = true;
				$data['inomor']         = $ispb." - ".$isj;
				$this->load->view('nomor',$data);
				
			}

		}else{
			$this->load->view('awal/index.php');
		}
	}

}
?>