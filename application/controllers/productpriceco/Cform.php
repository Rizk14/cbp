<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu445')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = 'Harga Barang Konsinyasi';
			$data['iproduct']='';
			$this->load->model('productpriceco/mmaster');
#      $data['isi']=$this->mmaster->bacakode();
			$cari = strtoupper($this->input->post('cari', FALSE));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Harga Barang Konsinyasi";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('productpriceco/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			  (($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu445')=='t')) ||
			  (($this->session->userdata('logged_in')) &&
			  ($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	    = $this->input->post('iproduct', TRUE);
			$eproductname 	= $this->input->post('eproductname', TRUE);
			$iproductgrade 	= $this->input->post('iproductgrade', TRUE);
			$ipricegroup   	= $this->input->post('ipricegroup', TRUE);
		  $vproductretail	= $this->input->post('vproductretail', TRUE);
			$vproductretail = str_replace(",","",$this->input->post('vproductretail', TRUE));
			if($vproductretail=='')
				$vproductretail=0;
      $vproductmill   = $this->input->post('vproductmill', TRUE);
			$vproductmill	  = str_replace(",","",$this->input->post('vproductmill', TRUE));
			if($vproductmill=='')
				$vproductmill=0;
			$ipricegroupco   	= $this->input->post('ipricegroupco', TRUE);
			if ((isset($iproduct) && $iproduct != '') && (isset($iproductgrade) && $iproductgrade != '') && (isset($vproductretail) && $vproductretail != '' && $vproductretail != '0') && (isset($vproductmill) && $vproductmill != '') && (isset($ipricegroup) && $ipricegroup != '') && (isset($ipricegroupco) && $ipricegroupco != ''))
			{
			  $this->load->model('productpriceco/mmaster');
			  $this->db->trans_begin();
#        $hitung=($vproductretail-$vproductmill)/$vproductretail;
#        $nmargin=$hitung*100;
        $nmargin=$this->input->post('nmargin', TRUE);
        if($this->mmaster->cekco($iproduct,$ipricegroup,$iproductgrade))
        {
  	      $this->mmaster->insert($iproduct,$ipricegroup,$iproductgrade,$vproductretail,$nmargin,$ipricegroupco);
        }else{
		      $this->mmaster->update($iproduct,$ipricegroup,$iproductgrade,$vproductretail,$nmargin,$ipricegroupco);
        }
        if($this->mmaster->cekharga($iproduct,$ipricegroup,$iproductgrade))
        {
		      $this->mmaster->insertnet($iproduct,$ipricegroup,$iproductgrade,$eproductname,$vproductmill,$nmargin);
        }else{
		      $this->mmaster->updatenet($iproduct,$ipricegroup,$iproductgrade,$eproductname,$vproductmill,$nmargin);
        }
        if ( ($this->db->trans_status() === FALSE) )
        {
          $this->db->trans_rollback();
        }else{
          $this->db->trans_commit();
#             $this->db->trans_rollback();

          	$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Input Harga Barang Konsinyasi ('.$iproduct.')';
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

          $data['sukses']         = true;
          $data['inomor']         = $iproduct;
          $this->load->view('nomor',$data);
        }

			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
  			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu445')=='t')) ||
	  		(($this->session->userdata('logged_in')) &&
	  		($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = 'Harga Barang Konsinyasi';
			$this->load->view('productpriceco/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
  			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu445')=='t')) ||
	  		(($this->session->userdata('logged_in')) &&
	  		($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = "Harga Barang Konsinyasi Update";
			if( ($this->uri->segment(4)) && ($this->uri->segment(5)) && ($this->uri->segment(6)) ){
				$iproduct = $this->uri->segment(4);
				$iproductgrade = $this->uri->segment(5);
				$ipricegroup = $this->uri->segment(6);
				$data['iproduct'] = $iproduct;
				$data['iproductgrade'] = $iproductgrade;
				$data['ipricegroup'] = $ipricegroup;
				$this->load->model('productpriceco/mmaster');
				$data['isi']=$this->mmaster->baca($iproduct,$iproductgrade,$ipricegroup);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Harga Barang Konsinyasi ('.$iproduct.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('productpriceco/vmainform',$data);
			}else{
				$this->load->view('productpriceco/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
  			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu445')=='t')) ||
	  		(($this->session->userdata('logged_in')) &&
	  		($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	    = $this->input->post('iproduct', TRUE);
			$eproductname 	= $this->input->post('eproductname', TRUE);
			$iproductgrade 	= $this->input->post('iproductgrade', TRUE);
			$ipricegroup   	= $this->input->post('ipricegroup', TRUE);
			$vproductmill	  = $this->input->post('vproductmill', TRUE);
			$vproductmill	  = str_replace(",","",$this->input->post('vproductmill', TRUE));
			if($vproductmill=='')
				$vproductmill=0;
			$vproducretail  = $this->input->post('vproductmill', TRUE);
			$vproductretail = str_replace(",","",$this->input->post('vproductretail', TRUE));
			if($vproductretail=='')
				$vproductretail=0;
      $hitung=($vproductretail-$vproductmill)/$vproductretail;
      $nmargin=$hitung*100;			
			$this->load->model('productpriceco/mmaster');
			$this->mmaster->update($iproduct,$eproductname,$iproductgrade,$nmargin,$vproductretail);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Harga Barang Konsinyasi ('.$iproduct.')';
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

	        $data['page_title'] = 'Harga Barang Konsinyasi';
			$data['iproduct']='';
			$this->load->model('productpriceco/mmaster');
#      $data['isi']=$this->mmaster->bacakode();
			$cari = strtoupper($this->input->post('cari', FALSE));

			$this->load->view('productpriceco/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu445')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	    = $this->uri->segment(4);
			$iproductgrade	= $this->uri->segment(5);
			$ipricegroup  	= $this->uri->segment(6);
			$this->load->model('productpriceco/mmaster');
			$this->mmaster->delete($iproduct,$iproductgrade,$ipricegroup);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Delete Harga Barang Konsinyasi ('.$iproduct.')';
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);
	        
			$config['base_url'] = base_url().'index.php/productpriceco/cform/index/';
			$query = $this->db->query(" select a.*, b.e_product_name
                                  from tr_product_priceco a, tr_product b
                                  where a.i_product=b.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '20';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = 'Harga Barang Konsinyasi';
			$data['iproduct']='';
			$data['iproductgrade']='';
			$data['ipricegroup']='';
			$this->load->model('productpriceco/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('productpriceco/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu445')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productpriceco/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select a.*, b.e_product_name
                                  from tr_product_priceco a, tr_product b
                                  where a.i_product=b.i_product
					                        and (upper(a.i_product) like '%$cari%' 
					                        or upper(a.e_product_name) = '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '20';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('productpriceco/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = 'Harga Barang Konsinyasi';
			$data['iproduct']='';
			$data['iproductgrade']='';
			$data['ipricegroup']='';
	 		$this->load->view('productpriceco/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productgrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu445')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productpriceco/cform/productgrade/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_product_grade from tr_product_grade 
										where upper(i_product_grade) like '%$cari%' or upper(e_product_gradename) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productpriceco/mmaster');
			$data['page_title'] = $this->lang->line('list_productgrade');
			$data['isi']=$this->mmaster->bacaproductgrade($config['per_page'],$this->uri->segment(5));
			$this->load->view('productpriceco/vlistproductgrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductgrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu445')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productpriceco/cform/productgrade/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_product_grade from tr_product_grade 
										where upper(i_product_grade) like '%$cari%' or upper(e_product_gradename) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productpriceco/mmaster');
			$data['page_title'] = $this->lang->line('list_productgrade');
			$data['isi']=$this->mmaster->cariproductgrade($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productpriceco/vlistproductgrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu445')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris=$this->uri->segment(4);
			$cari =$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/productpriceco/cform/product/'.$baris.'/'.$cari.'/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_product from tr_product
										where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productpriceco/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($cari,$config['per_page'],$this->uri->segment(7));
			$this->load->view('productpriceco/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu445')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/productpriceco/cform/product/'.$cari.'/'.'index/';
			$query = $this->db->query("select i_product from tr_product
						   where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productpriceco/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productpriceco/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function pricegroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu445')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
       $cari=strtoupper($this->input->post('cari'));
       if($cari=='' && $this->uri->segment(4)!='zxasqw') $cari=$this->uri->segment(4);
       if($cari==''){
         $config['base_url'] = base_url().'index.php/productpriceco/cform/pricegroup/zxasqw/';
       }else{
        $config['base_url'] = base_url().'index.php/productpriceco/cform/pricegroup/'.$cari.'/';
       }
       $query = $this->db->query("select i_price_groupco from tr_price_groupco 
                                  where upper(i_price_groupco) like '%$cari%' or upper(e_price_groupconame) like '%$cari%'",false);
       $config['total_rows'] = $query->num_rows();
       $config['per_page'] = '10';
       $config['first_link'] = 'Awal';
       $config['last_link'] = 'Akhir';
       $config['next_link'] = 'Selanjutnya';
       $config['prev_link'] = 'Sebelumnya';
       $config['cur_page'] = $this->uri->segment(5);
       $this->pagination->initialize($config);

       $this->load->model('productpriceco/mmaster');
       $data['cari']=$cari;
       $data['page_title'] = $this->lang->line('list_department');
       $data['isi']=$this->mmaster->bacapricegroupco($config['per_page'],$this->uri->segment(5),$cari);
       $this->load->view('productpriceco/vlistpricegroupco', $data);
  	}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
