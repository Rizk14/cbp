<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu88')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_promotype');
			$data['ipromotype']='';
			$this->load->model('promotype/mmaster');
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('promotype/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu88')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ipromotype 	= $this->input->post('ipromotype', TRUE);
			$epromotypename 	= $this->input->post('epromotypename', TRUE);

			if ($ipromotype != '' && $epromotypename != '')
			{
				$this->load->model('promotype/mmaster');
				$this->mmaster->insert($ipromotype,$epromotypename);
				$data['sukses']			= true;
				$data['inomor']			= $ipromotype." - ".$epromotypename;
				$this->load->view('nomor',$data);
			}
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu88')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_promotype');
			$this->load->view('promotype/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu88')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_promotype')." update";
			if($this->uri->segment(4)){
				$ipromotype = $this->uri->segment(4);
				$data['ipromotype'] = $ipromotype;
				$this->load->model('promotype/mmaster');
				$data['isi']=$this->mmaster->baca($ipromotype);
		 		$this->load->view('promotype/vmainform',$data);
			}else{
				$this->load->view('promotype/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu88')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ipromotype		= $this->input->post('ipromotype', TRUE);
			$epromotypename 	= $this->input->post('epromotypename', TRUE);
			$this->load->model('promotype/mmaster');
			$this->mmaster->update($ipromotype,$epromotypename);
			$data['sukses']			= true;
			$data['inomor']			= $ipromotype." - ".$epromotypename;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu88')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ipromotype		= $this->uri->segment(4);
			$this->load->model('promotype/mmaster');
			$this->mmaster->delete($ipromotype);
			$data['page_title'] = $this->lang->line('master_promotype');
			$data['ipromotype']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('promotype/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
