<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu53')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_opstatus');
			$data['iopstatus']='';
			$this->load->model('opstatus/mmaster');
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('opstatus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu53')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iopstatus 	= $this->input->post('iopstatus', TRUE);
			$eopstatusname 	= $this->input->post('eopstatusname', TRUE);

			if ($iopstatus != '' && $eopstatusname != '')
			{
				$this->load->model('opstatus/mmaster');
				$this->mmaster->insert($iopstatus,$eopstatusname);
				$data['sukses']			= true;
				$data['inomor']			= $iopstatus." - ".$eopstatusname;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu53')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_opstatus');
			$this->load->view('opstatus/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu53')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_opstatus')." update";
			$iopstatus = $this->uri->segment(4);
			$data['iopstatus'] = $iopstatus;
			$this->load->model('opstatus/mmaster');
			$data['isi']=$this->mmaster->baca($iopstatus);
	 		$this->load->view('opstatus/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu53')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iopstatus		= $this->input->post('iopstatus', TRUE);
			$eopstatusname 	= $this->input->post('eopstatusname', TRUE);
			$this->load->model('opstatus/mmaster');
			$this->mmaster->update($iopstatus,$eopstatusname);
			$data['sukses']			= true;
			$data['inomor']			= $iopstatus." - ".$eopstatusname;
			$this->load->view('nomor',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu53')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iopstatus		= $this->uri->segment(4);
			$this->load->model('opstatus/mmaster');
			$this->mmaster->delete($iopstatus);
			$data['page_title'] = $this->lang->line('master_opstatus');
			$data['iopstatus']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('opstatus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
