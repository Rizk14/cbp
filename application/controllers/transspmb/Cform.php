<?php
class Cform extends CI_Controller
{
	public $menu 	= "2006001G";
	public $title 	= "Transfer SPMB";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu298') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('transspmb');
			$data['iperiode']	= '';
			$data['iarea']	  = '';

			$this->logger->writenew("Membuka Menu : " . $this->menu . " - " . $this->title);

			$this->load->view('transspmb/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu298') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iperiode	= $this->input->post('iperiode');
			$iarea = $this->input->post('iarea');
			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}
			if ($iarea == '') $iarea = $this->uri->segment(5);
			$data['page_title'] = $this->lang->line('transspmb');
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;

			define('BOOLEAN_FIELD',   'L');
			define('CHARACTER_FIELD', 'C');
			define('DATE_FIELD',      'D');
			define('NUMBER_FIELD',    'N');
			define('READ_ONLY',  '0');
			define('WRITE_ONLY', '1');
			define('READ_WRITE', '2');

			$daerah = $iarea;
			if ($iarea == '20') $daerah = '07';
			if ($iarea == '26') $daerah = '07';
			if ($iarea == '06') $daerah = '23';
			if ($iarea == '24') $daerah = '05';
			if ($iarea == '22') $daerah = '09';
			if ($iarea == '18') $daerah = '11';
			if ($iarea == '19') $daerah = '11';
			if ($iarea == '21') $daerah = '11';
			if ($iarea == '29') $daerah = '11';
			if ($iarea == '30') $daerah = '11';
			if ($iarea == '32') $daerah = '11';
			if ($iarea == '14') $daerah = '12';
			if ($iarea == '15') $daerah = '12';
			if ($iarea == '21') $daerah = '13';
			if ($iarea == '28') $daerah = '13';
			$db_file = 'spb/' . $daerah . '/spmb' . $iarea . '.dbf';
			if (file_exists($db_file)) {
				@chmod($db_file, 0777);
				@unlink($db_file);
			}
			$dbase_definition = array(
				array('NODOK',  CHARACTER_FIELD,  6),
				array('TGLDOK',  DATE_FIELD),
				array('KONS', CHARACTER_FIELD, 2),
				array('KODEPROD', CHARACTER_FIELD, 9),
				array('HARGASAT', NUMBER_FIELD, 7, 0),
				array('JPESAN', NUMBER_FIELD, 7, 0),
				array('STOK', NUMBER_FIELD, 7, 0),
				array('JKIRIM', NUMBER_FIELD, 7, 0),
				array('ACC', NUMBER_FIELD, 7, 0),
				array('SALDO', NUMBER_FIELD, 7, 0),
				array('ITEM_NO', CHARACTER_FIELD, 3)
			);
			$create = @dbase_create($db_file, $dbase_definition)
				or die("Could not create dbf file <i>$db_file</i>.");
			$id = @dbase_open($db_file, READ_WRITE)
				or die("Could not open dbf file <i>$db_file</i>.");
			$per = substr($iperiode, 2, 4);
			$nospmb = 'SPMB-' . $per . '%';
			$sql	  = "select a.i_spmb, a.d_spmb, b.i_product, b.v_unit_price, b.n_order, b.n_deliver, b.n_stock, b.n_acc, b.n_saldo,
                 a.f_spmb_consigment, a.f_spmb_cancel, b.n_item_no
                 from tm_spmb_item b, tm_spmb a
                 where a.i_spmb=b.i_spmb and a.i_area=b.i_area and a.i_area='$iarea' and a.i_spmb like '$nospmb'
                 order by a.i_spmb, b.n_item_no";
			$rspmb		= $this->db->query($sql);

			foreach ($rspmb->result() as $rowpb) {
				if ($rowpb->f_spmb_consigment == 'f')
					$consigment = 'Tidak';
				else
					$consigment = 'Ya';
				$ispmb            = substr($rowpb->i_spmb, 10, 6);
				$dspmb            = substr($rowpb->d_spmb, 0, 4) . substr($rowpb->d_spmb, 5, 2) . substr($rowpb->d_spmb, 8, 2);
				$iproduct         = $rowpb->i_product;
				$vunitprice       = $rowpb->v_unit_price;
				$norder           = $rowpb->n_order;
				$nstock           = $rowpb->n_stock;
				$ndeliver         = $rowpb->n_deliver;
				$nacc             = $rowpb->n_acc;
				$nsaldo           = $rowpb->n_saldo;
				$itemno           = $rowpb->n_item_no;
				$isi = array($ispmb, $dspmb, $consigment, $iproduct, $vunitprice, $norder, $nstock, $ndeliver, $nacc, $nsaldo, $itemno);
				dbase_add_record($id, $isi) or die("Data TIDAK bisa ditambahkan ke file <i>$db_file</i>.");
			}
			dbase_close($id);
			@chmod($db_file, 0777);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Transfer ke SPMB lama Area ' . $iarea . ' Periode:' . $iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$data['sukses']			= true;
			$data['inomor']			= $pesan;
			$this->load->view('nomor', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu298') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/transspmb/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($area1 == '00' or $area2 == '00' or $area3 == '00' or $area4 == '00' or $area5 == '00') {
				$query = $this->db->query("select * from tr_area", false);
			} else {
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('transspmb/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu298') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url() . 'index.php/transspmb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if ($area1 == '00' or $area2 == '00' or $area3 == '00' or $area4 == '00' or $area5 == '00') {
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')", false);
			} else {
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('transspmb/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
