<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		  $config['base_url'] = base_url().'index.php/printdailydo/cform/index/';
			$data['page_title'] = $this->lang->line('printdailydo');
			$data['daily']='';
			$this->load->view('printdailydo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea  = $this->input->post('iarea');
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
      		if($dfrom=='')$dfrom=$this->uri->segment(4);
      		if($dto=='')$dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printdailydo/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" select a.i_stop from tm_stop a, tr_area b
										where a.i_area=b.i_area
										and (upper(a.i_stop) like '%$cari%') and a.i_area='$iarea' and 
										a.d_stop >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_stop <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printdailydo');
			$this->load->model('printdailydo/mmaster');
			$data['cari']=$cari;
			$data['dfrom']=$dfrom;
			$data['dto']=$dto;
			$data['iarea']=$iarea;
			$data['isi']=$this->mmaster->bacasemua($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('printdailydo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$daily	= $this->input->post('daily');
			if($daily=='')$daily=$this->uri->segment(4);
			//$dto	  = $this->input->post('dto');
			//$data['page_title'] = $this->lang->line('printdailydo');
			$this->load->model('printdailydo/mmaster');
			$data['isi']	=$this->mmaster->baca($daily);
			$data['daily']=$daily;

// 			$sess=$this->session->userdata('session_id');
// 			$id=$this->session->userdata('user_id');
// 			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
// 			$rs		=  $this->db->query($sql);
// 			if($rs->num_rows>0){
// 				foreach($rs->result() as $tes){
// 					$ip_address	  = $tes->ip_address;
// 					break;
// 				}
// 			}else{
// 				$ip_address='kosong';
// 			}

// 			$data['user']	= $this->session->userdata('user_id');
// #			$data['host']	= $this->session->userdata('printerhost');
// 			$data['host']	= $ip_address;
// 			$data['uri']	= $this->session->userdata('printeruri');
// 			$query 	= pg_query("SELECT current_timestamp as c");
// 			while($row=pg_fetch_assoc($query)){
// 				$now	  = $row['c'];
// 			}
// 			$pesan='Cetak DAILY DO';
// 			$this->load->model('logger');
// 			$this->logger->write($id, $ip_address, $now , $pesan );
	  	$this->load->view('printdailydo/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printdailydo');
			$this->load->view('printdailydo/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea  = $this->session->userdata('i_area');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printdailydo/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_dkb a, tr_area b
										              where a.i_area=b.i_area
										              and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										              or upper(a.i_dkb) like '%$cari%' or upper(a.i_dkb_old) like '%$cari%')
										              and a.i_area='$iarea' and 
										              a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
										              a.d_dkb <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('printdailydo');
			$this->load->model('printdailydo/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('printdailydo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printdailydo/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('printdailydo/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printdailydo/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printdailydo/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printdailydo/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printdailydo/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
