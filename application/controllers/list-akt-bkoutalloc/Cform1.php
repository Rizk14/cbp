<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listaktbkoutalloc');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('list-akt-bkoutalloc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/list-akt-bkoutalloc/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select * from tm_alokasi_bk
										              where (upper(i_alokasi) like '%$cari%') and f_alokasi_cancel='f' 
										              and i_supplier='$isupplier' and d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
										              d_alokasi <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('listaktbkoutalloc');
			$this->load->model('list-akt-bkoutalloc/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']		= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Bank Supplier '.$isupplier.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('list-akt-bkoutalloc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listaktbkoutalloc');
			$this->load->view('list-akt-bkoutalloc/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ialokasi	= $this->uri->segment(4);
			$ikbank		= $this->uri->segment(5);
			$isupplier	= $this->uri->segment(6);
			$dfrom		= $this->uri->segment(7);
			$dto		= $this->uri->segment(8);
			$iarea='00';
			$this->load->model('list-akt-bkoutalloc/mmaster');
      $this->db->trans_begin();
			$this->mmaster->delete($ialokasi,$ikbank,$iarea,$isupplier);
      if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
	      	$now	  = $row['c'];
			  }
			  $pesan='Hapus Alokasi Bank Masuk No:'.$ialokasi.' Supplier:'.$isupplier;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan ); 
#				$this->db->trans_rollback();
				$this->db->trans_commit();
      }
			$cari		= strtoupper($this->input->post('cari'));
			$config['base_url'] = base_url().'index.php/list-akt-bkoutalloc/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select * from tm_alokasi
										              where (upper(i_alokasi) like '%$cari%') and f_alokasi_cancel='f' 
										              and i_supplier='$isupplier' and d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
										              d_alokasi <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('listaktbkoutalloc');
			$this->load->model('list-akt-bkoutalloc/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']		= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('list-akt-bkoutalloc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/list-akt-bkoutalloc/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select * from tm_kb
										              where (upper(i_kb) like '%$cari%') and f_close='f' 
										              and i_supplier='$isupplier' and f_kb_cancel='f' and
										              d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
										              d_kb <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('listaktbkoutalloc');
			$this->load->model('list-akt-bkoutalloc/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']		= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('list-akt-bkoutalloc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/list-akt-bkoutalloc/cform/supplier/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_supplier order by i_supplier", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('list-akt-bkoutalloc/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('list-akt-bkoutalloc/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
{
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $isupplier    = strtoupper($this->input->post('area', FALSE));
         $cari    = strtoupper($this->input->post('cari', FALSE));
         if($isupplier=='' || $isupplier==null) $isupplier = $this->uri->segment(4);
         if($cari=='' || $cari==null) $cari = $this->uri->segment(5);
         if($cari=='' || $cari==null || $cari=='sikasep'){
           $config['base_url'] = base_url().'index.php/list-akt-bkoutalloc/cform/supplier/sikasep/';
         }else{
           $config['base_url'] = base_url().'index.php/list-akt-bkoutalloc/cform/supplier/'.$cari.'/';
           $query   = $this->db->query(" select i_supplier from tr_supplier
                                        where  (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') ",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('list-akt-bkoutalloc/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->carisupplier($cari,$isupplier,$config['per_page'],$this->uri->segment(6));
         $data['isupplier']=$isupplier;
         $data['cari']=$cari;
         $this->load->view('list-akt-bkoutalloc/vlistsupplier', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
