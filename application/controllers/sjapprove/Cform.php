<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/sjapprove/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
      $iuser  = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from v_list_sj_promo where i_area in ( select i_area from tm_user_area where i_user = '$iuser' ) ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('sjapprove');
			$this->load->model('sjapprove/mmaster');
			$data['ispb']='';
			$data['cari']=$cari;
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4),$iuser);
			$this->load->view('sjapprove/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listspb');
			$this->load->view('sjapprove/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/sjapprove/cform/index/';
			$config['per_page'] = '10';
      $iuser  = $this->session->userdata('user_id');
      $query = $this->db->query("select * from v_list_sj_promo
                                  where i_area in ( select i_area from tm_user_area where i_user = '$iuser' )
                                     and (upper(i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%' or 
                                  upper(i_customer) like '%$cari%' or upper(i_sj) like '%$cari%') ",false);
			$config['total_rows']	= $query->num_rows();	
			$config['first_link']	= 'Awal';
			$config['last_link']	= 'Akhir';
			$config['next_link']	= 'Selanjutnya';
			$config['prev_link']	= 'Sebelumnya';
			$config['cur_page']	= $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('sjapprove/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4),$iuser);
			$data['page_title'] = $this->lang->line('sjpromo');
			$data['ispb']= '';
			$data['cari']= $cari;
	 		$this->load->view('sjapprove/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('sjpromo');
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
			  )
			{
        $cari = $this->input->post('cari', FALSE);
        $cari = strtoupper($cari);
				$ispb = $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$query = $this->db->query("select i_spb from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows();
				$data['ispb'] = $ispb;
				$data['cari'] = $cari;
				$this->load->model('sjapprove/mmaster');
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea);
		 		$this->load->view('sjapprove/vmainform',$data);
			}else{
				$this->load->view('sjapprove/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb 		= $this->input->post('ispb', TRUE);
			$iarea		= $this->input->post('iarea',TRUE);
			$isj 		  = $this->input->post('ispb', TRUE);
      $vspbgross = $this->input->post('vspb',TRUE);
      $vspbgross	= str_replace(',','',$vspbgross);
      $vspbdiscount = $this->input->post('vspbdiscounttotal',TRUE);
      $vspbdiscount	= str_replace(',','',$vspbdiscount);
      $vsjgross = $this->input->post('vsjgross',TRUE);
      $vsjgross	= str_replace(',','',$vsjgross);
      $vsjdiscount = $this->input->post('vspbdiscounttotalafter',TRUE);
      $vsjdiscount	= str_replace(',','',$vsjdiscount);
			$user		  = $this->session->userdata('user_id');
			$this->db->trans_begin();
			$this->load->model('sjapprove/mmaster');
			$this->mmaster->simpan($ispb, $iarea, $user, $vsjgross, $vspbgross, $vsjdiscount, $vspbdiscount);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Ubah Discount Rupiah SJ Area '.$iarea.' No:'.$ispb;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );
				$data['sukses']			= true;
				$data['inomor']			= $ispb;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function sjapprove()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb 		= $this->input->post('nospb', TRUE);
			$iarea		= $this->input->post('kdarea',TRUE);
			$eapprove	= $this->input->post('ketapprove',TRUE);
			if($eapprove=='')
				$eapprove=null;
			$user		=$this->session->userdata('user_id');
			$this->db->trans_begin();
			$this->load->model('sjapprove/mmaster');
			$this->mmaster->sjapprove($ispb, $iarea, $eapprove,$user);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Approve SJ Promo Area '.$iarea.' No:'.$ispb;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );
				$data['sukses']			= true;
				$data['inomor']			= $ispb;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
