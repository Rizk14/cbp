<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printspmb');
			$data['spmbfrom']='';
			$data['spmbto']	='';
			$this->load->view('printspmbkelompok/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function spmbfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/printspmbkelompok/cform/spmbfrom/index/';
			$query = $this->db->query("	select i_spmb from tm_spmb
										where upper(i_spmb) like '%$cari%'
										and (i_area='$area1' or i_area='$area2' or i_area='$area3'
											 or i_area='$area4' or i_area='$area5')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printspmbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_spmb');
			$data['isi']=$this->mmaster->bacaspmb($cari,$area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printspmbkelompok/vlistspmbfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carispmbfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/printspmbkelompok/cform/spmbfrom/index/';
			$query = $this->db->query("	select i_spmb from tm_spmb
										where upper(i_spmb) like '%$cari%'
										and (i_area='$area1' or i_area='$area2' or i_area='$area3'
										or i_area='$area4' or i_area='$area5')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printspmbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_spmb');
			$data['isi']=$this->mmaster->bacaspmb($cari,$area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printspmbkelompok/vlistspmbfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function spmbto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/printspmbkelompok/cform/spmbto/index/';
			$query = $this->db->query("	select i_spmb from tm_spmb
										where upper(i_spmb) like '%$cari%'
										and (i_area='$area1' or i_area='$area2' or i_area='$area3'
											 or i_area='$area4' or i_area='$area5')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printspmbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_spmb');
			$data['isi']=$this->mmaster->bacaspmb($cari,$area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printspmbkelompok/vlistspmbto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carispmbto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/printspmbkelompok/cform/spmbto/index/';
			$query = $this->db->query("	select i_spmb from tm_spmb
										where upper(i_spmb) like '%$cari%'
										and (i_area='$area1' or i_area='$area2' or i_area='$area3'
										or i_area='$area4' or i_area='$area5')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printspmbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_spmb');
			$data['isi']=$this->mmaster->bacaspmb($cari,$area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printspmbkelompok/vlistspmbto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	= $this->input->post('areafrom');
			$spmbfrom= $this->input->post('spmbfrom');
			$spmbto	= $this->input->post('spmbto');
			$this->load->model('printspmbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['master']=$this->mmaster->bacamaster($area,$spmbfrom,$spmbto);
			$data['area']=$area;
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak SPMB Area:'.$area.' No:'.$spmbfrom.' s/d No:'.$spmbto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printspmbkelompok/vformrpt',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
