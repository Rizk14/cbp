<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb    = $this->input->post('ispb', TRUE);
         $dspb    = $this->input->post('dspb', TRUE);
         if($dspb!=''){
            $tmp=explode("-",$dspb);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dspb=$th."-".$bl."-".$hr;
#           $thbl=substr($th,2,2).$bl;
            $thbl=$th.$bl;
         }
         $icustomer     = $this->input->post('icustomer', TRUE);
### blacklist
#         $query=$this->db->query("select i_customer_status from tr_customer where i_customer='$icustomer'", false);
#         if ($query->num_rows() > 0){
#           $row=$query->row();
#           if($row->i_customer_status=='4'){
#             $fspbstockdaerah= 'f';
#           }else{
             $fspbstockdaerah  = $this->input->post('fspbstockdaerah',TRUE);
             if($fspbstockdaerah!=''){
                $fspbstockdaerah= 't';
             }else{
                $fspbstockdaerah= 'f';
             }
#           }
#         }
###
         $ecustomername = $this->input->post('ecustomername', TRUE);
         $ecumstomeraddress   = $this->input->post('ecumstomeraddress', TRUE);
         $eremarkx      = $this->input->post('eremarkx', TRUE);
         $iarea         = $this->input->post('iarea', TRUE);
         $eareaname     = $this->input->post('eareaname', TRUE);
         $ispbpo        = $this->input->post('ispbpo', TRUE);
         if($ispbpo=='') $ispbpo=null;
         $nspbtoplength = $this->input->post('nspbtoplength', TRUE);
         $isalesman     = $this->input->post('isalesman',TRUE);
         $esalesmanname = $this->input->post('esalesmanname',TRUE);
         $ipricegroup   = $this->input->post('ipricegroup',TRUE);
         $inota         = $this->input->post('inota',TRUE);
         $dspbreceive   = $this->input->post('dspbreceive',TRUE);
#        if($ispbpo!='')
#           $fspbop        = 't';#$this->input->post('fspbop',TRUE);
#        else
            $fspbop        = 'f';#$this->input->post('fspbop',TRUE);
         $ecustomerpkpnpwp = $this->input->post('ecustomerpkpnpwp',TRUE);
         if($ecustomerpkpnpwp!=''){
            $fspbpkp    = 't';#$this->input->post('fspbpkp',TRUE);
         }else{
            $fspbpkp    = 'f';#$this->input->post('fspbpkp',TRUE);
            $ecustomerpkpnpwp=null;
         }
         $fspbconsigment      = $this->input->post('fspbconsigment',TRUE);
         if($fspbconsigment!='')
            $fspbconsigment="t";
         else
            $fspbconsigment="f";
         $fspbplusppn      = $this->input->post('fspbplusppn',TRUE);
         $fspbplusdiscount = $this->input->post('fspbplusdiscount',TRUE);
         $fspbvalid     = 'f';
         $fspbprogram   = 'f';#$this->input->post('fspbprogram',TRUE);
         $fspbsiapnotagudang  = 'f';#$this->input->post('fspbsiapnota',TRUE);
         $fspbcancel    = 'f';#$this->input->post('fspbcancel',TRUE);
         $nspbtoplength = $this->input->post('nspbtoplength',TRUE);
         $nspbdiscount1 = $this->input->post('ncustomerdiscount1',TRUE);
         $nspbdiscount2 = $this->input->post('ncustomerdiscount2',TRUE);
         $nspbdiscount3 = $this->input->post('ncustomerdiscount3',TRUE);
         $vspbdiscount1 = $this->input->post('vcustomerdiscount1',TRUE);
         $vspbdiscount2 = $this->input->post('vcustomerdiscount2',TRUE);
         $vspbdiscount3 = $this->input->post('vcustomerdiscount3',TRUE);
         $vspbdiscounttotal   = $this->input->post('vspbdiscounttotal',TRUE);
         $vspb       = $this->input->post('vspb',TRUE);
         $nspbdiscount1 = str_replace(',','',$nspbdiscount1);
         $nspbdiscount2 = str_replace(',','',$nspbdiscount2);
         $nspbdiscount3 = str_replace(',','',$nspbdiscount3);
         $vspbdiscount1 = str_replace(',','',$vspbdiscount1);
         $vspbdiscount2 = str_replace(',','',$vspbdiscount2);
         $vspbdiscount3 = str_replace(',','',$vspbdiscount3);
         $vspbdiscounttotal   = str_replace(',','',$vspbdiscounttotal);
         $vspb       = str_replace(',','',$vspb);
         $ispbold = $this->input->post('ispbold',TRUE);
         $jml        = $this->input->post('jml', TRUE);
         if( ($ecustomername!='') && ($dspb!='') && ($iarea!='') && ($jml>0) )
         {
            $benar="false";
            $this->db->trans_begin();
            $this->load->model('spbreguler/mmaster');
          			for($i=1;$i<=$jml;$i++){
                  $iproduct  = $this->input->post('motif'.$i, TRUE);
                  $eproductname      = $this->input->post('eproductname'.$i, TRUE);
                  $norder              = $this->input->post('norder'.$i, TRUE);
				            if(($iproduct=='') ||
					            ($eproductname=='') ||
					            ($norder=='')){
					            $this->db->trans_rollback();
					            die;
				            }
  			        }
            $ispb =$this->mmaster->runningnumber($iarea,$thbl);
            $this->mmaster->insertheader(  $ispb, $dspb, $icustomer, $iarea, $ispbpo, $nspbtoplength, $isalesman,
                         $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp,
                         $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
                         $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1,
                         $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
                         $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold, $eremarkx);
            for($i=1;$i<=$jml;$i++){
              $iproduct          = $this->input->post('iproduct'.$i, TRUE);
              $iproductstatus  = $this->input->post('iproductstatus'.$i, TRUE);
              $iproductgrade        = 'A';
              $iproductmotif        = $this->input->post('motif'.$i, TRUE);
              $eproductname            = $this->input->post('eproductname'.$i, TRUE);
              $vunitprice           = $this->input->post('vproductretail'.$i, TRUE);
              $vunitprice           = str_replace(',','',$vunitprice);
              $norder            = $this->input->post('norder'.$i, TRUE);
              $eremark           = $this->input->post('eremark'.$i, TRUE);
              $data['iproduct']        = $iproduct;
              $data['iproductgrade']      = $iproductgrade;
              $data['iproductmotif']      = $iproductmotif;
              $data['eproductname']       = $eproductname;
              $data['vunitprice']         = $vunitprice;
              $data['norder']       = $norder;
              if($norder>0){
                $this->mmaster->insertdetail( $ispb,$iarea,$iproduct,$iproductstatus,$iproductgrade,$eproductname,$norder,null,
                          $vunitprice,$iproductmotif,$eremark,$i);
              }
            }
            if ( ($this->db->trans_status() === FALSE) )
            {
             $this->db->trans_rollback();
            }else{
             $this->db->trans_commit();
               $sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
                  $now    = $row['c'];
               }
               $pesan='Input SPB Reguler Area '.$iarea.' No:'.$ispb;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );
               $data['sukses']         = true;
               $data['inomor']         = $ispb;
               $this->load->view('nomor',$data);
            }
      }elseif( ($ecustomername!='')  && ($dspb!='') && ($iarea!='')){
         }else{
            $iarea = '';
            $data['page_title'] = $this->lang->line('spbreguler');
            $data['ispb']='';
            $this->load->model('spbreguler/mmaster');
            $data['isi']=$this->mmaster->bacasemua($iarea);
            $data['detail']="";
            $data['jmlitem']="";
        $data['tgl']=date('d-m-Y');
            $this->load->view('spbreguler/vmainform', $data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('spbreguler');
         $this->load->view('spbreguler/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function edit()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         (($this->session->userdata('menu159')=='t') || ($this->session->userdata('menu57')=='t'))) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('spbreguler')." update";
         if(
            ($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
           )
         {
            $ispb = $this->uri->segment(4);
            $iarea= $this->uri->segment(5);

            // desta 21-12-2010
            $dfrom= $this->uri->segment(6);
            $dto= $this->uri->segment(7);
            $iarea_awal= $this->uri->segment(8);
            $ipricegroup= $this->uri->segment(9);

            $query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
            $data['jmlitem'] = $query->num_rows();
            $data['ispb'] = $ispb;
            $data['departement']=$this->session->userdata('departement');
            $this->load->model('spbreguler/mmaster');
            $data['isi']=$this->mmaster->baca($ispb,$iarea);
            $data['detail']=$this->mmaster->bacadetail($ispb,$iarea,$ipricegroup);

            $qnilaispb  = $this->mmaster->bacadetailnilaispb($ispb,$iarea,$ipricegroup);
            if($qnilaispb->num_rows()>0){
               $row_nilaispb  = $qnilaispb->row();
               $data['nilaispb'] = $row_nilaispb->nilaispb;
            }else{
               $data['nilaispb'] = 0;
            }
            $qnilaiorderspb   = $this->mmaster->bacadetailnilaiorderspb($ispb,$iarea,$ipricegroup);
            if($qnilaiorderspb->num_rows()>0){
               $row_nilaiorderspb   = $qnilaiorderspb->row();
               $data['nilaiorderspb']  = $row_nilaiorderspb->nilaiorderspb;
            }else{
               $data['nilaiorderspb']  = 0;
            }

            $data['dfrom'] = $dfrom;
            $data['dto'] = $dto;
            $data['iarea_awal'] = $iarea_awal;

            $this->load->view('spbreguler/vmainform',$data);
         }else{
            $this->load->view('spbreguler/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function update()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb    = $this->input->post('ispb', TRUE);
         $dspb    = $this->input->post('dspb', TRUE);
         if($dspb!=''){
            $tmp=explode("-",$dspb);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dspb=$th."-".$bl."-".$hr;
         }
         $icustomer     = $this->input->post('icustomer', TRUE);
### blacklist
#         $query=$this->db->query("select i_customer_status from tr_customer where i_customer='$icustomer'", false);
#         if ($query->num_rows() > 0){
#           $row=$query->row();
#           if($row->i_customer_status=='4'){
#             $fspbstockdaerah= 'f';
#           }else{
             $fspbstockdaerah  = $this->input->post('fspbstockdaerah',TRUE);
             if($fspbstockdaerah!=''){
                $fspbstockdaerah= 't';
             }else{
                $fspbstockdaerah= 'f';
             }
#           }
#         }
###
         $ecustomername = $this->input->post('ecustomername', TRUE);
         $ecumstomeraddress   = $this->input->post('ecumstomeraddress', TRUE);
         $eremarkx      = $this->input->post('eremarkx', TRUE);
         $iarea         = $this->input->post('iarea', TRUE);
         $eareaname     = $this->input->post('eareaname', TRUE);
         $ispbpo        = $this->input->post('ispbpo', TRUE);
         $nspbtoplength = $this->input->post('nspbtoplength', TRUE);
         $isalesman     = $this->input->post('isalesman',TRUE);
         $esalesmanname = $this->input->post('esalesmanname',TRUE);
         $ipricegroup   = $this->input->post('ipricegroup',TRUE);
         $inota         = $this->input->post('inota',TRUE);
      $isj               = $this->input->post('isj',TRUE);
      $dsj  = $this->input->post('dsj', TRUE);
         if($dsj!=''){
            $tmp=explode("-",$dsj);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dsj=$th."-".$bl."-".$hr;
         }
         $istore            = $this->input->post('istore',TRUE);
         $istorelocation   = $this->input->post('istorelocation',TRUE);
         $istorelocationbin= '00';
         $dspbreceive   = $this->input->post('dspbreceive',TRUE);
         $fspb_op = $this->input->post('f_spb_op',TRUE);
         if($fspb_op!=''){
            $fspbop     = 't';
         }else{
            $fspbop     = 'f';
         }
         $ecustomerpkpnpwp = $this->input->post('ecustomerpkpnpwp',TRUE);
         if($ecustomerpkpnpwp!=''){
            $fspbpkp    = 't';
         }else{
            $fspbpkp    = 'f';
         }
         $fspbconsigment      = $this->input->post('fspbconsigment',TRUE);
         if($fspbconsigment!=''){
            $fspbconsigment="t";
         }else{
            $fspbconsigment="f";
         }
         $fspbplusppn      = $this->input->post('fspbplusppn',TRUE);
         $fspbplusdiscount = $this->input->post('fspbplusdiscount',TRUE);
         $fspbvalid     = 'f';
         $fspbprogramx  = $this->input->post('f_spb_program',TRUE);
         if($fspbprogramx!=''){
            $fspbprogram   = 't';
         }else{
            $fspbprogram   = 'f';
         }
         $fspbprogram   = 'f';
         $fspbsiapnotagudang  = $this->input->post('fspbsiapnotagudang',TRUE);
         if($fspbsiapnotagudang!=''){
            $fspbsiapnota  = 't';
         }else{
            $fspbsiapnota  = 'f';
         }
         $fspbcancel    = 'f';
         $nspbtoplength = $this->input->post('nspbtoplength',TRUE);
         $nspbdiscount1 = $this->input->post('ncustomerdiscount1',TRUE);
         $nspbdiscount2 = $this->input->post('ncustomerdiscount2',TRUE);
         $nspbdiscount3 = $this->input->post('ncustomerdiscount3',TRUE);
         $vspbdiscount1 = $this->input->post('vcustomerdiscount1',TRUE);
         $vspbdiscount2 = $this->input->post('vcustomerdiscount2',TRUE);
         $vspbdiscount3 = $this->input->post('vcustomerdiscount3',TRUE);
      $vspbdiscount1x   = $this->input->post('vcustomerdiscount1x',TRUE);
         $vspbdiscount2x   = $this->input->post('vcustomerdiscount2x',TRUE);
         $vspbdiscount3x   = $this->input->post('vcustomerdiscount3x',TRUE);
         $vspbdiscounttotal   = $this->input->post('vspbdiscounttotal',TRUE);
      $vspbdiscounttotalafter = $this->input->post('vspbdiscounttotalafter',TRUE);
         $vspb             = $this->input->post('vspb',TRUE);
      $vspbx            = $this->input->post('vspbx',TRUE);
      $vspbafter      = $this->input->post('vspbafter',TRUE);
         $nspbdiscount1 = str_replace(',','',$nspbdiscount1);
         $nspbdiscount2 = str_replace(',','',$nspbdiscount2);
         $nspbdiscount3 = str_replace(',','',$nspbdiscount3);
         $vspbdiscount1 = str_replace(',','',$vspbdiscount1);
         $vspbdiscount2 = str_replace(',','',$vspbdiscount2);
         $vspbdiscount3 = str_replace(',','',$vspbdiscount3);
      $vspbdiscount1x   = str_replace(',','',$vspbdiscount1x);
         $vspbdiscount2x   = str_replace(',','',$vspbdiscount2x);
         $vspbdiscount3x   = str_replace(',','',$vspbdiscount3x);
         $vspbdiscounttotal   = str_replace(',','',$vspbdiscounttotal);
      $vspbdiscounttotalafter = str_replace(',','',$vspbdiscounttotalafter);
         $vspb       = str_replace(',','',$vspb);
      $vspbx      = str_replace(',','',$vspbx);
      $vspbafter= str_replace(',','',$vspbafter);
         $ispbold = $this->input->post('ispbold', TRUE);
         $jml        = $this->input->post('jml', TRUE);
         if(($ecustomername!='') && ($ispb!=''))
         {
            $benar="false";
            $this->db->trans_begin();
            $this->load->model('spbreguler/mmaster');
            $this->mmaster->updateheader($ispb, $iarea, $dspb, $icustomer, $ispbpo, $nspbtoplength, $isalesman,
                   $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp,
                   $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
                   $fspbsiapnota, $fspbcancel, $nspbdiscount1,
                   $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
                   $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment,$ispbold,$eremarkx);
/*
        if($isj!=''){
          $this->mmaster->updatesjheader($ispb,$dspb,$isj,$dsj,$iarea,$isalesman,$icustomer,
                        $nspbdiscount1,$nspbdiscount2,$nspbdiscount3,$vspbdiscount1x,
                        $vspbdiscount2x,$vspbdiscount3x,$vspbdiscounttotalafter,$vspbx,$vspbafter,'');
        }
*/

            for($i=1;$i<=$jml;$i++){
              $iproduct          =$this->input->post('iproduct'.$i, TRUE);
              $iproductstatus  = $this->input->post('iproductstatus'.$i, TRUE);
              $iproductgrade        ='A';
              $iproductmotif        =$this->input->post('motif'.$i, TRUE);
              $eproductname            =$this->input->post('eproductname'.$i, TRUE);
              $vunitprice           =$this->input->post('vproductretail'.$i, TRUE);
              $vunitprice           =str_replace(',','',$vunitprice);
              $norder            =$this->input->post('norder'.$i, TRUE);
              $ndeliver          =$this->input->post('ndeliver'.$i, TRUE);
              $ndeliverx    = $this->input->post('ndeliverx'.$i, TRUE);
              if($ndeliver=='')$ndeliver=null;
              if($ndeliverx=='')$ndeliverx=null;
              $eremark        = $this->input->post('eremark'.$i, TRUE);
              $data['iproduct']     = $iproduct;
              $data['iproductgrade']   = $iproductgrade;
              $data['iproductmotif']   = $iproductmotif;
              $data['eproductname']    = $eproductname;
              $data['vunitprice']      = $vunitprice;
              $data['norder']    = $norder;
              $data['ndeliver']     =$ndeliver;
              $this->mmaster->deletedetail($ispb, $iarea, $iproduct, $iproductgrade, $iproductmotif);
/*
          if($isj!=''){
            $this->mmaster->deletesjdetail($iproduct,$iproductgrade,$iproductmotif,$isj,$iarea);
            $th=substr($dsj,0,4);
                  $bl=substr($dsj,5,2);
                  $emutasiperiode=$th.$bl;
                  $tra=$this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj);
                 $this->mmaster->updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliverx,$emutasiperiode);
                 $this->mmaster->updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ntmp);
          }
*/
              if($norder>0){
                $this->mmaster->insertdetail( $ispb,$iarea,$iproduct,$iproductstatus,$iproductgrade,$eproductname,$norder,$ndeliver,
                                    $vunitprice,$iproductmotif,$eremark,$i);
/*
            if($isj!=''){

                   $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                  if(isset($trans)){
                     foreach($trans as $itrans)
                     {
                       $q_aw =$itrans->n_quantity_stock;
                       $q_ak =$itrans->n_quantity_stock;
                       $q_in =0;
                       $q_out=0;
                       break;
                     }
                   }else{
                      $q_aw=0;
                      $q_ak=0;
                      $q_in=0;
                      $q_out=0;
                   }
                    $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$ndeliver,$q_aw,$q_ak,$tra);
                    if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
                    {
                      $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
                    }else{
                      $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode,$q_aw);
                    }
                    if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
                    {
                      $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$q_ak);
                    }else{
                      $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ndeliver,$q_aw);
                    }
            }
*/
              }
            }
            if ( ($this->db->trans_status() === FALSE) )
            {
             $this->db->trans_rollback();
            }else{
             $this->db->trans_commit();

               $sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
                  $now    = $row['c'];
               }
               $pesan='Update SPB Reguler Area '.$iarea.' No:'.$ispb;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );

               $data['sukses']         = true;
               $data['inomor']         = $ispb;
               $this->load->view('nomor',$data);
            }
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function delete()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb = $this->input->post('ispbdelete');
         $iarea   = $this->input->post('iareadelete');
         $this->load->model('spbreguler/mmaster');
         $this->mmaster->delete($ispb,$iarea);

         $sess=$this->session->userdata('session_id');
         $id=$this->session->userdata('user_id');
         $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
         $rs      = pg_query($sql);
         if(pg_num_rows($rs)>0){
            while($row=pg_fetch_assoc($rs)){
               $ip_address   = $row['ip_address'];
               break;
            }
         }else{
            $ip_address='kosong';
         }
         $query   = pg_query("SELECT current_timestamp as c");
         while($row=pg_fetch_assoc($query)){
            $now    = $row['c'];
         }
         $pesan='Delete SPB Reguler Area '.$iarea.' No:'.$ispb;
         $this->load->model('logger');
         $this->logger->write($id, $ip_address, $now , $pesan );

         $data['page_title'] = $this->lang->line('master_spb');
         $data['ispb']='';
         $data['jmlitem']='';
         $data['detail']='';
         $data['isi']=$this->mmaster->bacasemua($iarea);
         $this->load->view('spbreguler/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function deletedetail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb          = $this->uri->segment(4);
         $iarea            = $this->uri->segment(8);
         $iproduct         = $this->uri->segment(9);
         $iproductgrade    = $this->uri->segment(10);
         $iproductmotif    = $this->uri->segment(11);
         $vspbdiscount1    = $this->uri->segment(12);
         $vspbdiscount2    = $this->uri->segment(13);
         $vspbdiscount3    = $this->uri->segment(14);
         $vspbdiscounttotal   = $this->uri->segment(15);
         $vspb          = $this->uri->segment(16);
         //desta 24-12-2010
         $departement            = $this->uri->segment(17);

         $vspbdiscount1 = str_replace(',','',$vspbdiscount1);
         $vspbdiscount2 = str_replace(',','',$vspbdiscount2);
         $vspbdiscount3 = str_replace(',','',$vspbdiscount3);
         $vspbdiscounttotal   = str_replace(',','',$vspbdiscounttotal);
         $vspb       = str_replace(',','',$vspb);
         $this->db->trans_begin();
         $this->load->model('spbreguler/mmaster');
         $this->mmaster->uphead($ispb, $iarea, $vspbdiscount1, $vspbdiscount2,
        $vspbdiscount3, $vspbdiscounttotal, $vspb);
         $this->mmaster->deletedetail($ispb, $iarea, $iproduct, $iproductgrade, $iproductmotif);
         if ($this->db->trans_status() === FALSE)
         {
          $this->db->trans_rollback();
         }else{
          $this->db->trans_commit();

            $sess=$this->session->userdata('session_id');
            $id=$this->session->userdata('user_id');
            $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs      = pg_query($sql);
            if(pg_num_rows($rs)>0){
               while($row=pg_fetch_assoc($rs)){
                  $ip_address   = $row['ip_address'];
                  break;
               }
            }else{
               $ip_address='kosong';
            }
            $query   = pg_query("SELECT current_timestamp as c");
            while($row=pg_fetch_assoc($query)){
               $now    = $row['c'];
            }
            $pesan='Delete Item SPB Reguler Area '.$iarea.' No:'.$ispb;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now , $pesan );

            $data['page_title'] = $this->lang->line('spbreguler')." Update";
            $query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
            $data['jmlitem'] = $query->num_rows();
            $data['ispb'] = $ispb;
            $data['departement']=$this->session->userdata('departement');

            $ipricegroup   = $this->uri->segment(5);
         $dfrom      = $this->uri->segment(6);
         $dto     = $this->uri->segment(7);
            $data['isi']=$this->mmaster->baca($ispb, $iarea);
            $data['detail']=$this->mmaster->bacadetail($ispb, $iarea, $ipricegroup);

            $qnilaispb  = $this->mmaster->bacadetailnilaispb($ispb,$iarea,$ipricegroup);
            if($qnilaispb->num_rows()>0){
               $row_nilaispb  = $qnilaispb->row();
               $data['nilaispb'] = $row_nilaispb->nilaispb;
            }else{
               $data['nilaispb'] = 0;
            }
            $qnilaiorderspb   = $this->mmaster->bacadetailnilaiorderspb($ispb,$iarea,$ipricegroup);
            if($qnilaiorderspb->num_rows()>0){
               $row_nilaiorderspb   = $qnilaiorderspb->row();
               $data['nilaiorderspb']  = $row_nilaiorderspb->nilaiorderspb;
            }else{
               $data['nilaiorderspb']  = 0;
            }

            $dfrom= $this->uri->segment(6);
            $dto= $this->uri->segment(7);
            $iarea_awal= $iarea;
            $data['dfrom'] = $dfrom;
            $data['dto'] = $dto;
            $data['iarea_awal'] = $iarea_awal;
            $this->load->view('spbreguler/vmainform', $data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function product()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $baris=$this->uri->segment(4);
         $kdharga=$this->uri->segment(5);
         $cari =$this->uri->segment(6);
         $config['base_url'] = base_url().'index.php/spbreguler/cform/product/'.$baris.'/'.$kdharga.'/'.$cari.'/';
         $query = $this->db->query(" select a.i_product||a.i_product_motif as kode,
                                    c.e_product_name as nama,b.v_product_retail as harga
                                    from tr_product_motif a,tr_product_price b,tr_product c, tr_product_type d
                                    where
                                    d.i_product_type=c.i_product_type and d.i_product_group='00'
                                    and b.i_product=a.i_product and a.i_product_motif='00'
                                    and a.i_product=c.i_product
                                    and b.i_price_group='$kdharga'
                                    and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') ",false);

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);

         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
         $data['isi']=$this->mmaster->bacaproduct($cari,$config['per_page'],$this->uri->segment(7),$kdharga);
         $data['baris']=$baris;
         $data['kdharga']=$kdharga;
         $data['cari']=$cari;
         $this->load->view('spbreguler/vlistproduct', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cariproduct()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['baris']=$this->uri->segment(4);
         $baris=$this->uri->segment(4);
         $kdharga=$this->uri->segment(5);
         $cari    = $this->input->post('cari');
         if($cari!=FALSE) $cari  = strtoupper($cari);
         if($cari==FALSE) $cari =$this->uri->segment(6);
         $config['base_url'] = base_url().'index.php/spbreguler/cform/product/'.$baris.'/'.$kdharga.'/'.$cari.'/';
         $query   = $this->db->query(" select a.i_product||a.i_product_motif as kode,
                                           c.e_product_name as nama,b.v_product_retail as harga
                                           from tr_product_motif a,tr_product_price b,tr_product c, tr_product_type d
                                           where b.i_product=a.i_product and a.i_product_motif='00'
                                           and d.i_product_type=c.i_product_type and d.i_product_group='00'
                                           and a.i_product=c.i_product
                                             and b.i_price_group='$kdharga'
                                           and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);
         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
         $data['isi']=$this->mmaster->cariproduct($cari,$kdharga,$config['per_page'],$this->uri->segment(7));
         $data['baris']=$baris;
         $data['kdharga']=$kdharga;
         $data['cari']=$cari;
         $this->load->view('spbreguler/vlistproduct', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function productupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['baris']=$this->uri->segment(4);
         $baris=$this->uri->segment(4);
         $data['kdharga']=$this->uri->segment(5);
         $kdharga=$this->uri->segment(5);
         $cari =$this->uri->segment(6);
         $data['cari']=$this->uri->segment(6);
         $config['base_url'] = base_url().'index.php/spbreguler/cform/productupdate/'.$baris.'/'.$kdharga.'/'.$cari.'/';
         $query = $this->db->query(" select a.i_product||a.i_product_motif as kode,
                              c.e_product_name as nama,b.v_product_retail as harga
                              from tr_product_motif a,tr_product_price b,tr_product c, tr_product_type d
                              where b.i_product=a.i_product and a.i_product_motif='00'
                              and d.i_product_type=c.i_product_type and d.i_product_group='00'
                              and a.i_product=c.i_product
                                 and b.i_price_group='$kdharga'"
                             ,false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);

         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
         $data['isi']=$this->mmaster->bacaproduct($cari,$config['per_page'],$this->uri->segment(7),$kdharga);
         $this->load->view('spbreguler/vlistproductupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cariproductupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $baris=$this->uri->segment(4);
         $kdharga=$this->uri->segment(5);
         $data['kdharga']=$this->uri->segment(5);
         $cari    = $this->input->post('cari');
         if($cari!=FALSE) $cari  = strtoupper($cari);
         if($cari==FALSE) $cari =$this->uri->segment(6);
         $config['base_url'] = base_url().'index.php/spbreguler/cform/productupdate/'.$baris.'/'.$kdharga.'/'.$cari.'/';
         $cari = strtoupper($cari);
         $query   = $this->db->query("select a.i_product||a.i_product_motif as kode,
                              c.e_product_name as nama,b.v_product_retail as harga
                              from tr_product_motif a,tr_product_price b,tr_product c, tr_product_type d
                              where substr(b.i_product,1,7)=a.i_product
                              and d.i_product_type=c.i_product_type and d.i_product_group='00'
                              and a.i_product=c.i_product and a.i_product_motif='00'
                           and b.i_price_group='$kdharga'
                              and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')"
                              ,false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);
         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
         $data['isi']=$this->mmaster->cariproduct($cari,$kdharga,$config['per_page'],$this->uri->segment(7));
         $data['baris']=$baris;
         $data['cari']=$cari;
         $this->load->view('spbreguler/vlistproductupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function productgroup()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('listtypespb');
         $data['isi']=$this->mmaster->productgroup();
         $this->load->view('spbreguler/vlistproductgroup', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/spbreguler/cform/area/index/';

         $allarea= $this->session->userdata('allarea');
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         /* Disabled 13042011 */
         /*
         $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                              or i_area = '$area4' or i_area = '$area5'",false);
         */

         if($allarea=='t'){
            $query = $this->db->query(" select * from tr_area order by i_area", false);
         }elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
            $query = $this->db->query(" select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false);
         }else{
            $query = $this->db->query(" select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false);
         }

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$area1,$area2,$area3,$area4,$area5);
         $this->load->view('spbreguler/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $allarea= $this->session->userdata('allarea');
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');

         $config['base_url'] = base_url().'index.php/spbreguler/cform/area/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);

         /* Disabled 13042011 */
         /*
         $query   = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
               and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
               or i_area = '$area4' or i_area = '$area5')",false);
         */

         if($allarea=='t'){
            $query = $this->db->query(" select * from tr_area order by i_area", false);
         }elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
            $query = $this->db->query(" select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false);
         }else{
            $query = $this->db->query(" select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false);
         }

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$area1,$area2,$area3,$area4,$area5);
         $this->load->view('spbreguler/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function store()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $area=$this->session->userdata('i_area');
         $config['base_url'] = base_url().'index.php/spbreguler/cform/store/index/';
         $query = $this->db->query("select * from tr_area where i_area = '$area' or i_area='00'",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('list_store');
         $data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5),$area);
         $this->load->view('spbreguler/vliststore', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function caristore()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/spbreguler/cform/store/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $query   = $this->db->query("select * from tr_store
                     where upper(i_store) like '%$cari%' or upper(e_store_name) like '%$cari%'",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('list_store');
         $data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5));
         $this->load->view('spbreguler/vliststore', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function customer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
          $cari  = strtoupper($this->input->post('cari', FALSE));
          $iarea = strtoupper($this->input->post('iarea', FALSE));
          $dspb = strtoupper($this->input->post('dspb', FALSE));
          if($iarea=='') $iarea=$this->uri->segment(4);
          if($dspb=='') $dspb=$this->uri->segment(5);
          $per='';
          if($dspb!='x01' && $dspb!=''){
            $tmp=explode('-',$dspb);
            $yy=$tmp[2];
            $bl=$tmp[1];
            $per=$yy.$bl;
          }
          if($this->uri->segment(6)!='x01'){
            if($cari=='') $cari=$this->uri->segment(6);
            $config['base_url'] = base_url().'index.php/spbreguler/cform/customer/'.$iarea.'/'.$dspb.'/'.$cari.'/';
          }else{
            $config['base_url'] = base_url().'index.php/spbreguler/cform/customer/'.$iarea.'/'.$dspb.'/x01/';
          }
         $config['per_page'] = '10';
         $query = $this->db->query("   select a.i_customer from tr_customer a
                                       left join tr_customer_pkp b on
                                       (a.i_customer=b.i_customer)
                                       left join tr_price_group c on
                                       (a.i_price_group=c.n_line or a.i_price_group=c.i_price_group)
                                       left join tr_customer_area d on
                                       (a.i_customer=d.i_customer)
                                       left join tr_customer_salesman e on
                                       (a.i_customer=e.i_customer and e.i_product_group='00' and e.e_periode='$per')
                                       left join tr_customer_discount f on
                                       (a.i_customer=f.i_customer) where a.i_area='$iarea' and a.f_approve='t'
                                       and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);

         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->bacacustomer($cari,$iarea,$config['per_page'],$this->uri->segment(7),$per);
         $data['iarea']=$iarea;
         $data['dspb']=$dspb;
         $this->load->view('spbreguler/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
/*
   function caricustomer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea = $this->uri->segment(4);
         $config['base_url'] = base_url().'index.php/spbreguler/cform/customer/'.$iarea.'/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $query   = $this->db->query("select * from tr_customer a
                              left join tr_customer_pkp b on
                              (a.i_customer=b.i_customer)
                              left join tr_price_group c on
                              (a.i_price_group=c.n_line)
                              left join tr_customer_area d on
                              (a.i_customer=d.i_customer)
                              left join tr_customer_salesman e on
                              (a.i_customer=e.i_customer and e.i_product_group='00')
                              left join tr_customer_discount f on
                              (a.i_customer=f.i_customer) where a.i_area='$iarea' and a.f_approve='t' and a.f_approve2='t' and a.f_approve3='t' and a.f_approve4='t'
                              and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
         $data['iarea']=$iarea;
         $this->load->view('spbreguler/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
*/
   function cari()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $iarea   = $this->input->post('iarea', FALSE);
         $config['base_url'] = base_url().'index.php/spbreguler/cform/index/';
         $query   = $this->db->query("select * from tm_spb
                              where i_area='$iarea' and i_product_group='00'
                              and (upper(i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
         $config['total_rows'] = $query->num_rows();

         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(4);
         $this->pagination->initialize($config);
         $this->load->model('spbreguler/mmaster');
         $data['isi']=$this->mmaster->cari($iarea,$cari,$config['per_page'],$this->uri->segment(4));
         $data['page_title'] = $this->lang->line('trans_spb');
         $data['ispb']='';
         $data['jmlitem']='';
         $data['detail']='';
         $this->load->view('spbreguler/vmainform',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function refresh()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $jml    =$this->uri->segment(4);
         $kdharga=$this->uri->segment(5);
         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
      for($i=1;$i<=$jml;$i++)
      {
           $data['isi']=$this->mmaster->bacaproducts($cari,$config['per_page'],$this->uri->segment(7),$kdharga);
        $i++;
      }
         $data['baris']=$baris;
         $data['kdharga']=$kdharga;
         $data['cari']=$cari;
         $this->load->view('spbreguler/vlistproduct', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function salesman()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu159')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
          $iarea = strtoupper($this->input->post('iarea', FALSE));
          $dspb  = strtoupper($this->input->post('dspb', FALSE));
          if($iarea=='') $iarea=$this->uri->segment(4);
          if($dspb=='') $dspb=$this->uri->segment(5);
          $per='';
          if($dspb!=''){
            $tmp=explode('-',$dspb);
            $yy=$tmp[2];
            $bl=$tmp[1];
            $per=$yy.$bl;
          }
         $config['base_url'] = base_url().'index.php/spbreguler/cform/salesman/'.$iarea.'/'.$dspb.'/';
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $query = $this->db->query("select distinct a.i_salesman, a.e_salesman_name from tr_customer_salesman a, tr_salesman b
                                    where (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_salesman) like '%$cari%')
                                    and a.i_area='$iarea' and a.i_salesman = b.i_salesman
                                    and b.f_salesman_aktif='true' and a.e_periode='$per'",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $data['iarea']=$iarea;
         $data['dspb']=$dspb;
         $this->load->model('spbreguler/mmaster');
         $data['page_title'] = $this->lang->line('list_salesman');
         $data['isi']=$this->mmaster->bacasalesman($iarea,$per,$cari,$area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(6));
         $this->load->view('spbreguler/vlistsalesman', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
