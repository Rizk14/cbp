<?php
include "php/fungsi.php";
class Cform extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->library('paginationx');
        $this->load->model('exp-do/mmaster');
    }
    public function index()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu474') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $data['page_title'] = 'Export DO';
            $data['dfrom'] = '';
            $data['dto'] = '';
            $data['supplier'] = '';

            $this->load->view('exp-do/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function supplier()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu474') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $cari = strtoupper($this->input->post("cari"));
            $spb = strtoupper($this->input->post("spb"));
            $area = strtoupper($this->input->post("area"));
            if ($spb == '') {
                $spb = $this->uri->segment(4);
            }
            $data['spb'] = $spb;
            if ($area == '') {
                $area = $this->uri->segment(5);
            }
            $data['area'] = $area;
            $config['base_url'] = base_url() . 'index.php/exp-do/cform/supplier/' . $spb . '/' . $area . '/';
            $tmp = explode('-', $spb);
            $query = $this->db->query(" select i_supplier from tr_supplier where i_supplier_group='G0000'
                                  and (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(6);
            $this->pagination->initialize($config);

            $this->load->model('exp-do/mmaster');
            $data['page_title'] = $this->lang->line('list_supplier');
            $data['isi'] = $this->mmaster->bacasupplier($cari, $config['per_page'], $this->uri->segment(6));
            $this->load->view('exp-do/vlistsupplier', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function carisupplier()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu474') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $config['base_url'] = base_url() . 'index.php/exp-do/cform/supplier/index/';
            $cari = $this->input->post('cari', false);
            $cari = strtoupper($cari);
            $query = $this->db->query("select * from tr_supplier
									   where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(5);
            $this->pagination->initialize($config);
            $this->load->model('exp-do/mmaster');
            $data['page_title'] = $this->lang->line('list_supplier');
            $data['isi'] = $this->mmaster->carisupplier($cari, $config['per_page'], $this->uri->segment(5));
            $this->load->view('exp-do/vlistsupplier', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function view()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu474') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $dfrom = $this->input->post('dfrom');
            $dto = $this->input->post('dto');
            $isupplier = $this->input->post('isupplier');
            if ($isupplier == '') {
                $isupplier = $this->uri->segment(4);
            }

            if ($dfrom == '') {
                $dfrom = $this->uri->segment(5);
            }

            if ($dto == '') {
                $dto = $this->uri->segment(6);
            }

            $this->load->model('exp-do/mmaster');
            $data['page_title'] = $this->lang->line('exp-do');
            $data['dfrom'] = $dfrom;
            $data['dto'] = $dto;
            $data['isupplier'] = $isupplier;
            $data['isi'] = $this->mmaster->bacaperiode($dfrom, $dto, $isupplier);
            $sess = $this->session->userdata('session_id');
            $id = $this->session->userdata('user_id');
            $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs = pg_query($sql);
            if (pg_num_rows($rs) > 0) {
                while ($row = pg_fetch_assoc($rs)) {
                    $ip_address = $row['ip_address'];
                    break;
                }
            } else {
                $ip_address = 'kosong';
            }
            $query = pg_query("SELECT current_timestamp as c");
            while ($row = pg_fetch_assoc($query)) {
                $now = $row['c'];
            }
            $pesan = 'Membuka Data OP Periode:' . $dfrom . ' s/d ' . $dto;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now, $pesan);
            $this->load->view('exp-do/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function insert_fail()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu474') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $data['page_title'] = $this->lang->line('exp-do');
            $this->load->view('exp-do/vinsert_fail', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function delete()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu474') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $dto = $this->uri->segment(8);
            $dfrom = $this->uri->segment(7);
            $iarea = $this->uri->segment(6);
            $ispb = $this->uri->segment(5);
            $inota = $this->uri->segment(4);
            $cari = $this->input->post('cari', false);
            $cari = strtoupper($cari);
            $this->load->model('exp-do/mmaster');
            $this->mmaster->delete($inota, $ispb, $iarea);

            $sess = $this->session->userdata('session_id');
            $id = $this->session->userdata('user_id');
            $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs = pg_query($sql);
            if (pg_num_rows($rs) > 0) {
                while ($row = pg_fetch_assoc($rs)) {
                    $ip_address = $row['ip_address'];
                    break;
                }
            } else {
                $ip_address = 'kosong';
            }
            $query = pg_query("SELECT current_timestamp as c");
            while ($row = pg_fetch_assoc($query)) {
                $now = $row['c'];
            }
            $pesan = 'Menghapus Penjualan Per Pelanggan dari tanggal:' . $dfrom . ' sampai:' . $dto;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now, $pesan);

            $config['base_url'] = base_url() . 'index.php/listcustomercard/cform/view/' . $inota . '/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';
            $query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
																	where a.i_customer=b.i_customer
																	and a.f_ttb_tolak='f'
                                  and a.f_nota_koreksi='f'
																	and not a.i_nota isnull
																	and (upper(a.i_nota) like '%$cari%'
																		or upper(a.i_spb) like '%$cari%'
																		or upper(a.i_customer) like '%$cari%'
																		or upper(b.e_customer_name) like '%$cari%')
																	and a.i_area='$iarea' and
																	a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_nota <= to_date('$dto','dd-mm-yyyy')", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(9);
            $this->pagination->initialize($config);
            $data['page_title'] = $this->lang->line('listcustomercard');
            $data['cari'] = $cari;
            $data['dfrom'] = $dfrom;
            $data['keyword'] = '';
            $data['dto'] = $dto;
            $data['iarea'] = $iarea;
            $data['isi'] = $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(9), $cari);
            $this->load->view('listcustomercard/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function cari()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu474') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $cari = strtoupper($this->input->post('cari'));
            $dfrom = $this->input->post('dfrom');
            $dto = $this->input->post('dto');
            $iarea = $this->input->post('iarea');
            if ($dfrom == '') {
                $dfrom = $this->uri->segment(4);
            }

            if ($dto == '') {
                $dto = $this->uri->segment(5);
            }

            if ($iarea == '') {
                $iarea = $this->uri->segment(6);
            }

            if ($cari == '') {
                $cari = $this->uri->segment(7);
            }

            $config['base_url'] = base_url() . 'index.php/listcustomercard/cform/cari/' . $dfrom . '/' . $dto . '/' . $iarea . '/' . $cari . '/';
            $query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$cari%'
										  or upper(a.i_spb) like '%$cari%'
										  or upper(a.i_customer) like '%$cari%'
										  or upper(b.e_customer_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(8);
            $this->pagination->initialize($config);

            $this->load->model('listcustomercard/mmaster');
            $data['page_title'] = $this->lang->line('listcustomercard');
            $data['keyword'] = $cari;
            $data['cari'] = '';
            $data['dfrom'] = $dfrom;
            $data['dto'] = $dto;
            $data['iarea'] = $iarea;
            $data['isi'] = $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari);
            $this->load->view('listcustomercard/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }

    public function cariperpages()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu474') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $dfrom = $this->input->post('dfrom');
            $dto = $this->input->post('dto');
            $iarea = $this->input->post('iarea');
            if ($dfrom == '') {
                $dfrom = $this->uri->segment(4);
            }

            if ($dto == '') {
                $dto = $this->uri->segment(5);
            }

            if ($iarea == '') {
                $iarea = $this->uri->segment(6);
            }

            $keyword = strtoupper($this->uri->segment(7));
            $config['base_url'] = base_url() . 'index.php/listcustomercard/cform/cariperpages/' . $dfrom . '/' . $dto . '/' . $iarea . '/' . $keyword . '/index/';
            $query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$keyword%'
										  or upper(a.i_spb) like '%$keyword%'
										  or upper(a.i_customer) like '%$keyword%'
										  or upper(b.e_customer_name) like '%$keyword%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(9);
            $this->pagination->initialize($config);

            $this->load->model('listcustomercard/mmaster');
            $data['page_title'] = $this->lang->line('listcustomercard');
            $data['cari'] = '';
            $data['keyword'] = $keyword;
            $data['dfrom'] = $dfrom;
            $data['dto'] = $dto;
            $data['iarea'] = $iarea;
            $data['isi'] = $this->mmaster->bacaperiodeperpages($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(9), $keyword);
            $this->load->view('listcustomercard/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }

    public function area()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu474') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $config['base_url'] = base_url() . 'index.php/listcustomercard/cform/area/index/';
            $area1 = $this->session->userdata('i_area');
            $area2 = $this->session->userdata('i_area2');
            $area3 = $this->session->userdata('i_area3');
            $area4 = $this->session->userdata('i_area4');
            $area5 = $this->session->userdata('i_area5');
            if ($area1 == '00') {
                $query = $this->db->query("select * from tr_area", false);
            } else {
                $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
											or i_area = '$area4' or i_area = '$area5'", false);
            }
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(5);
            $this->pagination->initialize($config);
            $this->load->model('listcustomercard/mmaster');
            $data['page_title'] = $this->lang->line('list_area');
            $data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
            $this->load->view('listcustomercard/vlistarea', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function cariarea()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu474') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $area1 = $this->session->userdata('i_area');
            $area2 = $this->session->userdata('i_area2');
            $area3 = $this->session->userdata('i_area3');
            $area4 = $this->session->userdata('i_area4');
            $area5 = $this->session->userdata('i_area5');
            $config['base_url'] = base_url() . 'index.php/listcustomercard/cform/area/index/';
            $cari = strtoupper($this->input->post('cari', false));
            if ($area1 == '00') {
                $query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')", false);
            } else {
                $query = $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')", false);
            }
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(5);
            $this->pagination->initialize($config);
            $this->load->model('listcustomercard/mmaster');
            $data['page_title'] = $this->lang->line('list_area');
            $data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
            $this->load->view('listcustomercard/vlistarea', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function export()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu474') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $dfrom = $this->input->post('dfrom');
            $dto = $this->input->post('dto');
            $isupplier = $this->input->post('isupplier');

            $this->load->model('exp-do/mmaster');

            if ($isupplier == 'AS') {
                $query = $this->db->query("select a.i_op, d.e_supplier_name, a.i_do,b.i_area, c.e_area_name,  a.i_product, a.d_do, a.n_deliver, a.v_product_mill
								from tm_do_item a, tm_do b, tr_area c, tr_supplier d
								where
								a.i_do = b.i_do
								and a.i_supplier = b.i_supplier
								and a.i_op = b.i_op
								and b.i_area  = c.i_area
								and a.i_supplier = d.i_supplier
								and b.i_supplier = d.i_supplier
								and b.f_do_cancel = 'f'
								and a.d_do >= to_date('$dfrom','dd-mm-yyyy') AND a.d_do <= to_date('$dto','dd-mm-yyyy')
								order by a.i_do asc", false);
            } else {

                $query = $this->db->query("select a.i_op, d.e_supplier_name, a.i_do,b.i_area, c.e_area_name,  a.i_product, a.d_do, a.n_deliver, a.v_product_mill
								from tm_do_item a, tm_do b, tr_area c, tr_supplier d
								where
								a.i_do = b.i_do
								and a.i_supplier = b.i_supplier
								and a.i_op = b.i_op
								and b.i_area  = c.i_area
								and a.i_supplier = d.i_supplier
								and b.i_supplier = d.i_supplier
								and b.f_do_cancel = 'f'
								and a.d_do >= to_date('$dfrom','dd-mm-yyyy') AND a.d_do <= to_date('$dto','dd-mm-yyyy')
								and a.i_supplier = '$isupplier'
								order by a.i_do asc", false);
            }

            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("Laporan Detail OP vs DO ")->setDescription(NmPerusahaan);
            $objPHPExcel->setActiveSheetIndex(0);
            if ($query->num_rows() > 0) {
                $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                    array(
                        'font' => array(
                            'name' => 'Arial',
                            'bold' => true,
                            'italic' => false,
                            'size' => 10,
                        ),
                        'alignment' => array(
                            'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                            'vertical' => Style_Alignment::VERTICAL_CENTER,
                            'wrap' => true,
                        ),
                    ),
                    'A2:A4'
                );
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(9);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(9);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);

                $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan DO');
                $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 10, 2);
                $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Tanggal : ' . $dfrom . ' s/d ' . $dto);
                $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 10, 3);

                $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                    array(
                        'font' => array(
                            'name' => 'Arial',
                            'bold' => true,
                            'italic' => false,
                            'size' => 10,
                        ),
                        'alignment' => array(
                            'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                            'vertical' => Style_Alignment::VERTICAL_CENTER,
                            'wrap' => true,
                        ),
                    ),
                    'A5:L5'
                );

                $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Nama Supplier');
                $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),

                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('B5', 'No DO');
                $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Area');
                $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Tanggal DO');
                $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Kode Barang');
                $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Jumlah Kirim');
                $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Harga');
                $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Jumlah');
                $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('I5', 'No OP');
                $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $i = 6;
                $j = 6;
                $no = 0;
                foreach ($query->result() as $row) {
                    $no++;
                    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                        array(
                            'font' => array(
                                'name' => 'Arial',
                                'bold' => false,
                                'italic' => false,
                                'size' => 10,
                            ),
                        ),
                        'A' . $i . ':I' . $i
                    );
                    $jumlah = $row->n_deliver * $row->v_product_mill;
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->i_do, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->i_area, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->d_do, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->i_product, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->n_deliver, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->v_product_mill, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $jumlah, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row->i_op, Cell_DataType::TYPE_STRING);

                    $i++;
                    $j++;
                }
                $x = $i - 1;
                $objPHPExcel->getActiveSheet()->getStyle('F6:I' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
            }
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $nama = 'LapDO-' . $isupplier . '-' . $dto . '.xls';
            $objWriter->save('beli/' . $nama);

            // $sess = $this->session->userdata('session_id');
            // $id = $this->session->userdata('user_id');
            // $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            // $rs = pg_query($sql);
            // if (pg_num_rows($rs) > 0) {
            //     while ($row = pg_fetch_assoc($rs)) {
            //         $ip_address = $row['ip_address'];
            //         break;
            //     }
            // } else {
            //     $ip_address = 'kosong';
            // }
            // $query = pg_query("SELECT current_timestamp as c");
            // while ($row = pg_fetch_assoc($query)) {
            //     $now = $row['c'];
            // }
            // $pesan = 'Export Laporan DO Periode:' . $dto . ' Supplier:' . $isupplier;
            // $this->load->model('logger');
            // $this->logger->write($id, $ip_address, $now, $pesan);

            $this->logger->writenew('Export Laporan DO Periode:' . $dto . ' Supplier:' . $isupplier);

            $data['sukses'] = true;
            $data['inomor'] = $nama;
            $data['folder'] = "beli";

            $this->load->view('nomorurl', $data);
        } else {
            $this->load->view('awal/index');
        }
    }
}
