<?php 
	include ("php/fungsi.php");
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
		$this->load->model('exp-do/mmaster');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu474')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-do');
			$data['dfrom']='';
			$data['dto']='';
			$data['supplier']='';
			$this->load->view('exp-do/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
    	($this->session->userdata('menu474')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari=strtoupper($this->input->post("cari"));
      $spb=strtoupper($this->input->post("spb"));
      $area=strtoupper($this->input->post("area"));
      if($spb==''){
  			$spb=$this->uri->segment(4);
      }
			$data['spb']=$spb;
      if($area==''){
  			$area=$this->uri->segment(5);
      }      
			$data['area']=$area;
			$config['base_url'] = base_url().'index.php/exp-do/cform/supplier/'.$spb.'/'.$area.'/';
			$tmp=explode('-',$spb);
      $query = $this->db->query(" select i_supplier from tr_supplier where i_supplier_group='G0000'
                                  and (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('exp-do/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('exp-do/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu474')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-do/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_supplier
									   where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-do/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-do/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu474')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto  		= $this->input->post('dto');
			$isupplier= $this->input->post('isupplier');
			if($isupplier=='') $isupplier	= $this->uri->segment(4);
			if($dfrom=='') $dfrom	= $this->uri->segment(5);
			if($dto=='') $dto	= $this->uri->segment(6);
			$this->load->model('exp-do/mmaster');
			$data['page_title'] = $this->lang->line('exp-do');
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']= $isupplier;
			$data['isi']		  = $this->mmaster->bacaperiode($dfrom,$dto,$isupplier);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data DO Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('exp-do/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu474')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-do');
			$this->load->view('exp-do/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu474')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dto		= $this->uri->segment(8);
			$dfrom	= $this->uri->segment(7);
			$iarea	= $this->uri->segment(6);
			$ispb		= $this->uri->segment(5);
			$inota	= $this->uri->segment(4);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$this->load->model('exp-do/mmaster');
			$this->mmaster->delete($inota,$ispb,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus Penjualan Per Pelanggan dari tanggal:'.$dfrom.' sampai:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$config['base_url'] = base_url().'index.php/listcustomercard/cform/view/'.$inota.'/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
																	where a.i_customer=b.i_customer 
																	and a.f_ttb_tolak='f'
                                  and a.f_nota_koreksi='f'
																	and not a.i_nota isnull
																	and (upper(a.i_nota) like '%$cari%' 
																		or upper(a.i_spb) like '%$cari%' 
																		or upper(a.i_customer) like '%$cari%' 
																		or upper(b.e_customer_name) like '%$cari%')
																	and a.i_area='$iarea' and
																	a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['keyword']	= '';	
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listcustomercard/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu474')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($cari=='') $cari	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/listcustomercard/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$cari%' 
										  or upper(a.i_spb) like '%$cari%' 
										  or upper(a.i_customer) like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('listcustomercard/mmaster');
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listcustomercard/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu474')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$keyword = strtoupper($this->uri->segment(7));	
			$config['base_url'] = base_url().'index.php/listcustomercard/cform/cariperpages/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$keyword.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'                    
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$keyword%' 
										  or upper(a.i_spb) like '%$keyword%' 
										  or upper(a.i_customer) like '%$keyword%' 
										  or upper(b.e_customer_name) like '%$keyword%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('listcustomercard/mmaster');
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$keyword);
			$this->load->view('listcustomercard/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu474')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomercard/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
											or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomercard/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listcustomercard/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu474')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listcustomercard/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomercard/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listcustomercard/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
  function export() {
	  if (
	  (($this->session->userdata('logged_in')) &&
	  ($this->session->userdata('menu474')=='t')) ||
	  (($this->session->userdata('logged_in')) &&
	  ($this->session->userdata('allmenu')=='t'))
     ){
		//if($isupplier=='') $isupplier	= $this->uri->segment(4);
			$this->load->model('exp-do/mmaster');
		  $this->load->library('PHPExcel');
		  $this->load->library('PHPExcel/IOFactory');
		  $objPHPExcel = new PHPExcel();
		  $objPHPExcel->getProperties()->setTitle("Delivery Order")->setDescription(NmPerusahaan);
		  $objPHPExcel->setActiveSheetIndex(0);
	    $j    = $this->input->post('jumlah',TRUE);	
      $a    = 2;
      $f    = 9;

      $col = 0;
      $bar = 2;
      $col2= 3;
	    for($i=0;$i<=$j;$i++){
        $col2   = 3;
        $i_do   = $this->input->post('i_do'.$i,TRUE);
        $cek	  = $this->input->post('action_'.$i, TRUE);
	      if($cek=='on'){
			  //echo $i_do;

  			$header = $this->mmaster->bacaheader($i_do, $isupplier);
  			$ido='';
        foreach($header as $row){
        $jumlahtotal = 0;
          if($ido=='' or $ido==$row->i_do){
				  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
		  $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, $row->e_supplier_name);
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col2,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				  $col=$col+5;
				  $objPHPExcel->getActiveSheet()->setCellValue('F'.$a, 'Kepada Yth :');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('F'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				  $a++;
				  $col=0;
				  $col2=3;
				  $bar++;
		  //$objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'JL. CIBALIGO RT.004 RW.016 CIBEUREUM CIMAHI SELATAN ');
		  //$objPHPExcel->getActiveSheet()->setCellValue('A'.$a, $row->d_do);
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col2,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				  $col=$col+5;
				  //$objPHPExcel->getActiveSheet()->setCellValue('F'.$a, $row->e_supplier_name);
				  $objPHPExcel->getActiveSheet()->setCellValue('F'.$a, 'PT CHINTAKA BUMI PERTIWI');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
   				$objPHPExcel->getActiveSheet()->getStyle('F'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				  $a++;
				  $bar++;
				  $col=0;
				  $col2=3;
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'DELIVERI ORDER');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col2,$bar);
          $objPHPExcel->getActiveSheet()->getRowDimension($bar)->setRowHeight(-30);
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
						  'size'    => 18
    						),
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				  $col=$col+5;
				  $objPHPExcel->getActiveSheet()->setCellValue('F'.$a, 'JL. CIBALIGO RT.004 RW.016 CIBEUREUM CIMAHI SELATAN');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('F'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
          $a=$a+2;
          $col=0;
          $bar=$bar+2;
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'Nomor DO');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				  $col++;
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$a, ':');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('B'.$a)->applyFromArray(
					  array(
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				  $col++;
          $objPHPExcel->getActiveSheet()->setCellValue('C'.$a, $row->i_do);
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('C'.$a)->applyFromArray(
					  array(
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				  $a++;
				  $col=0;
				  $bar++;
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'No. OP');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
				  $col++;
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$a, ':');
				  $objPHPExcel->getActiveSheet()->getStyle('B'.$a)->applyFromArray(
					  array(
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				  $col++;
          $objPHPExcel->getActiveSheet()->setCellValue('C'.$a, $row->i_op);
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('C'.$a)->applyFromArray(
					  array(
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
	        $a=$a+2;
	        $bar=$bar+2;
	        $col=0;
	        $col2=5;
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'Mohon dikirim kepada kami barang-barang sebagai berikut :');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col2,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    )
						  )
				  );
	        $a++;
	        $bar++;
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'NO');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => Style_Border::BORDER_THIN)
						  )
						  )
				  );
          $col=1;
          $col2=2;
				  $objPHPExcel->getActiveSheet()->setCellValue('B'.$a, 'BANYAK');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col2,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('B'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => Style_Border::BORDER_THIN)
						  )
						  )
				  );
				  $col=$col+3;
				  $objPHPExcel->getActiveSheet()->setCellValue('D'.$a, 'KODE');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('D'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => Style_Border::BORDER_THIN)
						  )
						  )
				  );
				  $col++;
				  $objPHPExcel->getActiveSheet()->setCellValue('E'.$a, 'NAMA BARANG');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('E'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => Style_Border::BORDER_THIN)
						  )
						  )
				  );
				  $col++;
				  $objPHPExcel->getActiveSheet()->setCellValue('F'.$a, 'KETERANGAN');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				  $objPHPExcel->getActiveSheet()->getStyle('F'.$a)->applyFromArray(
					  array(
				    	'font'    => array(
						  'name'	  => 'Arial',
						  'bold'    => true,
						  'italic'  => false,
    						),
						  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => false
					    ),
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN),
							  'left' 	=> array('style' => Style_Border::BORDER_THIN),
							  'right' 	=> array('style' => Style_Border::BORDER_THIN),
						    'bottom' 	=> array('style' => Style_Border::BORDER_THIN)
						  )
						  )
				  );
    			$detail = $this->mmaster->bacadetail($i_do, $isupplier);
    			$no = 0;
    			$a++;
    			foreach ($detail as $det){
    			  $no++;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$a, $no, Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial'
      						),
						    'alignment' => array(
						    'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' => Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => Style_Border::BORDER_THIN)
						    )
						    )
				    );
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$a, '', Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('B'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial',
						    'italic'  => false
      						),
						    'alignment' => array(
						    'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' => Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => Style_Border::BORDER_THIN)
						    )
						    )
				    );
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$a, $det->n_deliver, Cell_DataType::TYPE_NUMERIC);
				    $objPHPExcel->getActiveSheet()->getStyle('C'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial',
						    'italic'  => false
      						),
						    'alignment' => array(
						    'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' => Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => Style_Border::BORDER_THIN)
						    )
						    )
				    );
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$a, $det->i_product, Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('D'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial',
						    'italic'  => false,
      						),
						    'alignment' => array(
						    'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' => Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => Style_Border::BORDER_THIN)
						    )
						    )
				    );
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$a, $det->e_product_name, Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('E'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial',
						    'italic'  => false,
      						),
						    'alignment' => array(
						    'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' => Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => Style_Border::BORDER_THIN)
						    )
						    )
				    );
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$a,'', Cell_DataType::TYPE_STRING);
				    $objPHPExcel->getActiveSheet()->getStyle('F'.$a)->applyFromArray(
					    array(
				      	'font'    => array(
						    'name'	  => 'Arial',
						    'italic'  => false,
      						),
						    'alignment' => array(
						    'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => false
					      ),
						    'borders' => array(
							    'bottom' 	=> array('style' => Style_Border::BORDER_THIN),
							    'left' 	  => array('style' => Style_Border::BORDER_THIN),
							    'right' 	=> array('style' => Style_Border::BORDER_THIN)
						    )
						    )
				    );
			        $a++;
			        $jumlah      = ($det->n_deliver) * ($det->v_product_mill);
			        $jumlahtotal = $jumlahtotal +$jumlah;
			        $iop=$row->i_do;
				      }
				      $a++;
				      $bar=$a;
			        $col=0;
              $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'Jumlah Total');
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$a,0,$a);
				      $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					      array(
				        	'font'    => array(
						      'name'	  => 'Arial',
						      'italic'  => false,
        						),
						      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => false
					        )
						      )
				      );
				      $col=$col+2;
              $objPHPExcel->getActiveSheet()->setCellValue('C'.$a, ':');
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
				      $objPHPExcel->getActiveSheet()->getStyle('C'.$a)->applyFromArray(
					      array(
				        	'font'    => array(
						      'name'	  => 'Arial',
						      'italic'  => false,
        						),
						      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => false
					        )
						      )
				      );
				      $col++;
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$a, $jumlahtotal, Cell_DataType::TYPE_NUMERIC);
				      $objPHPExcel->getActiveSheet()->getStyle('D'.$a)->applyFromArray(
					      array(
				        	'font'    => array(
						      'name'	  => 'Arial',
						      'italic'  => false,
        						),
						      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => false
					        )
						      )
				      );
				      $col++;
				      $objPHPExcel->getActiveSheet()->setCellValue('E'.$a, 'Penerima,');
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
       				$objPHPExcel->getActiveSheet()->getStyle('E'.$a)->applyFromArray(
					      array(
				        	'font'    => array(
						      'name'	  => 'Arial',
						      'italic'  => false,
        						),
						      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => false
					        )
						      )
				      );
				      $col++;
				      $objPHPExcel->getActiveSheet()->setCellValue('F'.$a, 'Mengetahui,');
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
       				$objPHPExcel->getActiveSheet()->getStyle('F'.$a)->applyFromArray(
					      array(
				        	'font'    => array(
						      'name'	  => 'Arial',
						      'italic'  => false,
        						),
						      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => false
					        )
						      )
				      );
				      $a++;
				      $col=0;
				      $col2=5;
	            $bilangan = new Terbilang;
	            $kata=ucwords($bilangan->eja($jumlahtotal));				      
				      $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, $kata.' Rupiah');
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$a,$col2,$a);
       				$objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					      array(
				        	'font'    => array(
						      'name'	  => 'Arial',
						      'italic'  => true,
        						),
						      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => false
					        )
						      )
				      );
				      $a++;
				      $col=0;
				      $col2=2;
              $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'TANGGAL DO');
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$a,$col2,$a);
				      $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
					      array(
				        	'font'    => array(
						      'name'	  => 'Arial',
						      'bold'    => true,
						      'italic'  => false,
        						),
						      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => false
					        )
						      )
				      );
				      $tmp = explode("-", $row->d_do);
              $det	= $tmp[2];
              $mon	= $tmp[1];
              $yir 	= $tmp[0];
              $ddo	= $yir."/".$mon."/".$det;
				      $col++;
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$a, ': '.$ddo, Cell_DataType::TYPE_STRING);
				      $objPHPExcel->getActiveSheet()->getStyle('D'.$a)->applyFromArray(
					      array(
				        	'font'    => array(
						      'name'	  => 'Arial',
						      'bold'    => true,
						      'italic'  => false,
        						),
						      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => false
					        )
						      )
				      );
				      $a++;
		          $col=0;
		          $col2=2;
		    //       if($row->n_top_length>0)
			//           $bayar= "Kredit ".$row->n_top_length." hari";
		    //       else
			//           $bayar= "Tunai";
            //   $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'Cara Pembayaran');
            //   $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$a,$col2,$a);
			// 	      $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
			// 		      array(
			// 	        	'font'    => array(
			// 			      'name'	  => 'Arial',
			// 			      'bold'    => true,
			// 			      'italic'  => false,
        	// 					),
			// 			      'alignment' => array(
			// 			      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
			// 			      'vertical'  => Style_Alignment::VERTICAL_CENTER,
			// 			      'wrap'      => false
			// 		        )
			// 			      )
			// 	      );
			// 	      $col=$col+2;
            //   $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$a, ': '.$bayar, Cell_DataType::TYPE_STRING);
            //   $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
			// 	      $objPHPExcel->getActiveSheet()->getStyle('D'.$a)->applyFromArray(
			// 		      array(
			// 	        	'font'    => array(
			// 			      'name'	  => 'Arial',
			// 			      'bold'    => true,
			// 			      'italic'  => false,
        	// 					),
			// 			      'alignment' => array(
			// 			      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
			// 			      'vertical'  => Style_Alignment::VERTICAL_CENTER,
			// 			      'wrap'      => false
			// 		        )
			// 			      )
			// 	      );
				      $col++;
				      $objPHPExcel->getActiveSheet()->setCellValue('F'.$a, TtdOP);
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
       				$objPHPExcel->getActiveSheet()->getStyle('F'.$a)->applyFromArray(
					      array(
				        	'font'    => array(
						      'name'	  => 'Arial',
						      'italic'  => false,
        						),
						      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => false
					        )
						      )
				      );
				      $a++;
				      $col++;
				      $objPHPExcel->getActiveSheet()->setCellValue('E'.$a, 'Adm Pembelian,');
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
       				$objPHPExcel->getActiveSheet()->getStyle('E'.$a)->applyFromArray(
					      array(
				        	'font'    => array(
						      'name'	  => 'Arial',
						      'italic'  => false,
        						),
						      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => false
					        ),
						    'borders' => array(
							    'top' 	=> array('style' => Style_Border::BORDER_THIN),
						    )
				        )
				      );			      
				      $col=5;
				      $objPHPExcel->getActiveSheet()->setCellValue('F'.$a, 'Spv Pembelian,');
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col,$bar);
       				$objPHPExcel->getActiveSheet()->getStyle('F'.$a)->applyFromArray(
					      array(
				        	'font'    => array(
						      'name'	  => 'Arial',
						      'italic'  => false,
        						),
						      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => false
					        ),
						    'borders' => array(
							    'top' 	=> array('style' => Style_Border::BORDER_THIN),
						    )
					      )
				      );
				      $a++;
				      $col=0;
            //   $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, $row->e_op_statusname);
            //   $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col2,$bar);
			// 	      $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
			// 		      array(
			// 	        	'font'    => array(
			// 			      'name'	  => 'Arial',
			// 			      'bold'    => true,
			// 			      'italic'  => false,
			// 			      'size'    => 14,
        	// 					),
			// 			      'alignment' => array(
			// 			      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
			// 			      'vertical'  => Style_Alignment::VERTICAL_CENTER,
			// 			      'wrap'      => false
			// 		        )
			// 			      )
			// 	      ); 
            //   $a++;
			// 	      $col=0;
            //   $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, $row->e_op_remark);
            //   $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col2,$bar);
			// 	      $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
			// 		      array(
			// 	        	'font'    => array(
			// 			      'name'	  => 'Arial',
			// 			      'bold'    => true,
			// 			      'italic'  => false,
        	// 					),
			// 			      'alignment' => array(
			// 			      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
			// 			      'vertical'  => Style_Alignment::VERTICAL_CENTER,
			// 			      'wrap'      => false
			// 		        )
			// 			      )
			// 	      ); 
			// 	      $a++;
		    //       $dudet	=dateAdd("d",$row->n_delivery_limit,$ddo);
		    //       $dudet 	= explode("-", $dudet);
		    //       $det1	= $dudet[2];
		    //       $mon1	= $dudet[1];
		    //       $yir1 	= $dudet[0];
		    //       $ddo	= $det1." ".mbulan($mon1)." ".$yir1;
		    //   		$tgl=date("d")." ".mbulan(date("m"))." ".date("Y")."  Jam : ".date("H:i:s");
            //   $objPHPExcel->getActiveSheet()->setCellValue('A'.$a, 'TANGGAL CETAK : '.$tgl);
            //   $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($col,$bar,$col2,$bar);
			// 	      $objPHPExcel->getActiveSheet()->getStyle('A'.$a)->applyFromArray(
			// 		      array(
			// 	        	'font'    => array(
			// 			      'name'	  => 'Arial',
			// 			      'italic'  => false,
			// 			      'size'    => 10,
        	// 					),
			// 			      'alignment' => array(
			// 			      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
			// 			      'vertical'  => Style_Alignment::VERTICAL_CENTER,
			// 			      'wrap'      => false
			// 		        )
			// 			      )
			// 	      ); 
				      $a=$a+3;
				      $bar=$a;
				      $col=0;
            }
			$supplier = $row->e_supplier_name;
		}
			  }
     }
      //die();
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='Exp DO '.$supplier.'.xls';
    //   if(file_exists('customer/'.$nama)){
    //     @chmod('customer/'.$nama, 0777);
    //     @unlink('customer/'.$nama);
    //   }
        $objWriter->save('beli/'.$nama);

		    $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		=  $this->db->query($sql);
		    if($rs->num_rows>0){
			    foreach($rs->result() as $tes){
				    $ip_address	  = $tes->ip_address;
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }
		    $data['user']	= $this->session->userdata('user_id');
		    $data['host']	= $ip_address;
		    $data['uri']	= $this->session->userdata('printeruri');
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Exp DO '.$supplier;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );
			  $data['sukses'] = true;
			  $data['inomor']	= 'Exp DO '.$supplier;
			  $this->load->view('nomor',$data);
     }
     else{
       $this->load->view('awal/index'); 
     }
  }
}
?>
