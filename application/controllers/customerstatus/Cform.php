<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu44')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerstatus');
			$data['icustomerstatus']='';
			$this->load->model('customerstatus/mmaster');
			$data['isi']=$this->mmaster->bacasemua();

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Status (Pelanggan)";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('customerstatus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu44')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerstatus	= $this->input->post('icustomerstatus', TRUE);
			$ecustomerstatusname 	= $this->input->post('ecustomerstatusname', TRUE);
			$ncustomerstatusdown 	= $this->input->post('ncustomerstatusdown', TRUE);
			$ncustomerstatusup	= $this->input->post('ncustomerstatusup', TRUE);
			$ncustomerstatusindex	= $this->input->post('ncustomerstatusindex', TRUE);
			if ((isset($icustomerstatus) && $icustomerstatus != ''))
			{
				$this->load->model('customerstatus/mmaster');
				$this->mmaster->insert($icustomerstatus,$ecustomerstatusname,$ncustomerstatusdown,$ncustomerstatusup,$ncustomerstatusindex);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Status (Pelanggan):('.$icustomerstatus.') -'.$ecustomerstatusname;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

				$data['page_title'] = $this->lang->line('master_customerstatus');
				$data['icustomerstatus']='';
				$data['isi']=$this->mmaster->bacasemua();
				$this->load->view('customerstatus/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu44')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerstatus');
			$this->load->view('customerstatus/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu44')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerstatus')." update";
			if($this->uri->segment(4)!=''){
				$icustomer = $this->uri->segment(4);
				$data['icustomerstatus'] = $icustomer;
				$this->load->model('customerstatus/mmaster');
				$data['isi']=$this->mmaster->baca($icustomer);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Status (Pelanggan):('.$icustomer.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('customerstatus/vmainform',$data);
			}else{
				$this->load->view('customerstatus/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu44')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerstatus	= $this->input->post('icustomerstatus', TRUE);
			$ecustomerstatusname 	= $this->input->post('ecustomerstatusname', TRUE);
			$ncustomerstatusdown	= $this->input->post('ncustomerstatusdown', TRUE);
			$ncustomerstatusup	= $this->input->post('ncustomerstatusup', TRUE);
			$ncustomerstatusindex	= $this->input->post('ncustomerstatusindex', TRUE);
			$this->load->model('customerstatus/mmaster');
			$this->mmaster->update($icustomerstatus,$ecustomerstatusname,$ncustomerstatusdown,$ncustomerstatusup,$ncustomerstatusindex);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Status (Pelanggan):('.$icustomerstatus.') -'.$ecustomerstatusname;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

			$data['page_title'] = $this->lang->line('master_customerstatus');
			$data['icustomerstatus']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('customerstatus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu44')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerstatus= $this->uri->segment(4);
			$this->load->model('customerstatus/mmaster');
			$this->mmaster->delete($icustomerstatus);
			
			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Delete Master Status (Pelanggan):('.$icustomer.')';
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);
			$data['page_title'] = $this->lang->line('master_customerstatus');
			$data['icustomerstatus']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('customerstatus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
