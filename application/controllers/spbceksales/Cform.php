<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu187') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('cekspb');
			$data['dfrom'] = '';
			$data['dto'] = '';
			$data['ispb'] = '';
			$this->load->view('spbceksales/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu187') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$cari		= strtoupper($this->input->post('cari'));
			$cari		= strtoupper($cari);
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');

			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea	= $this->uri->segment(6);

			$config['base_url'] = base_url() . 'index.php/spbceksales/cform/view/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';

			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$allarea = $this->session->userdata('allarea');
			$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name 
										from tr_area c, tm_spb a
										left join tr_customer b on(a.i_customer=b.i_customer)
										left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
										where 
										a.i_area=c.i_area
										and ((a.i_approve1 isnull and a.f_spb_stockdaerah='f') or a.f_spb_stockdaerah='t')
										and a.i_notapprove isnull 
										and a.f_spb_cancel='f'
										and (upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%')
										and a.i_area='$iarea' and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy'))
										order by a.d_spb,a.i_area,a.i_spb ", false);
			# and (not a.i_cek_cabang isnull and (a.i_area<>'00' || a.f_spb_consigment='f'))
			#and not a.i_cek_cabang isnull
			/*
			$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name 
					from tm_spb a, tr_customer b, tr_area c


					where 
					a.i_customer=b.i_customer 
					and a.i_area=c.i_area
					and ((a.i_approve1 isnull and a.f_spb_stockdaerah='f') or a.f_spb_stockdaerah='t')
					and a.i_notapprove isnull 
					and a.f_spb_cancel='f'
					and (upper(a.i_spb) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
					     upper(a.i_customer) like '%$cari%')
					and
					  a.i_area='$iarea' and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy'))
					order by a.d_spb,a.i_area,a.i_spb ",false);
*/
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8, 0);
			$this->pagination->initialize($config);
			$data['create_links']	= $this->pagination->create_links();
			$data['page_title'] 	= $this->lang->line('cekspb');
			$this->load->model('spbceksales/mmaster');
			$data['ispb']		  = '';
			$data['cari']		  = $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		  = $dto;
			$data['iarea']		= $iarea;
			$data['isi'] = $this->mmaster->bacasemua($iarea, $dfrom, $dto, $cari, $config['per_page'], $config['cur_page']);
			$this->load->view('spbceksales/vmainform', $data);
		} elseif ($this->session->userdata('logged_in')) {
			$this->load->view('errorauthority');
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu187') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('cekspb');
			$this->load->view('spbceksales/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu187') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$ispb	= $this->input->post('ispbdelete', TRUE);
			$iarea	= $this->input->post('iareadelete', TRUE);
			$this->load->model('spbceksales/mmaster');
			$this->mmaster->delete($ispb, $iarea);
			$config['base_url'] = base_url() . 'index.php/spbceksales/cform/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
										where a.i_customer=b.i_customer and a.i_area=c.i_area
										and a.i_approve1 isnull and a.f_spb_stockdaerah='f'
									 || ($area1=='00')	and a.i_notapprove isnull
										and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
									 	or upper(a.i_spb) like '%$cari%')", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('cekspb');
			$this->load->model('spbceksales/mmaster');
			$data['ispb'] = '';
			$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4));
			$this->load->view('spbceksales/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu187') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$dfrom	= $this->input->post('dfrom') ? $this->input->post('dfrom') : $this->uri->segment(4);
			$dto	= $this->input->post('dto') ? $this->input->post('dto') : $this->uri->segment(5);
			$iarea	= $this->input->post('iarea') ? $this->input->post('iarea') : $this->uri->segment(6);
			/*$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
      if ($cari == ''){
          $cari = 'all';
				  $data['cari'] = '';
				}
			else
        {
          $data['cari'] = $cari;
        }*/
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$allarea = $this->session->userdata('allarea');

			$cari = ($this->input->post("cari", false));
			if ($cari == '') $cari = $this->uri->segment(7);
			if ($cari == 'zxvf') {
				$cari = '';
				$config['base_url'] = base_url() . 'index.php/spbceksales/cform/cari/' . $dfrom . '/' . $dto . '/' . $iarea . '/' . 'zxvf/';
			} else {
				$config['base_url'] = base_url() . 'index.php/spbceksales/cform/cari/' . $dfrom . '/' . $dto . '/' . $iarea . '/' . $cari . '/';
			}

			if ($cari != 'all') {
				$query = $this->db->query("select a.*, b.e_customer_name, c.e_area_name 
								from tm_spb a
                left join tr_customer b on(a.i_customer=b.i_customer)
                left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
                , tr_area c
								where 
								a.i_area=c.i_area
      					and ((a.i_approve1 isnull and a.f_spb_stockdaerah='f') or a.f_spb_stockdaerah='t')
								and a.i_notapprove isnull 
								and a.f_spb_cancel='f'
								and (upper(a.i_spb) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%' or upper (c.e_area_name) ilike '%cari%' or upper(a.i_spb_old) ilike '%$cari%')
								and
								a.i_area='$iarea' and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy')) ", false);
			} else {
				$query = $this->db->query("select a.*, b.e_customer_name, c.e_area_name 
								from tm_spb a
                left join tr_customer b on(a.i_customer=b.i_customer)
                left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
                , tr_area c
								where 
								a.i_area=c.i_area
      					and ((a.i_approve1 isnull and a.f_spb_stockdaerah='f') or a.f_spb_stockdaerah='t')
								and a.i_notapprove isnull 
								and a.f_spb_cancel='f'
								and
								a.i_area='$iarea' and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy')) ", false);
			}


			$config['total_rows'] = $query->num_rows();
			/*$config['base_url'] = base_url().'index.php/spbceksales/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/';*/
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['create_links']	= $this->pagination->create_links();
			$this->load->model('spbceksales/mmaster');
			$data['cari']	= '';
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
			$data['isi'] = $this->mmaster->cari($iarea, $dfrom, $dto, $cari, $config['per_page'], $this->uri->segment(8));
			$data['page_title'] = $this->lang->line('cekspb');
			$data['ispb'] = '';
			$this->load->view('spbceksales/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu187') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('cekspb');
			if (
				($this->uri->segment(4) != '') && ($this->uri->segment(5) != '')
			) {
				$ispb = $this->uri->segment(4);
				$iarea = $this->uri->segment(5);
				$dfrom = $this->uri->segment(6);
				$dto = $this->uri->segment(7);
				$iarea = $this->uri->segment(8);
				$query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows();
				$data['ispb'] = $ispb;
				$data['dfrom'] = $dfrom;
				$data['dto'] = $dto;
				$data['iarea'] = $iarea;
				$this->load->model('spbceksales/mmaster');
				$data['isi'] = $this->mmaster->baca($ispb, $iarea);
				$data['detail'] = $this->mmaster->bacadetail($ispb, $iarea);
				$this->load->view('spbceksales/vmainform', $data);
			} else {
				$this->load->view('spbceksales/vinsert_fail', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu187') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$ispb 		= $this->input->post('ispb', TRUE);
			$iarea		= $this->input->post('iarea', TRUE);
			$ecek1	= $this->input->post('ecek1', TRUE);
			if ($ecek1 == '')
				$ecek1 = null;
			$user		= $this->session->userdata('user_id');
			$this->db->trans_begin();
			$this->load->model('spbceksales/mmaster');
			######
			$dspb 	= $this->input->post('dspb', TRUE);
			if ($dspb != '') {
				$tmp = explode("-", $dspb);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dspb = $th . "-" . $bl . "-" . $hr;
			}
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomername	= $this->input->post('ecustomername', TRUE);
			$eareaname		= $this->input->post('eareaname', TRUE);
			$ispbpo			= $this->input->post('ispbpo', TRUE);
			$nspbtoplength	= $this->input->post('nspbtoplength', TRUE);
			$isalesman		= $this->input->post('isalesman', TRUE);
			$esalesmanname	= $this->input->post('esalesmanname', TRUE);
			$ipricegroup	= $this->input->post('ipricegroup', TRUE);
			$inota			= $this->input->post('inota', TRUE);
			$dspbreceive	= $this->input->post('dspbreceive', TRUE);
			$fspbop			= 'f';
			$ecustomerpkpnpwp	= $this->input->post('ecustomerpkpnpwp', TRUE);
			if ($ecustomerpkpnpwp != '')
				$fspbpkp		= 't';
			else
				$fspbpkp		= 'f';
			$fspbconsigment		= $this->input->post('fspbconsigment', TRUE);
			if ($fspbconsigment != '')
				$fspbconsigment = "t";
			else
				$fspbconsigment = "f";
			$fspbplusppn		= $this->input->post('fspbplusppn', TRUE);
			$fspbplusdiscount	= $this->input->post('fspbplusdiscount', TRUE);
			$fspbstockdaerah	= $this->input->post('fspbstockdaerah', TRUE);
			if ($fspbstockdaerah != '')
				$fspbstockdaerah = 't';
			else
				$fspbstockdaerah = 'f';
			$fspbprogram	= 'f';
			$fspbvalid		= 'f';
			$fspbsiapnotagudang	= 'f';
			$fspbcancel		= 'f';
			$nspbtoplength	= $this->input->post('nspbtoplength', TRUE);
			$nspbdiscount1	= $this->input->post('ncustomerdiscount1', TRUE);
			$nspbdiscount2	= $this->input->post('ncustomerdiscount2', TRUE);
			$nspbdiscount3	= $this->input->post('ncustomerdiscount3', TRUE);
			$vspbdiscount1	= $this->input->post('vcustomerdiscount1', TRUE);
			$vspbdiscount2	= $this->input->post('vcustomerdiscount2', TRUE);
			$vspbdiscount3	= $this->input->post('vcustomerdiscount3', TRUE);
			$vspbdiscounttotal	= $this->input->post('vspbdiscounttotal', TRUE);
			$vspb			= $this->input->post('vspb', TRUE);
			$nspbdiscount1	= str_replace(',', '', $nspbdiscount1);
			$nspbdiscount2	= str_replace(',', '', $nspbdiscount2);
			$nspbdiscount3	= str_replace(',', '', $nspbdiscount3);
			$vspbdiscount1	= str_replace(',', '', $vspbdiscount1);
			$vspbdiscount2	= str_replace(',', '', $vspbdiscount2);
			$vspbdiscount3	= str_replace(',', '', $vspbdiscount3);
			$vspbdiscounttotal	= str_replace(',', '', $vspbdiscounttotal);
			$vspb		= str_replace(',', '', $vspb);
			$ispbold = $this->input->post('spbold', TRUE);
			$jml		= $this->input->post('jml', TRUE);
			if (($iarea != '') && ($ispb != '')) {
				$benar = "false";
				$this->mmaster->updateheader($ispb, $iarea, $ecek1, $user);
				if (($this->db->trans_status() === FALSE)) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
					$sess = $this->session->userdata('session_id');
					$id = $this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if (pg_num_rows($rs) > 0) {
						while ($row = pg_fetch_assoc($rs)) {
							$ip_address	  = $row['ip_address'];
							break;
						}
					} else {
						$ip_address = 'kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while ($row = pg_fetch_assoc($query)) {
						$now	  = $row['c'];
					}
					$pesan = 'Cek SPB Area ' . $iarea . ' No:' . $ispb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now, $pesan);
					$data['sukses']			= true;
					$data['inomor']			= $ispb;
					$this->load->view('nomor', $data);
				}
			}
		} else {
			$this->load->view('awal/index.php');
		}
		######
	}
	function notapprove()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu187') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$ispb 		= $this->input->post('nospb', TRUE);
			$iarea		= $this->input->post('kdarea', TRUE);
			$eapprove	= $this->input->post('enotapprove', TRUE);
			if ($eapprove == '')
				$eapprove = null;
			$user		= $this->session->userdata('user_id');
			$this->db->trans_begin();
			$this->load->model('spbceksales/mmaster');
			$this->mmaster->notapprove($ispb, $iarea, $eapprove, $user);
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
				$data['sukses']			= true;
				$data['inomor']			= $ispb;
				$this->load->view('nomor', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu187') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/spbceksales/cform/area/index/';
			$allarea = $this->session->userdata('allarea');
			$iuser   = $this->session->userdata('user_id');
			if ($allarea == 't') {
				$query = $this->db->query(" select * from tr_area order by i_area", false);
			} else {
				$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('spbceksales/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $allarea, $iuser);
			$this->load->view('spbceksales/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu187') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/spbceksales/cform/area/index/';
			$allarea = $this->session->userdata('allarea');
			$iuser   = $this->session->userdata('user_id');
			$cari    = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);

			if ($allarea == 't') {
				$query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
			} else {
				$query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('spbceksales/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $allarea, $iuser);
			$this->load->view('spbceksales/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
