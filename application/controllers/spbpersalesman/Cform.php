<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu96')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spbpersalesman');
			$data['dfrom']	= '';
			$data['dto']	= '';
			$this->load->view('spbpersalesman/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu96')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari	= strtoupper($this->input->post('cari'));
      if($cari==''){
        if($this->uri->segment(6)!='index'){
          $cari=$this->uri->segment(6);
        }
      }
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
      $tmp=explode('-',$dfrom);
      $thbl=$tmp[2].$tmp[1];
			$allarea= $this->session->userdata('allarea');	
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($cari!=''){
  			$config['base_url'] = base_url().'index.php/spbpersalesman/cform/view/'.$dfrom.'/'.$dto.'/'.$cari.'/';
      }else{
        $config['base_url'] = base_url().'index.php/spbpersalesman/cform/view/'.$dfrom.'/'.$dto.'/index/';
      }
      if(($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00')){
        $que = $this->db->query(" select sum(a.v_spb) as v_spb, sum(a.v_spb)-sum(a.v_spb_discounttotal) as v_spb_bersih
									                from tm_spb a, tr_area b, tr_salesman c
									                where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
									                and a.d_spb <= to_date('$dto','dd-mm-yyyy') and a.f_spb_cancel='f'
									                and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
									                and a.i_salesman=c.i_salesman",false);
        $tmp=$que->row();
        $data['kotor']=$tmp->v_spb;
        $data['bersih']=$tmp->v_spb_bersih;
        $que = $this->db->query(" select sum(v_target) as target from tm_target_itemsls where i_periode='$thbl'");
        if($que->num_rows()>0){
           foreach($que->result() as $tgt){
             $data['target']=$tgt->target;
           }
        }
			  $query = $this->db->query(" select sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, a.i_area, 
										                b.e_area_name, a.i_salesman, c.e_salesman_name
										                from tm_spb a, tr_area b, tr_salesman c
										                where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
										                and a.d_spb <= to_date('$dto','dd-mm-yyyy') and a.f_spb_cancel='f'
										                and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
										                and a.i_salesman=c.i_salesman
										                group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name",false);
      }else{
			  $que = $this->db->query(" select sum(a.v_spb) as v_spb, sum(a.v_spb)-sum(a.v_spb_discounttotal) as v_spb_bersih
										                from tm_spb a, tr_area b, tr_salesman c
										                where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
										                and a.d_spb <= to_date('$dto','dd-mm-yyyy') and a.f_spb_cancel='f'
										                and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%'or upper(c.i_salesman) like '%$cari%')
                                    and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
										                and a.i_salesman=c.i_salesman
										                group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name",false);
        $tmp=$que->row();
        $data['kotor']=$tmp->v_spb;
        $data['bersih']=$tmp->v_spb_bersih;
        $que = $this->db->query(" select sum(v_target) as target from tm_target_itemsls where i_periode='$thbl'");
        if($que->num_rows()>0){
           foreach($que->result() as $tgt){
             $data['target']=$tgt->target;
           }
        }

			  $query = $this->db->query(" select sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, a.i_area, 
										                b.e_area_name, a.i_salesman, c.e_salesman_name
										                from tm_spb a, tr_area b, tr_salesman c
										                where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
										                and a.d_spb <= to_date('$dto','dd-mm-yyyy') and a.f_spb_cancel='f'
										                and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%'or upper(c.i_salesman) like '%$cari%')
                                    and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
										                and a.i_salesman=c.i_salesman
										                group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('spbpersalesman/mmaster');
			$data['page_title'] = $this->lang->line('spbpersalesman');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isi']	= $this->mmaster->bacaperiode($cari,$allarea,$area1,$area2,$area3,$area4,$area5,$dfrom,$dto,$config['per_page'],$this->uri->segment(7));

      $sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Membuka Data SPB Per Salesman dari tanggal:'.$dfrom.' sampai:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('spbpersalesman/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
#####
	function rinci()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu96')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isalesman=$this->uri->segment(4);
			$esalesmanname=$this->uri->segment(5);
			$esalesmanname=str_replace("%20"," ",$esalesmanname);
			$dfrom=$this->uri->segment(6);
			$dto=$this->uri->segment(7);
 			$config['base_url'] = base_url().'index.php/spbpersalesman/cform/rinci/'.$isalesman.'/'.$esalesmanname.'/'.$dfrom.'/'.$dto;
		  $que = $this->db->query(" select d.i_product, d.e_product_name, sum(d.n_order) as n_order
								                from tm_spb a, tr_salesman c, tm_spb_item d
								                where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy') 
								                and a.f_spb_cancel='f' and a.i_salesman='$isalesman' and a.i_salesman=c.i_salesman
                                and a.i_spb=d.i_spb and a.i_area=d.i_area
								                group by d.i_product, d.e_product_name",false);
			$config['total_rows'] = $que->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('spbpersalesman/mmaster');
			$data['page_title'] = $this->lang->line('spbpersalesman');
			$data['isalesman']	= $isalesman;
			$data['esalesmanname']	= $esalesmanname;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isi']	= $this->mmaster->bacaperioderinci($isalesman,$esalesmanname,$dfrom,$dto,$config['per_page'],$this->uri->segment(8));
      $sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Membuka Data SPB Per Salesman ('.$isalesman.') dari tanggal:'.$dfrom.' sampai:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('spbpersalesman/vformviewrinci',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

#####
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu96')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/spbpersalesman/cform/view/'.$dfrom.'/'.$dto.'/index/';
			$query = $this->db->query(" select sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, a.i_area, 
										b.e_area_name, a.i_salesman, c.e_salesman_name
										from tm_spb a, tr_area b, tr_salesman c
										where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_spb <= to_date('$dto','dd-mm-yyyy') 
										and(upper(a.i_salesman) like '%$cari%' or upper(c.e_salesman_name) like '%$cari%' 
										or upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')
										and a.i_area=b.i_area
										and a.i_salesman=c.i_salesman
										group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
										order by a.i_area, a.i_salesman",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('spbpersalesman/mmaster');
			$data['page_title'] = $this->lang->line('spbpersalesman');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isi']	= $this->mmaster->cariperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);
			$this->load->view('spbpersalesman/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
