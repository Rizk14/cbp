<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu302')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listsjpbretur');
			$data['dfrom']='';
			$data['dto']='';
			$data['isjpbr']='';
			$this->load->view('listsjpbretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu302')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$iarea		= $this->input->post('iarea');
			if($iarea=='') $iarea=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listsjpbretur/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';

      $ispg=strtoupper($this->session->userdata("user_id"));
      #$iarea=$this->session->userdata("i_area");
      $query = $this->db->query(" select a.i_customer, a.i_area, a.e_spg_name, b.e_area_name, c.e_customer_name 
                                  from tr_spg a, tr_area b, tr_customer c
                                  where upper(a.i_spg) = '$ispg' and a.i_area='$iarea' and a.i_area=b.i_area 
                                  and a.i_customer=c.i_customer");
      if($query->num_rows>0){
        foreach($query->result() as $xx){
          $ispg=$ispg;
          $iarea=$iarea;
    			$icustomer = $xx->i_customer;
        }
      }else{
        $ispg=$ispg;
        $iarea=$iarea;
  			$icustomer = '';
      }
			$query = $this->db->query(" select a.*, b.e_customer_name
                          from tm_sjpbr a, tr_customer b
													where (upper(a.i_sjpbr) like '%$cari%') and
                          a.i_area='$iarea' and a.i_customer=b.i_customer and
													a.d_sjpbr >= to_date('$dfrom','dd-mm-yyyy') AND
													a.d_sjpbr <= to_date('$dto','dd-mm-yyyy')
													ORDER BY a.i_sjpbr",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpbretur');
			$this->load->model('listsjpbretur/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isjpbr']   = '';
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$icustomer,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SJPB Retur '.$icustomer.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listsjpbretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu302')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listsjpbretur');
			$this->load->view('listsjpbretur/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu302')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	 	= strtoupper($this->input->post('cari', FALSE));
			$isjpbr	= $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$dfrom	= $this->uri->segment(6);
			$dto		= $this->uri->segment(7);
			$this->load->model('listsjpbretur/mmaster');
			$this->mmaster->delete($isjpbr,$iarea);

	    $sess=$this->session->userdata('session_id');
	    $id=$this->session->userdata('user_id');
	    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	    $rs		= pg_query($sql);
	    if(pg_num_rows($rs)>0){
		    while($row=pg_fetch_assoc($rs)){
			    $ip_address	  = $row['ip_address'];
			    break;
		    }
	    }else{
		    $ip_address='kosong';
	    }
	    $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
	    }
	    $pesan='Hapus SJPB Retur No:'.$isjpbr.' Area:'.$iarea;
	    $this->load->model('logger');
	    $this->logger->write($id, $ip_address, $now , $pesan );  

			$config['base_url'] = base_url().'index.php/listsjpbretur/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
		  $ispg=strtoupper($this->session->userdata("user_id"));
      $iarea=$this->session->userdata("i_area");
      $query = $this->db->query(" select a.i_customer, a.i_area, a.e_spg_name, b.e_area_name, c.e_customer_name 
                                  from tr_spg a, tr_area b, tr_customer c
                                  where upper(a.i_spg) = '$ispg' and a.i_area='$iarea' and a.i_area=b.i_area 
                                  and a.i_customer=c.i_customer");
      if($query->num_rows>0){
        foreach($query->result() as $xx){
          $ispg=$ispg;
          $iarea=$iarea;
    			$icustomer = $xx->i_customer;
        }
      }else{
        $ispg=$ispg;
        $iarea=$iarea;
  			$icustomer = '';
      }
			$query = $this->db->query(" select a.i_sjpbr from tm_sjpbr a, tr_customer b
																	where a.f_sjpbr_cancel='f' and (upper(a.i_sjpbr) like '%$cari%') and
                                  a.i_customer='$icustomer' and a.i_area='$iarea' and a.i_customer=b.i_customer and
																	a.d_sjpbr >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_sjpbr <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpbretur');
			$this->load->model('listsjpbretur/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isjpbr']   = '';
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$icustomer,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$this->load->view('listsjpbretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu302')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listsjpbretur/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_sjp a, tr_area b
																	where a.i_area=b.i_area
																	and (upper(a.i_sjp) like '%$cari%')
																	and a.i_area='$iarea' and not a.d_sjp_receive is null and
																	a.d_sjp_receive >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_sjp_receive <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpbretur');
			$this->load->model('listsjpbretur/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['isjp']   = '';
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listsjpbretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu302')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listsjpbretur/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
        else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listsjpbretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listsjpbretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu302')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listsjpbretur/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listsjpbretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listsjpbretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu302')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjpbretur').' Update';
			if($this->uri->segment(4)!=''){
				$isjpbr= $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$dfrom = $this->uri->segment(6);
				$dto = $this->uri->segment(7);
				$data['isjpbr']= $isjpbr;
				$data['iarea']= $iarea;
        $data['dfrom']=$dfrom;
        $data['dto']  =$dto;
  			$data['pst']	= $this->session->userdata('i_area');
				$query 	= $this->db->query("select i_sjpbr from tm_sjpbr_item where i_sjpbr = '$isjpbr' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('listsjpbretur/mmaster');
				$data['isi']=$this->mmaster->baca($isjpbr,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isjpbr,$iarea);
		 		$this->load->view('listsjpbretur/vmainform',$data);
			}else{
				$this->load->view('listsjpbretur/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu302')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj 	= $this->input->post('isj', TRUE);
			$dsj 	= $this->input->post('dsj', TRUE);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$icustomer= $this->input->post('icustomer', TRUE);
			$ispg     = $this->input->post('ispg', TRUE);
			$vspbnetto=$this->input->post('vsj', TRUE);
			$vspbnetto= str_replace(',','',$vspbnetto);
			$jml	  = $this->input->post('jml', TRUE);
			if($dsj!='' && $iarea!='')
			{
				$gaono=true;
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					if($cek=='on'){
						$gaono=false;
					}
					if(!$gaono) break;
				}
				if(!$gaono){
					$this->db->trans_begin();
					$this->load->model('listsjpbretur/mmaster');
#					$isj		 		= $this->mmaster->runningnumbersj($iarea,$thbl);
					$this->mmaster->updatesjheader($isj,$dsj,$iarea,$vspbnetto,$icustomer,$ispg);
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
					  $iproduct			= $this->input->post('iproduct'.$i, TRUE);
					  $iproductgrade= 'A';
					  $iproductmotif= $this->input->post('motif'.$i, TRUE);
					  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
					  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
					  $vunitprice		= str_replace(',','',$vunitprice);
					  $nretur 			= $this->input->post('nretur'.$i, TRUE);
					  $nretur		  	= str_replace(',','',$nretur);
					  $nasal   			= $this->input->post('nasal'.$i, TRUE);
					  $nasal		  	= str_replace(',','',$nasal);
					  $nreceive	  	= $this->input->post('nreceive'.$i, TRUE);
					  $nreceive		  = str_replace(',','',$nreceive);
					  $eremark  		= $this->input->post('eremark'.$i, TRUE);
					  if($eremark=='')$eremark=null;
###################
            $this->mmaster->deletesjdetail( $isj, $iarea, $iproduct, $iproductgrade, $iproductmotif);
						$th=substr($dsj,0,4);
						$bl=substr($dsj,5,2);
						$emutasiperiode=$th.$bl;
            if( ($nasal!='') && ($nasal!=0) ){
					    $this->mmaster->updatemutasi04($icustomer,$iproduct,$iproductgrade,$iproductmotif,$nasal,$emutasiperiode);
					    $this->mmaster->updateic04($iproduct,$iproductgrade,$iproductmotif,$icustomer,$nasal);
            }
###################
            if($cek=='on'){
					    if($nretur>0){
						    $this->mmaster->insertsjdetail( $iproduct,$iproductgrade,$iproductmotif,$eproductname,$nreceive,$nretur,$vunitprice,$isj,$dsj,$iarea,
                                                $eremark,$i);
                $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$icustomer);
                if(isset($trans)){
                  foreach($trans as $itrans)
                  {
                    $q_aw =$itrans->n_quantity_stock;
                    $q_ak =$itrans->n_quantity_stock;
                    $q_in =0;
                    $q_out=0;
                    break;
                  }
                }else{
                  $q_aw=0;
                  $q_ak=0;
                  $q_in=0;
                  $q_out=0;
                }
                $th=substr($dsj,0,4);
                $bl=substr($dsj,5,2);
                $emutasiperiode=$th.$bl;
                $ada=$this->mmaster->cekmutasi2($iproduct,$iproductgrade,$iproductmotif,$icustomer,$emutasiperiode);
                if($ada=='ada')
                {
                  $this->mmaster->updatemutasi1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$nretur,$emutasiperiode);
                }else{
                  $this->mmaster->insertmutasi1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$nretur,$emutasiperiode,$q_aw,$q_ak);
                }
                if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$icustomer))
                {
                  $this->mmaster->updateic1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$nretur,$q_ak);
                }else{
                  $this->mmaster->insertic1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$eproductname,$nretur);
                }
					    }
						}
					}
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
#						$this->db->trans_rollback();
						$this->db->trans_commit();
						$sess=$this->session->userdata('session_id');
						$id=$this->session->userdata('user_id');
						$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
						$rs		= pg_query($sql);
						if(pg_num_rows($rs)>0){
							while($row=pg_fetch_assoc($rs)){
								$ip_address	  = $row['ip_address'];
								break;
							}
						}else{
							$ip_address='kosong';
						}
						$query 	= pg_query("SELECT current_timestamp as c");
						while($row=pg_fetch_assoc($query)){
							$now	  = $row['c'];
						}
						$pesan='Input SJPB Retur No:'.$isj;
						$this->load->model('logger');
						$this->logger->write($id, $ip_address, $now , $pesan );
						$data['sukses']			= true;
						$data['inomor']			= $isj;
						$this->load->view('nomor',$data);
					}
				}
			}else{
        $ispg=strtoupper($this->session->userdata("user_id"));
        $iarea=$this->session->userdata("i_area");
        $query = $this->db->query(" select a.i_customer, a.i_area, a.e_spg_name, b.e_area_name, c.e_customer_name 
                                    from tr_spg a, tr_area b, tr_customer c
                                    where upper(a.i_spg) = '$ispg' and a.i_area='$iarea' and a.i_area=b.i_area 
                                    and a.i_customer=c.i_customer");
        if($query->num_rows>0){
          foreach($query->result() as $xx){
            $ispg=$ispg;
            $iarea=$iarea;
      			$icustomer = $xx->i_customer;
      			$eareaname = $xx->e_area_name;
      			$espgname = $xx->e_spg_name;
      			$ecustomername = $xx->e_customer_name;
          }
        }else{
          $ispg=$ispg;
          $iarea=$iarea;
    			$icustomer = '';
    			$eareaname = '';
    			$espgname = '';
    			$ecustomername = '';
        }
        $data['page_title'] = $this->lang->line('sjpbretur');
				$data['isjr']				= '';
				$data['dsjr']				= date('d-m-Y');
				$data['ispg'] 			= $ispg;
				$data['iarea']			= $iarea;
				$data['icustomer']	= $icustomer;
				$data['eareaname']  = $eareaname;
				$data['espgname']		= $espgname;
				$data['ecustomername']= $ecustomername;
				$this->load->view('listsjpbretur/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu302')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari = strtoupper($this->input->post('cari', FALSE));
      $baris = strtoupper($this->input->post('baris', FALSE));
 			if($baris=='') $baris=$this->uri->segment(4);
      if($this->uri->segment(5)!='x01'){
        if($cari=='') $cari=$this->uri->segment(5);
        $config['base_url'] = base_url().'index.php/listsjpbretur/cform/product/'.$baris.'/'.$cari.'/';
      }else{
        $config['base_url'] = base_url().'index.php/listsjpbretur/cform/product/'.$baris.'/x01/';
      }
			$query = $this->db->query(" select a.i_product, a.e_product_name, b.v_product_retail, c.i_product_motif, c.e_product_motifname 
                                  from tr_product a, tr_product_price b, tr_product_motif c
                                  where a.i_product=b.i_product and b.i_price_group='00'
                                  and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                                  and a.i_product=c.i_product",false);# and a.i_product_status<>'4'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
 			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('listsjpbretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$this->uri->segment(4);
 			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('listsjpbretur/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
