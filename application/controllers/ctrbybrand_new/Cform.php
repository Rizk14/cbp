<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
    	$this->load->helper(array('file','directory','fusioncharts'));
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = "Sales Performance By Brand";
			$data['dfrom']	= '';
			$data['dto']	= '';
			$this->load->view('ctrbybrand_new/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

		   	$dfrom = $this->input->post('dfrom',TRUE);
		   	$dto   = $this->input->post('dto',TRUE);
			$iuser = $this->session->userdata('user_id');
			
			if($dfrom!=''){
				$tmp = explode("-", $dfrom);
				$day = $tmp[0];
				$month = $tmp[1];
				$year = $tmp[2];
			}
			$tahun=$year;

			
			########chart start
			$tipe=$this->uri->segment(5);
			
			if($tipe==''){
			$graph_swfFile      = base_url().'flash/FCF_MSColumn3D.swf';
			}else{
				$tipe=str_replace("tandatitik",".",$tipe);
				$graph_swfFile      = base_url().'flash/'.$tipe;
			}
			$graph_caption      = $this->lang->line('ctrbybrand_new');
			$graph_numberPrefix = '' ;
			$graph_title        = $this->lang->line('ctrbybrand_new') ;
			$graph_width        = 954;
			$graph_height       = 500;
			$this->load->model('ctrbybrand_new/mmaster');

      // group
			$i=0;
			$result = $this->mmaster->bacagroup($tahun);
			foreach($result as $row){
				$category[$i] = $row->group;
				$i++;
			}

      // data set
			$dataset[0] = 'GROWTHOA' ;
			$dataset[1] = 'GROWTHQTY';
			$dataset[2] = 'GROWTHRP';

      //data 1
			$i=0;
      
			$result = $this->mmaster->baca($dfrom,$dto);
			$grwoa=0;
			foreach($result as $row){
				if ($row->oaprev == 0) {
              			$grwoa = 0;
          			} else { //jika pembagi tidak 0
              			$grwoa = (($row->oa-$row->oaprev)/$row->oaprev);
          		}
				
				$arrData['GROWTHOA'][$i] = number_format($grwoa,2);
				$i++;
			}

			$result = $this->mmaster->baca($dfrom,$dto);
			$grwqty=0;
			foreach($result as $row){
		          if ($row->qnotaprev == 0) {
		              $grwqty = 0;
		          } else { //jika pembagi tidak 0
		              $grwqty = (($row->qnota-$row->qnotaprev)/$row->qnotaprev);
		          }
				
				$arrData['GROWTHQTY'][$i] = number_format($grwqty,2);
				$i++;
			}

			$result = $this->mmaster->baca($dfrom,$dto);
			$grwrp=0;
			foreach($result as $row){
		          if ($row->vnotaprev == 0) {
		              $grwrp = 0;
		          } else { //jika pembagi tidak 0
		              $grwrp = (($row->vnota-$row->vnotaprev)/$row->vnotaprev);
		          }

				$arrData['GROWTHRP'][$i] = number_format($grwrp,2);
				$i++;
			}

			$strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='0' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='2' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' showValues='0' startingangle='120' enablemultislicing='0' pieSliceDepth='30'>" ;
      
#      $strXML = "caption=Factory Output report;subCaption=By Quantity;pieSliceDepth=30; showBorder=1;formatNumberScale=0;numberSuffix=Units;animation=1";
# $FC->setChartParams($strXML);

      //Convert category to XML and append
			$strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
      foreach ($category as $c) {
			$strXML .= "<category name='".$c."'/>" ;
			}
			$strXML .= "</categories>" ;

      //Convert dataset and data to XML and append
      foreach ($dataset as $set) {
          $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
          foreach ($arrData[$set] as $d) {
              $strXML .= "<set value='".$d."'/>" ;
          }
          $strXML .= "</dataset>" ;
      }

  //Close <chart> element
	  $strXML .= "</graph>";

      $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);
      $data['tahun']=$tahun;
      $data['modul']='ctrbybrand_new';
		  $data['eusi']= directory_map('./flash/');
		  $data['file']='';
			########chart end
			
			$config['base_url'] = base_url().'index.php/ctrbybrand_new/cform/view/'.$tahun.'/'.$tipe.'/';
#			$this->load->model('ctrbybrand_new/mmaster');
			$data['page_title'] = "Sales Performance By Brand";
			$data['iuser'] 		= $iuser;
			$data['dfrom']		= $dfrom;
			$data['dto'] 		= $dto;
			$data['tahun']		= $tahun;
			$data['isi']		= $this->mmaster->baca($dfrom,$dto);
			$data['ob']  = $this->mmaster->bacaob($dfrom,$dto);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Ctr by Island periode :'.$tahun;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('ctrbybrand_new/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
