<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu128')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-gj-un/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);			
			$query = $this->db->query(" select * from tm_general_jurnal
										where (upper(i_area) like '%$cari%' or upper(i_jurnal) like '%$cari%') and f_close='f' 
										and f_posting='t'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listgj');
			$this->load->model('akt-gj-un/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$data['cari']=$cari;
			$data['ijurnal']='';
			$this->load->view('akt-gj-un/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu128')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('list_nota');
			$this->load->view('akt-gj-un/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu128')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-gj-un/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);			
			$query = $this->db->query(" select * from tm_general_jurnal
										where (upper(i_area) like '%$cari%' or upper(i_jurnal) like '%$cari%') and f_close='f' 
										and f_posting='t'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listgj');
			$this->load->model('akt-gj-un/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$data['cari']=$cari;
			$data['ijurnal']='';
			$this->load->view('akt-gj-un/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu128')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('gj');
			if(
				($this->uri->segment(4))			
			  )
			{
				$ijurnal			= $this->uri->segment(4);
				$query 				= $this->db->query("select * from tm_general_jurnalitem where i_jurnal = '$ijurnal' ");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ijurnal']	= $ijurnal;
				$this->load->model('akt-gj-un/mmaster');
				$data['isi']		= $this->mmaster->baca($ijurnal);
				$data['detail']		= $this->mmaster->bacadetail($ijurnal);
		 		$this->load->view('akt-gj-un/vmainform',$data);

			}else{
				$this->load->view('akt-gj-un/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function unposting()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu128')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$this->load->model('akt-gj-un/mmaster');


			$ijurnal		= $this->input->post('ijurnal', TRUE);
			$djurnal		= $this->input->post('djurnal', TRUE);
			if($djurnal!='')
			{
				$tmp=explode("-",$djurnal);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$djurnal=$th."-".$bl."-".$hr;
				$iperiode=$th.$bl;
			}
			$eareaname		= $this->input->post('eareaname', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$edescription	= $this->input->post('edescription', TRUE);
			$jml			= $this->input->post('jml', TRUE);
			$fclose			= 'f';
			$this->db->trans_begin();
			$this->mmaster->deletetransheader($ijurnal,$iarea,$djurnal);
			$this->mmaster->updatejurnal($ijurnal,$iarea);
			for($i=1;$i<=$jml;$i++)
			{
				$acc		= $this->input->post('icoa'.$i, TRUE);
				$accname	= $this->input->post('ecoaname'.$i, TRUE);
				$vdebet		= $this->input->post('vdebet'.$i, TRUE);
				$vdebet		= str_replace(',','',$vdebet);
				$vkredit	= $this->input->post('vkredit'.$i, TRUE);
				$vkredit	= str_replace(',','',$vkredit);
				if($vkredit=='0')
				{
					$this->mmaster->deletetransitemdebet($acc,$ijurnal,$djurnal);
					$this->mmaster->updatesaldodebet($acc,$iperiode,$vdebet);
					$this->mmaster->deletegldebet($acc,$ijurnal,$djurnal);
				}elseif($vdebet=='0')
				{
					$this->mmaster->deletetransitemkredit($acc,$ijurnal,$djurnal);
					$this->mmaster->updatesaldokredit($acc,$iperiode,$vkredit);
					$this->mmaster->deleteglkredit($acc,$ijurnal,$djurnal);
				}
			}
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='UnPosting Jurnal Umum No:'.$ijurnal.' Area:'.$iarea;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses'] = true;
				$data['inomor']	= $ijurnal;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
