<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu176')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/printdaftartagihan/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('printdaftartagihan');
			$data['cari'] = '';
      $data['dfrom']= '';
			$data['dto']  = '';
			$data['isi']  = '';
			$this->load->view('printdaftartagihan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu176')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printdaftartagihan/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			
			$this->load->model('printdaftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printdaftartagihan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu176')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printdaftartagihan/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printdaftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printdaftartagihan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu176')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari	  = strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
      if($dfrom=='')$dfrom=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
      if($iarea=='')$iarea=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printdaftartagihan/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');

			if($area1=='00')
			{
			   $query 	= $this->db->query("select a.* from tm_dt a, tr_area b
						                         where a.i_area=b.i_area and a.f_sisa='t' and a.f_dt_cancel ='f'
						                         and (upper(a.i_dt) like '%$cari%') and a.d_dt >= to_date('$dfrom','dd-mm-yyyy') and a.d_dt <= to_date('$dto','dd-mm-yyyy')
                                  and a.i_area='$iarea' order by a.i_dt DESC",false);
			}
			else
			{
			   $query 	= $this->db->query("select a.* from tm_dt a, tr_area b
						                         where a.i_area=b.i_area and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' 
                                      or a.i_area='$area4' or a.i_area='$area5')
                                      and a.d_dt >= to_date('$dfrom','dd-mm-yyyy') and a.d_dt <= to_date('$dto','dd-mm-yyyy')
                                      and a.i_area='$iarea'
						                         and a.f_sisa='t' and a.f_dt_cancel ='f'
						                         and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						                         or upper(a.i_dt) like '%$cari%') order by a.i_dt DESC",false);
			}

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printdaftartagihan');
			$this->load->model('printdaftartagihan/mmaster');
      $data['dfrom']= $dfrom;
			$data['dto']  = $dto;
			$data['iarea']= $iarea;
			$data['isi']=$this->mmaster->bacasemua($iarea,$area1,$area2,$area3,$area4,$area5,$cari,$config['per_page'],$this->uri->segment(7),$dfrom,$dto);
			$this->load->view('printdaftartagihan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu176')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$idt = $this->uri->segment(4);
			$iarea = $this->uri->segment(5);
      $dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
      if($iarea=='')$iarea=$this->uri->segment(5);
      if($dfrom=='')$dfrom=$this->uri->segment(6);
      if($dto=='')$dto=$this->uri->segment(7);

      if($idt!=''){
				$idt1=$idt;
			  $tmp=explode("-",$idt);
			  $siji=$tmp[0];
			  $loro=$tmp[1];
        if($loro!=''){
  			  $idt=$siji."/".$loro;
        }else{
          $idt=$siji;
        }
		  }

			$this->load->model('printdaftartagihan/mmaster');
      $data['dfrom']= $dfrom;
			$data['dto']  = $dto;
			$data['iarea']= $iarea;
			$data['idt']=$idt;
			$data['iarea']=$iarea;
			$data['page_title'] = $this->lang->line('printdaftartagihan');
      $query 	= $this->db->query("select * from tm_dt where i_area = '$iarea' and i_dt='$idt' ");
      if($query->num_rows()==0){
		    $data['isi']	=$this->mmaster->baca($idt1,$iarea);
		    $data['detail']=$this->mmaster->bacadetail($idt1,$iarea);
      }else{
			  $data['isi']	=$this->mmaster->baca($idt,$iarea);
			  $data['detail']=$this->mmaster->bacadetail($idt,$iarea);
      }
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak DT Area '.$iarea.' No:'.$idt;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printdaftartagihan/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu176')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printdaftartagihan');
			$this->load->view('printdaftartagihan/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu176')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$allarea= $this->session->userdata('allarea');	
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');

			$config['base_url'] = base_url().'index.php/printdaftartagihan/cform/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));

			if($allarea=='t')
			{
			   $query 	= $this->db->query("select a.i_dt from tm_dt a, tr_area b
						                         where a.i_area=b.i_area and a.f_sisa='t'
						                         and (upper(a.i_dt) like '%$cari%')",false);
			}
			elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'))
			{
			   $query 	= $this->db->query("select a.i_dt from tm_dt a, tr_area b
						                         where a.i_area=b.i_area and a.f_sisa='t'
						                         and (upper(a.i_dt) like '%$cari%')",false);
			}
			else
			{
			   $query 	= $this->db->query("select a.i_dt from tm_dt a, tr_area b
						                         where a.i_area=b.i_area and 
						                              ( a.i_area='$area1' or 
						                                 a.i_area='$area2' or 
						                                 a.i_area='$area3' or 
						                                 a.i_area='$area4' or 
						                                 a.i_area='$area5' )
						                         and a.f_sisa='t'
						                         and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
						                         or upper(a.i_dt) like '%$cari%')",false);
			}

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printdaftartagihan');
			$this->load->model('printdaftartagihan/mmaster');
			$data['idt']='';
			$data['cari']=$cari;
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4),$allarea,$area1,$area2,$area3,$area4,$area5);
			$this->load->view('printdaftartagihan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
