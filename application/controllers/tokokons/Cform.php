<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$config['base_url'] = base_url().'index.php/tokokons/cform/index/';
			$cari 	= $this->input->post('cari', TRUE);
			$cari	= strtoupper($cari);
			$query	= $this->db->query("select a.i_customer, b.e_customer_name, b.e_customer_address, b.e_customer_phone
                                  from tr_customer_consigment a, tr_customer b
                                  where a.i_customer=b.i_customer
                                  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                                  order by a.i_customer",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('tokokons');
			$data['icustomer']='';
			$this->load->model('tokokons/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Toko Konsinyasi";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('tokokons/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$icustomer 			= $this->input->post('icustomer', TRUE);
			if ($icustomer != '')
			{
				$this->load->model('tokokons/mmaster');
				$this->mmaster->insert($icustomer);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Toko Konsinyasi ('.$icustomer.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

				$config['base_url'] = base_url().'index.php/tokokons/cform/index/';
				$cari 			= $this->input->post('cari', TRUE);
				$cari				= strtoupper($cari);
				$query			= $this->db->query("select a.i_customer, b.e_customer_name, b.e_customer_address, b.e_customer_phone
                                  from tr_customer_consigment a, tr_customer b
                                  where a.i_customer=b.i_customer
                                  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                                  order by a.i_customer",false);
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('tokokons');
				$data['icustomer']='';
				$this->load->model('tokokons/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('tokokons/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('tokokons');
			$this->load->view('tokokons/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('tokokons')." update";
			if($this->uri->segment(4)){
				$icustomer = $this->uri->segment(4);
				$data['icustomer'] = $icustomer;
				$this->load->model('tokokons/mmaster');
				$data['isi']=$this->mmaster->baca($icustomer);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Toko Konsinyasi ('.$icustomer.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('tokokons/vmainform',$data);
			}else{
				$this->load->view('tokokons/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer				= $this->input->post('icustomer', TRUE);
			$this->load->model('tokokons/mmaster');
			$this->mmaster->update($icustomer);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Toko Konsinyasi ('.$icustomer.')';
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

			$config['base_url'] = base_url().'index.php/tokokons/cform/index/';
			$cari 				= $this->input->post('cari', TRUE);
			$cari				= strtoupper($cari);
			$query				= $this->db->query("select a.i_customer, b.e_customer_name, b.e_customer_address, b.e_customer_phone
                                  from tr_customer_consigment a, tr_customer b
                                  where a.i_customer=b.i_customer
                                  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                                  order by a.i_customer",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('tokokons');
			$data['icustomer']='';
			$this->load->model('tokokons/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('tokokons/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer	= $this->uri->segment(4);
			$this->load->model('tokokons/mmaster');
			$this->mmaster->delete($icustomer);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Delete Toko Konsinyasi ('.$icustomer.')';
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);
	        
			$config['base_url'] = base_url().'index.php/tokokons/cform/index/';
			$cari 				= $this->input->post('cari', TRUE);
			$cari				= strtoupper($cari);
			$query				= $this->db->query("select a.i_customer, b.e_customer_name, b.e_customer_address, b.e_customer_phone
                                  from tr_customer_consigment a, tr_customer b
                                  where a.i_customer=b.i_customer
                                  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                                  order by a.i_customer",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('tokokons');
			$data['icustomer']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('tokokons/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		$config['base_url'] = base_url().'index.php/tokokons/cform/index/';
		$cari 				= $this->input->post('cari', TRUE);
		$cari				= strtoupper($cari);
		$query				= $this->db->query("select a.i_customer, b.e_customer_name, b.e_customer_address, b.e_customer_phone
                                  from tr_customer_consigment a, tr_customer b
                                  where a.i_customer=b.i_customer
                                  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                                  order by a.i_customer",false);
		$config['total_rows'] = $query->num_rows();
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(4);
		$this->pagination->initialize($config);
		$data['page_title'] = $this->lang->line('tokokons');
		$data['icustomer']='';
		$this->load->model('tokokons/mmaster');
		$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
		$this->load->view('tokokons/vmainform', $data);
	}

	function pelanggan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){				
			
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/tokokons/cform/pelanggan/'.$baris.'/sikasep/';	
			$query = $this->db->query("select * from tr_customer order by e_customer_name asc",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('tokokons/mmaster');

			$data['baris']=$baris;
			$data['cari']='';
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacapelanggan($config['per_page'],$this->uri->segment(6));
			$this->load->view('tokokons/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caripelanggan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu443')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('tokokons/mmaster');
			
			/*$cari = $this->input->post('cari', FALSE)?$this->input->post('cari', FALSE):$this->input->get_post('cari', FALSE);
			$keywordcari	= $cari==''?'null':$cari;
			$config['base_url'] = base_url().'index.php/tokokons/cform/caripelanggan/index/'.$keywordcari.'/';*/

			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/tokokons/cform/caripelanggan/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/tokokons/cform/caripelanggan/'.$baris.'/sikasep/';
			
			//$cari=strtoupper($cari);
			$stquery = " select * from tr_customer
						where (upper(i_customer) ilike '%$cari%' or upper(e_customer_name) ilike '%$cari%') order by e_customer_name asc ";

			$query = $this->db->query($stquery,false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caripelanggan($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('tokokons/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
