<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu330')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isjold	= $this->input->post('isjold', TRUE);
			$dsj 	= $this->input->post('dsj', TRUE);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
				/*$thbl=substr($th,2,2).$bl;*/
				$thbl=$th.$bl;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$ispmb		= $this->input->post('ispmb', TRUE);
			$dspmb	 	= $this->input->post('dspmb', TRUE);
			if($dspmb!=''){
				$tmp=explode("-",$dspmb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspmb=$th."-".$bl."-".$hr;
			}
			$eareaname	= $this->input->post('eareaname', TRUE);
			$vspbnetto=$this->input->post('vsj', TRUE);
			$vspbnetto= str_replace(',','',$vspbnetto);
			$jml	  = $this->input->post('jml', TRUE);
			if($dsj!='' && $eareaname!='' && $ispmb!='')
			{
				$gaono=true;
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					if($cek=='on'){
						$gaono=false;
					}
					if(!$gaono) break;
				}
				if(!$gaono){
					$this->db->trans_begin();
					$this->load->model('sjreceivetoko/mmaster');
					$istore	  			= $this->input->post('istore', TRUE);
					if($istore=='AA'){
						$istorelocation		= '01';
					}else{
						$istorelocation		= '00';
					}
					$istorelocationbin	= '00';
					$isjtype			= '01';
/*					$isj		 		= $this->mmaster->runningnumbersj($iarea,$isjtype,$thbl); */
					$isj		 		= $this->mmaster->runningnumbersj($iarea,$thbl);
					$this->mmaster->insertsjheader($ispmb,$dspmb,$isj,$dsj,$iarea,$vspbnetto,$isjold);
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
						if($cek=='on'){
						  $iproduct			= $this->input->post('iproduct'.$i, TRUE);
						  $iproductgrade= 'A';
						  $iproductmotif= $this->input->post('motif'.$i, TRUE);
						  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
						  $vunitprice		= str_replace(',','',$vunitprice);
						  $ndeliver			= $this->input->post('ndeliver'.$i, TRUE);
						  $ndeliver			= str_replace(',','',$ndeliver);
						  $norder		  	= $this->input->post('norder'.$i, TRUE);
						  $norder			  = str_replace(',','',$norder);
              $nreceive     = '0';
						  $eremark  		= $this->input->post('eremark'.$i, TRUE);
						  if($eremark=='')$eremark=null;
						  if($norder>0){
							$this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$norder,$ndeliver,
										                          $vunitprice,$ispmb,$dspmb,$isj,$dsj,$iarea,$istore,$istorelocation,
                                              $istorelocationbin,$eremark,$i,$nreceive);
							$this->mmaster->updatespmbitem($ispmb,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$iarea);

              $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_awal;
                  $q_ak =$itrans->n_quantity_akhir;
                  $q_in =$itrans->n_quantity_in;
                  $q_out=$itrans->n_quantity_out;
                  break;
                }
              }else{
                $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
                if(isset($trans)){
                  foreach($trans as $itrans)
                  {
                    $q_aw =$itrans->n_quantity_stock;
                    $q_ak =$itrans->n_quantity_stock;
                    $q_in =0;
                    $q_out=0;
                    break;
                  }
                }else{
                  $q_aw=0;
                  $q_ak=0;
                  $q_in=0;
                  $q_out=0;
                }
              }
              $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$isj,$q_in,$q_out,$ndeliver,$q_aw,$q_ak);
              $th=substr($dsj,0,4);
              $bl=substr($dsj,5,2);
              $emutasiperiode=$th.$bl;
              $ada=$this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$emutasiperiode);
              if($ada=='ada')
              {
                $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$emutasiperiode);
              }else{
                $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$emutasiperiode);
              }
              if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00'))
              {
                $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$q_ak);
              }else{
                $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$ndeliver);
              }
						  }
						}
					}
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();

            $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		=  $this->db->query($sql);
			      if($rs->num_rows>0){
				      foreach($rs->result() as $tes){
					      $ip_address	  = $tes->ip_address;
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }

			      $data['user']	= $this->session->userdata('user_id');
      #			$data['host']	= $this->session->userdata('printerhost');
			      $data['host']	= $ip_address;
			      $data['uri']	= $this->session->userdata('printeruri');
			      $query 	= pg_query("SELECT current_timestamp as c");
			      while($row=pg_fetch_assoc($query)){
				      $now	  = $row['c'];
			      }
			      $pesan='Receive SJ Retur Toko No:'.$isj.' Area:'.$iarea;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan );

						$data['sukses']			= true;
						$data['inomor']			= $isj;
						$this->load->view('nomor',$data);
					}
				}
			}else{
				$data['page_title'] = $this->lang->line('sjrectoko');
			  $data['dfrom']='';
			  $data['dto']='';
			  $this->load->view('sjreceivetoko/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu330')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjrectoko');
			$this->load->view('sjreceivetoko/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$data['page_title'] = $this->lang->line('sjrectoko');
			if($this->uri->segment(4)!=''){
				$isj	= $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$ispb= $this->uri->segment(6);
				$data['isj'] = $isj;
				$data['iarea']= $iarea;
  			$data['pst']	= $this->session->userdata('i_area');
				$query 	= $this->db->query("select i_sj from tm_nota_item where i_sj = '$isj' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('sjreceivetoko/mmaster');
				$data['isi']=$this->mmaster->baca($isj,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isj,$iarea,$ispb);
		 		$this->load->view('sjreceivetoko/vmainform',$data);
			}else{
				$this->load->view('sjreceivetoko/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$isj	      = $this->input->post('isj', TRUE);
			$dsjreceive = $this->input->post('dreceive', TRUE);
			if($dsjreceive!=''){
				$tmp=explode("-",$dsjreceive);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsjreceive=$th."-".$bl."-".$hr;
			}
      $esjreceive = $this->input->post('esjreceive', TRUE);
			$iarea		  = $this->input->post('iarea', TRUE);
			$jml	      = $this->input->post('jml', TRUE);
			$gaono=true;
			if($jml>0){
					$gaono=false;
			}
			if( (!$gaono)&&($dsjreceive!='') ){
				$this->db->trans_begin();
				$this->load->model('sjreceivetoko/mmaster');
				$this->mmaster->updatesjheader($isj,$iarea,$dsjreceive,$esjreceive);
				if ( ($this->db->trans_status() === FALSE) )
				{
					$this->db->trans_rollback();
				}else{
#					$this->db->trans_rollback();
					$this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update SJ Retur Toko Receive No:'.$isj;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $newsj;
					$this->load->view('nomor',$data);
				}
			}
    }
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu330')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj	= $this->input->post('isjdelete', TRUE);
			$this->load->model('sjreceivetoko/mmaster');
			$this->mmaster->delete($isj);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus SJ Retur Toko Receive No:'.$isj;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('sjrectoko');
			$data['isj']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('sjreceivetoko/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu330')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj			= $this->input->post('isjdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
  			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$this->db->trans_begin();
			$this->load->model('sjreceivetoko/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $isj, $iproductmotif);
			if ($this->db->trans_status() === FALSE)

			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
				$data['page_title'] = $this->lang->line('sjrectoko')." Update";
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$isj'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['isj'] = $isj;
				$data['isi']=$this->mmaster->baca($isj);
				$data['detail']=$this->mmaster->bacadetail($isj);
				$this->load->view('sjreceivetoko/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu330')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/sjreceivetoko/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1!='00'){
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
  			$config['total_rows'] = $query->num_rows(); 
			}else{
  			$config['total_rows'] = 0; 
      }
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('sjreceivetoko/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('sjreceivetoko/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu330')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');$data['isi']		= $this->mmaster->bacaperiode($area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(4),$cari);
			$config['base_url'] = base_url().'index.php/sjreceivetoko/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1!='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
                                   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
  			$config['total_rows'] = $query->num_rows(); 
			}else{
  			$config['total_rows'] = 0; 
      }
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sjreceivetoko/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('sjreceivetoko/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function spmb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu330')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/sjreceivetoko/cform/spmb/'.$area.'/';
			$query = $this->db->query(" select distinct(a.i_spmb) from tm_spmb a, tm_spmb_item b
							where a.i_area='$area' and a.f_spmb_cancel='f' and a.f_spmb_pemenuhan='t'
							and a.i_spmb=b.i_spmb and a.i_area=b.i_area and a.f_spmb_acc='t'
							and b.n_acc>b.n_deliver and a.f_spmb_close='f'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('sjreceivetoko/mmaster');
			$data['page_title'] = $this->lang->line('list_spmb');
			$data['area']=$area;
			$data['isi']=$this->mmaster->bacaspmb($area,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sjreceivetoko/vlistspmb', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carispmb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu330')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area = $this->input->post('iarea', FALSE);
			$config['base_url'] = base_url().'index.php/sjreceivetoko/cform/spmb/'.$area.'/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" select distinct(a.*) from tm_spmb a, tm_spmb_item b
								where a.i_area='$area' and a.f_spmb_cancel='f' and a.f_spmb_close='f'
								and a.i_spmb=b.i_spmb and a.i_area=b.i_area and a.f_spmb_acc='t' and a.f_spmb_pemenuhan='t'
								and b.n_acc>b.n_deliver and (upper(a.i_spmb)like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sjreceivetoko/mmaster');
			$data['area']=$area;
			$data['page_title'] = $this->lang->line('list_spmb');
			$data['isi']=$this->mmaster->carispmb($cari,$area,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sjreceivetoko/vlistspmb', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu330')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/sjreceivetoko/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_sjp a, tr_area b
																	where a.i_area=b.i_area
																	and (upper(a.i_sjp) like '%$cari%')
																	and a.i_area='$iarea' and not a.d_sjp_receive is null and
																	a.d_sjp >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_sjp <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpreceive');
			$this->load->model('sjreceivetoko/mmaster');
			$data['cari']		= $cari;
#			$data['dfrom']	= $dfrom;
#			$data['dto']		= $dto;
#			$data['iarea']	= $iarea;
      $data['isjp']   = '';
#			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
      $data['isi']		= $this->mmaster->bacaperiode($area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(4),$cari);

			$this->load->view('sjreceivetoko/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function hitung()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu330')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispmb		= $this->uri->segment(4);
			$query = $this->db->query("	select a.i_product as kode, a.i_product_motif as motif,
							                    a.e_product_motifname as namamotif, b.n_order as order,
							                    c.e_product_name as nama,c.v_product_retail as harga
							                    from tr_product_motif a,tr_product_price c, tm_spmb_item b
							                    where a.i_product=c.i_product 
							                    and b.i_product_motif=a.i_product_motif
							                    and c.i_product=b.i_product
                                  and c.i_price_group='00'
                                  and c.i_product_grade='A'
							                    and b.i_spmb='$ispmb' and b.n_deliver<b.n_acc order by b.n_item_no ",false);
			$data['jmlitem'] = $query->num_rows(); 
			$dsj 	= $this->uri->segment(8);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
			}
			$ispmb		  = $this->uri->segment(4);
			$dspmb		  = $this->uri->segment(5);
			$iarea		  = $this->uri->segment(6);
			$eareaname	= $this->uri->segment(7);
			$istore		  = $this->uri->segment(9);
			$isjold		  = $this->uri->segment(10);
			$data['page_title'] = $this->lang->line('sjrectoko');
			$data['isj']	= '';
			$this->load->model('sjreceivetoko/mmaster');
			$data['isi']	= "xxxxx";
			$data['dsj']	= $dsj;
			$data['ispmb']	= $ispmb;
			$data['dspmb']	= $dspmb;
			$data['iarea']	= $iarea;
			$data['istore']	= $istore;
			$data['isjold']	= $isjold;
			$data['eareaname']= $eareaname;
			$data['detail']	= $this->mmaster->product($ispmb);
			$this->load->view('sjreceivetoko/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu330')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/sjreceivetoko/cform/index/';
			$query = $this->db->query(" select a.i_sj from tm_nota a, tr_area b
																	where a.i_area=b.i_area and a.d_sj_receive is null and a.f_nota_cancel='f' 
																	and (upper(a.i_sj) like '%$cari%')
																	and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('sjrectoko');
			$this->load->model('sjreceivetoko/mmaster');
			$data['cari']		= $cari;
			$data['isj'] 	= '';
			$data['isi']		= $this->mmaster->bacaperiode($area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(4),$cari);
			$this->load->view('sjreceivetoko/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu330')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari = strtoupper($this->input->post('cari', FALSE));
      $baris = strtoupper($this->input->post('baris', FALSE));
 			if($baris=='') $baris=$this->uri->segment(4);
      if($this->uri->segment(5)!='x01'){
        if($cari=='') $cari=$this->uri->segment(5);
        $config['base_url'] = base_url().'index.php/sjreceivetoko/cform/product/'.$baris.'/'.$cari.'/';
      }else{
        $config['base_url'] = base_url().'index.php/sjreceivetoko/cform/product/'.$baris.'/x01/';
      }
			$query = $this->db->query(" select a.i_product, a.e_product_name, b.v_product_retail, c.i_product_motif, c.e_product_motifname 
                                  from tr_product a, tr_product_price b, tr_product_motif c
                                  where a.i_product=b.i_product and b.i_price_group='00'
                                  and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                                  and a.i_product=c.i_product ",false);#and a.i_product_status<>'4'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
 			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('sjreceivetoko/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$this->uri->segment(4);
 			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('sjreceivetoko/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
