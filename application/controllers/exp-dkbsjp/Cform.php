<?php
class Cform extends CI_Controller
{
	public $title 	= "Export DKB-SJP";
	public $folder 	= "exp-dkbsjp";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->model($this->folder . '/mmaster');
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu163') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->title;
			$data['folder'] 	= $this->folder;
			$data['dfrom'] 		= '';
			$data['iarea'] 		= '';
			$data['dto'] 		= '';

			$this->logger->writenew("Membuka Menu " . $this->title);

			$this->load->view($this->folder . '/vform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu163') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iarea  = $this->input->post('iarea');
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');

			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea	= $this->uri->segment(6);

			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/view/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';

			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$iuser 	= $this->session->userdata('user_id');

			if ($iarea == '00') {
				$query = $this->db->query(" select a.i_dkb from tm_dkb_sjp a, tr_area b
											where a.i_area=b.i_area
											and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
											or upper(a.i_dkb) like '%$cari%') and substr(a.i_dkb,10,2)='$iarea' and 
											a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
											a.d_dkb <= to_date('$dto','dd-mm-yyyy')", false);
			} else {
				$query = $this->db->query(" select a.i_dkb from tm_dkb_sjp a, tr_area b
											where a.i_area=b.i_area
											and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
											or upper(a.i_dkb) like '%$cari%') and /*substr(a.i_dkb,10,2)='$iarea' and*/
											a.i_area='$iarea' and 
											a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
											a.d_dkb <= to_date('$dto','dd-mm-yyyy')", false);
			}

			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] 	= $this->title;
			$data['folder'] 		= $this->folder;
			$data['cari'] 			= $cari;
			$data['dfrom'] 			= $dfrom;
			$data['dto'] 			= $dto;
			$data['iarea'] 			= $iarea;
			$data['isi'] 			= $this->mmaster->bacasemua($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari, $iuser);

			$this->logger->writenew("Membuka Menu " . $this->title . " Area : " . $iarea . " Tgl " . $dfrom . " s/d " . $dto);

			$this->load->view($this->folder . '/vformview', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu163') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea  	= $this->session->userdata('i_area');

			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea	= $this->uri->segment(6);

			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/view/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';

			$iuser 		= $this->session->userdata('user_id');

			if ($iarea == '00') {
				$query = $this->db->query(" select a.i_dkb from tm_dkb_sjp a, tr_area b
											where a.i_area=b.i_area
											and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
											or upper(a.i_dkb) like '%$cari%') and substr(a.i_dkb,10,2)='$iarea' and 
											a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
											a.d_dkb <= to_date('$dto','dd-mm-yyyy')", false);
			} else {
				$query = $this->db->query(" select a.i_dkb from tm_dkb_sjp a, tr_area b
											where a.i_area=b.i_area
											and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
											or upper(a.i_dkb) like '%$cari%') and /*substr(a.i_dkb,10,2)='$iarea' and*/
											a.i_area='$iarea' and 
											a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
											a.d_dkb <= to_date('$dto','dd-mm-yyyy')", false);
			}

			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] 	= $this->title;
			$data['folder'] 		= $this->folder;
			$data['cari']			= $cari;
			$data['dfrom']			= $dfrom;
			$data['dto']			= $dto;
			$data['iarea']			= $iarea;
			$data['isi']			= $this->mmaster->bacasemua($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari, $iuser);

			$this->logger->writenew("Cari Data Menu " . $this->title . " Keywords : " . $cari);

			$this->load->view($this->folder . '/vformview', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu163') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$idkb 	= $this->uri->segment(4);
			$iarea 	= $this->uri->segment(5);
			$iareax	= $this->uri->segment(6);

			$data = [
				'page_title' 	=> $this->title,
				'folder' 		=> $this->folder,
				'idkb' 			=> $idkb,
				'iarea' 		=> $iarea,
				'iareax' 		=> $iareax,
				// 'dfrom' 		=> $dfrom,
				// 'dto' 			=> $dto,
				'isi' 			=> $this->mmaster->baca($idkb, $iarea),
				'detail' 		=> $this->mmaster->bacadetail($idkb, $iarea),
			];

			$this->logger->writenew('Membuka Detail DKB-SJP Area ' . $iarea . ' No:' . $idkb);

			$this->load->view($this->folder . '/vformrpt', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu163') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('exp-dkb');
			$this->load->view($this->folder . '/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu163') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/area/index/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);


			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view($this->folder . '/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu163') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view($this->folder . '/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
