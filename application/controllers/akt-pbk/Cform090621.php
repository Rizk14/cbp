<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bk').' (Masuk)';
			$this->load->view('akt-pbk/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			date_default_timezone_set("Asia/Jakarta");
			#$hari_ini = date("Y-m-d");
			$now 				= date("Y-m")."-04";
			$data['page_title'] = $this->lang->line('bk').' (Masuk)'." Update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5) && $this->uri->segment(6)
			  ){
				$ikbank		= $this->uri->segment(4);
				$iperiode	= $this->uri->segment(5);
				$iarea		= $this->uri->segment(6);
				$dfrom		= $this->uri->segment(7);
				$dto		= $this->uri->segment(8);
				$lepel		= $this->uri->segment(9);
				$icoabank = $this->uri->segment(10);
				$ibank = $this->uri->segment(11);
				$this->load->model("akt-pbk/mmaster");
				$data['isi']=$this->mmaster->baca($ikbank,$iperiode,$iarea,$ibank,$icoabank);
				$data['iarea']  = $iarea;
				$data['dfrom']  = $dfrom;
				$data['dto']    = $dto;
				$data['lepel']	= $lepel;
				$data['icoabank']	= $icoabank;
				$data['ibank']	= $ibank;
				$data['now']	= $now;
########
				$query  = $this->mmaster->baca($ikbank,$iperiode,$iarea,$ibank,$icoabank);
				foreach ($query as $row){
				  $dbank=substr($row->d_bank,0,4).substr($row->d_bank,5,2);
				}
				$data['bisaedit']=false;
				$query=$this->db->query("	select i_periode from tm_periode ",false);
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $rw){
            $periode=$rw->i_periode;
				  }
				  if($periode<=$dbank){
            $data['bisaedit']=true;				  
				  }
			  }
			  if($data['bisaedit']){
			    $query=$this->db->query("	select v_bank, v_sisa from tm_kbank where i_kbank='$ikbank' and i_coa_bank='$icoabank' ",false);
          if ($query->num_rows() > 0){
	          foreach($query->result() as $row){
              if($row->v_bank==$row->v_sisa)
                $data['bisaedit']=true; 
              else 
                $data['bisaedit']=false;
            }
          }
			  }
########
 		 		$this->load->view('akt-pbk/vformupdate',$data);
			}else{
				$this->load->view('akt-pbk/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikbank  	  = $this->input->post('ikbank', TRUE);
			$iareaold   = $this->input->post('iareaold', TRUE);
			$iarea		  = $this->input->post('iarea', TRUE);
			$iperiode 	= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$irvtype  	= $this->input->post('irvtype', TRUE);
			$tah		    = substr($this->input->post('iperiodeth', TRUE),2,2);
			$bul		    = $this->input->post('iperiodebl', TRUE);
			$vbank	    = $this->input->post('vbank', TRUE);
			$vbank 	  	= str_replace(',','',$vbank);
			$vbankold   = $this->input->post('vbankold', TRUE);
			$vbankold 	= str_replace(',','',$vbankold);
			$dbank  	  = $this->input->post('dbank', TRUE);
			$ibank    	= $this->input->post('ibank', TRUE);
			$icoabank 	= $this->input->post('icoabank', TRUE);
			$icoa      	= $this->input->post('icoa', TRUE);
			$ecoaname 	= $this->input->post('ecoaname', TRUE);
			$edescription	= $this->input->post('edescription', TRUE);
			$icoaold   	= $this->input->post('icoaold', TRUE);
      $jml        = $this->input->post('jml', TRUE);
			if($dbank!=''){
				$tmp=explode("-",$dbank);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbank=$th."-".$bl."-".$hr;
				$drv=$th."-".$bl."-".$hr;
			}
			$fdebet='f';
			if (
				(isset($ikbank) && $ikbank != '') &&
				(isset($iperiode) && $iperiode != '') && 
				(isset($iarea) && $iarea != '') &&
				(isset($dbank) && $dbank != '') &&
				(isset($icoa) && $icoa != '') &&
				(isset($icoabank) && $icoabank != '')
			   )
			{
				$this->load->model('akt-pbk/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($iarea,$ikbank,$iperiode,$icoa,$vbank,$dbank,$ecoaname,$edescription,$fdebet,$irvtype,$icoabank,$vbankold,
				                       $iareaold,$icoaold);
				$nomor=$ikbank;
###########posting##########
          $eremark		= $edescription;
          $fclose			= 'f';
			    $this->mmaster->inserttransheader($ikbank,$iarea,$eremark,$fclose,$dbank,$icoabank);
			    if($fdebet=='t'){
				    $accdebet		  = $icoa;
				    $namadebet		= $ecoaname;
				    $acckredit		= $icoabank;
				    $namakredit		= $this->mmaster->namaacc($acckredit);
			    }else{
				    $accdebet		  = $icoabank;
				    $namadebet		= $this->mmaster->namaacc($accdebet);
				    $acckredit		= $icoa;
				    $namakredit		= $ecoaname;
			    }
			    $this->mmaster->inserttransitemdebet($accdebet,$ikbank,$namadebet,'t','t',$iarea,$eremark,$vbank,$dbank,$iarea,$icoabank);
			    $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vbank);
			    $this->mmaster->inserttransitemkredit($acckredit,$ikbank,$namakredit,'f','t',$iarea,$eremark,$vbank,$dbank,$iarea,$icoabank);
			    $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vbank);
			    $this->mmaster->insertgldebet($accdebet,$ikbank,$namadebet,'t',$iarea,$vbank,$dbank,$eremark,$icoabank);
			    $this->mmaster->insertglkredit($acckredit,$ikbank,$namakredit,'f',$iarea,$vbank,$dbank,$eremark,$icoabank);
###########end of posting##########
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Update Bank Masuk No:'.$ikbank.' Area:'.$iarea;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );
#					$this->db->trans_rollback();
					$this->db->trans_commit();
					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-pbk/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
  			$query = $this->db->query("select * from tr_area",false);
      }else{
        $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
	                        			   or i_area = '$area4' or i_area = '$area5'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-pbk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-pbk/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/akt-pbk/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-pbk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-pbk/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function coa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-pbk/cform/coa/index/';
			$query = $this->db->query("select * from tr_coa where not (i_coa like '111.4%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('akt-pbk/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->bacacoa($config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-pbk/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricoa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-pbk/cform/coa/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_coa
						    									where not (i_coa like '111.4%') and 
						    									(upper(i_coa) like '6%$cari%' or (upper(e_coa_name) like '%$cari%'))",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-pbk/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->caricoa($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-pbk/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
