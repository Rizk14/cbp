<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu354')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('laporanpenjualancabang');
			$data['iperiode']	= '';
			$this->load->view('laporanpenjualancabang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu354')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari		  = strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$iarea	  = $this->input->post('iarea');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			if($iarea=='') $iarea=$this->uri->segment(5);
      if($iperiode!=''){
        $this->db->query("update tm_dgu_no set e_periode='$iperiode' where i_modul='VPJ'", false);
      }
			$this->load->model('laporanpenjualancabang/mmaster');
			$data['page_title'] = $this->lang->line('laporanpenjualancabang');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->baca($iperiode,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Laporan Penjualan Area : '.$iarea.' Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('laporanpenjualancabang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu354')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$dto	= $this->input->post('dto');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/laporanpenjualancabang/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_target a, tr_area b
										              where a.i_periode = '$iperiode'
										              and a.i_area=b.i_area
										              and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '50';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('laporanpenjualancabang/mmaster');
			$data['page_title'] = $this->lang->line('laporanpenjualancabang');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('laporanpenjualancabang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu354')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/laporanpenjualancabang/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('laporanpenjualancabang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('laporanpenjualancabang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function persales()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu354')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('laporanpenjualancabang/mmaster');
      $iperiode = $this->uri->segment(4);
      $iarea    = $this->uri->segment(5);
			$data['page_title'] = $this->lang->line('laporanpenjualancabang');
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacapersales($iperiode,$iarea);
			$this->load->view('laporanpenjualancabang/vformviewpersales',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
