<?php
class Cform extends CI_Controller
{
   public $title  = 'Export Laporan Gudang Per Divisi Per Area Nasional (SJ Pusat)';
   public $folder = 'exp-lapgudperdivareanaspst';

   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationx');
      require_once("php/fungsi.php");
      $this->load->model($this->folder . '/mmaster');
   }

   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu344') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data = [
            'page_title' => $this->title,
            'folder'     => $this->folder,
            'productgrp' => $this->mmaster->bacagroupbarang(),
            'iperiode'   => '',
            'b'          => '',
            'nb'         => '',
            'r'          => '',
         ];

         $this->load->view($this->folder . '/vmainform', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu344') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data['page_title'] = $this->title;

         $this->load->view($this->folder . '/vinsert_fail', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
   function store()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu344') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/exp-lapgudperdivareanaspst/cform/store/index/';
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
            $query = $this->db->query("select distinct(c.i_store) as i_store
                                  from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store");
         } else {
            $query = $this->db->query("select distinct(c.i_store) as i_store
													         from tr_store_location a, tr_store b, tr_area c
													         where a.i_store = b.i_store and b.i_store=c.i_store
													         and (c.i_area = '$area1' or c.i_area = '$area2' or
													         c.i_area = '$area3' or c.i_area = '$area4' or
													         c.i_area = '$area5')");
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);


         $data['page_title'] = $this->lang->line('list_store');
         $data['isi'] = $this->mmaster->bacastore($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
         $this->load->view($this->folder . '/vliststore', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
   function caristore()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu344') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/exp-lapgudperdivareanaspst/cform/store/index/';
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
            $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')", false);
         } else {
            $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')
                                    and (c.i_area = '$area1' or c.i_area = '$area2' or
                                    c.i_area = '$area3' or c.i_area = '$area4' or
                                    c.i_area = '$area5')", false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $data['page_title'] = $this->lang->line('list_store');
         $data['isi'] = $this->mmaster->caristore($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
         $this->load->view($this->folder . '/vliststore', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu344') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $iperiode   = $this->input->post('iperiode');
         $b          = $this->input->post('chkbx');
         $nb         = $this->input->post('chknbx');
         $r          = $this->input->post('chkrx');
         $jml        = $this->input->post('jml', TRUE);
         $judul      = $this->input->post('judul', TRUE);

         if ($b == '') $b = $this->uri->segment(4);
         if ($nb == '') $nb = $this->uri->segment(5);
         if ($r == '') $r = $this->uri->segment(6);

         $c    = substr($iperiode, 0, 4);
         $d    = substr($iperiode, 4, 2);
         $peri = mbulan($d) . " - " . $c;

         // if ($b == 'qqq') {
         //    $judul = 'Bedding';
         // } elseif ($nb == 'qqq') {
         //    $judul = 'Non Bedding';
         // } elseif ($r == 'qqq') {
         //    $judul = 'Reguler';
         // }

         for ($i = 0; $i < $jml; $i++) {
            if ($this->input->post('grp' . $i . 'x', TRUE) == "qqq") {
               $iproductgroup = $this->input->post('grp' . $i, TRUE);
            }
         }

         $this->load->library('PHPExcel');
         $this->load->library('PHPExcel/IOFactory');
         $objPHPExcel = new PHPExcel();
         $objPHPExcel->getProperties()->setTitle("Laporan Gudang Per Divisi Per Area Nasional khusus SJ Pusat")->setDescription(NmPerusahaan);
         $objPHPExcel->setActiveSheetIndex(0);
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            array(
               'font' => array(
                  'name'   => 'Arial',
                  'bold'  => true,
                  'italic' => false,
                  'size'  => 10
               ),
               'alignment' => array(
                  'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                  'vertical'  => Style_Alignment::VERTICAL_CENTER,
                  'wrap'      => true
               )
            ),
            'A2:A4'
         );
         $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
         $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
         $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
         $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
         $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
         $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
         $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
         $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);

         $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan Gudang Per Divisi Per Area Nasional khusus SJ Pusat Periode ' . $peri);
         $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 12, 2);
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            array(
               'font' => array(
                  'name'   => 'Arial',
                  'bold'  => true,
                  'italic' => false,
                  'size'  => 10
               ),
               'alignment' => array(
                  'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                  'vertical'  => Style_Alignment::VERTICAL_CENTER,
                  'wrap'      => true
               )
            ),
            'A5:L5'
         );


         $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
         $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               )
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Area');
         $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Gudang');
         $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Lokasi');
         $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Kode');
         $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Nama');
         $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Harga');
         $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Penjualan');
         $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Saldo Akhir');
         $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Stock Opname');
         $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Nilai Penjualan');
         $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Nilai Saldo Akhir');
         $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $i = 6;
         $j = 6;
         $no = 0;
         $totjual = 0;
         $totsaldo = 0;
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            array(
               'font' => array(
                  'name'   => 'Arial',
                  'bold'   => false,
                  'italic' => false,
                  'size'   => 10
               )
            ),
            'A' . $i . ':L10000' #.$i
         );
         $blth = substr($iperiode, 2, 4);
         $sisj = '%-' . $blth . '-00%';

         if ($iperiode > '201512') {
            // if ($b == 'qqq') {
            $query = $this->db->query("xselect a.i_product as i_product,b.e_product_name, d.i_area, d.i_store, d.i_store_location, 
                                       b.v_product_retail,
                                       sum(n_saldo_akhir) as n_saldo_akhir, sum(n_git_penjualan) as n_git_penjualan,
                                       sum(n_saldo_stockopname) as n_saldo_stockopname, sum(n_mutasi_git) as n_mutasi_git,
                                       sum(n_deliver) as n_mutasi_penjualan
                                       from tr_product b, f_mutasi_stock_daerah_all_saldoakhir('$iperiode') a, tm_nota_item c, tm_spb d, tm_nota f, tr_product_type g
                                       where a.e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
                                       /* and b.i_product_type='03' */
                                       AND g.i_product_group = '$iproductgroup' and a.i_store='AA'
                                       AND b.i_product_type = g.i_product_type 
                                       and c.i_sj like '$sisj' and c.i_product=a.i_product and c.i_product_grade=a.i_product_grade 
                                       and c.i_product_motif=a.i_product_motif and c.i_area=d.i_area
                                       and a.i_store=d.i_store and a.i_store_location=d.i_store_location and d.f_spb_cancel='f'
                                       and c.i_sj=f.i_sj and c.i_area=f.i_area and f.f_nota_cancel='f' and d.i_sj=f.i_sj 
                                       and d.i_area=f.i_area 
                                       group by d.i_area, d.i_store, d.i_store_location, a.i_product, e_mutasi_periode, 
                                       b.e_product_name, b.v_product_retail
                                       order by d.i_area, d.i_store, d.i_store_location, a.i_product", false);
            // } elseif ($nb == 'qqq') {
            //    $query = $this->db->query("select a.i_product as i_product,b.e_product_name, d.i_area, d.i_store, d.i_store_location, 
            //                                   b.v_product_retail,
            //                                   sum(n_saldo_akhir) as n_saldo_akhir, sum(n_git_penjualan) as n_git_penjualan,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_deliver) as n_mutasi_penjualan
            //                                   from tr_product b, f_mutasi_stock_daerah_all_saldoakhir('$iperiode') a, tm_nota_item c, tm_spb d, tm_nota f
            //                                   where a.e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
            //                                   and b.i_product_type='08' and a.i_store='AA'
            //                                   and c.i_sj like '$sisj' and c.i_product=a.i_product and c.i_product_grade=a.i_product_grade 
            //                                   and c.i_product_motif=a.i_product_motif and c.i_area=d.i_area
            //                                   and a.i_store=d.i_store and a.i_store_location=d.i_store_location and d.f_spb_cancel='f'
            //                                   and c.i_sj=f.i_sj and c.i_area=f.i_area and f.f_nota_cancel='f' and d.i_sj=f.i_sj 
            //                                   and d.i_area=f.i_area 
            //                                   group by d.i_area, d.i_store, d.i_store_location, a.i_product, e_mutasi_periode, 
            //                                   b.e_product_name, b.v_product_retail
            //                                   order by d.i_area, d.i_store, d.i_store_location, a.i_product", false);
            // } elseif ($r == 'qqq') {
            //    $query = $this->db->query("select a.i_product as i_product,b.e_product_name, d.i_area, d.i_store, d.i_store_location, 
            //                                   b.v_product_retail,
            //                                   sum(n_saldo_akhir) as n_saldo_akhir, sum(n_git_penjualan) as n_git_penjualan,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_deliver) as n_mutasi_penjualan
            //                                   from tr_product b, f_mutasi_stock_daerah_all_saldoakhir('$iperiode') a, tm_nota_item c, tm_spb d, tm_nota f
            //                                   where a.e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
            //                                   and b.i_product_type<>'08' and b.i_product_type<>'03' and a.i_store='AA'
            //                                   and c.i_sj like '$sisj' and c.i_product=a.i_product and c.i_product_grade=a.i_product_grade 
            //                                   and c.i_product_motif=a.i_product_motif and c.i_area=d.i_area
            //                                   and a.i_store=d.i_store and a.i_store_location=d.i_store_location and d.f_spb_cancel='f'
            //                                   and c.i_sj=f.i_sj and c.i_area=f.i_area and f.f_nota_cancel='f' and d.i_sj=f.i_sj 
            //                                   and d.i_area=f.i_area 
            //                                   group by d.i_area, d.i_store, d.i_store_location, a.i_product, e_mutasi_periode, 
            //                                   b.e_product_name, b.v_product_retail
            //                                   order by d.i_area, d.i_store, d.i_store_location, a.i_product", false);
            // }
         } else {
            // if ($b == 'qqq') {
            $query = $this->db->query("select a.i_product as i_product,b.e_product_name, d.i_area, d.i_store, d.i_store_location, 
                                       b.v_product_retail,
                                       sum(n_saldo_akhir) as n_saldo_akhir, sum(n_git_penjualan) as n_git_penjualan,
                                       sum(n_saldo_stockopname) as n_saldo_stockopname, sum(n_mutasi_git) as n_mutasi_git,
                                       sum(n_deliver) as n_mutasi_penjualan
                                       from tr_product b, f_mutasi_stock_daerah_all('$iperiode') a, tm_nota_item c, tm_spb d, tm_nota f, tr_product_type g
                                       where a.e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
                                       /* and b.i_product_type='03' */
                                       AND g.i_product_group = '$iproductgroup' and a.i_store='AA'
                                       AND b.i_product_type = g.i_product_type 
                                       and c.i_sj like '$sisj' and c.i_product=a.i_product and c.i_product_grade=a.i_product_grade 
                                       and c.i_product_motif=a.i_product_motif and c.i_area=d.i_area
                                       and a.i_store=d.i_store and a.i_store_location=d.i_store_location and d.f_spb_cancel='f'
                                       and c.i_sj=f.i_sj and c.i_area=f.i_area and f.f_nota_cancel='f' and d.i_sj=f.i_sj 
                                       and d.i_area=f.i_area 
                                       group by d.i_area, d.i_store, d.i_store_location, a.i_product, e_mutasi_periode, 
                                       b.e_product_name, b.v_product_retail
                                       order by d.i_area, d.i_store, d.i_store_location, a.i_product", false);
            // } elseif ($nb == 'qqq') {
            //    $query = $this->db->query("select a.i_product as i_product,b.e_product_name, d.i_area, d.i_store, d.i_store_location, 
            //                                   b.v_product_retail,
            //                                   sum(n_saldo_akhir) as n_saldo_akhir, sum(n_git_penjualan) as n_git_penjualan,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_deliver) as n_mutasi_penjualan
            //                                   from tr_product b, f_mutasi_stock_daerah_all('$iperiode') a, tm_nota_item c, tm_spb d, tm_nota f
            //                                   where a.e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
            //                                   and b.i_product_type='08' and a.i_store='AA'
            //                                   and c.i_sj like '$sisj' and c.i_product=a.i_product and c.i_product_grade=a.i_product_grade 
            //                                   and c.i_product_motif=a.i_product_motif and c.i_area=d.i_area
            //                                   and a.i_store=d.i_store and a.i_store_location=d.i_store_location and d.f_spb_cancel='f'
            //                                   and c.i_sj=f.i_sj and c.i_area=f.i_area and f.f_nota_cancel='f' and d.i_sj=f.i_sj 
            //                                   and d.i_area=f.i_area 
            //                                   group by d.i_area, d.i_store, d.i_store_location, a.i_product, e_mutasi_periode, 
            //                                   b.e_product_name, b.v_product_retail
            //                                   order by d.i_area, d.i_store, d.i_store_location, a.i_product", false);
            // } elseif ($r == 'qqq') {
            //    $query = $this->db->query("select a.i_product as i_product,b.e_product_name, d.i_area, d.i_store, d.i_store_location, 
            //                                   b.v_product_retail,
            //                                   sum(n_saldo_akhir) as n_saldo_akhir, sum(n_git_penjualan) as n_git_penjualan,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_deliver) as n_mutasi_penjualan
            //                                   from tr_product b, f_mutasi_stock_daerah_all('$iperiode') a, tm_nota_item c, tm_spb d, tm_nota f
            //                                   where a.e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
            //                                   and b.i_product_type<>'08' and b.i_product_type<>'03' and a.i_store='AA'
            //                                   and c.i_sj like '$sisj' and c.i_product=a.i_product and c.i_product_grade=a.i_product_grade 
            //                                   and c.i_product_motif=a.i_product_motif and c.i_area=d.i_area
            //                                   and a.i_store=d.i_store and a.i_store_location=d.i_store_location and d.f_spb_cancel='f'
            //                                   and c.i_sj=f.i_sj and c.i_area=f.i_area and f.f_nota_cancel='f' and d.i_sj=f.i_sj 
            //                                   and d.i_area=f.i_area 
            //                                   group by d.i_area, d.i_store, d.i_store_location, a.i_product, e_mutasi_periode, 
            //                                   b.e_product_name, b.v_product_retail
            //                                   order by d.i_area, d.i_store, d.i_store_location, a.i_product", false);
            // }
         }

         if ($query->num_rows() > 0) {
            $no       = 1;
            $store    = '';
            $storeloc = '';

            foreach ($query->result() as $raw) {
               if (($raw->i_store != $store && $store != '') || ($raw->i_store_location != $storeloc && $storeloc != '')) {
                  $no = 1;
               }
               $loc = ($raw->i_store_location == 'PB') ? 'Konsinyasi' : 'Reguler';
               #                  if($raw->i_store!=$store && $store==''){
               $prodname   = $raw->e_product_name;
               $mutasijual = $raw->n_mutasi_penjualan;
               $saldoakhir = $raw->n_saldo_akhir - $raw->n_git_penjualan - $raw->n_mutasi_git;
               $harga      = $raw->v_product_retail;
               $so         = $raw->n_saldo_stockopname + $raw->n_git_penjualan + $raw->n_mutasi_git;

               $nilaijual  = $mutasijual * $harga;
               $nilaisaldo = $saldoakhir * $harga;
               $totjual    = $totjual + $nilaijual;
               $totsaldo   = $totsaldo + $nilaisaldo;

               $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $no, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $raw->i_area, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $raw->i_store, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $loc, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $raw->i_product, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $raw->e_product_name, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $harga, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $mutasijual, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $saldoakhir, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $so, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . $i, $nilaijual, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('L' . $i, $nilaisaldo, Cell_DataType::TYPE_NUMERIC);
               /*
                  }elseif($raw->i_store!=$store && $store!=''){
                    $i++;
					          $j++;
                    $prodname=$raw->e_product_name;
                    $mutasijual=$raw->n_mutasi_penjualan;
                    $saldoakhir=$raw->n_saldo_akhir;
                    $harga=$raw->v_product_retail;
                    $so=$raw->n_saldo_stockopname;
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $no, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $raw->i_store, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $raw->i_store_location, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $raw->i_product, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $raw->e_product_name, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $harga, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $mutasijual, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $saldoakhir, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $so, Cell_DataType::TYPE_NUMERIC);
                  }elseif($raw->i_store==$store && $store!='' && $raw->i_store_location!=$storeloc){
                    $i++;
					          $j++;
                    $prodname=$raw->e_product_name;
                    $mutasijual=$raw->n_mutasi_penjualan;
                    $saldoakhir=$raw->n_saldo_akhir;
                    $harga=$raw->v_product_retail;
                    $so=$raw->n_saldo_stockopname;
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $no, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $raw->i_store, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $raw->i_store_location, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $raw->i_product, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $raw->e_product_name, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $harga, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $mutasijual, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $saldoakhir, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $so, Cell_DataType::TYPE_NUMERIC);
                  }else{
                    $prodname=$raw->e_product_name;
                    $mutasijual=$raw->n_mutasi_penjualan;
                    $saldoakhir=$raw->n_saldo_akhir;
                    $harga=$raw->v_product_retail;
                    $so=$raw->n_saldo_stockopname;
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $no, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $raw->i_store, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $raw->i_store_location, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $raw->i_product, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $raw->e_product_name, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $harga, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $mutasijual, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $saldoakhir, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $so, Cell_DataType::TYPE_NUMERIC);
                  }
*/
               $store = $raw->i_store;
               $storeloc = $raw->i_store_location;
               $i++;
               $j++;
               $no++;
            }
            $x = $i - 1;
         }
         $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, 'TOTAL');
         $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $totjual);
         $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $totsaldo);
         $objPHPExcel->getActiveSheet()->getStyle('K' . $i . ':L' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER);
         #$objPHPExcel->getActiveSheet()->getStyle('G6:H'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);

         $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

         $nama = 'lapgudnaspst-' . $judul . '-' . $iperiode . '.xls';

         if (file_exists('excel/00/' . $nama)) {
            @chmod('excel/00/' . $nama, 0777);
            @unlink('excel/00/' . $nama);
         }
         $objWriter->save('excel/00/' . $nama);
         @chmod('excel/00/' . $nama, 0777);

         $this->logger->writenew('Export Laporan Gudang Per Divisi Gudang Nasional (SJ Pusat) Periode:' . $iperiode);

         $data['sukses']   = true;
         $data['inomor']   = $nama;
         $data['folder']   = "excel/00";

         $this->load->view('nomorurl', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function get_group()
   {
      header("Content-Type: application/json", true);

      $query  = array(
         'detail' => $this->mmaster->bacagroupbarang()->result_array()
      );
      echo json_encode($query);
   }
}
