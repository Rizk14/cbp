<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu389')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnamehutangnota');
			$data['isupp']='';
			$data['dto']='';
      $data['nt']='';
      $data['jt']='';
			$this->load->view('opnnotahutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu389')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	= strtoupper($this->input->post('cari'));
      $nt   = $this->input->post('chkntx');
      $jt   = $this->input->post('chkjtx');
			$dto	= $this->input->post('dto');
			$isupp= $this->input->post('isupplier');
			if($dto=='') $dto=$this->uri->segment(4);
			if($nt=='') $nt=$this->uri->segment(5);
			if($jt=='') $jt=$this->uri->segment(6);
			if($isupp=='') $isupp	= $this->uri->segment(7);
/*
			$config['base_url'] = base_url().'index.php/opnnotahutang/cform/view/'.$dto.'/'.$nt.'/'.$jt.'/'.$isupp.'/';
      if($nt=='qqq'){
			  $query = $this->db->query(" select a.i_dtap from tr_supplier b, tm_dtap a
                                    left join tm_pelunasanap_item d on(a.i_dtap=d.i_dtap)
                                    left join tm_pelunasanap e on(d.i_pelunasanap=e.i_pelunasanap and d.d_bukti=e.d_bukti and d.i_area=e.i_area 
                                    and e.f_pelunasanap_cancel='f')
                                    where a.i_supplier=b.i_supplier
                                    and not a.i_dtap isnull
                                    and (upper(a.i_dtap) like '%$cari%' 
                                    or upper(a.i_supplier) like '%$cari%' 
                                    or upper(b.e_supplier_name) like '%$cari%')
                                    and a.i_supplier='$isupp' and
                                    a.d_dtap <= to_date('$dto','dd-mm-yyyy')
                                    and a.v_sisa>0
                                    and a.f_dtap_cancel='f'",false);
      }elseif($jt=='qqq'){
			  $query = $this->db->query(" select a.i_dtap from tr_supplier b, tm_dtap a
                                    left join tm_pelunasanap_item d on(a.i_dtap=d.i_dtap)
                                    left join tm_pelunasanap e on(d.i_pelunasanap=e.i_pelunasanap and d.d_bukti=e.d_bukti and d.i_area=e.i_area 
                                                                and e.f_pelunasanap_cancel='f')
																	  where a.i_supplier=b.i_supplier 
                                    and a.i_salesman=c.i_salesman
																	  and not a.i_dtap isnull
																	  and (upper(a.i_dtap) like '%$cari%' 
																		  or upper(a.i_supplier) like '%$cari%' 
																		  or upper(b.e_supplier_name) like '%$cari%')
																	  and a.i_supplier='$isupp' and
																	  a.d_jatuh_tempo <= to_date('$dto','dd-mm-yyyy')
                                    and a.v_sisa>0
                                    and a.f_dtap_cancel='f'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
*/
			$this->load->model('opnnotahutang/mmaster');
      if($nt=='qqq'){
	  		$data['page_title'] = $this->lang->line('opnamehutangnota').' per tanggal nota';
      }else{
  			$data['page_title'] = $this->lang->line('opnamehutangnota').' per tanggal jatuh tempo';
      }
			$data['cari']		= $cari;
			$data['keyword']= '';	
			$data['dto']		= $dto;
			$data['isupp']	= $isupp;
      $data['nt']     = $nt;
      $data['jt']     = $jt;
#			$data['isi']		= $this->mmaster->bacaperiode($isupp,$dto,$config['per_page'],$this->uri->segment(8),$cari,$nt,$jt);
			$data['isi']		= $this->mmaster->bacaperiode($isupp,$dto,$cari,$nt,$jt);
			$this->load->view('opnnotahutang/vformview',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu389')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnamehutangnota');
			$this->load->view('opnnotahutang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu389')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($cari=='') $cari	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/opnnotahutang/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_dtap a, tr_customer b
										where a.i_customer=b.i_customer 
										and not a.i_dtap isnull
										and (upper(a.i_dtap) like '%$cari%' 
										  or upper(a.i_spb) like '%$cari%' 
										  or upper(a.i_customer) like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_dtap <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('opnnotahutang/mmaster');
			$data['page_title'] = $this->lang->line('opnamehutangnota');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('opnnotahutang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu389')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$keyword = strtoupper($this->uri->segment(7));	
			$config['base_url'] = base_url().'index.php/opnnotahutang/cform/cariperpages/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$keyword.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_dtap a, tr_customer b
										where a.i_customer=b.i_customer 
										and not a.i_dtap isnull
										and (upper(a.i_dtap) like '%$keyword%' 
										  or upper(a.i_spb) like '%$keyword%' 
										  or upper(a.i_customer) like '%$keyword%' 
										  or upper(b.e_customer_name) like '%$keyword%')
										and a.i_area='$iarea' and
										a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_dtap <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('opnnotahutang/mmaster');
			$data['page_title'] = $this->lang->line('opnamehutangnota');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$keyword);
			$this->load->view('opnnotahutang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu389')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opnnotahutang/cform/supplier/index/';
  		$query = $this->db->query("select * from tr_supplier",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('opnnotahutang/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('opnnotahutang/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu389')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opnnotahutang/cform/supplier/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
 			$query = $this->db->query("select * from tr_supplier where (upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('opnnotahutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('opnnotahutang/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
     	 ($this->session->userdata('menu389')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dto	    = $this->input->post('dto');
			$iarea= $this->input->post('iarea');
      if($iarea=='')$icustomer=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$this->load->model('opnnotahutang/mmaster');
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$data['dto']  = $dto;
			$data['iarea']= $iarea;
			$data['page_title'] = $this->lang->line('printopnamehutangnota');
			$data['isi']=$this->mmaster->baca($iarea,$dto);
			$data['user']	= $this->session->userdata('user_id');
      $sess = $this->session->userdata('session_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
	    $query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
		    $now	  = $row['c'];
	    }
	    $pesan='Cetak Opname dtap Per Area sampai tanggal'.$dto.' Area:'.$iarea;
	    $this->load->model('logger');
	    $this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('opnnotahutang/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
