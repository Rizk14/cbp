<?php 
/**
 * Description of chart
 *
 * @author  Freddy Yuswanto
 * @link    http://www.kohaci.com
 */
class Chart extends CI_Controller {

    public function  __construct() {
        parent::__construct();
        $this->load->helper(array('url','fusioncharts')) ;
    }

	public function index() {
		return $this->multi() ;
	}
	
    public function single() {
        $graph_swfFile      = base_url().'flash/FCF_Column3D.swf' ;
        $graph_caption      = 'Harga Produk' ;
        $graph_numberPrefix = 'Rp.' ;
        $graph_title        = 'Penjualan Produk' ;
        $graph_width        = 450 ;
        $graph_height       = 250 ;

        // Store Name of Products
        $arrData[0][1] = "Product A/B";
        $arrData[1][1] = "Product B";
        $arrData[2][1] = "Product C";
        $arrData[3][1] = "Product D";
        $arrData[4][1] = "Product E";
        $arrData[5][1] = "Product F";

        //Store sales data
        $arrData[0][2] = 567500;
        $arrData[1][2] = 815300;
        $arrData[2][2] = 556800;
        $arrData[3][2] = 734500;
        $arrData[4][2] = 676800;
        $arrData[5][2] = 648500;

        $strXML = "<graph caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' formatNumberScale='0' decimalPrecision='0'>";

        //Convert data to XML and append
	foreach ($arrData as $arSubData) {
            $strXML .= "<set name='" . $arSubData[1] . "' value='" . $arSubData[2] . "' color='".getFCColor()."' />";
        }
	//Close <chart> element
	$strXML .= "</graph>";
        
        $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);

        $this->load->view('chart_view',$data) ;
    }

    public function multi() {
        $graph_swfFile      = base_url().'flash/FCF_MSColumn2D.swf' ;
        $graph_caption      = 'Harga Produk' ;
        $graph_numberPrefix = 'Rp.' ;
        $graph_title        = 'Penjualan Produk' ;
        $graph_width        = 450 ;
        $graph_height       = 250 ;

        // Store Category
        $category[0] = "Perusahaan A";
        $category[1] = "Perusahaan B";
        $category[2] = "Perusahaan C";
        $category[3] = "Perusahaan D";
        $category[4] = "Perusahaan E";

        // Store data set
        $dataset[0] = 'Pakaian' ;
        $dataset[1] = 'Sepatu' ;
        $dataset[2] = 'Topi' ;

        //Store data 1
        $arrData['Pakaian'][0] = 30;
        $arrData['Pakaian'][1] = 26;
        $arrData['Pakaian'][2] = 29;
        $arrData['Pakaian'][3] = 31;
        $arrData['Pakaian'][4] = 34;

        //Stire data 2
        $arrData['Sepatu'][0] = 67;
        $arrData['Sepatu'][1] = 98;
        $arrData['Sepatu'][2] = 79;
        $arrData['Sepatu'][3] = 73;
        $arrData['Sepatu'][4] = 80;

        //Stire data 3
        $arrData['Topi'][0] = 27;
        $arrData['Topi'][1] = 25;
        $arrData['Topi'][2] = 28;
        $arrData['Topi'][3] = 26;
        $arrData['Topi'][4] = 10;

        $strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='100' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='0' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."'>" ;

        //Convert category to XML and append
        $strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
        foreach ($category as $c) {
            $strXML .= "<category name='".$c."'/>" ;
        }
        $strXML .= "</categories>" ;

        //Convert dataset and data to XML and append
        foreach ($dataset as $set) {
            $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
            foreach ($arrData[$set] as $d) {
                $strXML .= "<set value='".$d."'/>" ;
            }
            $strXML .= "</dataset>" ;
        }

	//Close <chart> element
	$strXML .= "</graph>";

        $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);

        $this->load->view('chart_view',$data) ;
    }
}
