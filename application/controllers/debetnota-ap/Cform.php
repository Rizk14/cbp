<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu519')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('debetnota-ap');
			$this->load->model('debetnota-ap/mmaster');
			$data['idnap']='';
			//$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('debetnota-ap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu519')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('debetnota-ap');
			$this->load->view('debetnota-ap/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu519')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('debetnota-ap')." update";
		if(
				$this->uri->segment(4) && $this->uri->segment(5) && $this->uri->segment(6)
			  ){
				$this->load->model('debetnota-ap/mmaster');
				$data['idnap'] 		= $this->uri->segment(4);
				$data['nknyear']= $this->uri->segment(5);
				$data['isupplier'] 	= trim($this->uri->segment(6));
				$dfrom					= $this->uri->segment(7);
				$dto 						= $this->uri->segment(8);
				$data['dfrom'] 	= $this->uri->segment(7);
				$data['dto']	 	= $this->uri->segment(8);
				$idnap 					= $this->uri->segment(4);
				$nknyear				= $this->uri->segment(5);
				$isupplier 					= $this->uri->segment(6);
				$isupplier = str_replace('%20', '', $isupplier); 
				$data['isi']		= $this->mmaster->bacadnap($idnap,$nknyear,$isupplier);
		 		$this->load->view('debetnota-ap/vformupdate',$data);
			}else{
				$this->load->view('debetnota-ap/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu519')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isupplier 	    = $this->input->post('isupplier', TRUE);
			$idnap 			= $this->input->post('idnap', TRUE);
			$irefference	= $this->input->post('irefference', TRUE);
			$drefference	= $this->input->post('drefference', TRUE);
			if($drefference!=''){
				$tmp=explode("-",$drefference);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$drefference=$th."-".$bl."-".$hr;
			}
			$ddnap			= $this->input->post('ddnap', TRUE);
			if($ddnap!=''){
				$tmp=explode("-",$ddnap);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$ddnap=$th."-".$bl."-".$hr;
				$nknyear=$th;
			}
			$fcetak		= 'f';
			$fmasalah	= $this->input->post('fmasalah', TRUE);
			if($fmasalah=='')
				$fmasalah='f';
			else
				$fmasalah='t';
			$finsentif	= $this->input->post('finsentif', TRUE);
			if($finsentif=='')
				$finsentif='f';
			else
				$finsentif='t';
			$vnetto		= $this->input->post('vnetto', TRUE);
			$vnetto		= str_replace(',','',$vnetto);
			$vsisa		= $vnetto;
			$vgross		= $this->input->post('vgross', TRUE);
			$vgross		= str_replace(',','',$vgross);
			$vdiscount	= $this->input->post('vdiscount', TRUE);
			$vdiscount	= str_replace(',','',$vdiscount);
			$eremark	= $this->input->post('eremark', TRUE);
			if (
				(isset($irefference) && $irefference != '') && 
				(isset($isupplier) && $isupplier != '')
			   )
			{
				$this->load->model('debetnota-ap/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update(	$isupplier,$idnap,$irefference,$ddnap,$nknyear,$fcetak,$fmasalah,
										$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference);
			}
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Update Debet Nota AP No:'.$idnap.' Area:'.$isupplier;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $idnap;
				$this->load->view('nomor',$data);
			}
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu519')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikn		= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$this->load->model('debetnota-ap/mmaster');
			$this->mmaster->delete($ikn,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus Debet Nota No:'.$ikn.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('debetnota-ap');
			$data['isi']=$this->mmaster->bacasemua();
			$data['idn']='';
			$this->load->view('debetnota-ap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu519')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/debetnota-ap/cform/supplier/index/';
			$query = $this->db->query("select * from tr_supplier ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('debetnota-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('debetnota-ap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu519')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/debetnota-ap/cform/supplier/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_supplier
									   	where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('debetnota-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('debetnota-ap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu519')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isupplier 			= $this->input->post('isupplier', TRUE);
			$idnap 			= $this->input->post('idnap', TRUE);
			$irefference	= $this->input->post('irefference', TRUE);
			$drefference	= $this->input->post('drefference', TRUE);
			if($drefference!=''){
				$tmp=explode("-",$drefference);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$drefference=$th."-".$bl."-".$hr;
			}
			$ddnap			= $this->input->post('ddnap', TRUE);
			if($ddnap!=''){
				$tmp=explode("-",$ddnap);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$ddnap=$th."-".$bl."-".$hr;
				$nknyear=$th;
			}
			$fcetak		= 'f';
			$fmasalah	= $this->input->post('fmasalah', TRUE);
			if($fmasalah=='')
				$fmasalah='f';
			else
				$fmasalah='t';
			$finsentif	= $this->input->post('finsentif', TRUE);
			if($finsentif=='')
				$finsentif='f';
			else
				$finsentif='t';
			$vnetto		= $this->input->post('vnetto', TRUE);
			$vnetto		= str_replace(',','',$vnetto);
			$vsisa		= $vnetto;
			$vgross		= $this->input->post('vgross', TRUE);
			$vgross		= str_replace(',','',$vgross);
			$vdiscount	= $this->input->post('vdiscount', TRUE);
			$vdiscount	= str_replace(',','',$vdiscount);
			$eremark	= $this->input->post('eremark', TRUE);
			if (
				(isset($irefference) && $irefference != '') && 
				(isset($isupplier) && $isupplier != '')
			   )
			{
				$this->load->model('debetnota-ap/mmaster');
				$idnap = $this->mmaster->runningnumberkn($nknyear,$isupplier);
				$this->db->trans_begin();
				$this->mmaster->insert(	$isupplier,$idnap,$irefference,$ddnap,$nknyear,$fcetak,$fmasalah,
										$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input Debet Nota AP No:'.$idnap.' Supplier:'.$isupplier;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $idnap;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu519')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
      $cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/debetnota-ap/cform/customer/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_customer a
                            left join tr_customer_area b on (a.i_customer=b.i_customer and a.i_area=b.i_area)
                            left join tr_area c on (a.i_area=c.i_area)
                            left join tr_customer_salesman d on(a.i_customer=d.i_customer)
                            where a.i_customer like '%$cari%' and a.i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('debetnota-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('debetnota-ap/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu519')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/debetnota-ap/cform/customer/'.$iarea.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_customer
									   	where i_area='$iarea' 
										and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('debetnota-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('debetnota-ap/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
