<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listcabangspb');
			$data['dfrom']='';
			$data['dto']='';				
			$this->load->view('listcabangspb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listcabangspb');
			$this->load->view('listcabangspb/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			  $ispb	= $this->uri->segment(4);
			  $iarea= $this->uri->segment(5);
			  $dfrom= $this->uri->segment(6);
			  $dto	= $this->uri->segment(7);
			  $this->load->model('listcabangspb/mmaster');
			  $this->mmaster->delete($ispb,$iarea);

				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Delete SPB No:'.$ispb.' Area:'.$iarea;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );

   		  $is_cari = $this->input->post('is_cari'); 
			  $cari	= strtoupper($this->input->post('cari'));
			  $config['base_url'] = base_url().'index.php/listcabangspb/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
  			  $sql	= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
				where a.i_customer=b.i_customer
				and a.i_area='$iarea' and
				(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
				a.d_spb <= to_date('$dto','dd-mm-yyyy')) order by a.i_spb desc ";
			  $query = $this->db->query($sql,false);

			  $config['total_rows'] = $query->num_rows();
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(8);
			  $this->pagination->initialize($config);
			
			  $this->load->model('listcabangspb/mmaster');
			  $data['page_title'] = $this->lang->line('listcabangspb');
			  $data['cari']	= $cari;
			  $data['dfrom']	= $dfrom;
			  $data['dto']	= $dto;
			  $data['iarea'] = $iarea;
			  $data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			  $this->load->view('listcabangspb/vmainform',$data);
 		}else{
  		  $this->load->view('awal/index.php');
	  }
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
			if ($cari == '') {
				$cari=$this->uri->segment(7);
			}
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea'); 
			if($dfrom=='') $dfrom = $this->uri->segment(4);
			if($dto=='') $dto = $this->uri->segment(5);
			if($iarea=='') $iarea = $this->uri->segment(6);
			
			$config['base_url'] = base_url().'index.php/listcabangspb/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/index/';
			
						
			$sql= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%') 
					and a.i_area='$iarea' and
						(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and
						a.d_spb <= to_date('$dto','dd-mm-yyyy')) order by a.i_spb desc ";
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			
			$this->load->model('listcabangspb/mmaster');
			$data['page_title'] = $this->lang->line('listcabangspb');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listcabangspb/vmainform',$data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function paging() 
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($this->uri->segment(5)!=''){
				if($this->uri->segment(4)!='sikasep'){
					$cari=$this->uri->segment(4);
				}else{
					$cari='';
				}
			}elseif($this->uri->segment(4)!='sikasep'){
				$cari=$this->uri->segment(4);
			}else{
				$cari='';
			}
			if($cari=='')
				$config['base_url'] = base_url().'index.php/listcabangspb/cform/paging/sikasep/';
			else
				$config['base_url'] = base_url().'index.php/listcabangspb/cform/paging/'.$cari.'/';
			if($this->uri->segment(4)=='sikasep')
				$cari='';
			$cari	= $this->input->post('cari', TRUE);
			$cari  	= strtoupper($cari);
			if($this->session->userdata('level')=='0'){
				$sql= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
						where a.i_customer=b.i_customer
						and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						or upper(a.i_spb) like '%$cari%') order by a.i_spb desc ";
			}else{
				$sql= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
						where a.i_customer=b.i_customer
						and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						or upper(a.i_spb) like '%$cari%')
						and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
						or a.i_area='$area5') order by a.i_spb desc ";
			}
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('listcabangspb');
			$this->load->model('listcabangspb/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listcabangspb/vform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcabangspb/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
						
			if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('listcabangspb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listcabangspb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listcabangspb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcabangspb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listcabangspb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			$is_cari= $this->input->post('is_cari'); 
				
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea=$this->uri->segment(6);
				
			if ($is_cari == '')
				$is_cari= $this->uri->segment(8);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(7);
			
			if ($is_cari=="1") { 
				$config['base_url'] = base_url().'index.php/listcabangspb/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/'.$is_cari.'/index/';
			}
			else { 
				$config['base_url'] = base_url().'index.php/listcabangspb/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			} 
			
			if ($is_cari != "1") {
				$sql= " select a.i_spb , a.d_spb , b.e_customer_name , c.e_product_groupname , d.e_area_name , a.n_print , a.i_approve1 , a.i_approve2
						from tm_spb a , tr_customer b , tr_product_group c, tr_area d
						where a.i_customer=b.i_customer
						and a.i_product_group=c.i_product_group
						and a.i_area=d.i_area
						and a.f_spb_stockdaerah='true'
						and a.f_spb_cancel='false'
						and a.i_area='$iarea'
						and  (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
						                a.d_spb <= to_date('$dto','dd-mm-yyyy'))
						group by a.i_spb , a.d_spb , b.e_customer_name , c.e_product_groupname , a.i_area , a.n_print , d.e_area_name
						order by a.i_spb , a.i_area ";
			}
			else {
				$sql= " select a.i_spb , a.d_spb , b.e_customer_name , c.e_product_groupname , d.e_area_name , a.n_print , a.i_approve1 , a.i_approve2
						from tm_spb a , tr_customer b , tr_product_group c, tr_area d
						where a.i_customer=b.i_customer
						and a.i_product_group=c.i_product_group
						and a.i_area=d.i_area
						and a.f_spb_stockdaerah='true'
						and a.f_spb_cancel='false'
						and a.i_area='$iarea'
						and  (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
						                a.d_spb <= to_date('$dto','dd-mm-yyyy'))
						and (upper(b.e_customer_name) like '%$cari%' or upper(a.i_spb) like '%$cari%')
						group by a.i_spb , a.d_spb , b.e_customer_name , c.e_product_groupname , a.i_area , a.n_print , d.e_area_name
						order by a.i_spb , a.i_area";
			}
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if ($is_cari=="1")
				$config['cur_page'] = $this->uri->segment(10);
			else
				$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			
			$this->load->model('listcabangspb/mmaster');
			$data['page_title'] = $this->lang->line('listcabangspb');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;

			if ($is_cari=="1")
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(10),$cari);
			else
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data List SPB cabang '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listcabangspb/vmainform',$data);			

		}else{
			$this->load->view('awal/index.php');
		}
	}
   function edit()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         (($this->session->userdata('menu57')=='t'))) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('terimaspb')." update";
         if(
            ($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
           )
         {
            $ispb = $this->uri->segment(4);
            $iarea= $this->uri->segment(5);

            $dfrom   = $this->uri->segment(6);
            $dto  = $this->uri->segment(7);
            $iarea_awal = $this->uri->segment(8);
            $ipricegroup   = $this->uri->segment(9);

            $query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
            $data['jmlitem']= $query->num_rows();
            $data['ispb']  = $ispb;
            $data['departement']=$this->session->userdata('departement');
            $this->load->model('listcabangspb/mmaster');
            $data['isi']   = $this->mmaster->baca($ispb,$iarea);
            $data['detail']   = $this->mmaster->bacadetail($ispb,$iarea);

            $qnilaispb  = $this->mmaster->bacadetailnilaispb($ispb,$iarea);
            if($qnilaispb->num_rows()>0){
               $row_nilaispb  = $qnilaispb->row();
               $data['nilaispb'] = $row_nilaispb->nilaispb;
            }else{
               $data['nilaispb'] = 0;
            }
            $qnilaiorderspb   = $this->mmaster->bacadetailnilaiorderspb($ispb,$iarea);
            if($qnilaiorderspb->num_rows()>0){
               $row_nilaiorderspb   = $qnilaiorderspb->row();
               $data['nilaiorderspb']  = $row_nilaiorderspb->nilaiorderspb;
            }else{
               $data['nilaiorderspb']  = 0;
            }
            $qeket   = $this->db->query(" SELECT e_remark1 as keterangan from tm_spb where i_spb ='$ispb' and i_area='$iarea' ");
            if($qeket->num_rows()>0){
               $row_eket   = $qeket->row();
               $data['keterangan']  = $row_eket->keterangan;
            }

            $data['dfrom'] = $dfrom;
            $data['dto'] = $dto;
            $data['iarea_awal'] = $iarea_awal;

            $this->load->view('listcabangspb/vformupdate',$data);
         }else{
            $this->load->view('listcabangspb/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');

      }
   }
   function update()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu57')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb    = $this->input->post('ispb', TRUE);
         $dspb    = $this->input->post('dspb', TRUE);
         if($dspb!=''){
            $tmp=explode("-",$dspb);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dspb=$th."-".$bl."-".$hr;
         }
         $icustomer        = $this->input->post('icustomer', TRUE);
         $ecustomername   = $this->input->post('ecustomername', TRUE);
         $ecumstomeraddress= $this->input->post('ecumstomeraddress', TRUE);
         $eremarkx            = $this->input->post('eremarkx', TRUE);
         $iarea              = $this->input->post('iarea', TRUE);
         $eareaname        = $this->input->post('eareaname', TRUE);
         $ispbpo             = $this->input->post('ispbpo', TRUE);
         $nspbtoplength   = $this->input->post('nspbtoplength', TRUE);
         $isalesman        = $this->input->post('isalesman',TRUE);
         $esalesmanname   = $this->input->post('esalesmanname',TRUE);
         $ipricegroup       = $this->input->post('ipricegroup',TRUE);
         $inota              = $this->input->post('inota',TRUE);
         $isj               = $this->input->post('isj',TRUE);
      $dsj  = $this->input->post('dsj', TRUE);
         if($dsj!=''){
            $tmp=explode("-",$dsj);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dsj=$th."-".$bl."-".$hr;
         }
         $istore            = $this->input->post('istore',TRUE);
         $istorelocation   = $this->input->post('istorelocation',TRUE);
         $istorelocationbin= '00';
         $dspbreceive       = $this->input->post('dspbreceive',TRUE);
         $dspbstorereceive  = $this->input->post('dspbstorereceive',TRUE);
         $fspb_op         = $this->input->post('f_spb_op',TRUE);
         if($fspb_op!=''){
            $fspbop     = 't';
         }else{
            $fspbop     = 'f';
         }
         $ecustomerpkpnpwp = $this->input->post('ecustomerpkpnpwp',TRUE);
         if($ecustomerpkpnpwp!=''){
            $fspbpkp = 't';
         }else{
            $fspbpkp = 'f';
         }
         $fspbconsigment      = $this->input->post('fspbconsigment',TRUE);
         if($fspbconsigment!=''){
            $fspbconsigment = 't';
         }else{
            $fspbconsigment = 'f';
         }
         $fspbplusppn      = $this->input->post('fspbplusppn',TRUE);
         $fspbplusdiscount = $this->input->post('fspbplusdiscount',TRUE);
         $fspbstockdaerah  = $this->input->post('fspbstockdaerah',TRUE);
         if($fspbstockdaerah!=''){
            $fspbstockdaerah= 't';
         }else{
            $fspbstockdaerah= 'f';
         }
         $fspbprogramx  = $this->input->post('f_spb_program',TRUE);
         if($fspbprogramx!=''){
            $fspbprogram   = 't';
         }else{
            $fspbprogram   = 'f';
         }
         $fspbsiapnotagudangx = $this->input->post('fspbsiapnotagudang',TRUE);
         if($fspbsiapnotagudangx!='' || !empty($fspbsiapnotagudangx)){
            $fspbsiapnota  = 't';
         }else{
            $fspbsiapnota  = 'f';
         }
         $fspbcancel = 'f';
         $fspbprogram   = 'f';
         $fspbvalid     = 'f';
         $nspbtoplength = $this->input->post('nspbtoplength',TRUE);
         $nspbdiscount1 = $this->input->post('ncustomerdiscount1',TRUE);
         $nspbdiscount2 = $this->input->post('ncustomerdiscount2',TRUE);
         $nspbdiscount3 = $this->input->post('ncustomerdiscount3',TRUE);
         $vspbdiscount1 = $this->input->post('vcustomerdiscount1',TRUE);
         $vspbdiscount2 = $this->input->post('vcustomerdiscount2',TRUE);
         $vspbdiscount3 = $this->input->post('vcustomerdiscount3',TRUE);
         $vspbdiscount1x   = $this->input->post('vcustomerdiscount1x',TRUE);
         $vspbdiscount2x   = $this->input->post('vcustomerdiscount2x',TRUE);
         $vspbdiscount3x   = $this->input->post('vcustomerdiscount3x',TRUE);
         $vspbdiscounttotal   = $this->input->post('vspbdiscounttotal',TRUE);
         $vspbdiscounttotalafter = $this->input->post('vspbdiscounttotalafter',TRUE);
         $vspb             = $this->input->post('vspb',TRUE);
         $vspbx            = $this->input->post('vspbx',TRUE);
         $vspbafter      = $this->input->post('vspbafter',TRUE);
         $nspbdiscount1 = str_replace(',','',$nspbdiscount1);
         $nspbdiscount2 = str_replace(',','',$nspbdiscount2);
         $nspbdiscount3 = str_replace(',','',$nspbdiscount3);
         $vspbdiscount1 = str_replace(',','',$vspbdiscount1);
         $vspbdiscount2 = str_replace(',','',$vspbdiscount2);
         $vspbdiscount3 = str_replace(',','',$vspbdiscount3);
         $vspbdiscount1x   = str_replace(',','',$vspbdiscount1x);
         $vspbdiscount2x   = str_replace(',','',$vspbdiscount2x);
         $vspbdiscount3x   = str_replace(',','',$vspbdiscount3x);
         $vspbdiscounttotal   = str_replace(',','',$vspbdiscounttotal);
         $vspbdiscounttotalafter = str_replace(',','',$vspbdiscounttotalafter);
         $vspb      = str_replace(',','',$vspb);
         $vspbx      = str_replace(',','',$vspbx);
         $vspbafter= str_replace(',','',$vspbafter);
         $ispbold = $this->input->post('ispbold', TRUE);
         $jml     = $this->input->post('jml', TRUE);
         if(($ecustomername!='') && ($ispb!=''))
         {
            $benar="false";
            $this->db->trans_begin();
            $this->load->model('listcabangspb/mmaster');
            $this->mmaster->updateheader($ispb, $iarea, $dspb, $icustomer, $ispbpo, $nspbtoplength, $isalesman,
                   $ipricegroup, $dspbreceive, $dspbstorereceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp,
                   $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
                   $fspbsiapnota, $fspbcancel, $nspbdiscount1,
                   $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
                   $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold, $eremarkx);
            /*for($i=1;$i<=$jml;$i++){
              $iproduct         = $this->input->post('iproduct'.$i, TRUE);
              $iproductstatus = $this->input->post('iproductstatus'.$i, TRUE);
              $iproductgrade  = 'A';
              $iproductmotif  = $this->input->post('motif'.$i, TRUE);
              $eproductname     = $this->input->post('eproductname'.$i, TRUE);
              $vunitprice     = $this->input->post('vproductretail'.$i, TRUE);
              $vunitprice     = str_replace(',','',$vunitprice);
              $norder             = $this->input->post('norder'.$i, TRUE);
              $ndeliver       = $this->input->post('ndeliver'.$i, TRUE);
              $ndeliverx      = $this->input->post('ndeliverx'.$i, TRUE);
              if($ndeliver=='')$ndeliver=null;
              if($ndeliverx=='')$ndeliverx=null;

              $eremark        = $this->input->post('eremark'.$i, TRUE);
              if($norder>0){
                $this->mmaster->insertdetail( $ispb,$iarea,$iproduct,$iproductstatus,$iproductgrade,$eproductname,$norder,$ndeliver,
                                    $vunitprice,$iproductmotif,$eremark,$i);
              }
            }*/
            if ( ($this->db->trans_status() === FALSE) )
            {
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();

               $sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
                  $now    = $row['c'];
               }
               $pesan='Update Terima SPB Area '.$iarea.' No:'.$ispb;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );

               $data['sukses']         = true;
               $data['inomor']         = $ispb;
               $this->load->view('nomor',$data);
            }
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
