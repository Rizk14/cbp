<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu65')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transdo').' FORECAST';
			$data['isi']= '';#directory_map('./data/');
			$data['file']='';
			$this->load->view('transdofc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu65')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dfrom= $this->input->post('dfrom', TRUE);
			$dto= $this->input->post('dto', TRUE);
			$data['dfrom']=$dfrom;
			$data['dto']=$dto;
			$this->load->view('transdofc/vfile', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu65')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml= $this->input->post('jml', TRUE);
			$this->load->model('transdofc/mmaster');
			$idox					= '';
			$isupplierx		= '';
			$data['jml']	= $jml;
			$data['kosong']	= 0;
			$tmp[0]					= null;
			$istore					= 'AA';
			$istorelocation		= '01';
			$istorelocationbin= '00';
			$eremark			= 'DO Transfer';
			for($i=0;$i<$jml;$i++){
				$cek					=$this->input->post('chk'.$i, TRUE);
				if($cek!=''){
					$ido					= $this->input->post('nodok'.$i, TRUE);
					$iop					= $this->input->post('noop'.$i, TRUE);
					$iarea				= $this->input->post('wila'.$i, TRUE);
					$ddo					= $this->input->post('tgldok'.$i, TRUE);
					if($ddo!=''){
						$tmp=explode("-",$ddo);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$ddo=$th."-".$bl."-".$hr;
						$thbl=substr($th,2,2).$bl;
					}
					$ido='DO-'.$thbl.'-DT'.substr($ido,2,4);
					$dop					= $this->input->post('tglop'.$i, TRUE);
          if($dop!=''){
						$tmp=explode("-",$dop);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dop=$th."-".$bl."-".$hr;
						$thbl=substr($th,2,2).$bl;
					}
          $iop='OP-'.$thbl.'-'.$iop;
					$iproduct				= $this->input->post('kodeprod'.$i, TRUE);
					$iproduct				= substr($iproduct,0,7);
					$iproductgrade	= 'A';
					$iproductmotif	= $this->input->post('kodeprod'.$i, TRUE);
					$iproductmotif	= substr($iproductmotif,7,2);
					$isupplier			= $this->input->post('kodelang'.$i, TRUE);
					$vproductmill		= $this->input->post('hargasat'.$i, TRUE);
					$vproductmill		= str_replace(',','',$vproductmill);
					$jumlah					= $this->input->post('jumlah'.$i, TRUE);
					$jumlah					= str_replace(',','',$jumlah);
					$eproductname		= null;
					$this->mmaster->inserttmp( $ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
													  $vproductmill,$iproductmotif,$isupplier,$ddo,$iarea,$iop,$dop);
				}
			}
			$this->db->trans_begin();
			$dotmp	= '';
			$xx		= 0;
      $xxx  = array();
			$yy		= 0;
			for($i=0;$i<$jml;$i++){
				$cek					=$this->input->post('chk'.$i, TRUE);
				if($cek!=''){
					$ido					= $this->input->post('nodok'.$i, TRUE);
					$iop					= $this->input->post('noop'.$i, TRUE);
					$iarea				= $this->input->post('wila'.$i, TRUE);
					$ddo					= $this->input->post('tgldok'.$i, TRUE);
					if($ddo!=''){
						$tmp=explode("-",$ddo);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$ddo=$th."-".$bl."-".$hr;
            $emutasiperiode=$th.$bl;
					}
					$dop					= $this->input->post('tglop'.$i, TRUE);
					if($dop!=''){
						$tmp=explode("-",$dop);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dop=$th."-".$bl."-".$hr;
###						
            settype($iop,"string");
            $a=strlen($iop);
            while($a<6){
              $iop="0".$iop;
              $a=strlen($iop);
            }
###						
            $iop='OP-'.substr($th,2,2).$bl.'-'.$iop;
					}
					$thbl=substr($th,2,2).$bl;
					$ido='DO-'.$thbl.'-DT'.substr($ido,2,4);
					$iproduct				= $this->input->post('kodeprod'.$i, TRUE);
					$iproduct				= substr($iproduct,0,7);
					$iproductgrade	= 'A';
					$iproductmotif	= $this->input->post('kodeprod'.$i, TRUE);
					$iproductmotif	= substr($iproductmotif,7,2);
					$isupplier			= $this->input->post('kodelang'.$i, TRUE);
					$vproductmill		= $this->input->post('hargasat'.$i, TRUE);
					$vproductmill		= str_replace(',','',$vproductmill);
					$jumlah					= $this->input->post('jumlah'.$i, TRUE);
          $jumlah					= str_replace(',','',$jumlah);
					$query= $this->db->query("select e_product_name from tr_product where i_product='$iproduct'");
					foreach($query->result() as $riw){
						$eproductname	=$riw->e_product_name;
					}
					$adaop	= $this->mmaster->cekadaop($iop,$iproduct,$iproductmotif,$iproductgrade,$iarea);
					$adaitem=1;
					if($adaop!=0){
						$adaitem= $this->mmaster->cekdataitem($ido,$isupplier,$iproduct,$iproductmotif,$iproductgrade,$thbl);
						if($adaitem==0){
							$sudahada=false;
							$ono= $this->mmaster->cekdata($ido,$isupplier,$thbl);
							if( ($dotmp!= $ido) && ($ono==0) ){
								$dotmp= $ido;
								$query= $this->db->query("select sum(v_product_mill) as v_product_mill, i_do, i_supplier, d_do, i_area, i_op
													                from tt_dofc where i_do='$ido' and i_supplier='$isupplier'
													                group by i_do, i_supplier, d_do, i_area, i_op ", false);
								foreach($query->result() as $row){
                  $row->i_op=trim($row->i_op);
									$qq= $this->db->query("select i_op from tm_opfc where i_op like '%$row->i_op%' and i_area='$iarea'", false);
									foreach($qq->result() as $rr){
										$re= $this->db->query("select i_product from tm_opfc_item where i_op='$rr->i_op' and i_product='$iproduct'", false);
										if($re->num_rows() >0){
											$ada	= $this->mmaster->cekdata($row->i_do,$row->i_supplier,$thbl);
											$siop	= 0;
											$saldo	= $jumlah;
											$qy		= $this->db->query("select i_op from tm_opfc where i_op like '%$row->i_op%' and i_area='$iarea'", false);
											foreach($qy->result() as $rw){
												if($ada==0){
													if($siop==1){
														$row->i_do=trim($row->i_do).'-A';
													}elseif($siop==2){
														$row->i_do=trim($row->i_do).'-B';
													}elseif($siop==0){
														$this->mmaster->insertheader($row->i_do,$row->i_supplier,$rw->i_op,$row->i_area,$row->d_do,
																					 $row->v_product_mill);
													}
												}else{
													if($siop==1){
														$qy= $this->db->query("select i_product from tm_opfc_item where i_op='$rw->i_op' and i_product='$iproduct'", false);
														if($qy->num_rows() >0){
														}
													}elseif($siop==2){
														$qy= $this->db->query("select i_product from tm_opfc_item where i_op='$rw->i_op' and i_product='$iproduct'", false);
														if($qy->num_rows() >0){
														}
													}else{
														$this->mmaster->updateheader($row->i_do,$row->i_supplier,$rw->i_op,$row->i_area,$row->d_do,
																					 $row->v_product_mill);
													}
												}
												$siop++;
												$qy= $this->db->query("select i_product, n_order, n_item_no from tm_opfc_item where i_op='$iop' and i_product='$iproduct'", false);
												if($qy->num_rows() >0){
													foreach($qy->result() as $rwz){
														$saldo=$saldo-$rwz->n_order;
														if($saldo>=0){
															$this->mmaster->insertdetail($iop,$ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
																		 $vproductmill,$iproductmotif,$isupplier,$rwz->n_item_no,$ddo,$emutasiperiode);
  														$this->mmaster->updateopdetail($iop,$iproduct,$iproductgrade,$iproductmotif,$jumlah);
                              $iproductmotif='00';
														}
													}
													$sudahada=true;
												}
											}
										}
									}
								}
							}
							if(!$sudahada){
								if($ono>0){
									$saldo=$jumlah;
									$qy= $this->db->query("select i_product, n_order, n_item_no from tm_opfc_item where i_op='$iop' and i_product='$iproduct'", false);
									if($qy->num_rows() >0){
										foreach($qy->result() as $rwz){
											$saldo=$saldo-$rwz->n_order;
											$this->mmaster->insertdetail($iop,$ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
															 $vproductmill,$iproductmotif,$isupplier,$rwz->n_item_no,$ddo,$emutasiperiode);
											$this->mmaster->updateopdetail($iop,$iproduct,$iproductgrade,$iproductmotif,$jumlah);
										}
									}
								}else{
  								$this->mmaster->insertdetail($iop,$ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
																	 $vproductmill,$iproductmotif,$isupplier,$i,$ddo,$emutasiperiode);
									$this->mmaster->updateopdetail($iop,$iproduct,$iproductgrade,$iproductmotif,$jumlah);
								}
								$sudahada=false;
							}	
						}
					}else{
						$yy++;
						$xx++;
						$xxx[$yy]		= $iop.' - '.$iproduct.$iproductmotif.' - '.$eproductname.'  -  Tidak Ada di OP';
						$data['kosong']	= $xx;
					}
				}
			}
			$data['error']=$xxx;
  		$this->db->query("delete from tt_dofc");
			if ( ($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
				$this->load->view('transdofc/vformgagal',$data);
			}else{
				$this->db->trans_commit();
#				$this->db->trans_rollback();
		    $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		= pg_query($sql);
		    if(pg_num_rows($rs)>0){
			    while($row=pg_fetch_assoc($rs)){
				    $ip_address	  = $row['ip_address'];
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }
		    $query 	= pg_query("SELECT current_timestamp as c");
		    while($row=pg_fetch_assoc($query)){
			    $now	  = $row['c'];
		    }
		    $pesan='Transfer ke DO Berhasil';#Periode:'.$iperiode;
		    $this->load->model('logger');
		    $this->logger->write($id, $ip_address, $now , $pesan );
		    $this->load->view('transdofc/vformsukses',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
