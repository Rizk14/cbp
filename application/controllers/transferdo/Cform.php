<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
		$this->load->library('pagination');
	}
	function index()
	{
	
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu222')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$data['page_title'] = $this->lang->line('transferdo');
			$data['isi']= '';#directory_map('./data/');
			$data['file']='';
			$this->load->view('transferdo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu65')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari  = strtoupper($this->input->post('cari', FALSE));
      if( ($this->uri->segment(4)=='pqrst') || ($this->uri->segment(4)=='') ){
        $config['base_url'] = base_url().'index.php/transferdo/cform/supplier/pqrst/';        
      }else{
        if($cari=='') $cari=$this->uri->segment(4);
        $config['base_url'] = base_url().'index.php/transferdo/cform/supplier/'.$cari.'/';
      }
      $query 	= $this->db->query("select i_supplier from tr_supplier
                           where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transferdo/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('transferdo/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu222')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			  
			  $isupplier  = $this->input->post('isupplier',TRUE);
			  $iarea  = $this->input->post('iarea',TRUE);
			  $icustomer  = $this->input->post('icustomer',TRUE);
			 # $today  = $this->input->post('today',TRUE);
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
				if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$today=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
				}
				if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$thy=$tmp[2];
				$bly=$tmp[1];
				$hry=$tmp[0];
				$yest = $thy."-".$bly."-".$hry;
				$thbly=$thy.$bly;
				}
			  $data['iuser']=$this->session->userdata('user_id');
			  $data['yest']=$yest;
			  $data['today']=$today;
			  $data['isupplier']=$isupplier;
			  $data['icustomer']=$icustomer;
			  $data['iarea']=$iarea;
			  $this->load->view('transferdo/vfile', $data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu222')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $iuser   = $this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/transferdo/cform/area/index/';
            $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transferdo/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($iuser,$config['per_page'],$this->uri->segment(5));
			$this->load->view('transferdo/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu222')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/transferdo/cform/area/index/';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                              where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transferdo/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('transferdo/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function transfer(){
		if (
		(($this->session->userdata('logged_in')) &&
		($this->session->userdata('menu222')=='t')) ||
		(($this->session->userdata('logged_in')) &&
		($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('transferdo/mmaster');
			$jml= $this->input->post('jml', TRUE);
			$data['jml']	= $jml;
			$this->db->trans_begin();
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			  $now	  = $row['c'];
      	  $tmp=explode("-",$now);
	      $th=$tmp[0];
	      $bl=$tmp[1];
	      $hr=$tmp[2];
			  $thbl=$th.$bl;
			}

			if($jml!=0 || $jml!='' ){
			
			for($i=0;$i<$jml;$i++){
			  $j=1;
			$cek					=$this->input->post('chk'.$i, TRUE);
			if($cek!=''){
            $ido    = $this->input->post('ido'.$i, FALSE);
		    $ddo	 = $this->input->post('today', FALSE);
		    $iop    = $this->input->post('ireff'.$i, FALSE);
		    if($ddo!=''){
			    $tmp=explode("-",$ddo);
			    $th=$tmp[0];
			    $bl=$tmp[1];
			    $hr=$tmp[2];
			    $dpo=$hr."-".$bl."-".$th;
		    }
			$isupplier	 		= $this->input->post('isupplier', FALSE);
			$iarea 		      = $this->input->post('iarea', FALSE);
			$remark 		    = $this->input->post('eremark'.$i, FALSE);
			$vdo           	= $this->input->post('vdo'.$i, FALSE);
			$istore               = 'AA';
			$istorelocation      = '01';
			$istorelocationbin= '00';
		 #   var_dump($ido,$isupplier,$iop,$iarea,$ddo,$vdo,$now);
			#die;
			$this->mmaster->insertheader($ido,$isupplier,$iop,$iarea,$ddo,$vdo);
			$rcs = $this->mmaster->bacaop($iop);

           # $sql = "select * from tm_op_item where i_op ='$iop'";
           # $rcs		= pg_query($sql);
            
            $no=0;
            foreach($rcs as $raw){
	            $iproduct	      = $raw->i_product; 
	            $iproductmotif	= $raw->i_product_motif;
	            $iproductgrade 	= $raw->i_product_grade;
	            $ndeliv     		= 0;
	            $vmill   		    = $raw->v_product_mill; 
	            $eproduct       = $raw->e_product_name; 
	            $nitem         	= $raw->n_item_no; 
	            #var_dump($ido,$ddo,$iop,$iproduct,$iproductmotif,$iproductgrade,$ndeliv,$vmill,$eproduct,$nitem);
          #      die;
            
    		    $this->mmaster->insertdetail($ido,$isupplier,$iop,$iproduct,$iproductmotif,$iproductgrade,$ndeliv,$vmill,$eproduct,$nitem);
    		    $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
    		    #var_dump($iop,$iproduct,$iproductgrade,$iproductmotif,$ndeliv,$ido);
    		    #die;
    		    
				
				#$konek 	= "host=localhost user=postgres dbname=stock port=5432 password=omiland ";
				$konek 	= "host=192.168.0.7 user=dedy dbname=stock port=5432 password=dedyalamsyah ";
				$db    	= pg_connect($konek);
				$sql = "select d.i_customer, a.i_do, a.d_do, d.i_area,  d.i_op, d.e_op_remark ,i_product, b.v_product_mill , b.n_deliver 
						from tm_do a
						inner join tm_do_item b on ( b.i_do = a.i_do)
						inner join tr_supplier c on (a.i_supplier=c.i_supplier)
						inner join tm_po d on (a.i_po=d.i_po)
						inner join tr_customer e on (d.i_customer=e.i_customer) 
						where a.i_do ='$ido' and b.i_product='$iproduct'
						order by i_product";
				$det		= pg_query($sql);
				pg_close($db);
    		    while ($data=pg_fetch_assoc($det)) {
    		    	$iproduct=$data['i_product']; 
    		    	$ndeliv=$data['n_deliver'];
    		    	$th=substr($ddo,0,4);
					$bl=substr($ddo,5,2);
    		    	$emutasiperiode=$th.$bl;
    		    	$this->mmaster->updatendeliver($ido,$iproduct,$ndeliv,$ddo,$emutasiperiode);
    		    	$this->mmaster->updateopitem($iop,$iproduct,$iproductgrade,$iproductmotif,$ndeliv,$ido);
    		    	$this->mmaster->updatespbdetail($iop,$iproduct,$iproductgrade,$iproductmotif,$ndeliv);
					  
					  #var_dump($trans);
					  #die;
					  if(isset($trans)){
					  foreach($trans as $itrans)
					  {
					    $q_aw =$itrans->n_quantity_stock;
					    $q_ak =$itrans->n_quantity_stock;
					#                $q_in =$itrans->n_quantity_in;
					#                $q_out=$itrans->n_quantity_out;
					    $q_in =0;
					    $q_out=0;
					    break;
					  }
					}else{
					  $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
					  if(isset($trans)){
					    foreach($trans as $itrans)
					    {
					      $q_aw =$itrans->n_quantity_stock;
					      $q_ak =$itrans->n_quantity_stock;
					      $q_in =0;
					      $q_out=0;
					      break;
					    }
					  }else{
					    $q_aw=0;
					    $q_ak=0;
					    $q_in=0;
					    $q_out=0;
					  }
					}
					$this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproduct,$ido,$q_in,$q_out,$ndeliv,$q_aw,$q_ak);
					// $th=substr($ddo,0,4);
					// $bl=substr($ddo,5,2);
					// $emutasiperiode=$th.$bl;
					if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
					{

					  $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliv,$emutasiperiode);
					}else{
					  $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliv,$emutasiperiode);
					}
					if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
					{
					  #var_dump($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliv,$q_ak);
				      #die;
					  $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliv,$q_ak);
					}else{
					  $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproduct,$ndeliv);
					}
    		    }

    		    $jum = $this->mmaster->bacatotal($ido);
    		    foreach ($jum as $val) {
    		    	$vdo = $val->v_do;

    		    }
    		    $this->mmaster->updatevdo($ido,$vdo);

    		    }
    		    #$konek 	= "host=localhost user=postgres dbname=stock port=5432 password=omiland ";
				$konek 	= "host=192.168.0.7 user=dedy dbname=stock port=5432 password=dedyalamsyah ";
				$db    	= pg_connect($konek);
				$sql = "update tm_do set f_transfer ='t' where i_do ='$ido'";
				$run = pg_query($sql);
				pg_close($db);
          }
          		#pg_close($db);
				$j++;
				}
				$this->db->trans_commit();				
				$pesan='Transfer ke DO Berhasil';#Periode:'.$iperiode;
				$data['sukses']=true;
				$data['inomor']			= $pesan;
				$this->load->view('nomor',$data);
				}
		}else{
					$this->load->view('awal/index.php');
		}	
	}
  function customergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu222')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		  $iuser = $this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/transferdo/cform/customergroup/index/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));

			$query = $this->db->query("	select * from tr_customer_group
			                            where (upper(i_customer_group) like '%$cari%' 
              										or upper(e_customer_groupname) like '%$cari%')",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transferdo/mmaster');
			$data['page_title'] = $this->lang->line('list_custgroup');
			$data['isi']		=$this->mmaster->bacacustomergroup($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('transferdo/vlistcustomergroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function customer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu222')=='t'))|
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
        $cari  = strtoupper($this->input->post('cari', FALSE));
        if($this->uri->segment(4)!='x01'){
          if($cari=='') $cari=$this->uri->segment(4);
          $config['base_url'] = base_url().'index.php/transferdo/cform/customer/'.$cari.'/';
        }else{
          $config['base_url'] = base_url().'index.php/transferdo/cform/customer/x01/';
        }
        $query   = $this->db->query("select * from tr_customer_omiland_mapping a, tr_customer b
                                    where a.i_customer = b.i_customer and
                                    (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%') ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);
         $this->load->model('transferdo/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->bacacustomer($cari,$config['per_page'],$this->uri->segment(7));
         $this->load->view('transferdo/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
