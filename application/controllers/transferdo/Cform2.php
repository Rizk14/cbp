<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
		$this->load->library('pagination');
	}
	function index()
	{
	
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu222')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$data['page_title'] = $this->lang->line('transferdo');
			$data['isi']= '';#directory_map('./data/');
			$data['file']='';
			$this->load->view('transferdo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu65')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari  = strtoupper($this->input->post('cari', FALSE));
      if( ($this->uri->segment(4)=='pqrst') || ($this->uri->segment(4)=='') ){
        $config['base_url'] = base_url().'index.php/transferdo/cform/supplier/pqrst/';        
      }else{
        if($cari=='') $cari=$this->uri->segment(4);
        $config['base_url'] = base_url().'index.php/transferdo/cform/supplier/'.$cari.'/';
      }
      $query 	= $this->db->query("select i_supplier from tr_supplier
                           where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transferdo/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('transferdo/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu222')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			  
			  $isupplier  = $this->input->post('isupplier',TRUE);
			  $iarea  = $this->input->post('iarea',TRUE);
			  $icustomer  = $this->input->post('icustomer',TRUE);
			  $today  = $this->input->post('today',TRUE);
				if($today!=''){
				$tmp=explode("-",$today);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$today=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
				}
				if($hr !=1 || $hr!='1'){
					$hry=$hr-1;
				}else
				{
					if ($bl!=3 || $bl!='3'){
						$hry=28;
					}
					$hry=31;
				}
			  $yest = $th."-".$bl."-".$hry;
			  $data['iuser']=$this->session->userdata('user_id');
			  $data['yest']=$yest;
			  $data['today']=$today;
			  $data['isupplier']=$isupplier;
			  $data['icustomer']=$icustomer;
			  $data['iarea']=$iarea;
			  $this->load->view('transferdo/vfile', $data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu222')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $iuser   = $this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/transferdo/cform/area/index/';
            $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transferdo/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($iuser,$config['per_page'],$this->uri->segment(5));
			$this->load->view('transferdo/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu222')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/transferdo/cform/area/index/';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                              where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transferdo/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('transferdo/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function transfer(){
		if (
		(($this->session->userdata('logged_in')) &&
		($this->session->userdata('menu222')=='t')) ||
		(($this->session->userdata('logged_in')) &&
		($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('transferdo/mmaster');
			$jml= $this->input->post('jml', TRUE);
/*			$adaheader='';
			$spmbclose='';
			$adadetail='';*/
			$status ='';
			$ipox='';
			$data['jml']	= $jml;
			$this->db->trans_begin();
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			  $now	  = $row['c'];
        $tmp=explode("-",$now);
	      $th=$tmp[0];
	      $bl=$tmp[1];
	      $hr=$tmp[2];
			  $thbl=$th.$bl;
			}
			
			for($i=0;$i<$jml;$i++){
			  $j=1;
				$cek					=$this->input->post('chk'.$i, TRUE);
				if($cek!=''){
            $ido    = $this->input->post('ido'.$i, FALSE);
		    $ddo	 	= $this->input->post('today', FALSE);
		    if($ddo!=''){
			    $tmp=explode("-",$ddo);
			    $th=$tmp[0];
			    $bl=$tmp[1];
			    $hr=$tmp[2];
			    $dpo=$hr."-".$bl."-".$th;
		    }
			$isupplier	 		= $this->input->post('isupplier', FALSE);
			$iarea 		      = $this->input->post('iarea', FALSE);
			$ireff        	= $this->input->post('ireff'.$i, FALSE);
			$dreff       		= $this->input->post('dreff'.$i, FALSE);
			$remark 		    = $this->input->post('eremark'.$i, FALSE);
			$vdo           	= $this->input->post('vdo'.$i, FALSE);
		    
        #$query   	= $this->db->query("select * from tm_po where i_op like '$iop'",false);	
		#		$ipox=$ipo;				
			$this->mmaster->insertheader($ido,$isupplier,$iop,$iarea,$ddo,$vdogross,$now);
				#($ido,$isupplier,$iop,$iarea,$ddo,$vdogross)

##### konek ke db omiland #####

            $konek 	= "host=192.168.0.7 user=dedy dbname=omiland port=5432 password=dedyalamsyah ";
            $db    	= pg_connect($konek);
            $sql = "select * from tm_op_item where i_op ='$iop'";

            $rcs		= pg_query($sql);
            $no=0;
            while($raw=pg_fetch_assoc($rcs)){
	            $iproduct	      = $raw['i_product']; 
	            $iproductmotif	= $raw['i_product_motif'];
	            $iproductgrade 	= $raw['i_product_grade'];
	            $ndeliv     		= 0;
	            $vmill   		    = $raw['v_product_mill']; 
	            $eproduct       = $raw['e_product_name']; 
	            $nitem         	= $raw['n_item_no']; 
            	pg_close($db);
    				  
    				  $this->mmaster->insertdetail($ipo,$iproduct,$iproductmotif,$iproductgrade,$ndeliv,$vmill,$eproduct,$nitem);
						
  					}
					}
          }
				$j++;
				}
      
			
			$this->db->trans_commit();
#			$this->db->trans_rollback();

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Transfer PO Berhasil';#Periode:'.$iperiode;
			$this->load->model('logger');
			$data['sukses']='true';
			$this->logger->write($id, $ip_address, $now , $pesan );
			$data['inomor']			= 'Transfer PO Sukses ...';
			$this->load->view('nomor',$data);
		}else{
					$this->load->view('awal/index.php');
		}	
	}
  function customergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu222')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		  $iuser = $this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/transferdo/cform/customergroup/index/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));

			$query = $this->db->query("	select * from tr_customer_group
			                            where (upper(i_customer_group) like '%$cari%' 
              										or upper(e_customer_groupname) like '%$cari%')",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transferdo/mmaster');
			$data['page_title'] = $this->lang->line('list_custgroup');
			$data['isi']		=$this->mmaster->bacacustomergroup($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('transferdo/vlistcustomergroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function customer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu222')=='t'))|
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
        $cari  = strtoupper($this->input->post('cari', FALSE));
        if($this->uri->segment(4)!='x01'){
          if($cari=='') $cari=$this->uri->segment(4);
          $config['base_url'] = base_url().'index.php/transferdo/cform/customer/'.$cari.'/';
        }else{
          $config['base_url'] = base_url().'index.php/transferdo/cform/customer/x01/';
        }
        $query   = $this->db->query("select * from tr_customer_omiland_mapping a, tr_customer b
                                    where a.i_customer = b.i_customer and
                                    (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%') ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);
         $this->load->model('transferdo/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->bacacustomer($cari,$config['per_page'],$this->uri->segment(7));
         $this->load->view('transferdo/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
