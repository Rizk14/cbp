<?php 
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(26);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(23);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(13);
		$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(8);
		$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(8);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(8);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(15);
#				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(11);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:AK1'
				);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'NO NOTA');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'TGL NOTA');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'AREA');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'KODE LANG');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('E1', 'NAMA TOKO');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'KOTA');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'TOP');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'SALES');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'TGL JATUH TEMPO');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'NO SJ');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K1', 'TGL SJ');
				$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('L1', 'NILAI KOTOR');
			  $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('M1','DISCOUNT');
    		$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('N1', 'NILAI BERSIH');
			  $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );

        $i=2;
        $j=1;
        foreach($isi as $row){
        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			  array(
				  'font' => array(
					  'name'	=> 'Arial',
					  'bold'  => false,
					  'italic'=> false,
					  'size'  => 10
				  )
			  ),
			  'A'.$i.':N'.$i
			  );
			  
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
          $tmp=explode("-",$row->d_nota);
          $hr=$tmp[2];
          $bl=$tmp[1];
          $th=$tmp[0];
          $row->d_nota=$hr.'-'.$bl.'-'.$th;
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->d_nota, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->e_area_name, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_customer, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->e_city_name, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->n_customer_toplength, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->i_salesman, Cell_DataType::TYPE_STRING);
          $tmp=explode("-",$row->d_jatuh_tempo);
          $hr=$tmp[2];
          $bl=$tmp[1];
          $th=$tmp[0];
          $row->d_jatuh_tempo=$hr.'-'.$bl.'-'.$th;
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->d_jatuh_tempo, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->i_sj, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->d_sj, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->v_nota_gross, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->v_nota_discounttotal, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
        
#        echo $j.'  '.$row->i_spb.' '.$row->i_product.'<br>';
        $j++;
        $i++;
        }
?>