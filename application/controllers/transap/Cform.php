<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu374')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transap');
			$data['iperiode']	= '';
			//$data['iarea']	  = '';
			$this->load->view('transap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu374')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
#      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
#			if($iarea=='') $iarea=$this->uri->segment(5);
			$data['page_title'] = $this->lang->line('transap');
			$data['iperiode']	= $iperiode;
      $tahun=substr($iperiode,0,4);      
#			$data['iarea']	= $iarea;

      define ('BOOLEAN_FIELD',   'L');
      define ('CHARACTER_FIELD', 'C');
      define ('DATE_FIELD',      'D');
      define ('NUMBER_FIELD',    'N');
      define ('READ_ONLY',  '0');
      define ('WRITE_ONLY', '1');
      define ('READ_WRITE', '2');
#      $db_file = 'nota/nt'.$iarea.substr($iperiode,2,4).'.dbf';
      $db_file = 'nota/ap'.$iperiode.'.dbf';
      $dbase_definition = array (
         array ('KODETRAN',  CHARACTER_FIELD,  2),
         array ('NODOK',  CHARACTER_FIELD,  7),
         array ('TGLDOK',  DATE_FIELD),
         array ('NOFAK',  CHARACTER_FIELD,  9),
         array ('NODO',  CHARACTER_FIELD, 12),
         array ('NOOP',  CHARACTER_FIELD, 15),
         array ('KODELANG',  CHARACTER_FIELD,  5),
         array ('PKP',  NUMBER_FIELD,  1, 0),
         array ('TGLBAYAR', DATE_FIELD),
         array ('JTEMPO', DATE_FIELD),
         array ('DISCOUNT',  NUMBER_FIELD,  4,2),
         array ('KOTOR', NUMBER_FIELD, 10, 0),     
         array ('POTONG', NUMBER_FIELD, 9, 0),
         array ('PPN',  NUMBER_FIELD,  9, 0),  
         array ('JBERSIH',  NUMBER_FIELD,  14, 0),  
         array ('SISA',  NUMBER_FIELD,  14, 0),  
         array ('KODEAREA', CHARACTER_FIELD, 2)
      );
      if(file_exists($db_file)){
        @chmod($db_file, 0777);
        @unlink($db_file);
      }
      $create = @ dbase_create($db_file, $dbase_definition)
                or die ("Could not create dbf file <i>$db_file</i>.");
      $id = @ dbase_open ($db_file, READ_WRITE)
            or die ("Could not open dbf file <i>$db_file</i>."); 
      //$per=substr($iperiode,2,4);
#      $dicari='FP-'.$per.'-'.$iarea.'%';
      //$dicari='FP-'.$per.'-%';
      $this->db->select( " distinct(b.i_dtap), a.d_dtap, a.i_pajak, a.d_pajak, a.i_supplier, a.f_pkp, a.d_bayar,
                  a.d_due_date, a.n_discount, a.v_gross, a.v_discount, a.v_ppn, a.v_netto, a.v_sisa, a.i_area, b.i_op, b.i_do
                  from tm_dtap a, tm_dtap_item b
                  where a.i_dtap=b.i_dtap
                  and a.i_area=b.i_area and a.i_supplier=b.i_supplier
                  and to_char(a.d_dtap::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                  order by b.i_dtap",false);
      //$this->db->select($sql);
      $query = $this->db->get();

      $dttemp='';
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowap){
          if($dttemp!=$rowap->i_dtap){
          $kodetran   = '01';
	        $nodok      = $rowap->i_dtap;
	        $tgldok     = substr($rowap->d_dtap,0,4).substr($rowap->d_dtap,5,2).substr($rowap->d_dtap,8,2);
	        $nofak      = $rowap->i_pajak;
	        $nodo       = substr($rowap->i_do,9,6);
	        $noop       = substr($rowap->i_op,9,6);
	        $kodelang   = $rowap->i_supplier;
          if($rowap->f_pkp=='f'){ 
            $pkp='2'; 
          }else{ 
            $pkp='1';
          }
	        $tglbyr     = substr($rowap->d_bayar,0,4).substr($rowap->d_bayar,5,2).substr($rowap->d_bayar,8,2);
	        $jatuhtemp  = substr($rowap->d_due_date,0,4).substr($rowap->d_due_date,5,2).substr($rowap->d_due_date,8,2);;
	        $disc       = $rowap->n_discount;
          $kotor      = $rowap->v_gross;
          $potongan   = $rowap->v_discount;
	        $ppn        = $rowap->v_ppn;
	        $jbersih    = $rowap->v_netto;
	        $sisa       = $rowap->v_sisa;
	        $area       = $rowap->i_area;
          $isi = array ($kodetran,$nodok,$tgldok,$nofak,$nodo,$noop,$kodelang,$pkp,$tglbyr,$jatuhtemp,
                        $disc,$kotor,$potongan,$ppn,$jbersih,$sisa,$area);
          dbase_add_record ($id, $isi) or die ("Could not add record 'inari' to dbf file <i>$db_file</i>."); 
          }
          $dttemp=$rowap->i_dtap;
        }
      }
      dbase_close($id);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
      @chmod($db_file, 0777);
			$pesan='Transfer ke AP lama Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses']			= true;
			$data['inomor']			= $pesan;
			$this->load->view('nomor',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
/*	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listspmb/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspmb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listspmb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspmb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}*/
}
?>
