<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu183')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/spmbacc/cform/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_spmb a, tr_area b
																	where a.i_area=b.i_area and a.f_spmb_cancel='f' and f_spmb_acc='f'
                                  and a.i_approve2 isnull",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('spmbacc');
			$this->load->model('spmbacc/mmaster');
      $data['ispmb']='';
			$data['isi']		= $this->mmaster->bacaperiode($config['per_page'],$this->uri->segment(4));
			$this->load->view('spmbacc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu183')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('spmb');
			$this->load->view('spmbacc/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu183')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('spmbacc');
			if($this->uri->segment(4)!=''){
				$ispmb = $this->uri->segment(4);
        $peraw 	= $this->uri->segment(5);
			  $perak 	= $this->uri->segment(6);
				$data['ispmb'] = $ispmb;
        $data['peraw'] = $peraw;
				$data['perak'] = $perak;
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ispmb'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('spmbacc/mmaster');
				$data['isi']=$this->mmaster->baca($ispmb);
				$data['detail']=$this->mmaster->bacadetail($ispmb);
		 		$this->load->view('spmbacc/vmainform',$data);
			}else{
				$this->load->view('spmbacc/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{

		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu183')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){

			$ispmb 		= $this->input->post('ispmb', TRUE);
			$ispmbold	= $this->input->post('ispmbold', TRUE);
			$iarea 		= $this->input->post('iarea', TRUE);
			$jml		  = $this->input->post('jml', TRUE);
			if($ispmb!='')
			{

				$this->db->trans_begin();
				$this->load->model('spmbacc/mmaster');
				$this->mmaster->updateheader($ispmb, $ispmbold);
				for($i=1;$i<=$jml;$i++){
				  $iproduct	    = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade= 'A';
				  $iproductmotif= $this->input->post('motif'.$i, TRUE);
				  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice	  = $this->input->post('vproductmill'.$i, TRUE);
				  $vunitprice	  = str_replace(',','',$vunitprice);
				  $norder	      = $this->input->post('norder'.$i, TRUE);
				  $nacc		      = $this->input->post('nacc'.$i, TRUE);
				  $eremark	    = $this->input->post('eremark'.$i, TRUE);
				  
				  $this->mmaster->deletedetail($iproduct,$iproductgrade,$ispmb,$iproductmotif);
//				  if($norder>0){
			      $this->mmaster->insertdetail($ispmb,$iproduct,$iproductgrade,$eproductname,$norder,$nacc,$vunitprice,$iproductmotif,$eremark,$iarea,$i);
//				  }
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();

          $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		=  $this->db->query($sql);
			    if($rs->num_rows>0){
				    foreach($rs->result() as $tes){
					    $ip_address	  = $tes->ip_address;
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }

			    $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
			    $data['host']	= $ip_address;
			    $data['uri']	= $this->session->userdata('printeruri');
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='ACC SPMB No:'.$ispmb;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ispmb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu183')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/spmbacc/cform/product/'.$baris.'/index/';
			$query = $this->db->query(" select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_mill as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('spmbacc/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('spmbacc/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu183')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari=strtoupper($this->input->post("cari"));
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/spmbacc/cform/product/'.$baris.'/'.$cari.'/index/';
			$query = $this->db->query(" select a.i_product||a.i_product_motif as kode, 
										              c.e_product_name as nama,c.v_product_mill as harga
										              from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product and (a.i_product like '%$cari%' or c.e_product_name like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);

			$this->load->model('spmbacc/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$this->load->view('spmbacc/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu183')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			/*$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/spmbacc/cform/index/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_spmb a, tr_area b
																	where a.i_area=b.i_area and a.f_spmb_cancel='f' and a.f_spmb_acc='f'
                                  and a.i_approve2 isnull and upper(a.i_spmb) like '%$cari%'",false);*/
			
			$cari=($this->input->post("cari",false));
			if($cari=='')$cari=$this->uri->segment(4);
			if($cari=='zxvf'){
				$cari='';
				$config['base_url'] = base_url().'index.php/spmbacc/cform/cari/zxvf/';
			}else{
				$config['base_url'] = base_url().'index.php/spmbacc/cform/cari/'.$cari.'/';
			}

			$query = $this->db->query(" select a.*, b.e_area_name from tm_spmb a, tr_area b
										where a.i_area=b.i_area and a.f_spmb_cancel='f' and a.f_spmb_acc='f'
                                  		and a.i_approve2 isnull and (upper(a.i_spmb) ilike '%$cari%' or upper(b.e_area_name) ilike '%$cari%')",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('spmbacc');
			$this->load->model('spmbacc/mmaster');
      		$data['ispmb']='';
			$data['isi']		= $this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('spmbacc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
