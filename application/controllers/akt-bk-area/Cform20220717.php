<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kb').' (Keluar)';
			$this->load->view('akt-bk-area/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bk').' (Keluar)'." Update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5) && $this->uri->segment(6)
			  ){
				$ikb 		= $this->uri->segment(4);
				$iperiode	= $this->uri->segment(5);
				$iarea		= $this->uri->segment(6);
				$dfrom		= $this->uri->segment(7);
				$dto		= $this->uri->segment(8);
				$lepel		= $this->uri->segment(9);
				$icoabank   = $this->uri->segment(10);
				$this->load->model("akt-bk-area/mmaster");
				$data['isi']=$this->mmaster->baca($ikb,$iperiode,$iarea);
				$data['iarea']  = $iarea;
				$data['dfrom']  = $dfrom;
				$data['dto']    = $dto;
				$data['lepel']	= $lepel;
 		 		$this->load->view('akt-bk-area/vformupdate',$data);
			}else{
				$this->load->view('akt-bk-area/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikbank  	  = $this->input->post('ikbank', TRUE);
			$iarea		  = $this->input->post('iarea', TRUE);
			$iareaold	  = $this->input->post('iareaold', TRUE);
			$iperiode 	  = $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$ipvtype  	  = $this->input->post('ipvtype', TRUE);
			$tah		  = substr($this->input->post('iperiodeth', TRUE),2,2);
			$bul		  = $this->input->post('iperiodebl', TRUE);
			$vbank	      = $this->input->post('vbank', TRUE);
			$vbank	  	  = str_replace(',','',$vbank);
			$dbank    	  = $this->input->post('dbank', TRUE);
#			$dbukti		  = $this->input->post('dbukti', TRUE);
			$icoa	      = $this->input->post('icoa', TRUE);
			$ecoaname	  = $this->input->post('ecoaname', TRUE);
			$edescription = $this->input->post('edescription', TRUE);
			if($edescription==""){
				$edescription=null;
			}
      		$ibank  	= $this->input->post('ibank', TRUE);
			$icoabank	= $this->input->post('icoabank', TRUE);
			if($dbank!=''){
				$tmp=explode("-",$dbank);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbank=$th."-".$bl."-".$hr;
			}
#			if($dbukti!=''){
#				$tmp=explode("-",$dbukti);
#				$th=$tmp[2];
#				$bl=$tmp[1];
#				$hr=$tmp[0];
#				$dbukti=$th."-".$bl."-".$hr;
#			}
			$fdebet='t';
			if (
				(isset($ikbank) && $ikbank != '') &&
				(isset($iperiode) && $iperiode != '') && 
				(isset($iarea) && $iarea != '') &&
				(isset($vbank) && (($vbank != 0) || ($vbank != ''))) &&
				(isset($dbank) && $dbank != '') &&
				(isset($icoa) && $icoa != '')
			   )
			{
				$this->load->model('akt-bk-area/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($iarea,$ikbank,$iperiode,$icoa,$vbank,$dbank,$ecoaname,$edescription,$fdebet,$dbank,$ipvtype, $icoabank, $iareaold);
				$nomor=$ikbank;
###########posting##########
          $eremark		= $edescription;
          $fclose			= 'f';
			    $this->mmaster->inserttransheader($ikbank,$iarea,$eremark,$fclose,$dbank,$icoabank);
			    if($fdebet=='t'){
				    $accdebet		  = $icoa;
				    $namadebet		= $ecoaname;
				    $acckredit		= $icoabank;
				    $namakredit		= $this->mmaster->namaacc($acckredit);
			    }else{
				    $accdebet		  = $icoabank;
				    $namadebet		= $this->mmaster->namaacc($accdebet);
				    $acckredit		= $icoa;
				    $namakredit		= $ecoaname;
			    }
			    $this->mmaster->inserttransitemdebet($accdebet,$ikbank,$namadebet,'t','t',$iarea,$eremark,$vbank,$dbank,$icoabank);
			    $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vbank);
			    $this->mmaster->inserttransitemkredit($acckredit,$ikbank,$namakredit,'f','t',$iarea,$eremark,$vbank,$dbank,$icoabank);
			    $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vbank);
			    $this->mmaster->insertgldebet($accdebet,$ikbank,$namadebet,'t',$iarea,$vbank,$dbank,$eremark,$icoabank);
			    $this->mmaster->insertglkredit($acckredit,$ikbank,$namakredit,'f',$iarea,$vbank,$dbank,$eremark,$icoabank);
###########end of posting##########
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Update Bank Keluar No:'.$ikbank.' Area:'.$iarea;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );
#					$this->db->trans_rollback();
					$this->db->trans_commit();
					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-bk-area/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
  			$query = $this->db->query("select * from tr_area",false);
      }else{
        $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
	                        			   or i_area = '$area4' or i_area = '$area5'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-bk-area/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-bk-area/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/akt-bk-area/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-bk-area/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-bk-area/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function coa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-bk-area/cform/coa/index/';
			$query = $this->db->query("select * from tr_coa where i_coa like '6%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('akt-bk-area/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->bacacoa($config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-bk-area/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricoa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-bk-area/cform/coa/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_coa
						    									where (upper(i_coa) like '6%$cari%' or (upper(e_coa_name) like '%$cari%' 
																	and upper(i_coa) like '6%'))",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-bk-area/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->caricoa($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-bk-area/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bank()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu499')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-bk-area/cform/bank/';
			$cari=strtoupper($this->input->post('cari', FALSE));
 			$query = $this->db->query("select * from tr_bank where upper(e_bank_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-bk-area/mmaster');
			$data['page_title'] = $this->lang->line('list_bank');
			$data['isi']=$this->mmaster->bacabank($config['per_page'],$this->uri->segment(5),$cari);
			$data['cari']=$cari;
			$this->load->view('akt-bk-area/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
