<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb 	= $this->input->post('ispb', TRUE);
			$dspb 	= $this->input->post('dspb', TRUE);
			if($dspb!=''){
				$tmp=explode("-",$dspb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspb=$th."-".$bl."-".$hr;
			}
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomername	= $this->input->post('ecustomername', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$eareaname		= $this->input->post('eareaname', TRUE);
			$ispbpo			= $this->input->post('ispbpo', TRUE);
			if($ispbpo=='') $ispbpo=null;
			$nspbtoplength	= $this->input->post('nspbtoplength', TRUE);
			$isalesman		= $this->input->post('isalesman',TRUE);
			$esalesmanname	= $this->input->post('esalesmanname',TRUE);
			$ipricegroup	= $this->input->post('ipricegroup',TRUE);
			$inota			= $this->input->post('inota',TRUE);
			$dspbreceive	= $this->input->post('dspbreceive',TRUE);
			if($ispbpo!='')
				$fspbop			= 't';#$this->input->post('fspbop',TRUE);
			else
				$fspbop			= 'f';#$this->input->post('fspbop',TRUE);
			$ecustomerpkpnpwp	= $this->input->post('ecustomerpkpnpwp',TRUE);
			if($ecustomerpkpnpwp!=''){
				$fspbpkp		= 't';#$this->input->post('fspbpkp',TRUE);
			}else{
				$fspbpkp		= 'f';#$this->input->post('fspbpkp',TRUE);
				$ecustomerpkpnpwp=null;
			}
			$fspbconsigment		= $this->input->post('fspbconsigment',TRUE);
			if($fspbconsigment!='')
				$fspbconsigment="t";
			else
				$fspbconsigment="f";
			$fspbplusppn		= $this->input->post('fspbplusppn',TRUE);
			$fspbplusdiscount	= $this->input->post('fspbplusdiscount',TRUE);
			$fspbstockdaerah	= $this->input->post('fspbstockdaerah',TRUE);
			if($fspbstockdaerah!='')
				$fspbstockdaerah= 't';
			else
				$fspbstockdaerah= 'f';
			$ispbprogram	= $this->input->post('ipromo',TRUE);
			$fspbprogram	= 't';#$this->input->post('fspbprogram',TRUE);
			$fspbvalid		= 't';#$this->input->post('fspbvalid',TRUE);
			$fspbsiapnota	= 'f';#$this->input->post('fspbsiapnota',TRUE);
			$fspbcancel		= 'f';#$this->input->post('fspbcancel',TRUE);
			$nspbtoplength	= $this->input->post('nspbtoplength',TRUE);
			$nspbdiscount1	= $this->input->post('ncustomerdiscount1',TRUE);
			$nspbdiscount2	= $this->input->post('ncustomerdiscount2',TRUE);
			$nspbdiscount3	= $this->input->post('ncustomerdiscount3',TRUE);
			$nspbdiscount4	= $this->input->post('ncustomerdiscount4',TRUE);
			$vspbdiscount1	= $this->input->post('vcustomerdiscount1',TRUE);
			$vspbdiscount2	= $this->input->post('vcustomerdiscount2',TRUE);
			$vspbdiscount3	= $this->input->post('vcustomerdiscount3',TRUE);
			$vspbdiscount4	= $this->input->post('vcustomerdiscount4',TRUE);
			$vspbdiscounttotal	= $this->input->post('vspbdiscounttotal',TRUE);
			$vspb			= $this->input->post('vspb',TRUE);
			$nspbdiscount1	= str_replace(',','',$nspbdiscount1);
			$nspbdiscount2	= str_replace(',','',$nspbdiscount2);
			$nspbdiscount3	= str_replace(',','',$nspbdiscount3);
			$nspbdiscount4	= str_replace(',','',$nspbdiscount4);
			$vspbdiscount1	= str_replace(',','',$vspbdiscount1);
			$vspbdiscount2	= str_replace(',','',$vspbdiscount2);
			$vspbdiscount3	= str_replace(',','',$vspbdiscount3);
			$vspbdiscount4	= str_replace(',','',$vspbdiscount4);
			$vspbdiscounttotal	= str_replace(',','',$vspbdiscounttotal);
			$vspb			= str_replace(',','',$vspb);
			$ispbold	= $this->input->post('ispbold',TRUE);
			$jml			= $this->input->post('jml', TRUE);
			if($ecustomername!='')
			{
				$benar="false";
				$this->db->trans_begin();
				$this->load->model('spbpromo/mmaster');
				$ispb	=$this->mmaster->runningnumber($iarea);
				$this->mmaster->insertheader($ispb, $dspb, $icustomer, $iarea, $ispbpo, $nspbtoplength, $isalesman, 
											 $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp, 
											 $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
											 $fspbsiapnota, $fspbcancel, $nspbdiscount1, $nspbdiscount2, $nspbdiscount3, $nspbdiscount4, 
											 $vspbdiscount1, $vspbdiscount2, $vspbdiscount3, $vspbdiscount4, $vspbdiscounttotal, 
											 $vspb, $fspbconsigment, $ispbprogram, $ispbold);
				for($i=1;$i<=$jml;$i++){
				  $iproduct				= $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade			= 'A';
				  $iproductmotif			= $this->input->post('motif'.$i, TRUE);
				  $eproductname				= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice				= $this->input->post('vproductretail'.$i, TRUE);
				  $vunitprice				= str_replace(',','',$vunitprice);
				  $norder				= $this->input->post('norder'.$i, TRUE);
				  $eremark				= $this->input->post('eremark'.$i, TRUE);
				  $data['iproduct']			= $iproduct;
				  $data['iproductgrade']		= $iproductgrade;
				  $data['iproductmotif']		= $iproductmotif;
				  $data['eproductname']			= $eproductname;
				  $data['vunitprice']			= $vunitprice;
				  $data['norder']			= $norder;
				  if($norder>0){
				    $this->mmaster->insertdetail( $ispb,$iarea,$iproduct,$iproductgrade,$eproductname,$norder,
											$vunitprice,$iproductmotif,$eremark);
				  }
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();
					$data['sukses']			= true;
					$data['inomor']			= $ispb;
					$this->load->view('nomor',$data);
				}
			}else{
				$iarea = '';
				$data['page_title'] = $this->lang->line('spbpromo');
				$data['ispb']='';
				$this->load->model('spbpromo/mmaster');
				$data['isi']=$this->mmaster->bacasemua($iarea);
				$data['detail']="";
				$data['jmlitem']="";
				$this->load->view('spbpromo/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_spb');
			$this->load->view('spbpromo/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			(($this->session->userdata('menu91')=='t') || ($this->session->userdata('menu57')=='t'))) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spbpromo')." update";
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
			  )
			{
				$ispb  = $this->uri->segment(4);
				$iarea = $this->uri->segment(5);
				$ipromo= $this->uri->segment(6);
				$query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->db->select(" * from tm_spb where i_spb = '$ispb' and i_area='$iarea'");
				$query = $this->db->get();
				foreach($query->result() as $row){
				  $pesan=$row->e_notapprove;
				  $status=$row->i_notapprove;
				}
				$data['ispb'] = $ispb;
				$data['status'] = $status;
				$data['pesan'] 	= $pesan;
				$data['iarea'] = $iarea;
				$data['ipromo'] = $ipromo;
				$data['departement']=$this->session->userdata('departement');
				$this->load->model('spbpromo/mmaster');
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea);
		 		$this->load->view('spbpromo/vmainform',$data);
			}else{
				$this->load->view('spbpromo/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb 	= $this->input->post('ispb', TRUE);
			$dspb 	= $this->input->post('dspb', TRUE);
			if($dspb!=''){
				$tmp=explode("-",$dspb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspb=$th."-".$bl."-".$hr;
			}
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomername	= $this->input->post('ecustomername', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$eareaname		= $this->input->post('eareaname', TRUE);
			$ispbpo			= $this->input->post('ispbpo', TRUE);
			$nspbtoplength	= $this->input->post('nspbtoplength', TRUE);
			$isalesman		= $this->input->post('isalesman',TRUE);
			$esalesmanname	= $this->input->post('esalesmanname',TRUE);
			$ipricegroup	= $this->input->post('ipricegroup',TRUE);
			$inota			= $this->input->post('inota',TRUE);
			$dspbreceive	= $this->input->post('dspbreceive',TRUE);
			if($ispbpo!='')
				$fspbop			= 't';
			else
				$fspbop			= 'f';
			$ecustomerpkpnpwp	= $this->input->post('ecustomerpkpnpwp',TRUE);
			if($ecustomerpkpnpwp!='')
				$fspbpkp		= 't';
			else
				$fspbpkp		= 'f';
			$fspbconsigment		= $this->input->post('fspbconsigment',TRUE);
			if($fspbconsigment!='')
				$fspbconsigment="t";
			else
				$fspbconsigment="f";
			$fspbplusppn		= $this->input->post('fspbplusppn',TRUE);
			$fspbplusdiscount	= $this->input->post('fspbplusdiscount',TRUE);
			$fspbstockdaerah	= $this->input->post('fspbstockdaerah',TRUE);
			if($fspbstockdaerah!='')
				$fspbstockdaerah= 't';
			else
				$fspbstockdaerah= 'f';
			$fspbprogram	= 'f';
			$fspbvalid		= 't';
			$fspbsiapnota	= 'f';
			$fspbcancel		= 'f';
			$nspbtoplength	= $this->input->post('nspbtoplength',TRUE);
			$nspbdiscount1	= $this->input->post('ncustomerdiscount1',TRUE);
			$nspbdiscount2	= $this->input->post('ncustomerdiscount2',TRUE);
			$nspbdiscount3	= $this->input->post('ncustomerdiscount3',TRUE);
			$nspbdiscount4	= $this->input->post('ncustomerdiscount4',TRUE);
			$vspbdiscount1	= $this->input->post('vcustomerdiscount1',TRUE);
			$vspbdiscount2	= $this->input->post('vcustomerdiscount2',TRUE);
			$vspbdiscount3	= $this->input->post('vcustomerdiscount3',TRUE);
			$vspbdiscount4	= $this->input->post('vcustomerdiscount4',TRUE);
			$vspbdiscounttotal	= $this->input->post('vspbdiscounttotal',TRUE);
			$vspb			= $this->input->post('vspb',TRUE);
			$nspbdiscount1	= str_replace(',','',$nspbdiscount1);
			$nspbdiscount2	= str_replace(',','',$nspbdiscount2);
			$nspbdiscount3	= str_replace(',','',$nspbdiscount3);
			$nspbdiscount4	= str_replace(',','',$nspbdiscount4);
			$vspbdiscount1	= str_replace(',','',$vspbdiscount1);
			$vspbdiscount2	= str_replace(',','',$vspbdiscount2);
			$vspbdiscount3	= str_replace(',','',$vspbdiscount3);
			$vspbdiscount4	= str_replace(',','',$vspbdiscount4);
			$vspbdiscounttotal	= str_replace(',','',$vspbdiscounttotal);
			$ispbold	= $this->input->post('ispbold',TRUE);
			$vspb		= str_replace(',','',$vspb);
			$jml		= $this->input->post('jml', TRUE);
			if(($ecustomername!='') && ($ispb!=''))
			{
				$benar="false";
				$this->db->trans_begin();
				$this->load->model('spbpromo/mmaster');
				$this->mmaster->updateheader($ispb, $iarea, $dspb, $icustomer, $ispbpo, $nspbtoplength, $isalesman, 
						 $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp, 
						 $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
						 $fspbsiapnota, $fspbcancel, $nspbdiscount1, $nspbdiscount2, $nspbdiscount3, $nspbdiscount4, 
						 $vspbdiscount1, $vspbdiscount2, $vspbdiscount3, $vspbdiscount4, $vspbdiscounttotal, $vspb, $fspbconsigment,$ispbold);
				for($i=1;$i<=$jml;$i++){
				  $iproduct					=$this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade			='A';
				  $iproductmotif			=$this->input->post('motif'.$i, TRUE);
				  $eproductname				=$this->input->post('eproductname'.$i, TRUE);
				  $vunitprice				=$this->input->post('vproductretail'.$i, TRUE);
				  $vunitprice				=str_replace(',','',$vunitprice);
				  $norder				=$this->input->post('norder'.$i, TRUE);
				  $eremark				= $this->input->post('eremark'.$i, TRUE);
				  $data['iproduct']			=$iproduct;
				  $data['iproductgrade']	=$iproductgrade;
				  $data['iproductmotif']	=$iproductmotif;
				  $data['eproductname']		=$eproductname;
				  $data['vunitprice']		=$vunitprice;
				  $data['norder']			=$norder;
				  $this->mmaster->deletedetail($ispb, $iarea, $iproduct, $iproductgrade, $iproductmotif);
				  if($norder>0){
				    $this->mmaster->insertdetail( $ispb,$iarea,$iproduct,$iproductgrade,$eproductname,$norder,
											$vunitprice,$iproductmotif,$eremark);
				  }
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();
					$data['sukses']			= true;
					$data['inomor']			= $ispb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb	= $this->input->post('ispbdelete');
			$iarea	= $this->input->post('iareadelete');
			$this->load->model('spbpromo/mmaster');
			$this->mmaster->delete($ispb,$iarea);
			$data['page_title'] = $this->lang->line('master_spb');
			$data['ispb']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua($iarea);
			$this->load->view('spbpromo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb				= $this->uri->segment(4);
			$iarea				= $this->uri->segment(5);
			$iproduct			= $this->uri->segment(6);
  			$iproductgrade		= $this->uri->segment(7);
			$iproductmotif		= $this->uri->segment(8);
			$vspbdiscount1		= $this->uri->segment(9);
			$vspbdiscount2		= $this->uri->segment(10);
			$vspbdiscount3		= $this->uri->segment(11);
			$vspbdiscount4		= $this->uri->segment(12);
			$vspbdiscounttotal	= $this->uri->segment(13);
			$vspb				= $this->uri->segment(14);
			$ipromo				= $this->uri->segment(15);
			$vspbdiscount1		= str_replace(',','',$vspbdiscount1);
			$vspbdiscount2		= str_replace(',','',$vspbdiscount2);
			$vspbdiscount3		= str_replace(',','',$vspbdiscount3);
			$vspbdiscount4		= str_replace(',','',$vspbdiscount4);
			$vspbdiscounttotal	= str_replace(',','',$vspbdiscounttotal);
			$vspb				= str_replace(',','',$vspb);
//			$this->db->trans_begin();
			$this->load->model('spbpromo/mmaster');
			$this->mmaster->uphead($ispb, $iarea, $vspbdiscount1, $vspbdiscount2, $vspbdiscount3, $vspbdiscount4, $vspbdiscounttotal, $vspb);
			$this->mmaster->deletedetail($ispb, $iarea, $iproduct, $iproductgrade, $iproductmotif);
/*			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
*/				$data['page_title'] = $this->lang->line('spbpromo')." Update";
				$query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispb'] = $ispb;
				$data['iarea'] = $iarea;
				$data['ipromo'] = $ipromo;
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea);
		 		$this->load->view('spbpromo/vmainform',$data);
//			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$kdharga=$this->uri->segment(5);
			$promo	=$this->uri->segment(6);
			$q 		= $this->db->query("select * from tm_promo where i_promo = '$promo'",false);
			foreach($q->result() as $pro){
				$p	= $pro->f_all_product;
				$tipe	= $pro->i_promo_type;
			}
			$config['base_url'] = base_url().'index.php/spbpromo/cform/product/'.$baris.'/'.$kdharga.'/'.$promo.'/index/';
			if($p=='f'){
				if($tipe=='1'){
				$query = $this->db->query(" 	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama, d.v_product_retail as harga
								from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_price d
								where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
								and a.i_product=c.i_product 
								and d.i_product=a.i_product and d.i_price_group='$kdharga'
							   	and b.i_promo='$promo'" ,false);
				}else{
				$query = $this->db->query(" 	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama, b.v_unit_price as harga
								from tr_product_motif a,tm_promo_item b,tr_product c
								where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
								and a.i_product=c.i_product 
							   	and b.i_promo='$promo'" ,false);
				}
			}else{
				$query = $this->db->query(" 	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama,b.v_product_retail as harga
								from tr_product_motif a,tr_product_price b,tr_product c
								where b.i_product=a.i_product
								and a.i_product=c.i_product
							   	and b.i_price_group='$kdharga'" ,false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('spbpromo/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(8),$kdharga,$promo,$tipe);
			$data['baris']=$baris;
			$data['kdharga']=$kdharga;
			$data['promo']=$promo;
			$this->load->view('spbpromo/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$kdharga=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/spbpromo/cform/product/'.$baris.'/'.$kdharga.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("	select a.i_product||a.i_product_motif as kode, 
							c.e_product_name as nama,b.v_product_retail as harga
							from tr_product_motif a,tr_product_price b,tr_product c
							where b.i_product=a.i_product
							and a.i_product=c.i_product
						   	and b.i_price_group='$kdharga'
							and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
									  ,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('spbpromo/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$kdharga,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$data['kdharga']=$kdharga;
			$this->load->view('spbpromo/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$kdharga=$this->uri->segment(5);
			$promo	=$this->uri->segment(6);
			$q 		= $this->db->query("select * from tm_promo where i_promo = '$promo'",false);
			foreach($q->result() as $pro){
				$p		= $pro->f_all_product;
				$tipe	= $pro->i_promo_type;
			}
			$config['base_url'] = base_url().'index.php/spbpromo/cform/productupdate/'.$baris.'/'.$kdharga.'/'.$promo.'/index/';
			if($p=='f'){
				if($tipe=='1'){
				$query = $this->db->query(" 	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama, d.v_product_retail as harga
								from tr_product_motif a,tm_promo_item b,tr_product c, tr_product_price d
								where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
								and a.i_product=c.i_product 
								and d.i_product=a.i_product and d.i_price_group='$kdharga'
							   	and b.i_promo='$promo'" ,false);
				}else{
				$query = $this->db->query(" 	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama,b.v_unit_price as harga
								from tr_product_motif a,tm_promo_item b,tr_product c
								where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
								and a.i_product=c.i_product
							   	and b.i_promo='$promo'"
							  	,false);
				}
			}else{
				$query = $this->db->query(" 	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama,b.v_product_retail as harga
								from tr_product_motif a,tr_product_price b,tr_product c
								where b.i_product=a.i_product
								and a.i_product=c.i_product
							   	and b.i_price_group='$kdharga'"
								,false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('spbpromo/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(8),$kdharga,$promo);
			$data['baris']=$baris;
			$data['kdharga']=$kdharga;
			$data['promo']=$promo;
			$this->load->view('spbpromo/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$data['baris']=$this->uri->segment(4);
			$baris	=$this->uri->segment(4);
			$kdharga=$this->uri->segment(5);
			$promo	=$this->uri->segment(6);
			$q 		= $this->db->query("select * from tm_promo where i_promo = '$promo'",false);
			foreach($q->result() as $pro){
				$p	= $pro->f_all_product;
			}
			$config['base_url'] = base_url().'index.php/spbpromo/cform/productupdate/'.$baris.'/'.$kdharga.'/'.$promo.'/index/';
			if($p=='f'){
				$query = $this->db->query(" select a.i_product||a.i_product_motif as kode, 
											c.e_product_name as nama,b.v_unit_price as harga
											from tr_product_motif a,tm_promo_item b,tr_product c
											where b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
											and a.i_product=c.i_product
										   	and b.i_promo='$promo' and upper(c.i_product) like '%$cari%'"
										  ,false);
			}else{
				$query = $this->db->query(" select a.i_product||a.i_product_motif as kode, 
											c.e_product_name as nama,b.v_product_retail as harga
											from tr_product_motif a,tr_product_price b,tr_product c
											where b.i_product=a.i_product and upper(c.i_product) like '%$cari%'
											and a.i_product=c.i_product
										   	and b.i_price_group='$kdharga'"
										  ,false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('spbpromo/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproductupdate($cari,$config['per_page'],$this->uri->segment(8),$kdharga,$promo);
			$data['baris']=$baris;
			$data['kdharga']=$kdharga;
			$data['promo']=$promo;
			$this->load->view('spbpromo/vlistproductupdate', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$promo	= $this->uri->segment(4);			
			$config['base_url'] = base_url().'index.php/spbpromo/cform/area/'.$promo.'/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$q 		= $this->db->query("select * from tm_promo where i_promo = '$promo'",false);
			foreach($q->result() as $pro){
				$c	= $pro->f_all_customer;
				$g	= $pro->f_customer_group;
			}
			if(($c=='f') && ($g=='f')){
				$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
											from tm_promo_customer a, tr_area b
											where a.i_promo='$promo' and a.i_area=b.i_area 
											and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
										   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
			}else if(($c=='t') && ($g=='f')){
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}else if(($c=='f') && ($g=='t')){
				$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
											from tm_promo_customergroup a, tr_area b
											where a.i_promo='$promo' and a.i_area=b.i_area 
											and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
										   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('spbpromo/mmaster');
			$data['promo']=$promo;
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6),$area1,$area2,$area3,$area4,$area5,$promo,$c,$g);
			$this->load->view('spbpromo/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$promo	= $this->uri->segment(4);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/spbpromo/cform/area/'.$promo.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$q 		= $this->db->query("select * from tm_promo where i_promo = '$promo'",false);
			foreach($q->result() as $pro){
				$c	= $pro->f_all_customer;
				$g	= $pro->f_customer_group;
			}
			if(($c=='f') && ($g=='f')){
			$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
										from tm_promo_customer a, tr_area b
										where a.i_promo='$promo' and a.i_area=b.i_area 
										and (upper(b.e_area_name) like '%$cari%' or upper(b.i_area) like '%$cari%')
										and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
									   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
			}else if(($c=='t') && ($g=='f')){
				$query = $this->db->query("	select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')
											and i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   	or i_area = '$area4' or i_area = '$area5'",false);
			}else if(($c=='f') && ($g=='t')){
				$query = $this->db->query("	select distinct(a.i_area) as i_area, b.* 
											from tm_promo_customergroup a, tr_area b
											where a.i_promo='$promo' and a.i_area=b.i_area 
											and (upper(b.e_area_name) like '%$cari%' or upper(b.i_area) like '%$cari%')
											and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
										   	or a.i_area = '$area4' or a.i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('spbpromo/mmaster');
			$data['promo']=$promo;
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6),$area1,$area2,$area3,$area4,$area5,$promo,$c,$g);
			$this->load->view('spbpromo/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area=$this->session->userdata('i_area');
			$config['base_url'] = base_url().'index.php/spbpromo/cform/store/index/';
			$query = $this->db->query("select * from tr_area where i_area = '$area' or i_area='00'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('spbpromo/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5),$area);
			$this->load->view('spbpromo/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spbpromo/cform/store/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_store
						   where upper(i_store) like '%$cari%' or upper(e_store_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spbpromo/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('spbpromo/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$promo	= $this->uri->segment(4);
			$iarea = $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/spbpromo/cform/customer/'.$promo.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$q 		= $this->db->query("select * from tm_promo where i_promo = '$promo'",false);
			foreach($q->result() as $pro){
				$c		= $pro->f_all_customer;
				$g		= $pro->f_customer_group;
				$type	= $pro->i_promo_type;
				$disc1	= $pro->n_promo_discount1;
				$disc2	= $pro->n_promo_discount2;
			}
			if(($c=='f') && ($g=='f')){
			$query = $this->db->query("	select b.* 
										from tm_promo_customer a, tr_customer b
										where a.i_promo='$promo' and a.i_customer=b.i_customer 
										and a.i_area = '$iarea' ",false);
			}else if(($c=='t') && ($g=='f')){
				$query = $this->db->query("	select * from tr_customer where i_area = '$iarea'",false);
			}else if(($c=='f') && ($g=='t')){
				$query = $this->db->query("	select b.* 
											from tm_promo_customergroup a, tr_customer b
											where a.i_promo='$promo' and a.i_customer_group=b.i_customer_group
											and a.i_area = '$iarea' ",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('spbpromo/mmaster');
			$data['promo']=$promo;
			$data['type']=$type;
			$data['disc1']=$disc1;
			$data['disc2']=$disc2;
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$config['per_page'],$this->uri->segment(7),$promo,$c,$g,$type);
			$data['iarea']=$iarea;
			$this->load->view('spbpromo/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$promo	= $this->uri->segment(4);
			$iarea 	= $this->uri->segment(5);
			$cari	= $this->input->post("cari");
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/spbpromo/cform/customer/'.$promo.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$q 		= $this->db->query("select * from tm_promo where i_promo = '$promo'",false);
			foreach($q->result() as $pro){
				$c		= $pro->f_all_customer;
				$g		= $pro->f_customer_group;
				$type	= $pro->i_promo_type;
				$disc1	= $pro->n_promo_discount1;
				$disc2	= $pro->n_promo_discount2;
			}
			if(($c=='f') && ($g=='f')){
			$query = $this->db->query("	select b.* 
										from tm_promo_customer a, tr_customer b
										where a.i_promo='$promo' and a.i_customer=b.i_customer 
										and a.i_area = '$iarea' and (upper(b.i_customer) like '$cari' 
										or upper(b.e_customer_name) like '%$cari%') ",false);
			}else if(($c=='t') && ($g=='f')){
				$query = $this->db->query("	select * from tr_customer where i_area = '$iarea' and (upper(i_customer) like '$cari' 
											or upper(e_customer_name) like '%$cari%')",false);
			}else if(($c=='f') && ($g=='t')){
				$query = $this->db->query("	select b.* 
											from tm_promo_customergroup a, tr_customer b
											where a.i_promo='$promo' and a.i_customer_group=b.i_customer_group
											and a.i_area = '$iarea' and (upper(b.i_customer) like '$cari' 
											or upper(b.e_customer_name) like '%$cari%')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('spbpromo/mmaster');
			$data['promo']=$promo;
			$data['type']=$type;
			$data['disc1']=$disc1;
			$data['disc2']=$disc2;
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(7),$promo,$c,$g,$type);
			$data['iarea']=$iarea;
			$this->load->view('spbpromo/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$iarea 	= $this->input->post('iarea', FALSE);
			$config['base_url'] = base_url().'index.php/spbpromo/cform/index/';
			$query 	= $this->db->query("select * from tm_spb
						   where i_area='$iarea' 
							 and (upper(i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 

			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('spbpromo/mmaster');
			$data['isi']=$this->mmaster->cari($iarea,$cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spb');
			$data['ispb']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('spbpromo/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function promo()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dspb = $this->uri->segment(4);
			if($dspb!=''){
				$tmp=explode("-",$dspb);
				if(strlen($tmp[2])==4){
					$th=$tmp[2];
					$bl=$tmp[1];
					$hr=$tmp[0];
					$dspb=$th."-".$bl."-".$hr;
				}
			}
			$config['base_url'] = base_url().'index.php/spbpromo/cform/promo/'.$dspb.'/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tm_promo where d_promo_start<='$dspb' and d_promo_finish>='$dspb' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('spbpromo/mmaster');
			$data['page_title'] = $this->lang->line('list_promo');
			$data['isi']=$this->mmaster->bacapromo($dspb,$config['per_page'],$this->uri->segment(5));
			$data['dspb']=$dspb;
			$this->load->view('spbpromo/vlistpromo', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caripromo()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu91')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dspb = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/spbpromo/cform/promo/'.$dspb.'/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tm_promo where d_promo_start<='$dspb' and d_promo_finish>='$dspb'
										and (upper(i_promo) like '%$cari%' or upper(e_promo_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spbpromo/mmaster');
			$data['page_title'] = $this->lang->line('list_promo');
			$data['isi']=$this->mmaster->caripromo($cari, $dspb,$config['per_page'],$this->uri->segment(5));
			$data['dspb']=$dspb;
			$this->load->view('spbpromo/vlistpromo', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
