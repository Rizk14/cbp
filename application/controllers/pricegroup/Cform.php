<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu28')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_pricegroup');
			$data['ipricegroup']='';
			$this->load->model('pricegroup/mmaster');
			$data['isi']=$this->mmaster->bacasemua();

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Kelompok Harga";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('pricegroup/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu28')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ipricegroup 	= $this->input->post('ipricegroup', TRUE);
			$epricegroupname 	= $this->input->post('epricegroupname', TRUE);

			if ((isset($ipricegroup) && $ipricegroup != '') && (isset($epricegroupname) && $epricegroupname != ''))
			{
				$this->load->model('pricegroup/mmaster');
				$this->mmaster->insert($ipricegroup,$epricegroupname);
				$data['page_title'] = $this->lang->line('master_pricegroup');
				$data['ipricegroup']='';
				$data['isi']=$this->mmaster->bacasemua();

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Kelompok Harga:('.$ipricegroup.') -'.$epricegroupname;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

				$data['page_title'] = $this->lang->line('master_pricegroup');
				$data['ipricegroup']='';
				$data['isi']=$this->mmaster->bacasemua();
				$this->load->view('pricegroup/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu28')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_pricegroup');
			$this->load->view('pricegroup/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu28')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_pricegroup')." update";
			if($this->uri->segment(4)){
				$ipricegroup = $this->uri->segment(4);
				$data['ipricegroup'] = $ipricegroup;
				$this->load->model('pricegroup/mmaster');
				$data['isi']=$this->mmaster->baca($ipricegroup);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Kelompok Harga:('.$ipricegroup.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('pricegroup/vmainform',$data);
			}else{
				$this->load->view('pricegroup/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu28')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ipricegroup		= $this->input->post('ipricegroup', TRUE);
			$epricegroupname 	= $this->input->post('epricegroupname', TRUE);
			$this->load->model('pricegroup/mmaster');
			$this->mmaster->update($ipricegroup,$epricegroupname);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Kelompok Harga:('.$ipricegroup.') -'.$epricegroupname;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

	        $data['page_title'] = $this->lang->line('master_pricegroup');
			$data['ipricegroup']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('pricegroup/vmainform', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu28')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ipricegroup	= $this->uri->segment(4);
			$this->load->model('pricegroup/mmaster');
			$this->mmaster->delete($ipricegroup);

			$data['page_title'] = $this->lang->line('master_pricegroup');
			$data['ipricegroup']='';
			$data['isi']=$this->mmaster->bacasemua();

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
			  $ip_address	  = $row['ip_address'];
			  break;
			}
			}else{
			$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
			}
			$pesan='Delete Master Kelompok Harga:'.$ipricegroup;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);
			
			$this->load->view('pricegroup/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
