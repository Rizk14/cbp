<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu174')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listikhpkeluar');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listikhpkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu174')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	= $this->session->userdata('i_area');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listikhpkeluar/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      if($cari==''){
		    $query = $this->db->query(" select	a.i_ikhp, a.i_area, a.d_bukti, a.i_bukti, a.i_ikhp_type, a.i_coa,a.v_terima_tunai,
									a.v_terima_giro, a.v_keluar_giro, a.v_keluar_tunai, b.e_area_name from tm_ikhp a, tr_area b
							    where a.i_area=b.i_area and a.i_area='$iarea' and
							    not (a.v_terima_giro=0 and a.v_keluar_giro=0 and a.v_keluar_tunai=0 and a.v_terima_tunai=0) and
							    a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
		    $query = $this->db->query(" select	a.i_ikhp, a.i_area, a.d_bukti, a.i_bukti, a.i_ikhp_type, a.i_coa, a.v_terima_tunai,
									a.v_terima_giro, a.v_keluar_giro, a.v_keluar_tunai, b.e_area_name from tm_ikhp a, tr_area b
							    where a.i_area=b.i_area and
							    not (a.v_terima_giro=0 and a.v_keluar_giro=0 and a.v_keluar_tunai=0 and a.v_terima_tunai=0) and
							    and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_bukti) like '%$cari%' or a.i_ikhp=$cari)
							    and a.i_area='$iarea' and
							    a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
      }
/*
			$query = $this->db->query(" select a.*, b.e_area_name from tm_ikhp a, tr_area b
										              where a.i_area=b.i_area
										              and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										              or upper(a.i_ikhp) like '%$cari%')
										              and a.i_area='$iarea' and
										              a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
										              a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
*/
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listikhpkeluar');
			$this->load->model('listikhpkeluar/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['area1']  = $area1;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data IKHP Keluaran Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('listikhpkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu174')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listikhpkeluar');
			$this->load->view('listikhpkeluar/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu174')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$iikhp	= $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(6);
			if($dto=='') $dto=$this->uri->segment(7);
			$this->load->model('listikhpkeluar/mmaster');
			$this->mmaster->delete($iikhp,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus IKHP Pengeluaran No:'.$iikhp.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$config['base_url'] = base_url().'index.php/listikhpkeluar/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.i_ikhp, a.i_area, a.d_bukti, a.i_bukti, a.i_ikhp_type, a.i_coa, a.v_terima_tunai,
										a.v_terima_giro, a.v_keluar_giro, a.v_keluar_tunai, b.e_area_name from tm_ikhp a, tr_area b, tr_ikhp_type c
										where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
										and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										or upper(a.i_ikhp) like '%$cari%' or upper(a.i_bukti) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listikhpkeluar');
			$this->load->model('listikhpkeluar/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']= $iarea;
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listikhpkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu174')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	= $this->session->userdata('i_area');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listikhpkeluar/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      if($cari==''){
		    $query=$this->db->query("	select a.i_ikhp, a.i_area, a.d_bukti, a.i_bukti, a.i_ikhp_type, a.i_coa, a.v_terima_tunai,
									a.v_terima_giro, a.v_keluar_giro, a.v_keluar_tunai, b.e_area_name from tm_ikhp a, tr_area b, tr_ikhp_type c 
							    where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
							    and a.i_area='$iarea' and
							    not (a.v_terima_giro=0 and a.v_keluar_giro=0 and a.v_keluar_tunai=0 and a.v_terima_tunai=0) and
							    a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
		    $query=$this->db->query(" select	a.i_ikhp, a.i_area, a.d_bukti, a.i_bukti, a.i_ikhp_type, a.i_coa, a.v_terima_tunai,
									a.v_terima_giro, a.v_keluar_giro, a.v_keluar_tunai, b.e_area_name from tm_ikhp a, tr_area b, tr_ikhp_type c
							    where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
							    and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							    or upper(a.i_bukti) like '%$cari%')
							    and a.i_area='$iarea' and
							    not (a.v_terima_giro=0 and a.v_keluar_giro=0 and a.v_keluar_tunai=0 and a.v_terima_tunai=0) and
							    a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listikhpkeluar');
			$this->load->model('listikhpkeluar/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['iarea1']  = $area1;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listikhpkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu174')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listikhpkeluar/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listikhpkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listikhpkeluar/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu174')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listikhpkeluar/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listikhpkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listikhpkeluar/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
