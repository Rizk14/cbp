<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
//		$this->load->library('paginationxx');
	}
	function index()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/spbsiapnota/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query(" select distinct(b.i_spb) as no ,a.* from tm_spb_item b, tm_spb a
										inner join tr_customer 
											on (a.i_customer=tr_customer.i_customer 
												and(upper(tr_customer.e_customer_name) like '%$cari%' 
												  	or upper(tr_customer.i_customer) like '%$cari%')) 
										inner join tr_salesman 
											on (a.i_salesman=tr_salesman.i_salesman) 
										inner join tr_customer_area 
											on (a.i_customer=tr_customer_area.i_customer) 
										inner join tr_price_group 
											on (a.i_price_group=tr_price_group.i_price_group) 
										where not a.i_approve1 isnull
										and not a.i_approve2 isnull
										and not a.i_store isnull
										and not a.i_store_location isnull
										and a.f_spb_op = 'f'
										and upper(a.i_spb) like '%$cari%' 
										and a.i_spb=b.i_spb and b.n_order>b.n_deliver",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			
			$data['page_title'] = $this->lang->line('spbsiapnota');
			$data['ispb']='';
			$data['jmlitem'] = ''; 				
			$data['detail']='';
			$this->load->model('spbsiapnota/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('spbsiapnota/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('master_spb');
			$this->load->view('spbsiapnota/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('spbsiapnota');
			if(
				($this->uri->segment(4)!='')&&($this->uri->segment(5)!='')
			  )
			{
				$ispb 	= $this->uri->segment(4);
				$iarea	= $this->uri->segment(5);
				$query 	= $this->db->query("select * from tm_spb_item where i_spb='$ispb' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows();
				$data['ispb'] = $ispb;
				$this->load->model('spbsiapnota/mmaster');
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea);
		 		$this->load->view('spbsiapnota/vmainform',$data);
			}else{
				$this->load->view('spbsiapnota/vinsert_fail',$data);
				//$this->load->view('awal/index.php');
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if ($this->session->userdata('logged_in')){
			$ispb 			= $this->input->post('ispb', TRUE);
			$iarea 			= $this->input->post('iarea', TRUE);
			$fspbsiapnota	= 't';
			$benar			= 'false';
			$this->db->trans_begin();
			$this->load->model('spbsiapnota/mmaster');
			$this->mmaster->updateheader($ispb,$fspbsiapnota,$iarea);
			$benar="true";
			if ( ($this->db->trans_status() === FALSE) || ($benar=="false") )
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		=  $this->db->query($sql);
			  if($rs->num_rows>0){
				  foreach($rs->result() as $tes){
					  $ip_address	  = $tes->ip_address;
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }

			  $data['user']	= $this->session->userdata('user_id');
  #			$data['host']	= $this->session->userdata('printerhost');
			  $data['host']	= $ip_address;
			  $data['uri']	= $this->session->userdata('printeruri');
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='SPB Siap SJ No:'.$ispb.' Area:'.$iarea;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $ispb;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function isistore()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('spbsiapnota');
			if($this->input->post('ispbedit')){
				$ispb = $this->input->post('ispbedit');
				$query = $this->db->query("select * from tm_spb_item
							   where i_spb = '$ispb'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispb'] = $ispb;
				$this->load->model('spbsiapnota/mmaster');
				$data['isi']=$this->mmaster->baca($ispb);
				$data['detail']=$this->mmaster->bacadetail($ispb);
		 		$this->load->view('spbsiapnota/vmainform',$data);
			}else{
				$this->load->view('spbsiapnota/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if ($this->session->userdata('logged_in')){
			$area=$this->session->userdata('i_area');
			$data['jml']=$this->uri->segment(4);
			$jml =$this->uri->segment(4);
			$data['spb']=$this->uri->segment(5);
			$spb =$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/spbsiapnota/cform/store/'.$jml.'/'.$spb.'/index/';
			$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
									   where a.i_store in(select i_store from tr_area 
									   where i_area='$area' or i_area='00')
									   and a.i_store=b.i_store
									   order by a.i_store",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('spbsiapnota/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(7),$area);
			$this->load->view('spbsiapnota/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/spbsiapnota/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
									   where a.i_store in(select i_store from tr_area 
									   where i_area='$area' or i_area='00')
									   and a.i_store=b.i_store and upper(a.i_store) like '%$cari%' 
						      		   or upper(a.e_store_name) like '%$cari%' order by a.i_store",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spbsiapnota/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('spbsiapnota/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if ($this->session->userdata('logged_in')){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/spbsiapnota/cform/index/';
			$query = $this->db->query("select * from tm_spb 
									   where not i_approve1 isnull and not i_approve2 isnull
										and upper(i_spb) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('spbsiapnota/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spb');
			$data['ispb']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('spbsiapnota/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
