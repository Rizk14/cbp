<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationx');
      require_once("php/fungsi.php");
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu238')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['page_title'] = $this->lang->line('opnamenotanas');
         $data['dto']='';
         $data['nt']='';
         $data['jt']='';
         $this->load->view('exp-opnnotanas/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu238')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['page_title'] = $this->lang->line('opnamenotanas');
         $this->load->view('exp-opnnotanas/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu238')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
         {
            $this->load->model('exp-opnnotanas/mmaster');
            $dto  = $this->input->post('dto');
            $nt   = $this->input->post('chkntx');
            $jt   = $this->input->post('chkjtx');
            if($dto=='')$dto=$this->uri->segment(4);
            if($nt=='') $nt=$this->uri->segment(5);
            if($jt=='') $jt=$this->uri->segment(6);
            $this->load->model('exp-opnnotanas/mmaster');
            if($dto!='')
            {
               $tmp=explode("-",$dto);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $dto=$th."-".$bl."-".$hr;
               $peri=$hr.' '.mbulan($bl).' '.$th;
            }

            if($nt=='qqq')
            {
               $this->db->select(" xx.* from (
                                    select distinct
                                    a.*,b.e_salesman_name,
                                    c.e_customer_name, c.n_customer_toplength,
                                    x.e_pelunasan_remark, d.e_remark as remlunas,
                                    area.e_area_shortname,
                                    area.e_area_name, i.e_product_groupname,
                                  	case when substring(a.i_sj,9,2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                              	  		else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
                          	  			end as d_jatuh_tempo_plustoleransi,
                                  	case when substring(a.i_sj,9,2)='00' then g.n_toleransi_pusat
                              	  		else g.n_toleransi_cabang
                          	  			end as n_toleransi
											from tm_nota a
												left join tr_customer c on a.i_customer=c.i_customer 
												left join tr_city g on c.i_city = g.i_city and c.i_area = g.i_area
												left join tr_salesman b on a.i_salesman=b.i_salesman
												left join tm_spb h on a.i_spb=h.i_spb and a.i_area=h.i_area 
												left join tr_product_group i on h.i_product_group=i.i_product_group
												left join tr_area area on area.i_area=a.i_area
												left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
												left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
												left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area and e.f_pelunasan_cancel='f')

												left join tm_alokasi_item j on(a.i_nota=j.i_nota)
						                        left join tm_alokasi k on(j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area 
						                        and k.f_alokasi_cancel='f' and k.d_alokasi<='$dto')
						                        left join tm_alokasikn_item l on(a.i_nota=l.i_nota)
						                        left join tm_alokasikn m on(l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area 
						                        and m.f_alokasi_cancel='f' and m.d_alokasi<='$dto')
						                        left join tm_alokasiknr_item n on(a.i_nota=n.i_nota)
						                        left join tm_alokasiknr o on(n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area 
						                        and o.f_alokasi_cancel='f' and o.d_alokasi<='$dto')
						                        left join tm_alokasihl_item p on(a.i_nota=p.i_nota)
						                        left join tm_alokasihl q on(p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f' 
						                        and q.d_alokasi<='$dto')

                                 where a.d_nota<='$dto' and a.v_sisa>0 and a.i_salesman=b.i_salesman
                                       and a.f_nota_cancel='f'
                                       and h.f_spb_consigment='f'
                                 union all
                                    select distinct
                                    a.*,b.e_salesman_name,
                                    c.e_customer_name, c.n_customer_toplength,
                                    x.e_pelunasan_remark, d.e_remark as remlunas,
                                    area.e_area_shortname,
                                    area.e_area_name, 'Modern Outlet' as e_product_groupname,
                                  	case when substring(a.i_sj,9,2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                              	  		else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
                          	  			end as d_jatuh_tempo_plustoleransi,
                                  	case when substring(a.i_sj,9,2)='00' then g.n_toleransi_pusat
                              	  		else g.n_toleransi_cabang
                          	  			end as n_toleransi
											from tm_nota a
												left join tr_customer c on a.i_customer=c.i_customer 
												left join tr_city g on c.i_city = g.i_city and c.i_area = g.i_area
												left join tr_salesman b on a.i_salesman=b.i_salesman
												left join tm_spb h on a.i_spb=h.i_spb and a.i_area=h.i_area 
												left join tr_product_group i on h.i_product_group=i.i_product_group
												left join tr_area area on area.i_area=a.i_area
												left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
												left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
												left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area and e.f_pelunasan_cancel='f')

												left join tm_alokasi_item j on(a.i_nota=j.i_nota)
						                        left join tm_alokasi k on(j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area 
						                        and k.f_alokasi_cancel='f' and k.d_alokasi<='$dto')
						                        left join tm_alokasikn_item l on(a.i_nota=l.i_nota)
						                        left join tm_alokasikn m on(l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area 
						                        and m.f_alokasi_cancel='f' and m.d_alokasi<='$dto')
						                        left join tm_alokasiknr_item n on(a.i_nota=n.i_nota)
						                        left join tm_alokasiknr o on(n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area 
						                        and o.f_alokasi_cancel='f' and o.d_alokasi<='$dto')
						                        left join tm_alokasihl_item p on(a.i_nota=p.i_nota)
						                        left join tm_alokasihl q on(p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f' 
						                        and q.d_alokasi<='$dto')

                                 where a.d_nota<='$dto' and a.v_sisa>0 and a.i_salesman=b.i_salesman
                                       and a.f_nota_cancel='f'
                                       and h.f_spb_consigment='t'
                                 ) as xx
                                 order by xx.i_area, xx.e_customer_name, xx.i_nota desc",false);
            }
            else
            {
               $this->db->select(" xx.* from (
                                    select distinct
                                    a.*,b.e_salesman_name,
                                    c.e_customer_name, c.n_customer_toplength,
                                    x.e_pelunasan_remark, d.e_remark as remlunas,
                                    area.e_area_shortname,
                                    area.e_area_name, i.e_product_groupname,
                                  	case when substring(a.i_sj,9,2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                              	  		else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
                          	  			end as d_jatuh_tempo_plustoleransi,
                                  	case when substring(a.i_sj,9,2)='00' then g.n_toleransi_pusat
                              	  		else g.n_toleransi_cabang
                          	  			end as n_toleransi
											from tm_nota a
												left join tr_customer c on a.i_customer=c.i_customer 
												left join tr_city g on c.i_city = g.i_city and c.i_area = g.i_area
												left join tr_salesman b on a.i_salesman=b.i_salesman
												left join tm_spb h on a.i_spb=h.i_spb and a.i_area=h.i_area 
												left join tr_product_group i on h.i_product_group=i.i_product_group
												left join tr_area area on area.i_area=a.i_area
												left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
												left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
												left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area and e.f_pelunasan_cancel='f')

												left join tm_alokasi_item j on(a.i_nota=j.i_nota)
						                        left join tm_alokasi k on(j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area 
						                        and k.f_alokasi_cancel='f' and k.d_alokasi<='$dto')
						                        left join tm_alokasikn_item l on(a.i_nota=l.i_nota)
						                        left join tm_alokasikn m on(l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area 
						                        and m.f_alokasi_cancel='f' and m.d_alokasi<='$dto')
						                        left join tm_alokasiknr_item n on(a.i_nota=n.i_nota)
						                        left join tm_alokasiknr o on(n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area 
						                        and o.f_alokasi_cancel='f' and o.d_alokasi<='$dto')
						                        left join tm_alokasihl_item p on(a.i_nota=p.i_nota)
						                        left join tm_alokasihl q on(p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f' 
						                        and q.d_alokasi<='$dto')

                                 where a.d_jatuh_tempo<='$dto' and a.v_sisa>0
                                    and a.f_nota_cancel='f'
                                    and h.f_spb_consigment='f'
                                 union all
                                    select distinct
                                    a.*,b.e_salesman_name,
                                    c.e_customer_name, c.n_customer_toplength,
                                    x.e_pelunasan_remark, d.e_remark as remlunas,
                                    area.e_area_shortname,
                                    area.e_area_name, 'Modern Outlet' as e_product_groupname,
                                  	case when substring(a.i_sj,9,2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                              	  		else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
                          	  			end as d_jatuh_tempo_plustoleransi,
                                  	case when substring(a.i_sj,9,2)='00' then g.n_toleransi_pusat
                              	  		else g.n_toleransi_cabang
                          	  			end as n_toleransi
											from tm_nota a
												left join tr_customer c on a.i_customer=c.i_customer 
												left join tr_city g on c.i_city = g.i_city and c.i_area = g.i_area
												left join tr_salesman b on a.i_salesman=b.i_salesman
												left join tm_spb h on a.i_spb=h.i_spb and a.i_area=h.i_area 
												left join tr_product_group i on h.i_product_group=i.i_product_group
												left join tr_area area on area.i_area=a.i_area
												left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
												left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
												left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area and e.f_pelunasan_cancel='f')

												left join tm_alokasi_item j on(a.i_nota=j.i_nota)
						                        left join tm_alokasi k on(j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area 
						                        and k.f_alokasi_cancel='f' and k.d_alokasi<='$dto')
						                        left join tm_alokasikn_item l on(a.i_nota=l.i_nota)
						                        left join tm_alokasikn m on(l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area 
						                        and m.f_alokasi_cancel='f' and m.d_alokasi<='$dto')
						                        left join tm_alokasiknr_item n on(a.i_nota=n.i_nota)
						                        left join tm_alokasiknr o on(n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area 
						                        and o.f_alokasi_cancel='f' and o.d_alokasi<='$dto')
						                        left join tm_alokasihl_item p on(a.i_nota=p.i_nota)
						                        left join tm_alokasihl q on(p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f' 
						                        and q.d_alokasi<='$dto')
						                        
                                 where a.d_jatuh_tempo<='$dto' and a.v_sisa>0
                                    and a.f_nota_cancel='f'
                                    and h.f_spb_consigment='t'
                                 ) as xx
                                 order by xx.i_area, xx.e_customer_name, xx.i_nota desc",false);
            }
            $query = $this->db->get();
            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("Opname Nota")->setDescription(NmPerusahaan);
            $objPHPExcel->setActiveSheetIndex(0);
            if ($query->num_rows() > 0)
            {
               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'   => 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A2:A4'
               );
               $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
               $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
               $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
               $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(9);
               $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(9);
               $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(9);
               $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(30);
               $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
               
               $objPHPExcel->getActiveSheet()->setCellValue('A2', 'DAFTAR NOTA YANG BELUM LUNAS PER '.$peri);
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,16,2);
               $objPHPExcel->getActiveSheet()->setCellValue('A3', "Nasional");
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,16,3);

               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'   => 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A5:S5'
               );


               $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
               $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Area');
               $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Nama Area');
               $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('D5', 'KdLang');
               $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Nama');
               $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Salesman');
               $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('G5', 'TOP');
               $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('H5', 'No. SJ');
               $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('I5', 'No. Nota');
               $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Tgl. Nota');
               $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Tgl. JT');
               $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Tgl Opname');
               $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('M5', 'Jumlah');
               $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('N5', 'Sisa Tagihan');
               $objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('O5', 'Toleransi');
               $objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Lama');
               $objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Keterangan');
               $objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('R5', 'Status');
               $objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('S5', 'Divisi');
               $objPHPExcel->getActiveSheet()->getStyle('S5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $i=6;
               $j=6;
               $xarea='';
               $saldo=0;
               $no=1;
               $nonota='';
               $epelunasan_remark='';
               foreach($query->result() as $row)
               {
                  $objPHPExcel->getActiveSheet()->duplicateStyleArray
                  (
                     array(
                        'font' => array
                        (
                           'name'   => 'Arial',
                           'bold'   => false,
                           'italic' => false,
                           'size'   => 10
                        )
                     ),
                     'A'.$i.':S'.$i
                  );

                  $status=$row->v_nota_netto-$row->v_sisa;
                  if($status==0){
                    $ketstatus="Utuh";
                  }else{
                    $ketstatus="Sisa";
                  }

                  if( $nonota <> $row->i_nota)
                  {
                     //echo 'baris = '.$i.'<br>';
                     $lama=datediff("d",$row->d_jatuh_tempo,date("Y-m-d"),false);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $no, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_area.'-'.$row->e_area_shortname, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->e_area_name, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_customer, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->e_salesman_name, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->n_customer_toplength, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->i_sj, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->d_nota, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->d_jatuh_tempo, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $dto, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->v_sisa, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $row->n_toleransi, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $lama, Cell_DataType::TYPE_NUMERIC);
                    
                     if($lama>0 && $lama<=7){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':S'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF00FF00');
                     }elseif($lama>8 && $lama<=15){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':S'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
                     }elseif($lama>16){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':S'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF0000');
                     }
                     
                     if(trim($row->e_pelunasan_remark)!='' && $row->e_pelunasan_remark!=null)
                     {
                        $epelunasan_remark = $row->e_pelunasan_remark.",".$row->remlunas;
                        $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $epelunasan_remark);
                     }else{
                        $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $row->e_remark);
                     }
#                     {
#                        $epelunasan_remark = $row->e_pelunasan_remark.",".$row->e_remark;
#                        $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $epelunasan_remark);
#                     }
                     $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $ketstatus);
                     $objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $row->e_product_groupname);
                     $no++;
                     $i++;
                     $j++;
                  }
                  else
                  {
                     if($row->e_pelunasan_remark!='')
                     {
                        $k = $i-1;
                        $epelunasan_remark = $epelunasan_remark.",".$row->e_pelunasan_remark.",".$row->e_remark;
                        $objPHPExcel->getActiveSheet()->setCellValue('Q'.$k, $epelunasan_remark);
                     }
                  }
                  $nonota=$row->i_nota;
               }
               $x=$i-1;
            }
            $objPHPExcel->getActiveSheet()->getStyle('J6:K'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $nama='OPNALL'.$dto.'.xls';
            $area='00';
            if(file_exists('excel/00/'.$nama))
            {
               @chmod('excel/00/'.$nama, 0777);
               @unlink('excel/00/'.$nama);
            }
            $objWriter->save('excel/00/'.$nama);
            @chmod('excel/00/'.$nama, 0777);

               $sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
             while($row=pg_fetch_assoc($query)){
               $now    = $row['c'];
               }
               $pesan='Export Opname Nota Nasional sampai tanggal:'.$dto;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );

            $data['sukses']   = true;
            $data['inomor']   = "Ekspor Opname Nota Nasional berhasil ke file ".$nama;
            $this->load->view('nomor',$data);
         }
         else
         {
            $this->load->view('awal/index.php');
      }
   }
}
?>
