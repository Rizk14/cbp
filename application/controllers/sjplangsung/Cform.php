<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu156')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isjold	= $this->input->post('isjold', TRUE);
			$dsj 	= $this->input->post('dsj', TRUE);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$ispmb		= $this->input->post('ispmb', TRUE);
			$dspmb	 	= $this->input->post('dspmb', TRUE);
			if($dspmb!=''){
				$tmp=explode("-",$dspmb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspmb=$th."-".$bl."-".$hr;
			}
			$eareaname	= $this->input->post('eareaname', TRUE);
			$vspbnetto=$this->input->post('vsj', TRUE);
			$vspbnetto= str_replace(',','',$vspbnetto);
			$jml	  = $this->input->post('jml', TRUE);
			if($dsj!='' && $eareaname!='' && $ispmb!='')
			{
				$gaono=true;
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					if($cek=='on'){
						$gaono=false;
					}
					if(!$gaono) break;
				}
				if(!$gaono){
					$this->db->trans_begin();
					$this->load->model('sjplangsung/mmaster');
					$istore	  			= $this->input->post('istore', TRUE);
					if($istore=='AA'){
						$istorelocation		= '01';
					}else{
						$istorelocation		= '00';
					}
					$istorelocationbin	= '00';
					$isjtype			= '01';
					$isj		 		= $this->mmaster->runningnumbersj($iarea,$thbl);
					$this->mmaster->insertsjheader($ispmb,$dspmb,$isj,$dsj,$iarea,$vspbnetto,$isjold);
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
						if($cek=='on'){
						  $iproduct			= $this->input->post('iproduct'.$i, TRUE);
						  $iproductgrade= 'A';
						  $iproductmotif= $this->input->post('motif'.$i, TRUE);
						  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
						  $vunitprice		= str_replace(',','',$vunitprice);
						  $ndeliver			= $this->input->post('ndeliver'.$i, TRUE);
						  $ndeliver			= str_replace(',','',$ndeliver);
						  $norder		  	= $this->input->post('norder'.$i, TRUE);
						  $norder			  = str_replace(',','',$norder);
						  $eremark  		= $this->input->post('eremark'.$i, TRUE);
						  if($eremark=='')$eremark=null;
              if($norder==''||$ndeliver==''){
                  $this->mmaster->insertsjerror($iproduct,$iproductgrade,$iproductmotif,$eproductname,$norder,$ndeliver,
										                              $vunitprice,$ispmb,$dspmb,$isj,$dsj,$iarea,$istore,$istorelocation,
                                                  $istorelocationbin,$eremark,$i,$i);
              }else{
						    if($norder>0){
							    $this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$norder,$ndeliver,
										                              $vunitprice,$ispmb,$dspmb,$isj,$dsj,$iarea,$istore,$istorelocation,
                                                  $istorelocationbin,$eremark,$i,$i);
                  $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
                  if(isset($trans)){
                    foreach($trans as $itrans)
                    {
                      $q_aw =$itrans->n_quantity_awal;
                      $q_ak =$itrans->n_quantity_akhir;
                      $q_in =$itrans->n_quantity_in;
                      $q_out=$itrans->n_quantity_out;
                      break;
                    }
                  }else{
                    $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
                    if(isset($trans)){
                      foreach($trans as $itrans)
                      {
                        $q_aw =$itrans->n_quantity_stock;
                        $q_ak =$itrans->n_quantity_stock;
                        $q_in =0;
                        $q_out=0;
                        break;
                      }
                    }else{
                      $q_aw=0;
                      $q_ak=0;
                      $q_in=0;
                      $q_out=0;
                    }
                  }
                  $this->mmaster->inserttrans($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$isj,$q_in,$q_out,$ndeliver,$q_aw,$q_ak);
                  $th=substr($dsj,0,4);
                  $bl=substr($dsj,5,2);
                  $emutasiperiode=$th.$bl;
                  $ada=$this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$emutasiperiode);
                  if($ada=='ada')
                  {
                    $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$emutasiperiode);
                  }else{
                    $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$emutasiperiode);
                  }
                  if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00'))
                  {
                    $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$q_ak);
                  }else{
                    $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$ndeliver);
                  }
						    }
              }
						}
					}
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();
						$sess=$this->session->userdata('session_id');
						$id=$this->session->userdata('user_id');
						$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
						$rs		= pg_query($sql);
						if(pg_num_rows($rs)>0){
							while($row=pg_fetch_assoc($rs)){
								$ip_address	  = $row['ip_address'];
								break;
							}
						}else{
							$ip_address='kosong';
						}
						$query 	= pg_query("SELECT current_timestamp as c");
						while($row=pg_fetch_assoc($query)){
							$now	  = $row['c'];
						}
						$pesan='Input SJP No:'.$isj;
						$this->load->model('logger');
						$this->logger->write($id, $ip_address, $now , $pesan );
						$data['sukses']			= true;
						$data['inomor']			= $isj;
						$this->load->view('nomor',$data);
					}
				}
			}else{
				$this->load->model('sjplangsung/mmaster');
				$data['page_title'] = $this->lang->line('sjp');
				$data['isjp']				= '';
				$data['isi']				= '';
				$data['detail']			= "";
				$data['jmlitem']		= "";
				$data['dsjp']				= date('Y-m-d');
				$data['ispmb']			= '';
				$data['dspmb']			= '';
				$data['iarea']			= '';
				$data['istore']			= '';
				$data['isjold']			= '';
				$data['eareaname']	= $eareaname;
				$this->load->view('sjplangsung/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu156')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjp');
			$this->load->view('sjplangsung/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			 (($this->session->userdata('menu156')=='t') || ($this->session->userdata('menu158')=='t'))) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjp')." update";
			if($this->uri->segment(4)!=''){
        $area= $this->session->userdata('i_area');
				$isjp	= $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$dfrom= $this->uri->segment(6);
				$dto 	= $this->uri->segment(7);
				$ispmb= $this->uri->segment(8);
				$data['isjp'] 	= $isjp;
				$data['iarea']= $iarea;
        $data['area']= $area;
				$data['dfrom']= $dfrom;
				$data['dto']	= $dto;
				$query 	= $this->db->query("select * from tm_sjp_item where i_sjp = '$isjp' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('sjplangsung/mmaster');
				$data['isi']=$this->mmaster->baca($isjp,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isjp,$iarea,$ispmb);
		 		$this->load->view('sjplangsung/vmainform',$data);
			}else{
				$this->load->view('sjplangsung/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu156')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj	= $this->input->post('isj', TRUE);
			$isjold	= $this->input->post('isjold', TRUE);
			$dsj 	= $this->input->post('dsj', TRUE);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
				$thbl	= substr($th,2,2).$bl;
				$tmpsj	= explode("-",$isj);
				$firstsj= $tmpsj[0];
				$lastsj	= $tmpsj[2];
				$newsj	= $firstsj."-".$thbl."-".$lastsj;				
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$ispmb		= $this->input->post('ispmb', TRUE);
			$dspmb	 	= $this->input->post('dspmb', TRUE);
			if($dspmb!=''){
				$tmp=explode("-",$dspmb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspmb=$th."-".$bl."-".$hr;
			}
			$eareaname	= $this->input->post('eareaname', TRUE);
			$vspbnetto= $this->input->post('vsj', TRUE);
			$vspbnetto= str_replace(',','',$vspbnetto);
			$jml	  = $this->input->post('jml', TRUE);
			$gaono=true;
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$gaono=false;
				}
				if(!$gaono) break;
			}
			if(!$gaono){
				$this->db->trans_begin();
				$this->load->model('sjplangsung/mmaster');
				$istore	  			= $this->input->post('istore', TRUE);
				if($istore=='AA'){
					$istorelocation		= '01';
				}else{
					$istorelocation		= '00';
				}
				$istorelocationbin	= '00';
				$Qseachsjdaer	= $this->mmaster->searchsjheader($isj,$iarea);
				$nserachsjdaer	= $Qseachsjdaer->num_rows();
				if($nserachsjdaer<0 || $nserachsjdaer==0){				
					$this->mmaster->deletesjheader($isj,$iarea); 
					$this->mmaster->insertsjheader2($ispmb,$dspmb,$newsj,$dsj,$iarea,$vspbnetto,$isjold); 
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
						$iproduct		= $this->input->post('iproduct'.$i, TRUE);
						$iproductgrade	= 'A';
						$iproductmotif	= $this->input->post('motif'.$i, TRUE);
						$ndeliver		= $this->input->post('ndeliver'.$i, TRUE);
						$ndeliver		= str_replace(',','',$ndeliver);
						$this->mmaster->deletesjdetail( $ispmb,$isj,$iarea,$iproduct, $iproductgrade,$iproductmotif,$ndeliver);

						$th=substr($dsj,0,4);
						$bl=substr($dsj,5,2);
						$emutasiperiode=$th.$bl;
						$tra=$this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$isj);
					  $this->mmaster->updatemutasi01($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$emutasiperiode);
					  $this->mmaster->updateic01($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver);
						if($cek=='on'){
						  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
						  $vunitprice		= str_replace(',','',$vunitprice);
						  $norder		  	= $this->input->post('norder'.$i, TRUE);
						  $norder			  = str_replace(',','',$norder);
						  $eremark  		= $this->input->post('eremark'.$i, TRUE);
						  if($eremark=='')$eremark=null;
						  if($norder>0){
							  $this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$norder,$ndeliver,
                                               $vunitprice,$ispmb,$dspmb,$newsj,$dsj,$iarea,$istore,$istorelocation,
                                               $istorelocationbin,$eremark,$i);                        
							  $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
							  if(isset($trans)){
								foreach($trans as $itrans)
								{
								  $q_aw =$itrans->n_quantity_awal;
								  $q_ak =$itrans->n_quantity_akhir;
								  $q_in =$itrans->n_quantity_in;
								  $q_out=$itrans->n_quantity_out;
								  break;
								}
							  }else{
								$trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
								if(isset($trans)){
								  foreach($trans as $itrans)
								  {
									$q_aw =$itrans->n_quantity_stock;
									$q_ak =$itrans->n_quantity_stock;
									$q_in =0;
									$q_out=0;
									break;
								  }
								}else{
								  $q_aw=0;
								  $q_ak=0;
								  $q_in=0;
								  $q_out=0;
								}
							  }
							  $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$newsj,$q_in,$q_out,$ndeliver,$q_aw,$q_ak);
						    $th=substr($dsj,0,4);
						    $bl=substr($dsj,5,2);
						    $emutasiperiode=$th.$bl;
						    $ada=$this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$emutasiperiode);
						    if($ada=='ada')
						    {
    							$this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$emutasiperiode);
						    }else{
    							$this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$emutasiperiode);
						    }
						    if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00'))
						    {
    							$this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$q_ak);
						    }else{
    							$this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname);
						    }
						  }
						}
					}
					$sjnew=1;
				}else{	
					$this->mmaster->updatesjheader($isj,$iarea,$isjold,$dsj,$vspbnetto);
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
						$iproduct		= $this->input->post('iproduct'.$i, TRUE);
						$iproductgrade	= 'A';
						$iproductmotif	= $this->input->post('motif'.$i, TRUE);
						$ndeliver		= $this->input->post('ndeliver'.$i, TRUE);
						$ndeliver		= str_replace(',','',$ndeliver);
						$this->mmaster->deletesjdetail( $ispmb, $isj, $iarea, $iproduct, $iproductgrade, $iproductmotif, $ndeliver);

						$th=substr($dsj,0,4);
						$bl=substr($dsj,5,2);
						$emutasiperiode=$th.$bl;
            $this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$isj);
					  $this->mmaster->updatemutasi01($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$emutasiperiode);
					  $this->mmaster->updateic01($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver);
						if($cek=='on'){
						  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
						  $vunitprice		= str_replace(',','',$vunitprice);
						  $norder		  	= $this->input->post('norder'.$i, TRUE);
						  $norder			  = str_replace(',','',$norder);
						  $eremark  		= $this->input->post('eremark'.$i, TRUE);
						  if($eremark=='')$eremark=null;
						  if($norder>0){
							  $this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$norder,$ndeliver,
                                               $vunitprice,$ispmb,$dspmb,$isj,$dsj,$iarea,$istore,$istorelocation,
                                               $istorelocationbin,$eremark,$i);                      

							  $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');


							  if(isset($trans)){
								  foreach($trans as $itrans)
								  {
								    $q_aw =$itrans->n_quantity_awal;
								    $q_ak =$itrans->n_quantity_akhir;
								    $q_in =$itrans->n_quantity_in;
								    $q_out=$itrans->n_quantity_out;
								    break;
								  }
							  }else{
								  $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
								  if(isset($trans)){
								    foreach($trans as $itrans)
								    {
									    $q_aw =$itrans->n_quantity_stock;
									    $q_ak =$itrans->n_quantity_stock;
									    $q_in =0;
									    $q_out=0;
									    break;
								    }
								  }else{
								    $q_aw=0;
								    $q_ak=0;
								    $q_in=0;
								    $q_out=0;
								  }
							  }
							  
						    $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$isj,$q_in,$q_out,$ndeliver,$q_aw,$q_ak);
						    $th=substr($dsj,0,4);
						    $bl=substr($dsj,5,2);
						    $emutasiperiode=$th.$bl;
						    $ada=$this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$emutasiperiode);
						    if($ada=='ada')
						    {
  							  $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$emutasiperiode);
						    }else{
  							  $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$emutasiperiode);
						    }
						    if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00'))
						    {
  							  $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$q_ak);
						    }else{
  							  $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$ndeliver);
						    }
						  }
						}
					}
					$sjnew=0;
				}				
				if ( ($this->db->trans_status() === FALSE) )
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

          $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		=  $this->db->query($sql);
		      if($rs->num_rows>0){
			      foreach($rs->result() as $tes){
				      $ip_address	  = $tes->ip_address;
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }

		      $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
		      $data['host']	= $ip_address;
		      $data['uri']	= $this->session->userdata('printeruri');
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Update SJP Langsung No:'.$isj.' Area:'.$iarea;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $newsj;
					$this->load->view('nomor',$data);
				}
			}
    }
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu156')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj	= $this->input->post('isjdelete', TRUE);
			$this->load->model('sjplangsung/mmaster');
			$this->mmaster->delete($isj);
			$data['page_title'] = $this->lang->line('sjp');
			$data['isj']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('sjplangsung/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu156')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj			= $this->input->post('isjdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
  			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$this->db->trans_begin();
			$this->load->model('sjplangsung/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $isj, $iproductmotif);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
				$data['page_title'] = $this->lang->line('sjp')." Update";
				$data['jmlitem'] = 0; 				
				$data['isj'] = $isj;
				$data['isi']=$this->mmaster->baca($isj);
				$data['detail']=$this->mmaster->bacadetail($isj);
				$this->load->view('sjplangsung/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
  function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu156')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/sjplangsung/cform/area/index/';
			$query = $this->db->query("select * from tr_area ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('sjplangsung/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('sjplangsung/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu156')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/sjplangsung/cform/area/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query("select * from tr_area
						   where upper(i_area) like '%$cari%' 
						      or upper(e_area_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sjplangsung/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sjplangsung/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu156')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/sjplangsung/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('sjplangsung/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['isj']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('sjplangsung/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu156')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari = strtoupper($this->input->post('cari', FALSE));
      $baris = strtoupper($this->input->post('baris', FALSE));
 			if($baris=='') $baris=$this->uri->segment(4);
      if($this->uri->segment(5)!='x01'){
        if($cari=='') $cari=$this->uri->segment(5);
        $config['base_url'] = base_url().'index.php/sjplangsung/cform/product/'.$baris.'/'.$cari.'/';
      }else{
        $config['base_url'] = base_url().'index.php/sjplangsung/cform/product/'.$baris.'/x01/';
      }
			$query = $this->db->query(" select a.i_product, a.e_product_name, b.v_product_retail, c.i_product_motif, c.e_product_motifname 
                                  from tm_ic a, tr_product_price b, tr_product_motif c
                                  where a.i_product=b.i_product and b.i_price_group='00'
                                  and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                                  and a.i_product=c.i_product and a.i_product_motif=c.i_product_motif and a.i_store='AA'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
 			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('sjplangsung/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$this->uri->segment(4);
 			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('sjplangsung/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
