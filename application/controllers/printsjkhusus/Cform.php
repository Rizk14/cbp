<?php
class Cform extends CI_Controller
{
	public $folder = 'printsjkhusus';

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu104') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/printsjkhusus/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('printsj');
			$data['cari'] = '';
			$data['dfrom'] = '';
			$data['dto']  = '';
			$data['isi']  = '';
			$data['folder']  = $this->folder;
			$this->load->view('printsjkhusus/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu104') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			/*$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');*/
			$iuser	= $this->session->userdata('user_id');
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			$config['base_url'] = base_url() . 'index.php/printsjkhusus/cform/view/' . $dfrom . '/' . $dto . '/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			if ($iuser == 'user_id') {
				$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										                where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area 
										                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										                or upper(a.i_sj) like '%$cari%') and (substring(a.i_sj,9,2) in(select i_area from tm_user_area where i_user ='$iuser')
																	  and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')", false);
			} else {
				$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										                where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area 
										                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										                or upper(a.i_sj) like '%$cari%') and (substring(a.i_sj,9,2)in(select i_area from tm_user_area where i_user ='$iuser')
                                    or substring(a.i_sj,9,2)='BK')
																	  and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printsj');
			$this->load->model('printsjkhusus/mmaster');
			$data['cari'] = $cari;
			$data['dfrom'] = $dfrom;
			$data['dto'] = $dto;
			$data['folder']  = $this->folder;
			$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(6), $iuser, $dfrom, $dto);
			$this->load->view('printsjkhusus/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu104') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$isj  = $this->uri->segment(4);
			$area = substr($isj, 8, 2);
			$this->load->model('printsjkhusus/mmaster');
			$data['isj'] = $isj;
			$data['page_title'] = $this->lang->line('printsj');
			$data['isi'] = $this->mmaster->baca($isj, $area);
			$data['detail'] = $this->mmaster->bacadetail($isj, $area);

			// $this->mmaster->updatesj($isj, $area);

			$get = $this->db->query(" SELECT to_char(d_sj,'yyyymm') as periodesj FROM tm_nota WHERE i_sj = '$isj' ")->row();
			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if ($rs->num_rows > 0) {
				foreach ($rs->result() as $tes) {
					$ip_address	  = $tes->ip_address;
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$data['iarea']	= $area;
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Preview Cetak SJ Area:' . $area . ' No:' . $isj;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);
			/*if($area=='00')
			{
				$this->load->view('printsjkhusus/vformrpt',$data);
			}else{*/
			if ($get->periodesj > '202203') {
				$this->load->view('printsjkhusus/vformrptcab_newexc', $data);
			} else {
				$this->load->view('printsjkhusus/vformrptcab', $data);
			}
			//}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cetakinclude()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu104') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$isj  = $this->uri->segment(4);
			$area = substr($isj, 8, 2);
			$this->load->model('printsjkhusus/mmaster');
			$data['isj'] = $isj;
			$data['page_title'] = $this->lang->line('printsj');
			$data['isi'] = $this->mmaster->baca($isj, $area);
			$data['detail'] = $this->mmaster->bacadetail($isj, $area);
			$this->mmaster->updatesj($isj, $area);
			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if ($rs->num_rows > 0) {
				foreach ($rs->result() as $tes) {
					$ip_address	  = $tes->ip_address;
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$data['iarea']	= $area;
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Cetak SJ Area:' . $area . ' No:' . $isj;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);
			/*if($area=='00')
			{
				$this->load->view('printsjkhusus/vformrpt',$data);
			}else{*/
			$this->load->view('printsjkhusus/vformrptcabinc', $data);
			//}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu104') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('printsj');
			$this->load->view('printsjkhusus/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu104') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom = strtoupper($this->input->post('dfrom', FALSE));
			$dto 	= strtoupper($this->input->post('dto', FALSE));
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			$config['base_url'] = base_url() . 'index.php/printsjkhusus/cform/view/' . $dfrom . '/' . $dto . '/';
			if ($area1 != 'PB') {
				$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										                where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
										                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										                or upper(a.i_sj) like '%$cari%') and (substring(a.i_sj,9,2)='$area1' or substring(a.i_sj,9,2)='$area2'
										                or substring(a.i_sj,9,2) = '$area3' or substring(a.i_sj,9,2) = '$area4' or substring(a.i_sj,9,2)='$area5')
                                    and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')", false);
			} else {
				$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_nota a, tr_customer b, tr_area c
										                where a.i_customer=b.i_customer and substring(a.i_sj,9,2)=c.i_area
										                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										                or upper(a.i_sj) like '%$cari%') and (substring(a.i_sj,9,2)='$area1' or substring(a.i_sj,9,2)='$area2'
										                or substring(a.i_sj,9,2) = '$area3' or substring(a.i_sj,9,2) = '$area4' or substring(a.i_sj,9,2)='$area5'
                                    or substring(a.i_sj,9,2)='BK')
                                    and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('printsjkhusus/mmaster');
			$data['isi'] = $this->mmaster->cari($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5, $dfrom, $dto);
			$data['page_title'] = $this->lang->line('printsj');
			$data['cari'] = $cari;
			$data['dfrom'] = $dfrom;
			$data['dto'] = $dto;
			$data['isj'] = '';
			$data['folder']  = $this->folder;
			$data['detail'] = '';
			$this->load->view('printsjkhusus/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function update()
	{
		$isj  = $this->input->post('isj');

		$this->db->trans_begin();

		$this->load->model('printsjkhusus/mmaster');

		$qisj = $this->mmaster->updatesj($isj);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			echo 'fail';
		} else {
			$this->db->trans_commit();
			$this->logger->writenew($this->lang->line('printsj') . ' No SJ : ' . $isj);
			echo $isj;
		}
		redirect('printsjkhusus/cform');
	}

	public function undo()
	{
		$isj 		= $this->uri->segment(4);
		$iarea 		= $this->uri->segment(5);
		$dfrom 		= $this->uri->segment(6);
		$dto 		= $this->uri->segment(7);

		$this->load->model('printsjkhusus/mmaster');

		$this->db->trans_begin();

		$this->mmaster->reprint($isj, $iarea, $dfrom, $dto, $this->folder);

		if ($this->db->trans_status() === FALSE || $this->db->trans_status() === false) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
			$this->logger->writenew('Buka Akses Cetak Ulang SJ No : ' . $isj);
		}
	}
}
