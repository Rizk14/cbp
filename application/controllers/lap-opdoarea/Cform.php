<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu333')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('lap-opdo')." Per Area";
			$data['iperiode']	= '';
			$data['iarea']	= '';
			$this->load->view('lap-opdoarea/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu333')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);      
			$iarea	= $this->input->post('iarea');
			if($iarea=='') $iarea=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/lap-opdoarea/cform/view/'.$iperiode.'/'.$iarea.'/index/';
			$query = $this->db->query(" select sum(a.n_deliver) as pcsdo, sum(a.n_deliver*a.v_product_mill) as rpdo,
                                  sum(b.n_delivery) as pcsop, sum(b.n_delivery*b.v_product_mill) as rpop,
                                  a.i_product, a.e_product_name
                                  from tm_op_item b
                                  left join tm_do c on (b.i_op=c.i_op)
                                  left join tm_do_item a on (a.i_product=b.i_product and c.i_do=a.i_do)
                                  where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                                  and c.i_area='$iarea'
                                  group by a.i_product, a.e_product_name
                                  order by a.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(7);
			$this->paginationxx->initialize($config);

			$this->load->model('lap-opdoarea/mmaster');
			$data['page_title'] = $this->lang->line('lap-opdo')." Per Area";
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['isi']	= $this->mmaster->baca($iperiode,$iarea,$config['per_page'],$this->uri->segment(7),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Laporan OP vs DO Per Area:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('lap-opdoarea/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu333')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$iarea	= $this->input->post('iarea');
			if($iarea=='') $iarea=$this->uri->segment(5);
			$cari	= strtoupper($this->input->post('cari'));
			$config['base_url'] = base_url().'index.php/lap-opdoarea/cform/view/'.$iperiode.'/'.$iarea.'/index/';
			$query = $this->db->query(" select sum(a.n_deliver) as pcsdo, sum(a.n_deliver*a.v_product_mill) as rpdo,
                                  sum(b.n_delivery) as pcsop, sum(b.n_delivery*b.v_product_mill) as rpop,
                                  a.i_product, a.e_product_name
                                  from tm_op_item b
                                  left join tm_do c on (b.i_op=c.i_op)
                                  left join tm_do_item a on (a.i_product=b.i_product and c.i_do=a.i_do)
                                  where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                                  and c.i_area='$iarea' and(upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                                  group by a.i_product, a.e_product_name
                                  order by a.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);

			$this->load->model('lap-opdoarea/mmaster');
			$data['page_title'] = $this->lang->line('lap-opdo')." Per Area";
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['isi']	= $this->mmaster->cari($iperiode,$iarea,$cari,$config['per_page'],$this->uri->segment(7));
			$this->load->view('lap-opdoarea/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu333')=='t')) ||
		  (($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/lap-opdoarea/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
        else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('lap-opdoarea/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('lap-opdoarea/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu333')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			  ($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/lap-opdoarea/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('lap-opdoarea/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('lap-opdoarea/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
