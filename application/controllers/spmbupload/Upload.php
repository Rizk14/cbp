<?php

class Upload extends CI_Controller
{
	public $menu 	= "2006001F";
	public $title 	= "Upload SPmB";

	function Upload()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

	function index()
	{
		if ($this->session->userdata('logged_in')) {
			$this->logger->writenew("Membuka Menu : " . $this->menu . " - " . $this->title);
			$this->load->view('spmbupload/upload_form', array('error' => ' '));
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function spmb_upload()
	{
		if ($this->session->userdata('logged_in')) {
			$config['upload_path'] = './pstock/';
			$config['allowed_types'] = '*';
			$config['overwrite']	= 'TRUE';
			$config['max_size']	= '0';
			$this->load->library('upload', $config);
			$bener = false;
			$field = 'userfile';
			if (!$this->upload->do_upload($field)) {
				$error = array('error' => $this->upload->display_errors());
				$bener = false;
				$this->load->view('spbupload/upload_form', $error);
			} else {
				$bener = true;
			}
			if ($bener) {
				$data = array('upload_data' => $this->upload->data());
				$this->load->view('spbupload/upload_success', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
		/*
			$config['upload_path'] = './pstock/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '0';
			$config['overwrite']	= 'TRUE';
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload())
			{
				$error = array('error' => $this->upload->display_errors());
			
				$this->load->view('spmbupload/upload_form', $error);
			}	
			else
			{
				$data = array('upload_data' => $this->upload->data());
			
				$this->load->view('spmbupload/upload_success', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
*/
	}
}
