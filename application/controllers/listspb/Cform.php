<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$this->load->model('listspb/mmaster');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('listspb');
			$data['dfrom'] = '';
			$data['dto'] = '';
			$this->load->view('listspb/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('listspb');
			$this->load->view('listspb/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function si_delete()
	{
		$ispb  		= $this->uri->segment(4);
		$iarea 		= $this->uri->segment(5);
		$dfrom 		= $this->uri->segment(6);
		$dto	 	= $this->uri->segment(7);
		$iareax	 	= $this->uri->segment(8);
		$ireffadvo 	= $this->uri->segment(9); /* Referensi SPB di dgapps (13 Mei 2023) */

		$this->load->model('listspb/mmaster');

		$get = $this->db->query(" 	SELECT
										a.i_customer,
										e_customer_name
									FROM
										tm_spb a
									INNER JOIN tr_customer b ON
										a.i_customer = b.i_customer
										AND a.i_area = b.i_area
									WHERE 
										i_spb = '$ispb' AND a.i_area = '$iarea' ")->row();

		$data['page_title'] = $this->lang->line('listspb');
		$data['ispb']		= $ispb;
		$data['dfrom']		= $dfrom;
		$data['dto']		= $dto;
		$data['iarea'] 		= $iarea;
		$data['iareax']		= $iareax;
		$data['ireffadvo']	= $ireffadvo; /* Referensi SPB di dgapps (13 Mei 2023) */
		$data['icustomer']	= $get->i_customer;
		$data['ecustomer']	= $get->e_customer_name;

		$this->load->view('listspb/vdelete', $data);
	}

	function balik()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$ispb  		= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$dfrom 		= $this->uri->segment(6);
			$dto	 	= $this->uri->segment(7);
			$cari 		= '';


			$this->mmaster->balik($ispb, $iarea);

			$sess 	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);

			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Balik Status SPB No:' . $ispb . ' Area:' . $iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$data['sukses']			= true;
			$data['inomor']			= $ispb;
			$this->load->view('nomor', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			/* $ispb		= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$dfrom		= $this->uri->segment(6);
			$dto		= $this->uri->segment(7); */
			$ispb 		= $this->input->post('ispb');
			$iarea		= $this->input->post('iarea');
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$eremark 	= $this->input->post('eremark1') != "" ? $this->input->post('eremark1') : $this->input->post('eremark2');
			$ireffadvo	= $this->input->post('ireffadvo'); /* Referensi SPB di dgapps (13 Mei 2023) */

			$this->load->model('listspb/mmaster');

			$this->db->trans_begin();

			$this->mmaster->delete($ispb, $iarea, $eremark, $ireffadvo);

			if (($this->db->trans_status() === FALSE)) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();

				$que = $this->db->query(" select f_spb_consigment from tm_spb where i_spb='$ispb' and i_area='$iarea'");
				if ($que->num_rows() > 0) {
					foreach ($que->result() as $row) {
						$fspbkonsinyasi = $row->f_spb_consigment;
					}
					$this->mmaster->updatebon($ispb, $iarea);
				}

				$sess = $this->session->userdata('session_id');
				$id = $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {
						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {
					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {
					$now	  = $row['c'];
				}
				$pesan = 'Batal SPB No : ' . $ispb . ' Area : ' . $iarea;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$is_cari = $this->input->post('is_cari');
				$cari	= strtoupper($this->input->post('cari'));
				$config['base_url'] = base_url() . 'index.php/listspb/cform/view2/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';
				$sql	= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
				where a.i_customer=b.i_customer
				and a.i_area='$iarea' and
				(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
				a.d_spb <= to_date('$dto','dd-mm-yyyy')) order by a.i_spb desc ";
				$query = $this->db->query($sql, false);

				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(8);
				$this->pagination->initialize($config);

				$this->load->model('listspb/mmaster');
				$data['page_title'] = $this->lang->line('listspb');
				$data['cari']	= $cari;
				$data['dfrom']	= $dfrom;
				$data['dto']	= $dto;
				$data['iarea'] = $iarea;
				$data['isi']	= $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari);
				$this->load->view('listspb/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$cari	= strtoupper($this->input->post('cari'));
			if ($cari == '') {
				$cari = $this->uri->segment(7);
			}
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea = $this->uri->segment(6);

			$config['base_url'] = base_url() . 'index.php/listspb/cform/view2/' . $dfrom . '/' . $dto . '/' . $iarea . '/' . $cari . '/index/';


			$sql = " select a.*, b.e_customer_name from tm_spb a, tr_customer b
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%') 
					and a.i_area='$iarea' and
						(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and
						a.d_spb <= to_date('$dto','dd-mm-yyyy')) order by a.i_spb desc ";

			$query 	= $this->db->query($sql, false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);


			$data['page_title'] = $this->lang->line('listspb');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;
			$data['isi']	= $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(9), $cari);
			$this->load->view('listspb/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function paging()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($this->uri->segment(5) != '') {
				if ($this->uri->segment(4) != 'sikasep') {
					$cari = $this->uri->segment(4);
				} else {
					$cari = '';
				}
			} elseif ($this->uri->segment(4) != 'sikasep') {
				$cari = $this->uri->segment(4);
			} else {
				$cari = '';
			}
			if ($cari == '')
				$config['base_url'] = base_url() . 'index.php/listspb/cform/paging/sikasep/';
			else
				$config['base_url'] = base_url() . 'index.php/listspb/cform/paging/' . $cari . '/';
			if ($this->uri->segment(4) == 'sikasep')
				$cari = '';
			$cari	= $this->input->post('cari', TRUE);
			$cari  	= strtoupper($cari);
			if ($this->session->userdata('level') == '0') {
				$sql = " select a.*, b.e_customer_name from tm_spb a, tr_customer b
						where a.i_customer=b.i_customer
						and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						or upper(a.i_spb) like '%$cari%') order by a.i_spb desc ";
			} else {
				$sql = " select a.*, b.e_customer_name from tm_spb a, tr_customer b
						where a.i_customer=b.i_customer
						and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						or upper(a.i_spb) like '%$cari%')
						and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
						or a.i_area='$area5') order by a.i_spb desc ";
			}
			$query 	= $this->db->query($sql, false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('listspb');

			$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('listspb/vform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/listspb/cform/area/index/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);


			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('listspb/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/listspb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('listspb/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			$is_cari = $this->input->post('is_cari');
			$cari = str_replace("'", "", $cari);
			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea = $this->uri->segment(6);

			if ($is_cari == '')
				$is_cari = $this->uri->segment(8);
			if ($cari == '' && $is_cari == "1") $cari = $this->uri->segment(7);

			if ($is_cari == "1") {
				$config['base_url'] = base_url() . 'index.php/listspb/cform/view2/' . $dfrom . '/' . $dto . '/' . $iarea . '/' . $cari . '/' . $is_cari . '/index/';
			} else {
				$config['base_url'] = base_url() . 'index.php/listspb/cform/view2/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';
			}

			if ($is_cari != "1") {
				$sql = " select a.i_spb
                from tm_spb a 
								left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
								left join tr_customer b on(a.i_customer=b.i_customer)
								left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
								, tr_area c
                where 
                a.i_area=c.i_area and					
                (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                a.d_spb <= to_date('$dto','dd-mm-yyyy'))";
				if ($iarea != 'NA') {
					$sql .= " and a.i_area='$iarea'";
				}
			} else {
				$sql = " select a.i_spb
                from tm_spb a 
								left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f') 
								left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
								left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
								, tr_area c
                where 
                a.i_area=c.i_area and					
                (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                a.d_spb <= to_date('$dto','dd-mm-yyyy')) AND
                (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                or upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')";
				if ($iarea != 'NA') {
					$sql .= " and a.i_area='$iarea'";
				}
			}

			$query 	= $this->db->query($sql, false);
			$config['total_rows'] = $query->num_rows();
			//$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if ($is_cari == "1")
				$config['cur_page'] = $this->uri->segment(10);
			else
				$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);


			$data['page_title'] = $this->lang->line('listspb');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;

			if ($is_cari == "1")
				$data['isi']	= $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $this->uri->segment(10), $cari);
			else
				$data['isi']	= $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $this->uri->segment(8), $cari);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Membuka Data SPB Area ' . $iarea . ' Periode:' . $dfrom . ' s/d ' . $dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('listspb/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export_backup_20230104()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$cari	= strtoupper($this->input->post('xcari'));
			$dfrom	= $this->input->post('xdfrom');
			$dto	= $this->input->post('xdto');
			$iarea	= $this->input->post('xiarea');
			$is_cari = $this->input->post('xis_cari');
			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea = $this->uri->segment(6);
			if ($is_cari == '')
				$is_cari = $this->uri->segment(8);
			if ($cari == '' && $is_cari == "1") $cari = $this->uri->segment(7);

			$data['page_title'] = $this->lang->line('listspb');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;


			$sql = "	a.i_customer, a.f_spb_stockdaerah, a.d_spb, a.f_spb_cancel, a.i_approve1, a.i_notapprove, a.i_approve2,
              a.i_store, a.i_nota, a.f_spb_siapnotagudang, a.f_spb_op, a.f_spb_opclose, a.f_spb_siapnotasales,
              a.i_sj, a.v_spb, a.v_spb_discounttotal, a.i_spb, a.d_spb, a.i_salesman,
              a.i_area, a.i_spb_old, a.i_spb_program, a.i_product_group, a.i_spb_program, a.i_price_group,
              b.e_customer_name, c.e_area_name, d.i_dkb, x.e_customer_name as xname, q.e_promo_name
			  from tm_spb a 
			  left join tm_promo q on(a.i_spb_program = q.i_promo)
			        left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
			        left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
			        left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area 
			        and x.i_customer like '%000')
			        , tr_area c
              where 
                a.i_area=c.i_area and ";
			if ($iarea != 'NA') {
				$sql .= " a.i_area='$iarea' and ";
			}
			$sql .= " (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                a.d_spb <= to_date('$dto','dd-mm-yyyy'))
              order by a.i_area, a.i_spb ";
			$this->db->select($sql, false);
			$query = $this->db->get();
			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$queryy 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($queryy)) {
				$now	  = $row['c'];
			}
			$pesan = 'Export Daftar SPB Area ' . $iarea . ' Periode:' . $dfrom . ' s/d ' . $dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			#####
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar SPB ")->setDescription("PT. Dialogue Garmindo Utama");
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(40);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar SPB');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 10, 2);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:M5'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Tanggal');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Sales');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Lang');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Kotor');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Discount');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Bersih');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Status');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Daerah');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Jenis');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Kode Promo');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Nama Promo');
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i = 7;
				$j = 7;
				$no = 0;
				foreach ($query->result() as $row) {
					$no++;
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':M' . $i
					);

					if (
						($row->f_spb_cancel == 't')
					) {
						$status = 'Batal';
					} elseif (
						($row->i_approve1 == null) && ($row->i_notapprove == null)
					) {
						$status = 'Sales';
					} elseif (
						($row->i_approve1 == null) && ($row->i_notapprove != null)
					) {
						$status = 'Reject (sls)';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 == null) &
						($row->i_notapprove == null)
					) {
						$status = 'Keuangan';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 == null) &&
						($row->i_notapprove != null)
					) {
						$status = 'Reject (ar)';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store == null)
					) {
						$status = 'Gudang';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
					) {
						$status = 'Pemenuhan SPB';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
					) {
						$status = 'Proses OP';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
					) {
						$status = 'OP Close';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
					) {
						$status = 'Siap SJ (sales)';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					) {
						#			  	$status='Siap SJ (gudang)';
						$status = 'Siap SJ';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					) {
						$status = 'Siap SJ';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					) {
						$status = 'Siap DKB';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					) {
						$status = 'Siap Nota';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) &&
						($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
					) {
						$status = 'Siap SJ';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) &&
						($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					) {
						$status = 'Siap DKB';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) &&
						($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					) {
						$status = 'Siap Nota';
					} elseif (
						($row->i_approve1 != null) &&
						($row->i_approve2 != null) &&
						($row->i_store != null) &&
						($row->i_nota != null)
					) {
						$status = 'Sudah dinotakan';
					} elseif (($row->i_nota != null)) {
						$status = 'Sudah dinotakan';
					} else {
						$status = 'Unknown';
					}
					$bersih	= $row->v_spb - $row->v_spb_discounttotal;
					#          $bersih	= number_format($row->v_spb-$row->v_spb_discounttotal);
					#			    $row->v_spb	= number_format($row->v_spb);
					#   			  $row->v_spb_discounttotal	= number_format($row->v_spb_discounttotal);  
					if ($row->f_spb_stockdaerah == 't') {
						$daerah = 'Ya';
					} else {
						$daerah = 'Tidak';
					}
					if ($row->i_product_group == '00') {
						$jenis = 'Home';
					} elseif ($row->i_product_group == '01') {
						$jenis = 'Bd';
					} else {
						$jenis = 'NB';
					}
					$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $row->i_spb);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->d_spb);
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->i_salesman);
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, '(' . $row->i_customer . ') ' . $row->e_customer_name);
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, "'" . $row->i_area);
					$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $row->v_spb);
					$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row->v_spb_discounttotal);
					$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $bersih);
					$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $status);
					$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $daerah);
					$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $jenis);
					$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row->i_spb_program);
					$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $row->e_promo_name);
					$i++;
				}
			}
			$objPHPExcel->getActiveSheet()->getStyle('F7:H' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			if ($iarea == 'NA') $iarea = '00';
			$nama = 'SPB' . $iarea . '.xls';

			if (file_exists('spb/' . $iarea . '/' . $nama)) {
				@chmod('spb/' . $iarea . '/' . $nama, 0777);
				@unlink('spb/' . $iarea . '/' . $nama);
			}
			$objWriter->save("spb/" . $iarea . '/' . $nama);
			@chmod('spb/' . $iarea . '/' . $nama, 0777);

			$data['sukses'] = true;
			$data['folder'] = 'spb/' . $iarea;
			$data['inomor'] = $nama;
			$this->load->view('nomorurl', $data);

			#####
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$dfrom		= $this->input->post('xdfrom') == "" ? $this->uri->segment(4) : "";
			$dto		= $this->input->post('xdto') == "" ? $this->uri->segment(5) : "";
			$iarea		= $this->input->post('xiarea') == "" ? $this->uri->segment(6) : "";
			$is_cari 	= $this->input->post('xis_cari') == "" ? $this->uri->segment(7) : "";
			$cari		= strtoupper($this->input->post('xcari')) ? strtoupper($this->uri->segment(8)) : "";

			$periode 	= date('d', strtotime($dfrom)) . ' ' . mbulan(date('m', strtotime($dfrom))) . ' ' . date('Y', strtotime($dfrom)) . ' s/d ' . date('d', strtotime($dto)) . ' ' . mbulan(date('m', strtotime($dto))) . ' ' . date('Y', strtotime($dto));

			$objPHPExcel = new PHPExcel();

			$query = $this->mmaster->data($dfrom, $dto, $iarea);

			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
				$objPHPExcel->getProperties()->setCreator("M.I.S Dept 2011");
				$objPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2011");

				$objPHPExcel->getProperties()
					->setTitle("List SPB")
					->setSubject("Laporan SPB")
					->setDescription("Laporan SPB")
					->setKeywords("Laporan SPB")
					->setCategory("Laporan");

				$objPHPExcel->setActiveSheetIndex(0);

				$objPHPExcel->createSheet();

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 12
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2'
				);

				$header = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				);

				$stylehead = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER
					)
				);

				$stylehead1 = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					)
				);

				$stylerow = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic' => false,
						'size'  => 9
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					)
				);

				$stylerow1 = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic' => false,
						'size'  => 9
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER
					)
				);

				$stylerow2 = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic' => false,
						'size'  => 9
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'vertical'  => Style_Alignment::VERTICAL_CENTER
					)
				);

				foreach (range('A', 'AE') as $columnID) {
					$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
				}

				$objPHPExcel->getActiveSheet()->mergeCells('A2:AE2');
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'DAFTAR SPB');
				$objPHPExcel->getActiveSheet()->mergeCells('A3:AE3');
				$objPHPExcel->getActiveSheet()->duplicateStyleArray($header, 'A3');
				$objPHPExcel->getActiveSheet()->setCellValue('A3', $periode);
				$objPHPExcel->getActiveSheet()->mergeCells('A4:AE4');
				$objPHPExcel->getActiveSheet()->duplicateStyleArray($header, 'A4');
				$objPHPExcel->getActiveSheet()->setCellValue('A4', NmPerusahaan);

				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No SPB');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Tgl SPB');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Kode Sales');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Kode Lang');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Nama Pelanggan');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Kode Area');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Kotor');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Diskon');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Bersih');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Status');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Daerah');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('L6', 'Jenis');
				$objPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('M6', 'No Nota');
				$objPHPExcel->getActiveSheet()->getStyle('M6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('N6', 'Tgl Nota');
				$objPHPExcel->getActiveSheet()->getStyle('N6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('O6', 'Nilai Nota');
				$objPHPExcel->getActiveSheet()->getStyle('O6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('P6', 'No SJ');
				$objPHPExcel->getActiveSheet()->getStyle('P6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('Q6', 'Tgl SJ');
				$objPHPExcel->getActiveSheet()->getStyle('Q6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('R6', 'No DKB');
				$objPHPExcel->getActiveSheet()->getStyle('R6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('S6', 'Tgl DKB');
				$objPHPExcel->getActiveSheet()->getStyle('S6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('T6', 'Tgl Terima Gudang');
				$objPHPExcel->getActiveSheet()->getStyle('T6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('U6', 'Tgl Approve Sales');
				$objPHPExcel->getActiveSheet()->getStyle('U6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('V6', 'Tgl Approve Keuangan');
				$objPHPExcel->getActiveSheet()->getStyle('V6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('W6', 'Alasan Telat Sales');
				$objPHPExcel->getActiveSheet()->getStyle('W6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('X6', 'Alasan Telat Keuangan');
				$objPHPExcel->getActiveSheet()->getStyle('X6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('Y6', 'Alasan Telat SJ');
				$objPHPExcel->getActiveSheet()->getStyle('Y6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('Z6', 'Alasan Telat DKB');
				$objPHPExcel->getActiveSheet()->getStyle('Z6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('AA6', 'Alasan Batal');
				$objPHPExcel->getActiveSheet()->getStyle('AA6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('AB6', 'SPB Referensi');
				$objPHPExcel->getActiveSheet()->getStyle('AB6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('AC6', 'Kode Promo');
				$objPHPExcel->getActiveSheet()->getStyle('AC6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('AD6', 'Nama Promo');
				$objPHPExcel->getActiveSheet()->getStyle('AD6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('AE6', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('AE6')->applyFromArray($stylehead);

				$objPHPExcel->getActiveSheet()->getStyle('A6:AE6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ACEAA4');

				$j			= 7;
				$nomor		= 1;
				$jml 		= 1;
				foreach ($query->result() as $row) {
					$bersih	= $row->v_spb - $row->v_spb_discounttotal;
					$daerah = $row->f_spb_stockdaerah == 't' ? 'Ya' : 'Tidak';
					$dnota 	= $row->d_nota == "" ? "" : date('d-m-Y', strtotime($row->d_nota));
					$dsj 	= $row->d_sj == "" ? "" : date('d-m-Y', strtotime($row->d_sj));
					$ddkb 	= $row->d_dkb == "" ? "" : date('d-m-Y', strtotime($row->d_dkb));
					$dapp1 	= $row->d_approve1 == "" ? "" : date('d-m-Y', strtotime($row->d_approve1));
					$dapp2 	= $row->d_approve2 == "" ? "" : date('d-m-Y', strtotime($row->d_approve2));


					/* KONDISI STATUS */
					if (
						($row->f_spb_cancel == 't')
					) {
						$status = 'Batal';
					} elseif (
						($row->i_approve1 == null) && ($row->i_notapprove == null)
					) {
						$status = 'Sales';
					} elseif (
						($row->i_approve1 == null) && ($row->i_notapprove != null)
					) {
						$status = 'Reject (sls)';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 == null) &
						($row->i_notapprove == null)
					) {
						$status = 'Keuangan';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 == null) &&
						($row->i_notapprove != null)
					) {
						$status = 'Reject (ar)';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store == null)
					) {
						$status = 'Gudang';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
					) {
						$status = 'Pemenuhan SPB';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
					) {
						$status = 'Proses OP';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
					) {
						$status = 'OP Close';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
					) {
						$status = 'Siap SJ (sales)';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					) {
						#			  	$status='Siap SJ (gudang)';
						$status = 'Siap SJ';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					) {
						$status = 'Siap SJ';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					) {
						$status = 'Siap DKB';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
						($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					) {
						$status = 'Siap Nota';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) &&
						($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
					) {
						$status = 'Siap SJ';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) &&
						($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					) {
						$status = 'Siap DKB';
					} elseif (
						($row->i_approve1 != null) && ($row->i_approve2 != null) &&
						($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) &&
						($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					) {
						$status = 'Siap Nota';
					} elseif (
						($row->i_approve1 != null) &&
						($row->i_approve2 != null) &&
						($row->i_store != null) &&
						($row->i_nota != null)
					) {
						$status = 'Sudah dinotakan';
					} elseif (($row->i_nota != null)) {
						$status = 'Sudah dinotakan';
					} else {
						$status = 'Unknown';
					}
					/* ******************************** */

					$objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $row->i_spb);
					$objPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, date('d-m-Y', strtotime($row->d_spb)));
					$objPHPExcel->getActiveSheet()->getStyle('B' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $j, $row->i_salesman, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('C' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $j, $row->i_customer, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('D' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $j, $row->e_customer_name);
					$objPHPExcel->getActiveSheet()->getStyle('E' . $j)->applyFromArray($stylerow1);

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $j, $row->i_area, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('F' . $j)->applyFromArray($stylerow1);

					$objPHPExcel->getActiveSheet()->setCellValue('G' . $j, $row->v_spb);
					$objPHPExcel->getActiveSheet()->getStyle('G' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->getStyle('G' . $j)->applyFromArray($stylerow2);

					$objPHPExcel->getActiveSheet()->setCellValue('H' . $j, $row->v_spb_discounttotal);
					$objPHPExcel->getActiveSheet()->getStyle('H' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->getStyle('H' . $j)->applyFromArray($stylerow2);

					$objPHPExcel->getActiveSheet()->setCellValue('I' . $j, $bersih);
					$objPHPExcel->getActiveSheet()->getStyle('I' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->getStyle('I' . $j)->applyFromArray($stylerow2);

					$objPHPExcel->getActiveSheet()->setCellValue('J' . $j, $status);
					$objPHPExcel->getActiveSheet()->getStyle('J' . $j)->applyFromArray($stylerow1);

					$objPHPExcel->getActiveSheet()->setCellValue('K' . $j, $daerah);
					$objPHPExcel->getActiveSheet()->getStyle('K' . $j)->applyFromArray($stylerow1);

					$objPHPExcel->getActiveSheet()->setCellValue('L' . $j, $row->jenis);
					$objPHPExcel->getActiveSheet()->getStyle('L' . $j)->applyFromArray($stylerow1);

					$objPHPExcel->getActiveSheet()->setCellValue('M' . $j, $row->i_nota);
					$objPHPExcel->getActiveSheet()->getStyle('M' . $j)->applyFromArray($stylerow1);

					$objPHPExcel->getActiveSheet()->setCellValue('N' . $j, $dnota);
					$objPHPExcel->getActiveSheet()->getStyle('N' . $j)->applyFromArray($stylerow1);

					$objPHPExcel->getActiveSheet()->setCellValue('O' . $j, $row->v_nota_netto);
					$objPHPExcel->getActiveSheet()->getStyle('O' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->getStyle('O' . $j)->applyFromArray($stylerow2);

					$objPHPExcel->getActiveSheet()->setCellValue('P' . $j, $row->i_sj);
					$objPHPExcel->getActiveSheet()->getStyle('P' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('Q' . $j, $dsj);
					$objPHPExcel->getActiveSheet()->getStyle('Q' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('R' . $j, $row->i_dkb);
					$objPHPExcel->getActiveSheet()->getStyle('R' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('S' . $j, $ddkb);
					$objPHPExcel->getActiveSheet()->getStyle('S' . $j)->applyFromArray($stylerow1);

					if ($row->d_approve1 > $row->d_approve2) {
						$objPHPExcel->getActiveSheet()->setCellValue('T' . $j, $dapp1);
						$objPHPExcel->getActiveSheet()->getStyle('T' . $j)->applyFromArray($stylerow1);
					} else if ($row->d_approve1 == $row->d_approve2) {
						$objPHPExcel->getActiveSheet()->setCellValue('T' . $j, $dapp2);
						$objPHPExcel->getActiveSheet()->getStyle('T' . $j)->applyFromArray($stylerow1);
					} else {
						$objPHPExcel->getActiveSheet()->setCellValue('T' . $j, $dapp2);
						$objPHPExcel->getActiveSheet()->getStyle('T' . $j)->applyFromArray($stylerow1);
					}

					$objPHPExcel->getActiveSheet()->setCellValue('U' . $j, $dapp1);
					$objPHPExcel->getActiveSheet()->getStyle('U' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('V' . $j, $dapp2);
					$objPHPExcel->getActiveSheet()->getStyle('V' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('W' . $j, $row->e_sales);
					$objPHPExcel->getActiveSheet()->getStyle('W' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('X' . $j, $row->e_keuangan);
					$objPHPExcel->getActiveSheet()->getStyle('X' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('Y' . $j, $row->e_sj);
					$objPHPExcel->getActiveSheet()->getStyle('Y' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('Z' . $j, $row->e_dkb);
					$objPHPExcel->getActiveSheet()->getStyle('Z' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('AA' . $j, $row->e_batal);
					$objPHPExcel->getActiveSheet()->getStyle('AA' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('AB' . $j, $row->i_spb_refference);
					$objPHPExcel->getActiveSheet()->getStyle('AB' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('AC' . $j, $row->i_spb_program);
					$objPHPExcel->getActiveSheet()->getStyle('AC' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('AD' . $j, $row->e_promo_name);
					$objPHPExcel->getActiveSheet()->getStyle('AD' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('AE' . $j, trim($row->keterangan));
					$objPHPExcel->getActiveSheet()->getStyle('AE' . $j)->applyFromArray($stylerow1);

					$jml++;
					$j++;
					$nomor++;
				}
			}

			$this->logger->writenew('Export Daftar SPB Area ' . $iarea . ' Periode:' . $dfrom . ' s/d ' . $dto);

			$nama_file = 'SPB' . $iarea . "_" . $dfrom . "_" . $dto . '.xls';
			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename=' . $nama_file . ''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$objPHPExcel = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objPHPExcel->save('php://output', 'w');

			#####
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
