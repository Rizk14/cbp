<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu513')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = "Rekap Piutang Dagang";
			$data['iperiode']='';
      //$data['djt']='';
			$this->load->view('rekappiutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu513')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode	= $this->uri->segment(4);
			
			$config['per_page'] = '10';
			$this->load->model('rekappiutang/mmaster');
			$data['page_title'] = "Rekap Piutang Dagang";
			$data['cari']		= $cari;
			$data['iperiode']	= $iperiode;
			$data['hal'] = $this->uri->segment(5);
			$data['isi']		= $this->mmaster->bacaperiode($iperiode,$config['per_page'],$this->uri->segment(5),$cari);
			//echo "didieu"; die();
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Rekap Piutang Dagang Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$this->load->view('rekappiutang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

}
?>
