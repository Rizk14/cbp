<?php
class Cform extends CI_Controller
{
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      require_once("php/fungsi.php");
   }

   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu456') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data['page_title'] = $this->lang->line('exp-spmbblmreal');
         $data['datefrom'] = '';
         $data['dateto']   = '';
         $this->load->view('exp-spmbblmreal/vmainform', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu456') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $this->load->model('exp-spmbblmreal/mmaster');
         $iarea      = $this->input->post('iarea');
         $datefrom   = $this->input->post('datefrom');
         $dateto     = $this->input->post('dateto');
         $datefromx  = $datefrom;
         $datetox    = $dateto;
         $que        = $this->db->query(" select a.i_spmb, a.d_spmb, a.i_area, b.i_product, b.e_product_name,
                                          b.n_order, b.n_acc, b.n_saldo, c.e_area_name, a.f_spmb_consigment
                                          from tm_spmb a, tm_spmb_item b, tr_area c
                                          where a.i_spmb=b.i_spmb and a.i_area=b.i_area
                                          and a.i_area=c.i_area and a.f_spmb_cancel='f'
                                          and not a.i_approve2 is null and not a.i_store is null
                                          and b.n_deliver<b.n_saldo and a.d_spmb >= to_date('$datefromx','dd-mm-yyyy')
                                          and a.d_spmb <= to_date('$datetox','dd-mm-yyyy')
                                          and a.i_area='$iarea'
                                          order by a.i_area, a.i_spmb, b.n_item_no");

         $fromname = '';
         if ($datefrom != '') {
            $tmp = explode("-", $datefrom);
            $th = $tmp[2];
            $bl = $tmp[1];
            $hr = $tmp[0];
            $fromname = $fromname . $hr;
            $datefrom = $th . "-" . $bl . "-" . $hr;
            $periodeawal   = $hr . " " . mbulan($bl) . " " . $th;
         }
         $tmp           = explode("-", $dateto);
         $det           = $tmp[0];
         $mon           = $tmp[1];
         $yir           = $tmp[2];
         $dtos          = $yir . "/" . $mon . "/" . $det;
         $fromname      = $fromname . $det;
         $periodeakhir  = $det . " " . mbulan($mon) . " " . $yir;
         $dtos          = $this->mmaster->dateAdd("d", 1, $dtos);
         $tmp           = explode("-", $dtos);
         $det1          = $tmp[2];
         $mon1          = $tmp[1];
         $yir1          = $tmp[0];
         $dtos          = $yir1 . "-" . $mon1 . "-" . $det1;
         $data['page_title'] = $this->lang->line('exp-spmbblmreal');
         $qareaname   = $this->mmaster->eareaname($iarea);
         if ($qareaname->num_rows() > 0) {
            $row_areaname   = $qareaname->row();
            $aname   = $row_areaname->e_area_name;
         } else {
            $aname   = '';
         }

         $this->load->library('PHPExcel');
         $this->load->library('PHPExcel/IOFactory');
         $objPHPExcel = new PHPExcel();
         $objPHPExcel->getProperties()->setTitle("Daftar SPMB Tidak Terealisasi")
            ->setDescription(NmPerusahaan);
         $objPHPExcel->setActiveSheetIndex(0);
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            array(
               'font'   => array(
                  'name'   => 'Arial',
                  'bold'   => true,
                  'italic' => false,
                  'size'  => 12
               ),
               'alignment' => array(
                  'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
                  'vertical'  => Style_Alignment::VERTICAL_CENTER,
                  'wrap'      => true
               )
            ),
            'A1:A4'
         );
         $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
         $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
         $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(18);
         $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
         $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);

         $objPHPExcel->getActiveSheet()->setCellValue('A1', NmPerusahaan);
         $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 1, 8, 1);
         $objPHPExcel->getActiveSheet()->setCellValue('A2', 'DAFTAR SPMB Tidak Terealisasi - ' . $aname);
         $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 8, 2);
         $objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : ' . $periodeawal . ' - ' . $periodeakhir);

         $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 8, 3);
         $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
         $objPHPExcel->getActiveSheet()->getStyle('A5:A5')->applyFromArray(
            array(
               'borders'   => array(
                  'top'       => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Area');
         $objPHPExcel->getActiveSheet()->getStyle('B5:B5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('C5', 'No SPMB');
         $objPHPExcel->getActiveSheet()->getStyle('C5:C5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Tgl SPMB');
         $objPHPExcel->getActiveSheet()->getStyle('D5:D5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Konsinyasi');
         $objPHPExcel->getActiveSheet()->getStyle('E5:E5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Kd Brg');
         $objPHPExcel->getActiveSheet()->getStyle('F5:F5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Nama Brg');
         $objPHPExcel->getActiveSheet()->getStyle('G5:G5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Jml Order');
         $objPHPExcel->getActiveSheet()->getStyle('H5:H5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Jml Acc');
         $objPHPExcel->getActiveSheet()->getStyle('I5:I5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );

         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            array(
               'font' => array(
                  'name'   => 'Liberation Sans',
                  'bold'  => false,
                  'italic' => false,
                  'size'  => 12
               ),
               'alignment' => array(
                  'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
                  'vertical'  => Style_Alignment::VERTICAL_CENTER,
                  'wrap'      => true
               )
            ),
            'A5:I5'
         );

         $this->db->select(" a.i_spmb, a.d_spmb, a.i_area, b.i_product, b.e_product_name,
                                b.n_order, b.n_acc, b.n_saldo, c.e_area_name, a.f_spmb_consigment
                                from tm_spmb a, tm_spmb_item b, tr_area c
                                where a.i_spmb=b.i_spmb and a.i_area=b.i_area
                                and a.i_area=c.i_area and a.f_spmb_cancel='f'
                                and not a.i_approve2 is null and not a.i_store is null
                                and b.n_deliver<b.n_saldo and a.d_spmb >= to_date('$datefromx','dd-mm-yyyy')
                                and a.d_spmb <= to_date('$datetox','dd-mm-yyyy')
                                and a.i_area='$iarea'
                                order by a.i_area, a.i_spmb, b.n_item_no", false);

         $query = $this->db->get();
         if ($query->num_rows() > 0) {
            $i = 5;
            $j = 5;
            $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'   => array('style' => Style_Border::BORDER_THIN),
                     'right'  => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'   => array('style' => Style_Border::BORDER_THIN),
                     'right'  => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );

            foreach ($query->result() as $row) {

               $i++;
               $j++;

               $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $j - 5);
               $objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     ),
                     'alignment' => array(
                        'horizontal' => Style_Alignment::HORIZONTAL_RIGHT
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->i_area . '-' . $row->e_area_name);
               $objPHPExcel->getActiveSheet()->getStyle('B' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->i_spmb);
               $objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               if ($row->d_spmb) {
                  $tmp = explode('-', $row->d_spmb);
                  $tgl = $tmp[2];
                  $bln = $tmp[1];
                  $thn = $tmp[0];
                  $row->d_spmb = $tgl . '-' . $bln . '-' . $thn;
               }
               $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->d_spmb);
               $objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               if ($row->f_spmb_consigment == 't') {
                  $konsinyasi = 'Ya';
               } else {
                  $konsinyasi = 'Tidak';
               }
               $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $konsinyasi);
               $objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $row->i_product);
               $objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row->e_product_name);
               $objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->n_order);
               $objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->n_acc);
               $objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
            }
            $x = $i - 1;
         }
         $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
         $nama = 'spmbtdkrealisasi' . $iarea . '.xls';
         $area = $iarea;

         if (file_exists('excel/' . $area . '/' . $nama)) {
            @chmod('excel/' . $area . '/' . $nama, 0777);
            @unlink('excel/' . $area . '/' . $nama);
         }
         $objWriter->save('excel/' . $area . '/' . $nama);
         @chmod('excel/' . $area . '/' . $nama, 0777);

         // $sess=$this->session->userdata('session_id');
         // $id=$this->session->userdata('user_id');
         // $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
         // $rs		= pg_query($sql);
         // if(pg_num_rows($rs)>0){
         //    while($row=pg_fetch_assoc($rs)){
         //       $ip_address	  = $row['ip_address'];
         //       break;
         //    }
         // }else{
         //    $ip_address='kosong';
         // }
         // $query 	= pg_query("SELECT current_timestamp as c");
         // while($row=pg_fetch_assoc($query)){
         // 	$now	  = $row['c'];
         // }
         // $pesan='Export SPMB Tidak Terealisasi Area:'.$iarea.' tanggal:'.$datefrom.' sampai:'.$dateto;
         // $this->load->model('logger');
         $this->logger->writenew('Export SPMB Tidak Terealisasi Area:' . $iarea . ' tanggal:' . $datefrom . ' sampai:' . $dateto);

         $data['sukses']   = true;
         $data['inomor']   = $nama;
         $data['folder']   = 'excel/' . $area;

         $this->load->view('nomorurl', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu456') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/exp-spmbblmreal/cform/area/index/';
         $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('listspb/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
         $this->load->view('exp-spmbblmreal/vlistarea', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu456') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/exp-spmbblmreal/cform/area/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari   = strtoupper($cari);
         $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('listspb/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
         $this->load->view('exp-spmbblmreal/vlistarea', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
}
