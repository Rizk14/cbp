<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
    $this->load->helper(array('file','directory','fusioncharts'));
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = "Sales Performance By Salesman";
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('bysalesman/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iuser		= $this->session->userdata('user_id');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom = $this->uri->segment(4);
			$dto = $this->uri->segment(5);

			if($dfrom!=''){
				$tmp = explode("-", $dfrom);
				$hr=$tmp[0]; 
				$bl=$tmp[1];
				$th=$tmp[2];
				$prevth  =$th-1;
				//$prevdate=$prevth.$bl;
			}
			if($bl <9 ){
          		$bln = $bl+1;
          		$bln = '0'.$bln;
          		$akhir = $th.'-'.$bln.'-01';
          		$prevakhir = $prevth.'-'.$bln.'-01';
        	}elseif($bl >=9 && $bl <12){
          		$bln = $bl+1;
          		$akhir = $th.'-'.$bln.'-01';
          		$prevakhir = $prevth.'-'.$bln.'-01';
        	}elseif($bl ==12){
          		$thn = $th+1;
          		$akhir = $thn.'-01-01';
          		$prevakhir = $prevthn.'-01-01';
        	}

        	if($dto!=''){
				$tmpo = explode("-", $dto);
				$hari=$tmpo[0]; 
				$bulan=$tmpo[1];
				$tahun=$tmpo[2];
				$prevtahun  =$tahun-1;
				//$prevdate=$prevth.$bl;
			}

			if($bulan <9 ){
          		$month = $bulan+1;
          		$month = '0'.$month;
          		$last = $tahun.'-'.$month.'-01';
          		$prevlast = $prevtahun.'-'.$month.'-01';
        	}elseif($bulan >=9 && $bulan <12){
          		$month = $bulan+1;
          		$last = $tahun.'-'.$month.'-01';
          		$prevlast = $prevtahun.'-'.$month.'-01';
        	}elseif($bulan ==12){
          		$year = $tahun+1;
          		$last = $year.'-01-01';
          		$prevlast = $prevtahun.'-01-01';
        	}

			$config['base_url'] = base_url().'index.php/bysalesman/cform/view/'.$dfrom.'/'.$dto.'/index/';
			$this->load->model('bysalesman/mmaster');
			$data['page_title'] = "Sales Performance By Salesman";
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto'] 		= $dto;
			$data['th']			= $th;
			$data['prevth']		= $prevth;
			$data['bl']			= $bl;
			$data['iuser']		= $iuser;
			$data['isi']		= $this->mmaster->baca($dfrom,$dto,$th,$prevth,$bl,$bulan,$tahun,$iuser,$akhir,$prevakhir,$last);
			//var_dump($data['isi']);
			//die();
			$data['user']		= $this->mmaster->user();
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Activity Salesman:'.$dfrom;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('bysalesman/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
