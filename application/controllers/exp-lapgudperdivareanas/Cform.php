<?php
class Cform extends CI_Controller
{
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationx');
      require_once("php/fungsi.php");
      $this->load->model('exp-lapgudperdivareanas/mmaster');
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu344') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data['page_title']  = $this->lang->line('exp-lapgudperdiv');
         $data['iperiode']    = '';
         $data['b']           = '';
         $data['nb']          = '';
         $data['r']           = '';
         $data['productgrp']  = $this->mmaster->bacagroupbarang();

         $this->load->view('exp-lapgudperdivareanas/vmainform', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu344') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data['page_title'] = $this->lang->line('exp-lapgudperdiv');
         $this->load->view('exp-lapgudperdivareanas/vinsert_fail', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
   function store()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu344') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/exp-lapgudperdivareanas/cform/store/index/';
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
            $query = $this->db->query("select distinct(c.i_store) as i_store
                                  from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store");
         } else {
            $query = $this->db->query("select distinct(c.i_store) as i_store
													         from tr_store_location a, tr_store b, tr_area c
													         where a.i_store = b.i_store and b.i_store=c.i_store
													         and (c.i_area = '$area1' or c.i_area = '$area2' or
													         c.i_area = '$area3' or c.i_area = '$area4' or
													         c.i_area = '$area5')");
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);


         $data['page_title'] = $this->lang->line('list_store');
         $data['isi'] = $this->mmaster->bacastore($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
         $this->load->view('exp-lapgudperdivareanas/vliststore', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
   function caristore()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu344') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/exp-lapgudperdivareanas/cform/store/index/';
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
            $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')", false);
         } else {
            $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')
                                    and (c.i_area = '$area1' or c.i_area = '$area2' or
                                    c.i_area = '$area3' or c.i_area = '$area4' or
                                    c.i_area = '$area5')", false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $data['page_title'] = $this->lang->line('list_store');
         $data['isi'] = $this->mmaster->caristore($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
         $this->load->view('exp-lapgudperdivareanas/vliststore', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu344') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {

         $iperiode   = $this->input->post('iperiode');
         // $b   = $this->input->post('chkbx');
         // $nb  = $this->input->post('chknbx');
         // $r   = $this->input->post('chkrx');
         // $l   = $this->input->post('chklx');
         // if ($b == '') $b = $this->uri->segment(4);
         // if ($nb == '') $nb = $this->uri->segment(5);
         // if ($r == '') $r = $this->uri->segment(6);
         // if ($l == '') $l = $this->uri->segment(7);
         $c = substr($iperiode, 0, 4);
         $d = substr($iperiode, 4, 2);
         $peri = mbulan($d) . " - " . $c;

         $jml              = $this->input->post('jml', TRUE);
         $judul            = $this->input->post('judul', TRUE);

         // if ($b == 'qqq') {
         //    $judul = 'Bedding';
         // } elseif ($nb == 'qqq') {
         //    $judul = 'Non Bedding';
         // } elseif ($r == 'qqq') {
         //    $judul = 'Reguler';
         // } elseif ($l == 'qqq') {
         //    $judul = 'Lain-lain';
         // }

         for ($i = 0; $i < $jml; $i++) {
            if ($this->input->post('grp' . $i . 'x', TRUE) == "qqq") {
               $iproductgroup = $this->input->post('grp' . $i, TRUE);
            }
         }

         $this->load->library('PHPExcel');
         $this->load->library('PHPExcel/IOFactory');
         $objPHPExcel = new PHPExcel();
         $objPHPExcel->getProperties()->setTitle("Laporan Gudang Per Divisi Per Area Nasional")->setDescription(NmPerusahaan);
         $objPHPExcel->setActiveSheetIndex(0);
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            array(
               'font' => array(
                  'name'   => 'Arial',
                  'bold'  => true,
                  'italic' => false,
                  'size'  => 10
               ),
               'alignment' => array(
                  'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                  'vertical'  => Style_Alignment::VERTICAL_CENTER,
                  'wrap'      => true
               )
            ),
            'A2:A4'
         );

         $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
         $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
         $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
         $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
         $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
         $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
         $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
         $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan Gudang Per Divisi Per Area Nasional Periode ' . $peri);
         $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 13, 2);
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            array(
               'font' => array(
                  'name'   => 'Arial',
                  'bold'  => true,
                  'italic' => false,
                  'size'  => 10
               ),
               'alignment' => array(
                  'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                  'vertical'  => Style_Alignment::VERTICAL_CENTER,
                  'wrap'      => true
               )
            ),
            'A5:M5'
         );


         $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
         $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               )
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Gudang');
         $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Lokasi');
         $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Kode');
         $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Nama');
         $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Harga');
         $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Penjualan');
         $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('H5', 'SJ');
         $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Saldo Akhir');
         $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Stock Opname');
         $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('K5', 'On Hand');
         $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Nilai Penjualan');
         $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('M5', 'Nilai Saldo Akhir');
         $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $i = 6;
         $j = 6;
         $no = 0;
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            array(
               'font' => array(
                  'name'   => 'Arial',
                  'bold'   => false,
                  'italic' => false,
                  'size'   => 10
               )
            ),
            'A' . $i . ':M10000' #.$i
         );

         $totjual = 0;
         $totsaldo = 0;
         if ($iperiode > '201512') {
            ###
            // if ($b == 'qqq') {
            $query = $this->db->query("select x.i_product, x.i_product_grade, x.e_product_name,x.i_store, x.i_store_location, x.e_mutasi_periode, 
                                              x.v_product_retail, sum(x.n_git_penjualan) as n_git_penjualan, 
                                              sum(x.n_mutasi_git) as n_mutasi_git, sum(x.n_mutasi_penjualan) as n_mutasi_penjualan, 
                                              sum(x.n_saldo_akhir) as n_saldo_akhir, sum(x.n_saldo_stockopname) as n_saldo_stockopname, 
                                              sum(x.n_quantity_stock) as n_quantity_stock
                                              from( 
                                              select a.i_product, 'A' as i_product_grade, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
                                              b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                              from f_mutasi_stock_daerah_all_saldoakhir('$iperiode') a, tr_product_price b, tm_ic c, tr_product d, tr_product_type e
                                              where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
                                              and a.i_product=d.i_product 
                                              AND d.i_product_type = e.i_product_type 
	                                           AND e.i_product_group = '$iproductgroup' and 
                                              a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
                                              and a.i_product_motif=c.i_product_motif 
                                              and a.i_store=c.i_store and a.i_store_location=c.i_store_location
                                              group by a.i_store, a.i_store_location, a.i_product, a.i_product_grade, e_mutasi_periode, a.e_product_name, 
                                              b.v_product_retail
                                              union all
                                              select a.i_product, a.i_product_grade, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
                                              b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                              from f_mutasi_stock_pusat_saldoakhir('$iperiode') a, tr_product_price b, tm_ic c, tr_product d, tr_product_type e
                                              where a.i_product=b.i_product 
                                              AND d.i_product_type = e.i_product_type 
	                                           AND e.i_product_group = '$iproductgroup'
                                              and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
                                              and a.i_product=d.i_product and d.i_product_type='$iproductgroup' and 
                                              a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
                                              and a.i_product_motif=c.i_product_motif 
                                              and a.i_store=c.i_store and a.i_store_location=c.i_store_location
                                              group by a.i_store, a.i_store_location, a.i_product, a.i_product_grade, e_mutasi_periode, a.e_product_name, 
                                              b.v_product_retail
                                              ) as x
                                              group by x.i_store, x.i_store_location, x.i_product, x.i_product_grade, x.e_mutasi_periode, x.e_product_name, 
                                              x.v_product_retail
                                              order by x.i_store, x.i_store_location, x.e_product_name", false);
            // } elseif ($nb == 'qqq') {
            //    $query = $this->db->query("select x.i_product, x.i_product_grade, x.e_product_name,x.i_store, x.i_store_location, x.e_mutasi_periode, 
            //                                   x.v_product_retail, sum(x.n_git_penjualan) as n_git_penjualan, 
            //                                   sum(x.n_mutasi_git) as n_mutasi_git, sum(x.n_mutasi_penjualan) as n_mutasi_penjualan, 
            //                                   sum(x.n_saldo_akhir) as n_saldo_akhir, sum(x.n_saldo_stockopname) as n_saldo_stockopname, 
            //                                   sum(x.n_quantity_stock) as n_quantity_stock
            //                                   from( 
            //                                   select a.i_product, 'A' as i_product_grade, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
            //                                   b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
            //                                   from f_mutasi_stock_daerah_all_saldoakhir('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
            //                                   where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
            //                                   and a.i_product=d.i_product and d.i_product_type='08' and 
            //                                   a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
            //                                   and a.i_product_motif=c.i_product_motif 
            //                                   and a.i_store=c.i_store and a.i_store_location=c.i_store_location
            //                                   group by a.i_store, a.i_store_location, a.i_product, a.i_product_grade, e_mutasi_periode, a.e_product_name, 
            //                                   b.v_product_retail
            //                                   union all
            //                                   select a.i_product, a.i_product_grade, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
            //                                   b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
            //                                   from f_mutasi_stock_pusat_saldoakhir('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
            //                                   where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
            //                                   and a.i_product=d.i_product and d.i_product_type='08' and 
            //                                   a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
            //                                   and a.i_product_motif=c.i_product_motif 
            //                                   and a.i_store=c.i_store and a.i_store_location=c.i_store_location
            //                                   group by a.i_store, a.i_store_location, a.i_product, a.i_product_grade, e_mutasi_periode, a.e_product_name, 
            //                                   b.v_product_retail
            //                                   ) as x
            //                                   group by x.i_store, x.i_store_location, x.i_product, x.e_mutasi_periode, x.i_product_grade, x.e_product_name, 
            //                                   x.v_product_retail
            //                                   order by x.i_store, x.i_store_location, x.e_product_name", false);
            // } elseif ($r == 'qqq') {
            //    $query = $this->db->query("select x.i_product, x.i_product_grade, x.e_product_name,x.i_store, x.i_store_location, x.e_mutasi_periode, 
            //                                   x.v_product_retail, sum(x.n_git_penjualan) as n_git_penjualan, 
            //                                   sum(x.n_mutasi_git) as n_mutasi_git, sum(x.n_mutasi_penjualan) as n_mutasi_penjualan, 
            //                                   sum(x.n_saldo_akhir) as n_saldo_akhir, sum(x.n_saldo_stockopname) as n_saldo_stockopname, 
            //                                   sum(x.n_quantity_stock) as n_quantity_stock
            //                                   from( 
            //                                   select a.i_product, 'A' as i_product_grade, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
            //                                   b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
            //                                   from f_mutasi_stock_daerah_all_saldoakhir('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
            //                                   where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
            //                                   and a.i_product=d.i_product 
            //                                   and d.i_product_type<>'08' and d.i_product_type<>'03' and d.i_product_type<>'09' and 
            //                                   a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
            //                                   and a.i_product_motif=c.i_product_motif 
            //                                   and a.i_store=c.i_store and a.i_store_location=c.i_store_location
            //                                   group by a.i_store, a.i_store_location, a.i_product, a.i_product_grade, e_mutasi_periode, a.e_product_name, 
            //                                   b.v_product_retail
            //                                   union all
            //                                   select a.i_product, a.i_product_grade, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
            //                                   b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
            //                                   from f_mutasi_stock_pusat_saldoakhir('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
            //                                   where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
            //                                   and a.i_product=d.i_product 
            //                                   and d.i_product_type<>'08' and d.i_product_type<>'03' and d.i_product_type<>'09' and 
            //                                   a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
            //                                   and a.i_product_motif=c.i_product_motif 
            //                                   and a.i_store=c.i_store and a.i_store_location=c.i_store_location
            //                                   group by a.i_store, a.i_store_location, a.i_product, a.i_product_grade, e_mutasi_periode, a.e_product_name, 
            //                                   b.v_product_retail
            //                                   ) as x
            //                                   group by x.i_store, x.i_store_location, x.i_product, x.i_product_grade, x.e_mutasi_periode, x.e_product_name, 
            //                                   x.v_product_retail
            //                                   order by x.i_store, x.i_store_location, x.e_product_name", false);
            // } elseif ($l == 'qqq') {
            //    $query = $this->db->query("select x.i_product, x.i_product_grade, x.e_product_name,x.i_store, x.i_store_location, x.e_mutasi_periode, 
            //                                   x.v_product_retail, sum(x.n_git_penjualan) as n_git_penjualan, 
            //                                   sum(x.n_mutasi_git) as n_mutasi_git, sum(x.n_mutasi_penjualan) as n_mutasi_penjualan, 
            //                                   sum(x.n_saldo_akhir) as n_saldo_akhir, sum(x.n_saldo_stockopname) as n_saldo_stockopname, 
            //                                   sum(x.n_quantity_stock) as n_quantity_stock
            //                                   from( 
            //                                   select a.i_product, 'A' as i_product_grade, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
            //                                   b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
            //                                   from f_mutasi_stock_daerah_all_saldoakhir('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
            //                                   where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
            //                                   and a.i_product=d.i_product and d.i_product_type='09' and 
            //                                   a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
            //                                   and a.i_product_motif=c.i_product_motif 
            //                                   and a.i_store=c.i_store and a.i_store_location=c.i_store_location
            //                                   group by a.i_store, a.i_store_location, a.i_product, a.i_product_grade, e_mutasi_periode, a.e_product_name, 
            //                                   b.v_product_retail
            //                                   union all
            //                                   select a.i_product, a.i_product_grade, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
            //                                   b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
            //                                   from f_mutasi_stock_pusat_saldoakhir('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
            //                                   where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
            //                                   and a.i_product=d.i_product and d.i_product_type='09' and 
            //                                   a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
            //                                   and a.i_product_motif=c.i_product_motif 
            //                                   and a.i_store=c.i_store and a.i_store_location=c.i_store_location
            //                                   group by a.i_store, a.i_store_location, a.i_product, a.i_product_grade, e_mutasi_periode, a.e_product_name, 
            //                                   b.v_product_retail
            //                                   ) as x
            //                                   group by x.i_store, x.i_store_location, x.i_product, x.i_product_grade, x.e_mutasi_periode, x.e_product_name, 
            //                                   x.v_product_retail
            //                                   order by x.i_store, x.i_store_location, x.e_product_name", false);
            // }
            ###           
         } else {
            // if ($b == 'qqq') {
            $query = $this->db->query("select x.i_product, x.e_product_name,x.i_store, x.i_store_location, x.e_mutasi_periode, 
                                              x.v_product_retail, sum(x.n_git_penjualan) as n_git_penjualan, 
                                              sum(x.n_mutasi_git) as n_mutasi_git, sum(x.n_mutasi_penjualan) as n_mutasi_penjualan, 
                                              sum(x.n_saldo_akhir) as n_saldo_akhir, sum(x.n_saldo_stockopname) as n_saldo_stockopname, 
                                              sum(x.n_quantity_stock) as n_quantity_stock
                                              from( 
                                              select a.i_product, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
                                              b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                              from f_mutasi_stock_daerah_all('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
                                              where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
                                              and a.i_product=d.i_product and d.i_product_type='$iproductgroup' and 
                                              a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
                                              and a.i_product_motif=c.i_product_motif 
                                              and a.i_store=c.i_store and a.i_store_location=c.i_store_location
                                              group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, a.e_product_name, 
                                              b.v_product_retail
                                              union all
                                              select a.i_product, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
                                              b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                              sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                              sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                              from f_mutasi_stock_pusat('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
                                              where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
                                              and a.i_product=d.i_product and d.i_product_type='$iproductgroup' and 
                                              a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
                                              and a.i_product_motif=c.i_product_motif 
                                              and a.i_store=c.i_store and a.i_store_location=c.i_store_location
                                              group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, a.e_product_name, 
                                              b.v_product_retail
                                              ) as x
                                              group by x.i_store, x.i_store_location, x.i_product, x.e_mutasi_periode, x.e_product_name, 
                                              x.v_product_retail
                                              order by x.i_store, x.i_store_location, x.e_product_name", false);
            /*
                   $query = $this->db->query(" select a.i_product, a.e_product_name,a.i_store, a.i_store_location, 
                                      b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                      sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                      sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                      from f_mutasi_stock_daerah_all('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
                                      where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
                                      and a.i_product=d.i_product and d.i_product_type='03' and 
                                      a.i_product=c.i_product and a.i_product_grade=c.i_product_grade and a.i_product_motif=c.i_product_motif 
                                      and a.i_store=c.i_store and a.i_store_location=c.i_store_location
                                      group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, a.e_product_name, 
                                      b.v_product_retail
                                      order by a.i_store, a.i_store_location, a.e_product_name",false);
  */
            // } elseif ($nb == 'qqq') {
            //    $query = $this->db->query("select x.i_product, x.e_product_name,x.i_store, x.i_store_location, x.e_mutasi_periode, 
            //                                   x.v_product_retail, sum(x.n_git_penjualan) as n_git_penjualan, 
            //                                   sum(x.n_mutasi_git) as n_mutasi_git, sum(x.n_mutasi_penjualan) as n_mutasi_penjualan, 
            //                                   sum(x.n_saldo_akhir) as n_saldo_akhir, sum(x.n_saldo_stockopname) as n_saldo_stockopname, 
            //                                   sum(x.n_quantity_stock) as n_quantity_stock
            //                                   from( 
            //                                   select a.i_product, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
            //                                   b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
            //                                   from f_mutasi_stock_daerah_all('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
            //                                   where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
            //                                   and a.i_product=d.i_product and d.i_product_type='08' and 
            //                                   a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
            //                                   and a.i_product_motif=c.i_product_motif 
            //                                   and a.i_store=c.i_store and a.i_store_location=c.i_store_location
            //                                   group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, a.e_product_name, 
            //                                   b.v_product_retail
            //                                   union all
            //                                   select a.i_product, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
            //                                   b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
            //                                   from f_mutasi_stock_pusat('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
            //                                   where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
            //                                   and a.i_product=d.i_product and d.i_product_type='08' and 
            //                                   a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
            //                                   and a.i_product_motif=c.i_product_motif 
            //                                   and a.i_store=c.i_store and a.i_store_location=c.i_store_location
            //                                   group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, a.e_product_name, 
            //                                   b.v_product_retail
            //                                   ) as x
            //                                   group by x.i_store, x.i_store_location, x.i_product, x.e_mutasi_periode, x.e_product_name, 
            //                                   x.v_product_retail
            //                                   order by x.i_store, x.i_store_location, x.e_product_name", false);
            /*
                   $query = $this->db->query(" select a.i_product, a.e_product_name,a.i_store, a.i_store_location, 
                                      b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                      sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                      sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                      from f_mutasi_stock_daerah_all('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
                                      where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
                                      and a.i_product=d.i_product and d.i_product_type='08' and 
                                      a.i_product=c.i_product and a.i_product_grade=c.i_product_grade and a.i_product_motif=c.i_product_motif 
                                      and a.i_store=c.i_store and a.i_store_location=c.i_store_location
                                      group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, a.e_product_name, 
                                      b.v_product_retail
                                      order by a.i_store, a.i_store_location, a.e_product_name",false);
  */
            // } elseif ($r == 'qqq') {
            //    $query = $this->db->query("select x.i_product, x.e_product_name,x.i_store, x.i_store_location, x.e_mutasi_periode, 
            //                                   x.v_product_retail, sum(x.n_git_penjualan) as n_git_penjualan, 
            //                                   sum(x.n_mutasi_git) as n_mutasi_git, sum(x.n_mutasi_penjualan) as n_mutasi_penjualan, 
            //                                   sum(x.n_saldo_akhir) as n_saldo_akhir, sum(x.n_saldo_stockopname) as n_saldo_stockopname, 
            //                                   sum(x.n_quantity_stock) as n_quantity_stock
            //                                   from( 
            //                                   select a.i_product, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
            //                                   b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
            //                                   from f_mutasi_stock_daerah_all('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
            //                                   where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
            //                                   and a.i_product=d.i_product 
            //                                   and d.i_product_type<>'08' and d.i_product_type<>'03' and d.i_product_type<>'09' and 
            //                                   a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
            //                                   and a.i_product_motif=c.i_product_motif 
            //                                   and a.i_store=c.i_store and a.i_store_location=c.i_store_location
            //                                   group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, a.e_product_name, 
            //                                   b.v_product_retail
            //                                   union all
            //                                   select a.i_product, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
            //                                   b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
            //                                   from f_mutasi_stock_pusat('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
            //                                   where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
            //                                   and a.i_product=d.i_product 
            //                                   and d.i_product_type<>'08' and d.i_product_type<>'03' and d.i_product_type<>'09' and 
            //                                   a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
            //                                   and a.i_product_motif=c.i_product_motif 
            //                                   and a.i_store=c.i_store and a.i_store_location=c.i_store_location
            //                                   group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, a.e_product_name, 
            //                                   b.v_product_retail
            //                                   ) as x
            //                                   group by x.i_store, x.i_store_location, x.i_product, x.e_mutasi_periode, x.e_product_name, 
            //                                   x.v_product_retail
            //                                   order by x.i_store, x.i_store_location, x.e_product_name", false);
            /*
                   $query = $this->db->query(" select a.i_product, a.e_product_name,a.i_store, a.i_store_location, 
                                      b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
                                      sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
                                      sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
                                      from f_mutasi_stock_daerah_all('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
                                      where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
                                      and a.i_product=d.i_product 
                                      and d.i_product_type<>'08' and d.i_product_type<>'03' and d.i_product_type<>'09' and 
                                      a.i_product=c.i_product and a.i_product_grade=c.i_product_grade and a.i_product_motif=c.i_product_motif 
                                      and a.i_store=c.i_store and a.i_store_location=c.i_store_location
                                      group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, a.e_product_name, 
                                      b.v_product_retail
                                      order by a.i_store, a.i_store_location, a.e_product_name",false);
  */
            // } elseif ($l == 'qqq') {
            //    $query = $this->db->query("select x.i_product, x.e_product_name,x.i_store, x.i_store_location, x.e_mutasi_periode, 
            //                                   x.v_product_retail, sum(x.n_git_penjualan) as n_git_penjualan, 
            //                                   sum(x.n_mutasi_git) as n_mutasi_git, sum(x.n_mutasi_penjualan) as n_mutasi_penjualan, 
            //                                   sum(x.n_saldo_akhir) as n_saldo_akhir, sum(x.n_saldo_stockopname) as n_saldo_stockopname, 
            //                                   sum(x.n_quantity_stock) as n_quantity_stock
            //                                   from( 
            //                                   select a.i_product, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
            //                                   b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
            //                                   from f_mutasi_stock_daerah_all('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
            //                                   where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
            //                                   and a.i_product=d.i_product and d.i_product_type='09' and 
            //                                   a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
            //                                   and a.i_product_motif=c.i_product_motif 
            //                                   and a.i_store=c.i_store and a.i_store_location=c.i_store_location
            //                                   group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, a.e_product_name, 
            //                                   b.v_product_retail
            //                                   union all
            //                                   select a.i_product, a.e_product_name,a.i_store, a.i_store_location, a.e_mutasi_periode, 
            //                                   b.v_product_retail, sum(n_git_penjualan) as n_git_penjualan, sum(n_mutasi_git) as n_mutasi_git,
            //                                   sum(n_mutasi_penjualan) as n_mutasi_penjualan, sum(n_saldo_akhir) as n_saldo_akhir,
            //                                   sum(n_saldo_stockopname) as n_saldo_stockopname, sum(c.n_quantity_stock) as n_quantity_stock
            //                                   from f_mutasi_stock_pusat('$iperiode') a, tr_product_price b, tm_ic c, tr_product d
            //                                   where a.i_product=b.i_product and a.i_product_grade=b.i_product_grade and b.i_price_group='00' 
            //                                   and a.i_product=d.i_product and d.i_product_type='09' and 
            //                                   a.i_product=c.i_product and a.i_product_grade=c.i_product_grade 
            //                                   and a.i_product_motif=c.i_product_motif 
            //                                   and a.i_store=c.i_store and a.i_store_location=c.i_store_location
            //                                   group by a.i_store, a.i_store_location, a.i_product, e_mutasi_periode, a.e_product_name, 
            //                                   b.v_product_retail
            //                                   ) as x
            //                                   group by x.i_store, x.i_store_location, x.i_product, x.e_mutasi_periode, x.e_product_name, 
            //                                   x.v_product_retail
            //                                   order by x.i_store, x.i_store_location, x.e_product_name", false);
            // }
         }

         if ($query->num_rows() > 0) {
            $no       = 1;
            $store    = '';
            $storeloc = '';
            foreach ($query->result() as $raw) {
               if (($raw->i_store != $store && $store != '') || ($raw->i_store_location != $storeloc && $storeloc != '')) {
                  $no = 1;
               }
               $loc = ($raw->i_store_location == 'PB') ? 'Konsinyasi' : 'Reguler';
               $prodname = $raw->e_product_name;
               $mutasijual = $raw->n_mutasi_penjualan;
               $mutasisj = $raw->n_mutasi_penjualan + $raw->n_git_penjualan;
               $saldoakhir = $raw->n_saldo_akhir; #-$raw->n_git_penjualan-$raw->n_mutasi_git;
               $harga = $raw->v_product_retail;
               $so = $raw->n_saldo_stockopname + $raw->n_git_penjualan + $raw->n_mutasi_git;

               $nilaijual = $mutasijual * $harga;
               $nilaisaldo = $saldoakhir * $harga;
               $totjual = $totjual + $nilaijual;
               $totsaldo = $totsaldo + $nilaisaldo;

               $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $no, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $raw->i_store, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $loc, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $raw->i_product, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $raw->e_product_name, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $harga, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $mutasijual, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $mutasisj, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $saldoakhir, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $so, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . $i, $raw->n_quantity_stock, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('L' . $i, $nilaijual, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('M' . $i, $nilaisaldo, Cell_DataType::TYPE_NUMERIC);
               $store = $raw->i_store;
               $storeloc = $raw->i_store_location;
               $i++;
               $j++;
               $no++;
            }
            $x = $i - 1;
         }
         $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, 'TOTAL');
         $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $totjual);
         $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $totsaldo);
         $objPHPExcel->getActiveSheet()->getStyle('L' . $i . ':M' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER);
         $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
         $nama = 'lapgudnasx-' . $judul . '-' . $iperiode . '.xls';
         if (file_exists('spb/00/' . $nama)) {
            @chmod('excel/00/' . $nama, 0777);
            @unlink('excel/00/' . $nama);
         }
         $objWriter->save('excel/00/' . $nama);
         @chmod('excel/00/' . $nama, 0777);

         $this->logger->writenew('Export Laporan Gudang Per Divisi Gudang Nasional Periode:' . $iperiode);

         $data['sukses']   = true;
         $data['inomor']   = $nama;
         $data['folder']   = "excel/00";

         $this->load->view('nomorurl', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function get_group()
   {
      header("Content-Type: application/json", true);
      // $isalesman    = $this->input->post('isalesman');
      // $icity        = $this->input->post('icity');
      // $iarea        = $this->input->post('iarea');
      // $iperiode   = $this->input->post('iperiode');

      $query  = array(
         'detail' => $this->mmaster->bacagroupbarang()->result_array()
      );
      echo json_encode($query);
   }
}
