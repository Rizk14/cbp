<?php
class Cform extends CI_Controller
{
	public $title = "Master Kategori Penjualan";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('kategoripenjualan/mmaster');
	}

	function index()
	{
		$is_cari = $this->input->post('is_cari');

		$cari	= strtoupper($this->input->post('cari'));
		$cari 	= str_replace("'", "", $cari);

		if ($is_cari == '')
			$is_cari = $this->uri->segment(4);
		if ($cari == '' && $is_cari == "1") $cari = $this->uri->segment(3);

		if ($is_cari == "1") {
			$config['base_url'] = base_url() . 'index.php/kategoripenjualan/cform/index/' . $cari . '/' . $is_cari . '/index/';
		} else {
			$config['base_url'] = base_url() . 'index.php/kategoripenjualan/cform/index/' . '/index/';
		}

		$config['total_rows'] 	= $this->db->count_all('tr_product_sales_category');
		$config['per_page'] 	= '10';
		$config['first_link'] 	= 'Awal';
		$config['last_link'] 	= 'Akhir';
		$config['next_link'] 	= 'Selanjutnya';
		$config['prev_link'] 	= 'Sebelumnya';
		$config['cur_page'] 	= $is_cari == "1" ? $this->uri->segment(6) : $this->uri->segment(4);
		$this->pagination->initialize($config);

		$data['page_title'] 	= $this->title;
		$data['isi']			= $is_cari == "1" ? $this->mmaster->bacasemua($config['per_page'], $this->uri->segment(6), $cari) : $this->mmaster->bacasemua($config['per_page'], $this->uri->segment(4), $cari);

		$this->logger->writenew("Membuka Master Kategori Penjualan");

		$this->load->view('kategoripenjualan/vform', $data);
	}

	function simpan()
	{
		$esalescategoryname = $this->input->post('esalescategoryname', TRUE);

		if ($esalescategoryname != '') {
			$this->db->trans_begin();

			$this->mmaster->insert($esalescategoryname);

			if (($this->db->trans_status() === FALSE)) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();

				$this->logger->writenew("Tambah Kategori Penjualan : " . $esalescategoryname);

				$data['sukses'] = true;
				$data['inomor'] = $esalescategoryname;

				$this->load->view('nomor', $data);
			}
		}
	}

	function edit()
	{
		$data['page_title'] = $this->title . " Update";

		if ($this->uri->segment(4)) {
			$isalescategory = $this->uri->segment(4);

			$data['isalescategory'] = $isalescategory;
			$data['isi'] 			= $this->mmaster->baca($isalescategory);

			$this->load->view('kategoripenjualan/vformupdate', $data);
		}
	}

	function update()
	{
		$isalescategory		= $this->input->post('isalescategory', TRUE);
		$esalescategoryname = $this->input->post('esalescategoryname', TRUE);
		$esalescategorynamex = $this->input->post('esalescategorynamex', TRUE);

		if ($isalescategory != '' && $esalescategoryname != '') {
			$this->db->trans_begin();

			$this->mmaster->update($isalescategory, $esalescategoryname);

			if (($this->db->trans_status() === FALSE)) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();

				$this->logger->writenew("Update Kategori Penjualan : ID " . $isalescategory . " = " . $esalescategorynamex . " menjadi " . $esalescategoryname);

				$data['sukses'] = true;
				$data['inomor'] = $esalescategoryname;

				$this->load->view('nomor', $data);
			}
		}
	}

	/* function delete()
	{
		$isalescategory		= $this->uri->segment(4);
		$this->mmaster->delete($isalescategory);

		$data['page_title'] 	= $this->title;
		$data['isalescategory'] = '';
		$data['isi']			= $this->mmaster->bacasemua();

		$this->load->view('kategoripenjualan/vmainform', $data);
	} */
}
