<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu530')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('infogudangcabang');
			$data['dfrom']	= '';
			$data['dto']	= '';
			$this->load->view('infogudangcabang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
	}
}



function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu530')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/infogudangcabang/cform/view/'.$dfrom.'/'.$dto.'/';
			$query = $this->db->query("select data.e_area_name,data.moms,data.babyjpy ,sum(data.nilaibaby) as nilaibaby ,sum(data.nilaimoms) as nilaimoms
										from(
										select a.f_spb_stockdaerah as pusat, c.e_area_name, b.e_product_groupname as moms, '' as babyjpy, sum(a.v_spb) as nilaimoms, 0 as nilaibaby
										from tm_spb a, tr_product_group b, tr_area c
										where a.f_spb_stockdaerah=true and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_spb <= to_date('$dto','dd-mm-yyyy')  
										and a.f_spb_cancel=false
										and a.i_product_group=b.i_product_group 
										and a.i_area=c.i_area 
										and b.e_product_groupname='Moms Baby'
										group by a.f_spb_stockdaerah, a.i_area, b.e_product_groupname, c.e_area_name
									union all
										select a.f_spb_stockdaerah as pusat, c.e_area_name, '' as moms, b.e_product_groupname as babyjpy, 0 as nilaimoms, sum(a.v_spb) as nilaibaby
										from tm_spb a, tr_product_group b, tr_area c
										where a.f_spb_stockdaerah=true and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_spb <= to_date('$dto','dd-mm-yyyy')  
										and a.f_spb_cancel=false
										and a.i_product_group=b.i_product_group 
										and a.i_area=c.i_area 
										and b.e_product_groupname='Baby Joy'
										group by a.f_spb_stockdaerah, a.i_area, b.e_product_groupname, c.e_area_name
										)as data
										group by e_area_name, moms, babyjpy
										order by e_area_name asc");



			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = 'all';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('infogudangcabang/mmaster');
			$data['page_title'] = $this->lang->line('infogudangcabang');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isi']	= $this->mmaster->bacaperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(6),$cari);

      $sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Membuka Data SPB Per Area tanggal:'.$dfrom.' sampai:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('infogudangcabang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
