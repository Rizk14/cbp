<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu317')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listsjpb');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listsjpb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu317')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		  = $this->input->post('dfrom');
			$dto		    = $this->input->post('dto');
			$icustomer	= $this->input->post('icustomer');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($icustomer=='') $icustomer	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listsjpb/cform/view/'.$dfrom.'/'.$dto.'/'.$icustomer.'/index/';
      $area1	= $this->session->userdata('i_area');
			if($area1=='PB' || $area1=='00'){
			  $query = $this->db->query(" select a.i_sjpb from tm_sjpb a, tr_area b, tr_customer c
                            where a.i_area=b.i_area and a.i_customer=c.i_customer and
                            (upper(a.i_sjpb) like '%$cari%' or upper(a.i_sjp) like '%$cari%') and 
                            a.i_customer='$icustomer' and 
                            (a.d_sjpb >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjpb <= to_date('$dto','dd-mm-yyyy'))
                            order by a.i_sjpb asc, a.d_sjpb desc ",false);
      }else{
			  $query = $this->db->query(" select a.i_sjpb from tm_sjpb a, tr_area b, tr_customer c
                          where a.i_area=b.i_area and a.i_customer=c.i_customer and
                          (upper(a.i_sjpb) like '%$cari%' or upper(a.i_sjp) like '%$cari%') and 
                          a.i_customer='$icustomer' and a.i_area_entry='$area1' and 
                          (a.d_sjpb >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjpb <= to_date('$dto','dd-mm-yyyy'))
                          order by a.i_sjpb asc, a.d_sjpb desc ",false);
      }
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpb');
			$this->load->model('listsjpb/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['icustomer']	= $icustomer;
			$data['isi']		= $this->mmaster->bacaperiode($icustomer,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SJPB Customer '.$icustomer.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listsjpb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}	
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu317')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listsjpb');
			$this->load->view('listsjpb/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu317')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isjpb	= $this->uri->segment(4);
			$icustomer= $this->uri->segment(5);
			$dfrom= $this->uri->segment(6);
			$dto	= $this->uri->segment(7);
			$this->load->model('listsjpb/mmaster');
			$this->mmaster->delete($isjpb,$icustomer);

	    $sess=$this->session->userdata('session_id');
	    $id=$this->session->userdata('user_id');
	    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	    $rs		= pg_query($sql);
	    if(pg_num_rows($rs)>0){
		    while($row=pg_fetch_assoc($rs)){
			    $ip_address	  = $row['ip_address'];
			    break;
		    }
	    }else{
		    $ip_address='kosong';
	    }
	    $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
	    }
	    $pesan='Hapus SJPB No:'.$isj.' Customer:'.$icustomer;
	    $this->load->model('logger');
	    $this->logger->write($id, $ip_address, $now , $pesan );  

			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/listsjpb/cform/view/'.$dfrom.'/'.$dto.'/'.$icustomer.'/index/';
      $area1	= $this->session->userdata('i_area');
			if($area1=='00'){
			  $query = $this->db->query(" select a.i_sjpb from tm_sjpb a, tr_area b, tr_customer c
                            where a.i_area=b.i_area and a.i_customer=c.i_customer and
                            (upper(a.i_sjpb) like '%$cari%' or upper(a.i_sjp) like '%$cari%') and 
                            a.i_customer='$icustomer' and 
                            (a.d_sjpb >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjpb <= to_date('$dto','dd-mm-yyyy'))
                            order by a.i_sjpb asc, a.d_sjpb desc ",false);
      }else{
			  $query = $this->db->query("  select a.i_sjpb from tm_sjpb a, tr_area b, tr_customer c
                          where a.i_area=b.i_area and a.i_customer=c.i_customer and
                          (upper(a.i_sjpb) like '%$cari%' or upper(a.i_sjp) like '%$cari%') and 
                          a.i_customer='$icustomer' and a.i_area='$area1' and 
                          (a.d_sjpb >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjpb <= to_date('$dto','dd-mm-yyyy'))
                          order by a.i_sjpb asc, a.d_sjpb desc ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpb');
			$this->load->model('listsjpb/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['icustomer']	= $icustomer;
			$data['isi']		= $this->mmaster->bacaperiode($icustomer,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listsjpb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu317')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$icustomer	= $this->input->post('icustomer');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($icustomer=='') $icustomer	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listsjpb/cform/view/'.$dfrom.'/'.$dto.'/'.$icustomer.'/index/';
      $area1	= $this->session->userdata('i_area');
			if($area1=='00'){
			  $query = $this->db->query("  select a.i_sjpb from tm_sjpb a, tr_area b, tr_customer c
                            where a.i_area=b.i_area and a.i_customer=c.i_customer and
                            (upper(a.i_sjpb) like '%$cari%' or upper(a.i_sjp) like '%$cari%') and 
                            a.i_customer='$icustomer' and 
                            (a.d_sjpb >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjpb <= to_date('$dto','dd-mm-yyyy'))
                            order by a.i_sjpb asc, a.d_sjpb desc ",false);
      }else{
			  $query = $this->db->query("  select a.i_sjpb from tm_sjpb a, tr_area b, tr_customer c
                          where a.i_area=b.i_area and a.i_customer=c.i_customer and
                          (upper(a.i_sjpb) like '%$cari%' or upper(a.i_sjp) like '%$cari%') and 
                          a.i_customer='$icustomer' and a.i_area='$area1' and 
                          (a.d_sjpb >= to_date('$dfrom','dd-mm-yyyy') and a.d_sjpb <= to_date('$dto','dd-mm-yyyy'))
                          order by a.i_sjpb asc, a.d_sjpb desc ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpb');
			$this->load->model('listsjpb/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['icustomer']	= $icustomer;
			$data['isi']		= $this->mmaster->bacaperiode($icustomer,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listsjpb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu317')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listsjpb/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listsjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listsjpb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu317')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listsjpb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listsjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listsjpb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu317')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listsjpb/cform/customer/index/';
      $area1	= $this->session->userdata('i_area');
      if($area1=='PB' || $area1=='00'){
			  $query = $this->db->query(" select a.*, b.e_customer_name from tr_spg a, tr_customer b
                    where a.i_customer=b.i_customer order by a.i_customer",false);
      }else{
        $query = $this->db->query(" select a.*, b.e_customer_name from tr_spg a, tr_customer b, tr_customer_consigment c
                    where a.i_customer=b.i_customer and c.i_area_real='$area1' and b.i_customer=c.i_customer
                    order by a.i_customer",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listsjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(5));
			$this->load->view('listsjpb/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu317')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listsjpb/cform/customer/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
      $area1	= $this->session->userdata('i_area');
      if($area1=='PB' || $area1=='00'){
			  $query 	= $this->db->query("select a.*, b.e_customer_name from tr_spg a, tr_customer b
                            where a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                            or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
						                order by a.i_customer",false);
      }else{
			  $query 	= $this->db->query("select a.*, b.e_customer_name from tr_spg a, tr_customer b, tr_customer_consigment c
                            where a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                            or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%') and b.i_area_real='$area1' 
                            and b.i_customer=c.i_customer
						                order by a.i_customer",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listsjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listsjpb/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
