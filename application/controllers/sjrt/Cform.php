<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dsj 	  = $this->input->post('dsj', TRUE);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
				/*$thbl=substr($th,2,2).$bl;*/
				$thbl=$th.$bl;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$ittb		= $this->input->post('ittb', TRUE);
			$dttb	 	= $this->input->post('dttb', TRUE);
			if($dttb!=''){
				$tmp=explode("-",$dttb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dttb=$th."-".$bl."-".$hr;
			}
			$eareaname	= $this->input->post('eareaname', TRUE);
			$vspbnetto=$this->input->post('vsj', TRUE);
			$vspbnetto= str_replace(',','',$vspbnetto);
			$jml	  = $this->input->post('jml', TRUE);
			if($dsj!='' && $eareaname!='' && $ittb!='')
			{
				$gaono=true;
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					if($cek=='on'){
						$gaono=false;
					}
					if(!$gaono) break;
				}
				if(!$gaono){
					$this->db->trans_begin();
					$this->load->model('sjrt/mmaster');
					$isj		 		= $this->mmaster->runningnumbersj($iarea,$thbl);
					$this->mmaster->insertsjheader($ittb,$dttb,$isj,$dsj,$iarea,$vspbnetto);
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
						if($cek=='on'){
						  $iproduct			= $this->input->post('iproduct'.$i, TRUE);
						  $iproductgrade= 'A';
						  $iproductmotif= $this->input->post('motif'.$i, TRUE);
						  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
						  $vunitprice		= str_replace(',','',$vunitprice);
						  $ndeliver			= $this->input->post('ndeliver'.$i, TRUE);
						  $ndeliver			= str_replace(',','',$ndeliver);
						  $eremark  		= $this->input->post('eremark'.$i, TRUE);
						  if($eremark=='')$eremark=null;
						  if($ndeliver>0){
							  $this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
										                            $vunitprice,$ittb,$dttb,$isj,$dsj,$iarea,$eremark,$i,$i);
  					  }
						}
					}
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();

            $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		=  $this->db->query($sql);
			      if($rs->num_rows>0){
				      foreach($rs->result() as $tes){
					      $ip_address	  = $tes->ip_address;
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }

			      $data['user']	= $this->session->userdata('user_id');
      #			$data['host']	= $this->session->userdata('printerhost');
			      $data['host']	= $ip_address;
			      $data['uri']	= $this->session->userdata('printeruri');
			      $query 	= pg_query("SELECT current_timestamp as c");
			      while($row=pg_fetch_assoc($query)){
				      $now	  = $row['c'];
			      }
			      $pesan='Input SJ Retur Toko No:'.$isj.' Area:'.$iarea;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan );

						$data['sukses']			= true;
						$data['inomor']			= $isj;
						$this->load->view('nomor',$data);
					}
				}
			}else{
				$this->load->model('sjrt/mmaster');
				$data['page_title'] = $this->lang->line('sjrt');
				$data['isjrt']			= '';
				$data['isi']				= $this->mmaster->bacasemua();
				$data['detail']			= "";
				$data['jmlitem']		= "";
				$data['dsjrt']			= date('Y-m-d');
				$data['ittb']			= '';
				$data['dttb']			= '';
				$data['iarea']			= '';
				$data['istore']			= '';
				$data['isjold']			= '';
				$data['eareaname']	= $eareaname;
				$this->load->view('sjrt/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjrt');
			$this->load->view('sjrt/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjrt')." update";
			if($this->uri->segment(4)!=''){
				$isjrt	= $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$dfrom= $this->uri->segment(6);
				$dto 	= $this->uri->segment(7);
				$ittb= $this->uri->segment(8);
				$data['isjrt'] 	= $isjrt;
				$data['iarea']= $iarea;
				$data['dfrom']= $dfrom;
				$data['dto']	= $dto;
				$query 	= $this->db->query("select * from tm_sjrt_item where i_sjr = '$isjrt' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('sjrt/mmaster');
				$data['isi']=$this->mmaster->baca($isjrt,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isjrt,$iarea,$ittb);
		 		$this->load->view('sjrt/vmainform',$data);
			}else{
				$this->load->view('sjrt/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj	= $this->input->post('isj', TRUE);
			$dsj 	= $this->input->post('dsj', TRUE);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
				$thbl	= substr($th,2,2).$bl;
				$tmpsj	= explode("-",$isj);
				$firstsj= $tmpsj[0];
				$lastsj	= $tmpsj[2];
				$newsj	= $firstsj."-".$thbl."-".$lastsj;				
			}
			$iarea		= $this->input->post('iarea', TRUE); // area_to
			$ittb		= $this->input->post('ittb', TRUE);
			$dttb	 	= $this->input->post('dttb', TRUE);
			if($dttb!=''){
				$tmp=explode("-",$dttb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dttb=$th."-".$bl."-".$hr;
			}
			$eareaname	= $this->input->post('eareaname', TRUE);
			$vspbnetto= $this->input->post('vsj', TRUE);
			$vspbnetto= str_replace(',','',$vspbnetto);
			$jml	  = $this->input->post('jml', TRUE);
			$gaono=true;
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$gaono=false;
				}
				if(!$gaono) break;
			}
			if(!$gaono){
				$this->db->trans_begin();
				$this->load->model('sjrt/mmaster');
				$this->mmaster->updatesjheader($isj,$iarea,$dsj,$vspbnetto);
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					$iproduct		= $this->input->post('iproduct'.$i, TRUE);
					$iproductgrade	= 'A';
					$iproductmotif	= $this->input->post('motif'.$i, TRUE);
					$ndeliver		= $this->input->post('ndeliver'.$i, TRUE);
					$ndeliver		= str_replace(',','',$ndeliver);
					$this->mmaster->deletesjdetail($isj, $iarea, $iproduct, $iproductgrade, $iproductmotif);
					if($cek=='on'){
					  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
					  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
					  $vunitprice		= str_replace(',','',$vunitprice);
					  $eremark  		= $this->input->post('eremark'.$i, TRUE);
					  if($eremark=='')$eremark=null;
					  if($ndeliver>0){
						  $this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                                             $vunitprice,$ittb,$dttb,$isj,$dsj,$iarea,$eremark,$i);                      

					  }
					}
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

          $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		=  $this->db->query($sql);
		      if($rs->num_rows>0){
			      foreach($rs->result() as $tes){
				      $ip_address	  = $tes->ip_address;
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }

		      $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
		      $data['host']	= $ip_address;
		      $data['uri']	= $this->session->userdata('printeruri');
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Update SJ Retur Toko No:'.$isj.' Area:'.$iarea;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $newsj;
					$this->load->view('nomor',$data);
				}
			}
    }
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj	= $this->input->post('isjdelete', TRUE);
			$this->load->model('sjrt/mmaster');
			$this->mmaster->delete($isj);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		=  $this->db->query($sql);
      if($rs->num_rows>0){
	      foreach($rs->result() as $tes){
		      $ip_address	  = $tes->ip_address;
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }

      $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
      $data['host']	= $ip_address;
      $data['uri']	= $this->session->userdata('printeruri');
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
	      $now	  = $row['c'];
      }
      $pesan='Hapus SJ retur Toko No:'.$isj;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('sjrt');
			$data['isj']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('sjrt/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj			= $this->input->post('isjdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
  			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$this->db->trans_begin();
			$this->load->model('sjrt/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $isj, $iproductmotif);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
				$data['page_title'] = $this->lang->line('sjrt')." Update";
				$query = $this->db->query("select * from tm_ttb_item where i_ttb = '$isj'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['isj'] = $isj;
				$data['isi']=$this->mmaster->baca($isj);
				$data['detail']=$this->mmaster->bacadetail($isj);
				$this->load->view('sjrt/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/sjrt/cform/area/'.$baris.'/sikasep/';
      		$iuser   = $this->session->userdata('user_id');
      		$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
         	$data['cari']='';
			$this->load->model('sjrt/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6),$iuser);
			$this->load->view('sjrt/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			/*$config['base_url'] = base_url().'index.php/sjrt/cform/area/index/';
			$iuser   	= $this->session->userdata('user_id');
			$cari    	= $this->input->post('cari', FALSE);
			$cari 		  = strtoupper($cari);
			$query   	= $this->db->query("select * from tr_area
			                      where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
			                      and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);*/

			$iuser   	= $this->session->userdata('user_id');

	      	$baris   = $this->input->post('baris', FALSE);
	        if($baris=='')$baris=$this->uri->segment(4);
	        $cari   = ($this->input->post('cari', FALSE));
	        if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
	        if($cari!='sikasep')
	        $config['base_url'] = base_url().'index.php/sjrt/cform/cariarea/'.$baris.'/'.$cari.'/';
	          else
	        $config['base_url'] = base_url().'index.php/sjrt/cform/cariarea/'.$baris.'/sikasep/';

	      	$query   	= $this->db->query("select * from tr_area
	                              where (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%' 
	                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('sjrt/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['baris']=$baris;
         	$data['cari']=$cari;
      		$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6),$iuser);
			$this->load->view('sjrt/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function ttb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris=$this->uri->segment(4);
			$iarea=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/sjrt/cform/ttb/'.$baris.'/'.$iarea.'/';
			$query = $this->db->query(" select distinct(a.i_ttb) from tm_ttbretur a, tm_ttbretur_item b
							                    where a.i_area='$iarea' and a.f_ttb_cancel='f' and a.i_sjr is null
							                    and a.i_ttb=b.i_ttb and a.i_area=b.i_area",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('sjrt/mmaster');
			$data['page_title'] = $this->lang->line('listttbretur');
			$data['iarea']=$iarea;
			$data['baris']=$baris;
         	$data['cari']='';
			$data['isi']=$this->mmaster->bacattb($iarea,$config['per_page'],$this->uri->segment(6));
			$this->load->view('sjrt/vlistttb', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carittb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris   = $this->input->post('baris', FALSE);
	        if($baris=='')$baris=$this->uri->segment(4);
			$iarea = $this->input->post('iarea', FALSE);
	        if($iarea=='')$iarea=$this->uri->segment(5);
	        $cari   = ($this->input->post('cari', FALSE));
	        if($cari=='' && $this->uri->segment(6)!='sikasep')$cari=$this->uri->segment(6);

	        if($cari!='sikasep')
	        $config['base_url'] = base_url().'index.php/sjrt/cform/carittb/'.$baris.'/'.$iarea.'/'.$cari.'/';
	          else
	        $config['base_url'] = base_url().'index.php/sjrt/cform/carittb/'.$baris.'/'.$iarea.'/sikasep/';

			$query = $this->db->query(" select distinct(a.*) from tm_ttbretur a, tm_ttbretur_item b
							                    where a.i_area='$iarea' and a.f_ttb_cancel='f' and a.i_sjr is null
							                    and a.i_ttb=b.i_ttb and a.i_area=b.i_area and (upper(a.i_ttb) ilike '%$cari%') ",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('sjrt/mmaster');
			$data['iarea']=$iarea;
			$data['baris']=$baris;
         	$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('listttbretur');
			$data['isi']=$this->mmaster->carittb($cari,$iarea,$config['per_page'],$this->uri->segment(7));
			$this->load->view('sjrt/vlistttb', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/sjrt/cform/index/';
			$query = $this->db->query("select * from tm_ttb
						   where upper(i_ttb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('sjrt/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_ttb');
			$data['isj']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('sjrt/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function hitung()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ittb		= $this->uri->segment(4);
			$dttb		= $this->uri->segment(5);
      if($dttb!=''){
				$tmp=explode("-",$dttb);
				$tah=$tmp[2];
			}
			$iarea	= $this->uri->segment(6);
			$query = $this->db->query("	select a.i_product
                                  from tr_product_motif a, tm_ttbretur_item b, tr_product c
                                  where a.i_product=c.i_product and b.i_product1_motif=a.i_product_motif 
                                  and c.i_product=b.i_product1 
                                  and b.i_ttb='$ittb' and b.i_area='$iarea' and b.n_ttb_year=$tah order by b.n_item_no ",false);
			$data['jmlitem'] = $query->num_rows(); 
			$dsj 	= $this->uri->segment(8);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
			}
			$ittb		  = $this->uri->segment(4);
			$dttb		  = $this->uri->segment(5);
			$iarea		  = $this->uri->segment(6);
			$eareaname	= $this->uri->segment(7);
			$data['page_title'] = $this->lang->line('sjrt');
			$data['isjrt']	= '';
			$this->load->model('sjrt/mmaster');
			$data['isi']	= "xxxxx";
			$data['dsjrt']	= $dsj;
			$data['ittb']	= $ittb;
			$data['dttb']	= $dttb;
			$data['iarea']	= $iarea;
			$data['eareaname']= $eareaname;
			$data['detail']	= $this->mmaster->product($ittb,$iarea,$tah);
			$this->load->view('sjrt/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu213')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari = strtoupper($this->input->post('cari', FALSE));
      $baris = strtoupper($this->input->post('baris', FALSE));
      $ittb = strtoupper($this->input->post('ttb', FALSE));
 			if($baris=='') $baris=$this->uri->segment(4);
 			if($ittb=='') $ittb=$this->uri->segment(5);
 			if($iarea=='') $iarea=$this->uri->segment(6);
      if($this->uri->segment(7)!='x01'){
        if($cari=='') $cari=$this->uri->segment(7);
        $config['base_url'] = base_url().'index.php/sjrt/cform/product/'.$baris.'/'.$ittb.'/'.$iarea.'/'.$cari.'/';
      }else{
        $config['base_url'] = base_url().'index.php/sjrt/cform/product/'.$baris.'/'.$ittb.'/'.$iarea.'/x01/';
      }
			$query = $this->db->query(" select a.i_product, a.e_product_name, b.v_unit_price, c.i_product_motif, c.e_product_motifname 
                                  from tr_product a, tm_ttbretur_item b, tr_product_motif c
                                  where a.i_product=b.i_product1 
                                  and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                                  and b.i_product1=c.i_product and b.i_product1_motif=c.i_product_motif
                                  and b.i_area='$iarea' and b.i_ttb='$ittb'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
 			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('sjrt/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$this->uri->segment(4);
			$data['ittb']=$this->uri->segment(5);
			$data['iarea']=$this->uri->segment(6);
 			$data['isi']=$this->mmaster->bacaproduct($iarea,$ittb,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('sjrt/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
