<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu100')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listtppercity');
			$data['iperiode']	= '';
			$this->load->view('listtppercity/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu100')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$iarea		= $this->input->post('iarea');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			if($iarea=='') $iarea=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/listtppercity/cform/view/'.$iperiode.'/'.$iarea.'/index/';
			$query = $this->db->query(" select tr_city.e_city_name as kota, tm_target_itemkota.v_target as targetkota, 
										tr_salesman.e_salesman_name as sales, tm_target_itemsls.v_target as targetsales, 
										tr_area.e_area_name as area, tm_target.v_target as targetarea,
										tm_target_itemkota.v_nota_netto as penjualan
										from tm_target_itemkota
										inner join tm_target_itemsls on (tm_target_itemkota.i_area=tm_target_itemsls.i_area 
														 and tm_target_itemkota.i_salesman=tm_target_itemsls.i_salesman
														 and tm_target_itemkota.i_periode=tm_target_itemsls.i_periode
														 and tm_target_itemsls.i_periode='$iperiode')
										inner join tm_target on (tm_target_itemkota.i_area=tm_target.i_area 
													 and tm_target_itemkota.i_periode=tm_target_itemsls.i_periode
													 and tm_target.i_periode='$iperiode')
										inner join tr_city on (tm_target_itemkota.i_city=tr_city.i_city
												   and tm_target_itemkota.i_area=tr_city.i_area)
										inner join tr_salesman on (tm_target_itemkota.i_salesman=tr_salesman.i_salesman)
										inner join tr_area on (tm_target_itemkota.i_area=tr_area.i_area)
										where tm_target_itemkota.i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);

			$this->load->model('listtppercity/mmaster');
			$data['page_title'] = $this->lang->line('listtppercity');
			$data['cari']		= $cari;
			$data['iperiode']	= $iperiode;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->baca($iarea,$iperiode,$config['per_page'],$this->uri->segment(7),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Target penjualan per kota untuk Area '.$iarea.' Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listtppercity/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu100')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$iarea		= $this->input->post('iarea');
			$dto		= $this->input->post('dto');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listtppercity/cform/view/'.$iperiode.'/'.$iarea.'/index/';
			$query = $this->db->query(" select tr_city.e_city_name as kota, tm_target_itemkota.v_target as targetkota, 
										tr_salesman.e_salesman_name as sales, tm_target_itemsls.v_target as targetsales, 
										tr_area.e_area_name as area, tm_target.v_target as targetarea,
										tm_target_itemkota.v_nota_netto as penjualan
										from tm_target_itemkota
										inner join tm_target_itemsls on (tm_target_itemkota.i_area=tm_target_itemsls.i_area 
														 and tm_target_itemkota.i_salesman=tm_target_itemsls.i_salesman
														 and tm_target_itemkota.i_periode=tm_target_itemsls.i_periode
														 and tm_target_itemsls.i_periode='$iperiode')
										inner join tm_target on (tm_target_itemkota.i_area=tm_target.i_area 
													 and tm_target_itemkota.i_periode=tm_target_itemsls.i_periode
													 and tm_target.i_periode='$iperiode')
										inner join tr_city on (tm_target_itemkota.i_city=tr_city.i_city
												   and tm_target_itemkota.i_area=tr_city.i_area)
										inner join tr_salesman on (tm_target_itemkota.i_salesman=tr_salesman.i_salesman)
										inner join tr_area on (tm_target_itemkota.i_area=tr_area.i_area)
										where tm_target_itemkota.i_area='$iarea' and (upper(tr_city.e_city_name) like '%$cari%' or 
										upper(tr_city.i_city) like '%$cari%')",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link']	= 'Akhir';
			$config['next_link']	= 'Selanjutnya';
			$config['prev_link']	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('listtppercity/mmaster');
			$data['page_title'] = $this->lang->line('listtppercity');
			$data['cari']		= $cari;
			$data['iperiode']	= $iperiode;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->cari($iarea,$iperiode,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('listtppercity/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu100')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listtppercity/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
											or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listtppercity/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listtppercity/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu292')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listtppercity/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listtppercity/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listtppercity/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
