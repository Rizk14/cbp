<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu259')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari			= strtoupper($this->input->post('cari'));
      $query = $this->db->query(" select distinct(a.i_sjp), a.d_sjp, a.d_sjp_receive, c.i_area, c.e_area_name
                                from tm_sjp a, tm_sjp_item b, tr_area c
                                where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.i_area=c.i_area and (b.n_quantity_receive is null
                                or to_char(a.d_sjp::timestamp with time zone, 'yyyymm'::text)<to_char(a.d_sjp_receive::timestamp with time zone, 'yyyymm'::text))
                                order by a.i_sjp",false);
			$config['base_url'] = base_url().'index.php/sjpgit/cform/index/';
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpgit');
			$this->load->model('sjpgit/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$data['cari'] = $cari;
			$data['isjp']   = '';
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SJP GIT';
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('sjpgit/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu259')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listsjpgit');
			$this->load->view('sjpgit/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu259')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listsjpgit')." Update";
			if($this->uri->segment(4)!=''){
				$isjp	= $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
   			$areah= $this->session->userdata('i_area');
        $data['areah']=$areah;
				$data['isjp'] = $isjp;
				$data['iarea']= $iarea;
				$query 	= $this->db->query("select * from tm_sjp_item where i_sjp = '$isjp' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('sjpgit/mmaster');
				$data['isi']=$this->mmaster->baca($isjp,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isjp,$iarea);
		 		$this->load->view('sjpgit/vmainform',$data);
			}else{
				$this->load->view('sjp/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu259')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari			= strtoupper($this->input->post('cari'));
      if($cari=='') $cari= $this->uri->segment(4);
      $query = $this->db->query(" select distinct(a.i_sjp), a.d_sjp, a.d_sjp_receive, c.i_area, c.e_area_name
                                from tm_sjp a, tm_sjp_item b, tr_area c
                                where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.i_area=c.i_area
                                and (a.i_sjp like '%$cari%' or b.i_product like '%$cari%' or b.e_product_name like '%$cari%')
                                and (b.n_quantity_receive is null
                                or to_char(a.d_sjp::timestamp with time zone, 'yyyymm'::text)<to_char(a.d_sjp_receive::timestamp with time zone, 'yyyymm'::text))
                                order by a.i_sjp
                                ",false);
			$config['base_url'] = base_url().'index.php/sjpgit/cform/cari/'.$cari.'/';
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpgit');
			$this->load->model('sjpgit/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5));
			$data['cari'] = $cari;
			$data['isjp']   = '';
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SJP GIT';
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('sjpgit/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
