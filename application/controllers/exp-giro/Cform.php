<?php
class Cform extends CI_Controller
{
	public $title   = "Giro Pelanggan";
	public $folder  = "exp-giro";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->model($this->folder . '/mmaster');
		require_once("php/fungsi.php");
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu236') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data = [
				'page_title' => $this->title,
				'folder'     => $this->folder,
				'iperiode'   => '',
				'iarea'      => '',
			];

			$this->load->view($this->folder . '/vform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu236') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$dfrom		= $this->uri->segment('4');
			$dto		= $this->uri->segment('5');
			$iperiode 	= '';

			// $a = substr($iperiode, 0, 4);
			// $b = substr($iperiode, 4, 2);
			// $peri = mbulan($b) . " - " . $a;

			if ($dfrom < '01-01-2014') {
				$this->db->select("	distinct on(a.i_giro)
									a.i_area,
									tr_area.e_area_name, 
									a.i_giro, 
									a.d_giro, 
									a.d_giro_terima,
									a.d_giro_cair, 
									a.i_dt as giro_dt,
									tm_dt.i_dt,
									tm_dt.d_dt, 
									tr_customer.i_customer, 
									tr_customer.e_customer_name, 
									a.e_giro_bank, 
									a.v_jumlah, 
									a.v_sisa, 
									a.f_posting,
									a.f_giro_tolak, 
									a.f_giro_batal, 
									tm_pelunasan.d_bukti,
									tm_pelunasan.i_pelunasan 
									from tm_giro a 
									inner join tr_area on(a.i_area=tr_area.i_area)
									inner join tr_customer on (a.i_customer=tr_customer.i_customer and a.i_area=tr_customer.i_area)
									left join tm_pelunasan on (a.i_giro=tm_pelunasan.i_giro and tm_pelunasan.i_area=a.i_area  
									and a.d_giro=tm_pelunasan.d_giro and 
									tm_pelunasan.i_jenis_bayar='01' and tm_pelunasan.f_pelunasan_cancel='f')
									left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
									where 
									/* to_char(a.d_giro_terima,'yyyymm')='$iperiode' */ 
									a.d_giro_terima BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
									and a.f_giro_batal_input='f'
									/* ORDER BY a.i_area, a.d_giro_terima, a.d_giro, a.i_giro */
									ORDER BY
										a.i_giro,
										a.i_area,
										a.d_giro_terima,
										a.d_giro ", false);
			} else {
				$this->db->select("		distinct on(a.i_giro)
										a.i_area,
										tr_area.e_area_name, 
										a.i_giro, 
										a.d_giro, 
										a.d_giro_terima,
										a.d_giro_cair, 
										a.i_dt AS giro_dt,
										tm_dt.i_dt,
										tm_dt.d_dt, 
										tr_customer.i_customer, 
										tr_customer.e_customer_name, 
										a.e_giro_bank, 
										a.v_jumlah, 
										a.v_sisa, 
										a.f_posting,
										a.f_giro_tolak, 
										a.f_giro_batal, 
										al.d_alokasi as d_bukti,
										al.i_alokasi as i_pelunasan
									FROM
										tm_giro a
										INNER JOIN tr_area ON (a.i_area = tr_area.i_area)
										INNER JOIN tr_customer ON (a.i_customer = tr_customer.i_customer AND a.i_area = tr_customer.i_area)
										LEFT JOIN tm_alokasi al ON a.i_giro = al.i_giro AND a.i_area = al.i_area AND al.f_alokasi_cancel = 'f'
										LEFT JOIN tm_dt ON (al.i_dt = tm_dt.i_dt AND al.i_area = tm_dt.i_area)
									WHERE
										a.d_giro_terima BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
										AND a.f_giro_batal_input = 'f'
									ORDER BY
										a.i_giro,
										a.i_area,
										a.d_giro_terima,
										a.d_giro ", false);
			}

			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar Giro Pelanggan ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar Giro Pelanggan');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 11, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Tanggal : ' . $dfrom . " s/d " . $dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 11, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:M5'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'No Giro');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tgl Giro');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Tgl Cair');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'No DT');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Tgl DT');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Tgl Pelunasan');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Toko');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Bank');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Jumlah');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Sisa');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Tgl Terima');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$i 	= 6;
				$j 	= 6;
				$no = 0;
				foreach ($query->result() as $row) {
					$no++;



					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':M' . $i
					);

					if ($row->i_dt == '') $row->i_dt = $row->giro_dt;

					if ($row->f_giro_tolak == 't') {
						$ket1 = 'GIRO TOLAK';
					} else $ket1 = '';

					if ($row->f_giro_batal == 't') {
						$ket2 = 'GIRO BATAL';
					} else $ket2 = '';

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->i_area . '-' . $row->e_area_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->i_giro, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->d_giro);
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->d_giro_cair);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->i_dt, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $row->d_dt);
					$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row->d_bukti);
					$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->e_customer_name);
					$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->e_giro_bank);
					$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->v_jumlah);
					$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $row->v_sisa);
					$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row->d_giro_terima);
					$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $ket1 . $ket2);
					$i++;
					$j++;
				}
				$x = $i - 1;
			}

			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama = 'GR(' . $dfrom . '_sd_' . $dto . ').xls';
			$objWriter->save('excel/00/' . $nama);

			$this->logger->writenew('Export Giro periode:' . $iperiode);

			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output', 'w');
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
