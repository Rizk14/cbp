<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu5')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_bbktype');
			$data['ibbktype']='';
			$this->load->model('bbktype/mmaster');
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('bbktype/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu5')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ibbktype 	= $this->input->post('ibbktype', TRUE);
			$ebbktypename 	= $this->input->post('ebbktypename', TRUE);

			if ((isset($ibbktype) && $ibbktype != '') && (isset($ebbktypename) && $ebbktypename != ''))
			{
				$this->load->model('bbktype/mmaster');
				$this->mmaster->insert($ibbktype,$ebbktypename);

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
          while($row=pg_fetch_assoc($rs)){
	          $ip_address	  = $row['ip_address'];
	          break;
          }
        }else{
          $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
          $now	  = $row['c'];
        }
        $pesan='Input Jenis BBK:('.$ibbktype.') - '.$ebbktypename;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu5')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_bbktype');
			$this->load->view('bbktype/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu5')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_bbktype')." update";
			if($this->uri->segment(4)){
				$ibbktype = $this->uri->segment(4);
				$data['ibbktype'] = $ibbktype;
				$this->load->model('bbktype/mmaster');
				$data['isi']=$this->mmaster->baca($ibbktype);
		 		$this->load->view('bbktype/vmainform',$data);
			}else{
				$this->load->view('bbktype/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu5')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ibbktype	= $this->input->post('ibbktype', TRUE);
			$ebbktypename 	= $this->input->post('ebbktypename', TRUE);
			$this->load->model('bbktype/mmaster');
			$this->mmaster->update($ibbktype,$ebbktypename);

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
          while($row=pg_fetch_assoc($rs)){
	          $ip_address	  = $row['ip_address'];
	          break;
          }
        }else{
          $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
          $now	  = $row['c'];
        }
        $pesan='Update Jenis BBK:('.$ibbktype.') - '.$ebbktypename;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu5')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ibbktype	= $this->uri->segment(4);
			$this->load->model('bbktype/mmaster');
			$this->mmaster->delete($ibbktype);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
        while($row=pg_fetch_assoc($rs)){
          $ip_address	  = $row['ip_address'];
          break;
        }
      }else{
        $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
        $now	  = $row['c'];
      }
      $pesan='Delete Jenis BBK:('.$ibbktype.') - '.$ebbktypename;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('master_bbktype');
			$data['ibbktype']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('bbktype/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
