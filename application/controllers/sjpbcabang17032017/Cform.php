<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu597')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $data['ispg']='';
      $data['istore']='';
      $data['istorelocation']='';
      $data['istorelocationbin']='';
      $data['isi']='';
      $data['isj']='';
      $data['detail']='';
      $data['iarea']='';
      $data['eareaname']='';
      $data['ecustomername']='';
      $data['icustomer']='';
      $data['dsj']='';
			$eareaname	= $this->input->post('eareaname', TRUE);
      $icustomer	= $this->input->post('icustomer',TRUE);
      $ispg	  = $this->input->post('ispg',TRUE);
			$vsjpb	= $this->input->post('vsjpb',TRUE);
			$vsjpb	= str_replace(',','',$vsjpb);
			$jml	  = $this->input->post('jml', TRUE);
			$data['page_title'] = $this->lang->line('sjpbcabang');
			$data['jmlitem']="";
      $this->load->view('sjpbcabang/vmainform',$data);
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu597')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/sjpbcabang/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
      $iuser   = $this->session->userdata('user_id');

			if($area1=='00'){
				$stquery = "select * from tr_area where f_area_consigment='t'";
			}else{
				$stquery = "select * from tr_area where f_area_consigment='t' and 
				            (i_area in ( select i_area from tm_user_area where i_user='$iuser') or i_area='PB') order by i_area";
			}
			$query = $this->db->query($stquery,false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('sjpbcabang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$iuser);
			$this->load->view('sjpbcabang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu597')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                                    where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                    and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);

			$config['base_url'] = base_url().'index.php/sjpbcabang/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area where f_area_consigment='t' and (upper(i_area) like '%$cari%' or 
				                           upper(e_area_name) like '%$cari%')",false);
			}else{
         $query   	= $this->db->query("select * from tr_area
                                  where f_area_consigment='t' and  (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                  and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sjpbcabang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$iuser);
			$this->load->view('sjpbcabang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu597')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari  = strtoupper($this->input->post('cari', FALSE));
      $area_real= $this->session->userdata('i_area');
      $iarea = strtoupper($this->input->post('iarea', FALSE));
 			if($iarea=='') $iarea=$this->uri->segment(4);
      if($this->uri->segment(5)!='x01' && $this->uri->segment(5)!=''){
        if($cari=='') $cari=$this->uri->segment(5);
        $config['base_url'] = base_url().'index.php/sjpbcabang/cform/customer/'.$area_real.'/'.$cari.'/';
      }else{
        $config['base_url'] = base_url().'index.php/sjpbcabang/cform/customer/'.$area_real.'/x01/';
      }
			$query 	= $this->db->query("select a.i_customer from tr_customer a, tr_customer_consigment b, tr_spg c
                                  where b.i_area_real='$area_real' and a.i_customer=b.i_customer and a.i_customer=c.i_customer
										              and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('sjpbcabang/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($cari,$area_real,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('sjpbcabang/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
  function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu597')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
      $istore = $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/sjpbcabang/cform/product/'.$istore.'/cekproduk/';
      if($istore=='PB'){
				$stquery = "select a.i_product, a.i_product_grade, a.i_product_motif, b.e_product_motifname, c.v_product_retail, 
                    a.i_product_grade, a.i_store, a.i_store_location, 
                    a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active
                    FROM tm_ic a, tr_product_motif b, tr_product c
                    where b.i_product_motif='00' and a.i_store_location='00' and 
                    a.i_product = b.i_product and a.i_product = c.i_product and i_store ='$istore'";
			}else{
				$stquery = "select a.i_product, a.i_product_grade, a.i_product_motif, b.e_product_motifname, c.v_product_retail, 
                    a.i_product_grade, a.i_store, a.i_store_location, 
                    a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active
                    FROM tm_ic a, tr_product_motif b, tr_product c
                    where b.i_product_motif='00' and a.i_store_location='PB' and a.i_product = b.i_product 
                    and a.i_product = c.i_product and i_store ='$istore'";
			}
			$query = $this->db->query($stquery,false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$baris;
			$data['istore']=$istore;
			$data['cari']='';
			$this->load->model('sjpbcabang/mmaster');
			$data['isi']=$this->mmaster->bacaproduct($istore,$config['per_page'],$this->uri->segment(6));
			$this->load->view('sjpbcabang/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu597')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
      $istore = $this->uri->segment(5);
	    $cari = strtoupper($this->input->post('cari', FALSE));
	    if ($cari == ''){
        $cari= $this->uri->segment(6);	    
	      if ($cari == ''){
			  	$cari = "all";			
			  }
	    }

			$config['base_url'] = base_url().'index.php/sjpbcabang/cform/cariproduct/'.$baris.'/'.$istore.'/'.$cari.'/';
			if ($cari != "all"){
        if($istore=='PB'){
				  $stquery = "select a.i_product, a.i_product_motif, b.e_product_motifname, c.v_product_retail, 
                      a.i_product_grade, a.i_store, a.i_store_location, 
                      a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active
                      FROM tm_ic a, tr_product_motif b, tr_product c
                      where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%') and
                      b.i_product_motif='00' and a.i_store_location='00' and 
                      a.i_product = b.i_product and a.i_product = c.i_product and i_store ='$istore'";
			  }else{
				  $stquery = "select a.i_product, a.i_product_motif, b.e_product_motifname, c.v_product_retail, 
                      a.i_product_grade, a.i_store, a.i_store_location, 
                      a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active
                      FROM tm_ic a, tr_product_motif b, tr_product c
                      where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%') and
                      b.i_product_motif='00' and a.i_store_location='PB' and a.i_product = b.i_product 
                      and a.i_product = c.i_product and i_store ='$istore'";
			  }
			}
      else{
        if($istore=='PB'){
				  $stquery = "select a.i_product, a.i_product_grade, a.i_product_motif, b.e_product_motifname, c.v_product_retail, 
                      a.i_product_grade, a.i_store, a.i_store_location, 
                      a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active
                      FROM tm_ic a, tr_product_motif b, tr_product c
                      where b.i_product_motif='00' and a.i_store_location='00' and 
                      a.i_product = b.i_product and a.i_product = c.i_product and i_store ='$istore'";
			  }else{
				  $stquery = "select a.i_product, a.i_product_grade, a.i_product_motif, b.e_product_motifname, c.v_product_retail, 
                      a.i_product_grade, a.i_store, a.i_store_location, 
                      a.i_store_locationbin, a.e_product_name, a.n_quantity_stock, a.f_product_active
                      FROM tm_ic a, tr_product_motif b, tr_product c
                      where b.i_product_motif='00' and a.i_store_location='PB' and a.i_product = b.i_product 
                      and a.i_product = c.i_product and i_store ='$istore'";
			  }
      }
			$query = $this->db->query($stquery,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('sjpbcabang/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$baris;
			if ($cari == "all")
			{$data['cari']='';}
			else
			{$data['cari']=$cari;}
      $data['istore']=$istore;
			$data['isi']=$this->mmaster->cariproduct($cari,$istore,$config['per_page'],$this->uri->segment(7));
			$this->load->view('sjpbcabang/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

  function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu597')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('sjpbcabang/mmaster');
      $dsj    = $this->input->post('dsj', TRUE);
      $iarea  = $this->input->post('iarea',TRUE) ;
			$this->db->trans_begin();
			if($dsj!=''){
				$tmp	  = explode("-",$dsj);
				$th	    = $tmp[2];
				$bl	    = $tmp[1];
				$hr	    = $tmp[0];
				$dsjpb  = $th."-".$bl."-".$hr;
				$thbl	  = $th.$bl;
				$tbl    = substr($th,2,2).$bl;
   			$isjpb 	= $this->mmaster->runningnumbersj($iarea,$thbl);
				$tmpsj	= explode("-",$isjpb);
				$firstsj= $tmpsj[0];
				$lastsj	= $tmpsj[2];
				$newsj	= $firstsj."-".$tbl."-".$lastsj;
			}
			$iarea      	    = $this->input->post('iarea', TRUE);
			$icustomer		    = $this->input->post('icustomer',TRUE);
			$jml	            = $this->input->post('jml', TRUE);
      $ispg	            = $this->input->post('ispg',TRUE);
      $vsjpb            = $this->input->post('nilai',TRUE);
			$vsjpb	          = str_replace(',','',$vsjpb);
			$istore	          = $this->input->post('istore', TRUE);
			$istorelocation   = $this->input->post('istorelocation', TRUE);
			$istorelocationbin= $this->input->post('istorelocationbin', TRUE);
#			if($istore=='PB'){
#        $istorelocation		= '00';
#			}else{
#        $istorelocation		= 'PB';		
#			}
#      $istorelocationbin	= '00';
			$this->mmaster->insertsjpb($isjpb, $icustomer, $iarea, $ispg, $dsjpb, $vsjpb);
			for($i=1;$i<=$jml;$i++){
				  $iproduct		  = substr($this->input->post('iproduct'.$i, TRUE),0,7);
          $ipricegroup  = substr($this->input->post('iproduct'.$i, TRUE),7,2);
				  $iproductgrade= 'A';
				  $iproductmotif= $this->input->post('motif'.$i, TRUE);
				  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		= $this->input->post('productprice'.$i, TRUE);
				  $vunitprice		= str_replace(',','',$vunitprice);
				  $ndeliver	  	= $this->input->post('ndeliver'.$i, TRUE);
				  $ndeliver		  = str_replace(',','',$ndeliver);
				  $norder   		= $this->input->post('norder'.$i, TRUE);
				  $norder		    = str_replace(',','',$norder);
				  if($ndeliver>0){
					  $this->mmaster->insertsjpbdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                                			     $vunitprice,$isjpb,$iarea,$i,$dsjpb,$ipricegroup);
#					      $this->mmaster->updatesjpdetail($isjp,$iarea,$iproduct,$iproductgrade,$iproductmotif,$ndeliver);
###
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
                $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                if(isset($trans)){
                  foreach($trans as $itrans)
                  {
                    $q_aw =$itrans->n_quantity_stock;
                    $q_ak =$itrans->n_quantity_stock;
                    $q_in =0;
                    $q_out=0;
                    break;
                  }
                }else{
                  $q_aw=0;
                  $q_ak=0;
                  $q_in=0;
                  $q_out=0;
                }
              }
              $this->mmaster->inserttrans04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,
              $eproductname,$isjpb,$q_in,$q_out,$ndeliver,$q_aw,$q_ak);
              $th=substr($dsjpb,0,4);
              $bl=substr($dsjpb,5,2);
              $emutasiperiode=$th.$bl;
	
              if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
              {
                $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
              }else{
                $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode,$q_aw);
              }
              if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
              {
                $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$q_ak);
              }else{
                $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ndeliver,$q_aw);
              }
###              
					  }else{
#                $this->mmaster->updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$iarea);
            }
					 #}else{
					 # $iproduct		= $this->input->post('iproduct'.$i, TRUE);
					 # $iproductgrade	= 'A';
					 # $iproductmotif	= $this->input->post('motif'.$i, TRUE);
           # $this->mmaster->updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,0,$iarea);
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
					$this->db->trans_rollback();
				}else{
				  $sess=$this->session->userdata('session_id');
				  $id=$this->session->userdata('user_id');
				  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				  $rs		= pg_query($sql);
				  if(pg_num_rows($rs)>0){
					  while($row=pg_fetch_assoc($rs)){
						  $ip_address	  = $row['ip_address'];
						  break;
					  }
				  }else{
					  $ip_address='kosong';
				  }
				  $query 	= pg_query("SELECT current_timestamp as c");
				  while($row=pg_fetch_assoc($query)){
					  $now	  = $row['c'];
				  }
				  $pesan='Input SJPB Cabang';
				  $this->load->model('logger');
				  $this->logger->write($id, $ip_address, $now , $pesan );
				  $data['sukses']			= true;
#				  $this->db->trans_rollback();
  				$this->db->trans_commit();
				  $data['inomor']			= $isjpb;
				  $this->load->view('nomor',$data);
				}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
