<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu447')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/realisasispbkons/cform/index/';
			$cari   = strtoupper($this->input->post('cari', FALSE));
			$area1	= $this->session->userdata('i_area1');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$query  = $this->db->query(" 	select a.i_spb, a.d_spb, a.i_area, c.e_area_name, a.i_customer, b.e_customer_name, a.i_salesman,
                                    a.v_spb_discounttotal, a.v_spb as kotor, (a.v_spb-a.v_spb_discounttotal) as bersih
                                    from tm_spb a, tr_customer b, tr_area c
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area
                                    and a.i_store is null and a.f_spb_consigment='t'
                                    and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' 
                                    or a.i_area='$area4' or a.i_area='$area5' )
                                    and (upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                                    or upper(b.e_customer_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['ada']	= 'xx';
      $data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('realisasispbkons');
			$this->load->model('realisasispbkons/mmaster');
			$data['isi']=$this->mmaster->bacasemua($area1,$area2,$area3,$area4,$area5,$cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('realisasispbkons/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function closing()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu447')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml = $this->input->post('jml', TRUE);
			$this->db->trans_begin();
			$this->load->model('realisasispbkons/mmaster');
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$ispb = $this->input->post('ispb'.$i, TRUE);
					$iarea= $this->input->post('iarea'.$i, TRUE);
          if($iarea='PB'){
            $istoreloc='00';
          }else{
            $istoreloc='PB';
          }
					$this->mmaster->updatespb($ispb, $iarea,$istoreloc);
				}
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Realisasi SPB Konsinyasi No:'.$ispb.' Area:'.$iarea;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );

			}
			$area1	= $this->session->userdata('i_area1');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/realisasispbkons/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));

			$query = $this->db->query(" select a.i_spb, a.d_spb, a.i_area, c.e_area_name, a.i_customer, b.e_customer_name, a.i_salesman,
                                  a.v_spb_discounttotal, a.v_spb as kotor, (a.v_spb-a.v_spb_discounttotal) as bersih
                                  from tm_spb a, tr_customer b, tr_area c
                                  where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area
                                  and a.i_store is null and a.f_spb_consigment='t'
                                  and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' 
                                  or a.i_area='$area4' or a.i_area='$area5' )
                                  and (upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                                  or upper(b.e_customer_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['ada']	= 'xx';
      $data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('realisasispbkons');
			$this->load->model('realisasispbkons/mmaster');
			$data['isi']=$this->mmaster->bacasemua($area1,$area2,$area3,$area4,$area5,$cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('realisasispbkons/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu447')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('realisasispbkons');
			$this->load->view('realisasispbkons/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
	    	($this->session->userdata('menu447')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area1');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/realisasispbkons/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" select a.i_spb, a.d_spb, a.i_area, c.e_area_name, a.i_customer, b.e_customer_name, a.i_salesman,
                                  a.v_spb_discounttotal, a.v_spb as kotor, (a.v_spb-a.v_spb_discounttotal) as bersih
                                  from tm_spb a, tr_customer b, tr_area c
                                  where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area
                                  and a.i_store is null and a.f_spb_consigment='t'
                                  and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' 
                                  or a.i_area='$area4' or a.i_area='$area5' )
                                  and (upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                                  or upper(b.e_customer_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationx->initialize($config);
			$this->load->model('realisasispbkons/mmaster');
			$data['ada']	= 'xx';
      $data['cari'] = $cari;
			$data['isi']=$this->mmaster->cari($area1,$area2,$area3,$area4,$area5,$cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('realisasispbkons');
			$data['iop']='';
	 		$this->load->view('realisasispbkons/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
/*
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu447')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spb');
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
			  )
			{
				$ispb 	= $this->uri->segment(4);
				$iarea	= $this->uri->segment(5);
				$query 	= $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem']	= $query->num_rows(); 				
				$data['ispb'] 		= $ispb;
				$data['departement']= $this->session->userdata('departement');
				$this->load->model('realisasispbkons/mmaster');
				$data['ada']	= '';
      	$data['cari'] = '';
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea);
		 		$this->load->view('realisasispbkons/vmainform',$data);
			}else{
				$this->load->view('realisasispbkons/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
*/
}
?>
