<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu90')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listpromo/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      $data['area']=$area1;
      if($area1!='00'){
			$query = $this->db->query(" select * from tm_promo where 
																	f_all_area='t'
																	and (upper(e_promo_name) like '%$cari%' or upper(i_promo) like '%$cari%')
																	union all
																	select a.* from tm_promo a
																	inner join tm_promo_area b on (a.i_promo=b.i_promo and a.i_promo_type=b.i_promo_type
																	and (b.i_area = '$area1' or b.i_area = '$area2' or b.i_area = '$area3' or b.i_area = '$area4' or b.i_area = '$area5')
																	and (upper(a.e_promo_name) like '%$cari%' or upper(a.i_promo) like '%$cari%'))
																	where a.f_all_area='f'
																	",false);
      }else{
			$query = $this->db->query(" select a.* from tm_promo a
																	where upper(a.e_promo_name) like '%$cari%' or upper(a.i_promo) like '%$cari%'
																",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listpromo');
			$this->load->model('listpromo/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$data['cari']='';

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Promo';
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listpromo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu90')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listpromo');
			$this->load->view('listpromo/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu90')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      $data['area']=$area1;
			$ipromo	= $this->uri->segment(4);
			$this->load->model('listpromo/mmaster');
			$this->mmaster->delete($ipromo);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus Promo No:'.$ipromo;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$data['page_title'] = $this->lang->line('listpromo');
			$config['base_url'] = base_url().'index.php/listpromo/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" 	select * from tm_promo
							                      where upper(e_promo_name) like '%$cari%' 
							                      or upper(i_promo) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listpromo');
			$this->load->model('listpromo/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$data['cari']=$cari;
			$this->load->view('listpromo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu90')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listpromo/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      $data['area']=$area1;
      if($area1!='00'){
			$query = $this->db->query(" select * from tm_promo where 
																	f_all_area='t'
																	and (upper(e_promo_name) like '%$cari%' or upper(i_promo) like '%$cari%')
																	union all
																	select a.* from tm_promo a
																	inner join tm_promo_area b on (a.i_promo=b.i_promo 
																	and (b.i_area = '$area1' or b.i_area = '$area2' or b.i_area = '$area3' or b.i_area = '$area4' or b.i_area = '$area5')
																	and (upper(a.e_promo_name) like '%$cari%' or upper(a.i_promo) like '%$cari%'))
																	where a.f_all_area='f'
																	",false);
      }else{
			$query = $this->db->query(" select a.* from tm_promo a
																	inner join tm_promo_area b on (a.i_promo=b.i_promo 
																	and (upper(a.e_promo_name) like '%$cari%' or upper(a.i_promo) like '%$cari%'))
																	",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listpromo');
			$this->load->model('listpromo/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$data['cari']=$cari;
			$this->load->view('listpromo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
