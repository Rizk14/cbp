<?php 

class Upload extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	
	function index()
	{	
		if ($this->session->userdata('logged_in')){
			$this->load->view('doupload/upload_form', array('error' => ' ' ));
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function do_upload()
	{
		if ($this->session->userdata('logged_in')){
			$config['upload_path'] = './data/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '0';
		  $config['overwrite']	= 'TRUE';

			$this->load->library('upload', $config);
	
			if ( ! $this->upload->do_upload())
			{
				$error = array('error' => $this->upload->display_errors());
				$this->load->view('doupload/upload_form', $error);
			}	
			else
			{
				$data = array('upload_data' => $this->upload->data());
				$this->load->view('doupload/upload_success', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}	
}
?>
