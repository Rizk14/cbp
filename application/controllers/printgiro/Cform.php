<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu199')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_giro');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('printgiro/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu199')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printgiro/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';

			$query = $this->db->query(" select * from tm_giro a 
			
										inner join tr_area on(a.i_area=tr_area.i_area)
										inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro)
										left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt)
										
										where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%') and 
										((tm_pelunasan.i_jenis_bayar!='02' and 
										tm_pelunasan.i_jenis_bayar!='03' and 
										tm_pelunasan.i_jenis_bayar!='04' and 
										tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and										
										a.i_area='$iarea' and
										a.d_giro >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_giro <= to_date('$dto','dd-mm-yyyy')",false);
			$num	= $query->num_rows();										
			$config['total_rows'] = $num; 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_giro');
			$this->load->model('printgiro/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['item']	= $num;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('printgiro/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu199')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$this->load->model('printgiro/mmaster');
			$query = $this->db->query(" select * from tm_giro a 
										inner join tr_area on(a.i_area=tr_area.i_area)
										inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro)
										left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt)
										where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' or upper(a.i_customer) like '%$cari%') and 
										((tm_pelunasan.i_jenis_bayar!='02' and 
										tm_pelunasan.i_jenis_bayar!='03' and 
										tm_pelunasan.i_jenis_bayar!='04' and 
										tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and										
										a.i_area='$iarea' and
										a.d_giro >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_giro <= to_date('$dto','dd-mm-yyyy')",false);
			$num	= $query->num_rows();
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
			$data['isi']	= $this->mmaster->cetakgiro($iarea,$dfrom,$dto,$cari);
			$data['item']	= $num;
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak Giro Area:'.$iarea.' Periode:'.$dfrom.' s/d No:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printgiro/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}	
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu199')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('printgiro');
			$this->load->view('printgiro/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu199')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printgiro/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';

			$query = $this->db->query(" select * from tm_giro a 
										inner join tr_area on(a.i_area=tr_area.i_area)
										inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										left join tm_pelunasan on(a.i_giro=tm_pelunasan.i_giro)
										left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt)
										
										where (upper(a.i_giro) like '%$cari%' or upper(a.i_rv) like '%$cari%' 
										or upper(a.i_customer) like '%$cari%') and 
										((tm_pelunasan.i_jenis_bayar!='02' and 
										tm_pelunasan.i_jenis_bayar!='03' and 
										tm_pelunasan.i_jenis_bayar!='04' and 
										tm_pelunasan.i_jenis_bayar='01') or ((tm_pelunasan.i_jenis_bayar='01') is null)) and										
										a.i_area='$iarea' and
										a.d_giro >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_giro <= to_date('$dto','dd-mm-yyyy')",false);
			$num	= $query->num_rows();
			$config['total_rows'] = $num;
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_giro');
			$this->load->model('printgiro/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
			$data['item']	= $num;
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('printgiro/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu199')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printgiro/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('printgiro/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printgiro/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu199')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printgiro/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printgiro/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printgiro/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
