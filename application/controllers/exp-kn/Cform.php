<?php
class Cform extends CI_Controller
{
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      require_once("php/fungsi.php");
   }

   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu305') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data['page_title'] = $this->lang->line('exp-kn');
         $data['datefrom'] = '';
         $data['dateto']   = '';
         $this->load->view('exp-kn/vmainform', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu305') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $this->load->model('exp-kn/mmaster');

         // $iarea      = $this->input->post('iarea');
         // $datefrom   = $this->input->post('datefrom');
         // $dateto     = $this->input->post('dateto');

         $iarea      = $this->uri->segment(4);
         // $eareaname  = $this->uri->segment(5);
         // $eareaname  = str_replace("%20", " ", $eareaname);
         $datefrom   = $this->uri->segment(5);
         $dateto     = $this->uri->segment(6);

         $datefromx  = $datefrom;
         $datetox    = $dateto;

         if ($iarea == 'NA') {
            $que        = $this->db->query("  SELECT
                                                DISTINCT ON
                                                (a.i_kn) a.*,
                                                a.e_remark AS ket,
                                                h.e_area_name,
                                                CASE WHEN c.f_customer_pkp='f' THEN 'Non PKP' ELSE 'PKP'END AS pkp ,
                                                CASE WHEN a.f_kn_cancel ='t' THEN 'Batal' ELSE 'Tidak Batal' END AS f_kn_cancel,
                                                CASE
                                                   WHEN NOT b.i_pelunasan ISNULL THEN b.i_pelunasan
                                                   WHEN NOT f.i_alokasi ISNULL THEN f.i_alokasi
                                                   WHEN NOT g.i_alokasi ISNULL THEN g.i_alokasi
                                                END AS i_pelunasan,
                                                CASE
                                                   WHEN NOT b.d_bukti ISNULL THEN b.d_bukti
                                                   WHEN NOT f.d_alokasi ISNULL THEN f.d_alokasi
                                                   WHEN NOT g.d_alokasi ISNULL THEN g.d_alokasi
                                                END AS d_bukti,
                                                c.e_customer_name,
                                                d.e_salesman_name,
                                                e.i_customer_groupbayar,
                                                CASE
                                                   WHEN NOT b.i_area ISNULL THEN b.i_area
                                                   WHEN NOT f.i_area ISNULL THEN f.i_area
                                                   WHEN NOT g.i_area ISNULL THEN g.i_area
                                                END AS areapelunasan
                                             FROM
                                                tm_kn a
                                             INNER JOIN tr_customer c ON
                                                (a.i_customer = c.i_customer
                                                AND a.i_area = c.i_area)
                                             LEFT JOIN tr_salesman d ON
                                                (a.i_salesman = d.i_salesman)
                                             INNER JOIN tr_customer_groupbayar e ON
                                                (a.i_customer = e.i_customer)
                                             INNER JOIN tr_area h ON
                                                (h.i_area = a.i_area)
                                             LEFT JOIN tm_pelunasan b ON
                                                (b.f_pelunasan_cancel = 'f'
                                                AND a.i_kn = b.i_giro
                                                AND b.i_customer IN(
                                                SELECT
                                                   i_customer
                                                FROM
                                                   tr_customer_groupbayar
                                                WHERE
                                                   i_customer_groupbayar = e.i_customer_groupbayar))
                                             LEFT JOIN tm_alokasikn f ON
                                                (f.f_alokasi_cancel = 'f'
                                                AND a.i_kn = f.i_kn
                                                AND f.i_customer IN(
                                                SELECT
                                                   i_customer
                                                FROM
                                                   tr_customer_groupbayar
                                                WHERE
                                                   i_customer_groupbayar = e.i_customer_groupbayar))
                                             LEFT JOIN tm_alokasiknr g ON
                                                (g.f_alokasi_cancel = 'f'
                                                AND a.i_kn = g.i_kn
                                                AND g.i_customer IN(
                                                SELECT
                                                   i_customer
                                                FROM
                                                   tr_customer_groupbayar
                                                WHERE
                                                   i_customer_groupbayar = e.i_customer_groupbayar))
                                             WHERE
                                                a.f_kn_cancel = 'f'
                                                AND a.d_kn >= to_date('$datefromx', 'dd-mm-yyyy')
                                                AND a.d_kn <= to_date('$datetox', 'dd-mm-yyyy')
                                             ORDER BY
                                                a.i_kn,
                                                a.i_area,
                                                a.d_kn");
         } else {
            $que        = $this->db->query("  SELECT
                                                DISTINCT ON
                                                (a.i_kn) a.*,
                                                a.e_remark AS ket,
                                                CASE WHEN c.f_customer_pkp='f' THEN 'Non PKP' ELSE 'PKP'END AS pkp ,
                                                CASE WHEN a.f_kn_cancel ='t' THEN 'Batal' ELSE 'Tidak Batal' END AS f_kn_cancel,
                                                h.e_area_name,
                                                CASE
                                                   WHEN NOT b.i_pelunasan ISNULL THEN b.i_pelunasan
                                                   WHEN NOT f.i_alokasi ISNULL THEN f.i_alokasi
                                                   WHEN NOT g.i_alokasi ISNULL THEN g.i_alokasi
                                                END AS i_pelunasan,
                                                CASE
                                                   WHEN NOT b.d_bukti ISNULL THEN b.d_bukti
                                                   WHEN NOT f.d_alokasi ISNULL THEN f.d_alokasi
                                                   WHEN NOT g.d_alokasi ISNULL THEN g.d_alokasi
                                                END AS d_bukti,
                                                c.e_customer_name,
                                                d.e_salesman_name,
                                                e.i_customer_groupbayar,
                                                CASE
                                                   WHEN NOT b.i_area ISNULL THEN b.i_area
                                                   WHEN NOT f.i_area ISNULL THEN f.i_area
                                                   WHEN NOT g.i_area ISNULL THEN g.i_area
                                                END AS areapelunasan
                                             FROM
                                                tm_kn a
                                             INNER JOIN tr_customer c ON
                                                (a.i_customer = c.i_customer
                                                AND a.i_area = c.i_area)
                                             LEFT JOIN tr_salesman d ON
                                                (a.i_salesman = d.i_salesman)
                                             INNER JOIN tr_customer_groupbayar e ON
                                                (a.i_customer = e.i_customer)
                                             INNER JOIN tr_area h ON
                                                (h.i_area = a.i_area)
                                             LEFT JOIN tm_pelunasan b ON
                                                (b.f_pelunasan_cancel = 'f'
                                                AND a.i_kn = b.i_giro
                                                AND b.i_customer IN(
                                                SELECT
                                                   i_customer
                                                FROM
                                                   tr_customer_groupbayar
                                                WHERE
                                                   i_customer_groupbayar = e.i_customer_groupbayar))
                                             LEFT JOIN tm_alokasikn f ON
                                                (f.f_alokasi_cancel = 'f'
                                                AND a.i_kn = f.i_kn
                                                AND f.i_customer IN(
                                                SELECT
                                                   i_customer
                                                FROM
                                                   tr_customer_groupbayar
                                                WHERE
                                                   i_customer_groupbayar = e.i_customer_groupbayar))
                                             LEFT JOIN tm_alokasiknr g ON
                                                (g.f_alokasi_cancel = 'f'
                                                AND a.i_kn = g.i_kn
                                                AND g.i_customer IN(
                                                SELECT
                                                   i_customer
                                                FROM
                                                   tr_customer_groupbayar
                                                WHERE
                                                   i_customer_groupbayar = e.i_customer_groupbayar))
                                             WHERE
                                                a.i_area = '$iarea'
                                                AND a.f_kn_cancel = 'f'
                                                AND a.d_kn >= to_date('$datefromx', 'dd-mm-yyyy')
                                                AND a.d_kn <= to_date('$datetox', 'dd-mm-yyyy')
                                             ORDER BY
                                                a.i_kn,
                                                a.i_area,
                                                a.d_kn");
         }
         /*
         $que        = $this->db->query(" select a.*, b.i_pelunasan, b.d_bukti, c.e_customer_name, d.e_salesman_name, 
                                          e.i_customer_groupbayar, b.i_area as areapelunasan
                                          from tm_kn a
                                          inner join tr_customer c on (a.i_customer=c.i_customer and a.i_area=c.i_area)
                                          inner join tr_salesman d on (a.i_salesman=d.i_salesman)
                                          inner join tr_customer_groupbayar e on (a.i_customer=e.i_customer)
                                          left join tm_pelunasan b on (b.f_pelunasan_cancel='f' and a.i_kn=b.i_giro and b.i_customer in(
                                          select i_customer from tr_customer_groupbayar 
                                          where i_customer_groupbayar=e.i_customer_groupbayar))
                                          where a.i_area='$iarea' and a.f_kn_cancel='f' and 
                                          a.d_kn >= to_date('$datefromx','dd-mm-yyyy') and
                                          a.d_kn <= to_date('$datetox','dd-mm-yyyy')
                                          order by a.i_kn, a.d_kn");
*/
         $fromname = '';

         if ($datefrom != '') {
            $tmp = explode("-", $datefrom);
            $th = $tmp[2];
            $bl = $tmp[1];
            $hr = $tmp[0];
            $fromname = $fromname . $hr;
            $datefrom = $th . "-" . $bl . "-" . $hr;
            $periodeawal   = $hr . " " . mbulan($bl) . " " . $th;
         }

         $tmp           = explode("-", $dateto);
         $det           = $tmp[0];
         $mon           = $tmp[1];
         $yir           = $tmp[2];
         $dtos          = $yir . "/" . $mon . "/" . $det;
         $fromname      = $fromname . $det;
         $periodeakhir  = $det . " " . mbulan($mon) . " " . $yir;
         $dtos          = $this->mmaster->dateAdd("d", 1, $dtos);
         $tmp           = explode("-", $dtos);
         $det1          = $tmp[2];
         $mon1          = $tmp[1];
         $yir1          = $tmp[0];
         $dtos          = $yir1 . "-" . $mon1 . "-" . $det1;
         $data['page_title'] = $this->lang->line('exp-kn');

         $qareaname   = $this->mmaster->eareaname($iarea);
         if ($qareaname->num_rows() > 0) {
            $row_areaname   = $qareaname->row();
            $aname   = $row_areaname->e_area_name;
         } else if ($iarea == "NA") {
            $aname   = 'NASIONAL';
         } else {
            $aname   = '';
         }

         $this->load->library('PHPExcel');
         $this->load->library('PHPExcel/IOFactory');
         $objPHPExcel = new PHPExcel();
         $objPHPExcel->getProperties()->setTitle("Daftar Kredit Nota/Debet Nota")
            ->setDescription(NmPerusahaan);
         $objPHPExcel->setActiveSheetIndex(0);
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            array(
               'font'   => array(
                  'name'   => 'Arial',
                  'bold'   => true,
                  'italic' => false,
                  'size'  => 12
               ),
               'alignment' => array(
                  'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
                  'vertical'  => Style_Alignment::VERTICAL_CENTER,
                  'wrap'      => true
               )
            ),
            'A1:A4'
         );
         $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
         $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
         $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
         $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(17);
         $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
         $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
         $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
         $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
         $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);

         $objPHPExcel->getActiveSheet()->setCellValue('A1', NmPerusahaan);
         $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 1, 17, 1);
         $objPHPExcel->getActiveSheet()->setCellValue('A2', 'DAFTAR KN/DN - ' . $aname);
         $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 17, 2);
         $objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : ' . $periodeawal . ' - ' . $periodeakhir);
         $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 17, 3);
         $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
         $objPHPExcel->getActiveSheet()->getStyle('A5:A5')->applyFromArray(
            array(
               'borders'   => array(
                  'top'       => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Area');
         $objPHPExcel->getActiveSheet()->getStyle('B5:B5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('C5', 'No KN/DN');
         $objPHPExcel->getActiveSheet()->getStyle('C5:C5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Tgl KN/DN');
         $objPHPExcel->getActiveSheet()->getStyle('D5:D5')->applyFromArray(

            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Jenis KN');
         $objPHPExcel->getActiveSheet()->getStyle('E5:E5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Status KN');
         $objPHPExcel->getActiveSheet()->getStyle('F5:F5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('G5', 'No Referensi');
         $objPHPExcel->getActiveSheet()->getStyle('G5:G5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Kode Lang');
         $objPHPExcel->getActiveSheet()->getStyle('H5:H5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Nama Lang');
         $objPHPExcel->getActiveSheet()->getStyle('I5:I5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Salesman');
         $objPHPExcel->getActiveSheet()->getStyle('J5:J5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Nilai Bersih');
         $objPHPExcel->getActiveSheet()->getStyle('K5:K5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Discount');
         $objPHPExcel->getActiveSheet()->getStyle('L5:L5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('M5', 'Nilai Kotor');
         $objPHPExcel->getActiveSheet()->getStyle('M5:M5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('N5', 'Sisa');
         $objPHPExcel->getActiveSheet()->getStyle('N5:N5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('O5', 'No Pelunasan');
         $objPHPExcel->getActiveSheet()->getStyle('O5:O5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Tgl Pelunasan');
         $objPHPExcel->getActiveSheet()->getStyle('P5:P5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Area Pelunasan');
         $objPHPExcel->getActiveSheet()->getStyle('Q5:Q5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('R5', 'Alasan');
         $objPHPExcel->getActiveSheet()->getStyle('R5:R5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('S5', 'Status Pelanggan');
         $objPHPExcel->getActiveSheet()->getStyle('S5:S5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            array(
               'font' => array(

                  'name'   => 'Arial',
                  'bold'  => false,
                  'italic' => false,
                  'size'  => 12
               ),
               'alignment' => array(
                  'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
                  'vertical'  => Style_Alignment::VERTICAL_CENTER,
                  'wrap'      => true
               )
            ),
            'A5:S5'
         );

         if ($iarea == 'NA') {
            $query = $this->db->query("  SELECT
                                          DISTINCT ON
                                          (a.i_kn) a.*,
                                          a.e_remark AS ket,
                                          CASE WHEN c.f_customer_pkp='f' THEN 'Non PKP' ELSE 'PKP'END AS pkp ,
                                          CASE WHEN a.f_kn_cancel ='t' THEN 'Batal' ELSE 'Tidak Batal' END AS f_kn_cancel,
                                          h.e_area_name,
                                          CASE
                                             WHEN NOT b.i_pelunasan ISNULL THEN b.i_pelunasan
                                             WHEN NOT f.i_alokasi ISNULL THEN f.i_alokasi
                                             WHEN NOT g.i_alokasi ISNULL THEN g.i_alokasi
                                          END AS i_pelunasan,
                                          CASE
                                             WHEN NOT b.d_bukti ISNULL THEN b.d_bukti
                                             WHEN NOT f.d_alokasi ISNULL THEN f.d_alokasi
                                             WHEN NOT g.d_alokasi ISNULL THEN g.d_alokasi
                                          END AS d_bukti,
                                          c.e_customer_name,
                                          d.e_salesman_name,
                                          e.i_customer_groupbayar,
                                          CASE
                                             WHEN NOT b.i_area ISNULL THEN b.i_area
                                             WHEN NOT f.i_area ISNULL THEN f.i_area
                                             WHEN NOT g.i_area ISNULL THEN g.i_area
                                          END AS areapelunasan
                                       FROM
                                          tm_kn a
                                       INNER JOIN tr_customer c ON
                                          (a.i_customer = c.i_customer
                                          AND a.i_area = c.i_area)
                                       LEFT JOIN tr_salesman d ON
                                          (a.i_salesman = d.i_salesman)
                                       INNER JOIN tr_customer_groupbayar e ON
                                          (a.i_customer = e.i_customer)
                                       INNER JOIN tr_area h ON
                                          (h.i_area = a.i_area)
                                       LEFT JOIN tm_pelunasan b ON
                                          (b.f_pelunasan_cancel = 'f'
                                          AND a.i_kn = b.i_giro
                                          AND b.i_customer IN(
                                          SELECT
                                             i_customer
                                          FROM
                                             tr_customer_groupbayar
                                          WHERE
                                             i_customer_groupbayar = e.i_customer_groupbayar))
                                       LEFT JOIN tm_alokasikn f ON
                                          (f.f_alokasi_cancel = 'f'
                                          AND a.i_kn = f.i_kn
                                          AND f.i_customer IN(
                                          SELECT
                                             i_customer
                                          FROM
                                             tr_customer_groupbayar
                                          WHERE
                                             i_customer_groupbayar = e.i_customer_groupbayar))
                                       LEFT JOIN tm_alokasiknr g ON
                                          (g.f_alokasi_cancel = 'f'
                                          AND a.i_kn = g.i_kn
                                          AND g.i_customer IN(
                                          SELECT
                                             i_customer
                                          FROM
                                             tr_customer_groupbayar
                                          WHERE
                                             i_customer_groupbayar = e.i_customer_groupbayar))
                                       WHERE
                                          a.f_kn_cancel = 'f'
                                          AND a.d_kn >= to_date('$datefromx', 'dd-mm-yyyy')
                                          AND a.d_kn <= to_date('$datetox', 'dd-mm-yyyy')
                                       ORDER BY
                                          a.i_kn,
                                          a.i_area,
                                          a.d_kn");
         } else {
            $query = $this->db->query("  SELECT
                                          DISTINCT ON
                                          (a.i_kn) a.*,
                                          a.e_remark AS ket,
                                          CASE WHEN c.f_customer_pkp='f' THEN 'Non PKP' ELSE 'PKP'END AS pkp ,
                                          CASE WHEN a.f_kn_cancel ='t' THEN 'Batal' ELSE 'Tidak Batal' END AS f_kn_cancel,
                                          h.e_area_name,
                                          CASE
                                             WHEN NOT b.i_pelunasan ISNULL THEN b.i_pelunasan
                                             WHEN NOT f.i_alokasi ISNULL THEN f.i_alokasi
                                             WHEN NOT g.i_alokasi ISNULL THEN g.i_alokasi
                                          END AS i_pelunasan,
                                          CASE
                                             WHEN NOT b.d_bukti ISNULL THEN b.d_bukti
                                             WHEN NOT f.d_alokasi ISNULL THEN f.d_alokasi
                                             WHEN NOT g.d_alokasi ISNULL THEN g.d_alokasi
                                          END AS d_bukti,
                                          c.e_customer_name,
                                          d.e_salesman_name,
                                          e.i_customer_groupbayar,
                                          CASE
                                             WHEN NOT b.i_area ISNULL THEN b.i_area
                                             WHEN NOT f.i_area ISNULL THEN f.i_area
                                             WHEN NOT g.i_area ISNULL THEN g.i_area
                                          END AS areapelunasan
                                       FROM
                                          tm_kn a
                                       INNER JOIN tr_customer c ON
                                          (a.i_customer = c.i_customer
                                          AND a.i_area = c.i_area)
                                       LEFT JOIN tr_salesman d ON
                                          (a.i_salesman = d.i_salesman)
                                       INNER JOIN tr_customer_groupbayar e ON
                                          (a.i_customer = e.i_customer)
                                       INNER JOIN tr_area h ON
                                          (h.i_area = a.i_area)
                                       LEFT JOIN tm_pelunasan b ON
                                          (b.f_pelunasan_cancel = 'f'
                                          AND a.i_kn = b.i_giro
                                          AND b.i_customer IN(
                                          SELECT
                                             i_customer
                                          FROM
                                             tr_customer_groupbayar
                                          WHERE
                                             i_customer_groupbayar = e.i_customer_groupbayar))
                                       LEFT JOIN tm_alokasikn f ON
                                          (f.f_alokasi_cancel = 'f'
                                          AND a.i_kn = f.i_kn
                                          AND f.i_customer IN(
                                          SELECT
                                             i_customer
                                          FROM
                                             tr_customer_groupbayar
                                          WHERE
                                             i_customer_groupbayar = e.i_customer_groupbayar))
                                       LEFT JOIN tm_alokasiknr g ON
                                          (g.f_alokasi_cancel = 'f'
                                          AND a.i_kn = g.i_kn
                                          AND g.i_customer IN(
                                          SELECT
                                             i_customer
                                          FROM
                                             tr_customer_groupbayar
                                          WHERE
                                             i_customer_groupbayar = e.i_customer_groupbayar))
                                       WHERE
                                          a.i_area = '$iarea'
                                          AND a.f_kn_cancel = 'f'
                                          AND a.d_kn >= to_date('$datefromx', 'dd-mm-yyyy')
                                          AND a.d_kn <= to_date('$datetox', 'dd-mm-yyyy')
                                       ORDER BY
                                          a.i_kn,
                                          a.i_area,
                                          a.d_kn");
         }
         /*
            $this->db->select(" a.*, b.i_pelunasan, b.d_bukti, c.e_customer_name, d.e_salesman_name, e.i_customer_groupbayar,
                                b.i_area as areapelunasan
                                from tm_kn a
                                inner join tr_customer c on (a.i_customer=c.i_customer and a.i_area=c.i_area)
                                inner join tr_salesman d on (a.i_salesman=d.i_salesman)
                                inner join tr_customer_groupbayar e on (a.i_customer=e.i_customer)
                                left join tm_pelunasan b on (b.f_pelunasan_cancel='f' and a.i_kn=b.i_giro and b.i_customer in(
                                select i_customer from tr_customer_groupbayar 
                                where i_customer_groupbayar=e.i_customer_groupbayar))
                                where a.i_area='$iarea' and a.f_kn_cancel='f' and 
                                a.d_kn >= to_date('$datefromx','dd-mm-yyyy') and
                                a.d_kn <= to_date('$datetox','dd-mm-yyyy')
                                order by a.i_kn, a.d_kn",false);
*/
         #            $query = $this->db->get();
         if ($query->num_rows() > 0) {
            $i = 5;
            $j = 5;
            $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'   => array('style' => Style_Border::BORDER_THIN),
                     'right'  => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'   => array('style' => Style_Border::BORDER_THIN),
                     'right'  => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'   => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('S5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'   => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );

            foreach ($query->result() as $row) {

               if ($row->i_kn_type == '01') {
                  $kn = 'Retur';
               } elseif ($row->i_kn_type == '02') {
                  $kn = 'Non Retur';
               } elseif ($row->i_kn_type == '03') {
                  $kn = 'Debet';
               }


               $i++;
               $j++;

               $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $j - 5);
               $objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     ),
                     'alignment' => array(
                        'horizontal' => Style_Alignment::HORIZONTAL_RIGHT
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->i_area . "-" . $row->e_area_name);
               $objPHPExcel->getActiveSheet()->getStyle('B' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'   => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->i_kn);
               $objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               if ($row->d_kn) {
                  $tmp = explode('-', $row->d_kn);
                  $tgl = $tmp[2];
                  $bln = $tmp[1];
                  $thn = $tmp[0];
                  $row->d_kn = $tgl . '-' . $bln . '-' . $thn;
               }
               $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->d_kn);
               $objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               if (isset($kn)) {
                  $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $kn);
               }
               $objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $row->f_kn_cancel);
               $objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row->i_refference);
               $objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->i_customer);
               $objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->e_customer_name);
               $objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),

                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->e_salesman_name);
               $objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $row->v_netto);
               $objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row->v_discount);
               $objPHPExcel->getActiveSheet()->getStyle('L' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $row->v_gross);
               $objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $row->v_sisa);
               $objPHPExcel->getActiveSheet()->getStyle('N' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $row->i_pelunasan);
               $objPHPExcel->getActiveSheet()->getStyle('O' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               if ($row->d_bukti) {
                  $tmp = explode('-', $row->d_bukti);
                  $tgl = $tmp[2];
                  $bln = $tmp[1];
                  $thn = $tmp[0];
                  $row->d_bukti = $tgl . '-' . $bln . '-' . $thn;
               }
               $objPHPExcel->getActiveSheet()->setCellValue('P' . $i, $row->d_bukti);
               $objPHPExcel->getActiveSheet()->getStyle('P' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               if ($row->areapelunasan != '') $row->areapelunasan = "'" . $row->areapelunasan;
               $objPHPExcel->getActiveSheet()->setCellValue('Q' . $i, $row->areapelunasan);
               $objPHPExcel->getActiveSheet()->getStyle('Q' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('R' . $i, $row->ket);
               $objPHPExcel->getActiveSheet()->getStyle('R' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('S' . $i, $row->pkp);
               $objPHPExcel->getActiveSheet()->getStyle('S' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );

               // $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->$j-5, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->i_area . "-" . $row->e_area_name, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->i_kn, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->d_kn, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $kn, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->f_kn_cancel, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->i_refference, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->i_customer, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $row->e_salesman_name, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . $i, $row->v_netto, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('L' . $i, $row->v_discount, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('M' . $i, $row->v_gross, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('N' . $i, $row->v_sisa, Cell_DataType::TYPE_NUMERIC);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('O' . $i, $row->i_pelunasan, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('P' . $i, $row->d_bukti, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q' . $i, $row->areapelunasan, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('R' . $i, $row->ket, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('S' . $i, $row->pkp, Cell_DataType::TYPE_STRING);
            }
            $x = $i - 1;
            $objPHPExcel->getActiveSheet()->getStyle('J6:N' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
            //$objPHPExcel->getActiveSheet()->getStyle('H6:I'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::TYPE_NUMERIC);


         }
         $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

         $nama = 'KNDN-' . $iarea . '-' . substr($datefrom, 5, 2) . '-' . substr($datefrom, 0, 4) . '.xls';
         // if ($iarea == 'NA') $iarea = '00';
         // $area = $iarea;

         // Proses file excel    
         header('Content-Type: application/vnd.ms-excel');
         header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
         header('Cache-Control: max-age=0');

         $objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
         $objWriter->save('php://output', 'w');

         /* if($iarea=='22')
               $area='09';
            elseif($iarea=='24') 
               $area='05';
            elseif(($iarea=='20')||($iarea=='26'))
               $area='07';
            elseif($iarea=='06')
               $area='23';
            elseif(($iarea=='14')||($iarea=='15'))
               $area='12';
            elseif($iarea=='21')
               $area='13';
            elseif(($iarea=='30')||($iarea=='32')||($iarea=='18')||($iarea=='19')||($iarea=='29')||($iarea=='28')||($iarea=='27'))
               $area='11'; */
         /*  if (file_exists('excel/' . $area . '/' . $nama)) {
            @chmod('excel/' . $area . '/' . $nama, 0777);
            @unlink('excel/' . $area . '/' . $nama);
         }
         $objWriter->save('excel/' . $area . '/' . $nama);
         @chmod('excel/' . $area . '/' . $nama, 0777); */

         /* $sess = $this->session->userdata('session_id');
         $id = $this->session->userdata('user_id');
         $sql   = "select * from dgu_session where session_id='$sess' and not user_data isnull";
         $rs      = pg_query($sql);
         if (pg_num_rows($rs) > 0) {
            while ($row = pg_fetch_assoc($rs)) {
               $ip_address     = $row['ip_address'];
               break;
            }
         } else {
            $ip_address = 'kosong';
         }
         $query    = pg_query("SELECT current_timestamp as c");
         while ($row = pg_fetch_assoc($query)) {
            $now     = $row['c'];
         }
         $pesan = 'Export KN/DN sampai dengan:' . $dateto . ' Area:' . $iarea;
         $this->load->model('logger');
         $this->logger->write($id, $ip_address, $now, $pesan); */

         $this->logger->writenew('Export KN/DN sampai dengan:' . $dateto . ' Area:' . $iarea);

         /*  $data['sukses']   = true;
         $data['inomor']   = $nama;
         $data['folder']   = "excel/" . $area;
         $this->load->view('nomor', $data); */
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu305') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/exp-kn/cform/area/index/';
         $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('exp-kn/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
         $this->load->view('exp-kn/vlistarea', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu305') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $iuser = $this->session->userdata('user_id');
         $config['base_url'] = base_url() . 'index.php/exp-kn/cform/area/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari   = strtoupper($cari);
         $query      = $this->db->query("select * from tr_area
                                    where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                    and i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('exp-kn/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
         $this->load->view('exp-kn/vlistarea', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
}
