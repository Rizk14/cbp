<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu99')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listtppersalesman');
			$data['iperiode']	= '';
			$this->load->view('listtppersalesman/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu99')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
//			$area		= $this->input->post('iarea');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listtppersalesman/cform/view/'.$iperiode.'/';
			$query = $this->db->query(" select a.i_area 
                                  from tm_target_itemsls a, tr_salesman b, tr_area c
										              where a.i_periode = '$iperiode' and a.i_area=c.i_area
										              and a.i_salesman=b.i_salesman and (upper(a.i_salesman) like '%$cari%' or upper(b.e_salesman_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '1000';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('listtppersalesman/mmaster');
			$data['page_title'] = $this->lang->line('listtppersalesman');
			$data['cari']		= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']		= $this->mmaster->baca($iperiode,$config['per_page'],$this->uri->segment(5),$cari);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Target penjualan per salesman Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listtppersalesman/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu99')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
//			$area		= $this->input->post('iarea');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listtppersalesman/cform/view/'.$iperiode.'/';
			$query = $this->db->query(" select a.*, b.e_salesman_name from tm_target_itemsls a, tr_salesman b
										where a.i_periode = '$iperiode'
										and a.i_salesman=b.i_salesman
										and(upper(a.i_salesman) like '%$cari%' or upper(b.e_salesman_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '1000';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('listtppersalesman/mmaster');
			$data['page_title'] = $this->lang->line('listtppersalesman');
			$data['cari']		= $cari;
			$data['iperiode']	= $iperiode;
//			$data['iarea']		= $area;
//			$data['isi']		= $this->mmaster->cari($area,$iperiode,$config['per_page'],$this->uri->segment(7),$cari);
			$data['isi']		= $this->mmaster->cari($iperiode,$config['per_page'],$this->uri->segment(7),$cari);
			$this->load->view('listtppersalesman/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
