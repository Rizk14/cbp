<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu421')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printtp');
			$data['iperiode']	= '';
			$this->load->view('printtp/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu421')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('printtp/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			$config['base_url'] = base_url().'index.php/printtp/cform/view/'.$iperiode.'/';
#      if($iperiode!=''){
#        $this->db->query("update tm_dgu_no set e_periode='$iperiode' where i_modul='VPJ'", false);
#      }
			$data['page_title'] = $this->lang->line('printtp');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
#      $this->mmaster->simpan($iperiode);

	    $sess=$this->session->userdata('session_id');
	    $id=$this->session->userdata('user_id');
	    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	    $rs		= pg_query($sql);
	    if(pg_num_rows($rs)>0){
		    while($row=pg_fetch_assoc($rs)){
			    $ip_address	  = $row['ip_address'];
			    break;
		    }
	    }else{
		    $ip_address='kosong';
	    }
	    $query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
		    $now	  = $row['c'];
	    }
	    $pesan='Lihat Target Penjualan per Salesman Periode:'.$iperiode;
	    $this->load->model('logger');
	    $this->logger->write($id, $ip_address, $now , $pesan );

			$data['isi']		= $this->mmaster->baca($iperiode);
			$this->load->view('printtp/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function persales()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu421')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('printtp/mmaster');
      $iperiode = $this->uri->segment(4);
#      $iarea    = $this->uri->segment(5);
			$data['page_title'] = $this->lang->line('printtp');
			$data['iperiode']	= $iperiode;
#			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacapersales($iperiode);#,$iarea);
			$this->load->view('printtp/vformviewpersales',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function pernota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu421')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('printtp/mmaster');
      $iperiode = $this->uri->segment(4);
      $iarea    = $this->uri->segment(5);
			$data['page_title'] = $this->lang->line('printtp');
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacapernota($iperiode,$iarea);
			$this->load->view('printtp/vformviewpernota',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function perkota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu421')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('printtp/mmaster');
      $iperiode = $this->uri->segment(4);
      $iarea    = $this->uri->segment(5);
			$data['page_title'] = $this->lang->line('printtp');
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperkota($iperiode,$iarea);
			$this->load->view('printtp/vformviewperkota',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function retur()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu421')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('printtp/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			$config['base_url'] = base_url().'index.php/printtp/cform/retur/'.$iperiode.'/';
			$data['page_title'] = $this->lang->line('printtp');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']		= $this->mmaster->bacaretur($iperiode);
			$this->load->view('printtp/vformviewretur',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function retursales()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu421')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('printtp/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			$config['base_url'] = base_url().'index.php/printtp/cform/retursales/'.$iperiode.'/';
			$data['page_title'] = $this->lang->line('printtp');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']		= $this->mmaster->bacaretursales($iperiode);
			$this->load->view('printtp/vformviewretursales',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
  function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu421')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('printtp/mmaster');
			$iperiode	= $this->input->post('iperiode');
      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			if($iarea=='') $iarea=$this->uri->segment(5);
      if(strlen($iarea)==1) $iarea='0'.$iarea;
      $query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
      $st=$query->row();
      $store=$st->i_store;
			$config['base_url'] = base_url().'index.php/printtp/cform/view/'.$iperiode.'/'.$store.'/';
			$query = $this->db->query(" select i_product from tm_mutasi where e_mutasi_periode = '$iperiode' and i_store='$store'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('printtp');
      $cari='';
      $data['cari']=$cari;
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['istore']	= $store;
			$data['isi']		= $this->mmaster->baca($iperiode,$store,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('printtp/vmainform',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu421')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$dto	= $this->input->post('dto');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/printtp/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_target a, tr_area b
										where a.i_periode = '$iperiode'
										and a.i_area=b.i_area
										and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('printtp/mmaster');
			$data['page_title'] = $this->lang->line('printtp');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('printtp/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu421')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printtp/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    group by a.i_store, b.e_store_name",false);
			}else{
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                    or a.i_area = '$area4' or a.i_area = '$area5')
                                    group by a.i_store, b.e_store_name",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('printtp/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('printtp/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu421')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/printtp/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store and (upper(a.i_area) like '%$cari%' or upper(a.e_area_name) like '%$cari%')
                                    group by a.i_store, b.e_store_name",false);
			}else{
				$query = $this->db->query("select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                    or a.i_area = '$area4' or a.i_area = '$area5')
                                    and (upper(a.i_area) like '%$cari%' or upper(a.e_area_name) like '%$cari%')
                                    group by a.i_store, b.e_store_name
                                    order by a.i_store, b.e_store_name",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printtp/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('printtp/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu421')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $iperiode = $this->uri->segment(4);
      $iarea = $this->uri->segment(5);
			$this->load->model('printtp/mmaster');     
			$data['page_title'] = $this->lang->line('printtp');
			$data['iperiode']	  = $iperiode;
			$data['iarea']	    = $iarea;
			$data['detail']	    = $this->mmaster->detail($iarea,$iperiode);
			$this->load->view('printtp/vmainform',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
