<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu253')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('debetnota');
			$cari    = strtoupper($this->input->post('cari'));
			if($cari=='') $cari=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/debetnota/cform/view/'.$cari.'/index/';
			$sm=PiutangDagang;

			$query = $this->db->query("select a.i_kbank,a.i_giro, a.i_area, a.d_bank, a.v_bank, b.e_area_name, c.e_bank_name, a.v_sisa, a.i_coa_bank, a.e_description
			from tm_kbank a, tr_area b, tr_bank c
			where a.i_area=b.i_area and a.f_kbank_cancel='false' and a.f_debet=false and a.v_sisa>0 and a.v_sisa <= 10000
			and a.i_coa_bank=c.i_coa and (upper(a.i_kbank) like '%$cari%') and a.i_coa='$sm'
			order by a.i_kbank, a.d_bank",false);

			$config['total_rows']   = $query->num_rows();
			$config['per_page']  = '10';
			$config['first_link']   = 'Awal';
			$config['last_link']    = 'Akhir';
			$config['next_link']    = 'Selanjutnya';
			$config['prev_link']    = 'Sebelumnya';
			$config['cur_page']  = $this->uri->segment(6);
			$this->paginationxx->initialize($config);
			$this->load->model('debetnota/mmaster');
			$data['cari']  = $cari;
			$data['isi']   = $this->mmaster->bacasemua($config['per_page'],$this->uri->segment(6),$cari);
			
			$query=$this->db->query("	select i_periode from tm_periode ",false);
			if ($query->num_rows() > 0){
			foreach($query->result() as $row){
				$data['periode']=$row->i_periode;
			}
			}
			$this->load->view('debetnota/vform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpen()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu253')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$jml = $this->input->post('jml', TRUE);
			$dkn = $this->input->post('dkn', TRUE);
			$this->load->model('debetnota/mmaster');
			$this->db->trans_begin();
				for ($i=0; $i < $jml ; $i++) { 
					$chk			= $this->input->post('chk'.$i, TRUE);
					$i_kbank		= $this->input->post('i_kbank'.$i, TRUE);
					$iarea			= $this->input->post('i_area'.$i, TRUE);
					$i_coa_bank		= $this->input->post('i_coa_bank'.$i, TRUE);
					$vsisa			= $this->input->post('v_sisa'.$i, TRUE);
					$d_bank			= $this->input->post('d_bank'.$i, TRUE);
					$vnetto 		= $vsisa;
					$vgross 		= $vsisa;
					$vdiscount 		= 0;
					$eremark 		= '';
					$drefference 	= $d_bank;
					
					if($chk == 'on'){
						$data_alokasi  =$this->mmaster->data_alokasi($i_kbank, $iarea, $i_coa_bank);
						if($data_alokasi->num_rows() > 0){
							$year 				= date('Y', strtotime($dkn));
							$ikn 				= $this->mmaster->runningnumberkn($year,$iarea);
							$nomor[]			= $ikn;
							$data_alokasi 		= $data_alokasi->row();
							$icustomer 			= $data_alokasi->i_customer;
							$icustomergroupar 	= $data_alokasi->i_customer_groupar;
							$irefference 		= $i_kbank;
							$isalesman 			= '';
							$ikntype			= '03';
							$dkn 				= date('Y-m-d', strtotime($dkn));
							$fcetak				= 'f';
							$fmasalah			= 'f';
							$finsentif			= 'f';
							$ipajak 			= '';
							$dpajak 			= null;
							
							$this->mmaster->insert($iarea,$ikn,$icustomer,$irefference,$icustomergroupar,$isalesman,$ikntype,$dkn,$year,$fcetak,$fmasalah,
													$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference,$ipajak,$dpajak);
						
							$this->mmaster->updatebank($i_kbank,$i_coa_bank,$vsisa);
						}
					}
				}
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

					$sess	= $this->session->userdata('session_id');
					$id		= $this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input Debet Nota No:'.$ikn.' Area:'.$iarea;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomorarray',$data);
				}	
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu253')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea 			= $this->input->post('iarea', TRUE);
			$ikn 			= $this->input->post('idn', TRUE);
			$icustomer		= $this->input->post('icustomer', TRUE);
			$irefference	= $this->input->post('irefference', TRUE);
			$icustomergroupar= $this->input->post('icustomergroupar', TRUE);
			$isalesman		= $this->input->post('isalesman', TRUE);
			$ikntype		= '03';
			$drefference	= $this->input->post('drefference', TRUE);
			if($drefference!=''){
				$tmp=explode("-",$drefference);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$drefference=$th."-".$bl."-".$hr;
			}
			$ipajak 		= $this->input->post('ipajak', TRUE);
			$dpajak			= $this->input->post('dpajak', TRUE);
			if($dpajak!=''){
				$tmp=explode("-",$dpajak);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpajak=$th."-".$bl."-".$hr;
			}
			$dkn			= $this->input->post('ddn', TRUE);
			if($dkn!=''){
				$tmp=explode("-",$dkn);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkn=$th."-".$bl."-".$hr;
				$nknyear=$th;
			}
			$fcetak		= 'f';
			$fmasalah	= $this->input->post('fmasalah', TRUE);
			if($fmasalah=='')
				$fmasalah='f';
			else
				$fmasalah='t';
			$finsentif	= $this->input->post('finsentif', TRUE);
			if($finsentif=='')
				$finsentif='f';
			else
				$finsentif='t';
			$vnetto		= $this->input->post('vnetto', TRUE);
			$vnetto		= str_replace(',','',$vnetto);
			$vsisa		= $vnetto;
			$vgross		= $this->input->post('vgross', TRUE);
			$vgross		= str_replace(',','',$vgross);
			$vdiscount	= $this->input->post('vdiscount', TRUE);
			$vdiscount	= str_replace(',','',$vdiscount);
			$eremark	= $this->input->post('eremark', TRUE);
			if (
				(isset($irefference) && $irefference != '') && 
				(isset($iarea) && $iarea != '') &&
				(isset($icustomer) && $icustomer != '')
			   )
			{
				$this->load->model('debetnota/mmaster');
				$ikn = $this->mmaster->runningnumberkn($nknyear,$iarea);
				$this->db->trans_begin();
				$this->mmaster->insert(	$iarea,$ikn,$icustomer,$irefference,$icustomergroupar,$isalesman,$ikntype,$dkn,$nknyear,$fcetak,$fmasalah,
										$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference,$ipajak,$dpajak);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input Debet Nota No:'.$ikn.' Area:'.$iarea;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ikn;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	
}
?>
