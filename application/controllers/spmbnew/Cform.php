<?php
class Cform extends CI_Controller
{
	public $menu 	= "20060011";
	public $title 	= "SPmB";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$ispmb 		= $this->input->post('ispmb', TRUE);
			$ispmbold 	= $this->input->post('ispmbold', TRUE);
			$dspmb 		= $this->input->post('dspmb', TRUE);
			if ($dspmb != '') {
				$tmp = explode("-", $dspmb);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dspmb = $th . "-" . $bl . "-" . $hr;
				$thbl = substr($th, 2, 2) . $bl;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$eareaname  = $this->input->post('eareaname', TRUE);
			$eremark	= $this->input->post('eremark', TRUE);
			$jml		= $this->input->post('jml', TRUE);
			$fop		= 'f';
			$nprint		= 0;
			if ($dspmb != '' && $eareaname != '') {
				$this->db->trans_begin();
				$this->load->model('spmbnew/mmaster');
				$ispmb	= $this->mmaster->runningnumber($thbl);
				$this->mmaster->insertheader($ispmb, $dspmb, $iarea, $fop, $nprint, $ispmbold, $eremark);
				for ($i = 1; $i <= $jml; $i++) {
					$iproduct			  = $this->input->post('iproduct' . $i, TRUE);
					$iproductgrade	= 'A';
					$iproductmotif	= $this->input->post('motif' . $i, TRUE);
					$eproductname		= $this->input->post('eproductname' . $i, TRUE);
					$vunitprice		  = $this->input->post('vproductmill' . $i, TRUE);
					$vunitprice	  	= str_replace(',', '', $vunitprice);
					$norder	    		= $this->input->post('norder' . $i, TRUE);
					$nacc 	    		= $this->input->post('nacc' . $i, TRUE);
					$eremark		  	= $this->input->post('eremark' . $i, TRUE);
					if ($norder != '0' && $norder != 0 && $norder != '') {
						$this->mmaster->insertdetail($ispmb, $iproduct, $iproductgrade, $eproductname, $norder, $nacc, $vunitprice, $iproductmotif, $eremark, $iarea, $i);
					}
				}
				if (($this->db->trans_status() === FALSE)) {
					$this->db->trans_rollback();
				} else {
					#			    $this->db->trans_rollback();
					$this->db->trans_commit();

					$sess = $this->session->userdata('session_id');
					$id = $this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		=  $this->db->query($sql);
					if ($rs->num_rows > 0) {
						foreach ($rs->result() as $tes) {
							$ip_address	  = $tes->ip_address;
							break;
						}
					} else {
						$ip_address = 'kosong';
					}

					$data['user']	= $this->session->userdata('user_id');
					#			$data['host']	= $this->session->userdata('printerhost');
					$data['host']	= $ip_address;
					$data['uri']	= $this->session->userdata('printeruri');
					$query 	= pg_query("SELECT current_timestamp as c");
					while ($row = pg_fetch_assoc($query)) {
						$now	  = $row['c'];
					}
					$pesan = 'Input SPMB Area:' . $iarea . ' No:' . $ispmb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now, $pesan);

					$data['sukses']			= true;
					$data['inomor']			= $ispmb;
					$this->load->view('nomor', $data);
				}
			} else {
				$data['page_title'] = $this->lang->line('spmb');

				$this->logger->writenew("Membuka Menu : " . $this->menu . " - " . $this->title);

				$data['ispmb'] = '';
				$this->load->model('spmbnew/mmaster');
				$data['isi'] = '';
				$data['istore'] = '';
				$data['estorename'] = '';
				$data['eremark'] = '';
				$data['detail'] = "";
				$data['jmlitem'] = "";
				$data['tgl'] = date('d-m-Y');
				$this->load->view('spmbnew/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu8') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('spmb');
			$this->load->view('spmbnew/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$data['page_title'] = $this->lang->line('spmb') . " update";
			if ($this->uri->segment(4) != '') {
				$ispmb  = $this->uri->segment(4);
				$iarea  = $this->uri->segment(5);
				$dfrom  = $this->uri->segment(6);
				$dto 	  = $this->uri->segment(7);
				$peraw 	= $this->uri->segment(8);
				$perak 	= $this->uri->segment(9);
				$data['ispmb'] = $ispmb;
				$data['iarea'] = $iarea;
				$data['dfrom'] = $dfrom;
				$data['dto']	 = $dto;
				$data['peraw']	 = $peraw;
				$data['perak']	 = $perak;
				$query = $this->db->query("select i_product from tm_spmb_item where i_spmb='$ispmb'");
				$data['jmlitem'] = $query->num_rows();
				$this->load->model('spmbnew/mmaster');
				$data['isi'] = $this->mmaster->baca($ispmb);
				$data['detail'] = $this->mmaster->bacadetail($ispmb);
				$this->load->view('spmbnew/vmainform', $data);
			} else {
				$this->load->view('spmbnew/vinsert_fail', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$ispmb 		= $this->input->post('ispmb', TRUE);
			$ispmbold 	= $this->input->post('ispmbold', TRUE);
			$dspmb 		= $this->input->post('dspmb', TRUE);
			$eremark	= $this->input->post('eremark', TRUE);
			if ($dspmb != '') {
				$tmp = explode("-", $dspmb);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dspmb = $th . "-" . $bl . "-" . $hr;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$eareaname	= $this->input->post('eareaname', TRUE);
			$jml		= $this->input->post('jml', TRUE);
			$fop		= 'f';
			$nprint		= 0;

			$fretur		= $this->input->post('fretur', TRUE) != "" ? true : false;

			if ($dspmb != '' && $eareaname != '') {
				$this->db->trans_begin();
				$this->load->model('spmbnew/mmaster');
				$this->mmaster->updateheader($ispmb, $dspmb, $iarea, $ispmbold, $eremark, $fretur);
				for ($i = 1; $i <= $jml; $i++) {
					$iproduct		  	= $this->input->post('iproduct' . $i, TRUE);
					$iproductgrade	= 'A';
					$iproductmotif	= $this->input->post('motif' . $i, TRUE);
					$eproductname		= $this->input->post('eproductname' . $i, TRUE);
					$vunitprice	  	= $this->input->post('vproductmill' . $i, TRUE);
					$vunitprice 		= str_replace(',', '', $vunitprice);
					$norder   			= $this->input->post('norder' . $i, TRUE);
					$nacc 	    		= $this->input->post('nacc' . $i, TRUE);
					$eremark		  	= $this->input->post('eremark' . $i, TRUE);
					$this->mmaster->deletedetail($iproduct, $iproductgrade, $ispmb, $iproductmotif);
					if ($norder > 0) {
						$this->mmaster->insertdetail($ispmb, $iproduct, $iproductgrade, $eproductname, $norder, $nacc, $vunitprice, $iproductmotif, $eremark, $iarea, $i);
					}
				}
				if (($this->db->trans_status() === FALSE)) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();

					$sess = $this->session->userdata('session_id');
					$id = $this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		=  $this->db->query($sql);
					if ($rs->num_rows > 0) {
						foreach ($rs->result() as $tes) {
							$ip_address	  = $tes->ip_address;
							break;
						}
					} else {
						$ip_address = 'kosong';
					}

					$data['user']	= $this->session->userdata('user_id');
					#			$data['host']	= $this->session->userdata('printerhost');
					$data['host']	= $ip_address;
					$data['uri']	= $this->session->userdata('printeruri');
					$query 	= pg_query("SELECT current_timestamp as c");
					while ($row = pg_fetch_assoc($query)) {
						$now	  = $row['c'];
					}
					$pesan = 'Update SPMB Area:' . $iarea . ' No:' . $ispmb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now, $pesan);

					$data['sukses']			= true;
					$data['inomor']			= $ispmb;
					$this->load->view('nomor', $data);
				}
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$ispmb	= $this->input->post('ispmbdelete', TRUE);
			$this->load->model('spmbnew/mmaster');
			$this->mmaster->delete($ispmb);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if ($rs->num_rows > 0) {
				foreach ($rs->result() as $tes) {
					$ip_address	  = $tes->ip_address;
					break;
				}
			} else {
				$ip_address = 'kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
			#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Hapus SPMB No:' . $ispmb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$data['page_title'] = $this->lang->line('spmb');
			$data['ispmb'] = '';
			$data['jmlitem'] = '';
			$data['detail'] = '';
			$data['isi'] = $this->mmaster->bacasemua();
			$this->load->view('spmbnew/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$ispmb			= $this->input->post('ispmbdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$iarea = $this->input->post('iareadel', TRUE);
			$dfrom = $this->input->post('dfrom', TRUE);
			$dto 	= $this->input->post('dto', TRUE);

			$this->db->trans_begin();
			$this->load->model('spmbnew/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $ispmb, $iproductmotif);
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();

				$data['ispmb'] = $ispmb;
				$data['iarea'] = $iarea;
				$data['dfrom'] = $dfrom;
				$data['dto']	 = $dto;
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ispmb'");
				$data['jmlitem'] = $query->num_rows();

				$this->load->model('spmbnew/mmaster');
				$data['isi'] = $this->mmaster->baca($ispmb);
				$data['detail'] = $this->mmaster->bacadetail($ispmb);




				$this->load->view('spmbnew/vmainform', $data);

				/*
  			$data['page_title'] = $this->lang->line('spmb')." Update";
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ispmb'");
				$data['jmlitem']= $query->num_rows(); 				
				$data['ispmb']  = $ispmb;
				$data['isi']    =$this->mmaster->baca($ispmb);
				$data['detail']=$this->mmaster->bacadetail($ispmb);
				$this->load->view('spmbnew/vmainform', $data);
			*/
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$cari = strtoupper($this->input->post("cari"));
			$baris = $this->input->post("baris");
			$peraw = $this->input->post("peraw");
			$perak = $this->input->post("perak");
			$area = $this->input->post("area");
			if ($baris == '') $baris = $this->uri->segment(4);
			if ($peraw == '') $peraw = $this->uri->segment(5);
			if ($perak == '') $perak = $this->uri->segment(6);
			if ($area == '') $area = $this->uri->segment(7);
			$config['base_url'] = base_url() . 'index.php/spmbnew/cform/product/' . $baris . '/' . $peraw . '/' . $perak . '/' . $area . '/';

			/*
select trunc(sum(n_deliver)/3) as rata, i_product, i_area from tm_nota_item
where i_nota>'FP-1107' and i_nota<'FP-1111'
group by i_product, i_area
order by i_product, i_area
*/

			$query = $this->db->query(" select a.i_product
										              from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product and upper(a.i_product) like '%$cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('spmbnew/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris'] = $baris;
			$data['peraw'] = $peraw;
			$data['perak'] = $perak;
			$data['area'] = $area;
			$data['isi'] = $this->mmaster->bacaproduct($config['per_page'], $this->uri->segment(8), $peraw, $perak, $cari);
			$data['baris'] = $baris;
			$this->load->view('spmbnew/vlistproduct', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$data['baris'] = $this->uri->segment(4);
			$baris = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/spmbnew/cform/product/' . $baris . '/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama,c.v_product_mill as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
								and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('spmbnew/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi'] = $this->mmaster->cariproduct($cari, $config['per_page'], $this->uri->segment(6));
			$data['baris'] = $baris;
			$this->load->view('spmbnew/vlistproduct', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$config['base_url'] = base_url() . 'index.php/spmbnew/cform/area/index/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('spmbnew/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('spmbnew/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$config['base_url'] = base_url() . 'index.php/spmbnew/cform/cariarea/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$iuser   	= $this->session->userdata('user_id');
			$cari    	= $this->input->post('cari', FALSE);
			$cari		  = strtoupper($cari);
			$query   	= $this->db->query("select * from tr_area
                              where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spmbnew/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('spmbnew/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url() . 'index.php/spmbnew/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('spmbnew/mmaster');
			$data['isi'] = $this->mmaster->cari($cari, $config['per_page'], $this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['ispmb'] = '';
			$data['jmlitem'] = '';
			$data['detail'] = '';
			$this->load->view('spmbnew/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			#			$data['baris']=$this->uri->segment(4);
			#			$baris=$this->uri->segment(4);
			$cari = strtoupper($this->input->post("cari"));
			$baris = $this->input->post("baris");
			$peraw = $this->input->post("peraw");
			$perak = $this->input->post("perak");
			$area = $this->input->post("area");
			if ($baris == '') $baris = $this->uri->segment(4);
			if ($peraw == '') $peraw = $this->uri->segment(5);
			if ($perak == '') $perak = $this->uri->segment(6);
			if ($area == '') $area = $this->uri->segment(7);
			$config['base_url'] = base_url() . 'index.php/spmbnew/cform/productupdate/' . $baris . '/' . $peraw . '/' . $perak . '/' . $area . '/';

			#			$config['base_url'] = base_url().'index.php/spmbnew/cform/productupdate/'.$baris.'/index/';
			// $query = $this->db->query(" select a.i_product from tr_product_motif a,tr_product c
			// 							              where a.i_product=c.i_product", false);
			$query = $this->db->query("	SELECT a.i_product as kode, a.i_product_motif as motif,
										a.e_product_motifname as namamotif, 
										c.e_product_name as nama,c.v_product_retail as harga, d.e_sales_categoryname 
										from tr_product_motif a,tr_product c, tr_product_sales_category d
										where a.i_product=c.i_product 
										and a.i_product_motif='00'
										and c.i_product_status<>'4'
										AND c.i_sales_category = d.i_sales_category 
										and (upper(a.i_product) ilike '%$cari%' or upper(c.e_product_name) ilike '%$cari%') 
										order by d.i_sales_category, c.i_product, a.e_product_motifname ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('spmbnew/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi'] = $this->mmaster->bacaproduct($config['per_page'], $this->uri->segment(8), $peraw, $perak, $cari);
			$data['baris'] = $baris;
			$data['peraw'] = $peraw;
			$data['perak'] = $perak;
			$data['area'] = $area;
			$this->load->view('spmbnew/vlistproductupdate', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariproductupdate()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$data['baris'] = $this->uri->segment(4);
			$baris = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/spmbnew/cform/productupdate/' . $baris . '/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			// $query = $this->db->query(
			// 	"	select a.i_product||a.i_product_motif as kode, 
			// 							c.e_product_name as nama,c.v_product_mill as harga
			// 							from tr_product_motif a,tr_product c
			// 							where a.i_product=c.i_product
			// 							and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc ",
			// 	false
			// );
			$query = $this->db->query("	SELECT a.i_product as kode, a.i_product_motif as motif,
										a.e_product_motifname as namamotif, 
										c.e_product_name as nama,c.v_product_retail as harga, d.e_sales_categoryname 
										from tr_product_motif a,tr_product c, tr_product_sales_category d
										where a.i_product=c.i_product 
										and a.i_product_motif='00'
										and c.i_product_status<>'4'
										AND c.i_sales_category = d.i_sales_category 
										and (upper(a.i_product) ilike '%$cari%' or upper(c.e_product_name) ilike '%$cari%') 
										order by d.i_sales_category, c.i_product, a.e_product_motifname ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('spmbnew/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi'] = $this->mmaster->cariproduct($cari, $config['per_page'], $this->uri->segment(6));
			$data['baris'] = $baris;
			$this->load->view('spmbnew/vlistproductupdate', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function loaditem()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$istore = $this->uri->segment(4);
			$estorename = str_replace('%20', ' ', $this->uri->segment(5));
			$istorelocation = $this->uri->segment(6);
			$estorelocationname = str_replace('%20', ' ', $this->uri->segment(7));
			$iarea = $this->uri->segment(8);
			$dspmb = $this->uri->segment(9);
			$tmp = explode('-', $dspmb);
			$th = $tmp[2];
			$bl = $tmp[1];
			$dt = $tmp[0];
			$iperiode = $th . $bl;
			$data['istore']             = $this->uri->segment(4);
			$data['estorename']         = str_replace('%20', ' ', $this->uri->segment(5));
			$data['istorelocation']     = $this->uri->segment(6);
			$data['estorelocationname'] = str_replace('%20', ' ', $this->uri->segment(7));
			$data['iarea']              = $this->uri->segment(8);
			$data['dspmb']              = $this->uri->segment(9);
			$data['eremark']            = $this->uri->segment(10);
			$data['ispmb']              = '';
			$data['isi']	              = 'ada';
			$data['page_title'] = $this->lang->line('spmb');
			$this->load->model('spmbnew/mmaster');
			$data['jmlitem'] = $this->mmaster->jumlahitem($istore, $istorelocation, $iperiode);
			$data['detail'] = $this->mmaster->bacamutasi($istore, $istorelocation, $iperiode);
			$this->load->view('spmbnew/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$config['base_url'] = base_url() . 'index.php/spmbnew/cform/store/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store");
			} else {
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store
                                  and (c.i_area = '$area1' or c.i_area = '$area2' or
                                  c.i_area = '$area3' or c.i_area = '$area4' or
                                  c.i_area = '$area5')");
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('spmbnew/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi'] = $this->mmaster->bacastore($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('spmbnew/vliststore', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$config['base_url'] = base_url() . 'index.php/spmbnew/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
				$query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')", false);
			} else {
				$query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')
                                    and (c.i_area = '$area1' or c.i_area = '$area2' or
                                    c.i_area = '$area3' or c.i_area = '$area4' or
                                    c.i_area = '$area5')", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spmbnew/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi'] = $this->mmaster->caristore($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('spmbnew/vliststore', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
