<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu190')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listikhpkeluar');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('cekikhpkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu190')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cekikhpkeluar/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      if($cari==''){
		    $query = $this->db->query(" select	a.*, b.e_area_name from tm_ikhp a, tr_area b
							    where a.i_area=b.i_area 
							    and a.i_area='$iarea' and
							    a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
		    $query = $this->db->query(" select	a.*, b.e_area_name from tm_ikhp a, tr_area b
							    where a.i_area=b.i_area 
							    and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_bukti) like '%$cari%' or a.i_ikhp=$cari )
							    or a.v_keluar_tunai like '%$cari%' or a.v_keluar_giro like '%$cari%'
							    and a.i_area='$iarea' and
							    a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
      }
/*
			$query = $this->db->query(" select a.*, b.e_area_name from tm_ikhp a, tr_area b
										              where a.i_area=b.i_area
										              and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										              or upper(a.i_ikhp) like '%$cari%')
										              and a.i_area='$iarea' and
										              a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
										              a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
*/
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listikhpkeluar');
			$this->load->model('cekikhpkeluar/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
        while($row=pg_fetch_assoc($rs)){
	        $ip_address	  = $row['ip_address'];
	        break;
        }
      }else{
        $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
        $now	  = $row['c'];
      }
      $pesan='Buka IKHP keluar periode:'.$dfrom.' s/d '.$dto.' Area:'.$iarea;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('cekikhpkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu190')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listikhpkeluar');
			$this->load->view('cekikhpkeluar/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu190')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('ikhpkeluar')." update";
			if(
				$this->uri->segment(4)
			  ){
				$data['iikhp']	= $this->uri->segment(4);
				$iikhpkeluar				= $this->uri->segment(4);
				$data['dfrom']	= $this->uri->segment(5);
				$data['dto']	= $this->uri->segment(6);
				$data['iarea']	= $this->uri->segment(7);
				$this->load->model("cekikhpkeluar/mmaster");
				$data['isi']=$this->mmaster->baca($iikhpkeluar);
		 		$this->load->view('cekikhpkeluar/vformupdate',$data);
			}else{
				$this->load->view('cekikhpkeluar/vinsert_fail',$data); //
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu190')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$iikhp	= $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(6);
			if($dto=='') $dto=$this->uri->segment(7);
			$this->load->model('cekikhpkeluar/mmaster');
			$this->mmaster->delete($iikhp,$iarea);
			$config['base_url'] = base_url().'index.php/cekikhpkeluar/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_ikhp a, tr_area b, tr_ikhp_type c
										where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
										and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										or upper(a.i_ikhp) like '%$cari%' or upper(a.i_bukti) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listikhpkeluar');
			$this->load->model('cekikhpkeluar/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']= $iarea;
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('cekikhpkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu190')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cekikhpkeluar/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      if($cari==''){
		    $query=$this->db->query("	select a.*, b.e_area_name from tm_ikhp a, tr_area b, tr_ikhp_type c 
							    where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
							    and a.i_area='$iarea' and
							    a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
		    $query=$this->db->query(" select	a.*, b.e_area_name from tm_ikhp a, tr_area b, tr_ikhp_type c
							    where a.i_area=b.i_area and a.i_ikhp_type=c.i_ikhp_type
							    and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
							    or upper(a.i_bukti) like '%$cari%')
							    and a.i_area='$iarea' and
							    a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
							    a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listikhpkeluar');
			$this->load->model('cekikhpkeluar/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('cekikhpkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu190')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/cekikhpkeluar/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('cekikhpkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('cekikhpkeluar/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu190')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/cekikhpkeluar/cform/area/index/';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                                    where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                    and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('cekikhpkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('cekikhpkeluar/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu190')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iikhp	 			= $this->input->post('iikhp', TRUE);
			$iarea	 			= $this->input->post('iarea', TRUE);
			$dbukti 			= $this->input->post('dbukti', TRUE);
			$dtmp					= $dbukti;
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
			}
			$ibukti				= $this->input->post('ibukti', TRUE);
			if($ibukti=='') $ibukti=null;
			$eikhptypename= $this->input->post('eikhptypename', TRUE);
			$icoa					= $this->input->post('icoa', TRUE);
			$ecoaname 		= $this->input->post('ecoaname', TRUE);
			$iikhptype		= $this->input->post('iikhptype', TRUE);
			$vterimatunai = $this->input->post('vterimatunai', TRUE);
			if($vterimatunai=='') $vterimatunai=0;
			$vterimatunai		= str_replace(',','',$vterimatunai);
			$vterimagiro	= $this->input->post('vterimagiro', TRUE);
			if($vterimagiro=='') $vterimagiro=0;
			$vterimagiro		= str_replace(',','',$vterimagiro);
			$vkeluartunai = $this->input->post('vkeluartunai', TRUE);
			if($vkeluartunai=='') $vkeluartunai=0;
			$vkeluartunai		= str_replace(',','',$vkeluartunai);
			$vkeluargiro	= $this->input->post('vkeluargiro', TRUE);
			if($vkeluargiro=='') $vkeluargiro=0;
			$vkeluargiro		= str_replace(',','',$vkeluargiro);
			$ecek1	= $this->input->post('ecek1', TRUE);
			if($ecek1=='')
				$ecek1=null;
			$user	= $this->session->userdata('user_id');

			if (
				($dbukti != '') && ($eikhptypename != '') && ($iarea != '') &&
				( ($vterimatunai!='') || ($vterimagiro!='') || ($vkeluartunai!='') || ($vkeluargiro!='') )
			   )
			{
				$this->load->model('cekikhpkeluar/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($iikhp,$iarea,$dbukti,$ibukti,$icoa,$iikhptype,$vterimatunai,$vterimagiro,$vkeluartunai,$vkeluargiro,$ecek1,$user);
				$nomor=$dtmp." - ".$eikhptypename;
				if ($this->db->trans_status() === FALSE)
				{
					  $this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

          $sess=$this->session->userdata('session_id');
          $id=$this->session->userdata('user_id');
          $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
          $rs		= pg_query($sql);
          if(pg_num_rows($rs)>0){
            while($row=pg_fetch_assoc($rs)){
	            $ip_address	  = $row['ip_address'];
	            break;
            }
          }else{
            $ip_address='kosong';
          }
          $query 	= pg_query("SELECT current_timestamp as c");
          while($row=pg_fetch_assoc($query)){
            $now	  = $row['c'];
          }
          $pesan='Cek IKHP Keluar No:'.$iikhp.' Area:'.$iarea;
          $this->load->model('logger');
          $this->logger->write($id, $ip_address, $now , $pesan );


					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function uraian()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu190')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/cekikhpkeluar/cform/uraian/index/';
			$query = $this->db->query("select * from tr_ikhp_type ",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('cekikhpkeluar/mmaster');
			$data['page_title'] = $this->lang->line('listuraian');
			$data['isi']=$this->mmaster->bacauraian($config['per_page'],$this->uri->segment(5));
			$this->load->view('cekikhpkeluar/vlisturaian', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariuraian()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu190')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/cekikhpkeluar/cform/uraian/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query("select * from tr_ikhp_type
						   where upper(i_uraian) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('cekikhpuraian/mmaster');
			$data['page_title'] = $this->lang->line('listuraian');
			$data['isi']=$this->mmaster->cariuraian($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('cekikhpkeluar/vlisturaian', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
