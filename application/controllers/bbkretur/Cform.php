<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbkretur= $this->input->post('ibbkretur', TRUE);
			$dbbkretur= $this->input->post('dbbkretur', TRUE);
			if($dbbkretur!=''){
				$tmp=explode("-",$dbbkretur);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbbkretur=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
			}
			$isupplier		= $this->input->post('isupplier', TRUE);
			$esuppliername= $this->input->post('esuppliername', TRUE);
			$eremark	    = $this->input->post('eremark', TRUE);
		  $vbbkretur	  = $this->input->post('vtotal', TRUE);
		  $vbbkretur  	= str_replace(',','',$vbbkretur);
  		$jml		      = $this->input->post('jml', TRUE);
			if($dbbkretur!='' && $esuppliername!='' && $jml!='' && $jml!='0')
			{
				$this->db->trans_begin();
				$this->load->model('bbkretur/mmaster');
        $istore				    = 'AA';
				$istorelocation		= '01';
				$istorelocationbin= '00';
				$ibbkretur	=$this->mmaster->runningnumber($thbl);
				$this->mmaster->insertheader($ibbkretur, $dbbkretur, $isupplier, $eremark, $vbbkretur);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= 'A';
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		  = $this->input->post('vunitprice'.$i, TRUE);
				  $vunitprice	  	= str_replace(',','',$vunitprice);
				  $nquantity   		= $this->input->post('nquantity'.$i, TRUE);
				  $eremark		  	= $this->input->post('eremark'.$i, TRUE);
				  $idtap		  	  = $this->input->post('idtap'.$i, TRUE);
				  if($nquantity>0){
				    $this->mmaster->insertdetail( $ibbkretur,$isupplier,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$vunitprice,$eremark,$i,$thbl, $idtap);
############
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttransbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibbkretur,$q_in,$q_out,$nquantity,$q_aw,$q_ak);
            $th=substr($dbbkretur,0,4);
            $bl=substr($dbbkretur,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateicbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$q_ak);
            }else{
              $this->mmaster->inserticbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nquantity);
            }
############
				  }
				}
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Input BBK-Retur No:'.$ibbkretur;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );
#			    $this->db->trans_rollback();
			    $this->db->trans_commit();
					$data['sukses']			= true;
					$data['inomor']			= $ibbkretur;
					$this->load->view('nomor',$data);
				}
			}else{
				$data['page_title'] = $this->lang->line('bbkretur');
				$data['ibbk']='';
        $data['cari']='';
				$this->load->model('bbkretur/mmaster');
				$data['isi']="";
				$data['detail']="";
				$data['jmlitem']="";
				$data['tgl']=date('d-m-Y');
				$this->load->view('bbkretur/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bbkretur');
			$this->load->view('bbkretur/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) ) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bbkretur')." update";
			if($this->uri->segment(4)!=''){
				$ibbk  = $this->uri->segment(4);
				$dfrom  = $this->uri->segment(5);
				$dto 	  = $this->uri->segment(6);
				$data['ibbk'] = $ibbk;
				$data['dfrom'] = $dfrom;
				$data['dto']	 = $dto;
				$query = $this->db->query("select i_product from tm_bbkretur_item where i_bbkretur='$ibbk'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('bbkretur/mmaster');
				$data['isi']=$this->mmaster->baca($ibbk);
				$data['detail']=$this->mmaster->bacadetail($ibbk);
		 		$this->load->view('bbkretur/vmainform',$data);
			}else{
				$this->load->view('bbkretur/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbkretur= $this->input->post('ibbkretur', TRUE);
			$dbbkretur= $this->input->post('dbbkretur', TRUE);
			if($dbbkretur!=''){
				$tmp=explode("-",$dbbkretur);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbbkretur=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
			}
			$isupplier		= $this->input->post('isupplier', TRUE);
			$esuppliername= $this->input->post('esuppliername', TRUE);
			$eremark	    = $this->input->post('eremark', TRUE);
		  $vbbkretur	  = $this->input->post('vtotal', TRUE);
		  $vbbkretur  	= str_replace(',','',$vbbkretur);
  		$jml		      = $this->input->post('jml', TRUE);
			if($dbbkretur!='' && $esuppliername!='' && $jml!='' && $jml!='0')
			{
				$this->db->trans_begin();
				$this->load->model('bbkretur/mmaster');
        $istore				    = 'AA';
				$istorelocation		= '01';
				$istorelocationbin= '00';
				$this->mmaster->updateheader($ibbkretur, $dbbkretur, $isupplier, $eremark, $vbbkretur);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= 'A';
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		  = $this->input->post('vunitprice'.$i, TRUE);
				  $vunitprice	  	= str_replace(',','',$vunitprice);
				  $nquantity   		= $this->input->post('nquantity'.$i, TRUE);
				  $xnquantity  		= $this->input->post('xnquantity'.$i, TRUE);
				  $eremark		  	= $this->input->post('eremark'.$i, TRUE);
#				  if($nquantity>0){
          if($xnquantity>0){
  				  $this->mmaster->deletedetail($ibbkretur, $iproduct, $iproductmotif, $iproductgrade);
            $th=substr($dbbkretur,0,4);
				    $bl=substr($dbbkretur,5,2);
				    $emutasiperiode=$th.$bl;
				    $tra=$this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ibbkretur,$xnquantity,$eproductname);
		        $this->mmaster->updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$xnquantity,$emutasiperiode);
		        $this->mmaster->updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$xnquantity);
          }
#				    $this->mmaster->updatedetail($ibbkretur,$isupplier,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$vunitprice,$eremark,$i,$thbl);
			    $this->mmaster->insertdetail( $ibbkretur,$isupplier,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$vunitprice,$eremark,$i,$thbl);
############
          $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
          if(isset($trans)){
            foreach($trans as $itrans)
            {
              $q_aw =$itrans->n_quantity_awal;
              $q_ak =$itrans->n_quantity_akhir;
              $q_in =$itrans->n_quantity_in;
              $q_out=$itrans->n_quantity_out;
              break;
            }
          }else{
            $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_stock;
                $q_ak =$itrans->n_quantity_stock;
                $q_in =0;
                $q_out=0;
                break;
              }
            }else{
              $q_aw=0;
              $q_ak=0;
              $q_in=0;
              $q_out=0;
            }
          }
          $this->mmaster->inserttransbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibbkretur,$q_in,$q_out,$nquantity,$q_aw,$q_ak);
          $th=substr($dbbkretur,0,4);
          $bl=substr($dbbkretur,5,2);
          $emutasiperiode=$th.$bl;
          if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
          {
            $this->mmaster->updatemutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
          }else{
            $this->mmaster->insertmutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
          }
          if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
          {
            $this->mmaster->updateicbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$q_ak);
          }else{
            $this->mmaster->inserticbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nquantity);
          }
############
				}
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Edit BBK-Retur No:'.$ibbkretur;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );
#			    $this->db->trans_rollback();
			    $this->db->trans_commit();
					$data['sukses']			= true;
					$data['inomor']			= $ibbkretur;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbk	= $this->input->post('ibbkdelete', TRUE);
			$this->load->model('bbkretur/mmaster');
			$this->mmaster->delete($ibbk);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
	      $now	  = $row['c'];
      }
      $pesan='Delete BBK-Hadiah No:'.$ibbk;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('bbkretur');
			$data['ibbk']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('bbkretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbk			= $this->input->post('ibbkdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$iarea = $this->input->post('iareadel', TRUE);
			$dfrom= $this->input->post('dfrom', TRUE);
			$dto 	= $this->input->post('dto', TRUE);

			$this->db->trans_begin();
			$this->load->model('bbkretur/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $ibbk, $iproductmotif);
			if ($this->db->trans_status() === FALSE)
			{
			  $this->db->trans_rollback();
			}else{
			  $this->db->trans_commit();

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='Delete Item BBK-Hadiah No:'.$ibbk;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['ibbk'] = $ibbk;
			  $data['iarea'] = $iarea;
			  $data['dfrom'] = $dfrom;
			  $data['dto']	 = $dto;
			  $query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ibbk'");
			  $data['jmlitem'] = $query->num_rows(); 				
			  $this->load->model('bbkretur/mmaster');
			  $data['isi']=$this->mmaster->baca($ibbk);
			  $data['detail']=$this->mmaster->bacadetail($ibbk);
	   		$this->load->view('bbkretur/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari=strtoupper($this->input->post("cari"));
      $baris=$this->input->post("baris");
      $supp=$this->input->post("supp");
      $dbbk=$this->input->post("dbbk");
			if($baris=='')$baris=$this->uri->segment(4);
			if($supp=='')$supp=$this->uri->segment(5);
			if($dbbk=='')$dbbk=$this->uri->segment(6);
 		  $tmp=explode('-',$dbbk);
			$bulan=$tmp[1];
			$tahun=$tmp[2];
      if($cari=='' && $this->uri->segment(7)!='zxasqw'){
        $cari=$this->uri->segment(7);
      }
      if($cari!=''){
/*
			  $query = $this->db->query(" select a.i_product from tr_product_motif a,tr_product c, tr_supplier d
										                where a.i_product=c.i_product and (upper(a.i_product) like '%$cari%' 
                                    or upper(c.e_product_name) like '%$cari%')
                                    and c.i_supplier='$supp' and c.i_supplier=d.i_supplier",false);
*/
			  $query = $this->db->query(" select a.i_product from tr_product_motif a,tr_product c, tr_supplier d, tm_dtap e, tm_dtap_item f
										                where a.i_product=c.i_product and 
										                e.i_dtap=f.i_dtap and e.i_area=f.i_area and e.i_supplier=f.i_supplier and
    						                    f.i_product=c.i_product and e.i_supplier=d.i_supplier and (upper(a.i_product) like '%$cari%' 
                                    or upper(c.e_product_name) like '%$cari%' or upper(e.i_pajak) like '%$cari%')
                                    and c.i_supplier='$supp' and c.i_supplier=d.i_supplier",false);
			  $config['total_rows'] = $query->num_rows();
			$config['base_url'] = base_url().'index.php/bbkretur/cform/product/'.$baris.'/'.$supp.'/'.$dbbk.'/'.$cari.'/';
      }else{
        $config['total_rows'] = 10;
  			$config['base_url'] = base_url().'index.php/bbkretur/cform/product/'.$baris.'/'.$supp.'/'.$dbbk.'/zxasqw/';
      }
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('bbkretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$baris;
			$data['supp']=$supp;
			$data['cari']=$cari;
			$data['dbbk']=$dbbk;
			if($cari!=''){
  			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(8),$cari,$supp,$bulan,$tahun);
  	  }else{
  	    $data['isi']='';
  	  }
			$this->load->view('bbkretur/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bbkretur/cform/product/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
								and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc ", false);
#c.v_product_mill as harga
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('bbkretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('bbkretur/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari=strtoupper($this->input->post("cari"));
      if($cari==''){
        if($this->uri->segment(4)==''){
          $data['cari']=''; 
    			$config['base_url'] = base_url().'index.php/bbkretur/cform/supplier/sikasep/';
        }elseif($this->uri->segment(4)=='sikasep'){
          $data['cari']=''; 
    			$config['base_url'] = base_url().'index.php/bbkretur/cform/supplier/sikasep/';
        }elseif($this->uri->segment(4)!='sikasep'){
          $cari=$this->uri->segment(4);
          $data['cari']=$this->uri->segment(4);
    			$config['base_url'] = base_url().'index.php/bbkretur/cform/supplier/'.$cari.'/';
        }
      }else{
        $data['cari']=$cari;
        $config['base_url'] = base_url().'index.php/bbkretur/cform/supplier/'.$cari.'/';
      }

			$query = $this->db->query("select i_supplier from tr_supplier 
                                 where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') ",false);				
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);    

			$this->pagination->initialize($config);
			$this->load->model('bbkretur/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('bbkretur/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bbkretur/cform/area/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$allarea	= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($allarea=='t') {
				$query = $this->db->query("select * from tr_area ",false);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') ){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%'
						or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') ",false);				
			} else {
				$query = $this->db->query("select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3'
						or i_area='$area4' or i_area='$area5' ",false);				
			}				
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbkretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5,$allarea);
			$this->load->view('bbkretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/bbkretur/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('bbkretur/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['ibbk']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('bbkretur/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
#			$data['baris']=$this->uri->segment(4);
#			$baris=$this->uri->segment(4);
      $cari=strtoupper($this->input->post("cari"));
      $baris=$this->input->post("baris");
      $peraw=$this->input->post("peraw");
      $perak=$this->input->post("perak");
      $area =$this->input->post("area");
			if($baris=='')$baris=$this->uri->segment(4);
      if($peraw=='')$peraw=$this->uri->segment(5);
      if($perak=='')$perak=$this->uri->segment(6);
      if($area=='')$area=$this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/bbkretur/cform/productupdate/'.$baris.'/'.$peraw.'/'.$perak.'/'.$area.'/';

#			$config['base_url'] = base_url().'index.php/bbkretur/cform/productupdate/'.$baris.'/index/';
			$query = $this->db->query(" select a.i_product from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('bbkretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(8),$peraw,$perak,$cari);
			$data['baris']=$baris;
      $data['peraw']=$peraw;
      $data['perak']=$perak;
      $data['area']=$area;
			$this->load->view('bbkretur/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bbkretur/cform/productupdate/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_retail as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
										and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc "
									  ,false);
# c.v_product_mill as harga
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('bbkretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('bbkretur/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/bbkretur/cform/store/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store");
			} else {
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store
                                  and (c.i_area = '$area1' or c.i_area = '$area2' or
                                  c.i_area = '$area3' or c.i_area = '$area4' or
                                  c.i_area = '$area5')");				
			}
			$config['total_rows'] = $query->num_rows(); 	
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bbkretur/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('bbkretur/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu468')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/bbkretur/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
        $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')",false);
      }else{
			  $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')
                                    and (c.i_area = '$area1' or c.i_area = '$area2' or
                                    c.i_area = '$area3' or c.i_area = '$area4' or
                                    c.i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbkretur/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('bbkretur/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
