<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transferm');
			$this->load->model('transferuangmasuk/mmaster');
			$data['ikum']='';
			$this->load->view('transferuangmasuk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transferm');
			$this->load->view('transferuangmasuk/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (($this->session->userdata('logged_in'))
		){
			$data['page_title'] = $this->lang->line('transferm')." update";
			if(
				($this->uri->segment(4)!='')
			  ){
        $ikum           = $this->uri->segment(4);
				$ikum			      = str_replace('|','/',$ikum);
				$ikum			      = str_replace('%20',' ',$ikum);
				$nkumyear	      = $this->uri->segment(5);
				$iarea		      = $this->uri->segment(6);
				$dfrom		      = $this->uri->segment(7);
				$dto			      = $this->uri->segment(8);
				$data['ikum']     = $ikum;
				$data['nkumyear']	= $nkumyear;
				$data['iarea']		= $iarea;
				$data['dfrom']		= $dfrom;
				$data['dto']		= $dto;
        $data['pst']	= $this->session->userdata('i_area');
       # $query 	= $this->db->query("select * from tm_kum where i_kum = '$ikum' and i_area='$iarea'");
				$this->load->model("transferuangmasuk/mmaster");
				$data['isi']=$this->mmaster->baca($iarea,$ikum,$nkumyear);
		 		$this->load->view('transferuangmasuk/vformupdate',$data);
			}else{
				$this->load->view('transferuangmasuk/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikum 	= $this->input->post('ikum', TRUE);
			$xkum 	= $this->input->post('xkum', TRUE);
			$ikum		= str_replace(' ','',$ikum);
			$xkum		= str_replace(' ','',$xkum);
			$xdkum	= $this->input->post('xdkum', TRUE);
			if($xdkum!=''){
				$tmp=explode("-",$xdkum);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$xdkum=$th."-".$bl."-".$hr;
				$xtahun=$th;
			}
			$dkum	= $this->input->post('dkum', TRUE);
			if($dkum!=''){
				$tmp=explode("-",$dkum);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkum=$th."-".$bl."-".$hr;
				$tahun=$th;
			}
			$ebankname		= $this->input->post('ebankname', TRUE);
			$iarea			  = $this->input->post('iarea', TRUE);
			$iareaasal	  = $this->input->post('iareaasal', TRUE);
			$eareaname		= $this->input->post('eareaname', TRUE);
			$icustomer		= $this->input->post('icustomer', TRUE);
			$icustomergroupar	= $this->input->post('icustomergroupar', TRUE);
			$ecustomername		= $this->input->post('ecustomername', TRUE);
			$isalesman		= $this->input->post('isalesman', TRUE);
			$esalesmanname		= $this->input->post('esalesmanname', TRUE);
			$eremark		= $this->input->post('eremark', TRUE);
			$vjumlah		= $this->input->post('vjumlah', TRUE);
			$vjumlah		= str_replace(',','',$vjumlah);
			$vsisa			= $this->input->post('vsisa', TRUE);
			$vsisa			= str_replace(',','',$vsisa);
			if (
				($ikum != '') && ($iarea!='') && ($tahun!='')
			   )
			{
				$this->load->model('transferuangmasuk/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($ikum,$xkum,$dkum,$xtahun,$tahun,$ebankname,$iarea,$icustomer,$icustomergroupar,
									   $isalesman,$eremark,$vjumlah,$vsisa,$iareaasal);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update Transfer Uang Masuk Area '.$iarea.' No:'.$ikum;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ikum;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikum 	= $this->input->post('ikum', TRUE);
			$dkum	= $this->input->post('dkum', TRUE);
			if($dkum!=''){
				$tmp=explode("-",$dkum);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkum=$th."-".$bl."-".$hr;
				$tahun=$th;
			}
			$ibank    		= $this->input->post('ibank', TRUE);
			$ebankname		= $this->input->post('ebankname', TRUE);
			$iarea			  = $this->input->post('iarea', TRUE);
			$eareaname		= $this->input->post('eareaname', TRUE);
			$icustomer		= $this->input->post('icustomer', TRUE);
			$icustomergroupar	= $this->input->post('icustomergroupar', TRUE);
			$ecustomername		= $this->input->post('ecustomername', TRUE);
			$isalesman		= $this->input->post('isalesman', TRUE);
			$esalesmanname		= $this->input->post('esalesmanname', TRUE);
			$eremark		= $this->input->post('eremark', TRUE);
			$vjumlah		= $this->input->post('vjumlah', TRUE);
			$vjumlah		= str_replace(',','',$vjumlah);
			$vsisa			= $this->input->post('vsisa', TRUE);
			$vsisa			= str_replace(',','',$vsisa);
			if (
				($ikum != '') && ($iarea!='') && ($tahun!='') && ($ibank!='')
			   )
			{
				$this->load->model('transferuangmasuk/mmaster');
				$this->db->trans_begin();
				$cek=$this->mmaster->cek($iarea,$ikum,$tahun);
				if(!$cek){							
					$this->mmaster->insert($ikum,$dkum,$tahun,$ebankname,$iarea,$icustomer,$icustomergroupar,
										             $isalesman,$eremark,$vjumlah,$vsisa,$ibank);
				}else{
					$ikum="Bukti transfer ".$ikum." sudah ada, untuk mengedit lewat menu edit Transfer";
				}
				if( ($this->db->trans_status() === FALSE) && ($cek) )
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input KU Area '.$iarea.' No:'.$ikum;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ikum;
          if(substr($ikum,0,5)!='Bukti'){
					  $this->load->view('nomor',$data);
          }else{
					  $this->load->view('error',$data);
          }
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transferuangmasuk/cform/area/index/';
			$config['per_page'] = '10';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transferuangmasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('transferuangmasuk/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transferuangmasuk/cform/area/index/';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                              where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transferuangmasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('transferuangmasuk/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/transferuangmasuk/cform/customer/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select distinct on (a.i_customer, c.i_salesman) a.i_customer
			              from tr_customer a
										left join tr_customer_groupar b on(a.i_customer=b.i_customer)
										left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
										where a.i_area = '$iarea'
										and a.f_customer_aktif = 't' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('transferuangmasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
      $data['cari']='';
			$this->load->view('transferuangmasuk/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 	= $this->uri->segment(4);
			$cari 	= strtoupper($this->input->post('cari', FALSE));
      if( ($cari=='') && ($this->uri->segment(6)!='') )$cari=$this->uri->segment(5);
      $cari=str_replace("%20"," ",$cari);
      if($cari!='')
  			$config['base_url'] = base_url().'index.php/transferuangmasuk/cform/caricustomer/'.$iarea.'/'.$cari.'/';
      else
  			$config['base_url'] = base_url().'index.php/transferuangmasuk/cform/caricustomer/'.$iarea.'/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select distinct on (a.i_customer, c.i_salesman) a.i_customer
			              from tr_customer a
										left join tr_customer_groupar b on(a.i_customer=b.i_customer)
										left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
										where a.i_area = '$iarea' and ( 
									upper(a.i_customer) like '%$cari%' or 
									upper(a.e_customer_name) like '%$cari%' )",false);
			$config['total_rows'] = $query->num_rows();
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
      if($cari!='')
  			$config['cur_page'] = $this->uri->segment(6);
      else
   			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transferuangmasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
      if($cari!='')
  			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
      else
  			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
      $data['cari']=$cari;
			$this->load->view('transferuangmasuk/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bank()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transferuangmasuk/cform/bank/index/';
			$query = $this->db->query("select * from tr_bank",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transferuangmasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_bank');
			$data['isi']=$this->mmaster->bacabank($config['per_page'],$this->uri->segment(5));
			$this->load->view('transferuangmasuk/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caribank()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transferuangmasuk/cform/bank/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query("select * from tr_bank where (upper(i_bank) like '%$cari%' or upper(e_bank_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transferuangmasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_bank');
			$data['isi']=$this->mmaster->caribank($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('transferuangmasuk/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
