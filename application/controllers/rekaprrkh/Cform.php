<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu402')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/rekaprrkh/cform/index/';
			$data['page_title'] = $this->lang->line('rekaprrkh');
			$this->load->model('rekaprrkh/mmaster');
			$data['iperiode']='';
			$data['isi']='';
			$data['cari'] = '';
			$this->load->view('rekaprrkh/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu402')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari			= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$query = $this->db->query(" select a.i_salesman, b.e_salesman_name, a.n_jumlah_kunjungan, a.n_jumlah_order,
                                  ((a.n_jumlah_order/a.n_jumlah_kunjungan)*100) as hasil,
                                  trunc(a.n_jumlah_kunjungan/a.n_jumlah_hari) as rata,
                                  (a.n_jumlah_order/a.n_jumlah_hari) as efektif
                                  from tm_kunjungan a, tr_salesman b
                                  where a.i_salesman=b.i_salesman
                                  and a.i_periode='$iperiode'
                                  order by a.i_area, a.i_salesman ",false);
			$config['base_url'] = base_url().'index.php/rekaprrkh/cform/view/'.$iperiode.'/';
			//$config['total_rows'] = $query->num_rows(); 
			//$config['per_page'] = '10';
			//$config['first_link'] = 'Awal';
			//$config['last_link'] = 'Akhir';
			//$config['next_link'] = 'Selanjutnya';
			//$config['prev_link'] = 'Sebelumnya';
			//$config['cur_page'] = $this->uri->segment(5);
			//$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('rekaprrkh');
			$this->load->model('rekaprrkh/mmaster');
			//$data['isi']=$this->mmaster->bacasemua($iperiode,$cari,$config['per_page'],$this->uri->segment(5));
      $data['isi']=$this->mmaster->bacasemua($iperiode,$cari);
			$data['cari'] = $cari;
			$data['iperiode'] = $iperiode;
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Rekap RRKH Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('rekaprrkh/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu402')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('rekaprrkh');
			$this->load->view('rekaprrkh/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu402')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
      $iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/rekaprrkh/cform/view/'.$iperiode.'/';
      $query = $this->db->query(" select a.i_salesman, b.e_salesman_name, a.n_jumlah_kunjungan, a.n_jumlah_order,
                                  ((a.n_jumlah_order/a.n_jumlah_kunjungan)*100) as hasil,
                                  trunc(a.n_jumlah_kunjungan/a.n_jumlah_hari) as rata,
                                  (a.n_jumlah_order/a.n_jumlah_hari) as efektif
                                  from tm_kunjungan a, tr_salesman b
                                  where a.i_salesman=b.i_salesman
                                  and a.i_periode='$iperiode'
                                  order by a.i_area, a.i_salesman",false);
			//$config['total_rows'] = $query->num_rows(); 
			//$config['per_page'] = '10';
			//$config['first_link'] = 'Awal';
			//$config['last_link'] = 'Akhir';
			//$config['next_link'] = 'Selanjutnya';
			//$config['prev_link'] = 'Sebelumnya';
			//$config['cur_page'] = $this->uri->segment(5);
			//$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('rekaprrkh');
			$this->load->model('rekaprrkh/mmaster');
			//$data['isi']=$this->mmaster->cari($iperiode,$cari,$config['per_page'],$this->uri->segment(5));
      $data['isi']=$this->mmaster->cari($iperiode,$cari);
			$data['cari'] = $cari;
			$data['iperiode'] = $iperiode;
			$this->load->view('rekaprrkh/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detailkunjungan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu402')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $iperiode=$this->uri->segment(4);
      $isalesman=$this->uri->segment(5);
			$this->load->model('rekaprrkh/mmaster');
			$data['page_title'] = $this->lang->line('rekaprrkh');
			$data['isi']=$this->mmaster->bacadetailkunjungan($iperiode,$isalesman);
			$this->load->view('rekaprrkh/vdetailkunjungan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	/*function detailorder()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu402')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $iperiode=$this->uri->segment(4);
      $isalesman=$this->uri->segment(5);
			$this->load->model('rekaprrkh/mmaster');
			$data['page_title'] = $this->lang->line('rekaprrkh');
			$data['isi']=$this->mmaster->bacadetailorder($iperiode,$isalesman);
			$this->load->view('rekaprrkh/vdetailorder', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}*/
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu402')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('xcari'));
			$iperiode	= $this->input->post('xiperiode');
			$is_cari= $this->input->post('xis_cari');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			if ($is_cari == '')
				$is_cari= $this->uri->segment(6);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(5);

      $a=substr($iperiode,0,4);
	    $b=substr($iperiode,4,2);
		  $peri=mbulan($b)." - ".$a;

			$this->load->model('rekaprrkh/mmaster');
			$data['page_title'] = $this->lang->line('rekaprrkh');
			$data['cari']	= $cari;
			$data['iperiode'] = $iperiode;

      $this->db->select("	a.i_area, c.e_area_name, a.i_salesman, b.e_salesman_name, a.n_jumlah_kunjungan, a.n_jumlah_order,
                          ((a.n_jumlah_order/a.n_jumlah_kunjungan)*100) as hasil,
                          trunc(a.n_jumlah_kunjungan/a.n_jumlah_hari) as rata,
                          (a.n_jumlah_order/a.n_jumlah_hari) as efektif
                          from tm_kunjungan a, tr_salesman b, tr_area c
                          where a.i_salesman=b.i_salesman
                          and a.i_area=c.i_area and a.i_periode='$iperiode'
                          and (upper(a.i_salesman) like '%$cari%' or upper(b.e_salesman_name) like '%$cari%')
                          order by a.i_area, a.i_salesman",false);
      $query = $this->db->get();
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$queryy 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($queryy)){
	    	$now	  = $row['c'];
			}
			$pesan='Export Rekap RRKH Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
      $this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("REKAP RRKH")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      if ($query->num_rows() > 0){
     		$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'REKAP RRKH Periode '.$iperi);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,9,2);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:J6'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Jml Kunj.');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Jml Order');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Hsl Kunj. %');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Rata Kunj/Hari');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Effektif Kunj. %');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        $i=7;
				$j=7;
        $no=0;
				foreach($query->result() as $row){
          $no++;
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':I'.$i
				  );

					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, '('.$row->i_area.') '.$row->e_area_name);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->i_salesman, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->e_salesman_name);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->n_jumlah_kunjungan, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->n_jumlah_order, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->hasil, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->rata, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->efektif, Cell_DataType::TYPE_NUMERIC);
					$i++;
				}
			}
			//$objPHPExcel->getActiveSheet()->getStyle('E7:I'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='RekapRRKH'.$iperiode.'.xls';
      if(file_exists('spb/00/'.$nama)){
        @chmod('spb/00/'.$nama, 0777);
        @unlink('spb/00/'.$nama);
      }
			$objWriter->save("spb/00/".$nama); 
      @chmod('spb/00/'.$nama, 0777);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
