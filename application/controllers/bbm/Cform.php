<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title']	= $this->lang->line('bbm');
			$data['ibbm']		= '';
			$this->load->model('bbm/mmaster');
			$data['isi']		= "";
			$data['detail']		= "";
			$data['jmlitem']	= "";
			$this->load->view('bbm/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan(){
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iap 	= $this->input->post('iap', TRUE);
			$dap 	= $this->input->post('dap', TRUE);
			if($dap!=''){
				$tmp=explode("-",$dap);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dap=$th."-".$bl."-".$hr;
			}
			$isupplier		= $this->input->post('isupplier', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$iop			= $this->input->post('iop', TRUE);
			$vapgross		= $this->input->post('vapgross',TRUE);
			$vapgross		= str_replace(',','',$vapgross);
			$jml			= $this->input->post('jml', TRUE);
			if(
				($iap=='')
				&& ($isupplier!='')
				&& (($vapgross!='') || ($vapgross!='0'))
				&& ($iop!='')
				&& ($dap!='')
			  )
			{
				$this->db->trans_begin();
				$this->load->model('bbm/mmaster');
				$iap=$this->mmaster->runningnumber();
				$query=$this->db->query("select * from tm_ap where i_ap='$iap' and i_supplier='$isupplier'");
				if($query->num_rows()==0){
					$this->mmaster->insertheader($iap,$isupplier,$iop,$iarea,$dap,$vapgross);
					for($i=1;$i<=$jml;$i++){
					  $iproduct					= $this->input->post('iproduct'.$i, TRUE);
						$iproductgrade		= 'A';
						$iproductmotif		= $this->input->post('motif'.$i, TRUE);
						$eproductname			= $this->input->post('eproductname'.$i, TRUE);
						$vproductmill			= $this->input->post('vproductmill'.$i, TRUE);
						$vproductmill			= str_replace(',','',$vproductmill);
						$nreceive					= $this->input->post('nreceive'.$i, TRUE);
						$this->mmaster->insertdetail($iap,$isupplier,$iproduct,$iproductgrade,$iproductmotif,$eproductname,$nreceive,$vproductmill);
					}
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();

            $sess=$this->session->userdata('session_id');
            $id=$this->session->userdata('user_id');
            $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs		= pg_query($sql);
            if(pg_num_rows($rs)>0){
              while($row=pg_fetch_assoc($rs)){
                $ip_address	  = $row['ip_address'];
                break;
              }
            }else{
              $ip_address='kosong';
            }
            $query 	= pg_query("SELECT current_timestamp as c");
            while($row=pg_fetch_assoc($query)){
              $now	  = $row['c'];
            }
            $pesan='Input BBM no:'.$iap;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now , $pesan );

						$data['inomor']			= $iap;
						$this->load->view('nomor',$data);
					}
				}else{
					
				}
			}else{
				$this->load->view('awal/index.php');
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('bbm');
			$this->load->view('bbm/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('bbm')." update";
			if(
				($this->input->post('iapedit')) && ($this->input->post('isupplieredit'))
			  ){
				$iap				= $this->input->post('iapedit');
				$isupplier			= $this->input->post('isupplieredit');
				$query 				= $this->db->query("select * from tm_ap_item where i_ap = '$iap' ");//and i_supplier='$isupplier'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['iap'] 		= $iap;
				$data['isupplier']	= $isupplier;
				$this->load->model('bbm/mmaster');
				$data['isi']		= $this->mmaster->baca($iap,$isupplier);
				$data['detail']		= $this->mmaster->bacadetail($iap, $isupplier);
		 		$this->load->view('bbm/vmainform',$data);
			}else{
				$this->load->view('bbm/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if ($this->session->userdata('logged_in')){
			$iap 	= $this->input->post('iap', TRUE);
			$dap 	= $this->input->post('dap', TRUE);
			if($dap!=''){
				$tmp=explode("-",$dap);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dap=$th."-".$bl."-".$hr;
			}
			$isupplier		= $this->input->post('isupplier', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$iop			= $this->input->post('iop', TRUE);
			$vapgross		= $this->input->post('vapgross',TRUE);
			$vapgross		= str_replace(',','',$vapgross);
			$jml			= $this->input->post('jml', TRUE);
			if(
				($iap!='')
				&& ($isupplier!='')
				&& (($vapgross!='') || ($vapgross!='0'))
				&& ($iop!='')
				&& ($dap!='')
			  )
			{
				$this->db->trans_begin();
				$this->load->model('bbm/mmaster');
				$this->mmaster->updateheader($iap,$isupplier,$iop,$iarea,$dap,$vapgross);
				for($i=1;$i<=$jml;$i++){
					$iproduct					= $this->input->post('iproduct'.$i, TRUE);
					$iproductgrade				= 'A';
					$iproductmotif				= $this->input->post('motif'.$i, TRUE);
					$eproductname				= $this->input->post('eproductname'.$i, TRUE);
					$vproductmill				= $this->input->post('vproductmill'.$i, TRUE);
					$vproductmill				= str_replace(',','',$vproductmill);
					$nreceive					= $this->input->post('nreceive'.$i, TRUE);
					$this->mmaster->deletedetail($iproduct, $iproductgrade, $iap, $isupplier, $iproductmotif);
					$this->mmaster->insertdetail($iap,$isupplier,$iproduct,$iproductgrade,$iproductmotif,$eproductname,
												 $nreceive,$vproductmill);
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
			    $this->db->trans_rollback();
				}else{
			    $this->db->trans_commit();

          $sess=$this->session->userdata('session_id');
          $id=$this->session->userdata('user_id');
          $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
          $rs		= pg_query($sql);
          if(pg_num_rows($rs)>0){
            while($row=pg_fetch_assoc($rs)){
              $ip_address	  = $row['ip_address'];
              break;
            }
          }else{
            $ip_address='kosong';
          }
          $query 	= pg_query("SELECT current_timestamp as c");
          while($row=pg_fetch_assoc($query)){
            $now	  = $row['c'];
          }
          $pesan='Update BBM no :'.$iap;
          $this->load->model('logger');
          $this->logger->write($id, $ip_address, $now , $pesan );

					$data['inomor']			= $iap;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if ($this->session->userdata('logged_in')){
			$iap		= $this->input->post('iapdelete', TRUE);
			$isupplier	= $this->input->post('isupplierdelete', TRUE);
			$iop		= $this->input->post('iopdelete', TRUE);
			$this->load->model('bbm/mmaster');
			$this->mmaster->delete($iap,$isupplier,$iop);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
        while($row=pg_fetch_assoc($rs)){
          $ip_address	  = $row['ip_address'];
          break;
        }
      }else{
        $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
        $now	  = $row['c'];
      }
      $pesan='Delete BBM No:'.$iap;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title']	= $this->lang->line('bbm');
			$data['iap']		= '';
			$data['isupplier']	= '';
			$data['jmlitem']	= '';
			$data['detail']		= '';
			$data['isi']		= '';//$this->mmaster->bacasemua();
			$this->load->view('bbm/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if ($this->session->userdata('logged_in')){
			$iap			= $this->input->post('iapdelete', TRUE);
			$isupplier		= $this->input->post('isupplierdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
  		$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$vapgross		= $this->uri->segment(4);
			$iop			= $this->input->post('iop', TRUE);
			$iarea			= $this->input->post('iarea',TRUE);
			$dap			= $this->input->post('dap',TRUE);
			if($dap!=''){
				$tmp	= explode("-",$dap);
				$th		= $tmp[2];
				$bl		= $tmp[1];
				$hr		= $tmp[0];
				$dap	= $th."-".$bl."-".$hr;
			}
			$this->db->trans_begin();
			$this->load->model('bbm/mmaster');
//			$this->mmaster->uphead($iap,$isupplier,$iop,$iarea,$dap,$vapgross);
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $iap, $isupplier, $iproductmotif);
			if ($this->db->trans_status() === FALSE)
			{
		    $this->db->trans_rollback();
			}else{
		    $this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
          while($row=pg_fetch_assoc($rs)){
            $ip_address	  = $row['ip_address'];
            break;
          }
        }else{
          $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
          $now	  = $row['c'];
        }
        $pesan='Delete Item BBM no:'.$iap;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

				$data['page_title'] = $this->lang->line('bbm')." Update";
				$query 				= $this->db->query("select * from tm_ap_item  where i_ap = '$iap'");//and i_supplier='$isupplier'
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['iap'] 		= $iap;
				$data['isupplier']	= $isupplier;
				$data['isi']		= $this->mmaster->baca($iap,$isupplier);
				$data['detail']		= $this->mmaster->bacadetail($iap,$isupplier);
				$this->load->view('bbm/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if ($this->session->userdata('logged_in')){
			$data['baris']		= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$op					= $this->uri->segment(5);
			$data['op']			= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/bbm/cform/product/'.$baris.'/'.$op.'/index/';
			$query = $this->db->query(" select a.i_product as kode
									from tr_product_motif a, tm_op_item b, tr_product c 
									where a.i_product=c.i_product 
									and b.i_op='$op' and b.i_product=a.i_product 
									and b.i_product_motif=a.i_product_motif" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('bbm/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($op,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$this->load->view('bbm/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if ($this->session->userdata('logged_in')){
			$data['baris']		= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bbm/cform/product/'.$baris.'/index/';
			$cari 				= $this->input->post('cari', FALSE);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
									   	and (a.i_product like '%$cari%' or c.e_product_name like '%$cari%') "
									  ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('bbm/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('bbm/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if ($this->session->userdata('logged_in')){
			$data['baris']		= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$op					= $this->uri->segment(5);
			$data['op']			= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/bbm/cform/product/'.$baris.'/'.$op.'/index/';
			$query = $this->db->query(" select a.i_product as kode
									from tr_product_motif a, tm_op_item b, tr_product c 
									where a.i_product=c.i_product 
									and b.i_op='$op' and b.i_product=a.i_product 
									and b.i_product_motif=a.i_product_motif" ,false);			
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('bbm/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']		=$this->mmaster->bacaproduct($op,$config['per_page'],$this->uri->segment(7));
			$this->load->view('bbm/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductupdate()
	{
		if ($this->session->userdata('logged_in')){
			$baris				= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bbm/cform/productupdate/index/';
			$cari 				= $this->input->post('cari', FALSE);
			$query 	= $this->db->query("	select a.i_product||a.i_product_motif as kode
										from tr_product_motif a, tr_product c
										where a.i_product=c.i_product
										and (a.i_product like '%$cari%' or c.e_product_name like '%$cari%')"
										,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbm/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']		=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(5));
			$data['baris']		=$baris;
			$this->load->view('bbm/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/bbm/cform/supplier/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_supplier where i_supplier_group='G0000' ",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbm/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']		=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('bbm/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/bbm/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$query = $this->db->query("	select * from tr_supplier where i_supplier like '%$cari%' 
						      	  		or e_supplier_name like '%$cari%') ",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbm/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']		=$this->mmaster->carisupplier($cari, $config['per_page'],$this->uri->segment(5));
			$data['iarea']		=$iarea;
			$this->load->view('bbm/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/bbm/cform/index/';
			$query=$this->db->query("select * from tm_ap",false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(4);
			$this->pagination->initialize($config);
			$cari 				= $this->input->post('cari', FALSE);
			$this->load->model('bbm/mmaster');
			$data['isi']		= $this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title']	= $this->lang->line('bbm');
			$data['iap']		= '';
			$data['isupplier']	= '';
			$data['jmlitem']	= '';
			$data['detail']		= '';
	 		$this->load->view('bbm/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function op()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/bbm/cform/op/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tm_op where f_op_close='f' and f_op_cancel='f' and substr(i_reff,1,4)='SPMB'",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbm/mmaster');
			$data['page_title'] = $this->lang->line('list_op');
			$data['isi']		=$this->mmaster->bacaop($config['per_page'],$this->uri->segment(5));
			$this->load->view('bbm/vlistop', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariop()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/bbm/cform/op/index/';
			$cari = $this->input->post('cari', FALSE);
			$query = $this->db->query("	select * from tm_op 
										where (i_op like '%$cari%' or i_supplier like '%$cari%' or e_supplier_name like '%$cari%') 
										and f_op_close='f' and f_op_cancel='f' ",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbm/mmaster');
			$data['page_title']	= $this->lang->line('list_op');
			$data['isi']		= $this->mmaster->carisupplier($cari, $config['per_page'],$this->uri->segment(5));
			$data['iarea']		= $iarea;
			$this->load->view('bbm/vlistop', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
