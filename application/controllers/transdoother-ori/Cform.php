<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
    $this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu65')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transdo');
			$data['isi']= directory_map('./data/');
			$data['file']='';
			$this->load->view('transdoother/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu65')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari  = strtoupper($this->input->post('cari', FALSE));
      if( ($this->uri->segment(4)=='pqrst') || ($this->uri->segment(4)=='') ){
        $config['base_url'] = base_url().'index.php/transdoother/cform/supplier/pqrst/';        
      }else{
        if($cari=='') $cari=$this->uri->segment(4);
        $config['base_url'] = base_url().'index.php/transdoother/cform/supplier/'.$cari.'/';
      }
      $query 	= $this->db->query("select i_supplier from tr_supplier
                           where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transdoother/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('transdoother/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu65')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$file = $this->input->post('namafile', TRUE);
			$data['isupplier'] = $this->input->post('isupplier', TRUE);
			$data['file'] = './data/'.$file;
			$this->load->view('transdoother/vfile', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu65')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('transdoother/mmaster');
      $file= $this->input->post('xfile', TRUE);
      $isupplier  		= $this->input->post('xsupplier');
  		$jml= $this->input->post('jml', TRUE);
			$idox			      = '';
			$isupplierx	  	= '';
			$data['jml']	  = $jml;
			$data['kosong']	= 0;
			$tmp[0]	    		= null;
			$istore			  	= 'AA';
			$istorelocation	= '01';
			$istorelocationbin= '00';
			$eremark	  		= 'DO Transfer';
			for($i=0;$i<$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
        $ido=$this->input->post('nodok'.$i);
				if($cek!=''){
          $db1 =dbase_open($file,0) or die ("Could not open dbf file <i>$file</i>.");
          if($db1){
            $num=dbase_numrecords($db1);
            $noitem=0;
            for($j=1;$j<=$num;$j++){
              $col=dbase_get_record_with_names($db1,$j);
              if( (!$col['deleted']) && (trim($col['NOOP'])!='') ) {
                if(trim($col['NODOK'])==trim($ido)){
                  if($isupplier=='SP002'){
                    $ido  = $col['NODOK'];
                    $iop  = substr($col['NOOP'],0,6);
                    $iarea= substr($col['NOOP'],8,2);
					          $ddo	= substr($col['TGLDOK'],0,4).'-'.substr($col['TGLDOK'],4,2).'-'.substr($col['TGLDOK'],6,2);
					          $iproduct				=substr($col['KODEPROD'],0,7);
					          $iproductgrade	='A';
					          $iproductmotif	=substr($col['KODEPROD'],7,2);
					          $vproductmill	 	=$col['HARGASAT'];
					          $jumlah					=$col['JUMLAH'];
                  }elseif($isupplier=='SP004'){
                    $ido  = $col['NODOK'];
                    if(strlen(trim($col['NOOP']))==5){
                      $iop='0'.trim($col['NOOP']);
                    }else{
                      $iop=$col['NOOP'];
                    }
                    $iarea= $col['WILA'];
					          $ddo	= substr($col['TGLDOK'],0,4).'-'.substr($col['TGLDOK'],4,2).'-'.substr($col['TGLDOK'],6,2);
					          $iproduct				= substr($col['KODEPROD'],0,7);
					          $iproductgrade	= 'A';
					          $iproductmotif	= substr($col['KODEPROD'],7,2);

					          $vproductmill	 	= $col['HARGASAT'];
					          $jumlah					= $col['JUMLAH'];
                  }else{

                  }
                  $que=$this->db->query("select e_product_name 
                                         from tr_product where i_product='$iproduct'", false);
                  if($que->num_rows()>0){
                    foreach($que->result() as $trp){
                      $eproductname=$trp->e_product_name;
                    }
                  }else{
                    $eproductname='';
                  }
				          $this->mmaster->inserttmp( $ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
												            $vproductmill,$iproductmotif,$isupplier,$ddo,$iarea,$iop);
                }
              }
            }
            dbase_close ($db1);
          }
				}
			}
			$this->db->trans_begin();
			$dotmp	= '';
			$xx		= 0;
			$yy		= 0;
			for($i=0;$i<$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
        $nodok=$this->input->post('nodok'.$i);
				if($cek!=''){
          $db1 =dbase_open($file,0) or die ("Could not open dbf file <i>$file</i>.");
          if($db1){
            $num=dbase_numrecords($db1);
            $noitem=0;
            for($j=1;$j<=$num;$j++){
              $col=dbase_get_record_with_names($db1,$j);
              if( (!$col['deleted']) && (trim($col['NOOP'])!='') ) {
#                echo $col['NODOK'].'='.$nodok.'<br>';
                if(trim($col['NODOK'])==trim($nodok)){
                  if($isupplier=='SP002'){
                    $nodok= $col['NODOK'];
                    $iop  = substr($col['NOOP'],0,6);
                    $iarea= substr($col['NOOP'],8,2);
					          $ddo	= substr($col['TGLDOK'],0,4).'-'.substr($col['TGLDOK'],4,2).'-'.substr($col['TGLDOK'],6,2);
					          $iproduct				=substr($col['KODEPROD'],0,7);
					          $iproductgrade	='A';
					          $iproductmotif	=substr($col['KODEPROD'],7,2);
					          $vproductmill	 	=$col['HARGASAT'];
					          $jumlah					=$col['JUMLAH'];
					          $thbl=substr($col['TGLDOK'],2,2).substr($col['TGLDOK'],4,2);
					          $ido='DO-'.$thbl.'-IM'.substr($nodok,2,4);
                  }else{

                  }
					        $query= $this->db->query("select e_product_name from tr_product where i_product='$iproduct'");
					        foreach($query->result() as $riw){
						        $eproductname	=$riw->e_product_name;
					        }
					        $adaop	= $this->mmaster->cekadaop($iop,$iproduct,$iproductmotif,$iproductgrade,$iarea);
					        $adaitem=1;
					        if($adaop!=0){
						        $adaitem= $this->mmaster->cekdataitem($ido,$isupplier,$iproduct,$iproductmotif,$iproductgrade,
                                                          $thbl);
						        if($adaitem==0){
							        $sudahada=false;
							        $ono= $this->mmaster->cekdata($ido,$isupplier,$thbl);
							        if( ($dotmp!= $ido) && ($ono==0) ){
								        $dotmp= $ido;
								        $query= $this->db->query("select sum(v_product_mill) as v_product_mill, i_do, i_supplier, d_do, 
                                      i_area, i_op
														          from tt_do where i_do='$ido' and i_supplier='$isupplier'
														          group by i_do, i_supplier, d_do, i_area, i_op ", false);
								        foreach($query->result() as $row){
									        $qq= $this->db->query("select i_op from tm_op where i_op_old='$row->i_op' 
                                                 and i_area='$iarea'", false);
									        foreach($qq->result() as $rr){
										        $re= $this->db->query("select i_product from tm_op_item where i_op='$rr->i_op' 
                                                   and i_product='$iproduct'", false);
										        if($re->num_rows() >0){
											        $ada	= $this->mmaster->cekdata($row->i_do,$row->i_supplier,$thbl);
											        $opbaru	= '';
											        $siop	= 0;
											        $saldo	= $jumlah;
											        $qy		= $this->db->query("select i_op from tm_op where i_op_old like '%$row->i_op%' 
                                                        and i_area='$iarea'", false);
											        foreach($qy->result() as $rw){
												        if($opbaru!=$rw->i_op){
													        if($ada==0){
														        if($siop==1){
															        $row->i_do=trim($row->i_do).'-A';
														        }elseif($siop==2){
															        $row->i_do=trim($row->i_do).'-B';
														        }elseif($siop==0){
															        $this->mmaster->insertheader($row->i_do,$row->i_supplier,$rw->i_op,$row->i_area,
                                                                   $row->d_do,$row->v_product_mill);
														        }
													        }else{
														        if($siop==1){
															        $qy= $this->db->query("select i_product from tm_op_item where i_op='$rw->i_op' 
                                                             and i_product='$iproduct'", false);
															        if($qy->num_rows() >0){
															        }
														        }elseif($siop==2){
															        $qy= $this->db->query("select i_product from tm_op_item where i_op='$rw->i_op' 
                                                             and i_product='$iproduct'", false);
															        if($qy->num_rows() >0){
															        }
														        }else{
															        $this->mmaster->updateheader($row->i_do,$row->i_supplier,$rw->i_op,$row->i_area,
                                                                   $row->d_do,$row->v_product_mill);
														        }
													        }
													        $opbaru=$rw->i_op;
													        $siop++;
												        }
												        $qy= $this->db->query("select i_product, n_order from tm_op_item where i_op='$opbaru' 
                                                       and i_product='$iproduct'", false);
												        if($qy->num_rows() >0){
													        foreach($qy->result() as $rwz){
														        $saldo=$saldo-$rwz->n_order;
														        if($saldo>=0){
																        $this->mmaster->insertdetail($opbaru,$ido,$iproduct,$iproductgrade,
                                                                     $eproductname,
                                                                     $jumlah,$vproductmill,$iproductmotif,$isupplier);
															        $this->mmaster->updateopdetail($opbaru,$iproduct,$iproductgrade,$iproductmotif,
                                                                     $jumlah);
															        $this->mmaster->updatespbdetail($opbaru,$iproduct,$iproductgrade,$iproductmotif,
                                                                      $jumlah);
															        $trans=$this->mmaster->lasttrans(	$iproduct,$iproductgrade,$iproductmotif,$istore,
                                                                        $istorelocation,$istorelocationbin);
															        if(isset($trans)){
																        foreach($trans as $itrans)
																        {
																	        $q_aw =$itrans->n_quantity_awal;
																	        $q_ak =$itrans->n_quantity_akhir;
																	        $q_in =$itrans->n_quantity_in;
																	        $q_out=$itrans->n_quantity_out;
																	        break;
																        }
															        }else{
																        $trans=$this->mmaster->qic( $iproduct,$iproductgrade,$iproductmotif,$istore,
                                                                    $istorelocation,$istorelocationbin);
																        if(isset($trans)){
																	        foreach($trans as $itrans)
																	        {
																		        $q_aw =$itrans->n_quantity_stock;
																		        $q_ak =$itrans->n_quantity_stock;
																		        $q_in =0;
																		        $q_out=0;
																		        break;
																	        }
																        }else{
																	        $q_aw=0;
																	        $q_ak=0;
																	        $q_in=0;
																	        $q_out=0;
																        }
															        }
															        $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                                   $istorelocation,$istorelocationbin,$eproductname,
                                                                   $ido,
                                                                   $q_in,$q_out,$jumlah,$q_aw,$q_ak);
															        $th=substr($ddo,0,4);
															        $bl=substr($ddo,5,2);
															        $emutasiperiode=$th.$bl;
															        if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                                   $istorelocation,$istorelocationbin,$emutasiperiode))
															        {
																        $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                                      $istorelocation,
																														          $istorelocationbin,$jumlah,$emutasiperiode);
															        }else{
																        $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                                      $istorelocation,
																															        $istorelocationbin,$jumlah,$emutasiperiode);
															        }
															        if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                               $istorelocation,
																											         $istorelocationbin))
															        {
																        $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                                  $istorelocation,
																													        $istorelocationbin,$jumlah,$q_ak);
															        }else{
																        $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,    
                                                                  $istorelocation,
																													        $istorelocationbin,$eproductname,$jumlah);
															        }
														        }
													        }
													        $sudahada=true;
											          }
										          }
									          }
								          }
							          }
						          }
							        if(!$sudahada){
								        if($ono>0){
									        $opbaru= $this->mmaster->cekono($iop,$iarea);
									        $saldo=$jumlah;
									        $qy= $this->db->query("select i_product, n_order from tm_op_item where i_op='$opbaru' 
                                                 and i_product='$iproduct'", false);
									        if($qy->num_rows() >0){
										        foreach($qy->result() as $rwz){
											        $saldo=$saldo-$rwz->n_order;
											        if($saldo>=0){
												        $this->mmaster->insertdetail($opbaru,$ido,$iproduct,$iproductgrade,$eproductname, 
                                                             $jumlah,$vproductmill,$iproductmotif,$isupplier);
												        $this->mmaster->updateopdetail($opbaru,$iproduct,$iproductgrade,$iproductmotif,$jumlah);
												        $this->mmaster->updatespbdetail($opbaru,$iproduct,$iproductgrade,$iproductmotif,
                                                                $jumlah);
												        $trans=$this->mmaster->lasttrans(	$iproduct,$iproductgrade,$iproductmotif,$istore,
                                                                  $istorelocation,$istorelocationbin);
												        if(isset($trans)){
													        foreach($trans as $itrans)
													        {
														        $q_aw =$itrans->n_quantity_awal;
														        $q_ak =$itrans->n_quantity_akhir;
														        $q_in =$itrans->n_quantity_in;
														        $q_out=$itrans->n_quantity_out;
														        break;
													        }
												        }else{
													        $trans=$this->mmaster->qic( $iproduct,$iproductgrade,$iproductmotif,$istore,
                                                              $istorelocation,$istorelocationbin);
													        if(isset($trans)){
														        foreach($trans as $itrans)
														        {
															        $q_aw =$itrans->n_quantity_stock;
															        $q_ak =$itrans->n_quantity_stock;
															        $q_in =0;
															        $q_out=0;
															        break;
														        }
													        }else{
														        $q_aw=0;
														        $q_ak=0;
														        $q_in=0;
														        $q_out=0;
													        }
												        }
												        $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                             $istorelocation,$istorelocationbin,$eproductname,$ido,
                                                             $q_in,$q_out,$jumlah,$q_aw,$q_ak);
												        $th=substr($ddo,0,4);
												        $bl=substr($ddo,5,2);
												        $emutasiperiode=$th.$bl;
												        if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                             $istorelocation,$istorelocationbin,$emutasiperiode))
												        {
													        $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                                $istorelocation,
																											          $istorelocationbin,$jumlah,$emutasiperiode);
												        }else{
													        $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                                $istorelocation,
																												        $istorelocationbin,$jumlah,$emutasiperiode);
												        }
												        if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                         $istorelocation,$istorelocationbin))
												        {
													        $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                            $istorelocation,
																										        $istorelocationbin,$jumlah,$q_ak);
												        }else{
													        $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                            $istorelocation,
																										        $istorelocationbin,$eproductname,$jumlah);
												        }
											        }
										        }
									        }
								        }else{
									        $opbaru= $this->mmaster->cekono($iop,$iarea);
									        $this->mmaster->insertdetail($opbaru,$ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
																	         $vproductmill,$iproductmotif,$isupplier);
									        $this->mmaster->updateopdetail($opbaru,$iproduct,$iproductgrade,$iproductmotif,$jumlah);
									        $this->mmaster->updatespbdetail($opbaru,$iproduct,$iproductgrade,$iproductmotif,$jumlah);
									        $trans=$this->mmaster->lasttrans(	$iproduct,$iproductgrade,$iproductmotif,$istore,
                                                            $istorelocation,$istorelocationbin);
									        if(isset($trans)){
										        foreach($trans as $itrans)
										        {
											        $q_aw =$itrans->n_quantity_awal;
											        $q_ak =$itrans->n_quantity_akhir;
											        $q_in =$itrans->n_quantity_in;
											        $q_out=$itrans->n_quantity_out;
											        break;
										        }
									        }else{
										        $trans=$this->mmaster->qic( $iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																								        $istorelocationbin);
										        if(isset($trans)){
											        foreach($trans as $itrans)
											        {
												        $q_aw =$itrans->n_quantity_stock;
												        $q_ak =$itrans->n_quantity_stock;
												        $q_in =0;
												        $q_out=0;
												        break;
											        }
										        }else{
											        $q_aw=0;
											        $q_ak=0;
											        $q_in=0;
											        $q_out=0;
										        }
									        }
									        $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																							         $istorelocationbin,$eproductname,$ido,$q_in,$q_out,$jumlah,$q_aw,
                                                       $q_ak);
									        $th=substr($ddo,0,4);
									        $bl=substr($ddo,5,2);
									        $emutasiperiode=$th.$bl;
									        if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																							         $istorelocationbin,$emutasiperiode))
									        {
										        $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,
                                                          $istorelocation,
																								          $istorelocationbin,$jumlah,$emutasiperiode);
									        }else{
										        $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,  
                                                          $istorelocation,
																									        $istorelocationbin,$jumlah,$emutasiperiode);
									        }
									        if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																					         $istorelocationbin))
									        {
										        $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																							        $istorelocationbin,$jumlah,$q_ak);
									        }else{
										        $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																							        $istorelocationbin,$eproductname,$jumlah);
									        }
								        }
								        $sudahada=false;
							        }	
						        }
					        }else{
						        $yy++;
						        $xx++;
						        $xxx[$yy]		= $iop.' - '.$iproduct.$iproductmotif.' - '.$eproductname.'  -  Tidak Ada di OP';
						        $data['kosong']	= $xx;
					        }
				        }
			        }
            }
          }
        }
      }
			$data['error']=$xxx;
			$this->db->query("delete from tt_do");
			if (
				($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
				$this->load->view('transdoother/vformgagal',$data);
			}else{
				$this->db->trans_commit();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Transfer DO Other Dari File '.$file;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );
				$this->load->view('transdoother/vformsukses',$data);
			}			
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
