<?php 
ini_set('MAX_EXECUTION_TIME', -1);
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
    $this->load->helper(array('file','directory','fusioncharts'));
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('trendperitemtoko');
			$data['thn']	= '';
			$this->load->view('trendperitemtoko/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
#			$area	= $this->input->post('eareaname');
#			$icust= $this->input->post('icustomer');
#			$cust	= $this->input->post('ecustomername');
#			$todate	= $this->input->post('todate');
			//$thn	= $this->uri->segment(4);
			$thn = $this->input->post('tahun');
			$thnsebelumnya = $thn-1; 
			//$icust= $this->uri->segment(5);
			//$cust	= $this->uri->segment(6);
			//$cust=str_replace("tandakurungbuka","(",$cust);
			//$cust=str_replace("tandakurungtutup",")",$cust);
			//$cust=str_replace("%20"," ",$cust);
			//$todate	= $this->uri->segment(7);
			//if($thn!='') $todate=$this->uri->segment(4);
			/*if($todate!=''){
				$tmp=explode("-",$todate);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$todate  =$th."-".$bl."-".$hr;
				$prevth  =$th-1;
				$prevdate=$prevth."-".$bl."-".$hr;
			}*/
#			$config['base_url'] = base_url().'index.php/trendperitemtoko/cform/view/'.$todate.'/index/';
			$this->load->model('trendperitemtoko/mmaster');
			$data['page_title'] = $this->lang->line('trendperitemtoko').' - Semua Toko';
			
			//$data['todate']	= $todate;
			$data['thn']=$thn;
			$data['thnsebelumnya']=$thnsebelumnya;
			//$data['bl']=$bl;
#			$data['total']	= $this->mmaster->bacatotal($todate,$prevdate,$th,$prevth,$bl,$icust);			
			$data['isi']	= $this->mmaster->baca($thnsebelumnya,$thn);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Sales Performance todate:'.$thn;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('trendperitemtoko/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu150')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('trendperitemtoko/mmaster');
			//$thn	= $this->uri->segment(4);
			$thn = $this->input->post('tahun');
			$thnsebelumnya = $thn-1; 
			$this->db->select("	a.i_product, a.e_product_name, a.i_area, a.e_area_name, a.e_customer_name, a.i_customer, a.e_product_categoryname,  sum(a.vnota) as vnota, 
                          sum(a.qnota) as qnota ,sum(a.oa) as oa , sum(a.prevvnota) as prevvnota , sum(a.prevqnota) as prevqnota , sum(a.prevoa) as prevoa from (

SELECT b.i_product, b.e_product_name, a.i_area, f.e_area_name, c.e_customer_name, a.i_customer, e.e_product_categoryname,  sum(b.n_deliver*v_unit_price)  as vnota, 
                          0 as qnota , 0 as oa , 0 as prevvnota , 0 as prevqnota , 0 as prevoa 
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product_category e, tr_area f
                          where to_char(a.d_nota, 'yyyy') = '$thn' and a.f_nota_cancel='f' 
                          and a.i_sj=b.i_sj and a.i_area=b.i_area and f.i_area=a.i_area and f.i_area=b.i_area and a.i_customer=c.i_customer 
                          and b.i_product_category=e.i_product_category
                          group by to_char(a.d_nota, 'yyyy'), b.i_product, b.e_product_name, c.e_customer_name, a.i_customer, e.e_product_categoryname, a.i_area, f.e_area_name
union all
SELECT b.i_product, b.e_product_name, a.i_area, f.e_area_name, c.e_customer_name, a.i_customer, e.e_product_categoryname,  0  as vnota, 
                          sum(b.n_deliver)  as qnota , 0 as oa , 0 as prevvnota , 0 as prevqnota , 0 as prevoa 
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product_category e, tr_area f
                          where to_char(a.d_nota, 'yyyy') = '$thn' and a.f_nota_cancel='f' 
                          and a.i_sj=b.i_sj and a.i_area=b.i_area and f.i_area=a.i_area and f.i_area=b.i_area and a.i_customer=c.i_customer
                          and b.i_product_category=e.i_product_category
                          group by to_char(a.d_nota, 'yyyy'), b.i_product, b.e_product_name, c.e_customer_name, a.i_customer, e.e_product_categoryname, a.i_area, f.e_area_name
union all
select distinct b.i_product, b.e_product_name, a.i_area, f.e_area_name, c.e_customer_name, a.i_customer, e.e_product_categoryname,
        0  as vnota, 0  as qnota , count(a.i_customer) as oa, 0 as prevvnota , 0 as prevqnota , 0 as prevoa
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product_category e, tr_area f
                          where to_char(a.d_nota, 'yyyy') = '$thn' and a.f_nota_cancel='f' 
                          and a.i_sj=b.i_sj and a.i_area=b.i_area and f.i_area=a.i_area and a.i_customer=c.i_customer
                          and b.i_product_category=e.i_product_category
                          group by b.i_product, b.e_product_name, c.e_customer_name, a.i_customer, e.e_product_categoryname, a.i_area, f.e_area_name
--nah ini $thnsebelumnya dibawah
union all
SELECT b.i_product, b.e_product_name, a.i_area, f.e_area_name, c.e_customer_name, a.i_customer, e.e_product_categoryname,  0  as vnota, 
                          0 as qnota , 0 as oa , sum(b.n_deliver*v_unit_price) as prevvnota , 0 as prevqnota , 0 as prevoa 
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product_category e, tr_area f
                          where to_char(a.d_nota, 'yyyy') = '$thnsebelumnya' and a.f_nota_cancel='f' 
                          and a.i_sj=b.i_sj and a.i_area=b.i_area and f.i_area=a.i_area and f.i_area=b.i_area and a.i_customer=c.i_customer
                          and b.i_product_category=e.i_product_category
                          group by to_char(a.d_nota, 'yyyy'), b.i_product, b.e_product_name, c.e_customer_name, a.i_customer, e.e_product_categoryname, a.i_area, f.e_area_name
union all
SELECT b.i_product, b.e_product_name, a.i_area, f.e_area_name, c.e_customer_name, a.i_customer, e.e_product_categoryname,  0  as vnota, 
                          0  as qnota , 0 as oa , 0 as prevvnota , sum(b.n_deliver) as prevqnota , 0 as prevoa 
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product_category e, tr_area f
                          where to_char(a.d_nota, 'yyyy') = '$thnsebelumnya' and a.f_nota_cancel='f' 
                          and a.i_sj=b.i_sj and a.i_area=b.i_area and f.i_area=a.i_area and f.i_area=b.i_area and a.i_customer=c.i_customer
                          and b.i_product_category=e.i_product_category
                          group by to_char(a.d_nota, 'yyyy'), b.i_product, b.e_product_name, c.e_customer_name, a.i_customer, e.e_product_categoryname, a.i_area, f.e_area_name
union all
select distinct  b.i_product, b.e_product_name, a.i_area, f.e_area_name, c.e_customer_name, a.i_customer, e.e_product_categoryname,
        0  as vnota, 0  as qnota , 0 as oa, 0 as prevvnota , 0 as prevqnota , count(a.i_customer) as prevoa
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product_category e, tr_area f
                          where to_char(a.d_nota, 'yyyy') = '$thnsebelumnya' and a.f_nota_cancel='f' 
                          and a.i_sj=b.i_sj and a.i_area=b.i_area and f.i_area=a.i_area and a.i_customer=c.i_customer
                          and b.i_product_category=e.i_product_category
                          group by b.i_product, b.e_product_name, c.e_customer_name, a.i_customer, e.e_product_categoryname, a.i_area, f.e_area_name
                          ) as a
                          group by a.i_product, a.e_product_name, a.i_area, a.e_area_name, a.e_customer_name, a.i_customer, a.e_product_categoryname
                          order by a.i_customer, a.i_product ",false);
#			$this->db->select("	* from tm_kk 
#								          inner join tr_area on (tm_kk.i_area=tr_area.i_area)
#								          where tm_kk.d_kk >= '$datefrom' and tm_kk.d_kk <= '$dtos' 
#                          and tm_kk.i_area='$iarea' and tm_kk.f_kk_cancel='f'
#								          order by tm_kk.i_area,tm_kk.d_kk,tm_kk.i_kk",false);
			$query=$this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Trend Toko Peritem")
						->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:A4'
				);
        
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(22);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
       	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(13);  
				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Trend Toko Peritem');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,11,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Nasional ');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,11,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$thn);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,11,3);
        $objPHPExcel->getActiveSheet()->setCellValue('A6', 'Kode Produk');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Produk');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
        		$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Kode Pelanggan');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Pelanggan');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'OA'.$thnsebelumnya);
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'OA'.$thn);
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'GROWTH OA');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Sales QTY(Unit)'.$thnsebelumnya);
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Sales QTY(Unit)'.$thn);
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'GROWTH QTY');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('L6', 'NET SALES(Rp.)'.$thnsebelumnya);
				$objPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M6', 'NET SALES(Rp.)'.$thn);
				$objPHPExcel->getActiveSheet()->getStyle('M6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N6', '% GROWTH Rp');
				$objPHPExcel->getActiveSheet()->getStyle('N6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
     //   if($iarea=='00'){
				  $objPHPExcel->getActiveSheet()->setCellValue('O6', '% CTR NET SALES(RP.)');
				  $objPHPExcel->getActiveSheet()->getStyle('O6')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN),
							  'bottom'=> array('style' => Style_Border::BORDER_THIN),
							  'left'  => array('style' => Style_Border::BORDER_THIN),
							  'right' => array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
       // }
				$i=7;
				$j=7;
				$xarea='';
				$saldo=0;
				$grwoa=0;
        

				foreach($query->result() as $row){
#          $periode=substr($datefrom,0,4).substr($datefrom,5,2);
		if(($row->vnota==0) && ($row->qnota==0)){
		$totvnota=0;
        $totqnota=0;
    	$totvnota=$totvnota+0;
        $totqnota=$totqnota+0;
    	}else{
    	$totvnota=0;
        $totqnota=0;
		$totvnota=$totvnota+$row->vnota;
        $totqnota=$totqnota+$row->qnota;
		}
		$grwqty=0;
        $grwrp=0;
        $totnota=0;
        $totrp=0;
        $ctrrp=0;
        $totprevoa=0;
        $totoa=0;
        $totprevqnota=0;
        $totprevvnota=0;
        $totctrrp=0;
        $totgrwoa=0;
        $totgrwqty=0;
        $totgrwrp=0;
        $totpersenvnota=0;
        $ctrrp=0;

			 $totnota+=$row->vnota;
			 $totrp=$totnota;
		
			 //===============PROSES==================//
		
		  if($totvnota==0){
            $persenvnota=0;
          }else{
            $persenvnota=($row->vnota/$totvnota)*100;
          }
          $totpersenvnota=$totpersenvnota+$persenvnota;

          if ($row->prevoa == 0) {
              $grwoa = 0;
          } else { //jika pembagi tidak 0
              $grwoa = (($row->oa-$row->prevoa)/$row->prevoa);
          }

          if ($row->prevqnota == 0) {
              $grwqty = 0;
          } else { //jika pembagi tidak 0
              $grwqty = (($row->qnota-$row->prevqnota)/$row->prevqnota);
          }

          if ($row->prevvnota == 0) {
              $grwrp = 0;
          } else { //jika pembagi tidak 0
              $grwrp = (($row->vnota-$row->prevvnota)/$row->prevvnota);
          }

          if(($row->prevoa!=0)&&($row->oa!=0)&&($row->prevqnota!=0)&&($row->prevvnota!=0)&&($row->vnota!=0)){	
          	
          	$ctrrp= $row->vnota/$totrp;
          	$totprevoa=$totprevoa+$row->prevoa;
          	$totoa=$totoa+$row->oa;
          	$totprevqnota=$totprevqnota+$row->prevqnota;
          	$totprevvnota=$totprevvnota+$row->prevvnota;
          
          }else{

          	$totprevoa=$totprevoa+0;
          	$totoa=$totoa+0;
          	$totprevqnota=$totprevqnota+0;
          	$totprevvnota=$totprevvnota+0;

          }
          	$totctrrp=$totctrrp+$ctrrp;

          //===============PROSESII==================//

          if ($totprevoa == 0) {
              $totgrwoa = 0;
          } else { //jika pembagi tidak 0
              $totgrwoa = (($totoa-$totprevoa)/$totprevoa);
          }

          if ($totprevqnota == 0) {
              $totgrwqty = 0;
          } else { //jika pembagi tidak 0
              $totgrwqty = (($totqnota-$totprevqnota)/$totprevqnota);
          }

          if ($totprevvnota == 0) {
              $totgrwrp = 0;
          } else { //jika pembagi tidak 0
              $totgrwrp = (($totvnota-$totprevvnota)/$totprevvnota);
          }


				
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row->i_product);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->i_area.'-'.$row->e_area_name);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->i_customer);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->e_customer_name);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, number_format($row->prevoa));
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, number_format($row->oa));
					$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, number_format($grwoa,2));
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, number_format($row->prevqnota));
					$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, number_format($row->qnota));
					$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, number_format($grwqty,2));
					$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, number_format($row->prevvnota));
					$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, number_format($row->vnota));
					$objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, number_format($grwrp,2));
					$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
         // if($iarea=='00'){
					  $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, number_format($ctrrp,2));
					  $objPHPExcel->getActiveSheet()->getStyle('O'.$i)->applyFromArray(
						  array(
							  'borders' => array(
								  'top' 	=> array('style' => Style_Border::BORDER_THIN),
								  'bottom'=> array('style' => Style_Border::BORDER_THIN),
								  'left'  => array('style' => Style_Border::BORDER_THIN),
								  'right' => array('style' => Style_Border::BORDER_THIN)
							  ),
						  )
					  );
          //}
					$i++;
					$j++;
					
#					if($i==50000) break;
					
				}
				$x=$i-1;
				$objPHPExcel->getActiveSheet()->getStyle('E7'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
				$objPHPExcel->getActiveSheet()->getStyle('D7'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT);
				//$objPHPExcel->getActiveSheet()->getStyle('L7:N'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
        //if($iarea=='00'){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => true
					    )
				    ),
				    'A7:O'.$x
			    );
			    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						    'wrap'      => true
					    )
				    ),
				    'J7:M'.$x
			    );
        //}else{
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => true
					    )
				    ),
				    'A7:L'.$x
			    );
			    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						    'wrap'      => true
					    )
				    ),
				    'J7:L'.$x
			    );
        //}
			}
     // if($iarea=='00'){
	  		$objPHPExcel->getActiveSheet()->getStyle('A6:O6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
      //}else{
  		//	$objPHPExcel->getActiveSheet()->getStyle('A6:L6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
      //}
#			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $nama='Trend Toko Peritem-'.$thn.'.xls';
      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        @unlink('excel/00/'.$nama);
      }
			$objWriter->save('excel/00/'.$nama); 

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
      }
      $pesan='Export Trend Toko Peritem Nasional Tahun'.$thn;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses'] = true;
			$data['inomor']	= "Trend Toko Peritem";
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}


	function exportcsv()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu150')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$this->load->model('trendperitemtoko/mmaster');
			$this->load->dbutil();
			$this->load->helper('file');
			//$this->db->cache_on();
			//$thn	= $this->uri->segment(4);
			$thn = $this->input->post('tahun');
			$thnsebelumnya = $thn-1; 
			//$this->db->save_queries = FALSE;
			$this->db->select("	a.i_product, a.e_product_name, a.i_area, a.e_area_name, a.e_customer_name, a.i_customer, a.e_product_categoryname,  sum(a.vnota) as vnota, 
                          sum(a.qnota) as qnota ,sum(a.oa) as oa , sum(a.prevvnota) as prevvnota , sum(a.prevqnota) as prevqnota , sum(a.prevoa) as prevoa from (

SELECT b.i_product, b.e_product_name, a.i_area, f.e_area_name, c.e_customer_name, a.i_customer, e.e_product_categoryname,  sum(b.n_deliver*v_unit_price)  as vnota, 
                          0 as qnota , 0 as oa , 0 as prevvnota , 0 as prevqnota , 0 as prevoa 
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product_category e, tr_area f
                          where to_char(a.d_nota, 'yyyy') = '$thn' and a.f_nota_cancel='f'
                          and a.i_sj=b.i_sj and a.i_area=b.i_area and f.i_area=a.i_area and f.i_area=b.i_area and a.i_customer=c.i_customer 
                          and b.i_product_category=e.i_product_category
                          group by to_char(a.d_nota, 'yyyy'), b.i_product, b.e_product_name, c.e_customer_name, a.i_customer, e.e_product_categoryname, a.i_area, f.e_area_name
union all
SELECT b.i_product, b.e_product_name, a.i_area, f.e_area_name, c.e_customer_name, a.i_customer, e.e_product_categoryname,  0  as vnota, 
                          sum(b.n_deliver)  as qnota , 0 as oa , 0 as prevvnota , 0 as prevqnota , 0 as prevoa 
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product_category e, tr_area f
                          where to_char(a.d_nota, 'yyyy') = '$thn' and a.f_nota_cancel='f' 
                          and a.i_sj=b.i_sj and a.i_area=b.i_area and f.i_area=a.i_area and f.i_area=b.i_area and a.i_customer=c.i_customer
                          and b.i_product_category=e.i_product_category
                          group by to_char(a.d_nota, 'yyyy'), b.i_product, b.e_product_name, c.e_customer_name, a.i_customer, e.e_product_categoryname, a.i_area, f.e_area_name
union all
select distinct b.i_product, b.e_product_name, a.i_area, f.e_area_name, c.e_customer_name, a.i_customer, e.e_product_categoryname,
        0  as vnota, 0  as qnota , count(a.i_customer) as oa, 0 as prevvnota , 0 as prevqnota , 0 as prevoa
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product_category e, tr_area f
                          where to_char(a.d_nota, 'yyyy') = '$thn' and a.f_nota_cancel='f' 
                          and a.i_sj=b.i_sj and a.i_area=b.i_area and f.i_area=a.i_area and a.i_customer=c.i_customer
                          and b.i_product_category=e.i_product_category
                          group by b.i_product, b.e_product_name, c.e_customer_name, a.i_customer, e.e_product_categoryname, a.i_area, f.e_area_name
--nah ini $thnsebelumnya dibawah
union all
SELECT b.i_product, b.e_product_name, a.i_area, f.e_area_name, c.e_customer_name, a.i_customer, e.e_product_categoryname,  0  as vnota, 
                          0 as qnota , 0 as oa , sum(b.n_deliver*v_unit_price) as prevvnota , 0 as prevqnota , 0 as prevoa 
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product_category e, tr_area f
                          where to_char(a.d_nota, 'yyyy') = '$thnsebelumnya' and a.f_nota_cancel='f' 
                          and a.i_sj=b.i_sj and a.i_area=b.i_area and f.i_area=a.i_area and f.i_area=b.i_area and a.i_customer=c.i_customer
                          and b.i_product_category=e.i_product_category
                          group by to_char(a.d_nota, 'yyyy'), b.i_product, b.e_product_name, c.e_customer_name, a.i_customer, e.e_product_categoryname, a.i_area, f.e_area_name
union all
SELECT b.i_product, b.e_product_name, a.i_area, f.e_area_name, c.e_customer_name, a.i_customer, e.e_product_categoryname,  0  as vnota, 
                          0  as qnota , 0 as oa , 0 as prevvnota , sum(b.n_deliver) as prevqnota , 0 as prevoa 
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product_category e, tr_area f
                          where to_char(a.d_nota, 'yyyy') = '$thnsebelumnya' and a.f_nota_cancel='f' 
                          and a.i_sj=b.i_sj and a.i_area=b.i_area and f.i_area=a.i_area and f.i_area=b.i_area and a.i_customer=c.i_customer
                          and b.i_product_category=e.i_product_category
                          group by to_char(a.d_nota, 'yyyy'), b.i_product, b.e_product_name, c.e_customer_name, a.i_customer, e.e_product_categoryname, a.i_area, f.e_area_name
union all
select distinct  b.i_product, b.e_product_name, a.i_area, f.e_area_name, c.e_customer_name, a.i_customer, e.e_product_categoryname,
        0  as vnota, 0  as qnota , 0 as oa, 0 as prevvnota , 0 as prevqnota , count(a.i_customer) as prevoa
                          from tm_nota a, tm_nota_item b, tr_customer c, tr_product_category e, tr_area f
                          where to_char(a.d_nota, 'yyyy') = '$thnsebelumnya' and a.f_nota_cancel='f' 
                          and a.i_sj=b.i_sj and a.i_area=b.i_area and f.i_area=a.i_area and a.i_customer=c.i_customer
                          and b.i_product_category=e.i_product_category
                          group by b.i_product, b.e_product_name, c.e_customer_name, a.i_customer, e.e_product_categoryname, a.i_area, f.e_area_name
                          ) as a
                          group by a.i_product, a.e_product_name, a.i_area, a.e_area_name, a.e_customer_name, a.i_customer, a.e_product_categoryname
                          order by a.i_customer, a.i_product ",false);
#			$this->db->select("	* from tm_kk 
#								          inner join tr_area on (tm_kk.i_area=tr_area.i_area)
#								          where tm_kk.d_kk >= '$datefrom' and tm_kk.d_kk <= '$dtos' 
#                          and tm_kk.i_area='$iarea' and tm_kk.f_kk_cancel='f'
#								          order by tm_kk.i_area,tm_kk.d_kk,tm_kk.i_kk",false);
		$query=$this->db->get();
		$delimiter = "|";
		$newline = "\r\n";
		$enclosure = '"';
		$data= $this->dbutil->csv_from_result($query,$delimiter, $newline, $enclosure);
		//$this->db->cache_off();
		$nama='Trend Toko Peritem-'.$thn.'.csv';
      	//if(file_exists('excel/00/'.$nama)){
        //@chmod('excel/00/'.$nama, 0777);
        //@unlink('excel/00/'.$nama);
      	//}

		//echo $this->dbutil->csv_from_result($query);
		//die();
		
		write_file('excel/00/'.$nama, $data);
		//@chmod('excel/00/'.$nama, 0777);
        //@unlink('excel/00/'.$nama);
    	
		//	foreach($query->result() as $row){
#          $periode=substr($datefrom,0,4).substr($datefrom,5,2);
		

//		}

    //$data['sukses']			= true;
	//$this->load->view('status',$data);
#	die();
	}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$dto	= $this->input->post('dto');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/trendperitemtoko/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_target a, tr_area b
										              where a.i_periode = '$iperiode'
										              and a.i_area=b.i_area
										              and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '50';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('trendperitemtoko/mmaster');
			$data['page_title'] = $this->lang->line('trendperitemtoko');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('trendperitemtoko/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

		function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/trendperitemtoko/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listspb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listspb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	

		function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/trendperitemtoko/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('trendperitemtoko/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('trendperitemtoko/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

		function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $area=$this->uri->segment(4);
      $cari=strtoupper($this->input->post('cari', FALSE));
      if($cari=='')$cari=$this->uri->segment(5);
      if($cari=='sikasep' || $cari==''){
        $config['base_url'] = base_url().'index.php/trendperitemtoko/cform/customer/'.$area.'/sikasep/';    
      }else{
        $config['base_url'] = base_url().'index.php/trendperitemtoko/cform/customer/'.$area.'/'.$cari.'/';
      }
      if($cari=='sikasep')$cari='';			
      $query = $this->db->query(" select a.i_customer from tr_customer a, tr_area b
                                  where a.i_area='$area' and a.i_area=b.i_area and
                                  (a.i_customer like '%$cari%' or a.e_customer_name like '%$cari%')", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);
      $data['area']=$area;
      $data['cari']=$cari;
			$this->load->model('trendperitemtoko/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(6),$area,$cari);
			$this->load->view('trendperitemtoko/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}




	
  function chartx()
	{		
    $iperiode=$this->uri->segment(4);
    $data['charts'] = $this->getChart($iperiode);
    $this->load->view('trendperitemtoko/charts',$data);
	}
  function getChart($iperiode)
	{
    $this->load->library('highcharts');
    $th=substr($iperiode,0,4);
    $bl=substr($iperiode,4,2);
    $bl=mbulan($bl);
    $this->highcharts->set_title('Target Penjualan Per area', 'Periode : '.$bl.' '.$th);
    $this->highcharts->set_dimensions(1340, 500); 
    $this->highcharts->set_type('column');
    $this->highcharts->set_axis_titles('Area', 'Nilai');
    $credits->href = base_url();
    $credits->text = NmPerusahaan;
    $this->highcharts->set_credits($credits);
#    $this->highcharts->render_to("my_div");
    $this->load->model('trendperitemtoko/mmaster');
    $result = $this->mmaster->bacatarget($iperiode);
    foreach($result as $row){
      $target[] = intval($row->v_target);
    }
    $result = $this->mmaster->bacaspb($iperiode);
    foreach($result as $row){
      $spb[] = intval($row->v_spb_gross);
    }
    $result = $this->mmaster->bacasj($iperiode);
    foreach($result as $row){
      $sj[] = intval($row->v_sj_gross);
    }
    $result = $this->mmaster->bacanota($iperiode);
    foreach($result as $row){
      $nota[] = intval($row->v_nota_gross);
    }
    $result = $this->mmaster->bacaarea($iperiode);
    foreach($result as $row){
      $area[] = $row->i_area;
    }
    $data['axis']['categories'] = $area;
    $data['targets']['data'] = $target;
		$data['targets']['name'] = 'Target';
    $data['spbs']['data'] = $spb;
		$data['spbs']['name'] = 'SPB';
    $data['sjs']['data'] = $sj;
		$data['sjs']['name'] = 'SJ';
    $data['notas']['data'] = $nota;
		$data['notas']['name'] = 'Nota';
  
    $this->highcharts->set_xAxis($data['axis']);
		$this->highcharts->set_serie($data['targets']);
		$this->highcharts->set_serie($data['spbs']);
		$this->highcharts->set_serie($data['sjs']);
		$this->highcharts->set_serie($data['notas']);
    return $this->highcharts->render();
	}
  function fcf(){
    $iperiode=$this->uri->segment(4);
    $tipe=$this->uri->segment(5);
    if($tipe==''){
      $graph_swfFile      = base_url().'flash/FCF_MSColumn3D.swf';
    }else{
      $tipe=str_replace("tandatitik",".",$tipe);
      $graph_swfFile      = base_url().'flash/'.$tipe;
    }
    $th=substr($iperiode,0,4);
    $bl=substr($iperiode,4,2);
    $bl=mbulan($bl);
    $graph_caption      = 'Target Penjualan Per Area Periode : '.$bl.' '.$th ;
    $graph_numberPrefix = 'Rp.' ;
    $graph_title        = 'Penjualan Produk' ;
    $graph_width        = 954;
    $graph_height       = 500;
    $this->load->model('trendperitemtoko/mmaster');

    // Area
    $i=0;
    $result = $this->mmaster->bacaarea($iperiode);
    foreach($result as $row){
      $category[$i] = $row->i_area;
      $i++;
    }

    // data set
    $dataset[0] = 'Target' ;
    $dataset[1] = 'SPB' ;
    $dataset[2] = 'SJ' ;
    $dataset[3] = 'Nota' ;

    //data 1
    $i=0;
    $result = $this->mmaster->bacatarget($iperiode);
    foreach($result as $row){
      $arrData['Target'][$i] = intval($row->v_target);
      $i++;
    }

    //data 2
    $i=0;
    $result = $this->mmaster->bacaspb($iperiode);
    foreach($result as $row){
      $arrData['SPB'][$i] = intval($row->v_spb_gross);
      $i++;
    }

    //data 3
    $i=0;
    $result = $this->mmaster->bacasj($iperiode);
    foreach($result as $row){
      $arrData['SJ'][$i] = intval($row->v_sj_gross);
      $i++;
    }


    //data 4
    $i=0;
    $result = $this->mmaster->bacanota($iperiode);
    foreach($result as $row){
      $arrData['Nota'][$i] = intval($row->v_nota_gross);
      $i++;
    }

    $strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='100' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='0' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' showValues='0'>" ;

    //Convert category to XML and append
    $strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
    foreach ($category as $c) {
        $strXML .= "<category name='".$c."'/>" ;
    }
    $strXML .= "</categories>" ;

    //Convert dataset and data to XML and append
    foreach ($dataset as $set) {
        $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
        foreach ($arrData[$set] as $d) {
            $strXML .= "<set value='".$d."'/>" ;
        }
        $strXML .= "</dataset>" ;
    }

//Close <chart> element
$strXML .= "</graph>";

    $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);
    $data['iperiode']=$iperiode;
    $data['modul']='trendperitemtoko';
		$data['isi']= directory_map('./flash/');
		$data['file']='';

    $this->load->view('trendperitemtoko/chart_view',$data) ;
  }
	function detailnota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $iarea=$this->uri->segment(4);
      $period=$this->uri->segment(5);
			$this->load->model('trendperitemtoko/mmaster');
			$data['page_title'] = $this->lang->line('trendperitemtoko');
			$data['isi']=$this->mmaster->bacadetailnota($iarea,$period);
			$this->load->view('trendperitemtoko/vformdetail', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detailkn()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $iarea=$this->uri->segment(4);
      $periode=$this->uri->segment(5);
			$this->load->model('trendperitemtoko/mmaster');
			$data['page_title'] = $this->lang->line('trendperitemtoko');
			$data['isi']=$this->mmaster->bacadetailkn($iarea,$periode);
			$this->load->view('trendperitemtoko/vformdetailkn', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
