<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exp-penjualan-perpulau');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('exp-penjualan-perpulau/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-penjualan-perpulau/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
#      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }

      $a=substr($iperiode,0,4);
		$b=substr($iperiode,4,2);
		$peri=mbulan($b)." - ".$a;

#			if($iarea=='') $iarea=$this->uri->segment(5);
      $this->db->select("data.i_product,  data.e_product_name, 
sum(data.jawaqty) as jawaqty, 
sum(data.jawarp) as jawarp,
sum(data.sumatraqty) as sumatraqty,
sum(data.sumatrarp) as sumatrarp,

sum(data.kalimantanqty) as kalimantanqty, 
sum(data.kalimantanrp) as kalimantanrp, 
sum(data.kepulauanqty) as kepulauanqty, 
sum(data.kepulauanrp) as kepulauanrp

from(
select i_product, e_product_name, sum(n_deliver) as jawaqty, sum(v_nota_netto) as jawarp, 0 as sumatraqty, 0 AS sumatrarp, 0 as kalimantanqty, 0 as kalimantanrp, 0 as kepulauanqty, 0 as kepulauanrp
from tm_masterdata_item a
where to_char(d_nota,'yyyymm')='$iperiode' and i_area in('01','02','03','15')
group by i_product, e_product_name,v_nota_netto, i_area
union all
select i_product, e_product_name, 0 as jawaqty, 0 as jawarp, sum(n_deliver) as sumatraqty, sum(v_nota_netto) as sumatrarp, 0 as kalimantanqty, 0 as kalimantanrp, 0 as kepulauanqty, 0 as kepulauanrp
from tm_masterdata_item 
where to_char(d_nota,'yyyymm')='$iperiode' and i_area in('04','05','06','07','08','14','16')
group by i_product, e_product_name,v_nota_netto, i_area
union all
select i_product, e_product_name, 0 as jawaqty, 0 as jawarp, 0 as sumatraqty, 0 AS sumatrarp , sum(n_deliver) as kalimantanqty, sum(v_nota_netto) as kalimantanrp, 0 as kepulauanqty, 0 as kepulauanrp
from tm_masterdata_item 
where to_char(d_nota,'yyyymm')='$iperiode' and i_area in('11','12','13')
group by i_product, e_product_name, v_nota_netto, i_area
union all
select i_product, e_product_name, 0 as jawaqty, 0 as jawarp , 0 as sumatraqty, 0 AS sumatrarp, 0 as kalimantanqty, 0 as kalimantanrp , sum(n_deliver) as kepulauanqty, sum(v_nota_netto) as kepulauanrp
from tm_masterdata_item 
where to_char(d_nota,'yyyymm')='$iperiode' and i_area in('09','10')
group by i_product, e_product_name,v_nota_netto, i_area
) as data
group by i_product, e_product_name
order by i_product",false);
		  
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar Penjualan Per Pulau")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:L5'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(7);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(16);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(7);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(16);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(7);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(16);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(7);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(16);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar Penjualan Per Pulau');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,11,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Bulan : '.$peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,11,3);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Jawa');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(2,5,3,4);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Sumatra');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4,5,5,4);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Kalimatan');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6,5,7,4);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Kepulauan');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8,5,9,4);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
				 		'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A6:N6'
				);
				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Kode Produk');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);	
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Nama Produk');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);			
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Rupiah.');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

			    $objPHPExcel->getActiveSheet()->setCellValue('E6', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Rupiah');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

			    $objPHPExcel->getActiveSheet()->setCellValue('G6', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Rupiah');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Rupiah');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$i=7;
				$j=7;
        		$no=0;
				foreach($query->result() as $row)
				{
          		$no++;
          		$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  	array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':J'.$i
				  );         
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_product, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->e_product_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->jawaqty, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->jawarp, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->sumatraqty, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->sumatrarp, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->kalimantanqty, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->kalimantanrp, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->kepulauanqty, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->kepulauanrp, Cell_DataType::TYPE_NUMERIC);
					$i++;
					$j++;
				}
				$x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      	$nama='penjualanperpulau'.$iperiode.'.xls';
			$objWriter->save('excel/00/'.$nama); 

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
      }
      $pesan='Export Data Penjualan Per Pulau:'.$iperiode;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses'] = true;
			$data['inomor']	= "Export Penjualan Per Pulau ".$peri;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>