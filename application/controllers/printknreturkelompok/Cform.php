<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu178')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printknretur');
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['cari'] = '';
      $data['dfrom']= '';
			$data['dto']  = '';
		  $data['iarea'] = '';
			$data['knreturfrom']='';
			$data['knreturto']	='';
			$this->load->view('printknreturkelompok/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu178')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printknreturkelompok/cform/area/index/';
			$query = $this->db->query("select * from tr_area ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('printknreturkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('printknreturkelompok/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu178')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printknreturkelompok/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_area
									   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printknreturkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printknreturkelompok/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function knreturfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu178')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea	= $this->session->userdata('i_area');
			$cari	  = strtoupper($this->input->post('cari'));
			$dfrom	= strtoupper($this->input->post('dfrom'));
			$dto	  = strtoupper($this->input->post('dto'));
			$iarea  = strtoupper($this->input->post('iarea'));
      if($dfrom=='') $dfrom  =$this->uri->segment(4);
      if($dto=='') $dto=$this->uri->segment(5);
      if($iarea=='') $iarea=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printknreturkelompok/cform/knreturfrom/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$query = $this->db->query("	select a.i_kn, a.d_kn, a.i_area, c.e_area_name from tm_kn a, tr_customer b, tr_area c
                                  where (upper(a.i_kn) like '%$cari%')
                                  and a.i_area='$iarea' and a.i_area=c.i_area and a.i_customer=b.i_customer and a.i_kn_type='01'
                                  and a.d_kn >= to_date('$dfrom','dd-mm-yyyy') 
                                  and a.d_kn <= to_date('$dto','dd-mm-yyyy') order by a.i_kn",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('printknreturkelompok/mmaster');
			$data['page_title'] = $this->lang->line('printknretur');
			$data['dfrom'] = $dfrom;
			$data['dto'] = $dto;
			$data['iarea'] = $iarea;
			$data['isi']=$this->mmaster->bacaknretur($cari,$iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(7));
			$this->load->view('printknreturkelompok/vlistknreturfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function knreturto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu178')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea	= $this->session->userdata('i_area');
			$cari	  = strtoupper($this->input->post('cari'));
			$dfrom	= strtoupper($this->input->post('dfrom'));
			$dto	  = strtoupper($this->input->post('dto'));
			$iarea  = strtoupper($this->input->post('iarea'));
      if($dfrom=='') $dfrom  =$this->uri->segment(4);
      if($dto=='') $dto=$this->uri->segment(5);
      if($iarea=='') $iarea=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printknreturkelompok/cform/knreturto/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$query = $this->db->query("	select a.i_kn, a.d_kn, a.i_area, c.e_area_name from tm_kn a, tr_customer b, tr_area c
                                  where (upper(a.i_kn) like '%$cari%')
                                  and a.i_area='$iarea' and a.i_area=c.i_area and a.i_customer=b.i_customer and a.i_kn_type='01'
                                  and a.d_kn >= to_date('$dfrom','dd-mm-yyyy') 
                                  and a.d_kn <= to_date('$dto','dd-mm-yyyy') order by a.i_kn",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('printknreturkelompok/mmaster');
			$data['page_title'] = $this->lang->line('printknretur');
			$data['dfrom'] = $dfrom;
			$data['dto']   = $dto;
			$data['iarea'] = $iarea;
			$data['isi']=$this->mmaster->bacaknretur($cari,$iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(7));
			$this->load->view('printknreturkelompok/vlistknreturto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu178')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	= $this->input->post('iarea');
			$knreturfrom= $this->input->post('knreturfrom');
			$knreturto	= $this->input->post('knreturto');
			$this->load->model('printknreturkelompok/mmaster');
			$data['page_title'] = $this->lang->line('printknretur');
			$data['master']=$this->mmaster->bacamaster($area,$knreturfrom,$knreturto);
#		  print_r ($data['master']);die;
			$data['area']=$area;
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak KN Retur Kelompok Area:'.$area.' No:'.$knreturfrom.' s/d No:'.$knreturto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printknreturkelompok/vformrpt',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
