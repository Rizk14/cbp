<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index(){
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu50')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('spbrealisasiulang');
			$data['dfrom']		= '';
			$data['dto']		= '';
			$data['isi']		= '';
			$data['area']		= '';
			$this->load->view('spbrealisasiulang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu50')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$area	= $this->input->post('iarea');
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$area1 	= $this->session->userdata('i_area');

			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($area=='') $area=$this->uri->segment(6);

			$config['base_url'] = base_url().'index.php/spbrealisasiulang/cform/view/'.$dfrom.'/'.$dto."/".$area.'/index/';
			
			if($area == "NA"){
				$query = $this->db->query("	SELECT a.i_area, c.e_area_name, a.i_customer, e_customer_name, a.i_spb, to_char(a.d_spb,'dd-mm-yyyy') as d_spb,
											a.f_spb_stockdaerah
											FROM tm_spb a
											INNER JOIN tr_customer b on(a.i_customer = b.i_customer)
											INNER JOIN tr_area c on(a.i_area = c.i_area)
											LEFT JOIN tm_nota d on(a.i_sj = d.i_sj and a.i_area = d.i_area and f_nota_cancel = 'f')
											WHERE
											f_spb_cancel = 'f' AND NOT a.i_store isnull AND NOT a.i_store_location isnull
											AND (a.f_spb_siapnotagudang = 'f' or a.f_spb_siapnotagudang = 't')
											AND (a.f_spb_siapnotasales = 'f' or a.f_spb_siapnotasales = 't')
											AND d.i_sj ISNULL AND d.i_nota ISNULL AND a.i_spb_program ISNULL
											AND a.f_spb_rekap = 'f'
											AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
											AND (upper(a.i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%' or a.i_customer like '%$cari%')
											ORDER BY a.d_spb DESC ",false);
			}else{
				if($area1=="00"){
					$query = $this->db->query("	SELECT a.i_area, c.e_area_name, a.i_customer, e_customer_name, a.i_spb, to_char(a.d_spb,'dd-mm-yyyy') as d_spb,
												a.f_spb_stockdaerah
												FROM tm_spb a
												INNER JOIN tr_customer b on(a.i_customer = b.i_customer)
												INNER JOIN tr_area c on(a.i_area = c.i_area)
												LEFT JOIN tm_nota d on(a.i_sj = d.i_sj and a.i_area = d.i_area and f_nota_cancel = 'f')
												WHERE
												f_spb_cancel = 'f' AND NOT a.i_store isnull AND NOT a.i_store_location isnull
												AND (a.f_spb_siapnotagudang = 'f' or a.f_spb_siapnotagudang = 't')
												AND (a.f_spb_siapnotasales = 'f' or a.f_spb_siapnotasales = 't')
												AND (f_spb_stockdaerah='t' OR f_spb_stockdaerah='f')
												AND d.i_sj ISNULL AND d.i_nota ISNULL AND a.i_spb_program ISNULL
												AND a.f_spb_rekap = 'f' AND a.i_area = '$area'
												AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
												AND (upper(a.i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%' or a.i_customer like '%$cari%')
												ORDER BY a.d_spb DESC ",false);
				}else{
					$query = $this->db->query("	SELECT a.i_area, c.e_area_name, a.i_customer, e_customer_name, a.i_spb, to_char(a.d_spb,'dd-mm-yyyy') as d_spb,
												a.f_spb_stockdaerah
												FROM tm_spb a
												INNER JOIN tr_customer b on(a.i_customer = b.i_customer)
												INNER JOIN tr_area c on(a.i_area = c.i_area)
												LEFT JOIN tm_nota d on(a.i_sj = d.i_sj and a.i_area = d.i_area and f_nota_cancel = 'f')
												WHERE
												f_spb_cancel = 'f' AND NOT a.i_store isnull AND NOT a.i_store_location isnull
												AND (a.f_spb_siapnotagudang = 'f' or a.f_spb_siapnotagudang = 't')
												AND (a.f_spb_siapnotasales = 'f' or a.f_spb_siapnotasales = 't')
												AND f_spb_stockdaerah='t'
												AND d.i_sj ISNULL AND d.i_nota ISNULL AND a.i_spb_program ISNULL
												AND a.f_spb_rekap = 'f' AND a.i_area = '$area'
												AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
												AND (upper(a.i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%' or a.i_customer like '%$cari%')
												ORDER BY a.d_spb DESC ",false);
				}
			}

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('spbrealisasiulang');
			$this->load->model('spbrealisasiulang/mmaster');
			$data['dfrom'] 	= $dfrom;
			$data['dto'] 	= $dto;
			$data['area'] 	= $area;
			$data['cari'] 	= $cari;
			$data['isi'] 	= $this->mmaster->bacasemua($dfrom, $dto, $area, $cari,$config['per_page'],$this->uri->segment(8));

			$this->load->view('spbrealisasiulang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu50')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/spbrealisasiulang/cform/area/'.$baris.'/sikasep/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
         	$data['cari']='';
			$this->load->model('spbrealisasiulang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6),$iuser);
			$this->load->view('spbrealisasiulang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu50')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/spbrealisasiulang/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);*/

			$baris   = $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari   = ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
			$config['base_url'] = base_url().'index.php/spbrealisasiulang/cform/cariarea/'.$baris.'/'.$cari.'/';
			  else
			$config['base_url'] = base_url().'index.php/spbrealisasiulang/cform/cariarea/'.$baris.'/sikasep/';

      		$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('spbrealisasiulang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['baris']=$baris;
         	$data['cari']=$cari;
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6),$iuser);
			$this->load->view('spbrealisasiulang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function closing()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu50')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml 	= $this->input->post('jml', TRUE);
			$dfrom 	= $this->input->post('dfrom', TRUE);
			$dto 	= $this->input->post('dto', TRUE);
			$area1 	= $this->session->userdata('i_area');
			$this->db->trans_begin();
			$this->load->model('spbrealisasiulang/mmaster');
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$ispb = $this->input->post('ispb'.$i, TRUE);
					$area = $this->input->post('iarea'.$i, TRUE);
	          		$this->mmaster->updatespb($ispb,$area);
				}
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
			}

			$sess 	= $this->session->userdata('session_id');
			$id		= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Balik Realisasi Pemenuhan SPB No:'.$ispb." Area :".$area;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			/* $config['base_url'] = base_url().'index.php/spbrealisasiulang/cform/index/'; */
			$config['base_url'] = base_url().'index.php/spbrealisasiulang/cform/index/'.$dfrom.'/'.$dto."/".$area.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);

			if($area == "NA"){
				$query = $this->db->query("	SELECT a.i_area, c.e_area_name, a.i_customer, e_customer_name, a.i_spb, to_char(a.d_spb,'dd-mm-yyyy') as d_spb,
											a.f_spb_stockdaerah
											FROM tm_spb a
											INNER JOIN tr_customer b on(a.i_customer = b.i_customer)
											INNER JOIN tr_area c on(a.i_area = c.i_area)
											LEFT JOIN tm_nota d on(a.i_sj = d.i_sj and a.i_area = d.i_area and f_nota_cancel = 'f')
											WHERE
											f_spb_cancel = 'f' AND NOT a.i_store isnull AND NOT a.i_store_location isnull
											AND (a.f_spb_siapnotagudang = 'f' or a.f_spb_siapnotagudang = 't')
											AND (a.f_spb_siapnotasales = 'f' or a.f_spb_siapnotasales = 't')
											AND d.i_sj ISNULL AND d.i_nota ISNULL AND a.i_spb_program ISNULL
											AND a.f_spb_rekap = 'f'
											AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
											AND (upper(a.i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%' or a.i_customer like '%$cari%')
											ORDER BY a.d_spb DESC ",false);
			}else{
				if($area1=="00"){
					$query = $this->db->query("	SELECT a.i_area, c.e_area_name, a.i_customer, e_customer_name, a.i_spb, to_char(a.d_spb,'dd-mm-yyyy') as d_spb,
												a.f_spb_stockdaerah
												FROM tm_spb a
												INNER JOIN tr_customer b on(a.i_customer = b.i_customer)
												INNER JOIN tr_area c on(a.i_area = c.i_area)
												LEFT JOIN tm_nota d on(a.i_sj = d.i_sj and a.i_area = d.i_area and f_nota_cancel = 'f')
												WHERE
												f_spb_cancel = 'f' AND NOT a.i_store isnull AND NOT a.i_store_location isnull
												AND (a.f_spb_siapnotagudang = 'f' or a.f_spb_siapnotagudang = 't')
												AND (a.f_spb_siapnotasales = 'f' or a.f_spb_siapnotasales = 't')
												AND (f_spb_stockdaerah='t' OR f_spb_stockdaerah='f')
												AND d.i_sj ISNULL AND d.i_nota ISNULL AND a.i_spb_program ISNULL
												AND a.f_spb_rekap = 'f' AND a.i_area = '$area'
												AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
												AND (upper(a.i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%' or a.i_customer like '%$cari%')
												ORDER BY a.d_spb DESC ",false);
				}else{
					$query = $this->db->query("	SELECT a.i_area, c.e_area_name, a.i_customer, e_customer_name, a.i_spb, to_char(a.d_spb,'dd-mm-yyyy') as d_spb,
												a.f_spb_stockdaerah
												FROM tm_spb a
												INNER JOIN tr_customer b on(a.i_customer = b.i_customer)
												INNER JOIN tr_area c on(a.i_area = c.i_area)
												LEFT JOIN tm_nota d on(a.i_sj = d.i_sj and a.i_area = d.i_area and f_nota_cancel = 'f')
												WHERE
												f_spb_cancel = 'f' AND NOT a.i_store isnull AND NOT a.i_store_location isnull
												AND (a.f_spb_siapnotagudang = 'f' or a.f_spb_siapnotagudang = 't')
												AND (a.f_spb_siapnotasales = 'f' or a.f_spb_siapnotasales = 't')
												AND f_spb_stockdaerah='t'
												AND d.i_sj ISNULL AND d.i_nota ISNULL AND a.i_spb_program ISNULL
												AND a.f_spb_rekap = 'f' AND a.i_area = '$area'
												AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
												AND (upper(a.i_spb) like '%$cari%' or upper(e_customer_name) like '%$cari%' or a.i_customer like '%$cari%')
												ORDER BY a.d_spb DESC ",false);
				}
			}

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('spbrealisasiulang');
			$this->load->model('spbrealisasiulang/mmaster');
			$data['dfrom'] 	= $dfrom;
			$data['dto'] 	= $dto;
			$data['area'] 	= $area;
			$data['cari'] 	= $cari;
			$data['isi']=$this->mmaster->bacasemua($dfrom, $dto, $area, $cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('spbrealisasiulang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu50')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('opclose');
			$this->load->view('spbrealisasiulang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
