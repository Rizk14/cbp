<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transfer');
			$this->load->model('transferuang/mmaster');
			$data['iku']='';
			$this->load->view('transferuang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transfer');
			$this->load->view('transferuang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transfer')." update";
			if(
				($this->uri->segment(4)!='')
			  ){
				$data['iku']		= $this->uri->segment(4);
				$iku				= $this->uri->segment(4);
				$data['nkuyear']	= $this->uri->segment(5);
				$nkuyear			= $this->uri->segment(5);
				$data['iarea']		= $this->uri->segment(6);
				$iarea				= $this->uri->segment(6);
				$this->load->model("transferuang/mmaster");
				$data['isi']=$this->mmaster->baca($iarea,$iku,$nkuyear);
		 		$this->load->view('transferuang/vformupdate',$data);
			}else{
				$this->load->view('transferuang/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iku 	= $this->input->post('iku', TRUE);
			$dku	= $this->input->post('dku', TRUE);
			if($dku!=''){
				$tmp=explode("-",$dku);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dku=$th."-".$bl."-".$hr;
				$tahun=$th;
			}
			$ebankname			= $this->input->post('ebankname', TRUE);
			$iarea				= $this->input->post('iarea', TRUE);
			$eareaname			= $this->input->post('eareaname', TRUE);
			$icustomer			= $this->input->post('icustomer', TRUE);
			$icustomergroupar	= $this->input->post('icustomergroupar', TRUE);
			$ecustomername		= $this->input->post('ecustomername', TRUE);
			$isupplier			= $this->input->post('isupplier', TRUE);
			$esuppliername		= $this->input->post('esuppliername', TRUE);
			$isalesman			= $this->input->post('isalesman', TRUE);
			$esalesmanname		= $this->input->post('esalesmanname', TRUE);
			$eremark			= $this->input->post('eremark', TRUE);
			$vjumlah			= $this->input->post('vjumlah', TRUE);
			$vjumlah			= str_replace(',','',$vjumlah);
			$vsisa				= $this->input->post('vsisa', TRUE);
			$vsisa				= str_replace(',','',$vsisa);
			if (
				($iku != '') && ($iarea!='') && ($tahun!='')
			   )
			{
				$this->load->model('transferuang/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($iku,$dku,$tahun,$ebankname,$iarea,$icustomer,$icustomergroupar,
										   $isupplier,$isalesman,$eremark,$vjumlah,$vsisa);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update KU Area '.$iarea.' No:'.$iku;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $iku;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iku 	= $this->input->post('iku', TRUE);
			$dku	= $this->input->post('dku', TRUE);
			if($dku!=''){
				$tmp=explode("-",$dku);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dku=$th."-".$bl."-".$hr;
				$tahun=$th;
			}
			$ebankname			= $this->input->post('ebankname', TRUE);
			$iarea				= $this->input->post('iarea', TRUE);
			$eareaname			= $this->input->post('eareaname', TRUE);
			$icustomer			= $this->input->post('icustomer', TRUE);
			$icustomergroupar	= $this->input->post('icustomergroupar', TRUE);
			$ecustomername		= $this->input->post('ecustomername', TRUE);
			$isupplier			= $this->input->post('isupplier', TRUE);
			$esuppliername		= $this->input->post('esuppliername', TRUE);
			$isalesman			= $this->input->post('isalesman', TRUE);
			$esalesmanname		= $this->input->post('esalesmanname', TRUE);
			$eremark			= $this->input->post('eremark', TRUE);
			$vjumlah			= $this->input->post('vjumlah', TRUE);
			$vjumlah			= str_replace(',','',$vjumlah);
			$vsisa				= $this->input->post('vsisa', TRUE);
			$vsisa				= str_replace(',','',$vsisa);
			if (
				($iku != '') && ($iarea!='') && ($tahun!='')
			   )
			{
				$this->load->model('transferuang/mmaster');
				$this->db->trans_begin();
				$cek=$this->mmaster->cek($iarea,$iku,$tahun);
				if(!$cek){							
					$this->mmaster->insert($iku,$dku,$tahun,$ebankname,$iarea,$icustomer,$icustomergroupar,
										   $isupplier,$isalesman,$eremark,$vjumlah,$vsisa);
				}else{
					$nomor="Bukti transfer ".$iku." sudah ada, untuk mengedit lewat menu edit Transfer";
				}
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input KU Area '.$iarea.' No:'.$iku;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $iku;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transferuang/cform/area/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tr_area ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transferuang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('transferuang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_area
									   	where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transferuang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('transferuang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/transferuang/cform/customer/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman from tr_customer a
										left join tr_customer_groupar b on(a.i_customer=b.i_customer)
										left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
										where a.i_area = '$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('transferuang/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('transferuang/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 	= $this->uri->segment(4);
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/transferuang/cform/customer/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman from tr_customer a
										left join tr_customer_groupar b on(a.i_customer=b.i_customer)
										left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
										and (upper(a.i_customer) like '%$cari%' or a.e_customer_name like '%$cari')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('transferuang/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('transferuang/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transferuang/cform/supplier/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select i_supplier from tr_supplier ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transferuang/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('transferuang/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu140')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/transferuang/cform/supplier/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select i_supplier from tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transferuang/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('transferuang/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
