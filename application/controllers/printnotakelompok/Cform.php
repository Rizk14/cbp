<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
#		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printnota');
			$this->load->view('printnotakelompok/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function notafrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ) {
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= strtoupper($this->input->post('cari'));
      $dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printnotakelompok/cform/notafrom/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$query = $this->db->query("	select i_nota from tm_nota
              										where upper(i_nota) like '%$cari%' and f_nota_cancel='f' 
                                  and substring(i_sj,9,2)='$iarea' and
                                  d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
                                  d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('printnotakelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['iarea']=$iarea;
			$data['isi']=$this->mmaster->bacanota($dfrom,$dto,$iarea,$cari,$config['per_page'],$this->uri->segment(7));
			$this->load->view('printnotakelompok/vlistnotafrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carinotafrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= strtoupper($this->input->post('cari'));
      $dfrom		= $this->input->post('ydfrom');
			$dto		= $this->input->post('ydto');
			$iarea		= $this->input->post('yiarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printnotakelompok/cform/notafrom/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$query = $this->db->query("	select i_nota from tm_nota where upper(i_nota) like '%$cari%' and f_nota_cancel='f' 
                                  and substring(i_sj,9,2)='$iarea' and
                                  d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
                                  d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printnotakelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['iarea']=$iarea;
			$data['isi']=$this->mmaster->bacanota($dfrom,$dto,$iarea,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printnotakelompok/vlistnotafrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function notato()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	  = strtoupper($this->input->post('cari'));
      $dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printnotakelompok/cform/notato/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$query = $this->db->query("	select i_nota from tm_nota
              										where upper(i_nota) like '%$cari%' 
                                  and f_nota_cancel='f' and substring(i_sj,9,2)='$iarea' and
                                  d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
                                  d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('printnotakelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['iarea']=$iarea;
			$data['isi']=$this->mmaster->bacanotato($dfrom,$dto,$iarea,$cari,$config['per_page'],$this->uri->segment(7));
			$this->load->view('printnotakelompok/vlistnotato', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carinotato()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	  = strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('xdfrom');
			$dto		= $this->input->post('xdto');
			$iarea		= $this->input->post('xiarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printnotakelompok/cform/notato/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$query = $this->db->query("	select i_nota from tm_nota
              										where upper(i_nota) like '%$cari%' and f_nota_cancel='f' 
                                  and substring(i_sj,9,2)='$iarea' and
                                  d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
                                  d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('printnotakelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['iarea']=$iarea;
			$data['isi']=$this->mmaster->bacanotato($dfrom,$dto,$iarea,$cari,$config['per_page'],$this->uri->segment(7));
			$this->load->view('printnotakelompok/vlistnotato', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	= $this->input->post('areafrom');
			$notafrom= $this->input->post('notafrom');
			$notato	= $this->input->post('notato');
			$this->load->model('printnotakelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['master']=$this->mmaster->bacamaster($notafrom,$notato);
			$data['area']=$area;
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak Nota Area:'.$area.' No:'.$notafrom.' s/d No:'.$notato;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printnotakelompok/vformrpt',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printnotakelompok/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printnotakelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printnotakelompok/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printnotakelompok/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printnotakelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printnotakelompok/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
