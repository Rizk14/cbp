<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
  function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listop');
			$data['dfrom']='';
			$data['dto']='';				
			$this->load->view('listopfc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari	  = strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/listopfc/cform/view/'.$dfrom.'/'.$dto.'/index/';
			$query = $this->db->query(" select i_op from v_list_opfc 
                                  where (upper(i_supplier) like '%$cari%' or upper(i_op) like '%$cari%' 
                                  or upper(i_reff) like '%$cari%' or upper(i_supplier) like '%$cari%' 
                                  or upper(e_supplier_name) like '%$cari%') and 
										              d_op >= to_date('$dfrom','dd-mm-yyyy') AND
										              d_op <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('listop');
			$this->load->model('listopfc/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$dfrom,$dto,$config['per_page'],$this->uri->segment(7));
      $data['dfrom']=$dfrom;
			$data['dto']=$dto;

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data OP Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listopfc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listop');
			$this->load->view('listopfc/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iop	= $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$this->load->model('listopfc/mmaster');
			$this->mmaster->delete($iop,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Delete OP No:'.$iop.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('listop');
			$config['base_url'] = base_url().'index.php/listopfc/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari  = strtoupper($cari);
			$query = $this->db->query(" 	select a.*, b.e_supplier_name from tm_opfc a, tr_supplier b
							where a.i_supplier=b.i_supplier and a.f_op_cancel='f'
							and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
							or upper(a.i_op) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['cari']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('listopfc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari  = $this->input->post('cari', FALSE);
			$cari  = strtoupper($cari);
			if(($cari=='') && ($this->uri->segment(5)=='') && ($this->uri->segment(4)!='sikasep')) $cari=$this->uri->segment(4);
			if($cari=='') 
			{
				$config['base_url'] = base_url().'index.php/listopfc/cform/index/sikasep/';
			}else{
				$config['base_url'] = base_url().'index.php/listopfc/cform/index/'.$cari.'/';
			}
			$config['base_url'] = base_url().'index.php/listopfc/cform/index/'.$cari.'/';
			$query = $this->db->query(" select * from v_list_opfc where upper(i_supplier) like '%$cari%' or upper(i_op) like '%$cari%' 
										or upper(i_spb_old) like '%$cari%' or upper(i_spmb_old) like '%$cari%' or upper(i_reff) like '%$cari%' 
										or upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listopfc/mmaster');
			$data['cari'] = $cari;
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('listop');
			$data['iop']='';
	 		$this->load->view('listopfc/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
