<?php
class Cform extends CI_Controller
{
	public $title = "Export Pembelian Akunting";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
		$this->load->model('exp-pembelian-akunting/mmaster');
		require_once('php/fungsi.php');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu513') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->title;

			$this->load->view('exp-pembelian-akunting/vform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if ((($this->session->userdata('logged_in')) && ($this->session->userdata('menu513') == 't')) ||
			(($this->session->userdata('logged_in')) && ($this->session->userdata('allmenu') == 't'))
		) {
			$iperiode 	= $this->uri->segment(4);
			$bulan 		= $this->uri->segment(5);
			$tahun 		= $this->uri->segment(6);
			$this->load->library('excel');
			$objPHPExcel = new PHPExcel();

			$isi		= $this->mmaster->bacaperiode($iperiode);
			if ($isi) {
				$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
				$objPHPExcel->getProperties()->setCreator("M.I.S Dept");
				$objPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept");

				$objPHPExcel->getProperties()
					->setTitle("Nota Akunting")
					->setSubject("Nota Akunting")
					->setDescription(NmPerusahaan)
					->setKeywords("Nota Akunting")
					->setCategory("Laporan");

				$objPHPExcel->getActiveSheet()->setTitle("Nota Akunting");

				$objPHPExcel->setActiveSheetIndex(0);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 12
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2'
				);

				$header = array(
					'font' => array(
						'name' => 'Times New Roman',
						'bold' => true,
						'italic' => false,
						'size' => 12
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => Style_Alignment::VERTICAL_CENTER,
						'wrap' => true
					)
				);

				$footer = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'vertical'  => Style_Alignment::VERTICAL_CENTER
					)
				);


				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(28);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);

				$objPHPExcel->getActiveSheet()->mergeCells('A2:K2');
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan Pembelian Akunting');
				$objPHPExcel->getActiveSheet()->mergeCells('A3:K3');
				$objPHPExcel->getActiveSheet()->getStyle('A3')->applyFromArray($header);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Periode : ' . mbulan($bulan) . " " . $tahun);

				$style_col = array(
					'font' => array(
						'name'	=> 'Times New Roman',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER
					)
				);

				$style_col1 = array(
					'font' => array(
						'name'	=> 'Times New Roman',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'vertical'  => Style_Alignment::VERTICAL_CENTER
					)
				);

				$style_row1 = array(
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)

					),
					'font' => array(
						'name'	=> 'Times New Roman',
						'bold'  => false,
						'italic' => false,
						'size'  => 10
					),
				);

				$style_row = array(
					'font' => array(
						'name'	=> 'Times New Roman',
						'bold'  => false,
						'italic' => false,
						'size'  => 9
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						// 'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER
					)
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Kode Supplier');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($style_col);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Nama Supplier');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($style_col);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'No Faktur');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray($style_col);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Tgl Faktur');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($style_col);


				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray($style_col);


				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Jatuh Tempo');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray($style_col);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Tgl Pajak');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray($style_col);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Nilai Kotor');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray($style_col);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Diskon');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray($style_col);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'DPP');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray($style_col);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'PPN');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray($style_col);
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Hutang Dagang');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray($style_col);

				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Sisa');
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray($style_col);
				$objPHPExcel->getActiveSheet()->setCellValue('N5', 'TOP');
				$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray($style_col);
				$objPHPExcel->getActiveSheet()->setCellValue('O5', 'No Seri Pajak');
				$objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray($style_col);

				$objPHPExcel->getActiveSheet()->getStyle('A5:O5')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DFF1D0');

				// $i 	= 1;
				$j 			= 6;
				$vtgross 	= 0;
				$vtdisc 	= 0;
				$vtnetto 	= 0;
				$vtdpp 		= 0;
				$vtppn 		= 0;

				foreach ($isi as $row) {
					$row->v_dpp	= $row->f_supplier_pkp == 't' ? $row->v_dpp : 0;
					$row->v_ppn	= $row->f_supplier_pkp == 't' ? $row->v_ppn : 0;

					$vtgross	+= $row->v_gross;
					$vtdisc		+= $row->v_discount;
					$vtnetto	+= $row->v_netto;
					$vtdpp		+= $row->v_dpp;
					$vtppn		+= $row->v_ppn;

					$objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $row->i_supplier, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($style_row);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $j)->applyFromArray($style_row);
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $j, $row->nofaktur, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('C' . $j)->applyFromArray($style_row);
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $j, $row->tglfaktur, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('D' . $j)->applyFromArray($style_row);


					$objPHPExcel->getActiveSheet()->setCellValue('E' . $j, $row->e_area_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('E' . $j)->applyFromArray($style_row);					


					$objPHPExcel->getActiveSheet()->setCellValue('F' . $j, $row->jatuhtempo, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('F' . $j)->applyFromArray($style_row);
					$objPHPExcel->getActiveSheet()->setCellValue('G' . $j, $row->d_pajak, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('G' . $j)->applyFromArray($style_row);

					$objPHPExcel->getActiveSheet()->setCellValue('H' . $j, $row->v_gross);
					$objPHPExcel->getActiveSheet()->getStyle('H' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);;
					$objPHPExcel->getActiveSheet()->getStyle('H' . $j)->applyFromArray($style_row1);

					$objPHPExcel->getActiveSheet()->setCellValue('I' . $j, $row->v_discount);
					$objPHPExcel->getActiveSheet()->getStyle('I' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);;
					$objPHPExcel->getActiveSheet()->getStyle('I' . $j)->applyFromArray($style_row1);

					$objPHPExcel->getActiveSheet()->setCellValue('J' . $j, $row->v_dpp, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->getStyle('J' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);;
					$objPHPExcel->getActiveSheet()->getStyle('J' . $j)->applyFromArray($style_row1);

					$objPHPExcel->getActiveSheet()->setCellValue('K' . $j, $row->v_ppn, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->getStyle('K' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);;
					$objPHPExcel->getActiveSheet()->getStyle('K' . $j)->applyFromArray($style_row1);

					$objPHPExcel->getActiveSheet()->setCellValue('L' . $j, $row->v_netto, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->getStyle('L' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);;
					$objPHPExcel->getActiveSheet()->getStyle('L' . $j)->applyFromArray($style_row1);

					$objPHPExcel->getActiveSheet()->setCellValue('M' . $j, $row->v_sisa, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->getStyle('M' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);;
					$objPHPExcel->getActiveSheet()->getStyle('M' . $j)->applyFromArray($style_row1);

					$objPHPExcel->getActiveSheet()->setCellValue('N' . $j, $row->n_supplier_toplength, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('N' . $j)->applyFromArray($style_row);					

					$objPHPExcel->getActiveSheet()->setCellValue('O' . $j, $row->i_pajak , Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('O' . $j)->applyFromArray($style_row);					

					$j++;

				}

				$objPHPExcel->getActiveSheet()->setCellValue('H' . $j, $vtgross);
				$objPHPExcel->getActiveSheet()->getStyle('H' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
				$objPHPExcel->getActiveSheet()->getStyle('H' . $j)->applyFromArray($style_row1);

				$objPHPExcel->getActiveSheet()->setCellValue('I' . $j, $vtdisc);
				$objPHPExcel->getActiveSheet()->getStyle('I' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
				$objPHPExcel->getActiveSheet()->getStyle('I' . $j)->applyFromArray($style_row1);

				$objPHPExcel->getActiveSheet()->setCellValue('J' . $j, $vtdpp);
				$objPHPExcel->getActiveSheet()->getStyle('J' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
				$objPHPExcel->getActiveSheet()->getStyle('J' . $j)->applyFromArray($style_row1);

				$objPHPExcel->getActiveSheet()->setCellValue('K' . $j, $vtppn);
				$objPHPExcel->getActiveSheet()->getStyle('K' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
				$objPHPExcel->getActiveSheet()->getStyle('K' . $j)->applyFromArray($style_row1);

				$objPHPExcel->getActiveSheet()->setCellValue('L' . $j, $vtnetto);
				$objPHPExcel->getActiveSheet()->getStyle('L' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
				$objPHPExcel->getActiveSheet()->getStyle('L' . $j)->applyFromArray($style_row1);

				$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

				$nama_file = 'Pembelian_Akunting_Periode_' . $iperiode . '.xls';

				// Proses file excel    
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment; filename=' . $nama_file . ''); // Set nama file excel nya    
				header('Cache-Control: max-age=0');

				$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save('php://output', 'w');

				$this->logger->writenew('Export Pembelian Akunting Periode : ' . $iperiode);
			}
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu513') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->title;

			$this->load->view('exp-pembelian-akunting/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
