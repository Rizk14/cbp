<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu409')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exp-kb');
			$data['datefrom']='';
			$data['dateto']	='';
			$this->load->view('exp-kb-allarea/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu409')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-kb-allarea/mmaster');
			$datefrom = $this->input->post('datefrom');
			$dateto	  = $this->input->post('dateto');
#      $no   	  = $this->input->post('no');
			if($datefrom!=''){
				$tmp=explode("-",$datefrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$datefrom=$th."-".$bl."-".$hr;
				$periodeawal	= $hr."/".$bl."/".$th;
			}
			$tmp  = explode("-", $dateto);
			$det	= $tmp[0];
			$mon	= $tmp[1];
			$yir 	= $tmp[2];
			$dtos	= $yir."/".$mon."/".$det;
			$periodeakhir	= $det."/".$mon."/".$yir;
			$dtos	= $this->mmaster->dateAdd("d",1,$dtos);
			$tmp 	= explode("-", $dtos);
			$det1	= $tmp[2];
			$mon1	= $tmp[1];
			$yir1 	= $tmp[0];
			$dtos	= $yir1."-".$mon1."-".$det1;
			$data['page_title'] = $this->lang->line('exp-kb');
			$this->db->select("	vc, d_bukti, i_kb, e_description, i_coa, i_area, v_kb, f_debet, d_kb, i_kendaraan, n_km
								from(
								select  a.d_bukti, a.i_kb, a.e_description, a.i_coa, b.i_rv as vc, a.i_area, a.v_kb, a.f_debet, a.d_kb, a.i_kendaraan, a.n_km
								from tr_area e, tm_kb a
								left join tm_rv_item b on (a.i_kb=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='01')
								inner join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv) 
								where a.i_area=e.i_area and a.d_kb >= '$datefrom' and a.d_kb <= '$dtos'  and a.f_kb_cancel='f' 
								union all
								select  a.d_bukti, a.i_kb, a.e_description, a.i_coa, b.i_pv as vc, a.i_area, a.v_kb, a.f_debet, a.d_kb, a.i_kendaraan, a.n_km
								from tr_area e, tm_kb a
								left join tm_pv_item b on (a.i_kb=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='01')
								inner join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv) 
								where  a.i_area=e.i_area and a.d_kb >= '$datefrom' and a.d_kb <= '$dtos'  and a.f_kb_cancel='f' 
								) as a
								order by a.i_area, a.d_kb, a.i_kb",false);
			$query=$this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Kas Besar")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:A4'
				);
        
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(22);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);
#        if($iarea=='00'){
#  				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);
#        }	

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'LAPORAN KAS BESAR');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,12,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$periodeawal.' - '.$periodeakhir);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,12,3);
#        $objPHPExcel->getActiveSheet()->setCellValue('A4', 'No : '.$no);
#				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,4,12,4);
				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl Trans');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Tgl Bukti');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'No Reff');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'NO Perk');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'NO Perk Asal');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', '');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', '');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Debet');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L6', 'Kredit');
				$objPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M6', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('M6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N6', 'Voucher');
				$objPHPExcel->getActiveSheet()->getStyle('N6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        $objPHPExcel->getActiveSheet()->getStyle('A6:N6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
#        if($iarea=='00'){
#				  $objPHPExcel->getActiveSheet()->setCellValue('N6', 'No. Voucher');
#				  $objPHPExcel->getActiveSheet()->getStyle('N6')->applyFromArray(
#					  array(
#						  'borders' => array(
#							  'top' 	=> array('style' => Style_Border::BORDER_THIN),
#							  'bottom'=> array('style' => Style_Border::BORDER_THIN),
#							  'left'  => array('style' => Style_Border::BORDER_THIN),
#							  'right' => array('style' => Style_Border::BORDER_THIN)
#						  ),
#					  )
#				  );
#        }
				$i=7;
				$j=7;
				$xarea='';
				$saldo=0;

				foreach($query->result() as $row){
					$periode=substr($datefrom,0,4).substr($datefrom,5,2);
					
					if($row->i_area!=$xarea)
					{
    				$j=7;
						$saldo	=$this->mmaster->bacasaldo($row->i_area,$periode,$datefrom);
            if($i!=7){
              $i++;
				      $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'No');
				      $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      )

					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, 'Area');
				      $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Tgl Trans');
				      $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Tgl Bukti');
				      $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, 'No Reff');
				      $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, 'Keterangan');
				      $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, 'NO Perk');
				      $objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, 'NO Perk Asal');
				      $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, '');
				      $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, '');
				      $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );

				      $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, 'Debet');
				      $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, 'Kredit');
				      $objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, 'Saldo');
				      $objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, 'Voucher');
				      $objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
            }
            if($i!=7){
              $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':N'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
              $i++;
            }


						$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
					
#						$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, "Saldo Awal Area ".$row->i_area);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
						$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
            $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
            $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
            $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $saldo);
						$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								),
							)
						);
#            if($iarea=='00'){
#						  $objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray(
#							  array(
#								  'borders' => array(
#									  'top' 	=> array('style' => Style_Border::BORDER_THIN),
#									  'bottom'=> array('style' => Style_Border::BORDER_THIN),
#									  'left'  => array('style' => Style_Border::BORDER_THIN),
#									  'right' => array('style' => Style_Border::BORDER_THIN)
#								  ),
#							  )
#						  );
#            }
						$i++;
					}
					$xarea=$row->i_area;
					if($row->f_debet=='f')
					{
						$debet =$row->v_kb;
						$kredit=0;
					}else{
						$kredit=$row->v_kb;
						$debet =0;
					}
					$saldo=$saldo+$debet-$kredit;
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j-6);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					if($row->d_kb!=''){
						$tmp=explode("-",$row->d_kb);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$row->d_kb=$hr."-".$bl."-".$th;
					}
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_area);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_kb);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					if($row->d_bukti!=''){
						$tmp=explode("-",$row->d_bukti);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$row->d_bukti=$hr."-".$bl."-".$th;
					}
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->d_bukti);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->i_kb);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->e_description);
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->i_coa);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
           if(strlen($row->i_coa)==5){
			        $coa  = $row->i_coa.$row->i_area;
		        }else{
              $coa  = substr($row->i_coa,0,6).$row->i_area;
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $coa);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          }
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->i_kendaraan);
					$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->n_km);
					$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $debet);
					$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $kredit);
					$objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $saldo);
					$objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $row->vc);
					$objPHPExcel->getActiveSheet()->getStyle('N'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
#          if($iarea=='00'){
#					  $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $row->i_bukti_pengeluaran);
#					  $objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray(
#						  array(
#							  'borders' => array(
#								  'top' 	=> array('style' => Style_Border::BORDER_THIN),
#								  'bottom'=> array('style' => Style_Border::BORDER_THIN),
#								  'left'  => array('style' => Style_Border::BORDER_THIN),
#								  'right' => array('style' => Style_Border::BORDER_THIN)
#							  ),
#						  )
#					  );
#          }
					$i++;
					$j++;
				}
				$x=$i-1;
				$objPHPExcel->getActiveSheet()->getStyle('G7:H7'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
				$objPHPExcel->getActiveSheet()->getStyle('K7:N'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
/*        if($row->i_area=='00'){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => true
					    )
				    ),
				    'A7:M'.$x
			    );
			    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						    'wrap'      => true
					    )
				    ),
				    'J7:M'.$x
			    );
        }else{
*/
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => true
					    )
				    ),
				    'A7:N'.$x
			    );
			    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						    'wrap'      => true
					    )
				    ),
				    'K7:N'.$x
			    );
#        }
			}
#      if($iarea=='00'){
#	  		$objPHPExcel->getActiveSheet()->getStyle('A6:M6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB#('FFFFFF00');
#      }else{
  			$objPHPExcel->getActiveSheet()->getStyle('A6:N6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
#      }
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='KB-all-'.substr($datefrom,5,2).'-'.substr($datefrom,0,4).'.xls';
      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        @unlink('excel/00/'.$nama);
      }
			$objWriter->save('excel/00/'.$nama); 

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
      }
      $pesan='Export LKH tanggal:'.$datefrom.' sampai:'.$dateto;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses'] = true;
			$data['inomor']	= "Kas Besar";
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu409')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-kb-allarea/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-kb-allarea/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('exp-kb-allarea/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu409')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area1');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/exp-kb-allarea/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-kb-allarea/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('exp-kb-allarea/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
