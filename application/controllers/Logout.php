<?php 

class Logout extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!$this->dgu->is_logged_in()) {redirect('login');}
	}

	// --------------------------------------------------------------------

	function index()
	{
		$data['page_title'] = $this->lang->line('login_logout');
		$this->load->view('logout/index', $data);
	}

	// --------------------------------------------------------------------

	function logout_routine()
	{
		$sess=$this->session->userdata('session_id');
		$id=$this->session->userdata('user_id');
		$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		$rs		= pg_query($sql);
		if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
				$ip_address	  = $row['ip_address'];
				break;
			}
		}else{
			$ip_address='kosong';
		}
		$query 	= pg_query("SELECT current_timestamp as c");
    while($row=pg_fetch_assoc($query)){
    	$now	  = $row['c'];
		}
		$pesan='Log out';
		$this->load->model('logger');
		$this->logger->write($id, $ip_address, $now , $pesan );  
		$this->session->sess_destroy();
		redirect('start');
	}

	// --------------------------------------------------------------------

	function confirm()
    {
		$data['page_title'] = $this->lang->line('login_logout');
		$this->load->view('logout/logout_confirm', $data);
    }
	
}
?>
