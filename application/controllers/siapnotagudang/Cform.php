<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu87')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/siapnotagudang/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$area	= $this->session->userdata('i_area');
			$query = $this->db->query(" 	select distinct(a.i_spb||a.i_area),a.*, b.e_customer_name,c.e_area_name
                                    from tm_spb a, tr_customer b, tr_area c, 
                                    tm_spb_item d left join tm_op e on(d.i_op=e.i_op and e.f_op_close='t')
                                    where a.i_customer=b.i_customer 
                                    and a.i_area=c.i_area
                                    and a.f_spb_cancel='f' 
                                    and a.f_spb_valid='f' 
                                    and a.f_spb_siapnotagudang='f'
                                    and a.f_spb_siapnotasales='f'
                                    and a.f_spb_pemenuhan='t'
                                    and a.i_store='AA'
                                    and a.i_spb=d.i_spb and a.i_area=d.i_area
				                      and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
			                              or upper(a.i_spb) like '%$cari%')
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
							or upper(a.i_spb) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['ada']	= 'xx';
      $data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('siapsj');
			$this->load->model('siapnotagudang/mmaster');
			$data['isi']=$this->mmaster->bacasemua($area,$cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('siapnotagudang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function closing()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu87')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml = $this->input->post('jml', TRUE);
			$this->db->trans_begin();
			$this->load->model('siapnotagudang/mmaster');
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$ispb = $this->input->post('ispb'.$i, TRUE);
					$iarea= $this->input->post('iarea'.$i, TRUE);
					$this->mmaster->updatespb($ispb, $iarea);
				}
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='SPB Siap Nota Gudang No:'.$ispb.' Area:'.$iarea;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );

			}
			$area	= $this->session->userdata('i_area');
			$config['base_url'] = base_url().'index.php/siapnotagudang/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));

			$query = $this->db->query(" select distinct(a.i_spb||a.i_area),a.*, b.e_customer_name,c.e_area_name
                                  from tm_spb a, tr_customer b, tr_area c, 
                                  tm_spb_item d left join tm_op e on(d.i_op=e.i_op and e.f_op_close='t')
                                  where a.i_customer=b.i_customer 
                                  and a.i_area=c.i_area
                                  and a.f_spb_cancel='f' 
                                  and a.f_spb_valid='f' 
                                  and a.f_spb_siapnotagudang='f'
                                  and a.f_spb_siapnotasales='f'
                                  and a.f_spb_pemenuhan='t'
                                  and a.i_store='AA'
                                  and a.i_spb=d.i_spb and a.i_area=d.i_area
				                          and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
			                            or upper(a.i_spb) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['ada']	= 'xx';
      $data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('siapsj');
			$this->load->model('siapnotagudang/mmaster');
			$data['isi']=$this->mmaster->bacasemua($area,$cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('siapnotagudang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu87')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('siapsj');
			$this->load->view('siapnotagudang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
	    	($this->session->userdata('menu87')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	= $this->session->userdata('i_area');
			$config['base_url'] = base_url().'index.php/siapnotagudang/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" select distinct(a.i_spb||a.i_area),a.*, b.e_customer_name,c.e_area_name
                                  from tm_spb a, tr_customer b, tr_area c, 
                                  tm_spb_item d left join tm_op e on(d.i_op=e.i_op and e.f_op_close='t')
                                  where a.i_customer=b.i_customer 
                                  and a.i_area=c.i_area
                                  and a.f_spb_cancel='f' 
                                  and a.f_spb_valid='f' 
                                  and a.f_spb_siapnotagudang='f'
                                  and a.f_spb_siapnotasales='f'
                                  and a.f_spb_pemenuhan='t'
                                  and a.i_store='AA'
                                  and a.i_spb=d.i_spb and a.i_area=d.i_area
			                            and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
		                              or upper(a.i_spb) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationx->initialize($config);
			$this->load->model('siapnotagudang/mmaster');
			$data['ada']	= 'xx';
      $data['cari'] = $cari;
			$data['isi']=$this->mmaster->cari($area,$cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('siapsj');
			$data['iop']='';
	 		$this->load->view('siapnotagudang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu87')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spb');
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
			  )
			{
				$ispb 	= $this->uri->segment(4);
				$iarea	= $this->uri->segment(5);
				$query 	= $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem']	= $query->num_rows(); 				
				$data['ispb'] 		= $ispb;
				$data['departement']= $this->session->userdata('departement');
				$this->load->model('siapnotagudang/mmaster');
				$data['ada']	= '';
      	$data['cari'] = '';
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea);
		 		$this->load->view('siapnotagudang/vmainform',$data);
			}else{
				$this->load->view('siapnotagudang/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
