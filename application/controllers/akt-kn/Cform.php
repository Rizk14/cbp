<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu123')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-kn/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select a.*, b.e_customer_name, b.e_customer_address, c.e_area_name, d.i_customer_groupar, e.e_salesman_name
										from tm_kn a, tr_customer b, tr_area c, tr_customer_groupar d, tr_salesman e
										where a.i_customer=b.i_customer 
										  and b.i_customer=d.i_customer
										  and a.i_salesman=e.i_salesman
										  and a.i_area=c.i_area 
										  and a.f_posting = 'f'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_kn');
			$this->load->model('akt-kn/mmaster');
			$data['ikn']	= '';
			$data['iarea']	= '';
			$data['nknyear']= '';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('akt-kn/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu123')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('list_nota');
			$this->load->view('akt-kn/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu123')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/akt-kn/cform/index/';
			$config['per_page'] = '10';
			$limo=$config['per_page'];
			$ofso=$this->uri->segment(4);
			if($ofso=='')
				$ofso=0;
			$query = $this->db->query(" select a.*, b.e_customer_name, b.e_customer_address, c.e_area_name, d.i_customer_groupar, e.e_salesman_name
										from tm_kn a, tr_customer b, tr_area c, tr_customer_groupar d, tr_salesman e
										where a.i_customer=b.i_customer 
										  and b.i_customer=d.i_customer
										  and a.i_salesman=e.i_salesman
										  and a.i_area=c.i_area 
										  and a.f_posting = 'f'
										  and (upper(a.i_kn) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('akt-kn/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('list_nota');
			$data['inota']='';
	 		$this->load->view('akt-kn/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu123')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('kn');
			if(
				$this->uri->segment(4) && $this->uri->segment(5) && $this->uri->segment(6)
			  ){
				$this->load->model('akt-kn/mmaster');
				$data['ikn'] 			= $this->uri->segment(4);
				$data['nknyear']		= $this->uri->segment(5);
				$data['iarea'] 			= $this->uri->segment(6);
				$ikn 					= $this->uri->segment(4);
				$nknyear				= $this->uri->segment(5);
				$iarea 					= $this->uri->segment(6);
				$data['isi']			= $this->mmaster->bacakn($ikn,$nknyear,$iarea);
		 		$this->load->view('akt-kn/vformapprove',$data);
			}else{
				$this->load->view('akt-kn/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function posting()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu123')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('akt-kn/mmaster');
			$ikn		= $this->input->post('ikn', TRUE);
			$iarea		= $this->input->post('iarea', TRUE);
//			$eremark	= $this->input->post('eremark', TRUE);
			$irefference= $this->input->post('irefference', TRUE);
			$icustomer	= $this->input->post('icustomer', TRUE);
			$ecustomername	= $this->input->post('ecustomername', TRUE);
			$eremark	= "Kredit Nota refferensi:".$irefference."-".$icustomer."-".$ecustomername;
			$disc		= $this->input->post('vdiscount', TRUE);
			$disc		= str_replace(',','',$disc);
			$gros		= $this->input->post('vgross', TRUE);
			$gros		= str_replace(',','',$gros);
			$nett		= $this->input->post('vnetto', TRUE);
			$nett		= str_replace(',','',$nett);
			$dkn		= $this->input->post('dkn', TRUE);
			if($dkn!=''){
				$tmp=explode("-",$dkn);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkn=$th."-".$bl."-".$hr;
				$nknyear=$th;
				$iperiode=$th.$bl;
			}
			$fclose			= 'f';
			$this->db->trans_begin();
			$this->mmaster->inserttransheader($ikn,$iarea,$eremark,$fclose,$dkn);
			$this->mmaster->updatekn($ikn,$iarea,$nknyear);
			$accdebet		= '412.100';
			$namadebet		= $this->mmaster->namaacc($accdebet);
			$accdebet2		= '412.200';
			$namadebet2		= $this->mmaster->namaacc($accdebet2);
			$acckredit		= '112.2'.$iarea;
			$namakredit		= $this->mmaster->namaacc($acckredit);
			
			$this->mmaster->inserttransitemdebet($accdebet,$ikn,$namadebet,'t','t',$iarea,$eremark,$nett,$dkn);
			$this->mmaster->updatesaldodebet($accdebet,$iperiode,$nett);
			if($disc!='' && $disc!=0 && $disc!='0'){
				$this->mmaster->inserttransitemdebet($accdebet2,$ikn,$namadebet2,'t','t',$iarea,$eremark,$disc,$dkn);
				$this->mmaster->updatesaldodebet($accdebet2,$iperiode,$disc);
			}
			$this->mmaster->inserttransitemkredit($acckredit,$ikn,$namakredit,'f','t',$iarea,$eremark,$gros,$dkn);
			$this->mmaster->updatesaldokredit($acckredit,$iperiode,$gros);
			$this->mmaster->insertgldebet($accdebet,$ikn,$namadebet,'t',$iarea,$nett,$dkn,$eremark);
			if($disc!='' && $disc!=0 && $disc!='0'){
				$this->mmaster->insertgldebet($accdebet2,$ikn,$namadebet2,'t',$iarea,$disc,$dkn,$eremark);
			}
			$this->mmaster->insertglkredit($acckredit,$ikn,$namakredit,'f',$iarea,$gros,$dkn,$eremark);
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='Posting Kredit Nota No:'.$ikn.' Area:'.$iarea;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses'] = true;
				$data['inomor']	= $ikn;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
