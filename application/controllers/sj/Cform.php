<?php 
class Cform extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
    }
    public function index()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu86') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $iarea1 = $this->session->userdata('i_area');
            $isjold = $this->input->post('isjold', true);
            $dsj = $this->input->post('dsj', true);
            if ($dsj != '') {
                $tmp = explode("-", $dsj);
                $th = $tmp[2];
                $bl = $tmp[1];
                $hr = $tmp[0];
                $dsj = $th . "-" . $bl . "-" . $hr;
                $thbl = $th . $bl;
            }
            $iarea = $this->input->post('iarea', true);
            $ispb = $this->input->post('ispb', true);
            $dspb = $this->input->post('dspb', true);
            if ($dspb != '') {
                $tmp = explode("-", $dspb);
                $th = $tmp[2];
                $bl = $tmp[1];
                $hr = $tmp[0];
                $dspb = $th . "-" . $bl . "-" . $hr;
            }
            $eareaname = $this->input->post('eareaname', true);
            ##########################################
            $isalesman = $this->input->post('isalesman', true);
            $icustomer = $this->input->post('icustomer', true);
            $nsjdiscount1 = $this->input->post('nsjdiscount1', true);
            $nsjdiscount1 = str_replace(',', '', $nsjdiscount1);
            $nsjdiscount2 = $this->input->post('nsjdiscount2', true);
            $nsjdiscount2 = str_replace(',', '', $nsjdiscount2);
            $nsjdiscount3 = $this->input->post('nsjdiscount3', true);
            $nsjdiscount3 = str_replace(',', '', $nsjdiscount3);
            $vsjdiscount1 = $this->input->post('vsjdiscount1', true);
            $vsjdiscount1 = str_replace(',', '', $vsjdiscount1);
            $vsjdiscount2 = $this->input->post('vsjdiscount2', true);
            $vsjdiscount2 = str_replace(',', '', $vsjdiscount2);
            $vsjdiscount3 = $this->input->post('vsjdiscount3', true);
            $vsjdiscount3 = str_replace(',', '', $vsjdiscount3);
            $vsjdiscounttotal = $this->input->post('vsjdiscounttotal', true);
            $vsjdiscounttotal = str_replace(',', '', $vsjdiscounttotal);
            $vsjppn = $this->input->post('vsjppn', true);
            $vsjppn = str_replace(',', '', $vsjppn);
            $vsjgross = $this->input->post('vsjgross', true);
            $vsjgross = str_replace(',', '', $vsjgross);
            $vsjnetto = $this->input->post('vsjnetto', true);
            $vsjnetto = str_replace(',', '', $vsjnetto);
            $ntop = $this->input->post('ntop', true);
            ##########################################
            $jml = $this->input->post('jml', true);
            $telat1 = $this->input->post('etelat', true);
            $telat2 = $this->input->post('etelat2', true);
            $alasan = $this->input->post('alasan', true);

            if ($telat1 == '') {
                $etelat = $telat2;
            } else {
                $etelat = $telat1;
            }

            if ($dsj != '' && $eareaname != '') {
                $gaono = true;
                for ($i = 1; $i <= $jml; $i++) {
                    $cek = $this->input->post('chk' . $i, true);
                    if ($cek == 'on') {
                        $gaono = false;
                    }
                    if (!$gaono) {
                        break;
                    }

                }
                if (!$gaono) {
                    $this->db->trans_begin();
                    $this->load->model('sj/mmaster');
                    $istore = $this->input->post('istore', true);
                    $kons = $this->mmaster->cekkons($ispb, $iarea);
                    if ($istore == 'AA') {
                        $istorelocation = '01';
                    } else {
                        if ($kons == 't') {
                            if ($istore == 'PB') {
                                $istorelocation = '00';
                            } else {
                                if ($istore == '03' || $istore == '04' || $istore == '05' || $istore == '12') {
                                    $istorelocation = 'PB';
                                } else {
                                    $istorelocation = '00';
                                }
                            }
                        } else {
                            $istorelocation = '00';
                        }
                    }
                    $istorelocationbin = '00';
                    $eremark = 'SPB';
                    $isjtype = '04';
                    $typearea = $this->mmaster->cekdaerah($ispb, $iarea);

                    if ($typearea == 't') {
                        $areasj = $iarea;
                    } else {
                        $areasj = '00';
                    }

                    if ($iarea1 == '00' && $iarea1 != $iarea) {
                        $fentpusat = 't';
                        $iareareff = $iarea;
                        $areanumsj = $iarea;
                    } elseif ($iarea1 != '00' && $iarea1 == $iarea) {
                        $fentpusat = 'f';
                        $iareareff = $iarea1;
                        $areanumsj = $iarea1;
                    } else {
                        $fentpusat = 'f';
                        $iareareff = $iarea1;
                        $areanumsj = $iarea1;
                    }
                    $adasj = $this->mmaster->ceksj($ispb, $iarea);
                    if (!$adasj) {
                        $isj = $this->mmaster->runningnumbersj($areasj, $thbl, $kons);
                        
                        $this->mmaster->insertsjheader($ispb, $dspb, $isj, $dsj, $iarea, $isalesman, $icustomer, $nsjdiscount1, $nsjdiscount2, $nsjdiscount3, $vsjdiscount1, $vsjdiscount2, $vsjdiscount3, $vsjdiscounttotal, $vsjgross, $vsjnetto, $isjold, $fentpusat, $iareareff, $ntop, $etelat, $alasan, $vsjppn);

                        $this->mmaster->updatespb($ispb, $iarea, $isj, $dsj);
                        $spb_item = array();
                        $a = 0;
                        for ($i = 1; $i <= $jml; $i++) {
                            $cek = $this->input->post('chk' . $i, true);
                            $vunitprice = $this->input->post('vproductmill' . $i, true);
                            $vunitprice = str_replace(',', '', $vunitprice);
                            $iproduct = $this->input->post('iproduct' . $i, true);
                            $iproductgrade = 'A';
                            $iproductmotif = $this->input->post('motif' . $i, true);
                            $eproductname = $this->input->post('eproductname' . $i, true);
                            $ndeliver = $this->input->post('ndeliver' . $i, true);
                            $ndeliver = str_replace(',', '', $ndeliver);
                            $norder = $this->input->post('norder' . $i, true);
                            $norder = str_replace(',', '', $norder);

                            // * TAMBAHAN 24 MAR 2022
							$itemdiscounttotal = $this->input->post('vdiscount'.$i, true);
							$vdpp              = $this->input->post('vdpp'.$i, true);
							$vppn              = $this->input->post('vppn'.$i, true);
							$vnettoitem        = $this->input->post('vnetto'.$i, true);

                            if ($cek == 'on') {

                                if (($norder - $ndeliver) > 0) {
                                    $spb_item[$a] = array(
                                        'i_product' => $iproduct,
                                        'i_product_grade' => $iproductgrade,
                                        'i_product_motif' => $iproductmotif,
                                        'n_order' => ($norder - $ndeliver),
                                        'n_stock' => 0,
                                        'v_unit_price' => $vunitprice,
                                        'e_product_name' => $eproductname,
                                        'i_op' => '',
                                        'i_area' => $iarea,
                                        'e_remark' => '',
                                        'n_item_no' => 1,
                                        'i_product_status' => '1',
                                        'n_order_lama' => $ndeliver,
                                    );

                                }

                                if ($ndeliver > 0) {
                                    $this->mmaster->insertsjdetail($iproduct, $iproductgrade, $iproductmotif, $eproductname, $ndeliver, $vunitprice, $isj, $iarea, $i, $itemdiscounttotal, $vdpp, $vppn, $vnettoitem);
                                    $this->mmaster->updatespbitem($ispb, $iproduct, $iproductgrade, $iproductmotif, $ndeliver, $iarea, $vunitprice);
                                    ###
                                    #                $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                                    $trans = $this->mmaster->qic($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin);
                                    if (isset($trans)) {
                                        foreach ($trans as $itrans) {
                                            $q_aw = $itrans->n_quantity_stock;
                                            $q_ak = $itrans->n_quantity_stock;
                                            #                    $q_in =$itrans->n_quantity_in;
                                            #                    $q_out=$itrans->n_quantity_out;
                                            $q_in = 0;
                                            $q_out = 0;
                                            break;
                                        }
                                    } else {
                                        $trans = $this->mmaster->qic($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin);
                                        if (isset($trans)) {
                                            foreach ($trans as $itrans) {
                                                $q_aw = $itrans->n_quantity_stock;
                                                $q_ak = $itrans->n_quantity_stock;
                                                $q_in = 0;
                                                $q_out = 0;
                                                break;
                                            }
                                        } else {
                                            $q_aw = 0;
                                            $q_ak = 0;
                                            $q_in = 0;
                                            $q_out = 0;
                                        }
                                    }
                                    $this->mmaster->inserttrans04($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $eproductname, $isj, $q_in, $q_out, $ndeliver, $q_aw, $q_ak);
                                    $th = substr($dsj, 0, 4);
                                    $bl = substr($dsj, 5, 2);
                                    $emutasiperiode = $th . $bl;

                                    if ($this->mmaster->cekmutasi($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $emutasiperiode)) {
                                        $this->mmaster->updatemutasi4($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $ndeliver, $emutasiperiode);
                                    } else {
                                        $this->mmaster->insertmutasi4($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $ndeliver, $emutasiperiode, $q_aw);
                                    }
                                    if ($this->mmaster->cekic($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin)) {
                                        $this->mmaster->updateic4($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $ndeliver, $q_ak);
                                    } else {
                                        $this->mmaster->insertic4($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $eproductname, $ndeliver, $q_aw);
                                    }
                                    ###
                                } else {
                                    $this->mmaster->updatespbitem($ispb, $iproduct, $iproductgrade, $iproductmotif, 'NULL', $iarea, $vunitprice);

                                    $spb_item[$a] = array(
                                        'i_product' => $iproduct,
                                        'i_product_grade' => $iproductgrade,
                                        'i_product_motif' => $iproductmotif,
                                        'n_order' => $norder,
                                        'n_stock' => 0,
                                        'v_unit_price' => $vunitprice,
                                        'e_product_name' => $eproductname,
                                        'i_op' => '',
                                        'i_area' => $iarea,
                                        'e_remark' => '',
                                        'n_item_no' => 1,
                                        'i_product_status' => '1',
                                        'n_order_lama' => 0,
                                    );
                                }
                            } else {
                                $iproduct = $this->input->post('iproduct' . $i, true);
                                $iproductgrade = 'A';
                                $iproductmotif = $this->input->post('motif' . $i, true);
                                $this->mmaster->updatespbitem($ispb, $iproduct, $iproductgrade, $iproductmotif, 'NULL', $iarea, $vunitprice);

                                $spb_item[$a] = array(
                                    'i_product' => $iproduct,
                                    'i_product_grade' => $iproductgrade,
                                    'i_product_motif' => $iproductmotif,
                                    'n_order' => $norder,
                                    'n_stock' => 0,
                                    'v_unit_price' => $vunitprice,
                                    'e_product_name' => $eproductname,
                                    'i_op' => '',
                                    'i_area' => $iarea,
                                    'e_remark' => '',
                                    'n_item_no' => 1,
                                    'i_product_status' => '1',
                                    'n_order_lama' => 0,
                                );

                            }
                            $a++;
                        }

                        //* End FOR
                        $ispb_baru = '';
                        if (($alasan == "" || $alasan == null)) {
                            #if($iarea == '01' || $iarea == '05' || $iarea == '24' || $iarea == '51' || $iarea == '55' || $iarea == '74'){
                            // UNTUK UPDATE SJ
                            date_default_timezone_set("Asia/Jakarta");
                            $today = strtotime(date("Y-m-d"));
                            $end = strtotime("2020-06-16");
                            if ($today > $end) {
                                // * Buat SPB BARU * //
                                if (count($spb_item) > 0) {

                                    $this->load->model('spbbaby/mmaster2');

                                    $this->db->trans_begin();

                                    $ispb_baru = $this->mmaster2->runningnumber($iarea, $thbl);

                                    $data_spb_lama = $this->db->query("select * from tm_spb where i_spb = '$ispb' and i_area = '$iarea' and i_customer = '$icustomer'")->row();
                                    $dspb = $dsj;
                                    $ispbpo = $ispb;
                                    $nspbtoplength = $data_spb_lama->n_spb_toplength;
                                    $isalesman = $data_spb_lama->i_salesman;
                                    $ipricegroup = $data_spb_lama->i_price_group;
                                    $iproductgroup = $data_spb_lama->i_product_group;
                                    $dspbreceive = $data_spb_lama->d_spb_receive;
                                    $fspbop = $data_spb_lama->f_spb_op;
                                    $ecustomerpkpnpwp = $data_spb_lama->e_customer_pkpnpwp;
                                    $fspbpkp = $data_spb_lama->f_spb_pkp;
                                    $fspbplusppn = $data_spb_lama->f_spb_plusppn;
                                    $fspbplusdiscount = $data_spb_lama->f_spb_plusdiscount;
                                    $fspbstockdaerah = $data_spb_lama->f_spb_stockdaerah;
                                    $fspbvalid = $data_spb_lama->f_spb_valid;
                                    $fspbsiapnotagudang = 'f';
                                    $fspbcancel = 'f';
                                    $nspbdiscount1 = $data_spb_lama->n_spb_discount1;
                                    $nspbdiscount2 = $data_spb_lama->n_spb_discount2;
                                    $nspbdiscount3 = $data_spb_lama->n_spb_discount3;
                                    $vspb = 0;
                                    $fspbconsigment = $data_spb_lama->f_spb_consigment;
                                    $ispbold = $data_spb_lama->i_spb_old;
                                    $eremarkx = '';
                                    $fspbprogram = $data_spb_lama->f_spb_program;
                                    $no = 1;
                                    foreach ($spb_item as $row) {
                                        $iproduct = $row['i_product'];
                                        $iproductgrade = $row['i_product_grade'];
                                        $eproductname = $row['e_product_name'];
                                        $norder = $row['n_order'];
                                        $vunitprice = $row['v_unit_price'];
                                        $iproductmotif = $row['i_product_motif'];
                                        $eremark = $row['e_remark'];

                                        $vspb = $vspb + ($vunitprice * $norder);

                                        $iproductstatus = '1';
                                        $this->mmaster2->insertdetail($ispb_baru, $iarea, $iproduct, $iproductstatus, $iproductgrade, $eproductname, $norder, null,
                                            $vunitprice, $iproductmotif, $eremark, $no);

                                        $order_baru = $row['n_order_lama'];
                                        $harga = ($vunitprice * $norder);
                                        $this->db->query("update tm_spb_item set n_order = '$order_baru' where i_spb = '$ispb' and i_area = '$iarea' and i_product = '$iproduct'");
                                        $no++;
                                    }

                                    $vspbdiscount1 = ($vspb * $nspbdiscount1) / 100;
                                    $vspbdiscount2 = (($vspb - $vspbdiscount1) * $nspbdiscount2) / 100;
                                    $vspbdiscount3 = (($vspb - $vspbdiscount1 - $vspbdiscount2) * $nspbdiscount3) / 100;
                                    $vspbdiscounttotal = $vspbdiscount1 + $vspbdiscount2 + $vspbdiscount3;

                                    $this->mmaster2->insertheader($ispb_baru, $dspb, $icustomer, $iarea, $ispbpo, $nspbtoplength, $isalesman,
                                        $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp,
                                        $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
                                        $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1,
                                        $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
                                        $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold, $eremarkx, $iproductgroup);

                                    // * Update SPB lama //

                                    $data_spb_lama = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area = '$iarea'")->result();

                                    $vspb = 0;
                                    foreach ($data_spb_lama as $row) {
                                        $norder = $row->n_order;
                                        $vunitprice = $row->v_unit_price;
                                        $vspb = $vspb + ($vunitprice * $norder);
                                    }

                                    $vspbdiscount1 = ($vspb * $nspbdiscount1) / 100;
                                    $vspbdiscount2 = (($vspb - $vspbdiscount1) * $nspbdiscount2) / 100;
                                    $vspbdiscount3 = (($vspb - $vspbdiscount1 - $vspbdiscount2) * $nspbdiscount3) / 100;
                                    $vspbdiscounttotal = $vspbdiscount1 + $vspbdiscount2 + $vspbdiscount3;
                                    $v_spb_after = ($vspb - $vspbdiscounttotal);

                                    $this->db->query("update tm_spb set v_spb_discount1 = '$vspbdiscount1', v_spb_discount2 = '$vspbdiscount2', v_spb_discount3 = '$vspbdiscount3',
																										v_spb_discounttotal = '$vspbdiscounttotal', v_spb_discounttotalafter ='$vspbdiscounttotal', v_spb = '$vspb', v_spb_after = '$v_spb_after'
																										where i_spb = '$ispb' and i_area = '$iarea'");

                                    if (($this->db->trans_status() === false)) {
                                        $this->db->trans_rollback();
                                    } else {
                                        $this->db->trans_commit();
                                        $sess = $this->session->userdata('session_id');
                                        $id = $this->session->userdata('user_id');
                                        $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                                        $rs = pg_query($sql);
                                        if (pg_num_rows($rs) > 0) {
                                            while ($row = pg_fetch_assoc($rs)) {
                                                $ip_address = $row['ip_address'];
                                                break;
                                            }
                                        } else {
                                            $ip_address = 'kosong';
                                        }
                                        $query = pg_query("SELECT current_timestamp as c");
                                        while ($row = pg_fetch_assoc($query)) {
                                            $now = $row['c'];
                                        }
                                        $pesan = 'Create SPB Turunan No : '.$ispb_baru." No Ref. ".$ispb." Area : ".$iarea;
                                        $this->load->model('logger');
                                        $this->logger->write($id, $ip_address, $now, $pesan);
                                    }
                               } //End Buat SPB Baru
                            } //End kondisi area
                        } // End if alasan
                    }

                    if (($this->db->trans_status() === false)) {
                        $this->db->trans_rollback();
                    } elseif (!$adasj) {
                        // $this->db->trans_rollback();
                        $this->db->trans_commit();
                        $sess = $this->session->userdata('session_id');
                        $id = $this->session->userdata('user_id');
                        $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                        $rs = pg_query($sql);
                        if (pg_num_rows($rs) > 0) {
                            while ($row = pg_fetch_assoc($rs)) {
                                $ip_address = $row['ip_address'];
                                break;
                            }
                        } else {
                            $ip_address = 'kosong';
                        }
                        $query = pg_query("SELECT current_timestamp as c");
                        while ($row = pg_fetch_assoc($query)) {
                            $now = $row['c'];
                        }
                        $pesan = 'Input SJ No:' . $isj;
                        $this->load->model('logger');
                        $this->logger->write($id, $ip_address, $now, $pesan);
                        if ($ispb_baru != '') {
                            $ispb_baru = $ispb_baru . ' ( Silahkan cek ulang keterangan warna di spb baru ! )';
                        }
                        $data['sukses'] = true;
                        $data['inomor'] = $isj . ' - ' . $ispb_baru;
                        $this->load->view('nomor', $data);
                    }
                }
            } else {
                $data['page_title'] = $this->lang->line('sj');
                $data['isj'] = '';
                $data['isi'] = '';
                $data['detail'] = "";
                $data['jmlitem'] = "";
                $data['dsj'] = '';
                $data['ispb'] = '';
                $data['dspb'] = '';
                if ($this->uri->segment(5) != '') {
                    $data['iarea'] = $this->uri->segment(5);
                    $iarea = $this->uri->segment(5);
                    $data['istore'] = $this->uri->segment(6);
                    $data['eareaname'] = str_replace('%20', ' ', $this->uri->segment(4));
                } else {
                    $data['iarea'] = '';
                    $iarea = '';
                    $data['istore'] = '';
                    $data['eareaname'] = '';
                }
                $data['isjold'] = '';
                $data['vsjgross'] = '';
                $data['nsjdiscount1'] = '';
                $data['nsjdiscount2'] = '';
                $data['nsjdiscount3'] = '';
                $data['vsjdiscount1'] = '';
                $data['vsjdiscount2'] = '';
                $data['vsjdiscount3'] = '';
                $data['vsjdiscounttotal'] = '';
                $data['vsjnetto'] = '';
                $data['vsjppn'] = '';
                $data['icustomer'] = '';
                $data['ecustomername'] = '';
                $data['isalesman'] = '';
                $data['ntop'] = '';
                $data['fspbconsigment'] = '';
                $data['fspbplusppn'] = '';
                $data['fplusppn'] = '';
                $data['dsj'] = date('Y-m-d');
                $data['tglakhir'] = '';
                $data['dstore'] = '';
#        $data['tglakhirx']='';
                $this->load->view('sj/vmainform', $data);
            }
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function insert_fail()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu86') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $data['page_title'] = $this->lang->line('sj');
            $this->load->view('sj/vinsert_fail', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function edit()
    {
        if (
            (($this->session->userdata('logged_in')))
        ) {
            $periode = '';
            $this->load->model('sj/mmaster');
            $data['page_title'] = $this->lang->line('sj') . " update";
            if ($this->uri->segment(4) != '') {
                $isj = $this->uri->segment(4);
                $iarea = $this->uri->segment(5);
                $dfrom = $this->uri->segment(6);
                $dto = $this->uri->segment(7);
                $ispb = $this->uri->segment(8);
                $iareasj = substr($isj, 8, 2);
                $data['isj'] = $isj;
                $data['iarea'] = $iarea;
                $data['iareasj'] = substr($isj, 8, 2);
                $data['dfrom'] = $dfrom;
                $data['dto'] = $dto;
                $query = $this->db->query("select i_sj from tm_nota_item where i_sj = '$isj'");
                $data['jmlitem'] = $query->num_rows();
                $query = $this->db->query("	select f_spb_consigment
                                  from tm_spb where i_spb='$ispb' and i_area='$iarea' ", false);
                if ($query->num_rows() > 0) {
                    foreach ($query->result() as $row) {
                        $fspbconsigment = $row->f_spb_consigment;
                    }
                }
                $data['isi'] = $this->mmaster->baca($isj, $iarea);
                $data['detail'] = $this->mmaster->bacadetail($isj, $iarea);
                $this->db->select("	a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
									          where a.i_sj ='$isj' and a.i_customer=c.i_customer
									          and a.i_area=b.i_area", false);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    foreach ($query->result() as $row) {
                        $dsj = substr($row->d_sj, 0, 4) . substr($row->d_sj, 5, 2);
                        $data['dsj'] = $row->d_sj;
                        $data['ispb'] = $row->i_spb;
                        $data['dspb'] = $row->d_spb;
#                        if($iareasj=='00'){
                        #                            $data['istore']='AA';
                        #                        }else{
                        #                            $data['istore']=$iareasj;
                        #                        }
                        if ($iareasj == 'BK') {
                            $iareasj = $iarea;
                        }

                        $que = $this->db->query("select i_store from tr_area where i_area='$iareasj'");
                        $st = $que->row();
                        $data['istore'] = $st->i_store;
                        $data['isjold'] = $row->i_sj_old;
                        $data['eareaname'] = $row->e_area_name;
                        $data['vsjgross'] = $row->v_nota_gross;
                        $data['nsjdiscount1'] = $row->n_nota_discount1;
                        $data['nsjdiscount2'] = $row->n_nota_discount2;
                        $data['nsjdiscount3'] = $row->n_nota_discount3;
                        $data['vsjdiscount1'] = $row->v_nota_discount1;
                        $data['vsjdiscount2'] = $row->v_nota_discount2;
                        $data['vsjdiscount3'] = $row->v_nota_discount3;
                        $data['vsjdiscounttotal'] = $row->v_nota_discounttotal;
                        $data['vsjnetto'] = $row->v_nota_netto;
                        $data['icustomer'] = $row->i_customer;
                        $data['ecustomername'] = $row->e_customer_name;
                        $data['isalesman'] = $row->i_salesman;
                        $data['fplusppn'] = $row->f_plus_ppn;
                        $data['ntop'] = $row->n_nota_toplength;
                        $data['fspbconsigment'] = $fspbconsigment;
                        #$data['dsj']        =date('Y-m-d');
                        #$data['tglakhir']='';
                    }
                }

                $data['bisaedit'] = false;
                $query = $this->db->query("	select i_periode from tm_periode ", false);
                if ($query->num_rows() > 0) {
                    foreach ($query->result() as $row) {
                        $periode = $row->i_periode;
                    }
                    if ($periode <= $dsj) {
                        $data['bisaedit'] = true;
                    }

                }

                $this->load->view('sj/vmainform', $data);
            } else {
                $this->load->view('sj/vinsert_fail', $data);
            }
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function update()
    {
        if (
            (($this->session->userdata('logged_in')))
        ) {
            $isj = $this->input->post('isj', true);
            $isjold = $this->input->post('isjold', true);
            $dsj = $this->input->post('dsj', true);
            $iarea = $this->input->post('iarea', true);
            $ispb = $this->input->post('ispb', true);
            $dspb = $this->input->post('dspb', true);
            if ($dsj != '') {
                $tmp = explode("-", $dsj);
                $th = $tmp[2];
                $bl = $tmp[1];
                $hr = $tmp[0];
                $dsj = $th . "-" . $bl . "-" . $hr;
                $thbl = substr($th, 2, 2) . $bl;
                $tmpsj = explode("-", $isj);
                $firstsj = $tmpsj[0];
                $lastsj = $tmpsj[2];
                $newsj = $firstsj . "-" . $thbl . "-" . $lastsj;
            }
            if ($dspb != '') {
                $tmp = explode("-", $dspb);
                $th = $tmp[2];
                $bl = $tmp[1];
                $hr = $tmp[0];
                $dspb = $th . "-" . $bl . "-" . $hr;
            }
            $eareaname = $this->input->post('eareaname', true);
            $isalesman = $this->input->post('isalesman', true);
            $icustomer = $this->input->post('icustomer', true);
            $nsjdiscount1 = $this->input->post('nsjdiscount1', true);
            $nsjdiscount1 = str_replace(',', '', $nsjdiscount1);
            $nsjdiscount2 = $this->input->post('nsjdiscount2', true);
            $nsjdiscount2 = str_replace(',', '', $nsjdiscount2);
            $nsjdiscount3 = $this->input->post('nsjdiscount3', true);
            $nsjdiscount3 = str_replace(',', '', $nsjdiscount3);
            $vsjdiscount1 = $this->input->post('vsjdiscount1', true);
            $vsjdiscount1 = str_replace(',', '', $vsjdiscount1);
            $vsjdiscount2 = $this->input->post('vsjdiscount2', true);
            $vsjdiscount2 = str_replace(',', '', $vsjdiscount2);
            $vsjdiscount3 = $this->input->post('vsjdiscount3', true);
            $vsjdiscount3 = str_replace(',', '', $vsjdiscount3);
            $vsjdiscounttotal = $this->input->post('vsjdiscounttotal', true);
            $vsjdiscounttotal = str_replace(',', '', $vsjdiscounttotal);
            $vsjgross = $this->input->post('vsjgross', true);
            $vsjgross = str_replace(',', '', $vsjgross);
            $vsjnetto = $this->input->post('vsjnetto', true);
            $vsjnetto = str_replace(',', '', $vsjnetto);
            $vsjppn = $this->input->post('vsjppn', true);
            $vsjppn = str_replace(',', '', $vsjppn);
            $jml = $this->input->post('jml', true);

            if ($isj != '' && $dsj != '' && $eareaname != '') {
                $gaono = true;
                for ($i = 1; $i <= $jml; $i++) {
                    $cek = $this->input->post('chk' . $i, true);
                    if ($cek == 'on') {
                        $gaono = false;
                    }
                    if (!$gaono) {
                        break;
                    }

                }
                if (!$gaono) {
                    $this->db->trans_begin();
                    $this->load->model('sj/mmaster');
                    $istore = $this->input->post('istore', true);
                    $kons = $this->mmaster->cekkons($ispb, $iarea);
                    if ($istore == 'AA') {
                        $istorelocation = '01';
                    } else {
                        if ($kons == 't') {
                            if ($istore == 'PB') {
                                $istorelocation = '00';
                            } else {
                                $istorelocation = 'PB';
                            }
                        } else {
                            $istorelocation = '00';
                        }
                    }
                    $istorelocationbin = '00';
                    $eremark = 'SPB';
                    
                    $this->mmaster->updatesjheader($ispb, $dspb, $isj, $dsj, $iarea, $isalesman, $icustomer, $nsjdiscount1, $nsjdiscount2, $nsjdiscount3, $vsjdiscount1, $vsjdiscount2, $vsjdiscount3, $vsjdiscounttotal, $vsjgross, $vsjnetto, $isjold, $vsjppn);

                    $this->mmaster->updatespb($ispb, $iarea, $isj, $dsj);
                    $this->mmaster->updatedkb($vsjnetto, $isj, $iarea);
                    for ($i = 1; $i <= $jml; $i++) {
                        $cek = $this->input->post('chk' . $i, true);
                        $vunitprice = $this->input->post('vproductmill' . $i, true);
                        $vunitprice = str_replace(',', '', $vunitprice);
                        $iproduct = $this->input->post('iproduct' . $i, true);
                        $eproductname = $this->input->post('eproductname' . $i, true);
                        $iproductgrade = 'A';
                        $iproductmotif = $this->input->post('motif' . $i, true);
                        $ntmp = $this->input->post('ntmp' . $i, true);
                        $ntmp = str_replace(',', '', $ntmp);
                        $ndeliver = $this->input->post('ndeliver' . $i, true);
                        $ndeliver = str_replace(',', '', $ndeliver);

                        // * TAMBAHAN 24 MAR 2022
						$itemdiscounttotal = $this->input->post('vdiscount'.$i, true);
						$vdpp              = $this->input->post('vdpp'.$i, true);
						$vppn              = $this->input->post('vppn'.$i, true);
						$vnettoitem        = $this->input->post('vnetto'.$i, true);

                        if ($ntmp != $ndeliver) {
                            $this->mmaster->deletesjdetail($iproduct, $iproductgrade, $iproductmotif, $isj, $iarea);
                            $th = substr($dsj, 0, 4);
                            $bl = substr($dsj, 5, 2);
                            $emutasiperiode = $th . $bl;
#              if( ($ntmp!='') && ($ntmp!=0) ){
                            if (($ntmp > 0)) {
                                $tra = $this->mmaster->deletetrans($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $isj, $ntmp, $eproductname);
                                $this->mmaster->updatemutasi04($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $ntmp, $emutasiperiode);
                                $this->mmaster->updateic04($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $ntmp);
                            }
###############2013 09 26
                            #                        $vunitprice    = $this->input->post('vproductmill'.$i, TRUE);
                            #                        $vunitprice    = str_replace(',','',$vunitprice);
                            if ($cek == 'on') {
                                $ndeliver = $this->input->post('ndeliver' . $i, true);
                                $ndeliver = str_replace(',', '', $ndeliver);
                                $eproductname = $this->input->post('eproductname' . $i, true);
                                $norder = $this->input->post('norder' . $i, true);
                                $norderx = str_replace(',', '', $norder);
                                if ($ndeliver > 0) {
                                    $this->mmaster->insertsjdetail( $iproduct, $iproductgrade, $iproductmotif, $eproductname, $ndeliver, $vunitprice, $isj, $iarea, $i,  $itemdiscounttotal,  $vdpp,  $vppn,  $vnettoitem);
#                  $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                                    $trans = $this->mmaster->qic($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin);
                                    if (isset($trans)) {
                                        foreach ($trans as $itrans) {
                                            $q_aw = $itrans->n_quantity_stock;
                                            $q_ak = $itrans->n_quantity_stock;
#                      $q_in =$itrans->n_quantity_in;
                                            #                      $q_out=$itrans->n_quantity_out;
                                            $q_in = 0;
                                            $q_out = 0;
                                            break;
                                        }
                                    } else {
                                        $trans = $this->mmaster->qic($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin);
                                        if (isset($trans)) {
                                            foreach ($trans as $itrans) {
                                                $q_aw = $itrans->n_quantity_stock;
                                                $q_ak = $itrans->n_quantity_stock;
                                                $q_in = 0;
                                                $q_out = 0;
                                                break;
                                            }
                                        } else {
                                            $q_aw = 0;
                                            $q_ak = 0;
                                            $q_in = 0;
                                            $q_out = 0;
                                        }
                                    }
                                    $this->mmaster->inserttrans4($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $eproductname, $isj, $q_in, $q_out, $ndeliver, $q_aw, $q_ak, $trans);
                                    if ($this->mmaster->cekmutasi($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $emutasiperiode)) {
                                        $this->mmaster->updatemutasi4($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $ndeliver, $emutasiperiode);
                                    } else {
                                        $this->mmaster->insertmutasi4($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $ndeliver, $emutasiperiode, $q_aw);
                                    }
                                    if ($this->mmaster->cekic($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin)) {
                                        $this->mmaster->updateic4($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $ndeliver, $q_ak);
                                    } else {
                                        $this->mmaster->insertic4($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $eproductname, $ndeliver, $q_aw);
                                    }
                                    $this->mmaster->updatespbitem($ispb, $iproduct, $iproductgrade, $iproductmotif, $ndeliver, $iarea, $vunitprice);
                                } else {
                                    $this->mmaster->updatespbitem($ispb, $iproduct, $iproductgrade, $iproductmotif, 0, $iarea, $vunitprice);
                                }
                            } else {
                                $this->mmaster->updatespbitem($ispb, $iproduct, $iproductgrade, $iproductmotif, 0, $iarea, $vunitprice);
                            }
###############
                        } elseif ($ntmp == $ndeliver) {
                            if ($cek != 'on') {
                                $this->mmaster->deletesjdetail($iproduct, $iproductgrade, $iproductmotif, $isj, $iarea);
                                $th = substr($dsj, 0, 4);
                                $bl = substr($dsj, 5, 2);
                                $emutasiperiode = $th . $bl;
                                if (($ntmp != '') && ($ntmp != 0)) {
                                    $tra = $this->mmaster->deletetrans($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $isj, $ntmp, $eproductname);
                                    $this->mmaster->updatemutasi04($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $ntmp, $emutasiperiode);
                                    $this->mmaster->updateic04($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $ntmp);
                                }
                                $this->mmaster->updatespbitem($ispb, $iproduct, $iproductgrade, $iproductmotif, 0, $iarea, $vunitprice);
                            }
                        }
                    }
                    if (($this->db->trans_status() === false)) {
                        $this->db->trans_rollback();
                    } else {
#                        $this->db->trans_rollback();
                        $this->db->trans_commit();

                        $sess = $this->session->userdata('session_id');
                        $id = $this->session->userdata('user_id');
                        $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                        $rs = pg_query($sql);
                        if (pg_num_rows($rs) > 0) {
                            while ($row = pg_fetch_assoc($rs)) {
                                $ip_address = $row['ip_address'];
                                break;
                            }
                        } else {
                            $ip_address = 'kosong';
                        }
                        $query = pg_query("SELECT current_timestamp as c");
                        while ($row = pg_fetch_assoc($query)) {
                            $now = $row['c'];
                        }
                        $pesan = 'Edit SJ No:' . $isj;
                        $this->load->model('logger');
                        $this->logger->write($id, $ip_address, $now, $pesan);

                        $data['sukses'] = true;
                        if ($sjnew = 1) {
                            $data['inomor'] = $newsj;
                        } else {
                            $data['inomor'] = $isj;
                        }
                        $this->load->view('nomor', $data);
                    }
                }
            }
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function delete()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu86') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $isj = $this->input->post('isjdelete', true);
            $this->load->model('sj/mmaster');
            $this->mmaster->delete($isj);

            $sess = $this->session->userdata('session_id');
            $id = $this->session->userdata('user_id');
            $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs = pg_query($sql);
            if (pg_num_rows($rs) > 0) {
                while ($row = pg_fetch_assoc($rs)) {
                    $ip_address = $row['ip_address'];
                    break;
                }
            } else {
                $ip_address = 'kosong';
            }
            $query = pg_query("SELECT current_timestamp as c");
            while ($row = pg_fetch_assoc($query)) {
                $now = $row['c'];
            }
            $pesan = 'Hapus SJ No:' . $isj;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now, $pesan);

            $data['page_title'] = $this->lang->line('sj');
            $data['isj'] = '';
            $data['jmlitem'] = '';
            $data['detail'] = '';
            $data['isi'] = $this->mmaster->bacasemua();
            $this->load->view('sj/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function deletedetail()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu86') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $isj = $this->input->post('isjdelete', true);
            $iproduct = $this->input->post('iproductdelete', true);
            $iproductgrade = $this->input->post('iproductgradedelete', true);
            $iproductmotif = $this->input->post('iproductmotifdelete', true);
            $this->db->trans_begin();
            $this->load->model('sj/mmaster');
            $this->mmaster->deletedetail($iproduct, $iproductgrade, $isj, $iproductmotif);
            if ($this->db->trans_status() === false) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();

                $sess = $this->session->userdata('session_id');
                $id = $this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs = pg_query($sql);
                if (pg_num_rows($rs) > 0) {
                    while ($row = pg_fetch_assoc($rs)) {
                        $ip_address = $row['ip_address'];
                        break;
                    }
                } else {
                    $ip_address = 'kosong';
                }
                $query = pg_query("SELECT current_timestamp as c");
                while ($row = pg_fetch_assoc($query)) {
                    $now = $row['c'];
                }
                $pesan = 'Hapus Detail SJ No:' . $isj . ' Kode Barang :' . $iproduct;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now, $pesan);

                $data['page_title'] = $this->lang->line('sj') . " Update";
                $query = $this->db->query("select * from tm_spb_item where i_spb = '$isj'");
                $data['jmlitem'] = $query->num_rows();
                $data['isj'] = $isj;
                $data['isi'] = $this->mmaster->baca($isj);
                $data['detail'] = $this->mmaster->bacadetail($isj);
                $this->load->view('sj/vmainform', $data);
            }
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function product()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu86') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $data['baris'] = $this->uri->segment(4);
            $baris = $this->uri->segment(4);
            $data['spb'] = $this->uri->segment(5);
            $spb = $this->uri->segment(5);
            $config['base_url'] = base_url() . 'index.php/sj/cform/product/' . $baris . '/' . $spb . '/';
            $query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
							                    a.e_product_motifname as namamotif,
							                    c.e_product_name as nama,b.v_unit_price as harga
							                    from tr_product_motif a,tr_product c, tm_spb_item b
							                    where a.i_product=c.i_product
							                    and b.i_product_motif=a.i_product_motif
							                    and c.i_product=b.i_product
							                    and b.i_spb='$spb' ", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(6);
            $this->pagination->initialize($config);

            $this->load->model('sj/mmaster');
            $data['page_title'] = $this->lang->line('list_product');
            $data['isi'] = $this->mmaster->bacaproduct($spb, $config['per_page'], $this->uri->segment(6));
            $data['baris'] = $baris;
            $data['spb'] = $spb;
            $this->load->view('sj/vlistproduct', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function cariproduct()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu86') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $data['baris'] = $this->uri->segment(4);
            $baris = $this->uri->segment(4);
            $config['base_url'] = base_url() . 'index.php/sj/cform/product/' . $baris . '/index/';
            $cari = $this->input->post('cari', false);
            $cari = strtoupper($cari);
            $query = $this->db->query("	select a.i_product||a.i_product_motif as kode,
										              c.e_product_name as nama,c.v_product_mill as harga
										              from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product
										              and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
                , false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(6);
            $this->pagination->initialize($config);
            $this->load->model('sj/mmaster');
            $data['page_title'] = $this->lang->line('list_product');
            $data['isi'] = $this->mmaster->cariproduct($cari, $config['per_page'], $this->uri->segment(6));
            $data['baris'] = $baris;
            $this->load->view('sj/vlistproduct', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function area()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu86') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $baris=$this->uri->segment(4);
            $iuser = $this->session->userdata('user_id');
            $config['base_url'] = base_url() . 'index.php/sj/cform/area/'.$baris.'/sikasep/';
            $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(6);
            $this->pagination->initialize($config);
            $data['baris']=$baris;
            $data['cari']='';
            $this->load->model('sj/mmaster');
            $data['page_title'] = $this->lang->line('list_area');
            $data['isi'] = $this->mmaster->bacaarea($iuser, $config['per_page'], $this->uri->segment(6));
            $this->load->view('sj/vlistarea', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function cariarea()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu86') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $iuser = $this->session->userdata('user_id');
           /* $config['base_url'] = base_url() . 'index.php/sj/cform/area/index/';
            $cari = $this->input->post('cari', false);
            $cari = strtoupper($cari);*/

            $baris   = $this->input->post('baris', FALSE);
            if($baris=='')$baris=$this->uri->segment(4);
            $cari   = ($this->input->post('cari', FALSE));
            if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
            if($cari!='sikasep')
            $config['base_url'] = base_url().'index.php/sj/cform/cariarea/'.$baris.'/'.$cari.'/';
              else
            $config['base_url'] = base_url().'index.php/sj/cform/cariarea/'.$baris.'/sikasep/';

            $query = $this->db->query(
                                        "select * from tr_area
                                        where (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%' 
                                        and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",
                                        false
                                     );

            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(6);
            $this->pagination->initialize($config);
            $this->load->model('sj/mmaster');
            $data['page_title'] = $this->lang->line('list_area');
            $data['baris']=$baris;
            $data['cari']=$cari;
            $data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(6), $iuser);
            $this->load->view('sj/vlistarea', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function spb()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu86') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $area = $this->uri->segment(4);
            $area1 = $this->session->userdata('i_area');
            $area2 = $this->session->userdata('i_area2');
            $area3 = $this->session->userdata('i_area3');
            $area4 = $this->session->userdata('i_area4');
            $area5 = $this->session->userdata('i_area5');
            $config['base_url'] = base_url() . 'index.php/sj/cform/spb/' . $area . '/';
            if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
                $query = $this->db->query(" select a.i_spb from tm_spb a, tr_customer b, tr_area c
											where a.i_customer=b.i_customer and a.i_area=c.i_area
											and a.i_nota isnull
											and not a.i_store isnull
											and a.f_spb_cancel = 'f'
											and a.i_area='$area'
											and  (
										        (	not a.i_approve1 isnull
											        and not a.i_approve2 isnull
											        and a.f_spb_siapnotagudang = 't'
											        and a.f_spb_siapnotasales = 't')
                              and
                                ( (a.f_spb_stockdaerah = 'f' and f_spb_consigment='f')
                                   or

                                  (a.f_spb_stockdaerah = 't' and f_spb_consigment='t') )
										        )
											and a.i_sj isnull", false);
# and a.f_spb_valid = 't'
            } else {
                $query = $this->db->query("	select a.i_spb from tm_spb a, tr_customer b, tr_area c
											where a.i_customer=b.i_customer and a.i_area=c.i_area and
											a.i_area='$area'
											and a.i_nota isnull
											and not a.i_store isnull
											and a.f_spb_cancel = 'f'
											and (
													a.f_spb_stockdaerah = 't'
												)
											and a.i_sj isnull", false);
# and a.f_spb_valid = 't'
            }
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(5);
            $this->pagination->initialize($config);
            $this->load->model('sj/mmaster');
            $data['page_title'] = $this->lang->line('listspb');
            $data['area'] = $area;
            $data['isi'] = $this->mmaster->bacaspb($area1, $area2, $area3, $area4, $area5, $area, $config['per_page'], $this->uri->segment(5));
            $this->load->view('sj/vlistspb', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function carispb()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu86') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $area1 = $this->session->userdata('i_area');
            $area2 = $this->session->userdata('i_area2');
            $area3 = $this->session->userdata('i_area3');
            $area4 = $this->session->userdata('i_area4');
            $area5 = $this->session->userdata('i_area5');
            $area = $this->input->post('iarea', false);
            $config['base_url'] = base_url() . 'index.php/sj/cform/spb/' . $area . '/';
            $cari = $this->input->post('cari', false);
            $cari = strtoupper($cari);
            if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
                $query = $this->db->query("	select a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
											where a.i_customer=b.i_customer and a.i_area=c.i_area
											and a.i_nota isnull
											and not a.i_store isnull
											and a.f_spb_cancel = 'f'
											and a.i_area='$area'
											and  (
										        (	not a.i_approve1 isnull
											        and not a.i_approve2 isnull
											        and a.f_spb_siapnotagudang = 't'
											        and a.f_spb_siapnotasales = 't')
                              and
                                ( (a.f_spb_stockdaerah = 'f' and f_spb_consigment='f')
                                   or
                                  (a.f_spb_stockdaerah = 't' and f_spb_consigment='t') )
										        )
											and (upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')
											and a.i_sj isnull", false);
# and a.f_spb_valid = 't'
            } else {
                $query = $this->db->query("	select a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
											where a.i_customer=b.i_customer and a.i_area=c.i_area and
											a.i_area='$area'
											and a.i_nota isnull
											and not a.i_store isnull
											and a.f_spb_cancel = 'f'
											and (
													a.f_spb_stockdaerah = 't'
												)
											and (upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')
											and a.i_sj isnull", false);
# and a.f_spb_valid = 't'
            }
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(5);
            $this->pagination->initialize($config);
            $this->load->model('sj/mmaster');
            $data['area'] = $area;
            $data['page_title'] = $this->lang->line('listspb');
            $data['isi'] = $this->mmaster->carispb($cari, $area1, $area2, $area3, $area4, $area5, $area, $config['per_page'], $this->uri->segment(5));
            $this->load->view('sj/vlistspb', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }

    public function cari()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu86') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $cari = $this->input->post('cari', false);
            $cari = strtoupper($cari);
            $config['base_url'] = base_url() . 'index.php/sj/cform/index/';
            $query = $this->db->query("select * from tm_spb
						   where upper(i_spb) like '%$cari%' ", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(4);
            $this->pagination->initialize($config);
            $this->load->model('sj/mmaster');
            $data['isi'] = $this->mmaster->cari($cari, $config['per_page'], $this->uri->segment(5));
            $data['page_title'] = $this->lang->line('trans_spb');
            $data['isj'] = '';
            $data['jmlitem'] = '';
            $data['detail'] = '';
            $this->load->view('sj/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function hitung()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu86') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $this->load->model('sj/mmaster');
            $dsj = $this->uri->segment(8);
            if ($dsj != '') {
                $tmp = explode("-", $dsj);
                $th = $tmp[2];
                $bl = $tmp[1];
                $hr = $tmp[0];
                $tglsjbaru = $th . $bl . $hr;
                $dsj = $th . "-" . $bl . "-" . $hr;
            }
            $ispb = $this->uri->segment(4);
            $dspb = $this->uri->segment(5);
            $iarea = $this->uri->segment(6);
            $eareaname = str_replace('%20', ' ', $this->uri->segment(7));

            $typearea = $this->mmaster->cekdaerah($ispb, $iarea);
            if ($typearea == 't') {
                $areasj = $iarea;
            } else {
                $areasj = '00';
            }
            $tglakhir = '';
            $sjpot = 'SJ-' . substr(date('Y'), 2, 2) . '%-' . $areasj;
            $query = $this->db->query("SELECT d_sj from tm_nota where i_sj like '$sjpot%' order by d_sj desc");
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $tmp) {
                    $tglakhir = $tmp->d_sj;
                    break;
                }
            }
            $data['txttglakhir'] = '';
            $data['tglakhirx'] = $tglakhir;
            if ($tglakhir != '') {
                $tmp = explode("-", $tglakhir);
                $hr = $tmp[2];
                $bl = $tmp[1];
                $th = $tmp[0];
                $data['txttglakhir'] = $hr . "-" . $bl . "-" . $th;
//              $data['tglakhirx']=$hr."-".$bl."-".$th;
                $tglakhir = $th . $bl . $hr;
            }

            $istore = $this->uri->segment(9);
            $isjold = $this->uri->segment(12);
            $ecustomername = $this->uri->segment(10);
            $ecustomername = str_replace('%20', ' ', $ecustomername);
#      $ecustomername= str_replace(" n "," & ",$ecustomername);
            $ecustomername = str_replace("tandakoma", ",", $ecustomername);
            $ecustomername = str_replace("tandapetiksatukebalik", "`", $ecustomername);
            $ecustomername = str_replace("tandapetiksatu", "'", $ecustomername);
            $ecustomername = str_replace("tandakurungbuka", "(", $ecustomername);
            $ecustomername = str_replace("tandakurungtutup", ")", $ecustomername);
            $ecustomername = str_replace("tandadan", "&", $ecustomername);
            $ecustomername = str_replace("tandatitikkoma", ";", $ecustomername);
            $icustomer = $this->uri->segment(11);
            $query = $this->db->query(" select a.i_product as kode
                      from tr_product_motif a,tr_product c, tm_spb_item b
                      where a.i_product=c.i_product
                      and b.i_product_motif=a.i_product_motif
                      and c.i_product=b.i_product
                      and b.i_spb='$ispb' and b.i_area='$iarea' ", false);
            $data['jmlitem'] = $query->num_rows();
            $data['page_title'] = $this->lang->line('sj');
            $data['isj'] = '';
            $data['isi'] = "xxxxx";
            $data['dsj'] = $dsj;
            $data['ispb'] = $ispb;
            $data['dspb'] = $dspb;
            $data['iarea'] = $iarea;
            $data['istore'] = $istore;
            $data['isjold'] = $isjold;
            $data['eareaname'] = $eareaname;
            $data['ecustomername'] = $ecustomername;
            $data['icustomer'] = $icustomer;
            $query = $this->db->query("	select v_spb, n_spb_discount1, n_spb_discount2, n_spb_discount3,
                                v_spb_discount1, v_spb_discount2, v_spb_discount3, v_spb_discounttotal,
                                i_customer, i_salesman, n_spb_toplength, f_spb_consigment, f_spb_plusppn,d_approve1,d_approve2
                                from tm_spb where i_spb='$ispb' and i_area='$iarea' ", false);
            if ($query->num_rows() > 0) {
                foreach ($query->result() as $row) {
                    $vsjgross = $row->v_spb;
                    $nsjdiscount1 = $row->n_spb_discount1;
                    $nsjdiscount2 = $row->n_spb_discount2;
                    $nsjdiscount3 = $row->n_spb_discount3;
                    $vsjdiscount1 = $row->v_spb_discount1;
                    $vsjdiscount2 = $row->v_spb_discount2;
                    $vsjdiscount3 = $row->v_spb_discount3;
                    $vsjdiscounttotal = $row->v_spb_discounttotal;
                    $vsjnetto = $row->v_spb - $row->v_spb_discounttotal;
                    $icustomer = $row->i_customer;
                    $isalesman = $row->i_salesman;
                    $ntop = $row->n_spb_toplength;
                    $fspbconsigment = $row->f_spb_consigment;
                    $fspbplusppn = $row->f_spb_plusppn;
                    $sales = $row->d_approve1;
                    $keuangan = $row->d_approve2;
                    if ($sales == $keuangan) {
                        $store = $row->d_approve2;
                    } else if ($sales > $keuangan) {
                        $store = $row->d_approve1;
                    } else {
                        $store = $row->d_approve2;
                    }
//          echo 'disc = '.$vsjdiscounttotal;
                }
            }
            $data['vsjgross'] = $vsjgross;
            $data['nsjdiscount1'] = $nsjdiscount1;
            $data['nsjdiscount2'] = $nsjdiscount2;
            $data['nsjdiscount3'] = $nsjdiscount3;
            $data['vsjdiscount1'] = $vsjdiscount1;
            $data['vsjdiscount2'] = $vsjdiscount2;
            $data['vsjdiscount3'] = $vsjdiscount3;
            $data['vsjdiscounttotal'] = $vsjdiscounttotal;
            $data['vsjnetto'] = $vsjnetto;
            $data['vsjppn'] = 0;
            $data['icustomer'] = $icustomer;
            $data['isalesman'] = $isalesman;
            $data['ntop'] = $ntop;
            $data['fspbplusppn'] = $fspbplusppn;
            $data['fspbconsigment'] = $fspbconsigment;
            $data['detail'] = $this->mmaster->product($ispb, $iarea);
            $data['tglsjbaru'] = $tglsjbaru;
            $data['tglakhir'] = $tglakhir;
            $data['areasj'] = $areasj;
            $data['dstore'] = $store;
            $this->load->view('sj/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
}