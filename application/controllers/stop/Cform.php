<?php 
class Cform extends CI_Controller {
//  $data=array();
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $this->load->model('stop/mmaster');
			$data['iarealogin'] = $this->session->userdata('i_area');
			$data['page_title'] = $this->lang->line('stop');
			$data['istop']='';
			$data['isi']='';
			$data['detail']="";
			$data['jmlitem']="";
			$data['tgl']=date('d-m-Y');
			$data['iarea']='';
			$data['i_customer']='';
			if($this->uri->segment(4)!='')$data['iarea']=$this->uri->segment(4);
			if($this->uri->segment(5)!='')$data['i_customer']=$this->uri->segment(5);
			$this->load->view('stop/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dstop 		= $this->input->post('dstop', TRUE);
			if($dstop!=''){
				$tmp=explode("-",$dstop);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dstop=$th."-".$bl."-".$hr;
        	$thbl=$th.$bl;
			}
			$iarea    = $this->input->post('iarea', TRUE);
			$eremark	= $this->input->post('eremark', TRUE);		
			$jml	      = $this->input->post('jml', TRUE);
			if($dstop!='' && $iarea!='' &&  $jml!='' && $jml!=0)
			{
				$this->db->trans_begin();
				$this->load->model('stop/mmaster');
				$istop	=$this->mmaster->runningnumber($iarea,$thbl);
				$this->mmaster->insertheader($istop,$dstop,$iarea,$eremark);
				for($i=1;$i<=$jml;$i++){
				  $iop			= $this->input->post('iop'.$i, TRUE);
				  $dop			= $this->input->post('dop'.$i, TRUE);
				  $i_customer  = $this->input->post('i_customer'.$i, TRUE);
				  $ireff	= $this->input->post('ireff'.$i, TRUE);
				  $isupplier 	  = $this->input->post('isupplier'.$i, TRUE);
				  if($dop!=''){
					$tmp=explode("-",$dop);
					$th=$tmp[2];
					$bl=$tmp[1];
					$hr=$tmp[0];
					$dop=$th."-".$bl."-".$hr;
				  }
				  $this->mmaster->insertdetail($istop,$i_customer,$iop,$dstop,$dop,$isupplier,$ireff,$i);
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
			    $this->db->trans_rollback();
				}else{
			    $this->db->trans_commit();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input STOP Area '.$iarea.' No:'.$istop;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );
				  $data['sukses']			= true;
				  $data['inomor']			= $istop;
				  $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('stop');
			$this->load->view('stop/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		  $data['bisaedit']=false;
			$data['page_title'] = $this->lang->line('stop')." update";
			if($this->uri->segment(4)!=''){
				$istop   = $this->uri->segment(4);
				$iarea  = $this->uri->segment(5);
				$dfrom  = $this->uri->segment(6);
				$dto 	  = $this->uri->segment(7);
				$query  = $this->db->query("select * from tm_stop_item where i_stop='$istop' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows(); 	
				if ($query->num_rows() > 0){
					foreach($query->result() as $row){			
				    $dstop=substr($row->d_stop,0,4).substr($row->d_stop,5,2);
				  }
				}
				$data['istop'] 		= $istop;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				$query = $this->db->query("select * from tm_stop_ekspedisi where i_stop='$istop' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows();				
				$this->load->model('stop/mmaster');
				$data['isi']		= $this->mmaster->baca($istop,$iarea);
				$data['detail']		= $this->mmaster->bacadetail($istop,$iarea);
				$qrunningjml	= $this->mmaster->bacadetailrunningjml($istop,$iarea);
				if($qrunningjml->num_rows()>0){
					$row_jmlx	= $qrunningjml->row();
					$data['jmlx']	= $row_jmlx->v_total;
				}else{
					$data['jmlx']	= 0;	
				}
				$data['detailx']=$this->mmaster->bacadetailx($istop,$iarea);
				
				$query=$this->db->query("	select i_periode from tm_periode ",false);
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
            $periode=$row->i_periode;
				  }
				  if($periode<=$dstop)$data['bisaedit']=true;
			  }
				
		 		$this->load->view('stop/vmainform',$data);
			}else{
				$this->load->view('stop/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dstop 		= $this->input->post('dstop', TRUE);
			if($dstop!=''){
				$tmp=explode("-",$dstop);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dstop=$th."-".$bl."-".$hr;
			}
      $iarea	    = $this->session->userdata('i_area');
			$istopold    = $this->input->post('istopold', TRUE);
			$istop	      = $this->input->post('istop', TRUE);
			$i_customer	= $this->input->post('i_customer', TRUE);
			$iarea  	= $this->input->post('iarea', TRUE);
			$estopkirim 	= $this->input->post('estopkirim', TRUE);
			$istop 	= $this->input->post('istop', TRUE);
			$estopvia   	= $this->input->post('estopvia', TRUE);
			$istopvia   	= $this->input->post('istopvia', TRUE);
			$eekspedisi	= $this->input->post('eekspedisi', TRUE);
			$esupirname	= $this->input->post('esupirname', TRUE);
			$ikendaraan	= $this->input->post('ikendaraan', TRUE);		
			$vstop   	  = $this->input->post('vstop', TRUE);		
			$vstop  		= str_replace(',','',$vstop);
			$jml      	= $this->input->post('jml', TRUE);
      $jmlx	      = $this->input->post('jmlx', TRUE);
				  				  			
			if($istop!='' && $dstop!='' && $iarea!='' && $istop!='' && $istopvia!='')
			{
				$this->db->trans_begin();
  			$this->load->model('stop/mmaster');
				$this->mmaster->deleteheader($istop, $iarea);
				$this->mmaster->insertheader($istop, $dstop, $iarea, $istop, $istopvia, $ikendaraan, $esupirname, $vstop, $istopold);
				for($i=1;$i<=$jml;$i++){
				  $iop = $this->input->post('iop'.$i, TRUE);
				  $dop = $this->input->post('dop'.$i, TRUE);
				  if($dop!=''){
					  $tmp=explode("-",$dop);
					  $th=$tmp[2];
					  $bl=$tmp[1];
					  $hr=$tmp[0];
					  $dop=$th."-".$bl."-".$hr;
				  }
				  $vjumlah		= $this->input->post('vsjnetto'.$i, TRUE);
				  $vjumlah		= str_replace(',','',$vjumlah);
				  $eremark		= $this->input->post('eremark'.$i, TRUE);
				  $this->mmaster->deletedetail($istop,$iarea,$iop);
				  $this->mmaster->insertdetail($istop,$iarea,$iop,$dstop,$dop,$vjumlah,$eremark,$i);
				  $this->mmaster->updatesj($istop,$iop,$iarea,$dstop);
				}
				if($jmlx>0 && $istopvia!=2){	
					for($i=1;$i<=$jmlx;$i++){
					  $iekspedisi	= $this->input->post('iekspedisi'.$i, TRUE);
						$eremark	= $this->input->post('eremarkx'.$i, TRUE);
						$this->mmaster->deletedetailekspedisi($istop,$iarea,$iekspedisi);
						$this->mmaster->insertdetailekspedisi($istop,$iarea,$iekspedisi,$dstop,$eremark,$i);
					}
				}
						
				if (($this->db->trans_status()===FALSE))
				{
				    $this->db->trans_rollback();
				}else{
			      $this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update stop Area '.$iarea.' No:'.$istop;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

#				    $this->db->trans_rollback();
					  $data['sukses']			= true;
					  $data['inomor']			= $istop;
					  $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/stop/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('stop/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('stop/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function op()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu52')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $baris=$this->input->post('baris', FALSE);
      $area=$this->input->post('iarea', FALSE);
      $dstop=$this->input->post('dstop', FALSE);
			if($baris=='') $baris=$this->uri->segment(4);
			$data['baris']=$baris;
      if($area=='')	$area=$this->uri->segment(5);
      if($dstop=='')	$dstop=$this->uri->segment(6);
      $tmp=explode("-",$dstop);
      $dd=$tmp[0];
      $mm=$tmp[1];
      $yy=$tmp[2];
      $dstopx=$yy.'-'.$mm.'-'.$dd;
			//$data['area']=$area;
            $cari 	= strtoupper($this->input->post('cari', FALSE));
            if($this->uri->segment(7)!='x01'){
			if($cari=='') $cari=$this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/stop/cform/op/'.$baris.'/'.$area.'/'.$dstop.'/'.$cari.'/';
			}else{
			$config['base_url'] = base_url().'index.php/stop/cform/op/'.$baris.'/'.$area.'/'.$dstop.'/x01/';
			}
		    $query = $this->db->query("select a.i_op from tm_op a
									left join tm_spb b on a.i_reff = b.i_spb  
									left join tm_spmb c on a.i_reff = c.i_spmb  
									inner join tr_supplier d on a.i_supplier = d.i_supplier 
									inner join tr_customer e on b.i_customer = e.i_customer
									where a.i_area ='$area' and a.f_op_cancel='f' and a.d_op = to_date('$dstop','dd-mm-yyyy')
									and (upper(a.i_op) like '%$cari%' or upper(d.e_supplier_name) like '%$cari%' 
									or upper(a.i_reff) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('stop/mmaster');
			$data['page_title'] = $this->lang->line('list_op');
			$data['isi']=$this->mmaster->bacaop($dstop,$cari,$area,$config['per_page'],$this->uri->segment(8,0));
			$data['baris']=$baris;
			$data['area']=$area;
			$data['dstop']=$dstop;
			$data['cari']=$cari;
			$this->load->view('stop/vlistop', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function customer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu52')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
        $cari  = strtoupper($this->input->post('cari', FALSE));
        $iarea = strtoupper($this->input->post('iarea', FALSE));
        $dstop = strtoupper($this->input->post('dstop', FALSE));
        if($iarea=='') $iarea=$this->uri->segment(4);
        if($dstop=='') $dstop=$this->uri->segment(5);
        $per='';
        if($dstop!='x01' && $dstop!=''){
          $tmp=explode('-',$dstop);
          $yy=$tmp[2];
          $bl=$tmp[1];
          $per=$yy.$bl;
        }
        if($this->uri->segment(6)!='x01'){
          if($cari=='') $cari=$this->uri->segment(6);
          $config['base_url'] = base_url().'index.php/stop/cform/customer/'.$iarea.'/'.$dstop.'/'.$cari.'/';
        }else{
          $config['base_url'] = base_url().'index.php/stop/cform/customer/'.$iarea.'/'.$dstop.'/x01/';
        }
        $query   = $this->db->query("select a.i_customer from tr_customer a
                              left join tr_customer_pkp b on
                              (a.i_customer=b.i_customer)
                              left join tr_price_group c on
                              (a.i_price_group=c.n_line or a.i_price_group=c.i_price_group)
                              left join tr_customer_area d on
                              (a.i_customer=d.i_customer)
                              left join tr_customer_salesman e on
                              (a.i_customer=e.i_customer and e.i_product_group='01' and e.e_periode='$per')
                              left join tr_customer_discount f on
                              (a.i_customer=f.i_customer) where a.i_area='$iarea' and a.f_approve='t' and f_customer_aktif ='t'
                              and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);
         $this->load->model('stop/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->bacacustomer($cari,$iarea,$config['per_page'],$this->uri->segment(7),$per);
         $data['iarea']=$iarea;
         $data['dstop']=$dstop;
         $this->load->view('stop/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu52')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/stop/cform/area/index/';
         $allarea= $this->session->userdata('allarea');
         $iuser   = $this->session->userdata('user_id');
         if($allarea=='t')
         {
            $query = $this->db->query(" select * from tr_area order by i_area", false);
         }
         else
         {
            $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
         }

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('stop/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
         $this->load->view('stop/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu52')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/stop/cform/area/index/';
         $allarea = $this->session->userdata('allarea');
         $iuser   = $this->session->userdata('user_id');
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);

         if($allarea=='t'){
            $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
         }
         else{
            $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
         }

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('stop/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
         $this->load->view('stop/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
	

?>
