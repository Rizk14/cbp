<?php
class Cform extends CI_Controller
{

	public $title  = "Closing/Unclose Kas & Bank";
	public $folder = 'closingkasbank';

	function __construct()
	{
		parent::__construct();
		$this->load->model('closingkasbank/mmaster');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu512') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$time = $this->db->query(" SELECT d_closing_time  FROM tm_closing_kas_bank ")->row()->d_closing_time;

			/* $data = array(
				'dopen'				=> '01-' . date('m-Y'),
				'dclosing' 			=> '03-' . date('m-Y'),
				'closingperiode'	=> $this->mmaster->get_closingperiode()->row()->i_periode,
				'd_open_kb'			=> date('d-m-Y', strtotime($this->mmaster->get_closing_kasbank()->row()->d_open_kb)),
				'd_open_kbin'		=> date('d-m-Y', strtotime($this->mmaster->get_closing_kasbank()->row()->d_open_kbin)),
				'd_open_kbank'		=> date('d-m-Y', strtotime($this->mmaster->get_closing_kasbank()->row()->d_open_kbank)),
				'd_open_kbankin'	=> date('d-m-Y', strtotime($this->mmaster->get_closing_kasbank()->row()->d_open_kbankin)),
				'page_title' 		=> $this->title,
				'folder' 			=> $this->folder,
			); */

			// $closingperiode = $this->mmaster->get_closingperiode()->row()->i_periode; /* tm_periode */
			// $thn		= substr($closingperiode, 0, 4);
			// $bl			= substr($closingperiode, 4, 2);
			// $dopennow	= $thn . "-" . $bl . "-01";
			// $dclosing	= "03-" . $bl . "-" . $thn;

			$dopennow	= date('Y-m-01', strtotime($time));
			$dclosing	= date('03-m-Y', strtotime($time));

			$data = array(
				'closingperiode'	=> $this->mmaster->get_closingperiode()->row()->i_periode, /* tm_periode */
				'isi'				=> $this->mmaster->get_closing_kasbank(), /* tm_closing_kas_bank */
				'dopennow'			=> $dopennow,
				'dclosing'			=> $dclosing,
				'page_title' 		=> $this->title,
				'folder' 			=> $this->folder,
			);

			$this->logger->writenew("Membuka Menu Closing/Unclose Kas Bank");

			$this->load->view('closingkasbank/vform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function closing_new()
	{
		$dateclosing	=  $this->input->post('dclosing');
		$closingperiode	=  $this->input->post('iperiode');

		$this->db->trans_begin();

		$pesan = "Closing Periode (All) : " . $closingperiode;

		if ($this->input->post('kbin')) {
			$ref			=  date('Y-m-d', strtotime($this->input->post('kbin')));

			$this->mmaster->closingkbin($dateclosing, $ref);

			$tx = $ref == date('Y-m-01') ? "Closing" : "Unclose";
			$pesan = $tx . " Kas Besar (Masuk) Tanggal : " . $this->input->post('kbin');
		} else if ($this->input->post('kbankin')) {
			$ref			=  date('Y-m-d', strtotime($this->input->post('kbankin')));

			$this->mmaster->closingkbankin($dateclosing, $ref);

			$tx = $ref == date('Y-m-01') ? "Closing" : "Unclose";

			$pesan = $tx . " Bank (Masuk) Tanggal : " . $this->input->post('kbankin');
		} else if ($this->input->post('pkk')) {
			$ref			=  date('Y-m-d', strtotime($this->input->post('pkk')));

			$this->mmaster->closingkkin($dateclosing, $ref);

			$tx = $ref == date('Y-m-01') ? "Closing" : "Unclose";
			$pesan = $tx . " Pengisian Kas Kecil Tanggal : " . $this->input->post('pkk');
		} else if ($this->input->post('kb')) {
			$ref			=  date('Y-m-d', strtotime($this->input->post('kb')));

			$this->mmaster->closingkb($dateclosing, $ref);

			$tx = $ref == date('Y-m-01') ? "Closing" : "Unclose";
			$pesan = $tx . " Kas Besar (Keluar) Tanggal : " . $this->input->post('kb');
		} else if ($this->input->post('kbank')) {
			$ref			=  date('Y-m-d', strtotime($this->input->post('kbank')));

			$this->mmaster->closingkbank($dateclosing, $ref);

			$tx = $ref == date('Y-m-01') ? "Closing" : "Unclose";
			$pesan = $tx . " Bank (Keluar) Tanggal : " . $this->input->post('kbank');
		} else if ($this->input->post('kk')) {
			$ref			=  date('Y-m-d', strtotime($this->input->post('kk')));

			$this->mmaster->closingkk($dateclosing, $ref);

			$tx = $ref == date('Y-m-01') ? "Closing" : "Unclose";
			$pesan = $tx . " Kas Kecil Tanggal : " . $this->input->post('kk');
		}

		$this->mmaster->closeperiodeall($closingperiode, FALSE); /* UNTUK BUKA PERIODE tm_periode */

		if ($this->db->trans_status() === FALSE || $this->db->trans_status() === false) {
			$this->db->trans_rollback();
		} else {
			$this->logger->writenew($pesan);

			$this->db->trans_commit();

			// $ref	= $this->input->post('kbin') ? $this->input->post('kbin') : $this->input->get_post('kbin');

			// $kbin 			= $this->input->post('kbin', TRUE);
			// $kbankin 		= $this->input->post('kbankin', TRUE);
			// $pkk 			= $this->input->post('pkk', TRUE);
			// $kb 			= $this->input->post('kb', TRUE);
			// $kbank 			= $this->input->post('kbank', TRUE);
			// $kk 			= $this->input->post('kk', TRUE);

			// $data['sukses'] = true;
			// $data['inomor'] = $kbin;

			// $this->load->view('nomor', $data);
		}
	}
	function closing()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu512') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$dclosing = $this->input->post('dclosing', TRUE);
			$dopen 	  = $this->input->post('dopen', TRUE);
			$fkb 	  = $this->input->post('f_kb', TRUE);
			$fkbin 	  = $this->input->post('f_kbin', TRUE);
			$fkbank   = $this->input->post('f_kbank', TRUE);
			$fkbankin = $this->input->post('f_kbankin', TRUE);
			$fkk 	  = $this->input->post('f_kk', TRUE);
			$fkkin 	  = $this->input->post('f_kkin', TRUE);
			$closingperiode = $this->input->post('iperiode', TRUE);

			$closingperiodex = $this->input->post('iperiodex', TRUE);
			$dclosingx 		 = $this->input->post('dclosingx', TRUE);
			$dopenx 	  	 = $this->input->post('dopenx', TRUE);

			if ($dclosing != '' && $dopen != '') {
				$dateclosing = date('Y-m-d', strtotime($dclosing));
				$dateopen 	 = date('Y-m-d', strtotime($dopen));
				$this->db->trans_begin();
				$this->load->model('closingkasbank/mmaster');
				$query = $this->mmaster->cekclosing($dateclosing, $dateopen);
				if ($query->num_rows() > 0) {
					if ($fkb == 'on') {
						$this->mmaster->closingkb($dateclosing, $dateopen);
						/*$judul = 'Kas Besar (Keluar)';*/
					}
					if ($fkbin == 'on') {
						$this->mmaster->closingkbin($dateclosing, $dateopen);
						/*$judul = 'Kas Besar (Masuk)';*/
					}
					if ($fkbank == 'on') {
						$this->mmaster->closingkbank($dateclosing, $dateopen);
						/*$judul = 'Kas Bank (Keluar)';*/
					}
					if ($fkbankin == 'on') {
						$this->mmaster->closingkbankin($dateclosing, $dateopen);
						/*$judul = 'Kas Bank (Masuk)';*/
					}
					if ($fkk == 'on') {
						$this->mmaster->closingkk($dateclosing, $dateopen);
						/*$judul = 'Kas Kecil (Keluar)';*/
					}
					if ($fkkin == 'on') {
						$this->mmaster->closingkkin($dateclosing, $dateopen);
						/*$judul = 'Kas Kecil (Masuk)';*/
					}

					$this->mmaster->closeperiodeall($closingperiode); /* UNTUK BUKA PERIODE tm_periode */
				} else {
					$this->mmaster->closing($dateclosing, $dateopen);
				}
				if ($this->db->trans_status() === FALSE || $this->db->trans_status() === false) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();

					$sess	= $this->session->userdata('session_id');
					$id 	= $this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if (pg_num_rows($rs) > 0) {
						while ($row = pg_fetch_assoc($rs)) {
							$ip_address	  = $row['ip_address'];
							break;
						}
					} else {
						$ip_address = 'kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while ($row = pg_fetch_assoc($query)) {
						$now	  = $row['c'];
					}

					$pesan = "Update $this->title, Tanggal Closing : $dclosingx ke tanggal $dclosing, Tanggal Open : $dopenx ke tanggal $dopen, Periode Closing (All) : $closingperiodex ke Periode $closingperiode ";

					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now, $pesan);

					$data['sukses'] = true;
					$data['inomor'] = $dclosing;
					$this->load->view('nomor', $data);
					#print "<script>alert(\"Closing Berhasil. Terimakasih.\");show(\"".$this->folder."/cform\",\"#content\");</script>";
				}
			} else {
				print "<script>alert(\"Maaf, closing gagal disimpan. Terimakasih.\");show(\"" . $this->folder . "/cform\",\"#content\");</script>";
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
