<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu121')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-nota/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select i_nota, i_customer, i_area, d_nota, v_nota_discounttotal, v_nota_netto from tm_nota 
										where f_posting = 'f' and f_nota_koreksi='f'
										union all
										select i_nota, i_customer, i_area, d_nota, v_nota_discounttotal, v_nota_netto from tm_notakoreksi 
										where f_posting = 'f'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_nota');
			$this->load->model('akt-nota/mmaster');
			$data['inota']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('akt-nota/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu121')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('list_nota');
			$this->load->view('akt-nota/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu121')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/akt-nota/cform/index/';
			$config['per_page'] = '10';
			$limo=$config['per_page'];
			$ofso=$this->uri->segment(4);
			if($ofso=='')
				$ofso=0;
			$query = $this->db->query(" select i_nota, i_customer, i_area, d_nota, v_nota_discounttotal, v_nota_netto from tm_nota 
										where f_posting = 'f' and f_nota_koreksi='f' and (upper(i_nota) like '%$cari%')
										union all
										select i_nota, i_customer, i_area, d_nota, v_nota_discounttotal, v_nota_netto from tm_notakoreksi 
										where f_posting = 'f'
										and (upper(i_nota) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('akt-nota/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('list_nota');
			$data['inota']='';
	 		$this->load->view('akt-nota/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu121')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('nota');
			if(
				($this->uri->segment(4))			
			  )
			{
				$this->load->model('akt-nota/mmaster');
				$inota	= $this->uri->segment(4);
				$query	= $this->db->query("select * from tm_nota_item 
											              where i_nota='$inota'
											              union all
											              select * from tm_notakoreksi_item 
											              where i_nota='$inota'");
				$data['jmlitem']= $query->num_rows(); 				
				$data['inota']	= $inota;
				$data['isi']	=$this->mmaster->bacanota($inota);
				$data['detail']	=$this->mmaster->bacadetailnota($inota);
		 		$this->load->view('akt-nota/vmainform',$data);
			}else{
				$this->load->view('akt-nota/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function posting()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu121')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('akt-nota/mmaster');
			$inota		= $this->input->post('inota', TRUE);
			$iarea		= $this->input->post('iarea', TRUE);
			$icustomer	= $this->input->post('icustomer', TRUE);
			$ecustomername	= $this->input->post('ecustomername', TRUE);
//			$eremark	= $this->input->post('eremark', TRUE);
			$eremark	= "Penjualan kepada:".$icustomer."-".$ecustomername;
			$dnota		= $this->input->post('dnota', TRUE);
			$disc		= $this->input->post('vspbdiscounttotalafter', TRUE);
			$disc		= str_replace(',','',$disc);
			$gros		= $this->input->post('vspb', TRUE);
			$gros		= str_replace(',','',$gros);
			$nett		= $this->input->post('vspbafter', TRUE);
			$nett		= str_replace(',','',$nett);
			if($dnota!=''){
				$tmp=explode("-",$dnota);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dnota=$th."-".$bl."-".$hr;
				$iperiode=$th.$bl;
			}
			$fclose			= 'f';
			$this->db->trans_begin();
			$this->mmaster->inserttransheader($inota,$iarea,$eremark,$fclose,$dnota);
			$this->mmaster->updatenota($inota,$iarea);
			$accdebet		= '112.2'.$iarea;
			$namadebet		= $this->mmaster->namaacc($accdebet);
			$acckredit		= '411.100';
			$namakredit		= $this->mmaster->namaacc($acckredit);
			$acckredit2		= '412.200';
			$namakredit2	= $this->mmaster->namaacc($acckredit2);
			
			$this->mmaster->inserttransitemdebet($accdebet,$inota,$namadebet,'t','t',$iarea,$eremark,$gros,$dnota);
			$this->mmaster->updatesaldodebet($accdebet,$iperiode,$gros);
			$this->mmaster->inserttransitemkredit($acckredit,$inota,$namakredit,'f','t',$iarea,$eremark,$nett,$dnota);
			$this->mmaster->updatesaldokredit($acckredit,$iperiode,$nett);
			if($disc!='' && $disc!=0 && $disc!='0'){
				$this->mmaster->inserttransitemkredit($acckredit2,$inota,$namakredit2,'f','t',$iarea,$eremark,$disc,$dnota);
				$this->mmaster->updatesaldokredit($acckredit2,$iperiode,$disc);
			}
			$this->mmaster->insertgldebet($accdebet,$inota,$namadebet,'t',$iarea,$gros,$dnota,$eremark);
			$this->mmaster->insertglkredit($acckredit,$inota,$namakredit,'f',$iarea,$nett,$dnota,$eremark);
			if($disc!='' && $disc!=0 && $disc!='0'){
				$this->mmaster->insertglkredit($acckredit2,$inota,$namakredit2,'f',$iarea,$disc,$dnota,$eremark);
			}
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='Posting Nota Penjualan No:'.$inota.' Area:'.$iarea;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses'] = true;
				$data['inomor']	= $inota;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
