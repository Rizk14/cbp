<?php
class Cform extends CI_Controller
{
	public $title 	= "DKB-SJP Approve";
	public $folder 	= "dkbsjpapprove";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model($this->folder . '/mmaster');
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')) && ($this->session->userdata('menu182') == 't')) || (($this->session->userdata('logged_in')) && ($this->session->userdata('allmenu') == 't'))
		) {
			$data = [
				'page_title'	=> $this->title,
				'folder'		=> $this->folder,
				'dfrom' 		=> '',
				'dto' 			=> '',
				'idkb' 			=> ''
			];

			$this->logger->writenew("Membuka Menu " . $this->title);

			$this->load->view($this->folder . '/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu182') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea  = $this->input->post('iarea');

			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea	= $this->uri->segment(6);

			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/view/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';

			$query = $this->db->query(" SELECT 
											a.*,
											b.e_area_name
										FROM
											tm_dkb_sjp a,
											tr_area b
										WHERE
											a.i_area = b.i_area
											AND (upper(a.i_area) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' OR upper(a.i_dkb) LIKE '%$cari%')
											AND a.i_approve1 ISNULL
											AND a.f_dkb_batal = 'f'
											AND substring(a.i_dkb, 11, 2)= '$iarea'
											AND a.d_dkb BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
										ORDER BY
											a.i_dkb DESC", false);

			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] 	= $this->title;
			$data['folder'] 		= $this->folder;
			$data['cari']			= $cari;
			$data['dfrom']			= $dfrom;
			$data['dto']			= $dto;
			$data['iarea']			= $iarea;
			$data['idkb']   		= '';
			$data['isi']			= $this->mmaster->bacaperiode($dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari, $iarea);

			$this->load->view($this->folder . '/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu182') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea  	= $this->input->post('iarea');

			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea	= $this->uri->segment(6);

			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/view/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';

			$query = $this->db->query(" SELECT 
											a.*,
											b.e_area_name
										FROM
											tm_dkb_sjp a,
											tr_area b
										WHERE
											a.i_area = b.i_area
											AND (upper(a.i_area) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' OR upper(a.i_dkb) LIKE '%$cari%')
											AND a.i_approve1 ISNULL
											AND a.f_dkb_batal = 'f'
											AND substring(a.i_dkb, 11, 2)= '$iarea'
											AND a.d_dkb BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
										ORDER BY
											a.i_dkb DESC ", false);

			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(9);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->title;
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['idkb']   	= '';
			$data['isi']		= $this->mmaster->bacaperiode($dfrom, $dto, $config['per_page'], $this->uri->segment(9), $cari, $iarea);

			$this->load->view($this->folder . '/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu182') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->title;
			$this->load->view($this->folder . '/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu182') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$baris 				= $this->uri->segment(4);

			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/area/' . $baris . '/sikasep/';

			$iuser   = $this->session->userdata('user_id');

			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$data['baris'] = $baris;
			$data['cari'] = '';

			$data['page_title'] = $this->lang->line('list_area');
			$data['folder'] 	= $this->folder;
			$data['isi'] 		= $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(6), $iuser);

			$this->load->view($this->folder . '/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu182') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			/*$config['base_url'] = base_url().'index.php'.$folder.'/cform/area/index/';
			$cari    	= $this->input->post('cari', FALSE);
			$cari 		= strtoupper($cari);*/
			$iuser   	= $this->session->userdata('user_id');
			$baris   = $this->input->post('baris', FALSE);
			if ($baris == '') $baris = $this->uri->segment(4);
			$cari   = ($this->input->post('cari', FALSE));
			if ($cari == '' && $this->uri->segment(5) != 'sikasep') $cari = $this->uri->segment(5);
			if ($cari != 'sikasep')
				$config['base_url'] = base_url() . 'index.php' . $folder . '/cform/cariarea/' . $baris . '/' . $cari . '/';
			else
				$config['base_url'] = base_url() . 'index.php' . $folder . '/cform/cariarea/' . $baris . '/sikasep/';

			$query   	= $this->db->query("select * from tr_area
			                      where (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%' 
			                      and i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);

			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$data['baris'] = $baris;
			$data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(6), $iuser);
			$this->load->view($this->folder . '/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu182') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->title;
			$data['folder'] 	= $this->folder;

			if ($this->uri->segment(4) != '') {
				$idkb   = $this->uri->segment(4);
				$dfrom  = $this->uri->segment(5);
				$dto 	= $this->uri->segment(6);
				$iarea  = $this->uri->segment(7);
				$user  	= $this->session->userdata("i_area");

				$query  = $this->db->query("select * from tm_dkb_sjp_item where i_dkb='$idkb'");
				$data['jmlitem'] 	= $query->num_rows();
				$data['idkb'] 		= $idkb;
				$data['iarea']		= $iarea;
				$data['user'] 		= $user;
				$data['dfrom']  	= $dfrom;
				$data['dto']		= $dto;

				$query = $this->db->query("select * from tm_dkb_ekspedisi where i_dkb='$idkb'");
				$data['jmlitemx'] = $query->num_rows();

				$data['isi']	= $this->mmaster->baca($idkb, $iarea);
				$data['detail']	= $this->mmaster->bacadetail($idkb, $iarea);

				$qrunningjml	= $this->mmaster->bacadetailrunningjml($idkb, $iarea);
				if ($qrunningjml->num_rows() > 0) {
					$row_jmlx	= $qrunningjml->row();
					$data['jmlx']	= $row_jmlx->v_total;
				} else {
					$data['jmlx']	= 0;
				}

				$data['detailx'] = $this->mmaster->bacadetailx($idkb, $iarea);

				$this->load->view($this->folder . '/vmainform', $data);
			} else {
				$this->load->view($this->folder . '/vinsert_fail', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu182') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			// $iarea	= $this->session->userdata('i_area');
			$iarea	= $this->input->post('iarea', TRUE);
			$idkb	= $this->input->post('idkb', TRUE);
			$ddkb	= $this->input->post('ddkb', TRUE);

			if ($ddkb != '') {
				$tmp = explode("-", $ddkb);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$ddkb = $th . "-" . $bl . "-" . $hr;
				$thbl = substr($th, 2, 2) . $bl;
			}

			$jml	  = $this->input->post('jml', TRUE);

			if ($idkb != '' && $iarea != '') {
				$gaono = true;
				for ($i = 1; $i <= $jml; $i++) {
					$cek = $this->input->post('chk' . $i, TRUE);
					if ($cek == 'on') {
						$gaono = false;
					}
					if (!$gaono) break;
				}

				if (!$gaono) {
					$this->db->trans_begin();

					$this->mmaster->updateheader($idkb, $iarea);

					for ($i = 1; $i <= $jml; $i++) {
						$cek  = $this->input->post('chk' . $i, TRUE);
						if ($cek == 'on') {
							$isjp   = $this->input->post('isjp' . $i, TRUE);
							$fkirim = 1;
							$this->mmaster->updatedetail($idkb, $iarea, $isjp, $ddkb, $fkirim);
						} else {
							$isjp	= $this->input->post('isjp' . $i, TRUE);
							$fkirim = 0;
							$this->mmaster->updatedetail($idkb, $iarea, $isjp, $ddkb, $fkirim);
							$this->mmaster->updatesjp($idkb, $isjp, $iarea, $ddkb);
						}
					}

					if (($this->db->trans_status() === FALSE)) {
						$this->db->trans_rollback();
					} else {
						$this->db->trans_commit();

						$this->logger->writenew("Approve DKB-SJP No : " . $idkb);

						$data['sukses']	= true;
						$data['inomor']	= $idkb;
						$this->load->view('nomor', $data);
					}
				}
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
