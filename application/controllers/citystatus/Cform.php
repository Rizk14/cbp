<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu24')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_citystatus');
			$data['icitystatus']='';
			$this->load->model('citystatus/mmaster');
			$data['isi']=$this->mmaster->bacasemua();

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Status Kota";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('citystatus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu24')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icitystatus 	 = $this->input->post('icitystatus', TRUE);
			$ecitystatusname = $this->input->post('ecitystatusname', TRUE);

			if ((isset($icitystatus) && $icitystatus != '') && (isset($ecitystatusname) && $ecitystatusname != ''))
			{
				$this->load->model('citystatus/mmaster');
				$this->mmaster->insert($icitystatus,$ecitystatusname);

	        $sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
		        while($row=pg_fetch_assoc($rs)){
			        $ip_address	  = $row['ip_address'];
			        break;
		        }
	        }else{
		        $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
		        $now	  = $row['c'];
	        }
	        $pesan='Input Status Kota:('.$icitystatus.')-'.$ecitystatusname;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now , $pesan );

	        $data['page_title'] = $this->lang->line('master_citystatus');
			$data['icitystatus']='';
			$this->load->model('citystatus/mmaster');
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('citystatus/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu24')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_citystatus');
			$this->load->view('citystatus/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu24')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_citystatus')." update";
			if($this->uri->segment(4)){
				$icitystatus = $this->uri->segment(4);
				$data['icitystatus'] = $icitystatus;
				$this->load->model('citystatus/mmaster');
				$data['isi']=$this->mmaster->baca($icitystatus);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Status Kota:('.$icitystatus.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);
		        
		 		$this->load->view('citystatus/vmainform',$data);
			}else{
				$this->load->view('citystatus/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu24')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icitystatus		= $this->input->post('icitystatus', TRUE);
			$ecitystatusname 	= $this->input->post('ecitystatusname', TRUE);
			$this->load->model('citystatus/mmaster');
			$this->mmaster->update($icitystatus,$ecitystatusname);

		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
		        while($row=pg_fetch_assoc($rs)){
			        $ip_address	  = $row['ip_address'];
			        break;
		        }
		      }else{
		        $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
		        $now	  = $row['c'];
		      }
		      $pesan='Update Status Kota:('.$icitystatus.')-'.$ecitystatusname;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );

		        $data['page_title'] = $this->lang->line('master_citystatus');
				$data['icitystatus']='';
				$this->load->model('citystatus/mmaster');
				$data['isi']=$this->mmaster->bacasemua();
				$this->load->view('citystatus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu24')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icitystatus		= $this->uri->segment(4);
			$this->load->model('citystatus/mmaster');
			$this->mmaster->delete($icitystatus);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
        while($row=pg_fetch_assoc($rs)){
	        $ip_address	  = $row['ip_address'];
	        break;
        }
      }else{
        $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
        $now	  = $row['c'];
      }
      $pesan='Delete Status Kota:('.$icitystatus.')-'.$ecitystatusname;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('master_citystatus');
			$data['icitystatus']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('citystatus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
