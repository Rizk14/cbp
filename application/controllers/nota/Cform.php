<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu56')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('cnota');
			$data['dfrom']= '';
			$data['dto']	= '';
			$data['isj']	= '';
			$data['inota']= '';
			$this->load->view('nota/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu56')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);

			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/nota/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			if($iarea == 'NA'){
				$qstr	= " SELECT a.i_sj
                from tm_nota a, tr_customer b, tr_area c, tm_spb d
                where a.i_customer=b.i_customer and a.i_area=c.i_area
                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                  or upper(a.i_spb) like '%$cari%' or upper(d.i_spb_old) like '%$cari%') 
                and a.i_nota isnull and not a.i_sj isnull and a.f_nota_cancel = 'f' 
                and a.i_spb=d.i_spb and a.i_area=d.i_area 
                and (a.d_sj >= to_date('$dfrom', 'dd-mm-yyyy') 
                and a.d_sj <= to_date('$dto', 'dd-mm-yyyy')) ";
			}else{
	    $qstr	= " SELECT a.i_sj
                from tm_nota a, tr_customer b, tr_area c, tm_spb d
                where a.i_customer=b.i_customer and a.i_area=c.i_area
                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                  or upper(a.i_spb) like '%$cari%' or upper(d.i_spb_old) like '%$cari%') 
                and a.i_nota isnull and not a.i_sj isnull and a.f_nota_cancel = 'f' 
                and a.i_spb=d.i_spb and a.i_area=d.i_area 
                and a.i_area='$iarea' 
                and (a.d_sj >= to_date('$dfrom', 'dd-mm-yyyy') 
                and a.d_sj <= to_date('$dto', 'dd-mm-yyyy')) ";
			}
# and d.f_spb_valid='t'
			$query = $this->db->query($qstr,false);
			$num   = $query->num_rows();
			$config['total_rows'] = $num; 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('cnota');
			$this->load->model('nota/mmaster');
			$data['isj']	= '';
			$data['inota']	= '';
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
      $data['tgl']=date('d-m-Y');
			$data['isi']=$this->mmaster->bacasemua($iarea,$dfrom,$dto,$cari,$config['per_page'],$this->uri->segment(8));
			$data['noakhir']=$this->mmaster->bacaakhir($iarea,$dfrom,$dto,$cari,$config['per_page'],$this->uri->segment(8));
			$this->load->view('nota/vmainform', $data);

		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu56')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspb');
			$this->load->view('nota/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu56')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispb	= $this->input->post('ispbdelete', TRUE);
			$this->load->model('nota/mmaster');
			$this->mmaster->delete($inota,$ispb);
			$data['page_title'] = $this->lang->line('listspb');
			$data['ispb']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('nota/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu56')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);

			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/nota/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$qstr	= " SELECT a.i_sj
                from tm_nota a, tr_customer b, tr_area c, tm_spb d
                where a.i_customer=b.i_customer and a.i_area=c.i_area
                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                or upper(a.i_spb) like '%$cari%' or upper(d.i_spb_old) like '%$cari%') 
                and a.i_nota isnull and not a.i_sj isnull and a.f_nota_cancel = 'f' 
                and not a.i_dkb isnull
                and a.i_spb=d.i_spb and a.i_area=d.i_area 
                and a.i_area='$iarea' 
                and (a.d_sj >= to_date('$dfrom', 'dd-mm-yyyy') 
                and a.d_sj <= to_date('$dto', 'dd-mm-yyyy')) ";
			$query = $this->db->query($qstr,false);
			$num   = $query->num_rows();
			$config['total_rows'] = $num; 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';

			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('cnota');
			$this->load->model('nota/mmaster');
			$data['isj']	= '';
			$data['inota']	= '';
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
      $data['tgl']=date('d-m-Y');
			$data['isi']=$this->mmaster->bacasemua($iarea,$dfrom,$dto,$cari,$config['per_page'],$this->uri->segment(8));
			$this->load->view('nota/vmainform', $data);

		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu56')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('nota');
			if(
				($this->uri->segment(4))&&($this->uri->segment(5))
			  )
			{
        $cari='';
				$isj  	= $this->uri->segment(4);
				$iarea 	= $this->uri->segment(5);
				$dfrom	= $this->uri->segment(6);
				$dto	  = $this->uri->segment(7);
				$dsj	  = $this->uri->segment(8);
				$query  = $this->db->query("select i_sj from tm_nota_item where i_sj = '$isj' and i_area='$iarea'");
				$data['jmlitem']= $query->num_rows(); 				
				$data['isj'] 	  = $isj;
				$data['iarea'] 	= $iarea;
				$data['dfrom'] 	= $dfrom;
				$data['dto'] 	= $dto;
				$data['inota']='';
				$data['tgl']=date('d-m-Y');
        $data['noakhir']='';
				$this->load->model('nota/mmaster');
				$data['isi']=$this->mmaster->baca($isj,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isj,$iarea);
/*
        $sjpot=substr($isj,0,8);
        $query  = $this->db->query("SELECT a.i_sj
                                    from tm_nota a, tr_customer b, tr_area c, tm_spb d
                                    where a.i_customer=b.i_customer and a.i_area=c.i_area
                                    and a.i_nota isnull and not a.i_sj isnull and a.f_nota_cancel = 'f' 
                                    and not a.i_dkb isnull
                                    and a.i_spb=d.i_spb and a.i_area=d.i_area 
                                    and a.i_area='$iarea' and a.i_sj like '$sjpot%'
                                    and ((a.d_sj<'$dsj')) order by a.d_sj, a.i_sj");
				if($query->num_rows()>0){
          $data['adasj']=$query->result();
          $area1	= $this->session->userdata('i_area');
			    $area2	= $this->session->userdata('i_area2');
			    $area3	= $this->session->userdata('i_area3');
			    $area4	= $this->session->userdata('i_area4');
			    $area5	= $this->session->userdata('i_area5');
			    $config['base_url'] = base_url().'index.php/nota/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			    $cari = strtoupper($this->input->post('cari', FALSE));
	        $qstr	= " SELECT a.i_sj
                    from tm_nota a, tr_customer b, tr_area c, tm_spb d
                    where a.i_customer=b.i_customer and a.i_area=c.i_area
                    and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                      or upper(a.i_spb) like '%$cari%' or upper(d.i_spb_old) like '%$cari%') 
                    and a.i_nota isnull and not a.i_sj isnull and a.f_nota_cancel = 'f' 
                    and not a.i_dkb isnull
                    and a.i_spb=d.i_spb and a.i_area=d.i_area 
                    and a.i_area='$iarea' 
                    and (a.d_sj >= to_date('$dfrom', 'dd-mm-yyyy') 
                    and a.d_sj <= to_date('$dto', 'dd-mm-yyyy')) ";
			    $que  = $this->db->query($qstr,false);
			    $num  = $que->num_rows();
			    $config['total_rows'] = $num; 
			    $config['per_page'] = '10';
			    $config['first_link'] = 'Awal';
			    $config['last_link'] = 'Akhir';
			    $config['next_link'] = 'Selanjutnya';
			    $config['prev_link'] = 'Sebelumnya';
          if($this->uri->segment(7)=='index'){
  			    $config['cur_page'] = $this->uri->segment(8);
          }else{
  			    $config['cur_page'] = $this->uri->segment(9);
          }

			    $this->pagination->initialize($config);
			    $data['page_title'] = $this->lang->line('cnota');
			    $this->load->model('nota/mmaster');
			    $data['isj']	= '';
			    $data['inota']	= '';
			    $data['dfrom']	= $dfrom;
			    $data['dto']	= $dto;
			    $data['iarea']	= $iarea;
          $data['tgl']=date('d-m-Y');
			    $data['isi']=$this->mmaster->bacasemua($iarea,$dfrom,$dto,$cari,$config['per_page'],$config['cur_page']);
			    $data['noakhir']=$this->mmaster->bacaakhir($iarea,$dfrom,$dto,$cari,$config['per_page'],$config['cur_page']);
        }
*/
		 		$this->load->view('nota/vmainform',$data);
			}else{
				$this->load->view('nota/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$this->load->model('nota/mmaster');
			$ispb 	    = $this->input->post('ispb', TRUE);
			$eareaname 	= $this->input->post('eareaname', TRUE);
			$isj 	      = $this->input->post('isj', TRUE);
			$inotaold		= $this->input->post('inotaold', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$jml			  = $this->input->post('jml', TRUE);
			$dnota 			= $this->input->post('dnota', TRUE);
			if($dnota!=''){
        $tmp 	= explode("-", $dnota);
			  $det	= $tmp[0];
			  $mon	= $tmp[1];
			  $yir 	= $tmp[2];
			  $ddspb	= $yir."/".$mon."/".$det;
			  $iperiode=$yir.$mon;
        $nnotatoplength	= $this->input->post('nspbtoplength',TRUE);
			  if($nnotatoplength<0) $nnotatoplength=$nnotatoplength*-1;
			  $dudet	= $this->fungsi->dateAdd("d",$nnotatoplength,$ddspb);
			  $djatuhtempo=$dudet;

				$tmp=explode("-",$dnota);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dnota=$th."-".$bl."-".$hr;
        $thbl=$th.$bl;
			}
			$nprice			= $this->input->post('nprice',TRUE);
			$inota 		= $this->input->post('inota', TRUE);
			$eremark 	= $this->input->post('eremark', TRUE);
			if(($dnota!='') && ($inota=='')){
			  $fclose			= 'f';
			  $icustomer	= $this->input->post('icustomer', TRUE);
			  $ecustomername	= $this->input->post('ecustomername', TRUE);
			  $eremarkacc	= "Penjualan kepada:".$icustomer."-".$ecustomername;
        $this->db->trans_begin();
				$inota		   			= $this->mmaster->runningnumber($iarea,$thbl);
				$vspbdiscounttotalafter	= $this->input->post('vspbdiscounttotalafter',TRUE);
				$vspbdiscounttotalafter	= str_replace(',','',$vspbdiscounttotalafter);
				$vspbafter				= $this->input->post('vspbafter',TRUE);
				$vspbafter				= str_replace(',','',$vspbafter);
				
				/* PENAMBAHAN HITUNG DPP & PPN 27 AGS 2021 */
				// $this->mmaster->insertnota_dpp_ppn($isj,$iarea,$inota);
				/* *************************************** */

				$this->mmaster->updatespb($ispb,$iarea,$inota,$dnota,$vspbdiscounttotalafter,$vspbafter);
				$this->mmaster->updatenotabaru($isj,$iarea,$inota,$dnota,$eremark,$inotaold,$djatuhtempo,$nnotatoplength,$nprice,$vspbdiscounttotalafter,$vspbafter);

				// * 25 MAR 2022
				$get = $this->db->query(" SELECT *, n_tax / 100 + 1 excl_divider, n_tax / 100 n_tax_val FROM tr_tax_amount WHERE '$dnota' BETWEEN d_start AND d_finish ")->row();
########## Posting
        $gros=$vspbafter+$vspbdiscounttotalafter;
        $vdpp=$vspbafter/$get->excl_divider;
        $vdis=$vspbdiscounttotalafter;
        $vpen=$vdpp+$vspbdiscounttotalafter;
        $vppn=$vdpp*$get->n_tax_val;
        $vpiu=$vspbafter;


#        $gros=$vspbafter+$vspbdiscounttotalafter;
#        $vpen=$gros;#*0.1;
#        $vdis=$vspbdiscounttotalafter;#*0.1;
#        $vdpp=$vpen-$vdis;
#        $vppn=$vdpp;#*0.1;
#        $vpiu=$vdpp+$vppn;


				$this->mmaster->inserttransheader($inota,$iarea,$eremarkacc,$fclose,$dnota);
			  $this->mmaster->updatenotaacc($inota,$iarea);
			  $accdebet		  = PiutangDagang.$iarea;
			  $namadebet		= $this->mmaster->namaacc($accdebet);
			  if($namadebet=='')$namadebet='Piutang Dagang ('.$eareaname.')';
			  $accdebet2		= PotonganPenjualan;
			  $namadebet2		= $this->mmaster->namaacc($accdebet2);
			  $acckredit		= HasilPenjualanKotor;
			  $namakredit		= $this->mmaster->namaacc($acckredit);
			  $acckredit2		= HutangPPN;
			  $namakredit2		= $this->mmaster->namaacc($acckredit2);
#			  $this->mmaster->inserttransitemdebet($accdebet,$inota,$namadebet,'t','t',$iarea,$eremarkacc,$vspbafter,$dnota);
#			  $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vspbafter);
			  $this->mmaster->inserttransitemdebet($accdebet,$inota,$namadebet,'t','t',$iarea,$eremarkacc,$vpiu,$dnota);
			  $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vpiu);
			  if($vspbdiscounttotalafter!='' && $vspbdiscounttotalafter!=0 && $vspbdiscounttotalafter!='0'){
				  $this->mmaster->inserttransitemdebet($accdebet2,$inota,$namadebet2,'t','t',$iarea,$eremarkacc,$vdis,$dnota);
				  $this->mmaster->updatesaldodebet($accdebet2,$iperiode,$vdis);
			  }
			  $this->mmaster->inserttransitemkredit($acckredit,$inota,$namakredit,'f','t',$iarea,$eremarkacc,$vpen,$dnota);
			  $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vpen);
			  $this->mmaster->inserttransitemkredit($acckredit2,$inota,$namakredit2,'f','t',$iarea,$eremarkacc,$vppn,$dnota);
			  $this->mmaster->updatesaldokredit($acckredit2,$iperiode,$vppn);
			  $this->mmaster->insertgldebet($accdebet,$inota,$namadebet,'t',$iarea,$vpiu,$dnota,$eremarkacc);
			  if($vspbdiscounttotalafter!='' && $vspbdiscounttotalafter!=0 && $vspbdiscounttotalafter!='0'){
				  $this->mmaster->insertgldebet($accdebet2,$inota,$namadebet2,'t',$iarea,$vdis,$dnota,$eremarkacc);
			  }
			  $this->mmaster->insertglkredit($acckredit,$inota,$namakredit,'f',$iarea,$vpen,$dnota,$eremarkacc);
			  $this->mmaster->insertglkredit($acckredit2,$inota,$namakredit2,'f',$iarea,$vppn,$dnota,$eremarkacc);
##########
			}
			if ($this->db->trans_status() === FALSE)
			{
			  $this->db->trans_rollback();
			}else{
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Input Nota No:'.$inota;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );
#			  $this->db->trans_rollback();
				$this->db->trans_commit();

				$data['sukses']			= true;
				$data['inomor']			= $inota;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$data['page_title'] = $this->lang->line('nota');#." Koreksi";
			if(
				($this->uri->segment(4)) && ($this->uri->segment(5))
			  )
			{
			  $data['bisaedit']=false;
       			$data['per_skrg']=date('Ym');
				$isj 	= $this->uri->segment(9);
				$dto 	= $this->uri->segment(8);
				$dfrom= $this->uri->segment(7);
				$area = $this->uri->segment(6);
				$ispb = $this->uri->segment(5);
				$inota= $this->uri->segment(4);
				$query= $this->db->query("select i_nota, d_nota from tm_nota_item where i_nota = '$inota' and i_area='$area' and n_deliver>0");
				$data['jmlitem'] = $query->num_rows();
				if ($query->num_rows() > 0){
					foreach($query->result() as $row){			
				    $dnota=substr($row->d_nota,0,4).substr($row->d_nota,5,2);
				  }
				}
				$data['ispb']    = $ispb;
				$data['isj']     = $isj;
				$data['inota'] 	 = $inota;
				$data['dfrom']   = $dfrom;
				$data['dto']	   = $dto;
				$data['iarea']   = $area;
				$this->load->model('nota/mmaster');
				$data['isi'] 	=$this->mmaster->bacanota($inota,$ispb,$area);
				$data['detail']=$this->mmaster->bacadetailnota($inota,$area);
				
				$query=$this->db->query("	select i_periode from tm_periode ",false);
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
            $periode=$row->i_periode;
				  }
				  if($periode<=$dnota)$data['bisaedit']=true;
			  }
				
		 		$this->load->view('nota/vmainform',$data);
			}else{
				$this->load->view('nota/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function koreksinota() {
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu56')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('nota/mmaster');
			$ispb 	= $this->input->post('ispb', TRUE);
			$dspb 	= $this->input->post('dspb', TRUE);
			if($dspb!=''){
				$tmp=explode("-",$dspb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspb=$th."-".$bl."-".$hr;
			}
			$idkb 	= $this->input->post('idkb', TRUE);
      		if($idkb=='')$idkb=null;
			$ddkb 	= $this->input->post('ddkb', TRUE);
			if($ddkb!=''){
				$tmp=explode("-",$ddkb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$ddkb=$th."-".$bl."-".$hr;
			}else{
				$ddkb=null;
			}
			$ibapb 	= $this->input->post('ibapb', TRUE);
      		if($ibapb=='')$ibapb=null;
			$dbapb 	= $this->input->post('dbapb', TRUE);
			if($dbapb!=''){
				$tmp=explode("-",$dbapb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbapb=$th."-".$bl."-".$hr;
			}else{
				$dbapb=null;
			}
			$isj 	= $this->input->post('isj', TRUE);
			if($isj=='') $isj=null;
			$dsj 	= $this->input->post('dsj', TRUE);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
			}else{
				$dsj=null;
			}
			$inotaold		  = $this->input->post('inotaold', TRUE);
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomername= $this->input->post('ecustomername', TRUE);
			$eremarkacc	  = "Penjualan kepada:".$icustomer."-".$ecustomername;
			$iarea			  = $this->input->post('iarea', TRUE);
			$eareaname		= $this->input->post('eareaname', TRUE);
			$ispbpo			  = $this->input->post('ispbpo', TRUE);
			if($ispbpo=='') $ispbpo=null;
			$isalesman		= $this->input->post('isalesman',TRUE);
			$esalesmanname= $this->input->post('esalesmanname',TRUE);
			$ipricegroup	= $this->input->post('ipricegroup',TRUE);
			$dspbreceive	= $this->input->post('dspbreceive',TRUE);
			$nnotatoplength	= $this->input->post('nspbtoplength',TRUE);
			$nnotadiscount1	= $this->input->post('ncustomerdiscount1',TRUE);
			$nnotadiscount2	= $this->input->post('ncustomerdiscount2',TRUE);
			$nnotadiscount3	= $this->input->post('ncustomerdiscount3',TRUE);
			$nnotadiscount4	= $this->input->post('ncustomerdiscount4',TRUE);
			$vnotadiscount1	= $this->input->post('vcustomerdiscount1',TRUE);
			$vnotadiscount2	= $this->input->post('vcustomerdiscount2',TRUE);
			$vnotadiscount3	= $this->input->post('vcustomerdiscount3',TRUE);
			$vnotadiscount4	= $this->input->post('vcustomerdiscount4',TRUE);
			$vnotadiscounttotal	= $this->input->post('vspbdiscounttotal',TRUE);
			$vnotadiscounttotal	= str_replace(',','',$vnotadiscounttotal);
			$vnota			= $this->input->post('vspbafter',TRUE);
			$vnota			= str_replace(',','',$vnota);
			$nnotadiscount1	= str_replace(',','',$nnotadiscount1);
			$nnotadiscount2	= str_replace(',','',$nnotadiscount2);
			$nnotadiscount3	= str_replace(',','',$nnotadiscount3);
			$nnotadiscount4	= str_replace(',','',$nnotadiscount4);
			$vnotadiscount1	= str_replace(',','',$vnotadiscount1);
			$vnotadiscount2	= str_replace(',','',$vnotadiscount2);
			$vnotadiscount3	= str_replace(',','',$vnotadiscount3);
			$vnotadiscount4	= str_replace(',','',$vnotadiscount4);
			$jml		= $this->input->post('jml', TRUE);
			$djatuhtempo	= $this->input->post('djatuhtempo', TRUE);

			/* PENAMBAHAN HITUNG DPP & PPN 27 AGS 2021 */
			// $vdpp	= $this->input->post('vdpp', TRUE);
			// $vppn	= $this->input->post('vppn', TRUE);
			/* ************************************** */
/*
			if($djatuhtempo!=''){
				$tmp=explode("-",$djatuhtempo);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$djatuhtempo=$th."-".$bl."-".$hr;
			}
*/
      $xstore=substr($isj,8,2);
      if($xstore=='00') $istore='AA'; else $istore=$xstore;
			if($istore=='AA'){
				$istorelocation		= '01';
			}else{
				$istorelocation		= '00';
			}
			$istorelocationbin	= '00';
/*
			if($djatuhtempo!=''){
				$tmp=explode("-",$djatuhtempo);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$djatuhtempo=$th."-".$bl."-".$hr;
			}
*/
			$dnota 	= $this->input->post('dnota', TRUE);
			if($dnota!=''){

				$tmp=explode("-",$dnota);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dnota=$th."-".$bl."-".$hr;
				$iperiode=$th.$bl;
			}
			$inota 		= $this->input->post('inota', TRUE);
			$ispbprogram	= null;
			$eremark 	= $this->input->post('eremark', TRUE);
			$fmasalah 	= $this->input->post('fmasalah', TRUE);
			if($fmasalah==''){
				$fmasalah='f';
			}else{
				$fmasalah='t';
			}
			$finsentif 	= $this->input->post('finsentif', TRUE);
			if($finsentif==''){
				$finsentif='f';
			}else{
				$finsentif='t';
			}
			$flunas 	= $this->input->post('flunas', TRUE);
			if($flunas==''){
				$flunas='f';
			}else{
				$flunas='t';
			}
			$fcicil 	= $this->input->post('fcicil', TRUE);
			if($fcicil==''){
				$fcicil='f';
			}else{
				$fcicil='t';
			}
			$vnotanetto		= $this->input->post('vspbafter',TRUE);
			$vnotanetto		= str_replace(',','',$vnotanetto);
#      		$vsisa			= $this->input->post('vspbafter',TRUE);
#			$vsisa			= str_replace(',','',$vsisa);
      		$vsisa  = $vnotanetto;
			$vspbdiscounttotal	= $this->input->post('vspbdiscounttotal',TRUE);
			$vspbdiscounttotal	= str_replace(',','',$vspbdiscounttotal);
			$vspb			= $this->input->post('vspbbersih',TRUE);
			$vspb			= str_replace(',','',$vspb);
			$fspbplusppn		= $this->input->post('fspbplusppn',TRUE);
			$fspbplusdiscount	= $this->input->post('fspbplusdiscount',TRUE);
			$nprice				= $this->input->post('nprice',TRUE);
			$vnotappn			= $this->input->post('vnotappn',TRUE);
			$vnotappn			= str_replace(',','',$vnotappn);
			$vnotadiscount= $vnotadiscounttotal;
			$ealasan	  	= $this->input->post('ealasan',TRUE);
			if(($dnota!='') && ($inota!='') && ($ealasan!='')){
				$vspbdiscounttotalafter	= $this->input->post('vspbdiscounttotalafter',TRUE);
				$vspbdiscounttotalafter	= str_replace(',','',$vspbdiscounttotalafter);
				$vspbafter				= $this->input->post('vspbafter',TRUE);
				$vspbafter				= str_replace(',','',$vspbafter);
#				if($fspbplusppn=='t'){
				$vnotagross				= $vspbafter+$vspbdiscounttotalafter;
#				}else{
#					$vnotagross	= $this->input->post('vnotagross',TRUE);
#					$vnotagross	= str_replace(',','',$vnotagross);
#				}
				$ispb 					= $this->input->post('ispb', TRUE);
				
				$this->db->trans_begin();
				
				// DISINI ADA PROSES DELETE ITEM di tm_nota_item DI AKHIR SETELAH INSERT KE TABEL tm_notakoreksi
				$this->mmaster->insertheaderkoreksi($inota,$ispb,$iarea,$icustomer,$isalesman,$ispbprogram,$ispbpo,
													$dspb,$dnota,$djatuhtempo,$eremark,$fmasalah,$finsentif,$flunas,
													$nnotatoplength,$nnotadiscount1,$nnotadiscount2,$nnotadiscount3,
													$vnotadiscount1,$vnotadiscount2,$vnotadiscount3,$vnotadiscounttotal,
													$vnotanetto,$vsisa,$vspbdiscounttotal,$vspb,$fspbplusppn,$fspbplusdiscount,
													$nprice,$vnotappn,$vnotagross,$vnotadiscount,$nnotadiscount4,$vnotadiscount4,
													$fcicil,$inotaold,$isj,$dsj,$idkb,$ibapb,$ddkb,$dbapb);

				$this->mmaster->updatespb($ispb,$iarea,$inota, $dnota, $vspbdiscounttotalafter, $vspbafter);

				$this->mmaster->updatenota(	$inota,$ispb,$iarea,$icustomer,$isalesman,$ispbprogram,$ispbpo,$dspb,$dnota,
											$djatuhtempo,$eremark,$fmasalah,$finsentif,$flunas,$nnotatoplength,$nnotadiscount1,
											$nnotadiscount2,$nnotadiscount3,$vnotadiscount1,$vnotadiscount2,$vnotadiscount3,
											$vnotadiscounttotal,$vnotanetto,$vsisa,$vspbdiscounttotal,$vspb,$fspbplusppn,
											$fspbplusdiscount,$nprice,$vnotappn,$vnotagross,$vnotadiscount,$nnotadiscount4,
											$vnotadiscount4,$fcicil,$inotaold,$isj,$dsj,$ealasan);
				
				/* PENAMBAHAN HITUNG DPP PPN 27 AGS 2021 */
				// $this->mmaster->updatenota_dpp_ppn($isj,$iarea,$vdpp,$vppn);
				/* ************************************** */

				// * 25 MAR 2022
				$get = $this->db->query(" SELECT *, n_tax / 100 + 1 excl_divider, n_tax / 100 n_tax_val FROM tr_tax_amount WHERE '$dnota' BETWEEN d_start AND d_finish ")->row();

########## Posting
        		$fclose	= 'f';
        		$gros	= $vspbafter+$vspbdiscounttotalafter;
        		$vpen	= $gros*$get->n_tax_val;
        		$vdis	= $vspbdiscounttotalafter*$get->n_tax_val;
        		$vdpp	= $vpen-$vdis;
        		$vppn	= $vdpp*$get->n_tax_val;
        		$vpiu	= $vdpp+$vppn;
			  	
				$this->mmaster->inserttransheader($inota,$iarea,$eremarkacc,$fclose,$dnota);
			  	$this->mmaster->updatenotaacc($inota,$iarea);
			  	
				$accdebet		= PiutangDagang.$iarea;
			  	$namadebet		= $this->mmaster->namaacc($accdebet);
			  	$accdebet2		= PotonganPenjualan;
			  	$namadebet2		= $this->mmaster->namaacc($accdebet2);
			  	$acckredit		= HasilPenjualanKotor;
			  	$namakredit		= $this->mmaster->namaacc($acckredit);
			  	$acckredit2		= HutangPPN;
			  	$namakredit2	= $this->mmaster->namaacc($acckredit2);
#			  	$this->mmaster->inserttransitemdebet($accdebet,$inota,$namadebet,'t','t',$iarea,$eremarkacc,$vspbafter,$dnota);
#			  	$this->mmaster->updatesaldodebet($accdebet,$iperiode,$vspbafter);
			  	$this->mmaster->inserttransitemdebet($accdebet,$inota,$namadebet,'t','t',$iarea,$eremarkacc,$vpiu,$dnota);
			  	$this->mmaster->updatesaldodebet($accdebet,$iperiode,$vpiu);
			  
				if($vspbdiscounttotalafter!='' && $vspbdiscounttotalafter!=0 && $vspbdiscounttotalafter!='0'){
					$this->mmaster->inserttransitemdebet($accdebet2,$inota,$namadebet2,'t','t',$iarea,$eremarkacc,$vdis,$dnota);
					$this->mmaster->updatesaldodebet($accdebet2,$iperiode,$vdis);
			  	}

			  	$this->mmaster->inserttransitemkredit($acckredit,$inota,$namakredit,'f','t',$iarea,$eremarkacc,$vpen,$dnota);
			  	$this->mmaster->updatesaldokredit($acckredit,$iperiode,$vpen);
			  	$this->mmaster->inserttransitemkredit($acckredit2,$inota,$namakredit2,'f','t',$iarea,$eremarkacc,$vppn,$dnota);
			  	$this->mmaster->updatesaldokredit($acckredit2,$iperiode,$vppn);
			  	$this->mmaster->insertgldebet($accdebet,$inota,$namadebet,'t',$iarea,$vpiu,$dnota,$eremarkacc);
			  	
				if($vspbdiscounttotalafter!='' && $vspbdiscounttotalafter!=0 && $vspbdiscounttotalafter!='0'){
					  $this->mmaster->insertgldebet($accdebet2,$inota,$namadebet2,'t',$iarea,$vdis,$dnota,$eremarkacc);
			  	}
			  	
				$this->mmaster->insertglkredit($acckredit,$inota,$namakredit,'f',$iarea,$vpen,$dnota,$eremarkacc);
			  	$this->mmaster->insertglkredit($acckredit2,$inota,$namakredit2,'f',$iarea,$vppn,$dnota,$eremarkacc);
##########
				for($i=1;$i<=$jml;$i++){
					$iproduct			= $this->input->post('iproduct'.$i, TRUE);
					$iproductgrade		= 'A';
					$iproductmotif		= $this->input->post('motif'.$i, TRUE);
					$eproductname		= $this->input->post('eproductname'.$i, TRUE);
					$vunitprice			= $this->input->post('vproductretail'.$i, TRUE);
					$vunitprice			= str_replace(',','',$vunitprice);
					$ndeliver			= $this->input->post('ndeliver'.$i, TRUE);

				  	// * TAMBAHAN 24 MAR 2022
					$iproductcategory		= $this->input->post('iproductcategory'.$i, true);
					$iproductclass			= $this->input->post('iproductclass'.$i, true);
					$eproductcategoryname	= $this->input->post('eproductcategoryname'.$i, true);
					$eproductclassname		= $this->input->post('eproductclassname'.$i, true);
				  	$itemdiscounttotal 		= $this->input->post('vdiscount'.$i, true);
				  	$vdpp              		= $this->input->post('vdpp'.$i, true);
				  	$vppn              		= $this->input->post('vppn'.$i, true);
				  	$vnettoitem        		= $this->input->post('vnetto'.$i, true);

					// ITEMNYA DI INSERT ULANG DISINI
				  	$this->mmaster->insertdetailkoreksi ($isj,$inota,$iarea,$iproduct,$iproductgrade,$eproductname,$ndeliver, $vunitprice,$iproductmotif,$dnota,$i, $iproductcategory, $iproductclass, $eproductcategoryname, $eproductclassname, $itemdiscounttotal, $vdpp, $vppn, $vnettoitem);
          		  	$this->mmaster->updatespbitem ($ispb,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$iarea,$vunitprice,$i);
###
					$trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
					
					if(isset($trans)){
						foreach($trans as $itrans) {
							$q_aw =$itrans->n_quantity_awal;
							$q_ak =$itrans->n_quantity_akhir;
							$q_in =$itrans->n_quantity_in;
							$q_out=$itrans->n_quantity_out;
							break;
						}
					}else{
						$trans = $this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
						if(isset($trans)){
							foreach($trans as $itrans)
							{
							$q_aw =$itrans->n_quantity_stock;
							$q_ak =$itrans->n_quantity_stock;
							$q_in =0;
							$q_out=0;
							break;
							}
						}else{
							$q_aw=0;
							$q_ak=0;
							$q_in=0;
							$q_out=0;
						}
					}
                	
					$this->mmaster->inserttrans04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$ndeliver,$q_aw,$q_ak);
                	$th=substr($dsj,0,4);
                	$bl=substr($dsj,5,2);
                	$emutasiperiode=$th.$bl;
		
					if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode)) {
						$this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
					}else{
						$this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode,$q_aw);
					}
					if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin)) {
						$this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$q_ak);
					}else{
						$this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ndeliver,$q_aw);
					}
############         
				} // TUTUP FOR ITEM
        		
				$this->mmaster->updatenilaispb($ispb,$iarea);
			} // TUTUP KONDISI TRUE KOREKSI
			
			if ($this->db->trans_status() === FALSE || ($ealasan=='')){
				$this->db->trans_rollback();
			} else {
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Koreksi Nota No:'.$inota;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );

#				$this->db->trans_rollback();
				$this->db->trans_commit();

				$data['sukses']			= true;
				$data['inomor']	= $inota;

				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function pricegroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu56')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $nota=$this->uri->segment(4);
      $area=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/nota/cform/pricegroup/'.$nota.'/'.$area.'/';
      $data['nota']=$this->uri->segment(4);
      $data['area']=$this->uri->segment(5);
      $cari=strtoupper($this->input->post("cari"));
			$query = $this->db->query(" select * from tr_price_group
                                  where upper(i_price_group) like '%$cari%' or upper(e_price_groupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('nota/mmaster');
			$data['page_title'] = $this->lang->line('list_pricegroup');
			$data['isi']=$this->mmaster->bacapricegroup($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('nota/vlistpricegroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function balikspb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu56')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $this->load->model('nota/mmaster');

      $ispb	= $this->uri->segment(4);
      $area	= $this->uri->segment(5);
      $dfrom	= $this->uri->segment(6);
      $dto	= $this->uri->segment(7);
      $iarea	= $area;
      $cari = strtoupper($this->input->post('cari', FALSE));	
	    $this->mmaster->balikspb($ispb,$area);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Balik ke SPB No:'.$ispb.' area:'.$area;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/nota/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
	    $qstr	= " SELECT a.i_sj
                from tm_nota a, tr_customer b, tr_area c, tm_spb d
                where a.i_customer=b.i_customer and a.i_area=c.i_area
                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                  or upper(a.i_spb) like '%$cari%' or upper(d.i_spb_old) like '%$cari%') 
                and a.i_nota isnull and not a.i_sj isnull and a.f_nota_cancel = 'f' 
                and not a.i_dkb isnull
                and a.i_spb=d.i_spb and a.i_area=d.i_area 
                and a.i_area='$iarea' 
                and (a.d_sj >= to_date('$dfrom', 'dd-mm-yyyy') 
                and a.d_sj <= to_date('$dto', 'dd-mm-yyyy')) ";
			$query = $this->db->query($qstr,false);
			$num   = $query->num_rows();
			$config['total_rows'] = $num; 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('cnota');
			$this->load->model('nota/mmaster');
			$data['isj']	= '';
			$data['inota']	= '';
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
      $data['tgl']=date('d-m-Y');
			$data['isi']=$this->mmaster->bacasemua($iarea,$dfrom,$dto,$cari,$config['per_page'],$this->uri->segment(8));
			$data['noakhir']=$this->mmaster->bacaakhir($iarea,$dfrom,$dto,$cari,$config['per_page'],$this->uri->segment(8));
			$this->load->view('nota/vmainform', $data);
/*
      $data['noakhir']=$this->mmaster->bacaakhir($iarea,$dfrom,$dto,$cari,$config['per_page'],$this->uri->segment(8));
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');

      $config['base_url'] = base_url().'index.php/nota/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$qstr	= " SELECT a.i_sj
                from tm_nota a, tr_customer b, tr_area c, tm_spb d
                where a.i_customer=b.i_customer and a.i_area=c.i_area
                and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                  or upper(a.i_spb) like '%$cari%' or upper(d.i_spb_old) like '%$cari%') 
                and a.i_nota isnull and not a.i_sj isnull and a.f_nota_cancel = 'f' 
                and not a.i_dkb isnull
                and a.i_spb=d.i_spb and a.i_area=d.i_area 
                and a.i_area='$iarea' 
                and (a.d_sj >= to_date('$dfrom', 'dd-mm-yyyy') 
                and a.d_sj <= to_date('$dto', 'dd-mm-yyyy')) ";
			$query = $this->db->query($qstr,false);
			$num   = $query->num_rows();
			$config['total_rows'] = $num; 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';

			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('cnota');
			$this->load->model('nota/mmaster');
			$data['isj']	= '';
			$data['inota']	= '';
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
      $data['tgl']=date('d-m-Y');
			$data['isi']=$this->mmaster->bacasemua($iarea,$dfrom,$dto,$cari,$config['per_page'],$this->uri->segment(8));
			$this->load->view('nota/vmainform', $data);
*/
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/nota/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);

			$this->pagination->initialize($config);
			$this->load->model('listnota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('nota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/nota/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';



			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('nota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('nota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function salesman()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/nota/cform/salesman/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari = strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
			$query = $this->db->query(" select * from tr_salesman
									               	where (upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%') ",false);
			}else{
			$query = $this->db->query(" select * from tr_salesman
									               	where (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									                or i_area = '$area4' or i_area = '$area5')
                                  and (upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%') ",false);
			}
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('nota/mmaster');
			$data['page_title'] = $this->lang->line('list_salesman');
			$data['isi']=$this->mmaster->bacasalesman($cari,$area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(5));
			$this->load->view('nota/vlistsalesman', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari  = strtoupper($this->input->post('cari', FALSE));
      $iarea = strtoupper($this->input->post('iarea', FALSE));
 			if($iarea=='') $iarea=$this->uri->segment(4);
      if($this->uri->segment(5)!='x01'){
        if($cari=='') $cari=$this->uri->segment(5);
        $config['base_url'] = base_url().'index.php/nota/cform/customer/'.$iarea.'/'.$cari.'/';
      }else{
        $config['base_url'] = base_url().'index.php/nota/cform/customer/'.$iarea.'/x01/';
      }
			$query 	= $this->db->query("select * from tr_customer a 
										left join tr_customer_pkp b on
										(a.i_customer=b.i_customer) 
										left join tr_price_group c on
										(a.i_price_group=c.n_line or a.i_price_group=c.i_price_group) 
										left join tr_customer_area d on
										(a.i_customer=d.i_customer) 
										left join tr_customer_salesman e on
										(a.i_customer=e.i_customer and e.i_product_group='01')
										left join tr_customer_discount f on
										(a.i_customer=f.i_customer) where a.i_area='$iarea' and a.f_approve='t'
										and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('nota/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('nota/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari   = strtoupper($this->input->post('cari', FALSE));
      $baris  = strtoupper($this->input->post('baris', FALSE));
      $kdharga= strtoupper($this->input->post('kdharga', FALSE));
 			if($baris=='') $baris=$this->uri->segment(4);
 			if($kdharga=='') $kdharga=$this->uri->segment(5);
      if($this->uri->segment(6)!='x01'){
        if($cari=='') $cari=$this->uri->segment(6);
        $config['base_url'] = base_url().'index.php/nota/cform/product/'.$baris.'/'.$kdharga.'/'.$cari.'/';
      }else{
        $config['base_url'] = base_url().'index.php/nota/cform/product/'.$baris.'/'.$kdharga.'/x01/';
      }
			$str	= " select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama,b.v_product_retail as harga
								from tr_product_motif a,tr_product_price b,tr_product c, tr_product_type d
								where 
								d.i_product_type=c.i_product_type 
                and a.i_product_motif='00'
								and b.i_product=a.i_product
								and a.i_product=c.i_product
								and b.i_price_group='$kdharga'
								and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') ";
#and d.i_product_group='01'
			$query = $this->db->query($str,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('nota/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($cari,$config['per_page'],$this->uri->segment(7),$kdharga);
			$data['baris']=$baris;
			$data['kdharga']=$kdharga;
			$data['cari']=$cari;
			$this->load->view('nota/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$kdharga=$this->uri->segment(5);
			$cari 	= strtoupper($this->input->post('cari'));
			if($cari!=FALSE) $cari	= strtoupper($cari);
			if($cari==FALSE) $cari =$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/nota/cform/product/'.$baris.'/'.$kdharga.'/'.$cari.'/';
			$str	= "select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,b.v_product_retail as harga
										from tr_product_motif a,tr_product_price b,tr_product c
										where b.i_product=a.i_product and a.i_product_motif='00'
										and a.i_product=c.i_product
									   	and b.i_price_group='$kdharga'
										and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') ";
			$query 	= $this->db->query($str,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('nota/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$kdharga,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$data['kdharga']=$kdharga;
			$data['cari']=$cari;
			$this->load->view('nota/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
