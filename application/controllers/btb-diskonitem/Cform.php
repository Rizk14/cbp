<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationxx');
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['page_title'] = $this->lang->line('btb');
         $data['ido']='';
         $this->load->model('btb-diskonitem/mmaster');
         $data['isi']='';#$this->mmaster->bacasemua();
         $data['detail']="";
         $data['jmlitem']="";
      $data['tgl']=date('d-m-Y');
         $this->load->view('btb-diskonitem/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function simpan(){
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $ido  = $this->input->post('ido', TRUE);
         $ddo  = $this->input->post('ddo', TRUE);
         if($ddo!=''){
            $tmp=explode("-",$ddo);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $ddo=$th."-".$bl."-".$hr;
        $thbl=substr($th,2,2).$bl;
         }
         $isupplier     = $this->input->post('isupplier', TRUE);
/*
         if($isupplier=='SP030'){ #duta
            $ido='DO-'.$thbl.'-DT'.$ido;
         }elseif($isupplier=='SP002'){ #immanuel
            $ido='DO-'.$thbl.'-IM'.$ido;
         }elseif($isupplier=='SP004'){ #tegar
            $ido='DO-'.$thbl.'-TG'.$ido;
         }elseif($isupplier=='SP021'){ #harmoni
            $ido='DO-'.$thbl.'-HO'.$ido;
         }elseif($isupplier=='SP079'){ #cka
            $ido='DO-'.$thbl.'-CK'.$ido;
         }elseif($isupplier=='SP081'){ #popon
            $ido='DO-'.$thbl.'-PN'.$ido;
         }elseif($isupplier=='SP078'){ #tirai
            $ido='DO-'.$thbl.'-TP'.$ido;
         }elseif($isupplier=='SP088'){ #KAM
            $ido='DO-'.$thbl.'-KA'.$ido;
         }
*/
      $ido='DO-'.$thbl.'-'.$ido;
         $iarea      = $this->input->post('iarea', TRUE);
         $iop        = $this->input->post('iop', TRUE);
         $vdogross   = $this->input->post('vdogross',TRUE);
         $vdogross   = str_replace(',','',$vdogross);
         $vdonetto   = $this->input->post('vdonetto',TRUE);
         $vdonetto   = str_replace(',','',$vdonetto);
         $jml        = $this->input->post('jml', TRUE);
         if(
            ($ido!='')
            && ($isupplier!='')
            && (($vdogross!='') || ($vdogross!='0'))
            && ($iop!='')
            && ($ddo!='')
            && ($jml>0)
           )
         {
            $this->db->trans_begin();
            $this->load->model('btb-diskonitem/mmaster');
            $query=$this->db->query("select * from tm_do where i_do='$ido' and i_supplier='$isupplier'");
            if($query->num_rows()==0){
/*===================================================================================================================================================*/
#              $ibbm          = $this->mmaster->runningnumberbbm($thbl);
#              $dbbm          = $ddo;
               $istore               = 'AA';
               $istorelocation      = '01';
               $istorelocationbin= '00';
#              $eremark       = 'DO Manual';
#              $ibbktype         = '05';
#              $ibbmtype         = '04';

#              $ibbk = $this->mmaster->runningnumberbbk($thbl);
#              $dbbk = $ddo;
#              $this->mmaster->insertbbkheader($ido,$ddo,$ibbk,$dbbk,$ibbktype,$eremark,$iarea,$isupplier);
/*===================================================================================================================================================*/
               $this->mmaster->insertheader($ido,$isupplier,$iop,$iarea,$ddo,$vdogross,$vdonetto);
/*===================================================================================================================================================*/
#              $this->mmaster->insertbbmheader($ido,$ddo,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea, $isupplier);
/*===================================================================================================================================================*/
               for($i=1;$i<=$jml;$i++){
               $iproduct            = $this->input->post('iproduct'.$i, TRUE);
                  $iproductgrade       = 'A';
                  $iproductmotif       = $this->input->post('motif'.$i, TRUE);
                  $eproductname        = $this->input->post('eproductname'.$i, TRUE);
                  $vproductmill        = $this->input->post('vproductmill'.$i, TRUE);
                  $ndiskon        = $this->input->post('ndiskon'.$i, TRUE);
                  $vdiskon        = $this->input->post('vdiskon'.$i, TRUE);
                  $vproductmill        = str_replace(',','',$vproductmill);
                  $vdiskon            = str_replace(',','',$vdiskon);
                  $ndeliver            = $this->input->post('ndeliver'.$i, TRUE);
                  $eremark          = $this->input->post('eremark'.$i, TRUE);
                  $this->mmaster->insertdetail($iop,$ido,$isupplier,$iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,$vproductmill,$ddo,$eremark,$i,$ido,$vdiskon,$ndiskon);
                  $this->mmaster->updateopdetail($iop,$iproduct,$iproductgrade,$iproductmotif,$ndeliver);
                  $this->mmaster->updatespbdetail($iop,$iproduct,$iproductgrade,$iproductmotif,$ndeliver);
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ido,$q_in,$q_out,$ndeliver,$q_aw,$q_ak);
            $th=substr($ddo,0,4);
            $bl=substr($ddo,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$q_ak);
            }else{
              $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ndeliver);
            }
/*===================================================================================================================================================*/
#               $this->mmaster->insertbbmdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$ndeliver,
#                                         $vproductmill,$ido,$ibbm,$eremark,$ddo);
#                 $this->mmaster->insertbbkdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$ndeliver,
#                                         $vproductmill,$ido,$ibbk,$eremark,$ddo,$ibbktype,
#                                         $istore,$istorelocation,$istorelocationbin);
/*===================================================================================================================================================*/
               }
               if ( ($this->db->trans_status() === FALSE) )
               {
                  $this->db->trans_rollback();
               }else{
                  $this->db->trans_commit();
                  $sess=$this->session->userdata('session_id');
                  $id=$this->session->userdata('user_id');
                  $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                  $rs      = pg_query($sql);
                  if(pg_num_rows($rs)>0){
                     while($row=pg_fetch_assoc($rs)){
                        $ip_address   = $row['ip_address'];
                        break;
                     }
                  }else{
                     $ip_address='kosong';
                  }
                  $query   = pg_query("SELECT current_timestamp as c");
                  while($row=pg_fetch_assoc($query)){
                     $now    = $row['c'];
                  }
                  $pesan='Input DO No:'.$ido.' Supplier:'.$isupplier;
                  $this->load->model('logger');
                  $this->logger->write($id, $ip_address, $now , $pesan );
                  $data['sukses']         = true;
                  $data['inomor']         = $ido;
                  $this->load->view('nomor',$data);
               }
            }else{
              $this->db->trans_commit();
               $data['pesan']       = 'Nomor DO '.$ido.' sudah ada !!!';
               $this->load->view('nomorada',$data);
            }
         }else{
            //$this->load->view('awal/index.php');
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['page_title'] = $this->lang->line('btb');
         $this->load->view('btb-diskonitem/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function edit()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['page_title'] = $this->lang->line('btb')." update";
         if(
            ($this->uri->segment(4)!=='') && ($this->uri->segment(5)!=='')
           ){
          $ido      = $this->uri->segment(4);
            $ido          = str_replace('%20',' ',$ido);
               $isupplier    = $this->uri->segment(5);
               $data['dfrom']= $this->uri->segment(6);
               $data['dto']  = $this->uri->segment(7);
               $data['iarea']= $this->uri->segment(8);
               $query         = $this->db->query("select * from tm_do_item where i_do = '$ido' and i_supplier='$isupplier'");
               $data['jmlitem'] = $query->num_rows();
               $data['ido'] = $ido;
               $data['isupplier']=$isupplier;
               $this->load->model('btb-diskonitem/mmaster');
               $data['isi']=$this->mmaster->baca($ido,$isupplier);
               $data['detail']=$this->mmaster->bacadetail($ido, $isupplier);
               $this->load->view('btb-diskonitem/vmainform',$data);
         }else{
            $this->load->view('btb-diskonitem/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function update()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $idoold  = $this->input->post('idoold', TRUE);
         $ido  = $this->input->post('ido', TRUE);
         $ido  = trim($ido);
         $ddo  = $this->input->post('ddo', TRUE);
         if($ddo!=''){
            $tmp=explode("-",$ddo);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $ddo=$th."-".$bl."-".$hr;
            $tahun=$th;
         }
         $isupplier  = $this->input->post('isupplier', TRUE);
         $iarea         = $this->input->post('iarea', TRUE);
         $iop          = $this->input->post('iop', TRUE);
         $vdogross      = $this->input->post('vdogross',TRUE);
         $vdogross      = str_replace(',','',$vdogross);
         $jml          = $this->input->post('jml', TRUE);
         $istore           = 'AA';
         $istorelocation      = '01';
         $istorelocationbin= '00';
         $eremark       = 'DO Manual';
         if(
            ($ido!='')
            && ($isupplier!='')
            && (($vdogross!='') || ($vdogross!='0'))
            && ($iop!='')
            && ($ddo!='')
           )
         {
            $this->db->trans_begin();
            $this->load->model('btb-diskonitem/mmaster');
//          $this->mmaster->updateheader($ido,$isupplier,$iop,$ifaktur,$iarea,$ddo,$vdogross);
            $this->mmaster->updateheader($ido,$isupplier,$iop,$iarea,$ddo,$vdogross,$idoold);
/*===================================================================================================================================================*/
#           $this->mmaster->updatebbmheader($ido,$ddo,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea,$isupplier);
#           $this->mmaster->updatebbkheader($ido,$ddo,$ibbk,$dbbk,$ibbktype,$eremark,$iarea,$isupplier);
/*===================================================================================================================================================*/
            for($i=1;$i<=$jml;$i++){
               $iproduct               = $this->input->post('iproduct'.$i, TRUE);
               $iproductgrade    = 'A';
               $iproductmotif    = $this->input->post('motif'.$i, TRUE);
               $eproductname        = $this->input->post('eproductname'.$i, TRUE);
               $vproductmill        = $this->input->post('vproductmill'.$i, TRUE);
               $vproductmill        = str_replace(',','',$vproductmill);
               $ndeliver               = $this->input->post('ndeliver'.$i, TRUE);
               $ndeliver         = str_replace(',','',$ndeliver);
          $ntmp                = $this->input->post('ntmp'.$i, TRUE);
               $ntmp              = str_replace(',','',$ntmp);
               $eremark             = $this->input->post('eremark'.$i, TRUE);
               $this->mmaster->deletedetail($iproduct, $iproductgrade, $ido, $isupplier, $iproductmotif,$tahun,$idoold);

          $th=substr($ddo,0,4);
              $bl=substr($ddo,5,2);
              $emutasiperiode=$th.$bl;
              $tra=$this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ido,$ntmp,$eproductname);
          if( ($ntmp!='') && ($ntmp!=0) ){
               $this->mmaster->updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ntmp,$emutasiperiode);
               $this->mmaster->updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ntmp);
          }

               $this->mmaster->insertdetail($iop,$ido,$isupplier,$iproduct,$iproductgrade,$iproductmotif,$eproductname,
                                     $ndeliver,$vproductmill,$ddo,$eremark,$i,$idoold);
##############
          $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
          if(isset($trans)){
            foreach($trans as $itrans)
            {
              $q_aw =$itrans->n_quantity_awal;
              $q_ak =$itrans->n_quantity_akhir;
              $q_in =$itrans->n_quantity_in;
              $q_out=$itrans->n_quantity_out;
              break;
            }
          }else{
            $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_stock;
                $q_ak =$itrans->n_quantity_stock;
                $q_in =0;
                $q_out=0;
                break;
              }
            }else{
              $q_aw=0;
              $q_ak=0;
              $q_in=0;
              $q_out=0;
            }
          }
          $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ido,$q_in,$q_out,$ndeliver,$q_aw,$q_ak);
          $th=substr($ddo,0,4);
          $bl=substr($ddo,5,2);
          $emutasiperiode=$th.$bl;
          if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
          {
            $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
          }else{
            $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
          }
          if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
          {
            $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$q_ak);
          }else{
            $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ndeliver);
          }
##############
               $this->mmaster->updateopdetail($iop,$iproduct,$iproductgrade,$iproductmotif,$ndeliver);
#              $this->mmaster->insertbbmdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$ndeliver,
#                                      $vproductmill,$ido,$ibbm,$eremark,$ddo);
#              $this->mmaster->insertbbkdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$ndeliver,
#                                      $vproductmill,$ido,$ibbk,$eremark,$ddo,$ibbktype,
#                                      $istore,$istorelocation,$istorelocationbin);
            }
            if ( ($this->db->trans_status() === FALSE) )
            {
                $this->db->trans_rollback();
            }else{
              $this->db->trans_commit();
          $sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
                  $now    = $row['c'];
               }
               $pesan='Update DO No:'.$ido.' Supplier:'.$isupplier;
               $this->load->model('logger');
               $data['sukses']         = true;
               $data['inomor']         = $ido;
               $this->load->view('nomor',$data);
            }
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function delete()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $ido     = $this->input->post('idodelete', TRUE);
         $isupplier  = $this->input->post('isupplierdelete', TRUE);
         $iop     = $this->input->post('iopdelete', TRUE);
         $this->load->model('btb-diskonitem/mmaster');
         $this->mmaster->delete($ido,$isupplier,$iop);
         $sess=$this->session->userdata('session_id');
         $id=$this->session->userdata('user_id');
         $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
         $rs      = pg_query($sql);
         if(pg_num_rows($rs)>0){
            while($row=pg_fetch_assoc($rs)){
               $ip_address   = $row['ip_address'];
               break;
            }
         }else{
            $ip_address='kosong';
         }
         $query   = pg_query("SELECT current_timestamp as c");
         while($row=pg_fetch_assoc($query)){
            $now    = $row['c'];
         }
         $pesan='Delete DO No:'.$ido.' Supplier:'.$isupplier;
         $this->load->model('logger');
         $this->logger->write($id, $ip_address, $now , $pesan );

         $data['page_title'] = $this->lang->line('btb');
         $data['ido']='';
         $data['isupplier']='';
         $data['jmlitem']='';
         $data['detail']='';
         $data['isi']=$this->mmaster->bacasemua();
         $this->load->view('btb-diskonitem/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function deletedetail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $ido        = $this->input->post('idodelete', TRUE);
         $isupplier     = $this->input->post('isupplierdelete', TRUE);
         $iproduct      = $this->input->post('iproductdelete', TRUE);
         $iproductgrade = $this->input->post('iproductgradedelete', TRUE);
         $iproductmotif = $this->input->post('iproductmotifdelete', TRUE);
//       $vdogross      = $this->input->post('vdogross',TRUE);
//       $vdogross      = str_replace(',','',$vdogross);
         $vdogross      = $this->uri->segment(4);
         $iop        = $this->input->post('iop', TRUE);
         $iarea         = $this->input->post('iarea',TRUE);
         $ddo        = $this->input->post('ddo',TRUE);
         if($ddo!=''){
            $tmp=explode("-",$ddo);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $ddo=$th."-".$bl."-".$hr;
         }
         $this->db->trans_begin();
         $this->load->model('btb-diskonitem/mmaster');
         $tahun=substr($ddo,0,4);
         $this->mmaster->uphead($ido,$isupplier,$iop,$iarea,$ddo,$vdogross);
         $this->mmaster->deletedetail($iproduct, $iproductgrade, $ido, $isupplier, $iproductmotif,$tahun);
         if ($this->db->trans_status() === FALSE)
         {
          $this->db->trans_rollback();
         }else{
          $this->db->trans_commit();

            $sess=$this->session->userdata('session_id');
            $id=$this->session->userdata('user_id');
            $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs      = pg_query($sql);
            if(pg_num_rows($rs)>0){
               while($row=pg_fetch_assoc($rs)){
                  $ip_address   = $row['ip_address'];
                  break;
               }
            }else{
               $ip_address='kosong';
            }
            $query   = pg_query("SELECT current_timestamp as c");
            while($row=pg_fetch_assoc($query)){
               $now    = $row['c'];
            }
            $pesan='Delete Item DO No:'.$ido.' Supplier:'.$isupplier;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now , $pesan );

            $data['page_title'] = $this->lang->line('btb')." Update";
            $query = $this->db->query("select * from tm_do_item  where i_do = '$ido' and i_supplier='$isupplier'");
            $data['jmlitem'] = $query->num_rows();
            $data['ido'] = $ido;
            $data['isupplier']=$isupplier;
            $data['isi']=$this->mmaster->baca($ido,$isupplier);
            $data['detail']=$this->mmaster->bacadetail($ido,$isupplier);
            $this->load->view('btb-diskonitem/vmainform', $data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function product()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['baris']=$this->uri->segment(4);
         $data['op']=$this->uri->segment(5);
         $baris=$this->uri->segment(4);
         $op=$this->uri->segment(5);
         $config['base_url'] = base_url().'index.php/btb-diskonitem/cform/product/'.$baris.'/'.$op.'/';
         $query = $this->db->query(" select a.i_product||a.i_product_motif as kode,
                                         c.e_product_name as nama, d.v_product_mill as harga, b.n_order
                                         from tr_product_motif a,tr_product c,tm_op_item b, tr_harga_beli d
                                         where b.i_op='$op' and a.i_product=c.i_product and (b.n_delivery<b.n_order or b.n_delivery isnull)
                                         and b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
                                  and b.i_product=d.i_product and d.i_price_group='00'" ,false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);

         $this->load->model('btb-diskonitem/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
         $data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6),$op);
         $data['baris']=$baris;
         $this->load->view('btb-diskonitem/vlistproduct', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cariproduct()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['baris']=$this->uri->segment(4);
         $data['op']=$this->uri->segment(5);
         $baris=$this->uri->segment(4);
         $op=$this->uri->segment(5);
         $config['base_url'] = base_url().'index.php/btb-diskonitem/cform/product/'.$baris.'/'.$op.'/';
         $cari = $this->input->post('cari', FALSE);
         $cari =strtoupper($cari);
         $query = $this->db->query("   select a.i_product as kode,
                              a.i_product_motif as motif,
                              a.e_product_motifname as namamotif,
                              c.e_product_name as nama,
                              c.v_product_mill as harga
                              from tr_product_motif a,tr_product c, tm_op_item b
                              where b.i_op='$op' and a.i_product=c.i_product and (b.n_delivery<b.n_order or b.n_delivery isnull)
                              and b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
                                 and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
                             ,false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('btb-diskonitem/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
         $data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6),$op);
         $data['baris']=$baris;
         $this->load->view('btb-diskonitem/vlistproduct', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function productupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['baris']=$this->uri->segment(4);
         $data['op']=$this->uri->segment(5);
         $baris=$this->uri->segment(4);
         $op=$this->uri->segment(5);
         $config['base_url'] = base_url().'index.php/btb-diskonitem/cform/productupdate/'.$baris.'/'.$op.'/';
         $query = $this->db->query(" select a.i_product||a.i_product_motif as kode,
                     c.e_product_name as nama,c.v_product_mill as harga, b.n_order
                     from tr_product_motif a,tr_product c,tm_op_item b
                     where b.i_op='$op' and a.i_product=c.i_product
                     and b.i_product=a.i_product and b.i_product_motif=a.i_product_motif" ,false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);

         $this->load->model('btb-diskonitem/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
         $data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6),$op);
         $this->load->view('btb-diskonitem/vlistproductupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cariproductupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['baris']=$this->uri->segment(4);
         $data['op']=$this->uri->segment(5);
         $baris=$this->uri->segment(4);
         $op=$this->uri->segment(5);
         $config['base_url'] = base_url().'index.php/btb-diskonitem/cform/productupdate/'.$baris.'/'.$op.'/';
         $cari = $this->input->post('cari', FALSE);
         $cari =strtoupper($cari);
         $query = $this->db->query("   select a.i_product as kode,
                     a.i_product_motif as motif,
                     a.e_product_motifname as namamotif,
                     c.e_product_name as nama,
                     c.v_product_mill as harga
                     from tr_product_motif a,tr_product c, tm_op_item b
                     where b.i_op='$op' and a.i_product=c.i_product
                     and b.i_product=a.i_product and b.i_product_motif=a.i_product_motif
                        and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
                             ,false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('btb-diskonitem/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
         $data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6),$op);
         $data['baris']=$baris;
         $this->load->view('btb-diskonitem/vlistproductupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function supplier()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $config['base_url'] = base_url().'index.php/btb-diskonitem/cform/supplier/index/';
         $query = $this->db->query("select * from tr_supplier ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('btb-diskonitem/mmaster');
         $data['page_title'] = $this->lang->line('list_supplier');
         $data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
         $this->load->view('btb-diskonitem/vlistsupplier', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carisupplier()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $config['base_url'] = base_url().'index.php/btb-diskonitem/cform/supplier/index/';
         $cari = $this->input->post('cari', FALSE);
         $cari =strtoupper($cari);
         $query = $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%'
                             or upper(e_supplier_name) like '%$cari%' ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('btb-diskonitem/mmaster');
         $data['page_title'] = $this->lang->line('list_supplier');
         $data['isi']=$this->mmaster->carisupplier($cari, $config['per_page'],$this->uri->segment(5));
         //$data['iarea']=$iarea;
         $this->load->view('btb-diskonitem/vlistsupplier', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cari()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $cari = $this->input->post('cari', FALSE);
         $cari =strtoupper($cari);
         $config['base_url'] = base_url().'index.php/btb-diskonitem/cform/index/';
         $query=$this->db->query("select * from tm_do where upper(i_do) like '%$cari%'",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(4);
         $this->pagination->initialize($config);
         $this->load->model('btb-diskonitem/mmaster');
         $data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
         $data['page_title'] = $this->lang->line('btb-diskonitem');
         $data['ido']='';
         $data['isupplier']='';
         $data['jmlitem']='';
         $data['detail']='';
         $this->load->view('btb-diskonitem/vmainform',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function op()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $config['base_url'] = base_url().'index.php/btb-diskonitem/cform/op/index/';
         $config['per_page'] = '10';
         $query = $this->db->query("   select a.*, b.e_supplier_name, c.e_area_name
                                            from tm_op a,tr_supplier b, tr_area c
                                            where a.i_supplier=b.i_supplier and a.i_area=c.i_area
                                            and b.i_supplier_group<>'G0000'
                                            and a.f_op_cancel='f' and a.f_op_close='f'
                                            and a.i_op in (select i_op from tm_op_item where (n_delivery isnull or n_delivery<n_order))
                                            order by b.e_supplier_name, a.i_op",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('btb-diskonitem/mmaster');
         $data['page_title'] = $this->lang->line('list_op');
         $data['isi']=$this->mmaster->bacaop($config['per_page'],$this->uri->segment(5));
         $this->load->view('btb-diskonitem/vlistop', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cariop()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $config['base_url'] = base_url().'index.php/btb-diskonitem/cform/op/';
         $cari = $this->input->post('cari', FALSE);
         $cari =strtoupper($cari);
         $query = $this->db->query("   select a.*, b.e_supplier_name, c.e_area_name
                     from tm_op a, tr_supplier b, tr_area c
                     where (upper(a.i_op) like '%$cari%' or upper(a.i_supplier) like '%$cari%'
                     or upper(b.e_supplier_name) like '%$cari%'
                     or upper(a.i_reff) like '%$cari%')
                     and a.i_area=c.i_area
                     and b.i_supplier_group<>'G0000'
                     and a.i_op in (select i_op from tm_op_item where (n_delivery isnull or n_delivery<n_order))
                     and a.f_op_cancel='f' and a.f_op_close='f'
                     and a.i_supplier=b.i_supplier
                     order by b.e_supplier_name, a.i_op",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(4);
         $this->pagination->initialize($config);
         $this->load->model('btb-diskonitem/mmaster');
         $data['page_title'] = $this->lang->line('list_op');
         $data['isi']=$this->mmaster->cariop($cari, $config['per_page'],$this->uri->segment(4));

         //$data['iarea']=$iarea;
         $this->load->view('btb-diskonitem/vlistop', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function bacaitem()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['page_title'] = $this->lang->line('btb-diskonitem')." update";
         if(
            ($this->uri->segment(4)!=='') && ($this->uri->segment(5)!=='')
           ){
        $data['op']=$this->uri->segment(5);
        $op=$this->uri->segment(5);
        $config['base_url'] = base_url().'index.php/btb-diskonitem/cform/product/'.$op.'/';

        $query = $this->db->query(" select a.i_product||a.i_product_motif as kode,
                    c.e_product_name as nama,c.v_product_mill as harga, b.n_order, b.v_op_discount, b.n_op_discount
                    from tr_product_motif a,tr_product c,tm_op_item b
                    where b.i_op='$op' and a.i_product=c.i_product and (b.n_delivery<b.n_order or b.n_delivery isnull)
                    and b.i_product=a.i_product and b.i_product_motif=a.i_product_motif" ,false);
        $config['total_rows'] = $query->num_rows();
        $config['per_page'] = '10';
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = 'Selanjutnya';
        $config['prev_link'] = 'Sebelumnya';
        $config['cur_page'] = $this->uri->segment(6);
        $this->pagination->initialize($config);




        $this->load->model('btb-diskonitem/mmaster');
        $data['page_title'] = $this->lang->line('list_product');
        $data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6),$op);
        $data['baris']=$baris;
        $this->load->view('btb-diskonitem/vlistproduct', $data);
         }else{
            $this->load->view('btb-diskonitem/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function bacaitemop()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu67')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['page_title'] = $this->lang->line('btb');
         if(
            ($this->uri->segment(4)!=='') && ($this->uri->segment(5)!=='')
           ){
            $iop            = $this->uri->segment(4);
            $iarea          = $this->uri->segment(5);
            $eareaname      = $this->uri->segment(6);
            $isupplier      = $this->uri->segment(7);
            $esuppliername = str_replace('%20',' ',$this->uri->segment(8));
            $ddo             = $this->uri->segment(9);
            $xdo             = $this->uri->segment(10);
            $query   = $this->db->query("select * from tm_op_item where i_op = '$iop' and (n_delivery<n_order or n_delivery isnull)");
            $data['jmlitem'] = $query->num_rows();
            $data['ido'] = '';
            $this->load->model('btb-diskonitem/mmaster');
            $data['isi']='x';
            $data['iop']=$iop;
            $data['iarea']=$iarea;
            $data['eareaname']=$eareaname;
            $data['isupplier']=$isupplier;
            $data['esuppliername']=$esuppliername;
            $data['xdo']=$xdo;
            $data['ddo']=$ddo;
            $data['tgl']=date('d-m-Y');
            #$data['total']=$this->mmaster->hitungtotal($iop);
            $data['total']=0;
            $data['detail']=$this->mmaster->bacadetailop($iop);
            $this->load->view('btb-diskonitem/vmainform',$data);
         }else{
            $this->load->view('btb-diskonitem/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
