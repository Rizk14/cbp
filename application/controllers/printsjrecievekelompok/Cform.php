<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu592')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printsjrecieve');
			$data['sjfrom']='';
			$data['sjto']	='';
			$this->load->view('printsjrecievekelompok/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function sjfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu592')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= strtoupper($this->input->post('dfrom'));
			$dto	  = strtoupper($this->input->post('dto'));
      if($dfrom=='') $dfrom  =$this->uri->segment(4);
      if($dto=='') $dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/printsjrecievekelompok/cform/sjfrom/'.$dfrom.'/'.$dto.'/';
			$query = $this->db->query("	select i_sj from tm_nota where upper(i_sj) like '%$cari%'
                                  and (substring(i_sj,9,2)='$area1' or substring(i_sj,9,2)='$area2' or 
                                  substring(i_sj,9,2)='$area3' or substring(i_sj,9,2)='$area4' or substring(i_sj,9,2)='$area5')
                                  and d_sj >= to_date('$dfrom','dd-mm-yyyy') and d_sj <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('printsjrecievekelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
			$data['isi']=$this->mmaster->bacasj($cari,$dfrom,$dto,$config['per_page'],$this->uri->segment(6),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('printsjrecievekelompok/vlistsjfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function sjto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu592')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= strtoupper($this->input->post('dfrom'));
			$dto	  = strtoupper($this->input->post('dto'));
      if($dfrom=='') $dfrom  =$this->uri->segment(4);
      if($dto=='') $dto=$this->uri->segment(5);
			$area	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printsjrecievekelompok/cform/sjto/'.$dfrom.'/'.$dto.'/'.$area.'/';
			$query = $this->db->query("	select i_sj from tm_nota where upper(i_sj) like '%$cari%'
                                  and (substring(i_sj,9,2)='$area' or substring(i_sj,9,2)='$area' or 
                                  substring(i_sj,9,2)='$area' or substring(i_sj,9,2)='$area' or substring(i_sj,9,2)='$area')
                                  and d_sj >= to_date('$dfrom','dd-mm-yyyy') and d_sj <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('printsjrecievekelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
      $data['area']=$area;
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
			$data['isi']=$this->mmaster->bacasj2($cari,$dfrom,$dto,$config['per_page'],$this->uri->segment(7),$area);
			$this->load->view('printsjrecievekelompok/vlistsjto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu592')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
#			$area	  = $this->input->post('areafrom');
			$sjfrom = $this->input->post('sjfrom');
			$sjto	  = $this->input->post('sjto');
			$this->load->model('printsjrecievekelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['master']	= $this->mmaster->bacamaster($sjfrom,$sjto);
			$data['area'] 	= substr($sjfrom,8,2);#$area;
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
      $data['sjfrom'] = $sjfrom;
      $data['sjto']   = $sjto;
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak SJ No:'.$sjfrom.' s/d No:'.$sjto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printsjrecievekelompok/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
