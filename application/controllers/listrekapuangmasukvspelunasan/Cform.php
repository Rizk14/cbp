<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu506')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listrekapuangmasukvspelunasan');
			$data['iperiode']='';
			$data['iarea']='';
			$this->load->view('listrekapuangmasukvspelunasan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu506')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode		= $this->input->post('iperiode');
			if($iperiode=='') $iperiode	= $this->uri->segment(4);
			$this->load->model('listrekapuangmasukvspelunasan/mmaster');
			$data['page_title'] = $this->lang->line('listrekapuangmasukvspelunasan');
			$data['iperiode']		= $iperiode;
			$data['isi']		= $this->mmaster->bacaperiode($iperiode);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Informasi Rekap Uang Masuk vs Pelunasan Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listrekapuangmasukvspelunasan/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu506')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listrekapuangmasukvspelunasan');
			$this->load->view('listrekapuangmasukvspelunasan/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu506')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   
			$iperiode		= $this->input->post('iperiode');
			if($iperiode=='') $iperiode	= $this->uri->segment(4);
			$this->load->model('listrekapuangmasukvspelunasan/mmaster');
			$data['page_title'] = $this->lang->line('listrekapuangmasukvspelunasan');
			$data['iperiode']		= $iperiode;
			$isi		            = $this->mmaster->bacaperiode($iperiode);
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();

			$objPHPExcel->getProperties()->setTitle("Laporan Penjualan (Harga Beli)")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      if(count($isi)>0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(

						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,

						'wrap'      => true
					)
				),
				'A1:E1'
				);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'KODE BARANG');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(

							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'NAMA BARANG');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)

						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'QTY PENJUALAN');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),

					)
				);			
			  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'HARGA BELI');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'TOTAL (RP BELI)');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)

				);

        $i=2;
	      $total=0;
	      $totalbeli=0;
			  $cell='B';
        foreach($isi as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
		      array(
			      'font' => array(
				      'name'	=> 'Arial',

				      'bold'  => false,
				      'italic'=> false,
				      'size'  => 10
			      )
		      ),

		      'A'.$i.':E'.$i
		      );
		      
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_product, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->e_product_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->n_deliver, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->v_product_mill, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->total_harga_beli, Cell_DataType::TYPE_NUMERIC);

			    $total=$total + $row->n_deliver;
			    $totalbeli=$totalbeli + $row->total_harga_beli;
			    $i++;
          }

			  $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $nama='Export Laporan Penjualan (Harga Beli) '.$iperiode.'.xls';
        if(file_exists('excel/'.$nama)){
          @chmod('excel/'.$nama, 0777);
          @unlink('excel/'.$nama);
        }
			  $objWriter->save('excel/00/'.$nama);
			  $sess=$this->session->userdata('session_id');

			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){

					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{

				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){

				  $now	  = $row['c'];
			  }
			  $pesan='Export Laporan Penjualan (Harga Beli) '.$iperiode;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['sukses']			= true;
			  $data['inomor']			= $pesan;
			  $this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
  		}
    }
  }
}
?>
