<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu339')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exportpembelianperiodeitem');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('exportpembelianperiodeitem/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu339')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exportpembelianperiodeitem/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
#      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }

      $a=substr($iperiode,0,4);
		$b=substr($iperiode,4,2);
		$peri=mbulan($b)." - ".$a;

#			if($iarea=='') $iarea=$this->uri->segment(5);
      $this->db->select(" b.f_supplier_pkp, a.d_dtap, a.i_dtap, a.d_due_date, c.i_do, c.d_do, a.d_pajak, 
                          a.i_supplier, b.e_supplier_name, c.i_product, c.e_product_name, c.n_jumlah, c.v_pabrik as harga, a.n_discount, 
                          round (c.n_jumlah * c.v_pabrik ) as v_gross,
                          round ((c.n_jumlah * c.v_pabrik )* a.n_discount) as v_discount
                          from tm_dtap a, tr_supplier b, tm_dtap_item c
                          where a.i_supplier = b.i_supplier and a.i_dtap = c.i_dtap and a.i_supplier = c.i_supplier and a.i_area = c.i_area
                          and to_char(a.d_dtap,'yyyymm') = '$iperiode'",false);
		  
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar Pembelian Per item")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(28);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(28);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(16);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(16);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar Pembelian Per item');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,11,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Bulan : '.$peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,11,3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:P5'
				);
				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Tgl DTAP');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);				
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'No DTAP');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tgl Jatuh Tempo');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'No DO');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			    $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Tgl DO');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Tgl Pajak');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			    $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Kd Supplier');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			    $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Nama Supplier');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Kd Produk');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Nm Produk');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Jumlah');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Diskon');
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Nilai Kotor');
				$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O5', 'Nilai Diskon');
				$objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P5', 'PPN');
				$objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$i=6;
				$j=6;
        		$no=0;
				foreach($query->result() as $row)
				{
          		$no++;
          		$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  	array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':P'.$i
				  );         
          $tmp=explode("-",$row->d_dtap);
          $hr=$tmp[2];
          $bl=$tmp[1];
          $th=$tmp[0];
          $row->d_dtap=$hr.'-'.$bl.'-'.$th;
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->d_dtap, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_dtap, Cell_DataType::TYPE_STRING);
          $tmp=explode("-",$row->d_due_date);
          $hr=$tmp[2];
          $bl=$tmp[1];
          $th=$tmp[0];
          $row->d_due_date=$hr.'-'.$bl.'-'.$th;
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->d_due_date, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_do, Cell_DataType::TYPE_STRING);
          $tmp=explode("-",$row->d_do);
          $hr=$tmp[2];
          $bl=$tmp[1];
          $th=$tmp[0];
          $row->d_do=$hr.'-'.$bl.'-'.$th;
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->d_do, Cell_DataType::TYPE_STRING);
          $tmp=explode("-",$row->d_pajak);
          $hr=$tmp[2];
          $bl=$tmp[1];
          $th=$tmp[0];
          $row->d_pajak=$hr.'-'.$bl.'-'.$th;
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->d_pajak, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->i_supplier, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->i_product, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->e_product_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->n_jumlah, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->harga, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->n_discount, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->v_gross, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $row->v_discount, Cell_DataType::TYPE_NUMERIC);

          if($row->f_supplier_pkp=='f'){
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, 0, Cell_DataType::TYPE_NUMERIC);          
          }else{
          $ppn = round((($row->v_gross - $row->v_discount)/1.1)*0.1);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $ppn, Cell_DataType::TYPE_NUMERIC);
          }
					$i++;
					$j++;
				}
				$x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      	$nama='Pembelian Per item'.$iperiode.'.xls';
			$objWriter->save('beli/'.$nama); 

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
      }
      $pesan='Export Pembelian item:'.$iperiode;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
