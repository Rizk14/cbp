<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');

			$icustomer			= $this->input->post('icustomer', TRUE);
			$icustomerplugroup		= $this->input->post('icustomerplugroup', TRUE);
			$icity				= $this->input->post('icity', TRUE);
			$icustomergroup			= $this->input->post('icustomergroup', TRUE);
//			$ipricegroup			= $this->input->post('ipricegroup', TRUE);
			$ipricegroup			= $this->input->post('nline', TRUE);
			$iarea				= $this->input->post('iarea', TRUE);
			$icustomerstatus		= $this->input->post('icustomerstatus', TRUE);
			$icustomerproducttype		= $this->input->post('icustomerproducttype', TRUE);
			$icustomerspecialproduct	= $this->input->post('icustomerspecialproduct', TRUE);
			$icustomergrade			= $this->input->post('icustomergrade', TRUE);
			$icustomerservice		= $this->input->post('icustomerservice', TRUE);
			$icustomersalestype		= $this->input->post('icustomersalestype', TRUE);
			$icustomerclass			= $this->input->post('icustomerclass', TRUE);
			$icustomerstatus		= $this->input->post('icustomerstatus', TRUE);
			$ecustomername			= $this->input->post('ecustomername', TRUE);
			$ecustomeraddress		= $this->input->post('ecustomeraddress', TRUE);
			$ecityname			= $this->input->post('ecityname', TRUE);
			$ecustomerpostal		= $this->input->post('ecustomerpostal', TRUE);
			$ecustomerphone			= $this->input->post('ecustomerphone', TRUE);
			$ecustomerfax			= $this->input->post('ecustomerfax', TRUE);
			$ecustomermail			= $this->input->post('ecustomermail', TRUE);
			$ecustomersendaddress		= $this->input->post('ecustomersendaddress', TRUE);
			$ecustomerreceiptaddress	= $this->input->post('ecustomerreceiptaddress', TRUE);
			$ecustomerremark		= $this->input->post('ecustomerremark', TRUE);
			$ecustomerpayment		= $this->input->post('ecustomerpayment', TRUE);
			$ecustomerpriority		= $this->input->post('ecustomerpriority', TRUE);
			$ecustomercontact		= $this->input->post('ecustomercontact', TRUE);
			$ecustomercontactgrade		= $this->input->post('ecustomercontactgrade', TRUE);
			$ecustomerrefference		= $this->input->post('ecustomerrefference', TRUE);
			$ecustomerothersupplier		= $this->input->post('ecustomerothersupplier', TRUE);
			$fcustomerplusppn		= $this->input->post('fcustomerplusppn', TRUE);
			$fcustomerplusdiscount		= $this->input->post('fcustomerplusdiscount', TRUE);
			$fcustomerpkp			= $this->input->post('fcustomerpkp', TRUE);
			$fcustomeraktif			= $this->input->post('fcustomeraktif', TRUE);
			$fcustomertax			= $this->input->post('fcustomertax', TRUE);
			$fcustomercicil			= $this->input->post('fcustomercicil', TRUE);
			$ecustomerretensi		= $this->input->post('ecustomerretensi', TRUE);
			$ncustomertoplength		= $this->input->post('ncustomertoplength', TRUE);
			if($ncustomertoplength=='')
				$ncustomertoplength=0;
			if ((isset($icustomer) && $icustomer != '') && (isset($ecustomername) && $ecustomername != ''))
			{
				$this->load->model('listcustomer/mmaster');
				$this->mmaster->insert
					(
					$icustomer, $icustomerplugroup, $icity, $icustomergroup, $ipricegroup,
					$iarea, $icustomerstatus, $icustomerproducttype, $icustomerspecialproduct, 
					$icustomergrade, $icustomerservice, $icustomersalestype, $icustomerclass, 
					$icustomerstatus, $ecustomername, $ecustomeraddress,$ecityname, 
					$ecustomerpostal, $ecustomerphone, $ecustomerfax, $ecustomermail, 
					$ecustomersendaddress, $ecustomerreceiptaddress, $ecustomerremark, 
					$ecustomerpayment, $ecustomerpriority, $ecustomercontact, 
					$ecustomercontactgrade, $ecustomerrefference, $ecustomerothersupplier, 
					$fcustomerplusppn, $fcustomerplusdiscount, $fcustomerpkp,$fcustomeraktif, 
					$fcustomertax, $ecustomerretensi, $ncustomertoplength,$fcustomercicil
					);
			}
			else
			{
        $area1	= $this->session->userdata('i_area');
			  $area2	= $this->session->userdata('i_area2');
			  $area3	= $this->session->userdata('i_area3');
			  $area4	= $this->session->userdata('i_area4');
			  $area5	= $this->session->userdata('i_area5');
			  $cari=strtoupper($this->input->post("cari",false));
			  $config['base_url'] = base_url().'index.php/listcustomer/cform/index/';
        if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		      $query=$this->db->query("select * from tr_customer 
                                   where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%')", false);
        }else{
          $query=$this->db->query("select * from tr_customer 
                                   where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') 
                                   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                                   or i_area = '$area4' or i_area = '$area5')", false);
        }
				$config['total_rows'] = $query->num_rows();
#				$config['total_rows'] = $this->db->count_all('tr_customer');
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('master_customer');
				$data['icustomer']='';
				$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);
				$this->load->model('listcustomer/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4),$area1,$area2,$area3,$area4,$area5);

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
	      	$now	  = $row['c'];
			  }
			  $pesan='Membuka Data Customer KodeLang '.$icustomer;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );  

				$this->load->view('listcustomer/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customer');
			$this->load->view('listcustomer/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customer')." update";
			if($this->uri->segment(4)){
				$icustomer = $this->uri->segment(4);
				$data['icustomer'] = $icustomer;
				$this->load->model('listcustomer/mmaster');
				$data['isi']=$this->mmaster->baca($icustomer);
		 		$this->load->view('listcustomer/vmainform',$data);
			}else{
				$this->load->view('listcustomer/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer			= $this->input->post('icustomer', TRUE);
			$icustomerplugroup		= $this->input->post('icustomerplugroup', TRUE);
			$icity				= $this->input->post('icity', TRUE);
			$icustomergroup			= $this->input->post('icustomergroup', TRUE);
//			$ipricegroup			= $this->input->post('ipricegroup', TRUE);
			$ipricegroup			= $this->input->post('nline', TRUE);
			$iarea				= $this->input->post('iarea', TRUE);
			$icustomerstatus		= $this->input->post('icustomerstatus', TRUE);
			$icustomerproducttype		= $this->input->post('icustomerproducttype', TRUE);
			$icustomerspecialproduct	= $this->input->post('icustomerspecialproduct', TRUE);
			$icustomergrade			= $this->input->post('icustomergrade', TRUE);
			$icustomerservice		= $this->input->post('icustomerservice', TRUE);
			$icustomersalestype		= $this->input->post('icustomersalestype', TRUE);
			$icustomerclass			= $this->input->post('icustomerclass', TRUE);
			$icustomerstatus		= $this->input->post('icustomerstatus', TRUE);
			$ecustomername			= $this->input->post('ecustomername', TRUE);
			$ecustomeraddress		= $this->input->post('ecustomeraddress', TRUE);
			$ecityname			= $this->input->post('ecityname', TRUE);
			$ecustomerpostal		= $this->input->post('ecustomerpostal', TRUE);
			$ecustomerphone			= $this->input->post('ecustomerphone', TRUE);
			$ecustomerfax			= $this->input->post('ecustomerfax', TRUE);
			$ecustomermail			= $this->input->post('ecustomermail', TRUE);
			$ecustomersendaddress		= $this->input->post('ecustomersendaddress', TRUE);
			$ecustomerreceiptaddress	= $this->input->post('ecustomerreceiptaddress', TRUE);
			$ecustomerremark		= $this->input->post('ecustomerremark', TRUE);
			$ecustomerpayment		= $this->input->post('ecustomerpayment', TRUE);
			$ecustomerpriority		= $this->input->post('ecustomerpriority', TRUE);
			$ecustomercontact		= $this->input->post('ecustomercontact', TRUE);
			$ecustomercontactgrade		= $this->input->post('ecustomercontactgrade', TRUE);
			$ecustomerrefference		= $this->input->post('ecustomerrefference', TRUE);
			$ecustomerothersupplier		= $this->input->post('ecustomerothersupplier', TRUE);
			$fcustomerplusppn		= $this->input->post('fcustomerplusppn', TRUE);
			$fcustomerplusdiscount		= $this->input->post('fcustomerplusdiscount', TRUE);
			$fcustomerpkp			= $this->input->post('fcustomerpkp', TRUE);
			$fcustomeraktif			= $this->input->post('fcustomeraktif', TRUE);
			$fcustomertax			= $this->input->post('fcustomertax', TRUE);
			$fcustomercicil			= $this->input->post('fcustomercicil', TRUE);
			$ncustomerretensi		= $this->input->post('ncustomerretensi', TRUE);
			$ncustomertoplength		= $this->input->post('ncustomertoplength', TRUE);
			if($ncustomertoplength=='')
				$ncustomertoplength=0;
			$this->load->model('listcustomer/mmaster');

			$this->mmaster->update(
					$icustomer, $icustomerplugroup, $icity, $icustomergroup, $ipricegroup,
					$iarea, $icustomerstatus, $icustomerproducttype, $icustomerspecialproduct, 
					$icustomergrade, $icustomerservice, $icustomersalestype, $icustomerclass, 
					$icustomerstatus, $ecustomername, $ecustomeraddress,$ecityname, 
					$ecustomerpostal, $ecustomerphone, $ecustomerfax, $ecustomermail, 
					$ecustomersendaddress, $ecustomerreceiptaddress, $ecustomerremark, 
					$ecustomerpayment, $ecustomerpriority, $ecustomercontact, 
					$ecustomercontactgrade, $ecustomerrefference, $ecustomerothersupplier, 
					$fcustomerplusppn, $fcustomerplusdiscount, $fcustomerpkp,$fcustomeraktif, 
					$fcustomertax, $ncustomerretensi, $ncustomertoplength, $fcustomercicil
					      );

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Update Customer KodeLang '.$icustomer;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer	= $this->uri->segment(4);
			$this->load->model('listcustomer/mmaster');
			$this->mmaster->delete($icustomer);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus Customer KodeLang '.$icustomer;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$config['base_url'] = base_url().'index.php/listcustomer/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_customer');
			$data['icustomer']='';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('listcustomer/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari=strtoupper($this->input->post("cari",false));
			$config['base_url'] = base_url().'index.php/listcustomer/cform/index/';
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		    $query=$this->db->query("select * from tr_customer 
                                 where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%')", false);
      }else{
        $query=$this->db->query("select * from tr_customer 
                                 where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') 
                                 and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                                 or i_area = '$area4' or i_area = '$area5')", false);
      }
#			$query=$this->db->query("select * from tr_customer where i_customer like '%$cari%' or e_customer_name like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 	
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$data['page_title'] = $this->lang->line('master_customer');
			$data['icustomer']='';
	 		$this->load->view('listcustomer/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function plugroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/plugroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_plugroup');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_plugroup');
			$data['isi']=$this->mmaster->bacaplugroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistplugroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariplugroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/plugroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_plugroup where upper(i_customer_plugroup) like '%$cari%' 
										or upper(e_customer_plugroupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_plugroup');
			$data['isi']=$this->mmaster->cariplugroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistplugroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function city()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/listcustomer/cform/city/'.$iarea.'/index/';
//			$config['total_rows'] = $this->db->count_all('tr_city');
			$query = $this->db->query("	select * from tr_city where (upper(i_city) like '%$cari%' 
							or upper(e_city_name) like '%$cari%') and i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->bacacity($iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('listcustomer/vlistcity', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricity()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea= $this->input->post('iarea', FALSE);
			$config['base_url'] = base_url().'index.php/listcustomer/cform/city/'.$iarea.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
//			$query = $this->db->query("select * from tr_city where upper(i_city) like '%cari%' or upper(e_city_name) like '%cari%'",false);
			$query = $this->db->query("	select * from tr_city where (upper(i_city) like '%$cari%' 
							or upper(e_city_name) like '%$cari%') and i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->caricity($cari,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('listcustomer/vlistcity', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customergroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_group');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customergroup');
			$data['isi']=$this->mmaster->bacacustomergroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomergroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customergroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_group where upper(i_customer_group) like '%cari%' 
										or upper(e_customer_groupname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customergroup');
			$data['isi']=$this->mmaster->caricustomergroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomergroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function pricegroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/pricegroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_price_group');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_pricegroup');
			$data['isi']=$this->mmaster->bacapricegroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistpricegroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caripricegroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/pricegroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_price_group where upper(i_price_group) like '%cari%' 
										or upper(e_price_groupname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_pricegroup');
			$data['isi']=$this->mmaster->caripricegroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistpricegroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/area/index/';
			$config['total_rows'] = $this->db->count_all('tr_area');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/area/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select * from tr_area where upper(i_area) like '%cari%' or upper(e_area_name) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerstatus()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customerstatus/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_status');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerstatus');
			$data['isi']=$this->mmaster->bacacustomerstatus($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomerstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerstatus()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customerstatus/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_status where upper(i_customer_status) like '%cari%' 
										or upper(e_customer_statusname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerstatus');
			$data['isi']=$this->mmaster->caricustomerstatus($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomerstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerproducttype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customerproducttype/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_producttype');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerproducttype');
			$data['isi']=$this->mmaster->bacacustomerproducttype($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomerproducttype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerproducttype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customerproducttype/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_producttype where upper(i_customer_producttype) like '%cari%' 
										or upper(e_customer_producttypename) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerproducttype');
			$data['isi']=$this->mmaster->caricustomerproducttype($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomerproducttype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerspecialproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerproducttype = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customerspecialproduct/index/';
			$this->db->where("i_customer_producttype='$icustomerproducttype'");
			$config['total_rows'] = $this->db->count_all('tr_customer_specialproduct');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerspecialproduct');
			$data['isi']=$this->mmaster->bacacustomerspecialproduct($icustomerproducttype,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomerspecialproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerspecialproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customerspecialproduct/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_specialproduct 
										where upper(i_customer_specialproduct) like '%cari%' 
										or upper(e_customer_specialproductname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerspecialproduct');
			$data['isi']=$this->mmaster->caricustomerspecialproduct($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomerspecialproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customergrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customergrade/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_grade');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customergrade');
			$data['isi']=$this->mmaster->bacacustomergrade($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomergrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomergrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customergrade/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_grade 
										where upper(i_customer_grade) like '%cari%' 
										or upper(e_customer_gradename) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customergrade');
			$data['isi']=$this->mmaster->caricustomergrade($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomergrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerservice()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customerservice/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_service');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerservice');
			$data['isi']=$this->mmaster->bacacustomerservice($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomerservice', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerservice()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customerservice/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_customer_service where upper(i_customer_service) like '%cari%' 
										or upper(e_customer_servicename) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerservice');
			$data['isi']=$this->mmaster->caricustomerservice($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomerservice', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customersalestype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customersalestype/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_salestype');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customersalestype');
			$data['isi']=$this->mmaster->bacacustomersalestype($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomersalestype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomersalestype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customersalestype/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_salestype where upper(i_customer_salestype) like '%cari%' 
										or upper(e_customer_salestypename) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customersalestype');
			$data['isi']=$this->mmaster->caricustomersalestype($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomersalestype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerclass()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customerclass/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_class');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerclass');
			$data['isi']=$this->mmaster->bacacustomerclass($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomerclass', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerclass()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu254')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomer/cform/customerclass/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_class where upper(i_customer_class) like '%cari%' 
										or upper(e_customer_classname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_customerclass');
			$data['isi']=$this->mmaster->caricustomerclass($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomer/vlistcustomerclass', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
