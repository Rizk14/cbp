<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index(){
	  if (
			  (($this->session->userdata('logged_in')) &&
			  ($this->session->userdata('menu505')=='t')) ||
			  (($this->session->userdata('logged_in')) &&
			  ($this->session->userdata('allmenu')=='t'))
		  ){
          $data['dfrom']='';
          $data['dto']='';
          $data['page_title'] = $this->lang->line('listsjblmreceive');		    
          $this->load->view('listsjblmreceive/vmainform', $data);
		  }else{
  			$this->load->view('awal/index.php');
		  }
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu505')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea		= $this->input->post('iarea', TRUE);
			$dfrom		= $this->input->post('dfrom', TRUE);
			$dto  	 	= $this->input->post('dto', TRUE);

      if($dfrom){
			  $tmp=explode("-",$dfrom);
			  $th=$tmp[2];
			  $bl=$tmp[1];
			  $hr=$tmp[0];
			  $dfrom=$th."-".$bl."-".$hr;
			}
			if($dto){
			  $tmp=explode("-",$dto);
			  $th=$tmp[2];
			  $bl=$tmp[1];
			  $hr=$tmp[0];
			  $dto=$th."-".$bl."-".$hr;
			}

      if($dfrom=='')$dfrom = $this->uri->segment(4);
      if($dto=='')$dto = $this->uri->segment(5);
      if($iarea=='')$iarea = $this->uri->segment(6);
#  		echo $dfrom.'    '.$dto.'   '.$iarea;die;
			$cari = strtoupper($this->input->post('cari', FALSE));
			$iuser	= $this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/listsjblmreceive/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_nota a, tr_area b
                                  where a.i_area=b.i_area and a.d_sj_receive is null and a.f_nota_cancel='f' 
                                  and (upper(a.i_sj) like '%$cari%') and a.d_sj>='$dfrom' and a.d_sj<='$dto'
                                  and a.i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjblmreceive');
			$this->load->model('listsjblmreceive/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']  = $dfrom;
			$data['dto']   = $dto;
			$data['iarea']  = $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iuser,$config['per_page'],$this->uri->segment(7),$cari,$dfrom,$dto,$iarea);
			$this->load->view('listsjblmreceive/vformview', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu505')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjprec');
			$this->load->view('listsjblmreceive/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$data['page_title'] = $this->lang->line('sjblmreceive');
			if($this->uri->segment(4)!=''){
				$dfrom  = $this->uri->segment(4);
				$dto	  = $this->uri->segment(5);
				$iarea  = $this->uri->segment(6);
				$isj 	  = $this->uri->segment(7);
				$ispb	  = $this->uri->segment(8);
        $iareasj= substr($isj,8,2);
				$data['dfrom']  = $dfrom;
				$data['dto']	  = $dto;
				$data['isj'] 	  = $isj;
				$data['iarea']	= $iarea;
 		 		$data['iareasj']= substr($isj,8,2);
				$query 	= $this->db->query("select i_sj from tm_nota_item where i_sj = '$isj'");
				$data['jmlitem'] = $query->num_rows(); 				
			  $query=$this->db->query("	select f_spb_consigment
                                  from tm_spb where i_spb='$ispb' and i_area='$iarea' ",false);
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
            $fspbconsigment=$row->f_spb_consigment;
				  }
			  }
				$this->load->model('listsjblmreceive/mmaster');
				$data['isi']=$this->mmaster->baca($isj,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isj,$iarea);
				$this->db->select("	a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
									          where a.i_sj ='$isj' and a.i_customer=c.i_customer
									          and a.i_area=b.i_area", false);
				$query = $this->db->get();
				if ($query->num_rows() > 0){
					foreach($query->result() as $row){
						$data['dsj']=$row->d_sj;
						$data['ispb']=$row->i_spb;
						$data['dspb']=$row->d_spb;
#						if($iareasj=='00'){
#							$data['istore']='AA';
#						}else{
#							$data['istore']=$iareasj;
#						}
            if($iareasj=='BK')$iareasj=$iarea;
            $que = $this->db->query("select i_store from tr_area where i_area='$iareasj'");
            $st=$que->row();
            $data['istore']=$st->i_store;
						$data['isjold']=$row->i_sj_old;
						$data['eareaname']=$row->e_area_name;
						$data['vsjgross']=$row->v_nota_gross;
						$data['nsjdiscount1']=$row->n_nota_discount1;
						$data['nsjdiscount2']=$row->n_nota_discount2;
						$data['nsjdiscount3']=$row->n_nota_discount3;
						$data['vsjdiscount1']=$row->v_nota_discount1;
						$data['vsjdiscount2']=$row->v_nota_discount2;
						$data['vsjdiscount3']=$row->v_nota_discount3;
						$data['vsjdiscounttotal']=$row->v_nota_discounttotal;
						$data['vsjnetto']=$row->v_nota_netto;
						$data['icustomer']=$row->i_customer;
						$data['ecustomername']=$row->e_customer_name;
						$data['isalesman']=$row->i_salesman;
            $data['fplusppn']=$row->f_plus_ppn;
            $data['ntop']=$row->n_nota_toplength;
            $data['fspbconsigment']=$fspbconsigment;
        #$data['dsj']        =date('Y-m-d');
        #$data['tglakhir']='';
					}
				}
		 		$this->load->view('listsjblmreceive/vformupdate',$data);
			}else{
				$this->load->view('listsjblmreceive/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
  function update(){
    if (
       (($this->session->userdata('logged_in')) &&
       ($this->session->userdata('menu505')=='t')) ||
       (($this->session->userdata('logged_in')) &&
       ($this->session->userdata('allmenu')=='t'))
       ){
			    $isj        = $this->input->post('isj', TRUE);
    			$iarea		  = $this->input->post('iarea', TRUE);
			    $dlistsjblmreceive = $this->input->post('dlistsjblmreceive', TRUE);
    			$eremark	  = $this->input->post('eremark', TRUE);

          $this->load->model('listsjblmreceive/mmaster');
          $data['page_title'] = $this->lang->line('listsjblmreceive');
          $this->mmaster->updatesj($isj,$iarea,$dlistsjblmreceive,$eremark);

          $sess=$this->session->userdata('session_id');
          $id=$this->session->userdata('user_id');
          $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
          $rs      = pg_query($sql);
            if(pg_num_rows($rs)>0){
              while($row=pg_fetch_assoc($rs)){
                $ip_address   = $row['ip_address'];
                break;
              }
            }else{
              $ip_address='kosong';
            }
          $query   = pg_query("SELECT current_timestamp as c");
            while($row=pg_fetch_assoc($query)){
            $now    = $row['c'];
          }
           $sess=$this->session->userdata('session_id');
           $id=$this->session->userdata('user_id');
           $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
           $rs      = pg_query($sql);
           if(pg_num_rows($rs)>0){
              while($row=pg_fetch_assoc($rs)){
                 $ip_address   = $row['ip_address'];
                 break;
              }
           }else{
              $ip_address='kosong';
           }
           $query   = pg_query("SELECT current_timestamp as c");
           while($row=pg_fetch_assoc($query)){
              $now    = $row['c'];
           }
           $pesan='Receive SJ '.$iarea.' No:'.$isj;
           $this->load->model('logger');
           $this->logger->write($id, $ip_address, $now , $pesan );
           $data['sukses']         = true;
           $data['inomor']         = $isj;
           $this->load->view('nomor',$data);
           /*
		        $data['status'] = 'view';
		        $data['iarea'] = $iarea;
		        $data['iperiodeawal'] = $iperiodeawal;
		        $data['iperiodeakhir'] = $iperiodeakhir;
		        $data['isi']		= $this->mmaster->bacaperiode($iperiodeawal,$iperiodeakhir,$iarea);
	          $this->load->view('listaccplafond/vmainform', $data);*/
          }else{
            $this->load->view('awal/index.php');
          }
        }
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu505')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listsjblmreceive/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listsjblmreceive/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listsjblmreceive/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu505')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listsjblmreceive/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listsjblmreceive/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listsjblmreceive/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu505')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$iuser	= $this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/listsjblmreceive/cform/view/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_nota a, tr_area b
                                  where a.i_area=b.i_area and a.d_sj_receive is null
                                  and a.f_nota_cancel='f' and (upper(a.i_sj) like '%$cari%')
                                  and (a.i_area in (select i_area from tm_user_area where i_user='$iuser'))",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjblmreceive');
			$this->load->model('listsjblmreceive/mmaster');
			$data['cari']		= $cari;
			$data['isi']		= $this->mmaster->bacaperiode($iuser,$config['per_page'],$this->uri->segment(4),$cari);
			$this->load->view('listsjblmreceive/vformview', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
