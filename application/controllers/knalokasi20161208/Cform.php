<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationxx');
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('knalokasi');
         $data['dfrom']='';
         $data['dto']  ='';
         $data['ialokasi']  ='';
         $data['ikn']  ='';
         $data['iarea']='';
         $data['isi']  ='';

         $this->load->view('knalokasi/vmainform', $data);

      }elseif($this->session->userdata('logged_in')){
         $this->load->view('errorauthority');
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('listspb');
         $this->load->view('knalokasi/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function delete()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb = $this->input->post('ispbdelete', TRUE);
         $this->load->model('knalokasi/mmaster');
         $this->mmaster->delete($ispb);
         $data['page_title'] = $this->lang->line('listspb');
         $data['ispb']='';
         $data['isi']=$this->mmaster->bacasemua();
         $this->load->view('knalokasi/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cari()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $config['base_url'] = base_url().'index.php/knalokasi/cform/index/';
         $config['per_page'] = '10';
         $limo=$config['per_page'];
         $ofso=$this->uri->segment(4);
         if($ofso=='')
            $ofso=0;
         $query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
                              where a.i_customer=b.i_customer
                              and a.f_lunas = 'f'
                              and (upper(a.i_customer) like '%$cari%'
                                or upper(b.e_customer_name) like '%$cari%'
                                or upper(a.i_spb) like '%$cari%')",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(4);
         $this->pagination->initialize($config);
         $this->load->model('knalokasi/mmaster');
         $data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
         $data['page_title'] = $this->lang->line('knalokasi');
         $data['ispb']='';
         $data['ittb']='';
         $data['inota']='';
         $this->load->view('knalokasi/vmainform',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function approve()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('knalokasi');
         if(
            ($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
           )
         {
            $ikn        = str_replace('tandagaring','/',$this->uri->segment(4));
            $iarea      = $this->uri->segment(5);
            $eareaname= $this->uri->segment(6);
            $eareaname= str_replace('%20', ' ',$eareaname);
            $vsisa      = $this->uri->segment(7);
            $dfrom      = $this->uri->segment(8);
            $dto     = $this->uri->segment(9);
            $dkn     = $this->uri->segment(10);
            $data['jmlitem'] = 0;
            $data['ialokasi'] = '';
            $data['ikn'] = $ikn;
            $data['iarea']=$iarea;
            $data['eareaname']=$eareaname;
            $data['vsisa']=$vsisa;
            $data['dfrom']=$dfrom;
            $data['dto']=$dto;
            $data['dkn']=$dkn;
            $data['icustomer']= $this->uri->segment(11);
            $data['ecustomername']= $this->uri->segment(12);
            $data['ecustomeraddress']= $this->uri->segment(13);
            $data['ecustomercity']= $this->uri->segment(14);
            $data['ecustomername']= str_replace('%20',' ',$data['ecustomername']);
            $data['ecustomername']= str_replace('tandakoma',',',$data['ecustomername']);
            $data['ecustomername']= str_replace('tandakutipsatu',"'",$data['ecustomername']);
            $data['ecustomername']= str_replace('tandadan','&',$data['ecustomername']);
            $data['ecustomername']= str_replace('tandatitikkoma',';',$data['ecustomername']);
            $data['ecustomername']= str_replace('tandatitik','.',$data['ecustomername']);
            $data['ecustomername']= str_replace('tandakurungbuka','(',$data['ecustomername']);
            $data['ecustomername']= str_replace('tandakurungtutup',')',$data['ecustomername']);
            $data['ecustomername']= str_replace('tandagaring','/',$data['ecustomername']);
            $data['ecustomercity']= str_replace('%20',' ',$data['ecustomercity']);
            $data['ecustomercity']= str_replace('tandakoma',',',$data['ecustomercity']);
            $data['ecustomercity']= str_replace('tandakutipsatu',"'",$data['ecustomercity']);
            $data['ecustomercity']= str_replace('tandatitikkoma',';',$data['ecustomercity']);
            $data['ecustomercity']= str_replace('tandatitik','.',$data['ecustomercity']);
            $data['ecustomercity']= str_replace('tandakurungbuka','(',$data['ecustomercity']);
            $data['ecustomercity']= str_replace('tandakurungtutup',')',$data['ecustomercity']);
            $data['ecustomercity']= str_replace('tandagaring','/',$data['ecustomercity']);
            $data['ecustomeraddress']= str_replace('%20',' ',$data['ecustomeraddress']);
            $data['ecustomeraddress']= str_replace('tandakoma',',',$data['ecustomeraddress']);
            $data['ecustomeraddress']= str_replace('tandakutipsatu',"'",$data['ecustomeraddress']);
            $data['ecustomeraddress']= str_replace('tandatitikkoma',';',$data['ecustomeraddress']);
            $data['ecustomeraddress']= str_replace('tandatitik','.',$data['ecustomeraddress']);
            $data['ecustomeraddress']= str_replace('tandakurungbuka','(',$data['ecustomeraddress']);
            $data['ecustomeraddress']= str_replace('tandakurungtutup',')',$data['ecustomeraddress']);
            $data['ecustomeraddress']= str_replace('tandagaring','/',$data['ecustomeraddress']);
            $this->load->model('knalokasi/mmaster');
            $data['isi']='';
            $data['detail']='';
            $data['tgl']=date('d-m-Y');
            $this->load->view('knalokasi/vmainform',$data);
         }else{
            $this->load->view('knalokasi/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function update()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('knalokasi/mmaster');
         $ikn  = $this->input->post('ikn', TRUE);
         $dkn  = $this->input->post('dkn', TRUE);
         if($dkn!=''){
            $tmp=explode("-",$dkn);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dkn=$th."-".$bl."-".$hr;
         }
         $dalokasi  = $this->input->post('dalokasi', TRUE);
         if($dalokasi!=''){
            $tmp=explode("-",$dalokasi);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dalokasi=$th."-".$bl."-".$hr;
            $thbl=$th.$bl;
            $iperiode=$th.$bl;
         }
         $icustomer     = $this->input->post('icustomer', TRUE);
         $ecustomername = $this->input->post('ecustomername', TRUE);
         $ecustomeraddress= $this->input->post('ecustomeraddress', TRUE);
         $ecustomercity = $this->input->post('ecustomercity', TRUE);
         $iarea         = $this->input->post('iarea', TRUE);
         $eareaname     = $this->input->post('eareaname', TRUE);
         $ijenisbayar   = $this->input->post('ijenisbayar',TRUE);
         $ejenisbayarname= $this->input->post('ejenisbayarname',TRUE);
         $vjumlah    = $this->input->post('vjumlah',TRUE);
         $vjumlah    = str_replace(',','',$vjumlah);
         $vlebih     = $this->input->post('vlebih',TRUE);
         $vlebih        = str_replace(',','',$vlebih);
         $jml          = $this->input->post('jml', TRUE);
         $ada=false;
         if(($dkn!='') && ($dalokasi!='') && ($ikn!='') && ($vjumlah!='') && ($vjumlah!='0') && ($jml!='0') && ($ijenisbayar!='') && ($icustomer!='') ){
        for($i=1;$i<=$jml;$i++){
          $vjumla = $this->input->post('vjumlah'.$i, TRUE);
          $vsisa  = $this->input->post('vsisa'.$i, TRUE);
          $vjumla = str_replace(',','',$vjumla);
          $vsisa  = str_replace(',','',$vsisa);
          $vsisa  = $vsisa-$vjumla;
        }
        if(!$ada){
          $vjumlah=0;
          $this->db->trans_begin();
          $inotax = $this->input->post('inota1', TRUE);
          $ialokasi=$this->mmaster->runningnumberpl($iarea,$thbl);

########## Posting ###########
            $egirodescription="Alokasi Kredit Nota no:".$ikn;
			      $fclose			= 'f';
			      $jml			= $this->input->post('jml', TRUE);
			      for($i=1;$i<=$jml;$i++)
			      {
			        $inota=$this->input->post('inota'.$i, TRUE);
			        $ireff=$ialokasi.'|'.$inota;
              if($i==1){
			          $this->mmaster->inserttransheader($ireff,$iarea,$egirodescription,$fclose,$dalokasi);
#			          $this->mmaster->updatepelunasan($ialokasi,$iarea,$ikn);
              }
        			$vjuml		= $this->input->post('vjumlah'.$i, TRUE);
				      $vjuml		= str_replace(',','',$vjuml);
				      $vjumlah=$vjumlah+$vjuml;
				      if($ijenisbayar=='06'){
         				$accdebet		= ByPromosi;
				      }elseif($ijenisbayar=='07'){
         				$accdebet		= ByExpedisi;
				      }elseif($ijenisbayar=='08'){
         				$accdebet		= ByAdmBank;
				      }elseif($ijenisbayar=='09'){
         				$accdebet		= ByAdmPenjualan;
				      }elseif($ijenisbayar=='10'){
         				$accdebet		= ByPembulatan;
         			}elseif($ijenisbayar=='11'){
         				$accdebet		= ByLainlain;
				      }elseif($ijenisbayar=='12'){
         				$accdebet		= ByJasaPromosi;
         		  }elseif($ijenisbayar=='13'){
         				$accdebet		= ByAlatPromosi;
         		  }elseif($ijenisbayar=='14'){
         				$accdebet		= ByDiskonPromo;
         		  }
#       				$accdebet		= PiutangDagangSementara;
				      $namadebet	= $this->mmaster->namaacc($accdebet);
				      $tmp			  = $this->mmaster->carisaldo($accdebet,$iperiode);
				      if($tmp) 
					      $vsaldoaw1		= $tmp->v_saldo_awal;
				      else 
					      $vsaldoaw1		= 0;
				      if($tmp) 
					      $vmutasidebet1	= $tmp->v_mutasi_debet;
				      else
					      $vmutasidebet1	= 0;
				      if($tmp) 
					      $vmutasikredit1	= $tmp->v_mutasi_kredit;
				      else
					      $vmutasikredit1	= 0;
				      if($tmp) 
					      $vsaldoak1		= $tmp->v_saldo_akhir;
				      else
					      $vsaldoak1		= 0;
				
				      $acckredit		= PiutangDagang.$iarea;
				      $namakredit		= $this->mmaster->namaacc($acckredit);
				      $saldoawkredit	= $this->mmaster->carisaldo($acckredit,$iperiode);
				      if($tmp) 
					      $vsaldoaw2		= $tmp->v_saldo_awal;
				      else
					      $vsaldoaw2		= 0;
				      if($tmp) 
					      $vmutasidebet2	= $tmp->v_mutasi_debet;
				      else
					      $vmutasidebet2	= 0;
				      if($tmp) 
					      $vmutasikredit2	= $tmp->v_mutasi_kredit;
				      else
					      $vmutasikredit2	= 0;
				      if($tmp) 
					      $vsaldoak2		= $tmp->v_saldo_akhir;
				      else
					      $vsaldoak2		= 0;
				      $this->mmaster->inserttransitemdebet($accdebet,$ireff,$namadebet,'t','t',$iarea,$egirodescription,$vjuml,$dalokasi);
				      $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vjuml);
				      $this->mmaster->inserttransitemkredit($acckredit,$ireff,$namakredit,'f','t',$iarea,$egirodescription,$vjuml,$dalokasi);
				      $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vjuml);
				      $this->mmaster->insertgldebet($accdebet,$ireff,$namadebet,'t',$iarea,$vjuml,$dalokasi,$egirodescription);
				      $this->mmaster->insertglkredit($acckredit,$ireff,$namakredit,'f',$iarea,$vjuml,$dalokasi,$egirodescription);
			      }
########## End of Posting ###########

          $this->mmaster->insertheader($ialokasi,$ikn,$iarea,$ijenisbayar,$icustomer,$dkn,$dalokasi,$vjumlah,$vlebih);
          $asal=0;
          $pengurang=$vjumlah;#-$vlebih;
          $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
            $group='xxx';
            foreach($query->result() as $row){
               $group=$row->i_customer_groupbayar;
            }
            $this->mmaster->updatesaldo($group,$icustomer,$pengurang);
            $this->mmaster->updatekn($group,$iarea,$ikn,$pengurang,$asal);
            for($i=1;$i<=$jml;$i++){
              $inota              = $this->input->post('inota'.$i, TRUE);
              $dnota              = $this->input->post('dnota'.$i, TRUE);
              if($dnota!=''){
                 $tmp=explode("-",$dnota);
                 $th=$tmp[2];
                 $bl=$tmp[1];
                 $hr=$tmp[0];
                 $dnota=$th."-".$bl."-".$hr;
              }
              $vjumlah= $this->input->post('vjumlah'.$i, TRUE);
              $vsisa  = $this->input->post('vsisa'.$i, TRUE);
              $vsiso  = $this->input->post('vsisa'.$i, TRUE);
              $vjumlah= str_replace(',','',$vjumlah);
              $vsisa = str_replace(',','',$vsisa);
              $vsiso = str_replace(',','',$vsiso);
              $vsiso  = $vsiso-$vjumlah;
              $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
              $eremark= $this->input->post('eremark'.$i,TRUE);
              $this->mmaster->insertdetail($ialokasi,$ikn,$iarea,$inota,$dnota,$dkn,$vjumlah,$vsisa,$i,$ipelunasanremark,$eremark);
              $this->mmaster->updatenota($inota,$vjumlah);
            }
          if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Input knalokasi No:'.$ialokasi.' Area:'.$iarea;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );
                 $this->db->trans_commit();
#                 $this->db->trans_rollback();
                 $data['sukses'] = true;
                 $data['inomor'] = $ialokasi;
                 $this->load->view('nomor',$data);
              }
        }
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function edit()
   {
      if ( (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $area1   = $this->session->userdata('i_area');
         $data['page_title'] = $this->lang->line('knalokasi');
         if(
            ($this->uri->segment(4)) && ($this->uri->segment(5))
           )
         {
            $ialokasi = $this->uri->segment(4);
            $iarea= $this->uri->segment(5);
            $ikn  = $this->uri->segment(6);
            $dfrom= $this->uri->segment(7);
            $dto  = $this->uri->segment(8);
            $this->load->model('knalokasi/mmaster');
            $query   = $this->db->query("select * from tm_alokasikn_item
                                                      where i_alokasi = '$ialokasi' and i_area = '$iarea' and i_kn='$ikn' ");
            $data['jmlitem']     = $query->num_rows();
            $data['vsisa']=$this->mmaster->sisa($iarea,$ialokasi,$ikn);
            $data['isi']=$this->mmaster->bacapl($iarea,$ialokasi,$ikn);
            $data['detail']=$this->mmaster->bacadetailpl($iarea,$ialokasi,$ikn);
            $data['ialokasi']      = $ialokasi;
            $data['iarea'] = $iarea;
            $data['ikn']      = $ikn;
            $data['dfrom']  = $dfrom;
            $data['dto']     = $dto;
            $data['area1']  = $area1;
            $this->load->view('knalokasi/vmainform',$data);
         }else{
            $this->load->view('knalokasi/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function updateknalokasi()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('knalokasi/mmaster');
         $ipl  = $this->input->post('ialokasi', TRUE);
         $idt  = $this->input->post('idt', TRUE);
         $ddt  = $this->input->post('ddt', TRUE);
         if($ddt!=''){
            $tmp=explode("-",$ddt);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $ddt=$th."-".$bl."-".$hr;
         }
      $dbukti  = $this->input->post('dbukti', TRUE);
         if($dbukti!=''){
            $tmp=explode("-",$dbukti);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dbukti=$th."-".$bl."-".$hr;
         }
         $djt  = $this->input->post('djt', TRUE);
         if($djt!=''){
            $tmp=explode("-",$djt);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $djt=$th."-".$bl."-".$hr;
         }
         $dcair   = $this->input->post('dcair', TRUE);
         if($dcair!=''){
            $tmp=explode("-",$dcair);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dcair=$th."-".$bl."-".$hr;
         }
         $dgiro   = $this->input->post('dgiro', TRUE);
         if($dgiro!=''){
            $tmp=explode("-",$dgiro);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dgiro=$th."-".$bl."-".$hr;
         }
         $icustomer     = $this->input->post('icustomer', TRUE);
         $ecustomername = $this->input->post('ecustomername', TRUE);
         $ecustomeraddress= $this->input->post('ecustomeraddress', TRUE);
         $ecustomercity = $this->input->post('ecustomercity', TRUE);
         $iarea         = $this->input->post('iarea', TRUE);
         $eareaname     = $this->input->post('eareaname', TRUE);
         $ijenisbayar   = $this->input->post('ijenisbayar',TRUE);
         $ejenisbayarname= $this->input->post('ejenisbayarname',TRUE);
         $egirobank     = $this->input->post('egirobank',TRUE);
         if(($egirobank==Null) && ($ijenisbayar!='05')){
            $egirobank  = '-';
            $igiro      = $this->input->post('igiro',TRUE);
         }else if(($egirobank==Null) && ($ijenisbayar=='05')){
            $egirobank  = $this->input->post('igiro',TRUE);
            $igiro      ='';
         }else if(($egirobank!=Null) && ($ijenisbayar=='01')){
            $igiro      = $this->input->post('igiro',TRUE);
         }else if(($egirobank!=Null) && ($ijenisbayar=='03')){
            $nkuyear = $this->input->post('nkuyear',TRUE);
            $igiro      = $this->input->post('igiro',TRUE);
         }else if($ijenisbayar=='04'){
            $igiro      = $this->input->post('igiro',TRUE);
         }else{
            $igiro      ='';
         }
         $vjumlah    = $this->input->post('vjumlah',TRUE);
         $vjumlah    = str_replace(',','',$vjumlah);
#        $vlebih        = $this->input->post('vlebih',TRUE);
#      if($ijenisbayar=='04'||$ijenisbayar=='05'){
#           $vlebih        = '0';
#      }else{
         $vlebih        = $this->input->post('vlebih',TRUE);
#      }
         $vlebih        = str_replace(',','',$vlebih);
#      $ipelunasanremark = $this->input->post('ipelunsanremark',TRUE);
#      $eremark    = $this->input->post('eremark',TRUE);
         $jml        = $this->input->post('jml', TRUE);
      $ada=false;
         if(($ddt!='') && ($dbukti!='') && ($idt!='') && ($vjumlah!='') && ($vjumlah!='0') && ($jml!='0') && ($ijenisbayar!='') && ($icustomer!='') ){
        for($i=1;$i<=$jml;$i++){
          $vjumla = $this->input->post('vjumlah'.$i, TRUE);
             $vsisa  = $this->input->post('vsisa'.$i, TRUE);
             $vjumla = str_replace(',','',$vjumla);
             $vsisa  = str_replace(',','',$vsisa);
             $vsisa  = $vsisa-$vjumla;
          $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
#          if( ($vsisa-$vjumlah>0) && ($ipelunasanremark=='') ){
          if( ($vsisa>0) && ($ipelunasanremark=='') ){
            $ada=true;
          }
          if($ada) break;
        }
        if(!$ada){
              $this->db->trans_begin();
              $asalkn   = $this->mmaster->jmlasalkn($ipl,$idt,$iarea,$ddt);
              foreach($asalkn as $asl){
                 $jmlpl = $asl->v_jumlah;
                 $lbhpl = $asl->v_lebih;
              }
              $asal  = $jmlpl-$lbhpl;
              $this->mmaster->deleteheader(  $ipl,$idt,$iarea,$ddt);
              $this->mmaster->insertheader(  $ipl,$idt,$iarea,$ijenisbayar,$igiro,$icustomer,$dgiro,$ddt,$dbukti,$dcair,$egirobank,
                                      $vjumlah,$vlebih);
              $pengurang=$vjumlah-$vlebih;
          $igiro=trim($igiro);

          $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
            $group='xxx';
            foreach($query->result() as $row){
               $group=$row->i_customer_groupbayar;
            }

              if($ijenisbayar=='01'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updategiro($group,$iarea,$igiro,$pengurang,$asal);
              }elseif($ijenisbayar=='03'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updateku($group,$iarea,$igiro,$pengurang,$asal,$nkuyear);
              }elseif($ijenisbayar=='04'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updatekn($group,$iarea,$igiro,$pengurang,$asal);
              }elseif($ijenisbayar=='05'){
            $this->mmaster->updatelebihbayar($group,$iarea,$egirobank,$pengurang,$asal);
          }
              for($i=1;$i<=$jml;$i++){
                $inota              = $this->input->post('inota'.$i, TRUE);
                $dnota              = $this->input->post('dnota'.$i, TRUE);
                if($dnota!=''){
                   $tmp=explode("-",$dnota);
                   $th=$tmp[2];
                   $bl=$tmp[1];
                   $hr=$tmp[0];
                   $dnota=$th."-".$bl."-".$hr;
              }
  /*
  a=giro/cek -- 01
  b=tunai -- 02
  c=transfer/ku -- 03
  d=kn -- 04
  e=lebih -- 05
  */
                $vjumlah   = $this->input->post('vjumlah'.$i, TRUE);
                $vasal  = $this->input->post('vasal'.$i, TRUE);
                $vasol  = $this->input->post('vasal'.$i, TRUE);
                if($vasal==''){
                  $vasal   = $this->input->post('vsisa'.$i, TRUE);
                  $vasol   = $this->input->post('vsisa'.$i, TRUE);
                }
                $vjumlah= str_replace(',','',$vjumlah);
                 $vasal = str_replace(',','',$vasal);
                 $vasol = str_replace(',','',$vasol);
#               $vsisa  = $vasal-$vjumlah;
                $vsisa  = $vasal;
                $vsiso  = $vasal-$vjumlah;
            $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
            $eremark    = $this->input->post('eremark'.$i,TRUE);

                $this->mmaster->deletedetail(   $ipl,$idt,$iarea,$inota,$ddt);
                $this->mmaster->insertdetail(   $ipl,$idt,$iarea,$inota,$dnota,$ddt,$vjumlah,$vsisa,$i,$ipelunasanremark,$eremark);
#           $this->mmaster->updatedt($idt,$iarea,$ddt,$inota,$vsisa);
#               $this->mmaster->updatenota($inota,$vsisa);
#               $this->mmaster->updatenota($inota,$vsiso);
                $this->mmaster->updatenota($inota,$vjumlah);
              }
#             $nilai=$this->mmaster->hitungsisadt($idt,$iarea,$ddt);
#             if($nilai==0){
#                $this->mmaster->updatestatusdt($idt,$iarea,$ddt);
#             }
              if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                 $this->db->trans_commit();
  #               $this->db->trans_rollback();

                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Update knalokasi No:'.$ipl.' Area:'.$iarea;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );

                 $data['sukses'] = true;
                 $data['inomor'] = $ipl;
                 $this->load->view('nomor',$data);
              }
        }
         }

      }else{
         $this->load->view('awal/index.php');
      }
   }
   function customer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ikn  = str_replace('tandagaring','/',$this->uri->segment(4));
         $iarea   = $this->uri->segment(5);
         $config['base_url'] = base_url().'index.php/knalokasi/cform/customer/'.$ikn.'/'.$iarea.'/index/';
         $config['per_page'] = '10';
         $query = $this->db->query("   select a.i_customer from tm_kn a, tr_customer b
                              where a.i_customer=b.i_customer and a.v_sisa>0 and a.i_kn = '$ikn' and a.i_area = '$iarea'",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);

         $this->load->model('knalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->bacacustomer($ikn,$iarea,$config['per_page'],$this->uri->segment(7));
         $data['ikn']=$ikn;
         $data['iarea']=$iarea;
         $this->load->view('knalokasi/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function caricustomer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ikn = $this->uri->segment(4);
         $iarea = $this->uri->segment(5);
         $config['base_url'] = base_url().'index.php/knalokasi/cform/customer/'.$ikn.'/'.$iarea.'/';
         $cari    = strtoupper($this->input->post('cari', FALSE));
         $query   = $this->db->query("select a.i_customer from tm_kn a, tr_customer b
                                           where a.i_customer=b.i_customer
                                           and a.v_sisa>0
                                           and a.i_kn = '$ikn' and a.i_area = '$iarea'
                                           and (upper(b.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%') ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('knalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->caricustomer($cari,$idt,$iarea,$config['per_page'],$this->uri->segment(6));
         $data['ikn']=$ikn;
         $data['iarea']=$iarea;
         $this->load->view('knalokasi/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function girocek()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/knalokasi/cform/girocek/index/';
         $config['per_page'] = '10';
         if($this->session->userdata('i_area')=='00' || $this->session->userdata('i_area')=='PB'){
           $query = $this->db->query("select * from tr_jenis_bayar where i_jenis_bayar > '05'",false);
         }else{
           $query = $this->db->query("select * from tr_jenis_bayar where i_jenis_bayar > '05'",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('knalokasi/mmaster');
         $data['area']=$this->session->userdata('i_area');
         $data['page_title'] = $this->lang->line('list_girocek');
         $data['isi']=$this->mmaster->bacagirocek($config['per_page'],$this->uri->segment(5),$this->session->userdata('i_area'));
         $this->load->view('knalokasi/vlistgirocek', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function nota()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari             = strtoupper($this->input->post('cari', FALSE));
         $baris            = $this->input->post('baris', FALSE);
         $iarea            = $this->input->post('iarea', FALSE);
         $icustomer        = $this->input->post('icustomer', FALSE);
         if($baris=='') $baris=$this->uri->segment(4);
         if($iarea=='') $iarea=$this->uri->segment(5);
         if($icustomer=='') $icustomer=$this->uri->segment(6);
         $data['baris']    = $baris;
         $data['iarea']    = $iarea;
         $data['icustomer']= $icustomer;
         if($this->uri->segment(7)!='sikasep'){
           if($cari=='') $cari=$this->uri->segment(7);
           $config['base_url'] = base_url().'index.php/knalokasi/cform/nota/'.$baris.'/'.$iarea.'/'.$icustomer.'/'.$cari.'/';
         }else{
           $config['base_url'] = base_url().'index.php/knalokasi/cform/nota/'.$baris.'/'.$iarea.'/'.$icustomer.'/sikasep/';
         }
         $data['cari']    = $cari;
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupbayar;
         }
         $query = $this->db->query(" select c.i_nota
                                     from tr_customer_groupbayar b, tm_nota c
                                     where b.i_customer_groupbayar='$group' and c.i_customer=b.i_customer and c.v_sisa>0
                                     and (upper(c.i_nota) like '%$cari%')" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']     = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $this->load->model('knalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($iarea,$icustomer,$config['per_page'],$this->uri->segment(8),$group,$cari);
         $this->load->view('knalokasi/vlistnota', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function notaupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['baris']    = $this->uri->segment(4);
         $data['iarea']    = $this->uri->segment(5);
         $data['idt']      = $this->uri->segment(6);
         $data['icustomer']   = $this->uri->segment(7);
         //$data['dku']    = $this->uri->segment(8);
         $baris            = $this->uri->segment(4);
         $iarea            = $this->uri->segment(5);
         $idt           = $this->uri->segment(6);
         $icustomer        = $this->uri->segment(7);
         //$dku            = $this->uri->segment(8);
         $config['base_url'] = base_url().'index.php/knalokasi/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
      }
         $query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer_groupar b
                                            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                            and a.i_dt='$idt'
                                            and a.i_area='$iarea'" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         //$config['cur_page']   = $this->uri->segment(10);
         $config['cur_page']  = $this->uri->segment(9);
         $this->pagination->initialize($config);
         $this->load->model('knalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
         $this->load->view('knalokasi/vlistnotaupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carinotaupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari             = strtoupper($this->input->post('cari', FALSE));
         $data['baris']    = $this->input->post('baris', FALSE);
         $data['iarea']    = $this->input->post('iarea', FALSE);
         $data['idt']      = $this->input->post('idt', FALSE);
         $data['icustomer']   = $this->input->post('icustomer', FALSE);
         //$data['dku']    = $this->uri->segment(8);
         $baris            = $this->input->post('baris', FALSE);
         $iarea            = $this->input->post('iarea', FALSE);
         $idt           = $this->input->post('idt', FALSE);
         $icustomer        = $this->input->post('icustomer', FALSE);
         //$dku            = $this->uri->segment(8);
         $config['base_url'] = base_url().'index.php/knalokasi/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
      }
         $query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer_groupar b
                                            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                            and a.i_dt='$idt'
                                            and a.i_area='$iarea'" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         //$config['cur_page']   = $this->uri->segment(10);
         $config['cur_page']  = $this->uri->segment(9);
         $this->pagination->initialize($config);
         $this->load->model('knalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
         $this->load->view('knalokasi/vlistnotaupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/knalokasi/cform/area/index/';
         $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('knalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
         $this->load->view('knalokasi/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/knalokasi/cform/area/index/';
         $iuser   	= $this->session->userdata('user_id');
         $cari    	= $this->input->post('cari', FALSE);
         $cari 		  = strtoupper($cari);
         $query   	= $this->db->query("select * from tr_area
                                  where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                  and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('knalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
         $this->load->view('knalokasi/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function view()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $cari    = strtoupper($this->input->post('cari'));
         $dfrom      = $this->input->post('dfrom');
         $dto     = $this->input->post('dto');
         $iarea      = $this->input->post('iarea');
         if($dfrom=='') $dfrom=$this->uri->segment(4);
         if($dto=='') $dto=$this->uri->segment(5);
         if($iarea=='') $iarea   = $this->uri->segment(6);
         $config['base_url'] = base_url().'index.php/knalokasi/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';

         $query = $this->db->query("select a.i_kn from tm_kn a
                                    where a.i_area='$iarea' and a.v_sisa>0 and a.f_kn_cancel='f'
                                    and a.d_kn >= to_date('$dfrom','dd-mm-yyyy')
                                    and a.d_kn <= to_date('$dto','dd-mm-yyyy')
                                    and (upper(a.i_kn) like '%$cari%') and a.i_kn_type='02'",false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(8);
         $this->paginationxx->initialize($config);
         $data['page_title'] = $this->lang->line('knalokasi');
         $this->load->model('knalokasi/mmaster');
         $data['cari']  = $cari;
         $data['dfrom'] = $dfrom;
         $data['dto']   = $dto;
         $data['iarea'] = $iarea;
         $data['isi']   = $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
         $data['ikn']   = '';
         $data['ialokasi']   = '';
         $data['c']     = $dfrom.'/'.$dto.'/'.$iarea.'/'.$this->uri->segment(8);
         $this->load->view('knalokasi/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function kn()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = $this->uri->segment(4);
         $iarea      = $this->uri->segment(5);
         $dbukti     = $this->uri->segment(6);
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/knalokasi/cform/kn/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
        $group='xxx';
        foreach($query->result() as $row){
          $group=$row->i_customer_groupbayar;
        }
         $query = $this->db->query("select a.* from tm_kn a, tr_customer_groupbayar b
                                    where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                                    and a.v_sisa>0 and d_kn<='$xdbukti' and a.f_kn_cancel='f' and a.i_kn_type='02'",false);

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $this->load->model('knalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_kn');
         $data['isi']=$this->mmaster->bacakn($icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('knalokasi/vlistkn', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carikn()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = strtoupper($this->input->post('icustomer', FALSE));
         $iarea      = strtoupper($this->input->post('iarea', FALSE));
         $dbukti     = strtoupper($this->input->post('dbukti', FALSE));
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/knalokasi/cform/carikn/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
        $group='xxx';
        foreach($query->result() as $row){
          $group=$row->i_customer_groupar;
        }
         $query   = $this->db->query("select a.* from tm_kn a, tr_customer_groupar b
                                      where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                      and a.v_sisa>0 and (upper(i_kn) like '%$cari%')
                                      and d_kn<='$xdbukti' and a.f_kn_cancel='f' and a.i_kn_type='02' ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $this->load->model('knalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_kn');
         $data['isi']=$this->mmaster->carikn($cari,$icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('knalokasi/vlistkn', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function remark()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $baris = $this->uri->segment(4);
         $config['base_url'] = base_url().'index.php/knalokasi/cform/remark/'.$baris.'/';
         $config['per_page'] = '10';
         $query = $this->db->query("   select i_pelunasan_remark from tr_pelunasan_remark",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('knalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_pelunasan_remark');
         $data['isi']=$this->mmaster->bacaremark($config['per_page'],$this->uri->segment(5));
      $data['baris']=$baris;
         $this->load->view('knalokasi/vlistremark', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
