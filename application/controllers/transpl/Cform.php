<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu243')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transpl');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('transpl/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu243')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			if($iarea=='') $iarea=$this->uri->segment(5);
			$data['page_title'] = $this->lang->line('transpl');
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;

      define ('BOOLEAN_FIELD',   'L');
      define ('CHARACTER_FIELD', 'C');
      define ('DATE_FIELD',      'D');
      define ('NUMBER_FIELD',    'N');
      define ('READ_ONLY',  '0');
      define ('WRITE_ONLY', '1');
      define ('READ_WRITE', '2');
      $db_file = 'excel/'.$iarea.'/byr'.$iarea.'.dbf';
      if(file_exists($db_file)){
        @chmod($db_file, 0777);
        @unlink($db_file);
      }
      $dbase_definition = array (
         array ('CEK',  CHARACTER_FIELD,  1),
         array ('NOBUKTIBR',  CHARACTER_FIELD,  11),
         array ('NOBUKTI',  CHARACTER_FIELD,  11),
         array ('NOGIRO',  CHARACTER_FIELD,  10),
         array ('GIRO',  CHARACTER_FIELD,  10),
         array ('NMBANK',  CHARACTER_FIELD,  15),
         array ('TGLGIRO',  DATE_FIELD),
         array ('JUMLAH',  NUMBER_FIELD,  10,0),
         array ('KODELANG',  CHARACTER_FIELD,  5),
         array ('TGLBUKTI', DATE_FIELD),
         array ('TGLCAIR',  DATE_FIELD),  
         array ('NODOK',  CHARACTER_FIELD,  7),  
         array ('TGLDOK',  DATE_FIELD),  
         array ('NILAI', NUMBER_FIELD, 10, 0),
         array ('JUMBYR', NUMBER_FIELD, 10, 0),
         array ('SISA', NUMBER_FIELD, 10, 0),
         array ('LEBIH', NUMBER_FIELD, 10, 0),
         array ('KET', CHARACTER_FIELD, 1),
         array ('BATAL', BOOLEAN_FIELD),
         array ('TGLBUAT', DATE_FIELD),
         array ('JAMBUAT', CHARACTER_FIELD, 8),
         array ('TGLUBAH', DATE_FIELD),
         array ('JAMUBAH', CHARACTER_FIELD, 8),
         array ('TGLTRANSF', DATE_FIELD),
         array ('JAMTRANSF', DATE_FIELD)
      );
/*
  kn sama tunai tgl(giro & cair) semua sama dengan tanggal bukti
  giro tanggal cair sama dengan tanggal giro (klo belum cair)
*/
      $create = @ dbase_create($db_file, $dbase_definition)
                or die ("Could not create dbf file <i>$db_file</i>.");
      $id = @ dbase_open ($db_file, READ_WRITE)
            or die ("Could not open dbf file <i>$db_file</i>."); 
      $sql	  = " select a.i_pelunasan, a.i_dt, a.i_area, a.i_jenis_bayar, a.i_giro, a.i_customer, a.d_giro, 
                  a.d_bukti, a.d_cair,a.e_bank_name, a.v_jumlah, a.v_lebih, a.f_posting, a.f_close, a.f_pelunasan_cancel, 
                  a.i_cek, a.d_cek, a.e_cek,a.i_cek_ikhp, a.d_cek_ikhp, a.d_dt, b.i_nota, b.d_nota, b.v_jumlah as v_jumlah_item, 
                  b.v_sisa, c.e_jenis_bayarname, d.v_nota_netto
                  from tm_pelunasan a, tm_pelunasan_item b, tr_jenis_bayar c, tm_nota d
                  where a.i_pelunasan=b.i_pelunasan and a.i_area=b.i_area and a.i_dt=b.i_dt 
                  and to_char(a.d_bukti,'yyyymm')='$iperiode' and a.i_area='$iarea' and a.f_pelunasan_cancel='0' 
                  and a.f_giro_tolak='0' and a.f_giro_batal='0'
                  and a.i_jenis_bayar=c.i_jenis_bayar and d.i_nota=b.i_nota 
                  order by a.i_pelunasan, b.n_item_no";
#and d.i_area=b.i_area
      $rspb		= $this->db->query($sql);
      foreach($rspb->result() as $rowpb){
        $cek              = '';
	      $nobukti          = $rowpb->i_pelunasan;
        $jenis            = $rowpb->i_jenis_bayar;#substr($rowpb->i_pelunasan,10,1);
        switch ($jenis) {
        case '01':
            $jenis='G';
            break;
        case '02':
            $jenis='A';
            break;
        case '03':
            $jenis='B';
            break;
        case '04':
            $jenis='C';
            break;
        case '05':
            $jenis='D';
            break;
        }
	      $nobuktidbase     = str_replace('-','/',substr($rowpb->i_pelunasan,0,10)).$jenis;
        if( (trim($rowpb->i_giro)=='') || ($rowpb->i_giro==null) ){
  	      $nogiro         = $rowpb->i_jenis_bayar.$rowpb->e_jenis_bayarname;
        }else{
  	      $nogiro         = $rowpb->i_jenis_bayar.$rowpb->i_giro;
        }
        $giro             = '';
        if($jenis=='D'){
          switch (substr($rowpb->e_bank_name,10,1)) {
            case 'A':
              $jenislama='A';
              break;
            case 'B':
              $jenislama='G';
              break;
            case 'C':
              $jenislama='B';
              break;
            case 'D':
              $jenislama='C';
              break;
            case 'E':
              $jenislama='D';
              break;
          }
  	      $rowpb->e_bank_name = str_replace('-','/',substr($rowpb->e_bank_name,0,10)).$jenislama;
        }
        $nmbank           = $rowpb->e_bank_name;
        if($rowpb->i_jenis_bayar=='02' || $rowpb->i_jenis_bayar=='04'){
          $tglgiro        = substr($rowpb->d_bukti,0,4).substr($rowpb->d_bukti,5,2).substr($rowpb->d_bukti,8,2);
        }else{
          $tglgiro        = substr($rowpb->d_giro,0,4).substr($rowpb->d_giro,5,2).substr($rowpb->d_giro,8,2);
        }
        $jumlah           = $rowpb->v_jumlah;
        $kodelang         = $rowpb->i_customer;
        $tglbukti         = substr($rowpb->d_bukti,0,4).substr($rowpb->d_bukti,5,2).substr($rowpb->d_bukti,8,2);
        if($rowpb->i_jenis_bayar=='02' || $rowpb->i_jenis_bayar=='04'){
          $tglcair        = substr($rowpb->d_bukti,0,4).substr($rowpb->d_bukti,5,2).substr($rowpb->d_bukti,8,2);
        }elseif($rowpb->d_cair=='' || $rowpb->d_cair==null){
          $tglcair        = substr($rowpb->d_giro,0,4).substr($rowpb->d_giro,5,2).substr($rowpb->d_giro,8,2);
        }else{
          $tglcair        = substr($rowpb->d_cair,0,4).substr($rowpb->d_cair,5,2).substr($rowpb->d_cair,8,2);
        }
        $nodok            = substr($rowpb->i_nota,8,7);
        $tgldok           = substr($rowpb->d_nota,0,4).substr($rowpb->d_nota,5,2).substr($rowpb->d_nota,8,2);
        $nilai            = $rowpb->v_nota_netto;
        $jumbyr           = $rowpb->v_jumlah_item;
        $sisa             = $rowpb->v_sisa;
        $lebih            = $rowpb->v_lebih;
        $ket              = '';
        if($rowpb->f_pelunasan_cancel=='f') 
          $batal='F'; 
        else 
          $batal='T';
        $tglbuat          = '';
        $jambuat          = '';
        $tglubah          = '';
        $jamubah          = '';
        $tgltransf        = '';
        $jamtransf        = '';

        $isi = array ($cek,$nobukti,$nobuktidbase,$nogiro,$giro,$nmbank,$tglgiro,$jumlah,$kodelang,$tglbukti,$tglcair,$nodok,$tgldok,
                      $nilai,$jumbyr,$sisa,$lebih,$ket,$batal,$tglbuat,$jambuat,$tglubah,$jamubah,$tgltransf,$jamtransf);
        dbase_add_record ($id, $isi) or die ("Data TIDAK bisa ditambahkan ke file <i>$db_file</i>."); 
      }
      dbase_close($id);
      @chmod($db_file, 0777);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Transfer Pelunasan ke file BYR lama Area '.$iarea.' Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses']			= true;
			$data['inomor']			= $pesan;
			$this->load->view('nomor',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu243')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transpl/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transpl/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('transpl/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu243')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/transpl/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transpl/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('transpl/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
