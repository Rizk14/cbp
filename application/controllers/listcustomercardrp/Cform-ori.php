<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listcustomercard');
#			$data['iperiode']='';
			$data['dfrom']='';
			$data['dto']='';
			$data['iarea']='';
			$this->load->view('listcustomercard/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto  		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($iarea=='') $iarea	= $this->uri->segment(4);
			if($dfrom=='') $dfrom	= $this->uri->segment(5);
			if($dto=='') $dto	= $this->uri->segment(6);
			$this->load->model('listcustomercard/mmaster');
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;

			$interval	= $this->mmaster->interval($dfrom,$dto);
      $data['interval'] = $interval;
			$data['isi']		  = $this->mmaster->bacaperiode($dfrom,$dto,$iarea,$interval);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Customer Card Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listcustomercard/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('listcustomercard/mmaster');
			$iarea  	= $this->uri->segment(4);
			$dfrom    = $this->uri->segment(5);
			$dto      = $this->uri->segment(6);

			$this->load->model('listcustomercard/mmaster');
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      if($dfrom!=''){
		    $tmp=explode("-",$dfrom);
		    $blasal=$tmp[1];
        settype($bl,'integer');
	    }
      $bl=$blasal;
      
			$interval	= $this->mmaster->interval($dfrom,$dto);
      $data['interval'] = $interval;
			$isi		  = $this->mmaster->bacaperiode($dfrom,$dto,$iarea,$interval);
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Customer Card")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      if(count($isi)>0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(24);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(11);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:M1'
				);
        for($i=1;$i<=$interval;$i++){
          switch ($bl){
          case '1' : $bln='Jan';
            break;
          case '2' :$bln='Feb';
            break;
          case '3' :$bln='Mar';
            break;
          case '4' :$bln='Apr';
            break;
          case '5' :$bln='Mei';
            break;
          case '6' :$bln='Jun';
            break;
          case '7' :$bln='Jul';
            break;
          case '8' :$bln='Agu';
            break;
          case '9' :$bln='Sep';
            break;
          case '10' :$bln='Okt';
            break;
          case '11' :$bln='Nov';
            break;
          case '12' :$bln='Des';
            break;
          }
          $bl++;
          if($bl==13)$bl=1;
        }
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Area');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,0,2);
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'K-LANG');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(1,1,1,2);
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Kota/Kab');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(2,1,2,2);
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Salesman');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3,1,3,2);
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Jenis');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4,1,4,2);
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Nama Lang');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5,1,5,2);
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Alamat');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6,1,6,2);				
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Kode');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7,1,7,2);
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Product');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8,1,8,2);
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Supplier');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(9,1,9,2);
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			$objPHPExcel->getActiveSheet()->setCellValue('K1', 'Nota');
			$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
  		$objPHPExcel->getActiveSheet()->setCellValue('K2',$bln);
  		$objPHPExcel->getActiveSheet()->getStyle('K2')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Total Nota');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(11,1,11,2);
				$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			$objPHPExcel->getActiveSheet()->setCellValue('M1', 'Nilai');
      $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(12,1,12,2);
				$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        $total=0;
        $subtot=0;
        $vsubtot=0;
        $grandtot=0;
        $icity='';
        $kode='';
        $totkota=0;
        $vtotkota=0;
        $totarea=0;
        $vtotarea=0;
        $grandtotkota=0;
        $vgrandtotkota=0;	
				$i=3;
        foreach($isi as $row){
        $m=$i+4;
        $total=0;
        if($icity=='' || ($icity==$row->icity && $kode==substr($row->kode,0,2)) ){
        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			  array(
				  'font' => array(
					  'name'	=> 'Arial',
					  'bold'  => false,
					  'italic'=> false,
					  'size'  => 10
				  )
			  ),
			  'A'.$i.':M'.$i
			  );
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, substr($row->kode,0,2)."-".$row->area, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->kode, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->kota, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->sales, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->jenis, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->nama, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->alamat, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->product, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->productname, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->supplier, Cell_DataType::TYPE_STRING);
        $bl=$blasal;     
        for($n=1;$n<=$interval;$n++){   
          switch ($bl){
            case '1' :
              $notabln = $row->notajan;
              $total=$total+$row->notajan;
              $subtot=$subtot+$row->notajan;
              $vsubtot=$vsubtot+($row->notajan*$row->harga);
              $grandtot=$grandtot+$row->notajan;+
              $totkota=$totkota+$row->notajan;
              $vtotkota=$vtotkota+($row->notajan*$row->harga);
              $totarea=$totarea+$row->notajan;
              $vtotarea=$vtotarea+($row->notajan*$row->harga);
              break;
            case '2' :
              $total=$total+$row->notafeb;
              $notabln = $row->notafeb;
              $subtot=$subtot+$row->notafeb;
              $vsubtot=$vsubtot+($row->notafeb*$row->harga);
              $grandtot=$grandtot+$row->notafeb;
              $totkota=$totkota+$row->notafeb;
              $vtotkota=$vtotkota+($row->notafeb*$row->harga);
              $totarea=$totarea+$row->notafeb;
              $vtotarea=$vtotarea+($row->notafeb*$row->harga);
              break;
            case '3' :
              $notabln = $row->notamar;
              $total=$total+$row->notamar;
              $subtot=$subtot+$row->notamar;
              $vsubtot=$vsubtot+($row->notamar*$row->harga);
              $grandtot=$grandtot+$row->notamar;
              $totkota=$totkota+$row->notamar;
              $vtotkota=$vtotkota+($row->notamar*$row->harga);
              $totarea=$totarea+$row->notamar;
              $vtotarea=$vtotarea+($row->notamar*$row->harga);
              break;
            case '4' :
              $total=$total+$row->notaapr;
              $notabln = $row->notaapr;
              $subtot=$subtot+$row->notaapr;
              $vsubtot=$vsubtot+($row->notaapr*$row->harga);
              $grandtot=$grandtot+$row->notaapr;
              $totkota=$totkota+$row->notaapr;
              $vtotkota=$vtotkota+($row->notaapr*$row->harga);
              $totarea=$totarea+$row->notaapr;

              $vtotarea=$vtotarea+($row->notaapr*$row->harga);
              break;
            case '5' :
              $notabln = $row->notamay;
              $total=$total+$row->notamay;
              $subtot=$subtot+$row->notamay;
              $vsubtot=$vsubtot+($row->notamay*$row->harga);
              $grandtot=$grandtot+$row->notamay;
              $totkota=$totkota+$row->notamay;
              $vtotkota=$vtotkota+($row->notamay*$row->harga);
              $totarea=$totarea+$row->notamay;
              $vtotarea=$vtotarea+($row->notamay*$row->harga);
              break;
            case '6' :
              $notabln = $row->notajun;
              $total=$total+$row->notajun;
              $subtot=$subtot+$row->notajun;
              $vsubtot=$vsubtot+($row->notajun*$row->harga);
              $grandtot=$grandtot+$row->notajun;
              $totkota=$totkota+$row->notajun;
              $vtotkota=$vtotkota+($row->notajun*$row->harga);
              $totarea=$totarea+$row->notajun;
              $vtotarea=$vtotarea+($row->notajun*$row->harga);
              break;
            case '7' :
              $total=$total+$row->notajul;
              $notabln = $row->notajul;
              $subtot=$subtot+$row->notajul;
              $vsubtot=$vsubtot+($row->notajul*$row->harga);
              $grandtot=$grandtot+$row->notajul;
              $totkota=$totkota+$row->notajul;
              $vtotkota=$vtotkota+($row->notajul*$row->harga);
              $totarea=$totarea+$row->notajul;
              $vtotarea=$vtotarea+($row->notajul*$row->harga);
              break;
            case '8' :
              $total=$total+$row->notaaug;
              $notabln = $row->notaaug;
              $subtot=$subtot+$row->notaaug;
              $vsubtot=$vsubtot+($row->notaaug*$row->harga);
              $grandtot=$grandtot+$row->notaaug;
              $totkota=$totkota+$row->notaaug;
              $vtotkota=$vtotkota+($row->notaaug*$row->harga);
              $totarea=$totarea+$row->notaaug;
              $vtotarea=$vtotarea+($row->notaaug*$row->harga);
              break;
            case '9' :
              $bln='Sep';
              $total=$total+$row->notasep;
              $notabln = $row->notasep;
              $subtot=$subtot+$row->notasep;
              $vsubtot=$vsubtot+($row->notasep*$row->harga);
              $grandtot=$grandtot+$row->notasep;
              $totkota=$totkota+$row->notasep;
              $vtotkota=$vtotkota+($row->notasep*$row->harga);
              $totarea=$totarea+$row->notasep;
              $vtotarea=$vtotarea+($row->notasep*$row->harga);
              break;
            case '10' :
              $total=$total+$row->notaokt;
              $notabln = $row->notaokt;
              $subtot=$subtot+$row->notaokt;
              $vsubtot=$vsubtot+($row->notaokt*$row->harga);
              $grandtot=$grandtot+$row->notaokt;
              $totkota=$totkota+$row->notaokt;
              $vtotkota=$vtotkota+($row->notaokt*$row->harga);
              $totarea=$totarea+$row->notaokt;

              $vtotarea=$vtotarea+($row->notaokt*$row->harga);
              break;
            case '11' :
              $total=$total+$row->notanov;
              $notabln = $row->notanov;
              $subtot=$subtot+$row->notanov;
              $vsubtot=$vsubtot+($row->notanov*$row->harga);
              $totarea=$totarea+$row->notanov;
              $grandtot=$grandtot+$row->notanov;
              $totkota=$totkota+$row->notanov;
              $vtotkota=$vtotkota+($row->notanov*$row->harga);
              $vtotarea=$vtotarea+($row->notanov*$row->harga);
              break;
            case '12' :
              $total=$total+$row->notades;
              $notabln = $row->notades;
              $subtot=$subtot+$row->notades;
              $vsubtot=$vsubtot+($row->notades*$row->harga);
              $grandtot=$grandtot12+$row->notades;
              $totkota=$totkota+$row->notades;
              $vtotkota=$vtotkota+($row->notades*$row->harga);
              $totarea=$totarea+$row->notades;
              $vtotarea=$vtotarea+($row->notades*$row->harga);
              break;
            }
            $bl++;
            if($bl==13)$bl=1;
          }
        
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $notabln, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $total, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $total*$row->harga, Cell_DataType::TYPE_NUMERIC);
        }elseif( $kode!=substr($row->kode,0,2) || ($icity!='' && $icity!=$row->icity)){
				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A'.$i.':M'.$i
				  );

			    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'TOTAL KOTA');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,9,$i);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $subtot, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $totkota, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $vtotkota, Cell_DataType::TYPE_NUMERIC);
          $grandtotkota=$grandtotkota+$totkota;
          $vgrandtotkota=$vgrandtotkota+$vtotkota;
          $subtot=0;
          $totkota=0;
          $vtotkota=0;
          $i++;
          if( $kode!=substr($row->kode,0,2) ){
				      $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				      array(
					      'font' => array(
						      'name'	=> 'Arial',
						      'bold'  => true,
						      'italic'=> false,
						      'size'  => 10
					      ),
					      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => true
					      )
				      ),
				      'A'.$i.':M'.$i
				      );
			        $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'TOTAL AREA');
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,9,$i);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $totarea, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $totarea, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $vtotarea, Cell_DataType::TYPE_NUMERIC);
              $vtotarea=0;
              $totarea=0;
              $grandtotkota=$grandtotkota+$totkota;
              $vgrandtotkota=$vgrandtotkota+$vtotkota;
              $i++;
            }
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			    array(
				    'font' => array(
					    'name'	=> 'Arial',
					    'bold'  => false,
					    'italic'=> false,
					    'size'  => 10
				    )
			    ),
			    'A'.$i.':M'.$i
			    );

          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, substr($row->kode,0,2)."-".$row->area, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->kode, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->kota, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->sales, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->jenis, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->nama, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->alamat, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->product, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->productname, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->supplier, Cell_DataType::TYPE_STRING);
          $bl=$blasal;     
          for($n=1;$n<=$interval;$n++){   
            switch ($bl){
            case '1' :
              $notabln = $row->notajan;
              $total=$total+$row->notajan;
              $subtot=$subtot+$row->notajan;
              $vsubtot=$vsubtot+($row->notajan*$row->harga);
              $grandtot=$grandtot+$row->notajan;
              $totkota=$totkota+$row->notajan;
              $vtotkota=$vtotkota+($row->notajan*$row->harga);
              $totarea=$totarea+$row->notajan;
              $vtotarea=$vtotarea+($row->notajan*$row->harga);
              break;
            case '2' :
              $total=$total+$row->notafeb;
              $notabln = $row->notafeb;
              $subtot=$subtot+$row->notafeb;
              $vsubtot=$vsubtot+($row->notafeb*$row->harga);
              $grandtot=$grandtot+$row->notafeb;
              $totkota=$totkota+$row->notafeb;
              $vtotkota=$vtotkota+($row->notafeb*$row->harga);
              $totarea=$totarea+$row->notafeb;
              $vtotarea=$vtotarea+($row->notafeb*$row->harga);
              break;
            case '3' :
              $notabln = $row->notamar;
              $total=$total+$row->notamar;
              $subtot=$subtot+$row->notamar;
              $vsubtot=$vsubtot+($row->notamar*$row->harga);
              $grandtot=$grandtot+$row->notamar;
              $totkota=$totkota+$row->notamar;
              $vtotkota=$vtotkota+($row->notamar*$row->harga);
              $totarea=$totarea+$row->notamar;
              $vtotarea=$vtotarea+($row->notamar*$row->harga);
              break;
            case '4' :
              $total=$total+$row->notaapr;
              $notabln = $row->notaapr;
              $subtot=$subtot+$row->notaapr;
              $vsubtot=$vsubtot+($row->notaapr*$row->harga);
              $grandtot=$grandtot+$row->notaapr;
              $totkota=$totkota+$row->notaapr;
              $vtotkota=$vtotkota+($row->notaapr*$row->harga);
              $totarea=$totarea+$row->notaapr;
              $vtotarea=$vtotarea+($row->notaapr*$row->harga);
              break;
            case '5' :
              $notabln = $row->notamay;
              $total=$total+$row->notamay;
              $subtot=$subtot+$row->notamay;
              $vsubtot=$vsubtot+($row->notamay*$row->harga);
              $grandtot=$grandtot+$row->notamay;
              $totkota=$totkota+$row->notamay;
              $vtotkota=$vtotkota+($row->notamay*$row->harga);
              $totarea=$totarea+$row->notamay;
              $vtotarea=$vtotarea+($row->notamay*$row->harga);
              break;
            case '6' :
              $notabln = $row->notajun;
              $total=$total+$row->notajun;
              $subtot=$subtot+$row->notajun;
              $vsubtot=$vsubtot+($row->notajun*$row->harga);
              $grandtot=$grandtot+$row->notajun;
              $totkota=$totkota+$row->notajun;
              $vtotkota=$vtotkota+($row->notajun*$row->harga);
              $totarea=$totarea+$row->notajun;
              $vtotarea=$vtotarea+($row->notajun*$row->harga);
              break;
            case '7' :
              $total=$total+$row->notajul;
              $notabln = $row->notajul;
              $subtot=$subtot+$row->notajul;
              $vsubtot=$vsubtot+($row->notajul*$row->harga);
              $grandtot=$grandtot+$row->notajul;
              $totkota=$totkota+$row->notajul;
              $vtotkota=$vtotkota+($row->notajul*$row->harga);
              $totarea=$totarea+$row->notajul;
              $vtotarea=$vtotarea+($row->notajul*$row->harga);
              break;
            case '8' :
              $total=$total+$row->notaaug;
              $notabln = $row->notaaug;
              $subtot=$subtot+$row->notaaug;
              $vsubtot=$vsubtot+($row->notaaug*$row->harga);
              $grandtot=$grandtot+$row->notaaug;
              $totkota=$totkota+$row->notaaug;
              $vtotkota=$vtotkota+($row->notaaug*$row->harga);
              $totarea=$totarea+$row->notaaug;
              $vtotarea=$vtotarea+($row->notaaug*$row->harga);
              break;
            case '9' :
              $bln='Sep';
              $total=$total+$row->notasep;
              $notabln = $row->notasep;
              $subtot=$subtot+$row->notasep;
              $vsubtot=$vsubtot+($row->notasep*$row->harga);
              $grandtot=$grandtot+$row->notasep;
              $totkota=$totkota+$row->notasep;
              $vtotkota=$vtotkota+($row->notasep*$row->harga);
              $totarea=$totarea+$row->notasep;
              $vtotarea=$vtotarea+($row->notasep*$row->harga);
              break;
            case '10' :
              $total=$total+$row->notaokt;
              $notabln = $row->notaokt;
              $subtot=$subtot+$row->notaokt;
              $vsubtot=$vsubtot+($row->notaokt*$row->harga);
              $grandtot=$grandtot+$row->notaokt;
              $totkota=$totkota+$row->notaokt;
              $vtotkota=$vtotkota+($row->notaokt*$row->harga);
              $totarea=$totarea+$row->notaokt;

              $vtotarea=$vtotarea+($row->notaokt*$row->harga);
              break;
            case '11' :
              $total=$total+$row->notanov;
              $notabln = $row->notanov;
              $subtot=$subtot+$row->notanov;
              $vsubtot=$vsubtot+($row->notanov*$row->harga);
              $totarea=$totarea+$row->notanov;
              $grandtot=$grandtot+$row->notanov;
              $totkota=$totkota+$row->notanov;
              $vtotkota=$vtotkota+($row->notanov*$row->harga);
              $vtotarea=$vtotarea+($row->notanov*$row->harga);
              break;
            case '12' :
              $total=$total+$row->notades;
              $notabln = $row->notades;
              $subtot=$subtot+$row->notades;
              $vsubtot=$vsubtot+($row->notades*$row->harga);
              $grandtot=$grandtot12+$row->notades;
              $totkota=$totkota+$row->notades;
              $vtotkota=$vtotkota+($row->notades*$row->harga);
              $totarea=$totarea+$row->notades;
              $vtotarea=$vtotarea+($row->notades*$row->harga);
              break;
            }
            $bl++;
            if($bl==13)$bl=1;
          }
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $notabln, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $total, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $total*$row->harga, Cell_DataType::TYPE_NUMERIC);
        }        
        
        
        
        $i++;
        $icity=$row->icity;
        $kode=substr($row->kode,0,2);
      }
      $x=$i-1;
				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A'.$i.':M'.$i
				  );

			    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'TOTAL KOTA');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,9,$i);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $subtot, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $totkota, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $vtotkota, Cell_DataType::TYPE_NUMERIC);
          $i++;
				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A'.$i.':M'.$i
				  );
          $grandtotkota=$grandtotkota+$totkota;
          $vgrandtotkota=$vgrandtotkota+$vtotkota;  

			    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'TOTAL AREA');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,9,$i);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $totarea, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $totarea, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $vtotarea, Cell_DataType::TYPE_NUMERIC);
          $i++;
				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A'.$i.':M'.$i
				  );
			    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'GRAND TOTAL');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,9,$i);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $grandtot, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $grandtotkota, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $vgrandtotkota, Cell_DataType::TYPE_NUMERIC);
      }
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='Customer Card '.$iarea.'.xls';
      if(file_exists('customer/'.$nama)){
        @chmod('customer/'.$nama, 0777);
        @unlink('customer/'.$nama);
      }
			$objWriter->save('customer/'.$nama); 
      @chmod('customer/'.$nama, 0777);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
		  $data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Customer Card '.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$data['sukses'] = true;
			$data['inomor']	= "Customer Card";
			$this->load->view('nomor',$data);
      echo "<script>this.close();</script>";
		}else{
			$this->load->view('awal/index.php');
		}
  } 
  
  
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listcustomercard');
			$this->load->view('listcustomercard/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dto		= $this->uri->segment(8);
			$dfrom	= $this->uri->segment(7);
			$iarea	= $this->uri->segment(6);
			$ispb		= $this->uri->segment(5);
			$inota	= $this->uri->segment(4);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$this->load->model('listcustomercard/mmaster');
			$this->mmaster->delete($inota,$ispb,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus Penjualan Per Pelanggan dari tanggal:'.$dfrom.' sampai:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$config['base_url'] = base_url().'index.php/listcustomercard/cform/view/'.$inota.'/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
																	where a.i_customer=b.i_customer 
																	and a.f_ttb_tolak='f'
                                  and a.f_nota_koreksi='f'
																	and not a.i_nota isnull
																	and (upper(a.i_nota) like '%$cari%' 
																		or upper(a.i_spb) like '%$cari%' 
																		or upper(a.i_customer) like '%$cari%' 
																		or upper(b.e_customer_name) like '%$cari%')
																	and a.i_area='$iarea' and
																	a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['keyword']	= '';	
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listcustomercard/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($cari=='') $cari	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/listcustomercard/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$cari%' 
										  or upper(a.i_spb) like '%$cari%' 
										  or upper(a.i_customer) like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('listcustomercard/mmaster');
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listcustomercard/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$keyword = strtoupper($this->uri->segment(7));	
			$config['base_url'] = base_url().'index.php/listcustomercard/cform/cariperpages/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$keyword.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'                    
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$keyword%' 
										  or upper(a.i_spb) like '%$keyword%' 
										  or upper(a.i_customer) like '%$keyword%' 
										  or upper(b.e_customer_name) like '%$keyword%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('listcustomercard/mmaster');
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$keyword);
			$this->load->view('listcustomercard/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomercard/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
											or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomercard/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listcustomercard/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listcustomercard/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomercard/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listcustomercard/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
