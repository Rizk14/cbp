<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu352')=='t') && ($this->session->userdata('i_area')=='00')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t') && ($this->session->userdata('i_area')=='00'))
		   ){
			$data['page_title'] = $this->lang->line('laporanpenjualan').' per Minggu per Toko (Produk)';
			$data['iperiode']	= '';
			$this->load->view('srpenjualanpermingguperareapertoko/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu352')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$this->load->model('srpenjualanpermingguperareapertoko/mmaster');
			$data['page_title'] = $this->lang->line('laporanpenjualan').' per Minggu per Toko (Produk)';
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->baca($iperiode);
  		$data['jmlminggu']	= $this->mmaster->jmlminggu($iperiode);
  		$data['minggu']	= $this->mmaster->minggu($iperiode);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Laporan Penjualan All Area Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('srpenjualanpermingguperareapertoko/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu352')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$dto	= $this->input->post('dto');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/srpenjualanpermingguperareapertoko/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_target a, tr_area b
										              where a.i_periode = '$iperiode'
										              and a.i_area=b.i_area
										              and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '50';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('srpenjualanpermingguperareapertoko/mmaster');
			$data['page_title'] = $this->lang->line('laporanpenjualan');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('srpenjualanpermingguperareapertoko/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu351')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/srpenjualanpermingguperareapertoko/cform/area/index/';
      $cari=strtoupper($this->input->post("cari"));
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%'",false);
			}else{
				$query = $this->db->query("select * from tr_area where (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5') and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('srpenjualanpermingguperareapertoko/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('srpenjualanpermingguperareapertoko/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
