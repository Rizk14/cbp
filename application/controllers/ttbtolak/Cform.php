<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu79')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/ttbtolak/cform/index/';
			$iuser  = $this->session->userdata('user_id');
			$dttb   = date('Y/m/d');
			$now    = substr($dttb,2,2).substr($dttb,5,2);
			$dudet	= $this->fungsi->dateAdd("m",-1,$dttb);
			$dudet 	= explode("-", $dudet);
			$mon	= $dudet[1];
			$yir 	= substr($dudet[0],2,2);
			$dudet	= $yir.$mon;
			$cari 	= ($this->input->post('cari', FALSE));
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$allarea= $this->session->userdata('allarea');
			if($area1=='00'){
			    $query = $this->db->query("	select a.i_sj
			                            from tm_nota a, tr_customer b, tr_area c 
			                            where a.i_customer=b.i_customer and a.i_area=c.i_area and a.f_ttb_tolak='f'
			                            and (a.i_sj like '%-$now-%' or a.i_sj like '%-$dudet-%') and a.f_nota_cancel='f' 
			                            and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
			                            or upper(a.i_sj) like '%$cari%' or upper(a.i_nota) like '%$cari%') ",false);
			}else{
			    $query = $this->db->query("	select a.i_sj
			                            from tm_nota a, tr_customer b, tr_area c 
			                            where a.i_customer=b.i_customer and a.i_area=c.i_area and a.f_ttb_tolak='f' 
			                            and (a.i_sj like '%-$now-%' or a.i_sj like '%-$dudet-%') and a.f_nota_cancel='f' 
			                            and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
			                            or upper(a.i_sj) like '%$cari%' or upper(a.i_nota) like '%$cari%') 
			                            and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('cttbtolak');
			$this->load->model('ttbtolak/mmaster');
			$data['ittb']='';
			$data['isj']='';
			$data['isi']=$this->mmaster->bacasemua($iuser,$now,$dudet,$cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('ttbtolak/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu79')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listspb');
			$this->load->view('ttbtolak/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu79')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb	= $this->input->post('ispbdelete', TRUE);
			$this->load->model('ttbtolak/mmaster');
			$this->mmaster->delete($ispb);
			$data['page_title'] = $this->lang->line('listspb');
			$data['ispb']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('ttbtolak/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu79')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$cari = ($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/ttbtolak/cform/index/';*/
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$allarea= $this->session->userdata('allarea');

			$cari=($this->input->post("cari",false));
			if($cari=='')$cari=$this->uri->segment(4);
			if($cari=='zxvf'){
				$cari='';
				$config['base_url'] = base_url().'index.php/ttbtolak/cform/cari/zxvf/';
			}else{
				$config['base_url'] = base_url().'index.php/ttbtolak/cform/cari/'.$cari.'/';
			}

			$config['per_page'] = '10';
			$limo=$config['per_page'];
			$ofso=$this->uri->segment(4);
			if($ofso=='')
				$ofso=0;
      if($area1=='00'){
			$query = $this->db->query("	select a.*, b.e_customer_name, c.e_area_name 
                                  		from tm_nota a, tr_customer b, tr_area c 
                                  		where a.i_customer=b.i_customer
										and a.i_area=c.i_area and (trim(a.i_nota) = '' or a.i_nota isnull)
										and a.f_lunas = 'f' and a.f_ttb_tolak = 'f'
										and (upper(a.i_sj) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%'
										or upper(c.e_area_name) ilike '%$cari%')
										order by a.i_nota desc 
									",false);
      }else{
			$query = $this->db->query("	select a.*, b.e_customer_name, c.e_area_name 
                                  		from tm_nota a, tr_customer b, tr_area c 
                                  		where a.i_customer=b.i_customer
										and a.i_area=c.i_area and (trim(a.i_nota) = '' or a.i_nota isnull)
										and a.f_lunas = 'f' and a.f_ttb_tolak = 'f'
										and (a.i_area='$area1' or a.i_area='$area2' 
										or a.i_area='$area3' or a.i_area='$area4' 
										or a.i_area='$area5')
										and (upper(a.i_sj) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%'
										or upper(c.e_area_name) ilike '%$cari%')
										order by a.i_nota desc ",false);
      }
			$config['total_rows'] = $query->num_rows(); 			
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('ttbtolak/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('cttbtolak');
			$data['ittb']='';
			$data['isj']='';
	 		$this->load->view('ttbtolak/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu79')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('ttbtolak');
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
			  )
			{
        $area1	= $this->session->userdata('i_area');
				$isj = $this->uri->segment(4);
				$iarea = $this->uri->segment(5);
#        if($area1=='00'){
#  				$query = $this->db->query("select i_product from tm_nota_item where i_sj = '$isj' and i_area = '$iarea'");
#        }else{
          $query = $this->db->query("select i_product from tm_nota_item where i_sj = '$isj' and i_area = '$iarea'");
#        }
				$data['jmlitem'] = $query->num_rows(); 				
				$data['isj'] = $isj;
				$data['ittb']='';
        $data['tgl']=date('d-m-Y');
				$this->load->model('ttbtolak/mmaster');
				$data['isi']=$this->mmaster->baca($isj,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isj,$iarea);
		 		$this->load->view('ttbtolak/vmainform',$data);
			}else{
				$this->load->view('ttbtolak/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu79')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('ttbtolak/mmaster');
			$inota 	= $this->input->post('inota', TRUE);
			$dnota 	= $this->input->post('dnota', TRUE);
			if($dnota!=''){
				$tmp=explode("-",$dnota);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dnota=$th."-".$bl."-".$hr;
			}else{
        $dnota=null;
      }
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomername	= $this->input->post('ecustomername', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
      $isj        = $this->input->post('isj', TRUE);
			$eareaname		= $this->input->post('eareaname', TRUE);
			$isalesman		= $this->input->post('isalesman',TRUE);
			$esalesmanname	= $this->input->post('esalesmanname',TRUE);
			$nttbdiscount1	= $this->input->post('ncustomerdiscount1',TRUE);
			$nttbdiscount2	= $this->input->post('ncustomerdiscount2',TRUE);
			$nttbdiscount3	= $this->input->post('ncustomerdiscount3',TRUE);
			$vttbdiscount1	= $this->input->post('vcustomerdiscount1',TRUE);
			$vttbdiscount2	= $this->input->post('vcustomerdiscount2',TRUE);
			$vttbdiscount3	= $this->input->post('vcustomerdiscount3',TRUE);
			$vttbdiscounttotal	= $this->input->post('vttbdiscounttotal',TRUE);
			$vttbdiscounttotal	= str_replace(',','',$vttbdiscounttotal);
			$vttbnetto			= $this->input->post('vttbnetto',TRUE);
			$vttbnetto			= str_replace(',','',$vttbnetto);
			$vttbgross			= $vttbnetto+$vttbdiscounttotal;
			$fttbplusppn		= $this->input->post('fttbplusppn',TRUE);
			$fttbplusdiscount	= $this->input->post('fttbplusdiscount',TRUE);
			$jml				= $this->input->post('jml', TRUE);
			$ittb 				= $this->input->post('ittb', TRUE);
			$dttb 				= $this->input->post('dttb', TRUE);
			if($dttb!=''){
				$tmp=explode("-",$dttb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dttb=$th."-".$bl."-".$hr;
#        $thbl=substr($th,2,2).$bl;
        $thbl=$th.$bl;
			}
			$tahun	= $th;
			$dreceive1 	= $this->input->post('dreceive1', TRUE);
			if($dreceive1!=''){
				$tmp=explode("-",$dreceive1);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dreceive1=$th."-".$bl."-".$hr;
			}else{
        $dreceive1=null;
      }
			$ettbremark 	= $this->input->post('eremark', TRUE);
			if($ettbremark=='')
				$ettbremark=null;
			$ecustomerpkpnpwp 	= $this->input->post('ecustomerpkpnpwp', TRUE);
			if($ecustomerpkpnpwp=='')
				$fttbpkp	= 'f';
			else
				$fttbpkp	= 't';
			$fttbcancel='f';
			if(($dttb!='') && ($ittb!='')){
				$this->db->trans_begin();
				$ibbm				= $this->mmaster->runningnumberbbm($thbl);
				$dbbm				= $dttb;
				$istore				= 'AA';
				$istorelocation		= '01';
				$istorelocationbin	= '00';
				$eremark			= 'TTB Tolakan';
				$ibbktype			= '01';
				$ibbmtype			= '05';
				$this->mmaster->insertheader(	$iarea,$ittb,$dttb,$icustomer,$isalesman,$inota,$dnota,$nttbdiscount1,$nttbdiscount2,
												$nttbdiscount3,$vttbdiscount1,$vttbdiscount2,$vttbdiscount3,$fttbpkp,$fttbplusppn,
												$fttbplusdiscount,$vttbgross,$vttbdiscounttotal,$vttbnetto,$ettbremark,$fttbcancel,
												$dreceive1,$tahun,$isj);
				$this->mmaster->insertbbmheader($ittb,$dttb,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea,$isalesman);
        $x=0;
				for($i=1;$i<=$jml;$i++){
				  $iproduct					= $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade			= 'A';
				  $iproductmotif			= $this->input->post('motif'.$i, TRUE);
				  $eproductname				= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice				= $this->input->post('vproductretail'.$i, TRUE);
				  $vunitprice				= str_replace(',','',$vunitprice);
				  $ndeliver					= $this->input->post('ndeliver'.$i, TRUE);
				  $nquantity				= $this->input->post('nquantity'.$i, TRUE);
				  $ettbremark				= $this->input->post('eremark'.$i, TRUE);
				  if($ettbremark=='')
					$ettbremark=null;
				  if($nquantity>0){
            $x++;
					  $this->mmaster->insertdetail(	$iarea,$ittb,$dttb,$iproduct,$iproductgrade,$iproductmotif,
													$nquantity,$vunitprice,$ettbremark,$tahun,$ndeliver,$x,$isj);
					  $this->mmaster->insertbbmdetail(	$iproduct,$iproductgrade,$eproductname,$iproductmotif,$nquantity,
														$vunitprice,$ittb,$ibbm,$eremark,$dttb,$ibbmtype,$x);
#####
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$ibbm,$q_in,$q_out,$nquantity,$q_aw,$q_ak);
            $th=substr($dttb,0,4);
            $bl=substr($dttb,5,2);
            $emutasiperiode=$th.$bl;
            $ada=$this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$emutasiperiode);
            if($ada=='ada')
            {
              $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$nquantity,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$nquantity,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00'))
            {
              $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$nquantity,$q_ak);
            }else{
              $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$nquantity);
            }
#####
				  }
				}
			}
			if ($this->db->trans_status() === FALSE)
			{
			  $this->db->trans_rollback();
			}else{
#        $this->db->trans_rollback();
				$this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update TTB Tolak Area '.$iarea.' No:'.$ittb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses'] = true;
				$data['inomor']	= $iarea.$ittb;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu79')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('ttbtolak')." update";
			if(
				($this->uri->segment(4)) && ($this->uri->segment(5)) &&
				($this->uri->segment(6)) && ($this->uri->segment(7)) &&
				($this->uri->segment(8)) && ($this->uri->segment(9))			
			  )
			{
				$ittb 				= $this->uri->segment(4);
				$iarea				= $this->uri->segment(5);
				$tahun				= $this->uri->segment(6);
				$fnotakoreksi	= $this->uri->segment(7);
				$dfrom				= $this->uri->segment(8);
				$dto					= $this->uri->segment(9);
				$query 				= $this->db->query("select i_nota, i_area from tm_ttbtolak 
													where i_ttb = '$ittb' and i_area = '$iarea' and n_ttb_year=$tahun");
				if($query->num_rows()>0){
					foreach($query->result() as $row){
						$nota=$row->i_nota;
						$area=$row->i_area;
					}
					if($fnotakoreksi=='t'){
						$query 			= $this->db->query("select i_product from tm_notakoreksi_item 
															where i_nota = '$nota' and i_area = '$area'");
					}else{
						$query 			= $this->db->query("select i_product from tm_nota_item 
															where i_nota = '$nota' and i_area = '$area'");
					}
					$data['jmlitem'] 		= $query->num_rows();
					$data['ittb'] 			= $ittb;
					$data['iarea']			= $iarea;
					$data['tahun']			= $tahun;
					$data['fnotakoreksi']	= $fnotakoreksi;
					$data['dfrom']			= $dfrom;
					$data['dto']				= $dto;
					$this->load->model('ttbtolak/mmaster');
					$data['isi']=$this->mmaster->bacattb($iarea,$ittb,$tahun);
					$data['detail']=$this->mmaster->bacattbdetail($iarea,$ittb,$tahun,$fnotakoreksi,$nota);
			 		$this->load->view('ttbtolak/vmainform',$data);
				} 				
			}else{
				$this->load->view('ttbtolak/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function updatettbtolak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu79')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('ttbtolak/mmaster');
			$iarea			= $this->input->post('iarea', TRUE);
			$nttbdiscount1	= $this->input->post('ncustomerdiscount1',TRUE);
			$nttbdiscount2	= $this->input->post('ncustomerdiscount2',TRUE);
			$nttbdiscount3	= $this->input->post('ncustomerdiscount3',TRUE);
			$vttbdiscount1	= $this->input->post('vcustomerdiscount1',TRUE);
			$vttbdiscount2	= $this->input->post('vcustomerdiscount2',TRUE);
			$vttbdiscount3	= $this->input->post('vcustomerdiscount3',TRUE);
			$vttbdiscounttotal	= $this->input->post('vttbdiscounttotal',TRUE);
			$vttbdiscounttotal	= str_replace(',','',$vttbdiscounttotal);
			$vttbnetto			= $this->input->post('vttbnetto',TRUE);
			$vttbnetto			= str_replace(',','',$vttbnetto);
			$vttbgross			= $vttbnetto+$vttbdiscounttotal;
			$jml				= $this->input->post('jml', TRUE);
			$ittb 				= $this->input->post('ittb', TRUE);
			$dttb 				= $this->input->post('dttb', TRUE);
			if($dttb!=''){
				$tmp=explode("-",$dttb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dttb=$th."-".$bl."-".$hr;
			}
			$tahun	= $this->input->post('nttbyear', TRUE);
			$dreceive1 	= $this->input->post('dreceive1', TRUE);
			if($dreceive1!=''){
				$tmp=explode("-",$dreceive1);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dreceive1=$th."-".$bl."-".$hr;
			}else{
        $dreceive1=null;
      }
			$ettbremark 	= $this->input->post('eremark', TRUE);
			if($ettbremark=='') $ettbremark=null;
			$istore				= 'AA';
			$istorelocation		= '01';
			$istorelocationbin	= '00';
			$eremark			= 'TTB Tolakan';
			$ibbktype			= '01';
			$ibbmtype			= '05';
			$ibbm				= $this->input->post('ibbm', TRUE);
			$dbbm				= $dttb;
			if(($dttb!='') && ($ittb!='')){
				$this->db->trans_begin();
				$this->mmaster->updateheader(	$ittb,$iarea,$tahun,$dttb,$dreceive1,$ettbremark,
												$nttbdiscount1,$nttbdiscount2,$nttbdiscount3,$vttbdiscount1,
												$vttbdiscount2,$vttbdiscount3,$vttbdiscounttotal,$vttbnetto,
												$vttbgross	);
				$this->mmaster->updatebbmheader($ittb,$dttb,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea);
        $x=0;
				for($i=1;$i<=$jml;$i++){
				  $iproduct					= $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade			= 'A';
				  $iproductmotif			= $this->input->post('motif'.$i, TRUE);
				  $eproductname				= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice				= $this->input->post('vproductretail'.$i, TRUE);
				  $vunitprice				= str_replace(',','',$vunitprice);
				  $ndeliver					= $this->input->post('ndeliver'.$i, TRUE);
				  $nquantity				= $this->input->post('nquantity'.$i, TRUE);
				  $nasal    				= $this->input->post('nasal'.$i, TRUE);
				  $ettbremark				= $this->input->post('eremark'.$i, TRUE);
				  if($ettbremark=='')
					$ettbremark=null;
				  $this->mmaster->deletedetail(	$iproduct, $iproductgrade, $ittb, $iproductmotif,$nquantity,$istore,
							 					$istorelocation,$istorelocationbin,$tahun,$iarea);
#####
          $th=substr($dttb,0,4);
					$bl=substr($dttb,5,2);
					$emutasiperiode=$th.$bl;
					$tra=$this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ibbm);
				  $this->mmaster->updatemutasi01($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$nasal,$emutasiperiode);
				  $this->mmaster->updateic01($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$nasal);
#####
				  if($nquantity>0){
            $x++;
				 	  $this->mmaster->insertdetail(	$iarea,$ittb,$dttb,$iproduct,$iproductgrade,$iproductmotif,
													$nquantity,$vunitprice,$ettbremark,$tahun,$ndeliver,$x);
					  $this->mmaster->insertbbmdetail(	$iproduct,$iproductgrade,$eproductname,$iproductmotif,$nquantity,
														$vunitprice,$ittb,$ibbm,$eremark,$dttb,$ibbmtype,$x);
#####
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            
            $this->mmaster->inserttrans44($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$ibbm,$q_in,$q_out,$nquantity,$q_aw,$q_ak,$tra);
            $ada=$this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$emutasiperiode);
            if($ada=='ada')
            {
              $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$nquantity,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$nquantity,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00'))
            {
              $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$nquantity,$q_ak);
            }else{
              $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$nquantity);
            }
#####
				  }
				}
			}
			if ($this->db->trans_status() === FALSE)
			{
		    $this->db->trans_rollback();
			}else{
		    $this->db->trans_rollback();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update TTB Tolak Area '.$iarea.' No:'.$ittb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

#				$this->db->trans_commit();
				$data['sukses'] = true;
				$data['inomor']	= $iarea.$ittb;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
