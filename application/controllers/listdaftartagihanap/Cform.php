<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdaftartagihanap');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listdaftartagihanap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= trim($this->input->post('isupplier'));
			//$iarea	= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listdaftartagihanap/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			if($isupplier!='AS'){
			  $query = $this->db->query(" select a.*, b.e_area_name from tm_dtap a, tr_area b
										              where a.i_area=b.i_area 
										              and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_dtap) like '%$cari%')
										              and a.i_supplier='$isupplier' and
										              a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy')",false);
			}else{
				$query = $this->db->query(" select a.*, b.e_area_name from tm_dtap a, tr_area b
										              where a.i_area=b.i_area 
										              and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%' or upper(a.i_dtap) like '%$cari%') and
										              a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND a.d_dtap <= to_date('$dto','dd-mm-yyyy') ORDER BY a.i_supplier, a.i_dtap",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdaftartagihanap');
			$this->load->model('listdaftartagihanap/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Hutang Dagang Supplier '.$isupplier.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listdaftartagihanap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdaftartagihanap');
			$this->load->view('listdaftartagihanap/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$idt			= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$isupplier= $this->uri->segment(6);
			$dfrom		= $this->uri->segment(7);
			$dto			= $this->uri->segment(8);
			$this->load->model('listdaftartagihanap/mmaster');
			$this->mmaster->delete($idt,$iarea,$isupplier);
			
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus Hutang Dagang Supplier Area '.$iarea.' Supplier:'.$isupplier.' No:'.$idt;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);		
			$config['base_url'] = base_url().'index.php/listdaftartagihanap/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_dtap a, tr_area b
										where a.i_area=b.i_area 
										and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										or upper(a.i_dtap) like '%$cari%')
										and a.i_supplier='$isupplier' and
										a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_dtap <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdaftartagihanap');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listdaftartagihanap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listdaftartagihanap/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_dtap a, tr_area b
										where a.i_area=b.i_area 
										and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										or upper(a.i_dtap) like '%$cari%')
										and a.i_supplier='$isupplier' and
										a.d_dtap >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_dtap <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			if($cari!=NULL){$config['per_page'] = '40';}else{
			$config['per_page'] = '10';}
			//$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdaftartagihanap');
			$this->load->model('listdaftartagihanap/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']		= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listdaftartagihanap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listdaftartagihanap/cform/supplier/index/';
			$query = $this->db->query("select * from tr_supplier ",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10'; 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listdaftartagihanap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('listdaftartagihanap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listdaftartagihanap/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%' 
						      	  or upper(e_supplier_name) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listdaftartagihanap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('listdaftartagihanap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
