<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu270')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listnotapersalesman');
			$data['dfrom']	= '';
			$data['dto']	= '';
			$this->load->view('listnotapersalesman/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu270')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari	= strtoupper($this->input->post('cari'));
      if($cari==''){
        if($this->uri->segment(6)!='index'){
          $cari=$this->uri->segment(6);
        }
      }
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$allarea= $this->session->userdata('allarea');	
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($cari!=''){
  			$config['base_url'] = base_url().'index.php/listnotapersalesman/cform/view/'.$dfrom.'/'.$dto.'/'.$cari.'/';
      }else{
        $config['base_url'] = base_url().'index.php/listnotapersalesman/cform/view/'.$dfrom.'/'.$dto.'/index/';
      }
      if(($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00')){
        $que=$this->db->query(" select	sum(v_nota) as v_nota, sum(v_sj) as v_sj
                                from (
                                select sum(a.v_nota_netto) as v_nota, sum(a.v_nota_discounttotal) as v_nota_discounttotal, 0 as v_sj, 
                                0 as v_sj_discounttotal, a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name
                                from tm_nota a, tr_area b, tr_salesman c
                                where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
                                and a.d_sj <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f'
                                and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                                and a.i_salesman=c.i_salesman and not i_nota isnull
                                group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
                                union all
                                select 0 as v_nota, 0 as v_nota_discounttotal, sum(a.v_nota_netto) as v_sj, 
                                sum(a.v_nota_discounttotal) as v_sj_discounttotal, a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name
                                from tm_nota a, tr_area b, tr_salesman c
                                where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
                                and a.d_sj <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f'
                                and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                                and a.i_salesman=c.i_salesman and i_nota isnull
                                group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
                                ) as x",false);
        $tmp=$que->row();
        $data['nota']=$tmp->v_nota;
        $data['sj']=$tmp->v_sj;
        $query=$this->db->query("select	sum(v_nota)
                            from (
                            select sum(a.v_nota_netto) as v_nota, sum(a.v_nota_discounttotal) as v_nota_discounttotal, 0 as v_sj, 
                            0 as v_sj_discounttotal, a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name
                            from tm_nota a, tr_area b, tr_salesman c
                            where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
                            and a.d_sj <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f'
                            and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                            and a.i_salesman=c.i_salesman and not i_nota isnull
                            group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
                            union all
                            select 0 as v_nota, 0 as v_nota_discounttotal, sum(a.v_nota_netto) as v_sj, 
                            sum(a.v_nota_discounttotal) as v_sj_discounttotal, a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name
                            from tm_nota a, tr_area b, tr_salesman c
                            where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
                            and a.d_sj <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f'
                            and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                            and a.i_salesman=c.i_salesman and i_nota isnull
                            group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
                            ) as x
                            group by x.i_area, x.e_area_name, x.i_salesman, x.e_salesman_name",false);
      }else{
        $que=$this->db->query("select	sum(v_nota) as v_nota, sum(v_sj) as v_sj
                            from (
                            select sum(a.v_nota_netto) as v_nota, sum(a.v_nota_discounttotal) as v_nota_discounttotal, 0 as v_sj, 
                            0 as v_sj_discounttotal, a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name
                            from tm_nota a, tr_area b, tr_salesman c
                            where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
                            and a.d_sj <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f'
                            and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                            and a.i_salesman=c.i_salesman and not i_nota isnull
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
                            union all
                            select 0 as v_nota, 0 as v_nota_discounttotal, sum(a.v_nota_netto) as v_sj, 
                            sum(a.v_nota_discounttotal) as v_sj_discounttotal, a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name
                            from tm_nota a, tr_area b, tr_salesman c
                            where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
                            and a.d_sj <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f'
                            and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                            and a.i_salesman=c.i_salesman and i_nota isnull
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
                            ) as x ",false);
        $tmp=$que->row();
        $data['nota']=$tmp->v_nota;
        $data['sj']=$tmp->v_sj;

        $query=$this->db->query("select	sum(v_nota)
                            from (
                            select sum(a.v_nota_netto) as v_nota, sum(a.v_nota_discounttotal) as v_nota_discounttotal, 0 as v_sj, 
                            0 as v_sj_discounttotal, a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name
                            from tm_nota a, tr_area b, tr_salesman c
                            where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
                            and a.d_sj <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f'
                            and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                            and a.i_salesman=c.i_salesman and not i_nota isnull
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
                            union all
                            select 0 as v_nota, 0 as v_nota_discounttotal, sum(a.v_nota_netto) as v_sj, 
                            sum(a.v_nota_discounttotal) as v_sj_discounttotal, a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name
                            from tm_nota a, tr_area b, tr_salesman c
                            where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
                            and a.d_sj <= to_date('$dto','dd-mm-yyyy') and a.f_nota_cancel='f'
                            and a.i_area=b.i_area and (upper(c.e_salesman_name) like '%$cari%' or upper(c.i_salesman) like '%$cari%')
                            and a.i_salesman=c.i_salesman and i_nota isnull
                            and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')
                            group by a.i_area, b.e_area_name, a.i_salesman, c.e_salesman_name 
                            ) as x
                            group by x.i_area, x.e_area_name, x.i_salesman, x.e_salesman_name",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = 'all';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('listnotapersalesman/mmaster');
			$data['page_title'] = $this->lang->line('listnotapersalesman');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isi']	= $this->mmaster->bacaperiode($cari,$allarea,$area1,$area2,$area3,$area4,$area5,$dfrom,$dto,$config['per_page'],$this->uri->segment(7));

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Membuka Data Nota Per Salesman tanggal:'.$dfrom.' sampai:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('listnotapersalesman/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
