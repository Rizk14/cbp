<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu511')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-notaakunting');
#			$data['iperiode']='';
			$data['dfrom']='';
			$data['dto']='';
			$data['iarea']='';
			$this->load->view('exp-notaakunting/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu511')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('exp-notaakunting/mmaster');
			$iperiode  	= $this->input->post("iperiode");
			$this->load->model('exp-notaakunting/mmaster');
			$data['page_title'] = $this->lang->line('exp-notaakunting');
			$data['iperiode']	= $iperiode;
			$isi		  = $this->mmaster->bacaperiode($iperiode);
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Nota Akunting")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      if(count($isi)>0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(30);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:O2'
				);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Nota');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,0,1);
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Tgl Nota');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(1,1,1,1);
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Jatuh Tempo');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(2,1,2,1);
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Kode Lang');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3,1,3,1);
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Nama Lang');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4,1,4,1);
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'No Faktur Komersial');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5,1,5,1);
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Seri Pajak');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6,1,6,1);
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Tgl Pajak');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7,1,7,1);
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'DPP');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8,1,8,1);
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Nilai Kotor');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(9,1,9,1);
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Diskon');
			  $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('L1','Nilai Bersih');
    		$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('M1', 'PPn');
			  $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('N1','Kode Area');
    		$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('O1', 'Nama Area');
			  $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
        }
        $i=3;
        foreach($isi as $row){
        
        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			  array(
				  'font' => array(
					  'name'	=> 'Arial',
					  'bold'  => false,
					  'italic'=> false,
					  'size'  => 10
				  )
			  ),
			  'A'.$i.':O'.$i
			  );
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->d_nota, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->d_jatuh_tempo, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_customer, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->i_faktur_komersial, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->i_seri_pajak, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->d_pajak, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->v_dpp, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->v_nota_gross, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->v_nota_discount, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->v_nota_ppn, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $row->e_area_name, Cell_DataType::TYPE_STRING);
        $i++;
            }
        }
      
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='Nota Akunting'.$iperiode.'.xls';
      if(file_exists('excel/'.$nama)){
        @chmod('excel/'.$nama, 0777);
        @unlink('excel/'.$nama);
      }
      $objWriter->save('excel/'.'00'.'/'.$nama);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
		  $data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Nota Akunting '.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$data['sukses'] = true;
			$data['inomor']	= "Nota Akunting";
			$this->load->view('nomor',$data);
  } 
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu511')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-notaakunting');
			$this->load->view('exp-notaakunting/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
