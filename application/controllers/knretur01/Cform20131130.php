<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu111')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('knretur');
			$this->load->model('knretur/mmaster');
			$data['ikn']='';
      $data['irefference']='';
      $data['icustomer']='';
      $data['ecustomername']='';
      $data['iarea']='';
      $data['isalesman']='';
      $data['esalesmanname']='';
      $data['eareaname']='';
      $data['ecustomeraddress']='';
      $data['vgross']='';
      $data['vdiscount']='';
      $data['vnetto']='';
      $data['vsisa']='';
      $data['drefference']='';
      $data['icustomergroupar']='';
      $data['nttbdiscount1']='';
      $data['nttbdiscount2']='';
      $data['nttbdiscount3']='';
      $data['vttbdiscount1']='';
      $data['vttbdiscount2']='';
      $data['vttbdiscount3']='';
			$data['detail']='';
      $data['dkn']='';
      $data['jml']='';
			$this->load->view('knretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu111')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('knretur');
			$this->load->view('knretur/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$data['page_title'] = $this->lang->line('knretur')." update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5) && $this->uri->segment(6)
			  ){
				$this->load->model('knretur/mmaster');
				$data['ikn'] 		= $this->uri->segment(4);
				$data['nknyear']= $this->uri->segment(5);
				$data['iarea'] 	= $this->uri->segment(6);
				$dfrom					= $this->uri->segment(7);
				$dto 						= $this->uri->segment(8);
				$data['dfrom'] 	= $this->uri->segment(7);
				$data['dto']	 	= $this->uri->segment(8);
				$ikn 						= $this->uri->segment(4);
				$nknyear				= $this->uri->segment(5);
				$iarea 					= $this->uri->segment(6);
				$ibbm 					= $this->uri->segment(9);
				$data['isi']		= $this->mmaster->bacakn($ikn,$nknyear,$iarea);
        $data['detail'] = $this->mmaster->bacabbmdetail($ibbm);
        $data['jml']=$this->mmaster->jmldetail($ibbm);
		 		$this->load->view('knretur/vformupdate',$data);
			}else{
				$this->load->view('knretur/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu111')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea 			= $this->input->post('iarea', TRUE);
			$ikn 			= $this->input->post('ikn', TRUE);
			$icustomer		= $this->input->post('icustomer', TRUE);
			$irefference	= $this->input->post('irefference', TRUE);
			$icustomergroupar= $this->input->post('icustomergroupar', TRUE);
			$isalesman		= $this->input->post('isalesman', TRUE);
			$ikntype		= '01';
			$drefference	= $this->input->post('drefference', TRUE);
			if($drefference!=''){
				$tmp=explode("-",$drefference);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$drefference=$th."-".$bl."-".$hr;
			}
			$dkn			= $this->input->post('dkn', TRUE);
			if($dkn!=''){
				$tmp=explode("-",$dkn);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkn=$th."-".$bl."-".$hr;
				$nknyear=$th;
			}
			$ipajak 		= $this->input->post('ipajak', TRUE);
			$dpajak			= $this->input->post('dpajak', TRUE);
			if($dpajak!=''){
				$tmp=explode("-",$dpajak);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpajak=$th."-".$bl."-".$hr;
			}
			$fcetak		= 'f';
			$fmasalah	= $this->input->post('fmasalah', TRUE);
			if($fmasalah=='')
				$fmasalah='f';
			else
				$fmasalah='t';
			$finsentif	= $this->input->post('finsentif', TRUE);
			if($finsentif=='')
				$finsentif='f';
			else
				$finsentif='t';
			$vnetto		= $this->input->post('vnetto', TRUE);
			$vnetto		= str_replace(',','',$vnetto);
			$vsisa		= $vnetto;
			$vgross		= $this->input->post('vgross', TRUE);
			$vgross		= str_replace(',','',$vgross);
			$vdiscount	= $this->input->post('vdiscount', TRUE);
			$vdiscount	= str_replace(',','',$vdiscount);
			$eremark	= $this->input->post('eremark', TRUE);
			if (
				(isset($irefference) && $irefference != '') && 
				(isset($dkn) && $dkn != '') &&
				(isset($iarea) && $iarea != '') &&
				(isset($icustomer) && $icustomer != '') &&
				($finsentif != 'f')
			   )
			{
				$this->load->model('knretur/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update(	$iarea,$ikn,$icustomer,$irefference,$icustomergroupar,$isalesman,$ikntype,$dkn,$nknyear,$fcetak,$fmasalah,
										            $finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference,$ipajak,$dpajak);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
#					$this->db->trans_rollback();
					$this->db->trans_commit();

          $sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update KN Retur Area '.$iarea.' No:'.$ikn;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );


					$data['sukses']			= true;
					$data['inomor']			= $ikn;
					$this->load->view('nomor',$data);
				}
			}
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu111')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikn		= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$this->load->model('knretur/mmaster');
			$this->mmaster->delete($ikn,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Hapus KN Retur No:'.$ikn.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['page_title'] = $this->lang->line('knretur');
			$data['isi']=$this->mmaster->bacasemua();
			$data['ikn']='';
			$this->load->view('knretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu111')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/knretur/cform/area/index/';
			$query = $this->db->query("select * from tr_area ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('knretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('knretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu111')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/knretur/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_area
									   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('knretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('knretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bbm()
	{
		if (

			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu111')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/knretur/cform/bbm/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select a.i_bbm
							                    from tm_bbm a, tr_salesman d, tm_ttbretur b, tr_area c
							                    where a.i_bbm_type='05' and a.i_area='$iarea' 
							                    and a.i_salesman=d.i_salesman and a.i_bbm like 'BBM%'
							                    and a.i_bbm=b.i_bbm and a.i_area=b.i_area
							                    and a.i_area=c.i_area and not a.i_bbm in
                                  (select i_refference from tm_kn where i_area='$iarea' and i_refference like 'BBM%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('knretur/mmaster');
			$data['page_title'] = $this->lang->line('list_bbm');
			$data['isi']=$this->mmaster->bacabbm($iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('knretur/vlistbbm', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caribbm()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu111')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/knretur/cform/bbm/'.$iarea.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select a.i_bbm
						                     from tm_bbm a, tr_salesman d, tm_ttbretur b, tr_area c
						                     where a.i_bbm_type='05' and a.i_area='$iarea' 
						                     and a.i_salesman=d.i_salesman and a.i_bbm like 'BBM%'
						                     and a.i_bbm=b.i_bbm and a.i_area=b.i_area
						                     and a.i_area=c.i_area and not a.i_bbm in
                                (select i_refference from tm_kn where i_area='$iarea' and i_refference like 'BBM%')
						                     and (a.i_bbm like '%$cari%' or b.i_ttb like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('knretur/mmaster');
			$data['page_title'] = $this->lang->line('list_bbm');
			$data['isi']=$this->mmaster->caribbm($cari, $iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('knretur/vlistbbm', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu111')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea 			= $this->input->post('iarea', TRUE);
			$ikn 			= $this->input->post('ikn', TRUE);
			$icustomer		= $this->input->post('icustomer', TRUE);
			$irefference	= $this->input->post('irefference', TRUE);
			$icustomergroupar= $this->input->post('icustomergroupar', TRUE);
			$isalesman		= $this->input->post('isalesman', TRUE);
			$ikntype		= '01';
			$drefference	= $this->input->post('drefference', TRUE);
			if($drefference!=''){
				$tmp=explode("-",$drefference);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$drefference=$th."-".$bl."-".$hr;
			}
			$dkn			= $this->input->post('dkn', TRUE);
			if($dkn!=''){
				$tmp=explode("-",$dkn);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkn=$th."-".$bl."-".$hr;
				$nknyear=$th;
			}
      $ipajak 		= $this->input->post('ipajak', TRUE);
			$dpajak			= $this->input->post('dpajak', TRUE);
			if($dpajak!=''){
				$tmp=explode("-",$dpajak);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpajak=$th."-".$bl."-".$hr;
			}
			$fcetak		= 'f';
			$fmasalah	= $this->input->post('fmasalah', TRUE);
			if($fmasalah=='')
				$fmasalah='f';
			else
				$fmasalah='t';
			$finsentif	= $this->input->post('finsentif', TRUE);
			if($finsentif=='')
				$finsentif='f';
			else
				$finsentif='t';
			$vnetto		= $this->input->post('vnetto', TRUE);
			$vnetto		= str_replace(',','',$vnetto);
			$vsisa		= $vnetto;
			$vgross		= $this->input->post('vgross', TRUE);
			$vgross		= str_replace(',','',$vgross);
			$vdiscount	= $this->input->post('vdiscount', TRUE);
			$vdiscount	= str_replace(',','',$vdiscount);
      if($vdiscount=='') $vdiscount=0;
			$eremark	= $this->input->post('eremark', TRUE);
			if (
				(isset($irefference) && $irefference != '') && 
				(isset($iarea) && $iarea != '') &&
				(isset($icustomer) && $icustomer != '') &&
				($finsentif != 'f')
			   )
			{
				$this->load->model('knretur/mmaster');
				$ikn = $this->mmaster->runningnumberkn($nknyear,$iarea);
				$this->db->trans_begin();
				$this->mmaster->insert(	$iarea,$ikn,$icustomer,$irefference,$icustomergroupar,$isalesman,$ikntype,$dkn,$nknyear,$fcetak,$fmasalah,
										$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference,$ipajak,$dpajak);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
#					$this->db->trans_rollback();
					$this->db->trans_commit();

          $sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input KN Retur Area '.$iarea.' No:'.$ikn;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ikn;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function adabbm()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu111')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('knretur');
			$this->load->model('knretur/mmaster');
			$data['ikn']='';
      $irefference=$this->uri->segment(4);
      $icustomer=$this->uri->segment(5);
      $ecustomername=str_replace('%20',' ',$this->uri->segment(6));
      $ecustomername=str_replace('tandakurungbuka','(',$ecustomername);
      $ecustomername=str_replace('tandadan','&',$ecustomername);
      $ecustomername=str_replace('tandakurungtutup',')',$ecustomername);
      $ecustomername=str_replace('tandatitik','.',$ecustomername);
      $ecustomername=str_replace('tandakoma',',',$ecustomername);
      $ecustomername=str_replace('tandagaring','/',$ecustomername);
      $iarea=$this->uri->segment(7);
      $isalesman=$this->uri->segment(8);
      $esalesmanname=str_replace('%20',' ',$this->uri->segment(9));
      $esalesmanname=str_replace('tandatitik','.',$esalesmanname);
      $eareaname=str_replace('%20',' ',$this->uri->segment(10));
      $ecustomeraddress=str_replace('%20',' ',$this->uri->segment(11));
      $ecustomeraddress=str_replace('tandatitik','.',$ecustomeraddress);
      $ecustomeraddress=str_replace('tandakurungbuka','(',$ecustomeraddress);
      $ecustomeraddress=str_replace('tandakurungtutup',')',$ecustomeraddress);
      $ecustomeraddress=str_replace('tandatikma',';',$ecustomeraddress);
      $ecustomeraddress=str_replace('tandatikwa',':',$ecustomeraddress);
      $ecustomeraddress=str_replace('tandakoma',',',$ecustomeraddress);
      $ecustomeraddress=str_replace('tandagaring','/',$ecustomeraddress);
      $ecustomeraddress=str_replace('tandadan','&',$ecustomeraddress);
      $ecustomeraddress=str_replace('kosong','',$ecustomeraddress);
      $vgross=$this->uri->segment(12);
      $vdiscount=$this->uri->segment(13);
      $vnetto=$this->uri->segment(14);
      $vsisa=$this->uri->segment(15);
	    $drefference=$this->uri->segment(16);
      $nttbdiscount1=$this->uri->segment(17);
      $nttbdiscount2=$this->uri->segment(18);
      $nttbdiscount3=$this->uri->segment(19);
      $vttbdiscount1=$this->uri->segment(20);
      $vttbdiscount2=$this->uri->segment(21);
      $vttbdiscount3=$this->uri->segment(22);
      $dkn=str_replace('%20',' ',$this->uri->segment(23));
      $icustomergroupar=$this->uri->segment(24);
      $data['irefference']=$irefference;
      $data['icustomer']=$icustomer;
      $data['ecustomername']=$ecustomername;
      $data['iarea']=$iarea;
      $data['isalesman']=$isalesman;
      $data['esalesmanname']=$esalesmanname;
      $data['eareaname']=$eareaname;
      $data['ecustomeraddress']=$ecustomeraddress;
      $data['vgross']=$vgross;
      $data['vdiscount']=$vdiscount;
      $data['vnetto']=$vnetto;
      $data['vsisa']=$vsisa;
      $data['nttbdiscount1']=$nttbdiscount1;
      $data['nttbdiscount2']=$nttbdiscount2;
      $data['nttbdiscount3']=$nttbdiscount3;
      $data['vttbdiscount1']=$vttbdiscount1;
      $data['vttbdiscount2']=$vttbdiscount2;
      $data['vttbdiscount3']=$vttbdiscount3;
      $data['drefference']=$drefference;
      $data['icustomergroupar']=$icustomergroupar;
      $data['dkn']=$dkn;
			$data['jml']=$this->mmaster->jmldetail($irefference);
			$data['detail']=$this->mmaster->bacabbmdetail($irefference);
			$this->load->view('knretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function pajak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
	    ($this->session->userdata('menu111')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cust = $this->uri->segment(4);
			$prod= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/knretur/cform/pajak/'.$cust.'/'.$prod.'/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" select a.i_nota
																	from tm_nota a, tr_customer b, tm_nota_item c
																	where a.i_customer=b.i_customer 
																	and a.i_nota=c.i_nota and a.i_area=c.i_area
																	and a.f_ttb_tolak='f' and a.f_nota_koreksi='f' 
																	and not a.i_nota isnull and a.i_customer='$cust' 
																	and upper(c.i_product) like '%$prod%'
                                  and (upper(b.i_customer) like '%$cari%' 
                                  or upper(b.e_customer_name) like '%$cari%' 
							                    or upper(c.i_product) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('knretur/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->bacapajak($prod,$cust,$cari,$config['per_page'],$this->uri->segment(6));
			$data['cust']=$cust;
			$data['prod']=$prod;
			$this->load->view('knretur/vlistpajak', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
