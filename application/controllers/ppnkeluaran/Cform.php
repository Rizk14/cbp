<?php 
class Cform extends CI_Controller
{
	public $folder = 'pajak';

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu351') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('ppnkeluaran');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('ppnkeluaran/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu351') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-daftarnota/mmaster');
			$iarea  	= $this->input->post('iarea');
			$iperiode	= $this->input->post('iperiode');
			$a = substr($iperiode, 2, 2);
			$b = substr($iperiode, 4, 2);
			$peri = mbulan($b) . " - " . $a;
			if ($iarea == 'NA') {
				$no = 'FP-' . $a . $b . '-%';
			} else {
				$no = 'FP-' . $a . $b . '-' . $iarea . '%';
			}
			$this->db->select(" a.i_nota, a.d_nota, a.d_jatuh_tempo, a.i_customer, b.e_customer_name, a.i_salesman, a.i_faktur_komersial, b.f_customer_pkp,
								a.i_seri_pajak, a.d_pajak, a.v_nota_gross, a.v_nota_discount, a.v_nota_discounttotal, a.v_nota_ppn, 
								a.v_nota_gross, a.v_nota_netto, a.v_nota_discount1, a.v_nota_discount2, a.v_nota_discount3, a.v_nota_discount4,
								d.e_customer_pkpname, d.e_customer_pkpaddress, d.e_customer_pkpnpwp, c.f_spb_pkp,
								n_tax / 100 + 1 excl_divider,
								n_tax / 100 n_tax_val,
								x.n_tax
								from tm_nota a
								left join tr_tax_amount x on a.d_nota between x.d_start and x.d_finish, tm_spb c, tr_customer b
								left join tr_customer_pkp d on(b.i_customer=d.i_customer) 
								where to_char(a.d_nota,'yyyymm') = '$iperiode' and a.i_customer=b.i_customer and a.i_spb=c.i_spb and a.i_area=c.i_area
								and not a.i_faktur_komersial isnull 
								--and a.n_faktur_komersialprint>0 
								and not a.i_seri_pajak isnull and a.f_nota_cancel='f'
								order by a.i_area, a.d_nota, a.i_nota", false);
			#and a.n_faktur_komersialprint>0
			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("PPN Keluaran")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(24);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(24);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(14);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A1:S1'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'KODETRAN,C,2');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'NAMADGU,C,40');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'NPWPDGU,C,20');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'NODOK,C,7');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'TGLDOK,D');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'KODELANG,C,5');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'NAMALANG,C,25');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'KODESALES,C,2');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'NOFAKTUR,C,6');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'NOSERI,C,20');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K1', 'TGLPJK,D');
				$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L1', 'GROSS,N,10,0');
				$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M1', 'POTONG,N,10,0');
				$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N1', 'DPP,N,10,0');
				$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O1', 'PPN,N,10,0');
				$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P1', 'NET,N,10,0');
				$objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'NAMAPKP,C,40');
				$objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('R1', 'ALAMATPKP,C,90');
				$objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('S1', 'NPWP');
				$objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i = 2;
				foreach ($query->result() as $row) {
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':S' . $i
					);

					if ($row->d_nota != '') {
						$tgl = $row->d_nota;
						$tmp = explode('-', $row->d_nota);
						$hr = $tmp[2];
						$bl = $tmp[1];
						$th = $tmp[0];
						$row->d_nota = $hr . '-' . $bl . '-' . $th;
					}
					if ($row->d_pajak != '') {
						$tmp = explode('-', $row->d_pajak);
						$hr = $tmp[2];
						$bl = $tmp[1];
						$th = $tmp[0];
						$row->d_pajak = $hr . '-' . $bl . '-' . $th;
					}
					// if ($tgl > '2022-03-31') {
					// 	$row->v_nota_ppn = floor($row->v_nota_netto / 1.11 * 0.11);
					// } else {
					// 	$row->v_nota_ppn = floor($row->v_nota_netto / 1.1 * 0.1);
					// }
					// $row->v_nota_dpp = $row->v_nota_netto - $row->v_nota_ppn;

					if ($tgl > '2022-03-31') {
						/* AMBIL TOTAL DPP PPN DARI ITEM (12 JUL 2022) */
						$getdetail = $this->db->query(" SELECT 
															sum(n_deliver * v_unit_price) AS v_gross, sum(v_nota_discount) AS v_nota_discount,
															sum(v_dpp) AS v_dpp, sum(v_ppn) AS v_ppn, sum(v_netto) AS v_netto
														FROM tm_nota_item
														WHERE i_nota = '$row->i_nota' ");

						foreach ($getdetail->result() as $riw) {
							$row->v_nota_gross 		= $riw->v_gross;
							$row->v_nota_discount 	= $riw->v_nota_discount;
							$row->v_nota_dpp 		= $riw->v_dpp;
							$row->v_nota_ppn 		= $riw->v_ppn;
							$row->v_nota_netto 		= $riw->v_netto;
						}
					} else {
						/* AMBIL TOTAL DPP PPN DARI TOTAL NOTA (12 JUL 2022) */
						$row->v_nota_ppn = round($row->v_nota_netto / $row->excl_divider * $row->n_tax_val);
						$row->v_nota_dpp = round($row->v_nota_netto / $row->excl_divider);
					}

					if ($row->f_customer_pkp == 't') {
						$kodetran = '04';
					} else {
						$kodetran = '07';
					}

					if ($row->v_nota_discount != $row->v_nota_discounttotal) {
						$row->v_nota_discount = $row->v_nota_discount1 + $row->v_nota_discount2 + $row->v_nota_discount3 + $row->v_nota_discount4;
					}

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $kodetran, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, NmPerusahaan, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, NPWPPerusahaan, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_nota, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->d_nota, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->i_customer, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->i_salesman, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row->i_faktur_komersial, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $row->i_seri_pajak, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . $i, $row->d_pajak, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('L' . $i, $row->v_nota_gross, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('M' . $i, $row->v_nota_discount, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('N' . $i, $row->v_nota_dpp, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('O' . $i, $row->v_nota_ppn, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('P' . $i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('Q' . $i, $row->e_customer_pkpname, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('R' . $i, $row->e_customer_pkpaddress, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('S' . $i, $row->e_customer_pkpnpwp, Cell_DataType::TYPE_STRING);
					$i++;
				}
				$x = $i - 1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$tglskrg 		= date("YmdHis");
			$nama = 'ppnkeluaran_' . $iarea . '_' . $iperiode . '-' . $tglskrg . '.xls';
			if (file_exists('pajak/' . $nama)) {
				@chmod('pajak/' . $nama, 0777);
				@unlink('pajak/' . $nama);
			}
			$objWriter->save('pajak/' . $nama);
			@chmod('pajak/' . $nama, 0777);

			/* $sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if ($rs->num_rows > 0) {
				foreach ($rs->result() as $tes) {
					$ip_address	  = $tes->ip_address;
					break;
				}
			} else {
				$ip_address = 'kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			} 
			$pesan = 'Export PPN Keluaran Area ' . $iarea . ' Periode:' . $iperiode;
			$this->load->model('logger');*/

			$this->logger->writenew('Export PPN Keluaran Area ' . $iarea . ' Periode:' . $iperiode);

			$data['sukses'] = true;
			$data['inomor']	= 'ppnkeluaran_' . $iarea . '_' . $iperiode . '-' . $tglskrg . '.xls';
			$data['folder']	= $this->folder;

			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu351') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/ppnkeluaran/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('ppnkeluaran/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('ppnkeluaran/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu351') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url() . 'index.php/ppnkeluaran/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if ($area1 == '00' or $area2 == '00' or $area3 == '00' or $area4 == '00' or $area5 == '00') {
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')", false);
			} else {
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('ppnkeluaran/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('ppnkeluaran/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
