<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu412')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('ubahsalesspb');
			$data['dfrom']='';
			$data['dto']='';
			$data['iarea']='';
      $data['ispb']='';
			$this->load->view('ubahsalesspb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu412')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('ubahsalesspb');
			$this->load->view('ubahsalesspb/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu412')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			  $ispb	= $this->uri->segment(4);
			  $iarea= $this->uri->segment(5);
			  $dfrom= $this->uri->segment(6);
			  $dto	= $this->uri->segment(7);
			  $this->load->model('ubahsalesspb/mmaster');
			  $this->mmaster->delete($ispb,$iarea);

				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Delete SPB No:'.$ispb.' Area:'.$iarea;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );

   		  $is_cari = $this->input->post('is_cari'); 
			  $cari	= strtoupper($this->input->post('cari'));
			  $config['base_url'] = base_url().'index.php/ubahsalesspb/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
  			  $sql	= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
				where a.i_customer=b.i_customer
				and a.i_area='$iarea' and
				(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
				a.d_spb <= to_date('$dto','dd-mm-yyyy')) order by a.i_spb desc ";
			  $query = $this->db->query($sql,false);

			  $config['total_rows'] = $query->num_rows();
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(8);
			  $this->pagination->initialize($config);
			
			  $this->load->model('ubahsalesspb/mmaster');
			  $data['page_title'] = $this->lang->line('ubahsalesspb');
			  $data['cari']	= $cari;
			  $data['dfrom']	= $dfrom;
			  $data['dto']	= $dto;
			  $data['iarea'] = $iarea;
			  $data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			  $this->load->view('ubahsalesspb/vmainform',$data);
 		}else{
  		  $this->load->view('awal/index.php');
	  }
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu412')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
			if ($cari == '') {
				$cari=$this->uri->segment(7);
			}
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea'); 
			if($dfrom=='') $dfrom = $this->uri->segment(4);
			if($dto=='') $dto = $this->uri->segment(5);
			if($iarea=='') $iarea = $this->uri->segment(6);
			
			$config['base_url'] = base_url().'index.php/ubahsalesspb/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/index/';
			
						
			$sql= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%') 
					and a.i_area='$iarea' and
						(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and
						a.d_spb <= to_date('$dto','dd-mm-yyyy')) order by a.i_spb desc ";
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			
			$this->load->model('ubahsalesspb/mmaster');
			$data['page_title'] = $this->lang->line('ubahsalesspb');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('ubahsalesspb/vmainform',$data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function paging() 
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu412')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($this->uri->segment(5)!=''){
				if($this->uri->segment(4)!='sikasep'){
					$cari=$this->uri->segment(4);
				}else{
					$cari='';
				}
			}elseif($this->uri->segment(4)!='sikasep'){
				$cari=$this->uri->segment(4);
			}else{
				$cari='';
			}
			if($cari=='')
				$config['base_url'] = base_url().'index.php/ubahsalesspb/cform/paging/sikasep/';
			else
				$config['base_url'] = base_url().'index.php/ubahsalesspb/cform/paging/'.$cari.'/';
			if($this->uri->segment(4)=='sikasep')
				$cari='';
			$cari	= $this->input->post('cari', TRUE);
			$cari  	= strtoupper($cari);
			if($this->session->userdata('level')=='0'){
				$sql= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
						where a.i_customer=b.i_customer
						and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						or upper(a.i_spb) like '%$cari%') order by a.i_spb desc ";
			}else{
				$sql= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
						where a.i_customer=b.i_customer
						and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						or upper(a.i_spb) like '%$cari%')
						and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
						or a.i_area='$area5') order by a.i_spb desc ";
			}
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('ubahsalesspb');
			$this->load->model('ubahsalesspb/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('ubahsalesspb/vform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu412')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/ubahsalesspb/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
        else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('ubahsalesspb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('ubahsalesspb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu412')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/ubahsalesspb/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('ubahsalesspb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('ubahsalesspb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu412')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			$is_cari= $this->input->post('is_cari');
				
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea=$this->uri->segment(6);
				
			if ($is_cari == '')
				$is_cari= $this->uri->segment(8);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(7);
			
			if ($is_cari=="1") { 
				$config['base_url'] = base_url().'index.php/ubahsalesspb/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/'.$is_cari.'/index/';
			}
			else { 
				$config['base_url'] = base_url().'index.php/ubahsalesspb/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			} 
			
			if ($is_cari != "1") {
				$sql= " select a.i_spb
                from tm_spb a 
								left join tr_customer b on(a.i_customer=b.i_customer)
								left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
								, tr_area c
                where 
                a.i_area=c.i_area and a.i_sj isnull and a.f_spb_cancel='f' and
                a.i_area='$iarea' and
                (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                a.d_spb <= to_date('$dto','dd-mm-yyyy'))";
			}
			else {
				$sql= " select a.i_spb
                from tm_spb a 
								left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
								left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
								, tr_area c
                where 
                a.i_area=c.i_area and a.i_sj isnull and a.f_spb_cancel='f' and
                a.i_area='$iarea' and
                (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                a.d_spb <= to_date('$dto','dd-mm-yyyy')) AND
                (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                or upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')";
			}
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if ($is_cari=="1")
				$config['cur_page'] = $this->uri->segment(10);
			else
				$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			
			$this->load->model('ubahsalesspb/mmaster');
			$data['page_title'] = $this->lang->line('ubahsalesspb');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;
      $data['ispb']='';

			if ($is_cari=="1")
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(10),$cari);
			else
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
/*
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SPB Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
*/
			$this->load->view('ubahsalesspb/vmainform',$data);			

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu412')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('xcari'));
			$dfrom	= $this->input->post('xdfrom');
			$dto	= $this->input->post('xdto');
			$iarea	= $this->input->post('xiarea');
			$is_cari= $this->input->post('xis_cari'); 
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea=$this->uri->segment(6);
			if ($is_cari == '')
				$is_cari= $this->uri->segment(8);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(7);
			$this->load->model('ubahsalesspb/mmaster');
			$data['page_title'] = $this->lang->line('ubahsalesspb');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;

      $this->db->select("	a.i_customer, a.f_spb_stockdaerah, a.d_spb, a.f_spb_cancel, a.i_approve1, a.i_notapprove, a.i_approve2,
                          a.i_store, a.i_nota, a.f_spb_siapnotagudang, a.f_spb_op, a.f_spb_opclose, a.f_spb_siapnotasales,
                          a.i_sj, a.v_spb, a.v_spb_discounttotal, a.i_spb, a.d_spb, a.i_salesman,
                          a.i_area, a.i_spb_old, a.i_spb_program, a.i_product_group, a.i_spb_program, a.i_price_group,
                          b.e_customer_name, c.e_area_name, d.i_dkb, x.e_customer_name as xname
                          from tm_spb a 
									        left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
									        left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
									        left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area and x.i_customer like '%000')
									        , tr_area c
                          where 
                            a.i_area=c.i_area and					
                            a.i_area='$iarea' and
                            (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                            a.d_spb <= to_date('$dto','dd-mm-yyyy'))
                          order by a.i_spb ",false);
      $query = $this->db->get();
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$queryy 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($queryy)){
	    	$now	  = $row['c'];
			}
			$pesan='Export Daftar SPB Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

#####
      $this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar SPB ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      if ($query->num_rows() > 0){
     		$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar SPB');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,9,2);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:J6'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Tanggal');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Sales');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Lang');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Kotor');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Discount');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Bersih');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Status');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Daerah');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        $i=7;
				$j=7;
        $no=0;
				foreach($query->result() as $row){
          $no++;
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':J'.$i
				  );
    
          if(
				 	  ($row->f_spb_cancel == 't') 
					 ){
			    	$status='Batal';
			    }elseif(
				   	  ($row->i_approve1 == null) && ($row->i_notapprove == null)
					   ){
			    	$status='Sales';
			    }elseif(
					    ($row->i_approve1 == null) && ($row->i_notapprove != null)
					   ){
			    	$status='Reject (sls)';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 == null) &
					    ($row->i_notapprove == null)
					   ){
			    	$status='Keuangan';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 == null) && 
					    ($row->i_notapprove != null)
					   ){
			    	$status='Reject (ar)';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store == null)
					   ){
			    	$status='Gudang';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					    ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
					   ){
			    	$status='Pemenuhan SPB';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					    ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
					   ){
			    	$status='Proses OP';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					    ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
					   ){
			    	$status='OP Close';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					    ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
					   ){
			    	$status='Siap SJ (sales)';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
					    ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					   ){
  #			  	$status='Siap SJ (gudang)';
			    	$status='Siap SJ';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					    ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
					   ){
			    	$status='Siap SJ';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					    ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					   ){
			    	$status='Siap DKB';
          }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
					    ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
					   ){
			    	$status='Siap Nota';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && 
					    ($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
					   ){
			    	$status='Siap SJ';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) && 
					    ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					   ){
			    	$status='Siap DKB';
			    }elseif(
					    ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
			    		  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) && 
					    ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
					   ){
			    	$status='Siap Nota';
			    }elseif(
					    ($row->i_approve1 != null) && 
			    		  ($row->i_approve2 != null) &&
			     		  ($row->i_store != null) && 
					    ($row->i_nota != null) 
					   ){
			    	$status='Sudah dinotakan';			  
			    }elseif(($row->i_nota != null)){
			    	$status='Sudah dinotakan';
			    }else{
			    	$status='Unknown';		
			    }
          $bersih	= $row->v_spb-$row->v_spb_discounttotal;
#          $bersih	= number_format($row->v_spb-$row->v_spb_discounttotal);
#			    $row->v_spb	= number_format($row->v_spb);
#   			  $row->v_spb_discounttotal	= number_format($row->v_spb_discounttotal);  
          if($row->f_spb_stockdaerah=='t')
          {
            $daerah='Ya';
          }else{
            $daerah='Tidak';
          }

					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row->i_spb);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->d_spb);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->i_salesman);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, '('.$row->i_customer.') '.$row->e_customer_name);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, "'".$row->i_area);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->v_spb);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->v_spb_discounttotal);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $bersih);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $status);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $daerah);
					$i++;
				}
			}
			$objPHPExcel->getActiveSheet()->getStyle('F7:H'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='SPB'.$iarea.'.xls';
      if(file_exists('spb/'.$iarea.'/'.$nama)){
        @chmod('spb/'.$iarea.'/'.$nama, 0777);
        @unlink('spb/'.$iarea.'/'.$nama);
      }
			$objWriter->save("spb/".$iarea.'/'.$nama); 
      @chmod('spb/'.$iarea.'/'.$nama, 0777);
      echo $nama;
#####
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function edit()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         (($this->session->userdata('menu412')=='t'))) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('ubahsalesspb');
         if(
            ($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
           )
         {
            $ispb = $this->uri->segment(4);
            $iarea= $this->uri->segment(5);

            // desta 21-12-2010
            $dfrom   = $this->uri->segment(6);
            $dto  = $this->uri->segment(7);
            $ipricegroup   = $this->uri->segment(8);

            $query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
            $data['jmlitem']= $query->num_rows();
            $data['ispb']  = $ispb;
            $data['departement']=$this->session->userdata('departement');
            $this->load->model('ubahsalesspb/mmaster');
            $data['isi']   = $this->mmaster->baca($ispb,$iarea);
            $data['detail']   = $this->mmaster->bacadetail($ispb,$iarea,$ipricegroup);

            $qnilaispb  = $this->mmaster->bacadetailnilaispb($ispb,$iarea,$ipricegroup);
            if($qnilaispb->num_rows()>0){
               $row_nilaispb  = $qnilaispb->row();
               $data['nilaispb'] = $row_nilaispb->nilaispb;
            }else{
               $data['nilaispb'] = 0;
            }
            $qnilaiorderspb   = $this->mmaster->bacadetailnilaiorderspb($ispb,$iarea,$ipricegroup);
            if($qnilaiorderspb->num_rows()>0){
               $row_nilaiorderspb   = $qnilaiorderspb->row();
               $data['nilaiorderspb']  = $row_nilaiorderspb->nilaiorderspb;
            }else{
               $data['nilaiorderspb']  = 0;
            }
            $qeket   = $this->db->query(" SELECT e_remark1 as keterangan from tm_spb where i_spb ='$ispb' and i_area='$iarea' ");
            if($qeket->num_rows()>0){
               $row_eket   = $qeket->row();
               $data['keterangan']  = $row_eket->keterangan;
            }

            //
            $data['dfrom'] = $dfrom;
            $data['dto'] = $dto;
            $data['ispb'] = $ispb;
            $data['iarea'] = $iarea;

            $this->load->view('ubahsalesspb/vmainform',$data);
         }else{
            $this->load->view('ubahsalesspb/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');

      }
   }
   function update()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu412')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb    = $this->input->post('ispb', TRUE);
         $isalesman        = $this->input->post('isalesman',TRUE);
         $iarea            = $this->input->post('iarea',TRUE);
         if(($ispb!='') && ($iarea!=''))
         {
            $this->db->trans_begin();
            $this->load->model('ubahsalesspb/mmaster');
            $this->mmaster->updateheader($ispb, $iarea, $isalesman);
            if ( ($this->db->trans_status() === FALSE) )
            {
                $this->db->trans_rollback();
            }else{
                $this->db->trans_commit();

               $sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
                  $now    = $row['c'];
               }
               $pesan='Update Kode sales SPB Area '.$iarea.' No:'.$ispb;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );

               $data['sukses']         = true;
               $data['inomor']         = $ispb;
               $this->load->view('nomor',$data);
            }
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function salesman()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu412')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
          $iarea = strtoupper($this->input->post('iarea', FALSE));
          $dspb  = strtoupper($this->input->post('dspb', FALSE));
          if($iarea=='') $iarea=$this->uri->segment(4);
          if($dspb=='') $dspb=$this->uri->segment(5);
          $per='';
          if($dspb!=''){
            $tmp=explode('-',$dspb);
            $yy=$tmp[2];
            $bl=$tmp[1];
            $per=$yy.$bl;
          }

         $config['base_url'] = base_url().'index.php/ubahsalesspb/cform/salesman/'.$iarea.'/'.$dspb.'/';
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         $cari = strtoupper($this->input->post('cari', FALSE));
         $query = $this->db->query("select distinct i_salesman, e_salesman_name from tr_customer_salesman
                                    where (upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%')
                                    and i_area='$iarea' and e_periode='$per' ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $data['iarea']=$iarea;
         $data['dspb']=$dspb;
         $this->load->model('ubahsalesspb/mmaster');
         $data['page_title'] = $this->lang->line('list_salesman');
         $data['isi']=$this->mmaster->bacasalesman($iarea,$per,$cari,$area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(6));
         $this->load->view('ubahsalesspb/vlistsalesman', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
