<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu62')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printspb');
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['cari'] = '';
      $data['dfrom']= '';
			$data['dto']  = '';
			$data['spbfrom']='';
			$data['spbto']	='';
			$this->load->view('printspbkelompok/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu62')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printspbkelompok/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('printspbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printspbkelompok/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu62')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printspbkelompok/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printspbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printspbkelompok/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function spbfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu62')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	  = strtoupper($this->input->post('cari'));
			$dfrom	= strtoupper($this->input->post('dfrom'));
			$dto	  = strtoupper($this->input->post('dto'));
			$iarea  = strtoupper($this->input->post('iarea'));
      if($dfrom=='') $dfrom  =$this->uri->segment(4);
      if($dto=='') $dto=$this->uri->segment(5);
      if($iarea=='') $iarea=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printspbkelompok/cform/spbfrom/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$query = $this->db->query("	select a.i_spb, b.e_customer_name from tm_spb a, tr_customer b
										              where (upper(a.i_spb) like '%$cari%')
                                  and a.i_customer=b.i_customer
										              and a.i_area='$iarea ' and (a.n_print=0 or a.n_print isnull)
                                  and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
                                  and a.d_spb <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);
			$this->load->model('printspbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('listspb');
			$data['dfrom'] = $dfrom;
			$data['dto'] = $dto;
			$data['iarea'] = $iarea;
			$data['isi']=$this->mmaster->bacaspb($cari,$iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(7));
			$this->load->view('printspbkelompok/vlistspbfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function spbto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu62')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	  = strtoupper($this->input->post('cari'));
			$dfrom	= strtoupper($this->input->post('dfrom'));
			$dto	  = strtoupper($this->input->post('dto'));
			$iarea  = strtoupper($this->input->post('iarea'));
      if($dfrom=='') $dfrom  =$this->uri->segment(4);
      if($dto=='') $dto=$this->uri->segment(5);
      if($iarea=='') $iarea=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printspbkelompok/cform/spbto/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$query = $this->db->query("	select a.i_spb, b.e_customer_name from tm_spb a, tr_customer b
										where (upper(a.i_spb) like '%$cari%') and a.i_customer=b.i_customer
										and a.i_area='$iarea ' and (a.n_print=0 or a.n_print isnull)
                    and a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and a.d_spb <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);
			$this->load->model('printspbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('listspb');
			$data['dfrom'] = $dfrom;
			$data['dto']   = $dto;
			$data['iarea'] = $iarea;
			$data['isi']=$this->mmaster->bacaspb($cari,$iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(7));
			$this->load->view('printspbkelompok/vlistspbto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu62')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	= $this->input->post('iarea');
			$spbfrom= $this->input->post('spbfrom');
			$spbto	= $this->input->post('spbto');
			$this->load->model('printspbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('listspb');
			$data['master']=$this->mmaster->bacamaster($area,$spbfrom,$spbto);
#      $this->mmaster->close($area,$spbfrom,$spbto);
			$data['area']=$area;
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak SPB Area:'.$area.' No:'.$spbfrom.' s/d No:'.$spbto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printspbkelompok/vformrpt',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
