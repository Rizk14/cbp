<?php 
class Cform extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
    }
    public function index()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu409') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $data['page_title'] = $this->lang->line('exp-kb');
            $data['datefrom'] = '';
            $data['dateto'] = '';
            $this->load->view('exp-kball/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function export()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu409') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $this->load->model('exp-kball/mmaster');
            $dfrom = $this->input->post('datefrom');
            $dto = $this->input->post('dateto');

            if ($dfrom != '' && $dto != '') {

                $query = $this->db->query(" SELECT * FROM (
                                                SELECT
                                                    x.e_coa_name,
                                                    x.d_kb,
                                                    x.d_bukti,
                                                    CASE WHEN NOT i_rv ISNULL THEN c.i_rv 
                                                    ELSE d.i_pv 
                                                    END AS i_reff,
                                                    x.i_kb,
                                                    x.e_description,
                                                    x.i_area || ' - ' || b.e_area_name AS i_area,
                                                    x.i_coa,
                                                    x.d_entry,
                                                    sum(x.debet) AS debet,
                                                    sum(x.kredit) AS kredit
                                                FROM
                                                    (
                                                        SELECT
                                                            d_kb,
                                                            d_bukti,
                                                            i_kb,
                                                            e_description,
                                                            i_area,
                                                            i_coa,
                                                            e_coa_name,
                                                            0 AS debet,
                                                            v_kb AS kredit,
                                                            d_entry
                                                        FROM
                                                            tm_kb
                                                        WHERE
                                                            d_kb >= to_date('$dfrom', 'dd-mm-yyyy')
                                                            AND d_kb <= to_date('$dto', 'dd-mm-yyyy')
                                                            AND f_debet = 't'
                                                            AND f_kb_cancel = 'f'
                                                    UNION ALL
                                                        SELECT
                                                            d_kb,
                                                            d_bukti,
                                                            i_kb,
                                                            e_description,
                                                            i_area,
                                                            i_coa,
                                                            e_coa_name,
                                                            v_kb AS debet,
                                                            0 AS kredit,
                                                            d_entry
                                                        FROM
                                                            tm_kb
                                                        WHERE
                                                            d_kb >= to_date('$dfrom', 'dd-mm-yyyy')
                                                            AND d_kb <= to_date('$dto', 'dd-mm-yyyy')
                                                            AND f_debet = 'f'
                                                            AND f_kb_cancel = 'f'
                                                    ) AS x
                                                INNER JOIN tr_area b ON (x.i_area = b.i_area)
                                                LEFT JOIN tm_rv_item c ON (x.i_kb = c.i_kk AND x.i_area = c.i_area_kb)
                                                LEFT JOIN tm_pv_item d ON (x.i_kb = d.i_kk AND x.i_area = d.i_area_kb)
                                                GROUP BY
                                                    x.e_coa_name,
                                                    x.d_kb,
                                                    x.i_kb,
                                                    x.e_description,
                                                    x.i_area,
                                                    x.i_coa,
                                                    b.e_area_name,
                                                    x.d_bukti,
                                                    c.i_rv,
                                                    d.i_pv,
                                                    x.d_entry
                                            ) AS a
                                            ORDER BY
                                                a.d_kb, a.d_entry, a.i_reff ");
                if ($query->num_rows() > 0) {
                    $this->load->library('PHPExcel');
                    $this->load->library('PHPExcel/IOFactory');
                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getProperties()->setTitle("Kas Besar")->setDescription(NmPerusahaan);

                    $objPHPExcel->setActiveSheetIndex(0);
                    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                        array(
                            'font' => array(
                                'name' => 'Arial',
                                'bold' => true,
                                'italic' => false,
                                'size' => 11,
                            ),
                            'alignment' => array(
                                'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => Style_Alignment::VERTICAL_CENTER,
                                'wrap' => true,
                            ),
                        ),
                        'A2:A4'
                    );

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);

                    $objPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN KAS BESAR ' . NmPerusahaan);
                    $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 13, 2);
                    $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Tanggal  : ' . $dfrom . ' s/d ' . $dto);
                    $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 13, 3);

                    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                        array(
                            'font' => array(
                                'name' => 'Arial',
                                'bold' => true,
                                'italic' => false,
                                'size' => 11,
                            ),
                            'alignment' => array(
                                'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => Style_Alignment::VERTICAL_CENTER,
                                'wrap' => true,
                            ),
                            'borders' => array(
                                'top' => array('style' => Style_Border::BORDER_THIN),
                                'bottom' => array('style' => Style_Border::BORDER_THIN),
                                'left' => array('style' => Style_Border::BORDER_THIN),
                                'right' => array('style' => Style_Border::BORDER_THIN),
                            ),
                        ),

                        
                        'A5:K5'
                    );
                    $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
                    $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Tgl Trans');
                    $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tgl Bukti');
                    $objPHPExcel->getActiveSheet()->setCellValue('D5', 'No Reff');
                    $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Keterangan');
                    $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Area');
                    $objPHPExcel->getActiveSheet()->setCellValue('G5', 'No COA');
                    $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Nama COA');
                    $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Debet');
                    $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Kredit');
                    $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Saldo');

                    $i = 7;
					$no = 1;
					if ($dfrom) {
						$tmp = explode('-', $dfrom);
						$tgl = $tmp[2];
						$bln = $tmp[1];
						$thn = $tmp[0];
						$periode = $tgl.$bln;
					}

					$cek_saldo = $this->db->query("select v_saldo_awal from tm_coa_saldo
					where i_periode='$periode' and i_coa = '110-11000'");
					if($cek_saldo->num_rows() > 0){
						$saldo_awal = $cek_saldo->row();
						$saldo = $saldo_awal->v_saldo_awal;

						$cek_kedua = $this->db->query("select sum(x.debet) as debet, sum(x.kredit) as kredit from(
							select d_kb, d_bukti, i_kb, e_description, i_area, i_coa, e_coa_name, 0 as debet, v_kb as kredit from tm_kb where
							d_kb >= to_date('$periode', 'yyyymm') and d_kb < to_date('$dfrom', 'dd-mm-yyyy') and f_debet = 't' and f_kb_cancel = 'f'
							union all
							select d_kb, d_bukti, i_kb, e_description, i_area, i_coa, e_coa_name, v_kb as debet,  0 as kredit from tm_kb where
							d_kb >= to_date('$periode', 'yyyymm') and d_kb < to_date('$dfrom', 'dd-mm-yyyy') and f_debet = 'f' and f_kb_cancel = 'f'
							) as x");
						
							if($cek_kedua->num_rows() > 0){
								$cekk = $cek_kedua->row();

								$debet = $cekk->debet;
								$kredit = $cekk->kredit;
								
							}else{
								$debet = 0;
								$kredit = 0;
							}
							$saldo = $saldo + $debet - $kredit;
					}else{
						echo "saldo awal belum ada";
						die;
					}
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'borders' => array(
								'top' => array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left' => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN),
							),
							'font' 	=> array(
								'name' 	=> 'Arial',
								'bold'  => false,
								'italic'=> false,
								'size'  => 10
							),
						),
						'A6:K6'
					);

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A6', '', Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B6', '', Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C6', '', Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D6', '', Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E6', '', Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F6', '', Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('G6', '', Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('H6', '', Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('I6', '', Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('J6', '', Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('K6', $saldo, Cell_DataType::TYPE_NUMERIC);

                    foreach ($query->result() as $row) {
						$saldo = $saldo + $row->debet - $row->kredit;

                        if ($row->d_kb) {
                            $tmp = explode('-', $row->d_kb);
                            $tgl = $tmp[2];
                            $bln = $tmp[1];
                            $thn = $tmp[0];
                            $row->d_kb = $tgl . '-' . $bln . '-' . $thn;
                        }
                        if ($row->d_bukti) {
                            $tmp = explode('-', $row->d_bukti);
                            $tgl = $tmp[2];
                            $bln = $tmp[1];
                            $thn = $tmp[0];
                            $row->d_bukti = $tgl . '-' . $bln . '-' . $thn;
                        } else {
                            $row->d_bukti = '';
                        }

                        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                            array(
                                'borders' => array(
                                    'top' => array('style' => Style_Border::BORDER_THIN),
                                    'bottom' => array('style' => Style_Border::BORDER_THIN),
                                    'left' => array('style' => Style_Border::BORDER_THIN),
                                    'right' => array('style' => Style_Border::BORDER_THIN),
								),
								'font' 	=> array(
									'name' 	=> 'Arial',
									'bold'  => false,
									'italic'=> false,
									'size'  => 10
								),
                            ),
                            'A' . $i . ':K' . $i
                        );

                        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $no, Cell_DataType::TYPE_NUMERIC);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->d_kb, Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->d_bukti, Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_reff, Cell_DataType::TYPE_STRING);
                        // if($row->debet <> 0){
                            // $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_rv, Cell_DataType::TYPE_STRING);
                        // }else{
                            // $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_pv, Cell_DataType::TYPE_STRING);
                        // }
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->e_description, Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->i_area, Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->i_coa, Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->e_coa_name, Cell_DataType::TYPE_STRING);
                        $objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row->debet, Cell_DataType::TYPE_NUMERIC);
						$objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $row->kredit, Cell_DataType::TYPE_NUMERIC);
						$objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . $i, $saldo, Cell_DataType::TYPE_NUMERIC);
                        $i++;
                        $no++;
                    }
                    $i = $i + 2;

                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, 'Tanggal Export : ' . date('d-m-Y H:i:s'), Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->mergeCells('A' . $i . ':D' . $i);

                    $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
                    $nama = 'KB ' . $dfrom . '.xls';
                    $objWriter->save('excel/00/' . $nama);
                    $data['sukses'] = true;
                    $data['inomor'] = "Export Kas Besar " . $nama;
                    $data['folder'] = "exp-kball";
                    $this->load->view('nomorexport', $data);
                } else {
                    $this->load->view('exp-kball/vformfail');
                }

            }
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function area()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu409') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $config['base_url'] = base_url() . 'index.php/exp-kball/cform/area/index/';
            $area1 = $this->session->userdata('i_area');
            $area2 = $this->session->userdata('i_area2');
            $area3 = $this->session->userdata('i_area3');
            $area4 = $this->session->userdata('i_area4');
            $area5 = $this->session->userdata('i_area5');
            if ($area1 == '00') {
                $query = $this->db->query("select * from tr_area", false);
            } else {
                $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									or i_area = '$area4' or i_area = '$area5'", false);
            }
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(5);
            $this->pagination->initialize($config);

            $this->load->model('exp-kball/mmaster');
            $data['page_title'] = $this->lang->line('list_area');
            $data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
            $this->load->view('exp-kball/vlistarea', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function cariarea()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu409') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $area1 = $this->session->userdata('i_area1');
            $area2 = $this->session->userdata('i_area2');
            $area3 = $this->session->userdata('i_area3');
            $area4 = $this->session->userdata('i_area4');
            $area5 = $this->session->userdata('i_area5');
            $config['base_url'] = base_url() . 'index.php/exp-kball/cform/area/index/';
            $cari = strtoupper($this->input->post('cari', false));
            if ($area1 == '00') {
                $query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')", false);
            } else {
                $query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2'
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')", false);
            }
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(5);
            $this->pagination->initialize($config);
            $this->load->model('exp-kk-lkhall/mmaster');
            $data['page_title'] = $this->lang->line('list_area');
            $data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
            $this->load->view('exp-kk-lkhall/vlistarea', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
}
