<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu457')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('laporanpenjualan').' Grup Konsinyasi';
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listpenjualangrupkonsinyasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu457')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto  		= $this->input->post('dto');
			if($dfrom=='') $dfrom	= $this->uri->segment(4);
			if($dto=='') $dto	= $this->uri->segment(5);
			$this->load->model('listpenjualangrupkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('laporanpenjualan').' Grup Konsinyasi';
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;

			$interval	= $this->mmaster->interval($dfrom,$dto);
      $data['interval'] = $interval;
			$data['isi']		  = $this->mmaster->bacaperiode($dfrom,$dto,$interval);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Penjualan Grup Konsinyasi Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listpenjualangrupkonsinyasi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu457')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('laporanpenjualan').' Grup Konsinyasi';
			$this->load->view('listpenjualangrupkonsinyasi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
