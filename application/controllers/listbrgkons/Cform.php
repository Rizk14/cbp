<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu448')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			  $iproduct			= $this->input->post('iproduct', TRUE);
			  $ipricegroup	= $this->input->post('ipricegroup', TRUE);

			  $cari=strtoupper($this->input->post("cari",false));
			  $config['base_url'] = base_url().'index.php/listbrgkons/cform/index/';
	      	  $query=$this->db->query(" select a.*, b.e_product_name, c.e_product_gradename
                                  from tr_product_priceco a, tr_product b, tr_product_grade c, tr_price_groupco d 
                                  where a.i_product=b.i_product and a.i_product_grade=c.i_product_grade 
                                  and a.i_price_groupco=d.i_price_groupco
                                  and (upper(a.i_product) like '%$cari%' 
                                  or upper(b.e_product_name) = '%$cari%')", false);
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
        		$this->pagination->initialize($config);
				$data['page_title'] = 'List Data Harga Barang Kosinyasi';
				$data['iproduct']='';
				$data['ipricegroup']='';
				$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);
				$this->load->model('listbrgkons/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Membuka Data Barang Konsinyasi No:'.$iproduct;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

				$this->load->view('listbrgkons/vmainform', $data);
			
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu448')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = 'Daftar Harga Barang Konsinyasi';
			$this->load->view('listbrgkons/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu448')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = 'Daftar Harga Barang Konsinyasi Update';
			if($this->uri->segment(4)){
				$iproduct    = $this->uri->segment(4);
				$ipricegroup = $this->uri->segment(5);
				$data['iproduct']       = $iproduct;
				$data['ipricegroup'] 		= $ipricegroup;
				$this->load->model('listbrgkons/mmaster');
				$data['isi']=$this->mmaster->baca($iproduct,$ipricegroup);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit List Data Harga Barang Kosinyasi ('.$iproduct.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);
		        
		 		$this->load->view('listbrgkons/vmainform',$data);
			}else{
				$this->load->view('listbrgkons/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu448')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	 	  = $this->input->post('iproduct');
			$eproductname   = $this->input->post('eproductname');
			$iproductgrade 	= $this->input->post('iproductgrade');
			$ipricegroup  	= $this->input->post('ipricegroup');
			$vproductmill	 	= $this->input->post('vproductmill');
			$vproductmill	  = str_replace(",","",$this->input->post('vproductmill', TRUE));
			if($vproductmill=='')
				$vproductmill=0;
			$vproductretail	= $this->input->post('vproductretail');
			$vproductretail = str_replace(",","",$this->input->post('vproductretail', TRUE));
			if($vproductretail=='')
				$vproductretail=0;
      $hitung=($vproductretail-$vproductmill)/$vproductretail;
      $nmargin=$hitung*100;	
			$this->load->model('listbrgkons/mmaster');
			$this->mmaster->update($iproduct,$eproductname,$iproductgrade,$ipricegroup,$vproductretail,$nmargin);
      if ( ($this->db->trans_status() === FALSE) )
      {
        $this->db->trans_rollback();
      }else{
        $this->db->trans_commit();
#             $this->db->trans_rollback();
        $data['sukses']         = true;
        $data['inomor']         = $iproduct;
        $this->load->view('nomor',$data);
      }
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Update Data Barang Konsinyasi No:'.$iproduct;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu448')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct    = $this->uri->segment(4);
			$ipricegroup = $this->uri->segment(5);
			$this->load->model('listbrgkons/mmaster');
			$this->mmaster->delete($iproduct,$ipricegroup);
			
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus Barang Konsinyasi No:'.$iproduct;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$config['base_url'] = base_url().'index.php/listbrgkons/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select a.*, b.e_product_name, c.e_product_gradename
                                  from tr_product_priceco a, tr_product b, tr_product_grade c
                                  where a.i_product=b.i_product and a.i_product_grade=c.i_product_grade
                                  and (upper(a.i_product) like '%$cari%' 
                                  or upper(b.e_product_name) = '%$cari%') ",false);
			$config['total_rows']	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link']	= 'Awal';
			$config['last_link']	= 'Akhir';
			$config['next_link']	= 'Selanjutnya';
			$config['prev_link']	= 'Sebelumnya';
			$config['cur_page']		= $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('listbrgkons/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = 'Daftar Harga Barang Konsinyasi';
			$data['iproduct']	= '';
			$data['ipricegroup']	= '';
	 		$this->load->view('listbrgkons/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu448')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listbrgkons/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
	    $query=$this->db->query(" select a.*, b.e_product_name, c.e_product_gradename
                                from tr_product_priceco a, tr_product b, tr_product_grade c
                                where a.i_product=b.i_product and a.i_product_grade=c.i_product_grade
                                and (upper(a.i_product) like '%$cari%' 
                                or upper(b.e_product_name) = '%$cari%')", false);
	    $config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('listbrgkons/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = 'Daftar Harga Barang Konsinyasi';
			$data['iproduct']	= '';
			$data['ipricegroup']	= '';
	 		$this->load->view('listbrgkons/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
