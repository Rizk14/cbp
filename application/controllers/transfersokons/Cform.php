<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
    require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu366')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('transfersokons/mmaster');
			$data['page_title'] = $this->lang->line('transfersokonscon');
			$store	= $this->session->userdata('store');
#			echo $store;die;
			$data['isi']= directory_map('./so/'.$store);
			$data['file']='';
      $ispg = $this->session->userdata('user_id');
      $data['cust']	= $this->mmaster->bacacust($ispg);
			$this->load->view('transfersokons/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu366')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$file= $this->input->post('namafile', TRUE);
			$tgl = $this->input->post('tgl', TRUE);
			$store	= $this->session->userdata('store');
			$data['file']='so/'.$store.'/'.$file;
			$data['tgl'] =$tgl;
			$this->load->view('transfersokons/vfile', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu366')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $filespb = $this->input->post('fileso', TRUE);
      $this->load->model('transfersokons/mmaster');
      $this->db->trans_begin();
      $db1 =dbase_open($filespb,2) or die ("Could not open dbf file <i>$filespb</i>.");
      $this->db->trans_begin();
      if($db1){
        $record_numbers1=dbase_numrecords($db1);
        $noitem=0;
        for($j=1;$j<=$record_numbers1;$j++){
          $noitem++;
          $col=dbase_get_record_with_names($db1,$j);
          $iproduct		  = substr($col['KODEPROD'],0,7);

          $que=$this->db->query("select e_product_name from tr_product where i_product='$iproduct'", false);
          if($que->num_rows()>0){
        		$tes=$que->row();
            $eproductname=$tes->e_product_name;
          }      
		      $iproductgrade	= 'A';
		      $iproductmotif	= '00';
          if(isset($col['STOCKOPNAM'])){
            if(trim($col['STOCKOPNAM'])=='') $col['STOCKOPNAM']=0;
            $nstockopname   = $col['STOCKOPNAM'];
          }else{
            if(trim($col['STOCKOPNAM'])=='') $col['STOCKOPNAM']=0;
            $nstockopname   = $col['STOCKOPNAM'];
          }
          if(isset($col['STOCKOPNAM'])){
            if($col['STOCKOPNAM']<0) $col['STOCKOPNAME']=0;
          }
		      $nstockopname		= str_replace(',','',$nstockopname);
          $dstockopname 	= $this->input->post('tglso', TRUE);
			    if($dstockopname!=''){
				    $tmp=explode("-",$dstockopname);
				    $th=$tmp[2];
				    $bl=$tmp[1];
				    $hr=$tmp[0];
				    $dstockopname=$th."-".$bl."-".$hr;
            $thbl=substr($th,2,2).$bl;
			    }
        #  $thbl =substr($filespb,12,4);
          $icustomer =substr($filespb,8,5);
          $iarea =substr($filespb,8,2);
			    //$istore	= $this->session->userdata('store');
#          if($istore=='AA') $iarea='00'; else $iarea=$istore;
          //if($istore=='AA') $istorelocation='01'; else $istorelocation='00';
			    //$istorelocationbin	= '00';
					//$id=$this->session->userdata('user_id');
					$ispg='';
          $query = $this->db->query(" select a.i_spg from tr_spg a, tm_user b
                                      where a.i_area=b.i_area1 and a.i_user=b.i_user and a.i_customer='$icustomer'
                                      order by a.i_spg");
          if($query->num_rows()>0){
        		$tes=$query->row();
            $ispg=$tes->i_spg;
          } 

			    $jml			= $this->input->post('jml', TRUE);
			    $benar="false";
          if($noitem==1){
  			    $istockopname	=$this->mmaster->runningnumber($icustomer,$thbl);
  			    $this->mmaster->insertheader($istockopname,$dstockopname,$icustomer,$iarea,$ispg);
          }
			      $this->mmaster->insertdetail($iproduct, $iproductgrade, $eproductname, $nstockopname,$istockopname, $icustomer,
                                         $iproductmotif,$dstockopname,$iarea,$j);
            $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$icustomer);
            if(isset($trans)){              
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_stock;
                $q_ak =$itrans->n_quantity_stock;
                $q_in =0;
                $q_out=0;
                break;
              }
            }else{
              $q_aw=0;
              $q_ak=0;
              $q_in=0;
              $q_out=0;
            }
         //$this->mmaster->inserttrans4x($iproduct,$iproductgrade,$iproductmotif,$icustomer,$eproductname,$istockopname,$q_in,$q_out,$nstockopname,$q_aw,$q_ak);
            $emutasiperiode='20'.substr($istockopname,3,4);
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$icustomer,$emutasiperiode))
            {
              $this->mmaster->updatemutasi4x($iproduct,$iproductgrade,$iproductmotif,$icustomer,$nstockopname,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasi4x($iproduct,$iproductgrade,$iproductmotif,$icustomer,$nstockopname,$emutasiperiode);
#              $this->mmaster->insertheadermutasi4x($icustomer,$emutasiperiode);              
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$icustomer))
            {
#              $this->mmaster->updateic4x($iproduct,$iproductgrade,$iproductmotif,$icustomer,$nstockopname,$q_ak);
            }else{
#              $this->mmaster->insertic4x($iproduct,$iproductgrade,$iproductmotif,$icustomer,$eproductname,$nstockopname);
            }
  ###########
			      $benar="true";
		    }
			    if ( ($this->db->trans_status() === FALSE))
		    {
	        $this->db->trans_rollback();
				  $this->load->view('transfersokons/vformgagal',$data);
		    }else{
		        $this->db->trans_commit();
#		        $this->db->trans_rollback();
				    $data['sukses']=true;
				    $data['inomor']=$istockopname;
				    $this->load->view('transfersokons/vformsukses',$data);
        }
      }

		/*	if ( ($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
				$this->load->view('transfersokons/vformgagal',$data);
			}else{
				$this->db->trans_commit();
#				$this->db->trans_rollback();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Transfer SO PB Pelanggan '.$icustomer.' No:'.$istockopname;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );
				  $this->load->view('transfersokons/vformsukses',$data);
			}	*/		
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
