<?php
class Cform extends CI_Controller
{
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationx');
      require_once("php/fungsi.php");
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu405') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data['page_title'] = $this->lang->line('exp-customerlist');
         $data['iarea'] = '';
         $this->load->view('exp-customerlist/vmainform', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu405') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data['page_title'] = $this->lang->line('exp-customerlist');
         $this->load->view('exp-customerlist/vinsert_fail', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu405') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/exp-customerlist/cform/area/index/';
         $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

         $config['total_rows'] = $query->num_rows();
         $config['per_page']   = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('exp-customerlist/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
         $this->load->view('exp-customerlist/vlistarea', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu405') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/exp-customerlist/cform/area/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari   = strtoupper($cari);
         $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('exp-customerlist/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
         $this->load->view('exp-customerlist/vlistarea', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu405') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         set_time_limit(3600000);
         ini_set("memory_limit", "512M");
         ini_set("max_execution_time", "3600000");

         $this->load->model('exp-customerlist/mmaster');
         $iarea = $this->input->post('iarea');
         $istore = $this->input->post('istore');
         $eareaname = $this->input->post('eareaname');
         if ($iarea == '') $iarea = $this->uri->segment(4);
         $this->load->model('exp-customerlist/mmaster');
         if ($iarea == 'NA') $eareaname = 'Nasional';
         if ($iarea != 'NA') {
            $this->db->select("   a.i_customer, a.e_customer_name, a.f_customer_aktif, a.e_customer_address, b.e_city_name, 
												a.e_customer_phone, a.i_price_group, e.e_price_groupname, a.n_customer_toplength, g.v_flapond, 
												c.n_customer_discount1, c.n_customer_discount2,  
												d.i_salesman, 
												a.e_customer_contact, a.d_signin, 
												x.e_customer_pkpname, x.e_customer_pkpaddress, x.e_customer_pkpnpwp,
												a.i_customer_class || '-' || e_customer_classname as e_customer_classname, i.e_customer_statusname, a.i_area, a.i_customer_grade, j.e_customer_gradename
											   from tr_customer a
												left join tr_city b on ( a.i_city=b.i_city  and a.i_area=b.i_area )
												left join tr_customer_discount c on (a.i_customer=c.i_customer)
												left join tr_customer_salesman d on (a.i_customer=d.i_customer and d.e_periode=to_char(now(),'yyyymm') and d.i_product_group='01' )
												left join tr_price_group e on (a.i_price_group=e.i_price_group)
												left join tr_customer_pkp x on (a.i_customer=x.i_customer)
												left join tr_customer_class f on (f.i_customer_class=a.i_customer_class)
                                    left join tr_customer_groupar g on(a.i_customer = g.i_customer)
                                    left join tr_customer_status i on(a.i_customer_status = i.i_customer_status)
                                    left join tr_customer_grade j on(a.i_customer_grade = j.i_customer_grade)
											where a.i_area='$iarea' /* and a.f_customer_aktif='t' */
											order by a.i_customer, a.i_customer , d.i_salesman desc", false);
         } else {
            $this->db->select(" a.i_customer, a.e_customer_name, a.f_customer_aktif, a.e_customer_address, b.e_city_name, 
												a.e_customer_phone, a.i_price_group, e.e_price_groupname, a.n_customer_toplength, g.v_flapond, 
												c.n_customer_discount1, c.n_customer_discount2,  
												d.i_salesman, 
												a.e_customer_contact, a.d_signin, 
												x.e_customer_pkpname, x.e_customer_pkpaddress, x.e_customer_pkpnpwp,
												a.i_customer_class || '-' || e_customer_classname as e_customer_classname, i.e_customer_statusname, a.i_area, a.i_customer_grade, j.e_customer_gradename
											from tr_customer a
												left join tr_city b on ( a.i_city=b.i_city  and a.i_area=b.i_area )
												left join tr_customer_discount c on (a.i_customer=c.i_customer)
												left join tr_customer_salesman d on (a.i_customer=d.i_customer and d.e_periode=to_char(now(),'yyyymm') and d.i_product_group='01' )
												left join tr_price_group e on (a.i_price_group=e.i_price_group)
												left join tr_customer_pkp x on (a.i_customer=x.i_customer)
												left join tr_customer_class f on (f.i_customer_class=a.i_customer_class)
                                    left join tr_customer_groupar g on(a.i_customer = g.i_customer)
                                    left join tr_customer_status i on(a.i_customer_status = i.i_customer_status)
                                    left join tr_customer_grade j on(a.i_customer_grade = j.i_customer_grade)
												/* where a.f_customer_aktif='t' */
											order by a.i_customer, a.i_customer, d.i_salesman desc", false);
         }

         $query = $this->db->get();
         $this->load->library('PHPExcel');
         $this->load->library('PHPExcel/IOFactory');
         $objPHPExcel = new PHPExcel();
         $objPHPExcel->getProperties()->setTitle("Customer List")->setDescription(NmPerusahaan);
         $objPHPExcel->setActiveSheetIndex(0);
         if ($query->num_rows() > 0) {
            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
               array(
                  'font' => array(
                     'name'   => 'Arial',
                     'bold'  => true,
                     'italic' => false,
                     'size'  => 10
                  ),
                  'alignment' => array(
                     'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                     'vertical'  => Style_Alignment::VERTICAL_CENTER,
                     'wrap'      => true
                  )
               ),
               'A2:A4'
            );
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(20);

            $objPHPExcel->getActiveSheet()->setCellValue('A2', 'CUSTOMER LIST ');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 16, 2);
            if ($iarea != 'NA') {
               $objPHPExcel->getActiveSheet()->setCellValue('A3', "Area : " . $iarea . " - " . $eareaname);
            } else {
               $objPHPExcel->getActiveSheet()->setCellValue('A3', "Area : Nasional");
            }
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 16, 3);

            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
               array(
                  'font' => array(
                     'name'   => 'Arial',
                     'bold'  => true,
                     'italic' => false,
                     'size'  => 10
                  ),
                  'alignment' => array(
                     'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                     'vertical'  => Style_Alignment::VERTICAL_CENTER,
                     'wrap'      => true
                  )
               ),
               'A5:W5'
            );


            $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
            $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('B5', 'KdLang');
            $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Nama');
            $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Alamat');
            $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Kota');
            $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('F5', 'No Tlp');
            $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('G5', 'TOP');
            $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Flapond');
            $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Disc 1');
            $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Disc 2');
            $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('K5', 'KS');
            $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Contact Person');
            $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('M5', 'Tgl Daftar');
            $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('N5', 'Aktif');
            $objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('O5', 'Klp Harga');
            $objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Nama PKP');
            $objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Alamat PKP');
            $objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('R5', 'NPWP');
            $objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('S5', 'Jenis Pelanggan');
            $objPHPExcel->getActiveSheet()->getStyle('S5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('T5', 'Status Pelanggan');
            $objPHPExcel->getActiveSheet()->getStyle('T5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('U5', 'Tingkat Pelanggan');
            $objPHPExcel->getActiveSheet()->getStyle('U5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('V5', 'Status Toko');
            $objPHPExcel->getActiveSheet()->getStyle('V5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('W5', 'TTL Owner');
            $objPHPExcel->getActiveSheet()->getStyle('W5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $i = 6;
            $j = 6;
            $xarea = '';
            $no = 1;
            $lang = '';
            foreach ($query->result() as $row) {
               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'   => 'Arial',
                        'bold'   => false,
                        'italic' => false,
                        'size'   => 10
                     )
                  ),
                  'A' . $i . ':W' . $i
               );

               if ($row->f_customer_aktif == 't') {
                  $aktif = "Ya";
               } else {
                  $aktif = "Tdk";
               }

               //* PENAMBAHAN SHOP STATUS (12 JUN 2021)
               $cek1 = $this->db->query(" SELECT
                                             a.i_customer,
                                             a.i_shop_status,
                                             CASE
                                                WHEN (a.i_shop_status = ''
                                                OR a.i_shop_status = NULL) THEN '-'
                                                ELSE b.e_shop_status
                                             END AS status,
                                             CASE
                                                WHEN (a.e_customer_ownerttl = ''
                                                OR a.e_customer_ownerttl = NULL) THEN '-'
                                                ELSE a.e_customer_ownerttl
                                             END AS e_customer_ownerttl
                                          FROM
                                             tr_customer_tmp a
                                          LEFT JOIN tr_shop_status b ON
                                             (a.i_shop_status = b.i_shop_status)
                                          WHERE
                                             a.i_customer = '$row->i_customer'
                                             AND a.i_area = '$row->i_area'
                                          ORDER BY a.i_customer ");
               if ($cek1->num_rows() > 0) {
                  foreach ($cek1->result() as $xx) {
                     $eshopstatus = $xx->status;
                     $ttl         = $xx->e_customer_ownerttl;
                  }
               } else {
                  $cek2 = $this->db->query(" SELECT
                                                   a.i_customer,
                                                   a.i_shop_status,
                                                   CASE
                                                      WHEN (a.i_shop_status = ''
                                                      OR a.i_shop_status = NULL) THEN '-'
                                                      ELSE b.e_shop_status
                                                   END AS status,
                                                   CASE
                                                      WHEN (a.e_customer_ownerttl = ''
                                                      OR a.e_customer_ownerttl = NULL) THEN '-'
                                                      ELSE a.e_customer_ownerttl
                                                   END AS e_customer_ownerttl
                                                FROM
                                                   tr_customer_tmpnonspb a
                                                LEFT JOIN tr_shop_status b ON
                                                   (a.i_shop_status = b.i_shop_status)
                                                WHERE
                                                   a.i_customer = '$row->i_customer'
                                                   AND a.i_area = '$row->i_area'
                                                ORDER BY a.i_customer ");
                  if ($cek2->num_rows() > 0) {
                     foreach ($cek2->result() as $zz) {
                        $eshopstatus = $zz->status;
                        $ttl         = $zz->e_customer_ownerttl;
                     }
                  }
               }
               /****************************************************************** */
               $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $no);
               $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, "'" . $row->i_customer);
               $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->e_customer_name);
               $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->e_customer_address);
               $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $row->e_city_name);
               $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, "'" . $row->e_customer_phone);
               $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row->n_customer_toplength);
               $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->v_flapond);
               $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->n_customer_discount1);
               $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->n_customer_discount2);
               $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, "'" . $row->i_salesman);
               $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row->e_customer_contact);
               $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $row->d_signin);
               $objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $aktif);
               $objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $row->e_price_groupname);
               $objPHPExcel->getActiveSheet()->setCellValue('P' . $i, $row->e_customer_pkpname);
               $objPHPExcel->getActiveSheet()->setCellValue('Q' . $i, $row->e_customer_pkpaddress);
               $objPHPExcel->getActiveSheet()->setCellValue('R' . $i, $row->e_customer_pkpnpwp);
               $objPHPExcel->getActiveSheet()->setCellValue('S' . $i, $row->e_customer_classname);
               $objPHPExcel->getActiveSheet()->setCellValue('T' . $i, $row->e_customer_statusname);
               $objPHPExcel->getActiveSheet()->setCellValue('U' . $i, $row->e_customer_gradename);
               $objPHPExcel->getActiveSheet()->setCellValue('V' . $i, $eshopstatus);
               $objPHPExcel->getActiveSheet()->setCellValue('W' . $i, $ttl);
               $no++;
               $i++;
               $j++;
            }
            $x = $i - 1;
         }
         $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
         $nama = 'CustList-' . $iarea . '.xls';
         if ($iarea != 'NA') {
            $store = $iarea;
         } else {
            $store = '00';
         }
         if (file_exists('excel/' . $store . '/' . $nama)) {
            @chmod('excel/' . $store . '/' . $nama, 0777);
            @unlink('excel/' . $store . '/' . $nama);
         }
         $objWriter->save("excel/" . $store . '/' . $nama);

         $sess = $this->session->userdata('session_id');
         $id = $this->session->userdata('user_id');
         $sql   = "select * from dgu_session where session_id='$sess' and not user_data isnull";
         $rs      = pg_query($sql);
         if (pg_num_rows($rs) > 0) {
            while ($row = pg_fetch_assoc($rs)) {
               $ip_address     = $row['ip_address'];
               break;
            }
         } else {
            $ip_address = 'kosong';
         }
         $query    = pg_query("SELECT current_timestamp as c");
         while ($row = pg_fetch_assoc($query)) {
            $now     = $row['c'];
         }
         $pesan = 'Export Customer List area:' . $store;
         $this->load->model('logger');
         $this->logger->write($id, $ip_address, $now, $pesan);

         $data['sukses']   = true;
         // $data['inomor']   = "Ekspor Customer List Area : " . $iarea . "-" . $eareaname . " berhasil ke file " . $nama;
         // $data['folder']   = "exp-customerlist";

         $data['folder'] = 'excel/' . $store;
         $data['inomor']   = $nama;

         $this->load->view('nomorurl', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
}
