<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu322')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listmutasipbbytl');
			$data['iperiode']	= '';
			$data['icustomer']= '';
			$this->load->view('listmutasipbbytl/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu322')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('listmutasipbbytl/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iarea  = $this->session->userdata('i_area');
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url']   = base_url().'index.php/listmutasipbbytl/cform/view/'.$iperiode.'/';
			$data['iperiode']	    = $iperiode;
			$data['cari']         = $cari;
			$data['isi']    		  = $this->mmaster->baca($iperiode,$iarea);
			$data['iarea']        = $iarea;
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Membuka Data Mutasi PB By Area TL periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('listmutasipbbytl/vmainform',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu322')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('listmutasipbbytl/mmaster');
			$iperiode	= $this->input->post('iperiode');
      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			if($iarea=='') $iarea=$this->uri->segment(5);
      if(strlen($iarea)==1) $iarea='0'.$iarea;
      $query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
      $st=$query->row();
      $store=$st->i_store;
			$config['base_url'] = base_url().'index.php/listmutasipbbytl/cform/view/'.$iperiode.'/'.$store.'/';
			$query = $this->db->query(" select i_product from tm_mutasi where e_mutasi_periode = '$iperiode' and i_store='$store'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listmutasipbbytl');
      $cari='';
      $data['cari']=$cari;
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['istore']	= $store;
			$data['isi']		= $this->mmaster->baca($iperiode,$store,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('listmutasipbbytl/vmainform',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu322')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$dto	= $this->input->post('dto');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listmutasipbbytl/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_target a, tr_area b
										where a.i_periode = '$iperiode'
										and a.i_area=b.i_area
										and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('listmutasipbbytl/mmaster');
			$data['page_title'] = $this->lang->line('listmutasipbbytl');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('listmutasipbbytl/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu322')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari=strtoupper($this->input->post("cari"));
			$config['base_url'] = base_url().'index.php/listmutasipbbytl/cform/customer/index/';
      $ispg=strtoupper($this->session->userdata("user_id"));
      $iarea=$this->session->userdata("i_area");
      $query = $this->db->query(" select a.i_customer, a.i_area, a.e_spg_name, b.e_area_name, c.e_customer_name 
                                  from tr_spg a, tr_area b, tr_customer c
                                  where upper(a.i_spg) = '$ispg' and a.i_area='$iarea' and a.i_area=b.i_area 
                                  and a.i_customer=c.i_customer");
      if($query->num_rows>0){
        foreach($query->result() as $xx){
          $data['ispg']=$ispg;
          $data['iarea']=$iarea;
    			$data['icustomer'] = $xx->i_customer;
    			$data['eareaname'] = $xx->e_area_name;
    			$data['espgname'] = $xx->e_spg_name;
    			$data['ecustomername'] = $xx->e_customer_name;
        }
      }else{
        $data['ispg']=$ispg;
        $data['iarea']=$iarea;
  			$data['icustomer'] = '';
  			$data['eareaname'] = '';
  			$data['espgname'] = '';
  			$data['ecustomername'] = '';
      }
			if($data['icustomer']==''){
        $query = $this->db->query(" select a.i_customer from tr_customer_consigment a, tr_customer b, tr_spg c
                                    where a.i_customer=b.i_customer and a.i_customer like '%$cari%' and b.i_customer=c.i_customer",false);
			}else{
        $icustomer=$data['icustomer'];
        $query = $this->db->query(" select a.i_customer from tr_customer_consigment a, tr_customer b, tr_spg c
                                    where a.i_customer=b.i_customer and a.i_customer='$icustomer' and b.i_customer=c.i_customer",false);			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
      $icustomer=$data['icustomer'];
			$this->load->model('listmutasipbbytl/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($cari,$config['per_page'],$this->uri->segment(5),$icustomer);
			$this->load->view('listmutasipbbytl/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu322')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listmutasipbbytl/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store and (upper(a.i_area) like '%$cari%' or upper(a.e_area_name) like '%$cari%')
                                    group by a.i_store, b.e_store_name",false);
			}else{
				$query = $this->db->query("select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                    or a.i_area = '$area4' or a.i_area = '$area5')
                                    and (upper(a.i_area) like '%$cari%' or upper(a.e_area_name) like '%$cari%')
                                    group by a.i_store, b.e_store_name
                                    order by a.i_store, b.e_store_name",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listmutasipbbytl/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listmutasipbbytl/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu322')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('listmutasipbbytl/mmaster');
      $iperiode = $this->uri->segment(4);
      $iproduct = $this->uri->segment(5);
      $saldo    = $this->uri->segment(6);
			$areanya    = $this->uri->segment(7);
			$this->load->model('listmutasipbbytl/mmaster');     
			$data['page_title'] = $this->lang->line('listmutasipbbytl');
			$data['iperiode']	  = $iperiode;
			$data['iproduct']	  = $iproduct;
			$data['saldo']	    = $saldo;
			$data['areanya']    = $areanya;
			$data['detail']	    = $this->mmaster->detail($areanya,$iperiode,$iproduct);
			$this->load->view('listmutasipbbytl/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetakdetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu322')=='t')) ||
			(($this->session->userdata('logged_in')) &&

			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('listmutasipbbytl/mmaster');
      $iperiode = $this->uri->segment(4);
      $icustomer= $this->uri->segment(5);
      $iproduct = $this->uri->segment(6);
      $saldo    = $this->uri->segment(7);
			$this->load->model('listmutasipbbytl/mmaster');     
			$data['page_title'] = $this->lang->line('listmutasipbbytl');
			$data['iperiode']	  = $iperiode;
			$data['icustomer']  = $icustomer;
			$data['iproduct']	  = $iproduct;
			$data['saldo']	    = $saldo;
			$data['detail']	    = $this->mmaster->detail($iperiode,$icustomer,$iproduct);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak Detail Mutasi PB Pelanggan:'.$icustomer.' Product:'.$iproduct;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('listmutasipbbytl/vformprintdetail',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu322')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('listmutasipbbytl/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('pperiode');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
#      $query=$this->mmaster->bacaexcel($iperiode,$store,$cari);
      if($iperiode>'201512'){
        $query = $this->db->select("	i_product, e_product_name, sum(n_saldo_awal) as n_saldo_awal, 
                    sum(n_mutasi_daripusat) as n_mutasi_daripusat, 
                    sum(n_mutasi_darilang) as n_mutasi_darilang, sum(n_mutasi_penjualan) as n_mutasi_penjualan, 
                    sum(n_mutasi_kepusat) as n_mutasi_kepusat, sum(n_mutasi_kelang) as n_mutasi_kelang, 
                    sum(n_saldo_akhir) as n_saldo_akhir, sum(n_saldo_stockopname) as n_saldo_stockopname 
                    from f_mutasi_stock_mo_cust_all_saldoakhir('$iperiode') where i_area = '$iarea'
                    group by i_product, e_product_name, e_mutasi_periode
                    order by i_product, e_product_name, e_mutasi_periode",false);
      }else{
        $query = $this->db->select("	i_product, e_product_name, sum(n_saldo_awal) as n_saldo_awal, 
                            sum(n_mutasi_daripusat) as n_mutasi_daripusat, 
		                        sum(n_mutasi_darilang) as n_mutasi_darilang, sum(n_mutasi_penjualan) as n_mutasi_penjualan, 
		                        sum(n_mutasi_kepusat) as n_mutasi_kepusat, sum(n_mutasi_kelang) as n_mutasi_kelang, 
		                        sum(n_saldo_akhir) as n_saldo_akhir, sum(n_saldo_stockopname) as n_saldo_stockopname 
		                        from f_mutasi_stock_mo_cust_all('$iperiode') where i_area = '$iarea'
		                        group by i_product, e_product_name, e_mutasi_periode
		                        order by i_product, e_product_name, e_mutasi_periode",false);
      }
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan Mutasi Area By TL")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN MUTASI KONSINYASI ALL AREA');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,11,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B4', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('C4', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('C4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D4', 'Saldo Awal');
				$objPHPExcel->getActiveSheet()->getStyle('D4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E4', 'Dari Pusat');
				$objPHPExcel->getActiveSheet()->getStyle('E4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F4', 'Dari Lang');
				$objPHPExcel->getActiveSheet()->getStyle('F4')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G4', 'Penjualan');
				$objPHPExcel->getActiveSheet()->getStyle('G4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H4', 'Ke Pusat');
				$objPHPExcel->getActiveSheet()->getStyle('H4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I4', 'Ke Lang');
				$objPHPExcel->getActiveSheet()->getStyle('I4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        $objPHPExcel->getActiveSheet()->setCellValue('J4', 'Saldo Akhir');
				$objPHPExcel->getActiveSheet()->getStyle('J4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K4', 'Saldo Opname');
				$objPHPExcel->getActiveSheet()->getStyle('K4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L4', 'Selisih');
				$objPHPExcel->getActiveSheet()->getStyle('L4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i=5;
				$saldo=0;
        $no=0;
        $selisih=0;
/*				foreach($query->result() as $row){
        $selisih=($row->n_saldo_stockopname)-$row->n_saldo_akhir;
          $no++;
          $selisih=$row->saldo_stockopname-$row->saldo_akhir;
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_product, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->e_product_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->n_saldo_awal, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->n_mutasi_daripusat, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->n_mutasi_darilang, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->n_mutasi_penjualan, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->n_mutasi_kepusat, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->n_mutasi_kelang, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->n_saldo_akhir, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->n_saldo_stockopname, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $selisih, Cell_DataType::TYPE_NUMERIC);
					$i++;
				}*/
			}
			$objPHPExcel->getActiveSheet()->getStyle('A4:L4')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='MUTASI PB BY TL - '.$this->session->userdata('i_area').' - '.$iperiode.'.xls';
			$objWriter->save("excel/".'PB'.'/'.$nama); 

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Export Mutasi PB Per Area By TL :'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses'] = true;
			$data['inomor']	= "Laporan Mutasi PB Per Area By TL";
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
