<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbbmretur');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listbbmretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listbbmretur/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      if($iarea=='NA'){
			  $query = $this->db->query(" select a.*, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area
										  from tm_bbm a, tr_customer b, tm_ttbretur c
										  where a.i_bbm=c.i_bbm and c.i_customer=b.i_customer 
										  and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
										  and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
										  a.d_bbm <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
			  $query = $this->db->query("select a.*, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area
										  from tm_bbm a, tr_customer b, tm_ttbretur c
										  where a.i_bbm=c.i_bbm and c.i_customer=b.i_customer 
										  and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
										  and a.i_area='$iarea' and
										  a.d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bbm <= to_date('$dto','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listbbmretur');
			$this->load->model('listbbmretur/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data BBM Retur Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listbbmretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbbmretur');
			$this->load->view('listbbmretur/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbm		= $this->uri->segment(4);
			$ittb		= $this->uri->segment(5);
			$iarea	= $this->uri->segment(6);
			$tahun	= $this->uri->segment(7);
			$this->load->model('listbbmretur/mmaster');
			$this->mmaster->delete($ibbm,$ittb,$iarea,$tahun);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Hapus BBM Retur No:'.$ibbm.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$cari		= strtoupper($this->input->post('cari'));
			$dfrom	= $this->uri->segment(8);
			$dto 		= $this->uri->segment(9);
			$config['base_url'] = base_url().'index.php/listbbmretur/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area
										from tm_bbm a, tr_customer b, tm_ttbretur c
										where a.i_bbm=c.i_bbm and c.i_customer=b.i_customer 
										and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_bbm <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(10);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listbbmretur');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(10),$cari);
			$this->load->view('listbbmretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listbbmretur/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      if($iarea=='NA'){
			  $query = $this->db->query(" select a.*, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area
										  from tm_bbm a, tr_customer b, tm_ttbretur c
										  where a.i_bbm=c.i_bbm and c.i_customer=b.i_customer 
										  and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
										  and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
										  a.d_bbm <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
			  $query = $this->db->query(" select a.*, b.e_customer_name, c.i_ttb, c.d_ttb, c.i_area
										  from tm_bbm a, tr_customer b, tm_ttbretur c
										  where a.i_bbm=c.i_bbm and c.i_customer=b.i_customer 
										  and (c.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
										  and a.i_area='$iarea' and
										  a.d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
										  a.d_bbm <= to_date('$dto','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listbbmretur');
			$this->load->model('listbbmretur/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listbbmretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bbmretur')." update";
			if(

				($this->uri->segment(4)!=='')
			  ){
				$ibbm		= $this->uri->segment(4);
				$iarea	= $this->uri->segment(5);
				$dfrom	= $this->uri->segment(6);
				$dto		= $this->uri->segment(7);
				$query 	= $this->db->query("select a.*, b.e_product_motifname 
											              from tm_bbm_item a, tr_product_motif b 
											              where a.i_bbm = '$ibbm' 
											              and a.i_product=b.i_product 
											              and a.i_product_motif=b.i_product_motif");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ibbm'] = $ibbm;
				$data['iarea']= $iarea;
				$data['dfrom']= $dfrom;
				$data['dto'] 	= $dto;
				$this->load->model('bbmretur/mmaster');
				$data['isi']=$this->mmaster->baca($ibbm);
				$data['detail']=$this->mmaster->bacadetail($ibbm);
				$this->load->view('listbbmretur/vformupdate',$data);
			}else{
				$this->load->view('bbmretur/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listbbmretur/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listbbmretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listbbmretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
