<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu397')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbrgprice');
			$data['awal']='ya';
			$this->load->view('pricelist/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu397')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbrgprice');
			$this->load->model('pricelist/mmaster');
######      
      $this->db->select("	* from crosstab(
							'select a.i_product,m.i_product_seri ,
							m.e_product_seriname , b.i_product_type, c.e_product_typename,
	 						d.i_product_class, b.e_product_name, c.i_product_group, 
							x.e_product_groupname, d.e_product_classname, e.i_product_category, 
							e.e_product_categoryname, b.f_product_pricelist, b.d_product_register,
							g.v_product_mill, b.i_product_status, y.e_product_statusname, b.i_supplier, 
							f.e_supplier_name, a.i_price_group, sum(a.v_product_retail) 
							from tr_product_price a, tr_product b
							left join tr_product_seri m on b.i_product_seri  = m.i_product_seri  	 
							left join tr_harga_beli g
							 on (b.i_product=g.i_product and g.i_price_group=''00'')
							 left join tr_product_type c on (b.i_product_type=c.i_product_type) 
							left join tr_product_class d on (b.i_product_class=d.i_product_class) 
							left join tr_product_group x on (x.i_product_group=c.i_product_group) 
							left join tr_product_status y on (y.i_product_status=b.i_product_status) 
							left join tr_product_category e on (b.i_product_category=e.i_product_category 
							and b.i_product_class=e.i_product_class), tr_supplier f where a.i_product=b.i_product
							 and b.i_supplier=f.i_supplier and b.f_product_pricelist=''t'' 
							and a.i_product_grade=''A'' group by a.i_product, a.i_price_group, 
							b.i_product_type, c.e_product_typename, d.i_product_class, b.e_product_name, 
							c.i_product_group, x.e_product_groupname, d.e_product_classname, e.i_product_category, 
							e.e_product_categoryname, b.f_product_pricelist, b.d_product_register, 
							g.v_product_mill, b.i_product_status, y.e_product_statusname, b.i_supplier, 
							f.e_supplier_name, m.i_product_seri,
							m.e_product_seriname order by a.i_product, a.i_price_group',
							'select i_price_group from tr_price_group order by i_price_group ')
                           as
                          (i_product text,i_product_seri text,
						  e_product_seriname text, i_product_type text, e_product_typename text, i_product_class text, e_product_name text,
                           i_product_group text, e_product_groupname text, 
                           e_product_classname text, i_product_category text, e_product_categoryname text, f_product_pricelist boolean,
                           d_product_register date, v_product_mill numeric, i_product_status text, e_product_statusname text, 
                           i_supplier text, e_supplier_name text, 
                           h00 numeric, h01 numeric, h02 numeric, h03 numeric, h04 numeric, 
                           h05 numeric, h06 numeric , hG0 numeric, hG2 numeric)",false);
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar Harga Barang ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(8);
				// $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(8);


				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar Harga Barang');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,27,2);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:AE5'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'H00');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'H01');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'H02');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'H03');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'H04');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'H05');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'H06');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'G00');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'G02');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				// $objPHPExcel->getActiveSheet()->setCellValue('L5', 'G03');
				// $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
				// 	array(
				// 		'borders' => array(
				// 			'top' 	=> array('style' => Style_Border::BORDER_THIN)
				// 		),
				// 	)
				// );
				// $objPHPExcel->getActiveSheet()->setCellValue('M5', 'G05');
				// $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
				// 	array(
				// 		'borders' => array(
				// 			'top' 	=> array('style' => Style_Border::BORDER_THIN)
				// 		),
				// 	)
				// );
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Jenis');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Nama Jenis');
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Kelas');
				$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('O5', 'Nama Kelas');
				$objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('P5', 'Kategori');
				$objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Nm Kategori');
				$objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('R5', 'Price List');
				$objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('S5', 'Tgl Daftar');
				$objPHPExcel->getActiveSheet()->getStyle('S5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('T5', 'Pabrik');
				$objPHPExcel->getActiveSheet()->getStyle('T5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('U5', 'Grade');
				$objPHPExcel->getActiveSheet()->getStyle('U5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('V5', 'Status');
				$objPHPExcel->getActiveSheet()->getStyle('V5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('W5', 'Kode Supplier');
				$objPHPExcel->getActiveSheet()->getStyle('W5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('X5', 'Nama Supplier');
				$objPHPExcel->getActiveSheet()->getStyle('X5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Y5', 'Ada Stock');
				$objPHPExcel->getActiveSheet()->getStyle('Y5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Z5', 'Group');
				$objPHPExcel->getActiveSheet()->getStyle('Z5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('AA5', 'Nama Group');
				$objPHPExcel->getActiveSheet()->getStyle('AA5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AB5', 'kD Seri');
				$objPHPExcel->getActiveSheet()->getStyle('AB5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AC5', 'Nama Seri');
				$objPHPExcel->getActiveSheet()->getStyle('AC5')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$i=6;
				$j=6;
        $no=0;
        $data['isi']=$query->result();
				foreach($query->result() as $row){
          $no++;
          $quer=$this->db->query("select * from tm_ic where i_store='AA' and i_product='$row->i_product'");
          if($quer->num_rows()>0){
            foreach($quer->result() as $xx){
              if($xx->n_quantity_stock>0){
                $adastok='Ya';
              }else{
                $adastok='Tidak';
              }  
            }
          }else{
            $adastok='Tidak';
          }
          if(substr($row->i_product,0,1)=='Z' || substr($row->i_product,0,1)=='z'){
            $grade='B';
          }else{
            $grade='A';
          }

          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':AC'.$i
				  );
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_product, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->e_product_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->h00, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->h01, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->h02, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->h03, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->h04, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->h05, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->h06, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->hg0, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->hg2, Cell_DataType::TYPE_NUMERIC);
        //   $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->hg3, Cell_DataType::TYPE_NUMERIC);
        //   $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->hg5, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->i_product_type, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->e_product_typename, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->i_product_class, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $row->e_product_classname, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $row->i_product_category, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $row->e_product_categoryname, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->f_product_pricelist, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $row->d_product_register, Cell_DataType::TYPE_STRING);
          if($this->session->userdata('departement')=='6'){
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $row->v_product_mill, Cell_DataType::TYPE_NUMERIC);
          }
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $grade, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('V'.$i, $row->e_product_statusname, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('W'.$i, $row->i_supplier, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('X'.$i, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('Y'.$i, $adastok, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('Z'.$i, $row->i_product_group, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('AA'.$i, $row->e_product_groupname, Cell_DataType::TYPE_STRING);
		  $objPHPExcel->getActiveSheet()->setCellValueExplicit('AB'.$i, $row->i_product_seri, Cell_DataType::TYPE_STRING);
		  $objPHPExcel->getActiveSheet()->setCellValueExplicit('AC'.$i, $row->e_product_seriname, Cell_DataType::TYPE_STRING);
					$i++;
					$j++;
				}
				$x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='PriceList.xls';
      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        @unlink('excel/00/'.$nama);
      }
			$objWriter->save('excel/00/'.$nama);
      @chmod('excel/00/'.$nama, 0777);
######
#			$data['isi']=$this->mmaster->bacasemua();
      $data['awal']='bukan';
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Harga Barang';
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('pricelist/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu397')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbrgprice');
			$this->load->view('pricelist/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
