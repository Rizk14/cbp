<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationxx');
      $this->load->helper(array('file','directory','fusioncharts'));
      require_once("php/fungsi.php");
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu467')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('listareacover');
         $data['df'] = '';

         $sess  = $this->session->userdata('session_id');
         $id   = $this->session->userdata('user_id');
         $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
         $rs   = pg_query($sql);
         if(pg_num_rows($rs)>0){
           while($row=pg_fetch_assoc($rs)){
             $ip_address   = $row['ip_address'];
             break;
           }
         }else{
           $ip_address='kosong';
         }
         $query  = pg_query("SELECT current_timestamp as c");
         while($row=pg_fetch_assoc($query)){
           $now    = $row['c'];
         }
         $pesan="Membuka Menu List Area Cover";
         $this->load->model('logger');
         $this->logger->write($id, $ip_address, $now, $pesan);

         $this->load->view('listareacover/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
    function view()
    {
      if (
      (($this->session->userdata('logged_in')) &&
      ($this->session->userdata('menu467')=='t')) ||
      (($this->session->userdata('logged_in')) &&
      ($this->session->userdata('allmenu')=='t'))
      ){
        $this->load->model('listareacover/mmaster');
        #$cari      = strtoupper($this->input->post('cari'));
        $df   = $this->input->post('df');
        $dt   = $this->input->post('dt');
        if(($df=='')&&($dt=='')){
          $df=$this->uri->segment(4);
          $dt=$this->uri->segment(5);
        }

        $query1 = $this->db->query("select*from tr_area",false);
        $query2 = $this->db->query("select*from tr_area_cover",false);
        
 
        $data['page_title'] = $this->lang->line('listareacover'); 
        $data['df'] = $df;
        $data['dt'] = $dt;
        $data['isi']      = $this->mmaster->baca($df,$dt); 
 
        $this->load->view('listareacover/vmainform',$data);
      }else{
        echo 'tes';
        $this->load->view('awal/index.php');
      }
    }
    
   function detail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu467')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){ 
      $iarea = $this->uri->segment(4);
         $this->load->model('listareacover/mmaster');
         $data['page_title'] = $this->lang->line('listareacover');
          
         $data['iarea']     = $iarea;
         $data['detail']       = $this->mmaster->detail($iarea);

         $sess  = $this->session->userdata('session_id');
         $id   = $this->session->userdata('user_id');
         $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
         $rs   = pg_query($sql);
         if(pg_num_rows($rs)>0){
           while($row=pg_fetch_assoc($rs)){
             $ip_address   = $row['ip_address'];
             break;
           }
         }else{
           $ip_address='kosong';
         }
         $query  = pg_query("SELECT current_timestamp as c");
         while($row=pg_fetch_assoc($query)){
           $now    = $row['c'];
         }
         $pesan='Membuka Menu View Detail List Area Cover('.$iarea.')';
         $this->load->model('logger');
         $this->logger->write($id, $ip_address, $now, $pesan);
         
         $this->load->view('listareacover/vformdetail',$data);

      }else{
         $this->load->view('awal/index.php');
      }
   }
   
}
?>
