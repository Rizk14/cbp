<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnamenota');
			$data['iarea']='';
			$data['dto']='';
      $data['nt']='';
      $data['jt']='';
			$this->load->view('opnnotacustomer/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	= strtoupper($this->input->post('cari'));
      $nt   = $this->input->post('chkntx');
      $jt   = $this->input->post('chkjtx');
			$dto	= $this->input->post('dto');
			$iarea= $this->input->post('iarea');
			if($dto=='') $dto=$this->uri->segment(4);
			if($nt=='') $nt=$this->uri->segment(5);
			if($jt=='') $jt=$this->uri->segment(6);
			if($iarea=='') $iarea	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/opnnotacustomer/cform/view/'.$dto.'/'.$nt.'/'.$jt.'/'.$iarea.'/';
      if($nt=='qqq'){
			  $query = $this->db->query(" select a.i_nota from tr_customer b, tr_salesman c, tm_nota a
                                    left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                                    left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area 
                                                                and e.f_pelunasan_cancel='f')
                                    left join tr_pelunasan_remark f on(d.i_pelunasan_remark=f.i_pelunasan_remark)
																	  where a.i_customer=b.i_customer 
                                    and a.i_salesman=c.i_salesman and a.i_area=c.i_area
																	  and not a.i_nota isnull
																	  and (upper(a.i_nota) like '%$cari%' 
																		  or upper(a.i_spb) like '%$cari%' 
																		  or upper(a.i_customer) like '%$cari%' 
																		  or upper(b.e_customer_name) like '%$cari%')
																	  and a.i_area='$iarea' and
																	  a.d_nota <= to_date('$dto','dd-mm-yyyy')
                                    and a.v_sisa>0
                                    and a.f_nota_cancel='f'",false);
      }elseif($jt=='qqq'){
			  $query = $this->db->query(" select a.i_nota from tr_customer b, tr_salesman c, tm_nota a
                                    left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                                    left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area 
                                                                and e.f_pelunasan_cancel='f')
                                    left join tr_pelunasan_remark f on(d.i_pelunasan_remark=f.i_pelunasan_remark)
																	  where a.i_customer=b.i_customer 
                                    and a.i_salesman=c.i_salesman and a.i_area=c.i_area
																	  and not a.i_nota isnull
																	  and (upper(a.i_nota) like '%$cari%' 
																		  or upper(a.i_spb) like '%$cari%' 
																		  or upper(a.i_customer) like '%$cari%' 
																		  or upper(b.e_customer_name) like '%$cari%')
																	  and a.i_area='$iarea' and
																	  a.d_jatuh_tempo <= to_date('$dto','dd-mm-yyyy')
                                    and a.v_sisa>0
                                    and a.f_nota_cancel='f'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('opnnotacustomer/mmaster');
      if($nt=='qqq'){
	  		$data['page_title'] = $this->lang->line('opnamenota').' per tanggal nota';
      }else{
  			$data['page_title'] = $this->lang->line('opnamenota').' per tanggal jatuh tempo';
      }
			$data['cari']		= $cari;
			$data['keyword']= '';	
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['nt']     = $nt;
      $data['jt']     = $jt;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dto,$config['per_page'],$this->uri->segment(8),$cari,$nt,$jt);
			$this->load->view('opnnotacustomer/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnamenota');
			$this->load->view('opnnotacustomer/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($cari=='') $cari	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/opnnotacustomer/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$cari%' 
										  or upper(a.i_spb) like '%$cari%' 
										  or upper(a.i_customer) like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('opnnotacustomer/mmaster');
			$data['page_title'] = $this->lang->line('opnamenota');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('opnnotacustomer/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$keyword = strtoupper($this->uri->segment(7));	
			$config['base_url'] = base_url().'index.php/opnnotacustomer/cform/cariperpages/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$keyword.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$keyword%' 
										  or upper(a.i_spb) like '%$keyword%' 
										  or upper(a.i_customer) like '%$keyword%' 
										  or upper(b.e_customer_name) like '%$keyword%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('opnnotacustomer/mmaster');
			$data['page_title'] = $this->lang->line('opnamenota');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$keyword);
			$this->load->view('opnnotacustomer/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opnnotacustomer/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
											or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('opnnotacustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('opnnotacustomer/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/opnnotacustomer/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('opnnotacustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('opnnotacustomer/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
     	 ($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dto	    = $this->input->post('dto');
			$icustomer= $this->input->post('icustomer');
      $nt   = $this->input->post('chkntx');
      $jt   = $this->input->post('chkjtx');
      if($icustomer=='')$icustomer=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
      if($nt=='') $nt=$this->uri->segment(6);
			if($jt=='') $jt=$this->uri->segment(7);
			$this->load->model('opnnotacustomer/mmaster');
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$data['dto']  = $dto;
			$data['icustomer']= $icustomer;
			$data['page_title'] = $this->lang->line('printopnnotacustomer');
			$data['isi']=$this->mmaster->baca($icustomer,$dto,$nt,$jt);
			$data['user']	= $this->session->userdata('user_id');
      $sess=$this->session->userdata('session_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
	    $query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
		    $now	  = $row['c'];
	    }
	    $pesan='Cetak Opname Nota Per Customer sampai tanggal '.$dto.' Pelanggan:'.$icustomer;
	    $this->load->model('logger');
	    $this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('opnnotacustomer/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
