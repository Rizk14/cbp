<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationxx');
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('bankmultiinalloc');
         $data['dfrom']='';
         $data['dto']  ='';
         $data['ialokasi']  ='';
         $data['ikbank']  ='';
         $data['iarea']='';
         $data['isi']  ='';

         $this->load->view('akt-bankin-multialloc/vmainform', $data);

      }elseif($this->session->userdata('logged_in')){
         $this->load->view('errorauthority');
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('listspb');
         $this->load->view('akt-bankin-multialloc/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function delete()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb = $this->input->post('ispbdelete', TRUE);
         $this->load->model('akt-bankin-multialloc/mmaster');
         $this->mmaster->delete($ispb);
         $data['page_title'] = $this->lang->line('listspb');
         $data['ispb']='';
         $data['isi']=$this->mmaster->bacasemua();
         $this->load->view('akt-bankin-multialloc/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cari()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/index/';
         $config['per_page'] = '10';
         $limo=$config['per_page'];
         $ofso=$this->uri->segment(4);
         if($ofso=='')
            $ofso=0;
         $query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
                              where a.i_customer=b.i_customer
                              and a.f_lunas = 'f'
                              and (upper(a.i_customer) like '%$cari%'
                                or upper(b.e_customer_name) like '%$cari%'
                                or upper(a.i_spb) like '%$cari%')",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(4);
         $this->pagination->initialize($config);
         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
         $data['page_title'] = $this->lang->line('bankmultiinalloc');
         $data['ispb']='';
         $data['ittb']='';
         $data['inota']='';
         $this->load->view('akt-bankin-multialloc/vmainform',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function approve()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('bankmultiinalloc');
         if(
            ($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
           )
         {
#            $idt        = str_replace('tandagaring','/',$this->uri->segment(4));
            $ikbank     = $this->uri->segment(4);
            $iarea      = $this->uri->segment(5);
            $eareaname  = $this->uri->segment(6);
            $eareaname  = str_replace('%20', ' ',$eareaname);
            $vsisa      = $this->uri->segment(7);
            $dfrom      = $this->uri->segment(8);
            $dto        = $this->uri->segment(9);
            $dbank      = $this->uri->segment(10);
            $ebankname  = $this->uri->segment(11);
            $ebankname  = str_replace('%20', ' ',$ebankname);
            $icoabank   = $this->uri->segment(12);
            $igiro   = $this->uri->segment(13);
//          $query = $this->db->query("select * from tm_nota_item where i_nota = '$idt' and i_area = '$iarea'");
            $data['jmlitem'] = 0;//$query->num_rows();
            $data['ialokasi'] = '';
            $data['ikbank'] = $ikbank;
            $data['iarea']=$iarea;
            $data['igiro']=$igiro;
            $data['eareaname']=$eareaname;
            $data['ebankname']=$ebankname;
            $data['vsisa']=$vsisa;
            $data['dfrom']=$dfrom;
            $data['dto']=$dto;
            $data['dbank']=$dbank;
            $data['icoabank'] = $icoabank;
            $this->load->model('akt-bankin-multialloc/mmaster');
            $data['isi']='';
            //$this->mmaster->bacapl($iarea,$ialokasi,$ikbank,$icoabank);
            $data['detail']='';
            //$this->mmaster->bacadetail($idt,$iarea);
            $this->load->view('akt-bankin-multialloc/vmainform',$data);
         }else{
            $this->load->view('akt-bankin-multialloc/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function update()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('akt-bankin-multialloc/mmaster');
         $ikbank  = $this->input->post('ikbank', TRUE);
         $icoabank= $this->input->post('icoabank', TRUE);
         $dalokasi  = $this->input->post('dalokasi', TRUE);
         if($dalokasi!=''){
            $tmp=explode("-",$dalokasi);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dalokasi=$th."-".$bl."-".$hr;
            $thbl=$th.$bl;
            $iperiode=$th.$bl;
         }
         $dbank  = $this->input->post('dbank', TRUE);
         if($dbank!=''){
            $tmp=explode("-",$dbank);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dbank=$th."-".$bl."-".$hr;
         }
         $icustomer     = $this->input->post('icustomer', TRUE);
         $ecustomername = $this->input->post('ecustomername', TRUE);
         $ecustomeraddress= $this->input->post('ecustomeraddress', TRUE);
         $ecustomercity = $this->input->post('ecustomercity', TRUE);
         $iarea         = $this->input->post('iarea', TRUE);
         $eareaname     = $this->input->post('eareaname', TRUE);
         $ebankname     = $this->input->post('ebankname', TRUE);
         $vjumlahx      = $this->input->post('vjumlah',TRUE);
         $vjumlahx      = str_replace(',','',$vjumlahx);
         $vlebihx       = $this->input->post('vlebih',TRUE);
         $vlebihx       = str_replace(',','',$vlebihx);
         $jml           = $this->input->post('jml', TRUE);
         $igiro         = $this->input->post('igiro', TRUE);
         $ada=false;
         if(($dbank!='') && ($dalokasi!='') && ($ikbank!='') && ($vjumlahx!='') && ($vjumlahx!='0') && ($jml!='0') && ($icustomer!='') && ($igiro!='')){
#         if(($dbank!='') && ($dalokasi!='') && ($ikbank!='') && ($vjumlahx!='') && ($vjumlahx!='0') && ($jml!='0') && ($icustomer!='') ){
/*
          for($i=1;$i<=$jml;$i++){
            $vjumla = $this->input->post('vjumlah'.$i, TRUE);
            $vsisa  = $this->input->post('vsisa'.$i, TRUE);
            $vjumla = str_replace(',','',$vjumla);
            $vsisa  = str_replace(',','',$vsisa);
            $vsisa  = $vsisa-$vjumla;
            if( ($vsisa>0) ){
              $ada=true;
              break;
            }
          }
*/
          if(!$ada){
            $this->db->trans_begin();
            $inotax = $this->input->post('inota1', TRUE);
            $ialokasi=$this->mmaster->runningnumberpl($iarea,$thbl);
########## Posting ###########
            $egirodescription="Alokasi Bank Masuk no:".$ikbank;
            $fclose     = 'f';
            $jml      = $this->input->post('jml', TRUE);
            for($i=1;$i<=$jml;$i++)
            {
              $inota=$this->input->post('inota'.$i, TRUE);
              $ireff=$ialokasi.'|'.$inota; 
              if($i==1){
                $this->mmaster->inserttransheader($ireff,$iarea,$egirodescription,$fclose,$dalokasi);
#               $this->mmaster->updatepelunasan($ialokasi,$iarea,$ikn);
              }
              $vjumlah    = $this->input->post('vjumlah'.$i, TRUE);
              $vjumlah    = str_replace(',','',$vjumlah);
              $accdebet   = PiutangDagang;
              $namadebet  = $this->mmaster->namaacc($accdebet);
              $tmp        = $this->mmaster->carisaldo($accdebet,$iperiode);
              if($tmp) 
                $vsaldoaw1    = $tmp->v_saldo_awal;
              else 
                $vsaldoaw1    = 0;
              if($tmp) 
                $vmutasidebet1  = $tmp->v_mutasi_debet;
              else
                $vmutasidebet1  = 0;
              if($tmp) 
                $vmutasikredit1 = $tmp->v_mutasi_kredit;
              else
                $vmutasikredit1 = 0;
              if($tmp) 
                $vsaldoak1    = $tmp->v_saldo_akhir;
              else
                $vsaldoak1    = 0;
        
              $acckredit    = PiutangDagang.$iarea;
              $namakredit   = $this->mmaster->namaacc($acckredit);
              $saldoawkredit  = $this->mmaster->carisaldo($acckredit,$iperiode);
              if($tmp) 
                $vsaldoaw2    = $tmp->v_saldo_awal;
              else
                $vsaldoaw2    = 0;
              if($tmp) 
                $vmutasidebet2  = $tmp->v_mutasi_debet;
              else
                $vmutasidebet2  = 0;
              if($tmp) 
                $vmutasikredit2 = $tmp->v_mutasi_kredit;
              else
                $vmutasikredit2 = 0;
              if($tmp) 
                $vsaldoak2    = $tmp->v_saldo_akhir;
              else
                $vsaldoak2    = 0;
              $this->mmaster->inserttransitemdebet($accdebet,$ireff,$namadebet,'t','t',$iarea,$egirodescription,$vjumlah,$dalokasi,$icoabank);
              $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vjumlah);
              $this->mmaster->inserttransitemkredit($acckredit,$ireff,$namakredit,'f','t',$iarea,$egirodescription,$vjumlah,$dalokasi,$icoabank);
              $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vjumlah);
              $this->mmaster->insertgldebet($accdebet,$ireff,$namadebet,'t',$iarea,$vjumlah,$dalokasi,$egirodescription,$icoabank);
              $this->mmaster->insertglkredit($acckredit,$ireff,$namakredit,'f',$iarea,$vjumlah,$dalokasi,$egirodescription,$icoabank);
            }
########## End of Posting ###########
            $this->mmaster->insertheader( $ialokasi,$ikbank,$iarea,$icustomer,$dbank,$dalokasi,$ebankname,
                                          $vjumlahx,$vlebihx,$icoabank,$igiro);
            $asal=0;
            $pengurang=$vjumlahx-$vlebihx;

          $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
          $group='xxx';
          foreach($query->result() as $row){
             $group=$row->i_customer_groupbayar;
          }
          $fupdatebank=$this->mmaster->updatebank($ikbank,$icoabank,$iarea,$pengurang);
          #echo 'bank:'.$fupdatebank;die;
          $this->mmaster->updatesaldo($group,$icustomer,$pengurang);
          for($i=1;$i<=$jml;$i++){
            $inota              = $this->input->post('inota'.$i, TRUE);
            $dnota              = $this->input->post('dnota'.$i, TRUE);
            if($dnota!=''){
               $tmp=explode("-",$dnota);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $dnota=$th."-".$bl."-".$hr;
            }
            $vjumlah= $this->input->post('vjumlah'.$i, TRUE);
            $vsisa  = $this->input->post('vsisa'.$i, TRUE);
            $vsiso  = $this->input->post('vsisa'.$i, TRUE);
            $vjumlah= str_replace(',','',$vjumlah);
            $vsisa = str_replace(',','',$vsisa);
            $vsiso = str_replace(',','',$vsiso);
            $vsiso  = $vsiso-$vjumlah;
            $eremark= $this->input->post('eremark'.$i,TRUE);
            $this->mmaster->insertdetail($ialokasi,$ikbank,$iarea,$inota,$dnota,$vjumlah,$vsisa,$i,$eremark,$icoabank);
            $fupdatenota=$this->mmaster->updatenota($inota,$vjumlah);
            if($fupdatenota==false) break;
          #echo 'nota:'.$fupdatenota.'<br>';
          #echo 'bank:'.$fupdatebank;'<br>';
          #echo 'siso:'.$vsiso;die;
/***COMMENT TANGGAL 07 APR 2021***/
//             if($vsiso>0 && $vsiso<=100 && $fupdatebank==true && $fupdatenota==true){
//               ########## Posting pembulatan ###########
//               $egirodescription="Alokasi Bank Masuk no:".$ikbank.'('.$icoabank.')';
//               $fclose     = 'f';
//               $ireff=$ialokasi.'|'.$inota;
//               $this->mmaster->inserttransheader($ireff,$iarea,$egirodescription,$fclose,$dalokasi);
//               $accdebet   = ByPembulatan;
//               $namadebet  = $this->mmaster->namaacc($accdebet);
//               $tmp        = $this->mmaster->carisaldo($accdebet,$iperiode);
//               if($tmp) 
//                 $vsaldoaw1    = $tmp->v_saldo_awal;
//               else 
//                 $vsaldoaw1    = 0;
//               if($tmp) 
//                 $vmutasidebet1  = $tmp->v_mutasi_debet;
//               else
//                 $vmutasidebet1  = 0;
//               if($tmp) 
//                 $vmutasikredit1 = $tmp->v_mutasi_kredit;
//               else
//                 $vmutasikredit1 = 0;
//               if($tmp) 
//                 $vsaldoak1    = $tmp->v_saldo_akhir;
//               else
//                 $vsaldoak1    = 0;
//               $acckredit    = PiutangDagang.$iarea;
//               $namakredit   = $this->mmaster->namaacc($acckredit);
//               $saldoawkredit  = $this->mmaster->carisaldo($acckredit,$iperiode);
//               if($tmp) 
//                 $vsaldoaw2    = $tmp->v_saldo_awal;
//               else
//                 $vsaldoaw2    = 0;
//               if($tmp) 
//                 $vmutasidebet2  = $tmp->v_mutasi_debet;
//               else
//                 $vmutasidebet2  = 0;
//               if($tmp) 
//                 $vmutasikredit2 = $tmp->v_mutasi_kredit;
//               else
//                 $vmutasikredit2 = 0;
//               if($tmp) 
//                 $vsaldoak2    = $tmp->v_saldo_akhir;
//               else
//                 $vsaldoak2    = 0;
//               $this->mmaster->inserttransitemdebet($accdebet,$ireff,$namadebet,'t','t',$iarea,$egirodescription,$vsiso,$dalokasi,$icoabank);
//               $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vsiso);
//               $this->mmaster->inserttransitemkredit($acckredit,$ireff,$namakredit,'f','t',$iarea,$egirodescription,$vsiso,$dalokasi,$icoabank);
//               $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vsiso);
//               $this->mmaster->insertgldebet($accdebet,$ireff,$namadebet,'t',$iarea,$vsiso,$dalokasi,$egirodescription,$icoabank);
//               $this->mmaster->insertglkredit($acckredit,$ireff,$namakredit,'f',$iarea,$vsiso,$dalokasi,$egirodescription,$icoabank);
//               $fupdatenota=$this->mmaster->updatenota($inota,$vsiso);
// #             $fupdatebank=$this->mmaster->updatebank2($ikbank,$icoabank,$iarea,$vsiso);
//               #echo 'bank:'.$fupdatebank;die;
//   ########## End of Posting pembulatan ###########
//             }

          }
          #echo 'nota:'.$fupdatenota.'<br>';
          #echo 'bank:'.$fupdatebank;die;

          if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Input Bank Allocation No:'.$ialokasi.' Area:'.$iarea;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );
                if($fupdatebank==true && $fupdatenota==true){
                  $this->db->trans_commit();
                }
#                 $this->db->trans_rollback();
                 $data['sukses'] = true;
                 $data['inomor'] = $ialokasi;
                 $this->load->view('nomor',$data);
              }
        }
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function edit()
   {
      if ( (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $area1   = $this->session->userdata('i_area');
         $data['page_title'] = $this->lang->line('bankmultiinalloc')." update";
         if(
            ($this->uri->segment(4)) && ($this->uri->segment(5))
           )
         {
            $ialokasi = $this->uri->segment(4);
            $ikbank   = $this->uri->segment(5);
            $iarea    = $this->uri->segment(6);
            $icoabank = $this->uri->segment(7);
            $dfrom    = $this->uri->segment(8);
            $dto      = $this->uri->segment(9);

            $this->load->model('akt-bankin-multialloc/mmaster');
            $query   = $this->db->query("select b.i_nota, to_char(a.d_alokasi,'yyyymm') as thbl from tm_alokasi a, tm_alokasi_item b
                                         where a.i_alokasi=b.i_alokasi and a.i_area=b.i_area and a.i_kbank=b.i_kbank
                                         and a.i_alokasi = '$ialokasi' and a.i_area = '$iarea' and a.i_kbank='$ikbank' 
                                         and a.i_coa_bank='$icoabank' ");
            if($query->num_rows()>0){
               $data['jmlitem'] = $query->num_rows();
               $data['vsisa']   = $this->mmaster->sisa($iarea,$ialokasi,$ikbank);
               $data['vbulat']  = $this->mmaster->bulat($iarea,$ialokasi,$ikbank);
               $data['isi']     = $this->mmaster->bacapl($iarea,$ialokasi,$ikbank,$icoabank);
               $data['detail']  = $this->mmaster->bacadetailpl($iarea,$ialokasi,$ikbank);
               
               $hasilrow = $query->row();
      				 $thbl	= $hasilrow->thbl;
               $query3	= $this->db->query(" select i_periode from tm_periode ");
		           if ($query3->num_rows() > 0){
			           $hasilrow = $query3->row();
			           $i_periode	= $hasilrow->i_periode;
		           }
         			 if($i_periode <= $thbl)
                  $data['bisaedit']=true;
                else
                  $data['bisaedit']=false;
            }
          }

          $data['ialokasi'] = $ialokasi;
          $data['iarea']    = $iarea;
          $data['ikbank']   = $ikbank;
          $data['dfrom']    = $dfrom;
          $data['dto']      = $dto;
          $this->load->view('akt-bankin-multialloc/vmainform',$data);
         }elseif($this->session->userdata('logged_in')){
         $this->load->view('errorauthority');
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function updatepelunasan()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('akt-bankin-multialloc/mmaster');
         $ipl  = $this->input->post('ipelunasan', TRUE);
         $idt  = $this->input->post('idt', TRUE);
         $ddt  = $this->input->post('ddt', TRUE);
         if($ddt!=''){
            $tmp=explode("-",$ddt);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $ddt=$th."-".$bl."-".$hr;
         }
      $dbukti  = $this->input->post('dbukti', TRUE);
         if($dbukti!=''){
            $tmp=explode("-",$dbukti);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dbukti=$th."-".$bl."-".$hr;
         }
         $djt  = $this->input->post('djt', TRUE);
         if($djt!=''){
            $tmp=explode("-",$djt);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $djt=$th."-".$bl."-".$hr;
         }
         $dcair   = $this->input->post('dcair', TRUE);
         if($dcair!=''){
            $tmp=explode("-",$dcair);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dcair=$th."-".$bl."-".$hr;
         }
         $dgiro   = $this->input->post('dgiro', TRUE);
         if($dgiro!=''){
            $tmp=explode("-",$dgiro);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dgiro=$th."-".$bl."-".$hr;
         }
         $icustomer     = $this->input->post('icustomer', TRUE);
         $ecustomername = $this->input->post('ecustomername', TRUE);
         $ecustomeraddress= $this->input->post('ecustomeraddress', TRUE);
         $ecustomercity = $this->input->post('ecustomercity', TRUE);
         $iarea         = $this->input->post('iarea', TRUE);
         $eareaname     = $this->input->post('eareaname', TRUE);
         $ijenisbayar   = $this->input->post('ijenisbayar',TRUE);
         $ejenisbayarname= $this->input->post('ejenisbayarname',TRUE);
         $egirobank     = $this->input->post('egirobank',TRUE);
         if(($egirobank==Null) && ($ijenisbayar!='05')){
            $egirobank  = '-';
            $igiro      = $this->input->post('igiro',TRUE);
         }else if(($egirobank==Null) && ($ijenisbayar=='05')){
            $egirobank  = $this->input->post('igiro',TRUE);
            $igiro      ='';
         }else if(($egirobank!=Null) && ($ijenisbayar=='01')){
            $igiro      = $this->input->post('igiro',TRUE);
         }else if(($egirobank!=Null) && ($ijenisbayar=='03')){
            $nkuyear = $this->input->post('nkuyear',TRUE);
            $igiro      = $this->input->post('igiro',TRUE);
         }else if($ijenisbayar=='04'){
            $igiro      = $this->input->post('igiro',TRUE);
         }else{
            $igiro      ='';
         }
         $vjumlah    = $this->input->post('vjumlah',TRUE);
         $vjumlah    = str_replace(',','',$vjumlah);
#        $vlebih        = $this->input->post('vlebih',TRUE);
#      if($ijenisbayar=='04'||$ijenisbayar=='05'){
#           $vlebih        = '0';
#      }else{
         $vlebih        = $this->input->post('vlebih',TRUE);
#      }
         $vlebih        = str_replace(',','',$vlebih);
#      $ipelunasanremark = $this->input->post('ipelunsanremark',TRUE);
#      $eremark    = $this->input->post('eremark',TRUE);
         $jml        = $this->input->post('jml', TRUE);
      $ada=false;
         if(($ddt!='') && ($dbukti!='') && ($idt!='') && ($vjumlah!='') && ($vjumlah!='0') && ($jml!='0') && ($ijenisbayar!='') && ($icustomer!='') ){
        for($i=1;$i<=$jml;$i++){
          $vjumla = $this->input->post('vjumlah'.$i, TRUE);
             $vsisa  = $this->input->post('vsisa'.$i, TRUE);
             $vjumla = str_replace(',','',$vjumla);
             $vsisa  = str_replace(',','',$vsisa);
             $vsisa  = $vsisa-$vjumla;
          $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
#          if( ($vsisa-$vjumlah>0) && ($ipelunasanremark=='') ){
          if( ($vsisa>0) && ($ipelunasanremark=='') ){
            $ada=true;
          }
          if($ada) break;
        }
        if(!$ada){
              $this->db->trans_begin();
              $asalkn   = $this->mmaster->jmlasalkn($ipl,$idt,$iarea,$ddt);
              foreach($asalkn as $asl){
                 $jmlpl = $asl->v_jumlah;
                 $lbhpl = $asl->v_lebih;
              }
              $asal  = $jmlpl-$lbhpl;
              $this->mmaster->deleteheader(  $ipl,$idt,$iarea,$ddt);
              $this->mmaster->insertheader(  $ipl,$idt,$iarea,$ijenisbayar,$igiro,$icustomer,$dgiro,$ddt,$dbukti,$dcair,$egirobank,
                                      $vjumlah,$vlebih);
              $pengurang=$vjumlah-$vlebih;
          $igiro=trim($igiro);

          $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
            $group='xxx';
            foreach($query->result() as $row){
               $group=$row->i_customer_groupbayar;
            }

              if($ijenisbayar=='01'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updategiro($group,$iarea,$igiro,$pengurang,$asal);
              }elseif($ijenisbayar=='03'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updateku($group,$iarea,$igiro,$pengurang,$asal,$nkuyear);
              }elseif($ijenisbayar=='04'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updatekn($group,$iarea,$igiro,$pengurang,$asal);
              }elseif($ijenisbayar=='05'){
            $this->mmaster->updatelebihbayar($group,$iarea,$egirobank,$pengurang,$asal);
          }
              for($i=1;$i<=$jml;$i++){
                $inota              = $this->input->post('inota'.$i, TRUE);
                $dnota              = $this->input->post('dnota'.$i, TRUE);
                if($dnota!=''){
                   $tmp=explode("-",$dnota);
                   $th=$tmp[2];
                   $bl=$tmp[1];
                   $hr=$tmp[0];
                   $dnota=$th."-".$bl."-".$hr;
              }
  /*
  a=giro/cek -- 01
  b=tunai -- 02
  c=transfer/ku -- 03
  d=kn -- 04
  e=lebih -- 05
  */
                $vjumlah   = $this->input->post('vjumlah'.$i, TRUE);
                $vasal  = $this->input->post('vasal'.$i, TRUE);
                $vasol  = $this->input->post('vasal'.$i, TRUE);
                if($vasal==''){
                  $vasal   = $this->input->post('vsisa'.$i, TRUE);
                  $vasol   = $this->input->post('vsisa'.$i, TRUE);
                }
                $vjumlah= str_replace(',','',$vjumlah);
                 $vasal = str_replace(',','',$vasal);
                 $vasol = str_replace(',','',$vasol);
#               $vsisa  = $vasal-$vjumlah;
                $vsisa  = $vasal;
                $vsiso  = $vasal-$vjumlah;
            $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
            $eremark    = $this->input->post('eremark'.$i,TRUE);

                $this->mmaster->deletedetail(   $ipl,$idt,$iarea,$inota,$ddt);
                $this->mmaster->insertdetail(   $ipl,$idt,$iarea,$inota,$dnota,$ddt,$vjumlah,$vsisa,$i,$ipelunasanremark,$eremark);
#           $this->mmaster->updatedt($idt,$iarea,$ddt,$inota,$vsisa);
#               $this->mmaster->updatenota($inota,$vsisa);
#               $this->mmaster->updatenota($inota,$vsiso);
                $this->mmaster->updatenota($inota,$vjumlah);
              }
#             $nilai=$this->mmaster->hitungsisadt($idt,$iarea,$ddt);
#             if($nilai==0){
#                $this->mmaster->updatestatusdt($idt,$iarea,$ddt);
#             }
              if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                 $this->db->trans_commit();
  #               $this->db->trans_rollback();

                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Update Pelunasan No:'.$ipl.' Area:'.$iarea;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );

                 $data['sukses'] = true;
                 $data['inomor'] = $ipl;
                 $this->load->view('nomor',$data);
              }
        }
         }

      }else{
         $this->load->view('awal/index.php');
      }
   }
   function customer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea  = $this->uri->segment(4);
         $cari   = $this->uri->segment(5);
         if($cari=='sikasep'){
           $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/customer/'.$iarea.'/sikasep/';
           $query = $this->db->query("select i_customer from tr_customer where i_area = '$iarea'",false);
         }else{
           $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/customer/'.$iarea.'/'.$cari.'/';
           $query   = $this->db->query(" select i_customer from tr_customer where i_area = '$iarea' and f_customer_aktif = 't'
                                         and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
         }
         $config['per_page'] = '10';
         
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);

         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
         $data['iarea']=$iarea;
         $data['cari']=$cari;
         $this->load->view('akt-bankin-multialloc/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function caricustomer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea    = strtoupper($this->input->post('area', FALSE));
         $cari    = strtoupper($this->input->post('cari', FALSE));
         if($iarea=='' || $iarea==null) $iarea = $this->uri->segment(4);
         if($cari=='' || $cari==null) $cari = $this->uri->segment(5);
         if($cari=='' || $cari==null || $cari=='sikasep'){
           $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/customer/'.$iarea.'/sikasep/';
         }else{
           $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/customer/'.$iarea.'/'.$cari.'/';
           $query   = $this->db->query(" select i_customer from tr_customer where i_area = '$iarea'
                                         and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
         $data['iarea']=$iarea;
         $data['cari']=$cari;
         $this->load->view('akt-bankin-multialloc/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function girocek()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/girocek/index/';
         $config['per_page'] = '10';
         if($this->session->userdata('i_area')=='00' || $this->session->userdata('i_area')=='PB'){
           $query = $this->db->query("select * from tr_jenis_bayar",false);
         }else{
           $query = $this->db->query("select * from tr_jenis_bayar where i_jenis_bayar<>'05'",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['area']=$this->session->userdata('i_area');
         $data['page_title'] = $this->lang->line('list_girocek');
         $data['isi']=$this->mmaster->bacagirocek($config['per_page'],$this->uri->segment(5),$this->session->userdata('i_area'));
         $this->load->view('akt-bankin-multialloc/vlistgirocek', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function nota()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari             = strtoupper($this->input->post('cari', FALSE));
         $data['baris']    = $this->uri->segment(4);
         $data['iarea']    = $this->uri->segment(5);
         $data['icustomer']= $this->uri->segment(6);
         $baris            = $this->uri->segment(4);
         $iarea            = $this->uri->segment(5);
         $icustomer        = $this->uri->segment(6);
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/nota/'.$baris.'/'.$iarea.'/'.$icustomer.'/index/';
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupbayar;
         }
         $query = $this->db->query(" select c.v_nota_netto, c.i_customer, c.d_nota, c.i_nota, c.v_sisa
                                     from tr_customer_groupbayar b, tm_nota c
                                     where b.i_customer_groupbayar='$group' and b.i_customer=c.i_customer
                                     and c.v_sisa>0 and not c.i_nota is null and c.f_nota_cancel='f'
                                     and (upper(c.i_nota) like '%$cari%' or upper(c.i_customer) like '%$cari%')" ,false);
/*
         $query = $this->db->query(" select c.v_nota_netto, c.i_customer, c.d_nota, c.i_nota, c.v_sisa
                                     from tr_customer_groupbayar b, tm_nota c
                                     where b.i_customer_groupbayar='$group' and b.i_customer=c.i_customer and c.i_area='$iarea' 
                                     and c.v_sisa>0 and not c.i_nota is null and c.f_nota_cancel='f'
                                     and (upper(c.i_nota) like '%$cari%' or upper(c.i_customer) like '%$cari%')" ,false);
*/
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($cari,$iarea,$icustomer,$config['per_page'],$this->uri->segment(8),$group);
         $this->load->view('akt-bankin-multialloc/vlistnota', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carinota()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari             = strtoupper($this->input->post('cari', FALSE));
         $baris            = $this->input->post('baris', FALSE)?$this->input->post('baris', FALSE):$this->uri->segment(4);
         $iarea            = $this->input->post('iarea', FALSE)?$this->input->post('iarea', FALSE):$this->uri->segment(5);
         $icustomer        = $this->input->post('icustomer', FALSE)?$this->input->post('icustomer', FALSE):$this->uri->segment(6);
         $data['baris']    = $baris;
         $data['iarea']    = $iarea;
         $data['icustomer']= $icustomer;
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/nota/'.$baris.'/'.$iarea.'/'.$icustomer.'/index/';
      $group='xxx';
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
      foreach($query->result() as $row){
        $group=$row->i_customer_groupbayar;
      }
         $query = $this->db->query(" select c.v_nota_netto, c.i_customer, c.d_nota, c.i_nota, c.v_sisa
                                     from tr_customer_groupbayar b, tm_nota c
                                     where b.i_customer_groupbayar='$group' and b.i_customer=c.i_customer
                                     and c.v_sisa>0 and not c.i_nota is null and c.f_nota_cancel='f'
                                     and (upper(c.i_nota) like '%$cari%' or upper(c.i_customer) like '%$cari%')" ,false);
/*
         $query = $this->db->query(" select c.v_nota_netto, c.i_customer, c.d_nota, c.i_nota, c.v_sisa
                                     from tr_customer_groupbayar b, tm_nota c
                                     where b.i_customer_groupbayar='$group' and b.i_customer=c.i_customer and c.i_area='$iarea' 
                                     and c.v_sisa>0 and not c.i_nota is null and c.f_nota_cancel='f'
                                     and (upper(c.i_nota) like '%$cari%' or upper(c.i_customer) like '%$cari%')" ,false);
*/
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $this->load->model('akt-bankin-multialloc/mmaster');

         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->carinota($cari,$iarea,$icustomer,$config['per_page'],$this->uri->segment(8),$group);
         $data['baris']=$baris;
         $this->load->view('akt-bankin-multialloc/vlistnota', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
##########
   function tunai()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = $this->uri->segment(4);
         $iarea      = $this->uri->segment(5);
         $dbukti     = $this->uri->segment(6);
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/tunai/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $config['per_page'] = '10';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
#        $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
#           $group=$row->i_customer_groupbayar;
         }
         $query = $this->db->query(" select a.i_tunai from tm_tunai  a, tr_customer_groupar b, tm_rtunai c, tm_rtunai_item d
                                     where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                                     and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
                                     and a.i_tunai=d.i_tunai and not c.i_cek isnull and a.d_tunai<='$xdbukti'
                                     and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f'
                                     ",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_tunai');
         $data['isi']=$this->mmaster->bacatunai($icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('akt-bankin-multialloc/vlisttunai', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
	function caritunai()
	{
		if (
       (($this->session->userdata('logged_in')) &&
       ($this->session->userdata('menu523')=='t')) ||
       (($this->session->userdata('logged_in')) &&
       ($this->session->userdata('allmenu')=='t'))
       ){
       $cari      = strtoupper($this->input->post('cari', FALSE));
       $icustomer = strtoupper($this->input->post('icustomer', FALSE));
       $iarea     = strtoupper($this->input->post('iarea', FALSE));
       $dbukti    = strtoupper($this->input->post('dbukti', FALSE));
       if($dbukti!=''){
        $tmp=explode('-',$dbukti);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2];
        $xdbukti=$th.'-'.$bl.'-'.$hr;
       }
       if($cari!=''){
        $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/tunai/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
       }
       
       $config['per_page'] = '10';
       $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
#        $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
       $group='xxx';
       foreach($query->result() as $row){
      $group=$row->i_customer_groupar;
#           $group=$row->i_customer_groupbayar;
       }
       $query = $this->db->query(" select a.i_tunai from tm_tunai  a, tr_customer_groupar b, tm_rtunai c, tm_rtunai_item d
                                   where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                                   and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
                                   and a.i_tunai=d.i_tunai and not c.i_cek isnull and a.d_tunai<='$xdbukti'
                                   and upper(a.i_tunai) like '%$cari%' and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_tunai_cancel='f' 
                                   and c.f_rtunai_cancel='f'",false);
       $config['total_rows'] = $query->num_rows();
       $config['first_link'] = 'Awal';
       $config['last_link'] = 'Akhir';
       $config['next_link'] = 'Selanjutnya';
       $config['prev_link'] = 'Sebelumnya';
       $config['cur_page'] = $this->uri->segment(8);
       $this->pagination->initialize($config);
       $this->load->model('akt-bankin-multialloc/mmaster');
       $data['page_title'] = $this->lang->line('list_tunai');
       $data['isi']=$this->mmaster->caritunai($cari,$icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
       $data['icustomer']=$icustomer;
       $data['iarea']=$iarea;
       $data['dbukti']=$dbukti;
       $this->load->view('akt-bankin-multialloc/vlisttunai', $data);
    }else{
       $this->load->view('awal/index.php');
    }
	}
##########
   function giro()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = $this->uri->segment(5);
         $iarea      = $this->uri->segment(4);
         $dbukti     = $this->uri->segment(6);
         $sisa       = $this->uri->segment(7);
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/giro/'.$iarea.'/'.$icustomer.'/'.$dbukti.'/'.$sisa.'/';
         $config['per_page'] = '10';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
#        $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
#           $group=$row->i_customer_groupbayar;
         }
         $query = $this->db->query(" select a.* from (
                          select a.i_giro as bayar, a.d_giro_cair as tgl from tm_giro  a, tr_customer_groupar b
                          where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                          and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                          and not a.d_giro_cair isnull and a.d_giro_cair<='$xdbukti'
                          union all
                          select a.i_tunai as bayar, a.d_tunai as tgl from tm_tunai  a, tr_customer_groupar b, tm_rtunai c, 
                          tm_rtunai_item d
                          where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                          and c.i_rtunai=d.i_rtunai and c.i_area=d.i_area and a.i_area=d.i_area_tunai
                          and a.i_tunai=d.i_tunai and a.d_tunai<='$xdbukti'
                          and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f'
                          union all
                          select a.i_kum as bayar, d_kum as tgl from tm_kum a, tr_customer_groupar b
                          where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                          and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
                          and d_kum<='$xdbukti'
                          )as a
                          order by a.tgl, a.bayar ",false);
/*
         $query = $this->db->query("   select a.i_giro from tm_giro  a, tr_customer_groupar b
                                       where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                                       and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                                       and not a.d_giro_cair isnull and a.d_giro_cair<='$xdbukti'",false);
*/
/*
         $query = $this->db->query("   select a.i_giro from tm_giro  a, tr_customer_groupbayar b
                                          where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                                          and (a.f_giro_tolak='f' or a.f_giro_batal='f')",false);
#                                           and a.i_area='$iarea'
*/
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_kugirotunai');
         $data['isi']=$this->mmaster->bacagiro($icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $data['xdbukti']=$xdbukti;
         $data['group']=$group;
         $this->load->view('akt-bankin-multialloc/vlistgiro', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
	function carigiro()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu523')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer = $this->input->post('icustomer', FALSE);
			$iarea= $this->input->post('iarea', FALSE);
			$dbukti     = strtoupper($this->input->post('dbukti', FALSE));
      if($dbukti!=''){
        $tmp=explode('-',$dbukti);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2];
        $xdbukti=$th.'-'.$bl.'-'.$hr;
      }
      $cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/giro/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
			$group='xxx';
			foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
			}
			$query = $this->db->query("	select a.i_giro from tm_giro  a, tr_customer_groupar b
									              	where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
							                    and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                                  and (upper(a.i_giro) like '%$cari%') and not a.d_giro_cair isnull and a.d_giro_cair<='$xdbukti'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('akt-bankin-multialloc/mmaster');
			$data['page_title'] = $this->lang->line('list_giro');
			$data['isi']=$this->mmaster->carigiro($cari,$icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$data['dbukti']=$dbukti;
			$this->load->view('akt-bankin-multialloc/vlistgiro', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function lebihbayar()

   {

      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = $this->uri->segment(4);
         $iarea      = $this->uri->segment(5);
         $dbukti     = $this->uri->segment(6);
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/lebihbayar/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $config['per_page'] = '10';
          $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
          $group='xxx';
          foreach($query->result() as $row){
            $group=$row->i_customer_groupar;
          }
/*
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupbayar;
         }
*/
         $query = $this->db->query("SELECT   a.i_dt, min(a.v_jumlah), min(a.v_lebih), a.i_area, a.i_customer,
                                    a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2))
                                    from tm_pelunasan_lebih  a, tr_customer_groupar b
                                    where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and i_area='$iarea'
                                    and v_lebih>0 and a.f_pelunasan_cancel='f' and a.d_bukti<='$xdbukti'
                                    group by i_dt, d_bukti, i_area, a.i_customer",false);
/*
         $query = $this->db->query("   SELECT   a.i_dt, min(a.v_jumlah), min(a.v_lebih), a.i_area,
                                             a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2))
                                            from tm_pelunasan_lebih  a, tr_customer_groupbayar b
                                            where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                                            and v_lebih>0 and a.f_pelunasan_cancel='f'
                                            group by i_dt, d_bukti, i_area",false);
*/
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_pelunasan');
         $data['isi']=$this->mmaster->bacapelunasan($icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('akt-bankin-multialloc/vlistpelunasan', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carilebihbayar()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = strtoupper($this->input->post('icustomer', FALSE));
         $iarea      = strtoupper($this->input->post('iarea', FALSE));
         $dbukti     = strtoupper($this->input->post('dbukti', FALSE));
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/lebihbayar/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
          $group='xxx';
          foreach($query->result() as $row){
            $group=$row->i_customer_groupar;
          }
         $query   = $this->db->query("SELECT a.i_dt, min(a.v_jumlah), min(a.v_lebih), a.i_area, a.i_customer,
                                             a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2))
                                            from tm_pelunasan_lebih  a, tr_customer_groupar b
                                            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and i_area='$iarea'
                                            and v_lebih>0 and a.f_pelunasan_cancel='f' and a.d_bukti<='$xdbukti'
                                            and (upper(a.i_pelunasan) like '%$cari%') 
                                            group by i_dt, d_bukti, i_area, a.i_customer",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);
         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_pelunasan');
         $data['isi']=$this->mmaster->caripelunasan($cari,$icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('akt-bankin-multialloc/vlistpelunasan', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function kn()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = $this->uri->segment(4);
         $iarea      = $this->uri->segment(5);
         $dbukti     = $this->uri->segment(6);
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/kn/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
        $group='xxx';
        foreach($query->result() as $row){
          $group=$row->i_customer_groupbayar;
        }
/*
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupbayar;
         }
*/
         /*$query = $this->db->query(" select a.* from tm_kn a, tr_customer_groupar b
                                            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                            and a.v_sisa>0",false);
         */

         $query = $this->db->query("select a.* from tm_kn a, tr_customer_groupbayar b
                                    where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                                    and a.v_sisa>0 and d_kn<='$xdbukti' and a.f_kn_cancel='f'",false);

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_kn');
         $data['isi']=$this->mmaster->bacakn($icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('akt-bankin-multialloc/vlistkn', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carikn()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = strtoupper($this->input->post('icustomer', FALSE));
         $iarea      = strtoupper($this->input->post('iarea', FALSE));
         $dbukti     = strtoupper($this->input->post('dbukti', FALSE));
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/carikn/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
        $group='xxx';
        foreach($query->result() as $row){
          $group=$row->i_customer_groupar;
        }
         $query   = $this->db->query("select a.* from tm_kn a, tr_customer_groupar b
                                      where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                      and a.v_sisa>0 and (upper(i_kn) like '%$cari%')
                                      and d_kn<='$xdbukti' and a.f_kn_cancel='f' ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_kn');
         $data['isi']=$this->mmaster->carikn($cari,$icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('akt-bankin-multialloc/vlistkn', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function ku()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = $this->uri->segment(4);
         $iarea      = $this->uri->segment(5);
         $dbukti     = $this->uri->segment(6);
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/ku/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $config['per_page'] = '10';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupar;
         }
/*
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupbayar;
         }
*/
         $query = $this->db->query("  select a.* from tm_kum a, tr_customer_groupar b
                                      where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                                      and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
                                      and d_kum<='$xdbukti'",false);
/*
         $query = $this->db->query("   select a.* from tm_kum a, tr_customer_groupbayar b
                              where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                              and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                              and a.f_close='f' ",false);
*/
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_ku');
         $data['isi']=$this->mmaster->bacaku($icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('akt-bankin-multialloc/vlistku', $data);
      }else{
         $this->load->view('awal/index.php');
      }

   }
   function cariku()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari       = strtoupper($this->input->post('cari', FALSE));
         $icustomer  = strtoupper($this->input->post('customer', FALSE));
         $iarea      = strtoupper($this->input->post('area', FALSE));
         $dbukti     = strtoupper($this->input->post('dbukti', FALSE));
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/ku/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $config['per_page'] = '10';

        $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
        $group='xxx';
        foreach($query->result() as $row){
          $group=$row->i_customer_groupbayar;
        }
#        if($group!='G0000'){
             $query = $this->db->query(" select a.* from tm_kum a, tr_customer_groupbayar b
                                  where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                                  and a.v_sisa>0 and d_kum<='$xdbukti'
                                  and a.f_close='f' and a.f_kum_cancel='f'",false);
#        }else{
#          $query = $this->db->query("   select a.* from tm_kum a
#                               where a.i_customer='$icustomer'
#                               and a.i_area='$iarea'
#                               and a.v_sisa>0
#                               and a.f_close='f' ",false);
#        }
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_ku');
#        if($group!='G0000'){
         $data['isi']=$this->mmaster->cariku($cari,$icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
#      }else{
#           $data['isi']=$this->mmaster->bacaku2($icustomer,$iarea,$config['per_page'],$this->uri->segment(7),$group);
#      }
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('akt-bankin-multialloc/vlistku', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function notaupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['baris']    = $this->uri->segment(4);
         $data['iarea']    = $this->uri->segment(5);
         $data['icustomer']= $this->uri->segment(6);
         $baris                = $this->uri->segment(4);
         $iarea                = $this->uri->segment(5);
         $icustomer          = $this->uri->segment(6);
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/notaupdate/'.$baris.'/'.$iarea.'/'.$icustomer.'/index/';
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupbayar;
         }
         $query = $this->db->query("select * from tr_customer_groupbayar where i_customer_groupbayar='$group'" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(7);
         $this->pagination->initialize($config);
         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($iarea,$icustomer,$config['per_page'],$this->uri->segment(7),$group);
         $this->load->view('akt-bankin-multialloc/vlistnotaupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carinotaupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari             = strtoupper($this->input->post('cari', FALSE));
         $baris            = $this->input->post('baris', FALSE)?$this->input->post('baris', FALSE):$this->uri->segment(4);
         $iarea            = $this->input->post('iarea', FALSE)?$this->input->post('iarea', FALSE):$this->uri->segment(5);
         $icustomer        = $this->input->post('icustomer', FALSE)?$this->input->post('icustomer', FALSE):$this->uri->segment(6);
         $data['baris']    = $baris;
         $data['iarea']    = $iarea;
         $data['icustomer']= $icustomer;
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/notaupdate/'.$baris.'/'.$iarea.'/'.$icustomer.'/index/';
      $group='xxx';
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
      foreach($query->result() as $row){
        $group=$row->i_customer_groupbayar;
      }
         $query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer_groupbayar b
                                            where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                                            and a.i_dt='$idt'
                                            and a.i_area='$iarea'
                                            and (upper(a.i_nota) like '%$cari%' or upper(a.i_customer) like '%$cari%')" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(10);
         $this->pagination->initialize($config);
         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->carinota($cari,$iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(10),$group);
         $data['baris']=$baris;
         $this->load->view('akt-bankin-multialloc/vlistnota', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/area/index/';
         $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
         $this->load->view('akt-bankin-multialloc/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/area/index/';
         $iuser   	= $this->session->userdata('user_id');
         $cari    	= $this->input->post('cari', FALSE);
         $cari 		  = strtoupper($cari);
         $query   	= $this->db->query("select * from tr_area
                                  where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                  and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
         $this->load->view('akt-bankin-multialloc/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function view()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $cari    = strtoupper($this->input->post('cari'));
         $dfrom      = $this->input->post('dfrom');
         $dto     = $this->input->post('dto');
         $iarea      = $this->input->post('iarea');
         if($dfrom=='') $dfrom=$this->uri->segment(4);
         if($dto=='') $dto=$this->uri->segment(5);
         if($iarea=='') $iarea   = $this->uri->segment(6);
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
         $sm=PiutangDagang;
         if($iarea!='NA'){
         $query = $this->db->query("select a.i_kbank,a.i_giro
                                    from tm_kbank a, tr_area b, tr_bank c
                                    where a.i_area=b.i_area and a.f_kbank_cancel='false' and a.f_debet=false
                                    and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') and a.v_sisa>0
                                    and a.d_bank <= to_date('$dto','dd-mm-yyyy') and a.i_coa='$sm'
                                    and upper(a.i_area)='$iarea' and a.i_coa_bank=c.i_coa
                                    and (upper(a.i_kbank) like '%$cari%')",false);
        }else{
        $query = $this->db->query("select a.i_kbank,a.i_giro
                                    from tm_kbank a, tr_area b, tr_bank c
                                    where a.i_area=b.i_area and a.f_kbank_cancel='false' and a.f_debet=false
                                    and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') and a.v_sisa>0
                                    and a.d_bank <= to_date('$dto','dd-mm-yyyy') and a.i_coa='$sm'
                                    and a.i_coa_bank=c.i_coa and (upper(a.i_kbank) like '%$cari%')",false);
        }
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(8);
         $this->paginationxx->initialize($config);
         $data['page_title'] = $this->lang->line('bankmultiinalloc');
         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['cari']  = $cari;
         $data['dfrom'] = $dfrom;
         $data['dto']   = $dto;
         $data['iarea'] = $iarea;
         $data['isi']   = $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
         $data['ikbank']   = '';
         $data['ialokasi']   = '';
         $data['c']     = $dfrom.'/'.$dto.'/'.$iarea.'/'.$this->uri->segment(8);
         
         $query=$this->db->query("	select i_periode from tm_periode ",false);
         if ($query->num_rows() > 0){
           foreach($query->result() as $row){
             $data['periode']=$row->i_periode;
           }
         }
         
         $this->load->view('akt-bankin-multialloc/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function remark()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu523')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $baris = $this->uri->segment(4);
         $config['base_url'] = base_url().'index.php/akt-bankin-multialloc/cform/remark/'.$baris.'/';
         $config['per_page'] = '10';
         $query = $this->db->query("   select i_pelunasan_remark from tr_pelunasan_remark",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('akt-bankin-multialloc/mmaster');
         $data['page_title'] = $this->lang->line('list_pelunasan_remark');
         $data['isi']=$this->mmaster->bacaremark($config['per_page'],$this->uri->segment(5));
      $data['baris']=$baris;
         $this->load->view('akt-bankin-multialloc/vlistremark', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
