<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu195')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spmbimport');
#			$data['isi']= directory_map('./pstock/');
      $data['path']='./pstock/';
			$data['isi']= scandir('./pstock/');
			$data['file']='';
			$this->load->view('spmbimport/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu195')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$file    = $this->input->post('namafile', TRUE);
			$ispmbold= $this->input->post('ispmbold', TRUE);
			$data['file']='./pstock/'.$file;
			$data['ispmbold']=$ispmbold;
			$this->load->view('spmbimport/vfile', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu195')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml= $this->input->post('jml', TRUE);
			$this->load->model('spmbimport/mmaster');
  		$ispmbold = strtoupper($this->input->post('spmblama', TRUE));
			$this->db->trans_begin();
			$data['jml']	= $jml;
			$data['kosong']	= 0;
			$tmp[0] 			  = null;
			$ispmb='';
      $i=0;
			for($i=0;$i<$jml;$i++){
				$dspmb = $this->input->post('dspmb'.$i, TRUE);
				if($dspmb!=''){
					$tmp=explode("-",$dspmb);
					$th=$tmp[2];
					$bl=$tmp[1];
					$hr=$tmp[0];
					$dspmb=$th."-".$bl."-".$hr;
					$thbl=$th.$bl;
				}
				$iarea        = $this->input->post('iarea'.$i, TRUE);
				$iproduct	    = $this->input->post('iproduct'.$i, TRUE);
#				$eproductname = $this->input->post('eproductname'.$i, TRUE);
        $query = $this->db->query("	select e_product_name, v_product_retail from tr_product_price 
                                    where i_product='$iproduct' and  i_price_group='00'",false);
  			if($query->num_rows()>0){
          foreach($query->result() as $nm){
            $eproductname=$nm->e_product_name;
            $vunitprice  =$nm->v_product_retail;
          }
        }else{
          $eproductname=$this->input->post('eproductname'.$i, TRUE);
          $vunitprice=$this->input->post('vunitprice'.$i, TRUE);
        }
				$norder	      = $this->input->post('norder'.$i, TRUE);
				$norder 	    = str_replace(',','',$norder);		
#				$vunitprice   = $this->input->post('vunitprice'.$i, TRUE);
#				$vunitprice   = str_replace(',','',$vunitprice);		
				$eremark      = $this->input->post('eremark'.$i, TRUE);
				if($ispmb==''){
				  $ispmb	= $this->mmaster->runningnumber($thbl,$iarea);
				  $this->mmaster->insertheader($ispmb, $dspmb, $iarea, $ispmbold);
				}
				$this->mmaster->insertdetail($ispmb,$iproduct,$eproductname,$norder,$iarea,$vunitprice,$eremark,$i);
			}
			if (
				($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
				$this->load->view('spmbimport/vformgagal');
			}else{
				$this->db->trans_commit();
#				$this->db->trans_rollback();

        $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		=  $this->db->query($sql);
			  if($rs->num_rows>0){
				  foreach($rs->result() as $tes){
					  $ip_address	  = $tes->ip_address;
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }

			  $data['user']	= $this->session->userdata('user_id');
  #			$data['host']	= $this->session->userdata('printerhost');
			  $data['host']	= $ip_address;
			  $data['uri']	= $this->session->userdata('printeruri');
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Transfer SPMB Area:'.$iarea.' No:'.$ispmb;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

				$data['ispmb']=$ispmb;
        if($ispmb!='') $this->load->view('spmbimport/vformsukses',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
