<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu246')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transttbretur');
			$data['iperiode']	= '';
			$this->load->view('transttbretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu246')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			$data['page_title'] = $this->lang->line('transttbretur');
			$data['iperiode']	= $iperiode;

      define ('BOOLEAN_FIELD',   'L');
      define ('CHARACTER_FIELD', 'C');
      define ('DATE_FIELD',      'D');
      define ('NUMBER_FIELD',    'N');
      define ('READ_ONLY',  '0');
      define ('WRITE_ONLY', '1');
      define ('READ_WRITE', '2');
      $db_file = 'spb/00/ttbr'.substr($iperiode,2,4).'.dbf';
      if(file_exists($db_file)){
        @chmod($db_file, 0777);
        @unlink($db_file);
      }
      $dbase_definition = array (
          array ('KODEAREA',  CHARACTER_FIELD,  2),
          array ('NOTTB',  CHARACTER_FIELD,  6),
          array ('TGLTTB',  DATE_FIELD),
          array ('KODELANG',  CHARACTER_FIELD,  5),
          array ('KELOMPOK',  CHARACTER_FIELD,  2),
          array ('KODESALES',  CHARACTER_FIELD,  2),
          array ('NOBBM',  CHARACTER_FIELD,  6),
          array ('TGLBBM',  DATE_FIELD),
          array ('DISC1', NUMBER_FIELD, 7, 2),
          array ('DISC2', NUMBER_FIELD, 7, 2),
          array ('PKP',  BOOLEAN_FIELD),
          array ('PLUSPPN',  BOOLEAN_FIELD),
          array ('PLUSDISC',  BOOLEAN_FIELD),
          array ('JKOTOR', NUMBER_FIELD, 10, 0),
          array ('JPOTONGAN', NUMBER_FIELD, 10, 0),
          array ('JBERSIH', NUMBER_FIELD, 10, 0),
          array ('KET',  CHARACTER_FIELD,  20),
          array ('KET2',  CHARACTER_FIELD,  20),
          array ('BATAL',  BOOLEAN_FIELD),
          array ('TGLTRM1',  DATE_FIELD),
          array ('TGLTRM2',  DATE_FIELD),
          array ('USERID', NUMBER_FIELD, 10, 0),
          array ('TGLBUAT',  DATE_FIELD),
          array ('JAMBUAT',  CHARACTER_FIELD,  10),
          array ('TGLUBAH',  DATE_FIELD),
          array ('JAMUBAH',  CHARACTER_FIELD,  10),
          array ('KODEPROD',  CHARACTER_FIELD,  9),
          array ('KODEPROD2',  CHARACTER_FIELD,  9),
          array ('JUMLAH', NUMBER_FIELD, 6, 0),
          array ('JUMTRM', NUMBER_FIELD, 6, 0),
          array ('HARGASAT', NUMBER_FIELD, 10, 0)
      );
      $create = @ dbase_create($db_file, $dbase_definition)
                or die ("Could not create dbf file <i>$db_file</i>.");
      $id = @ dbase_open ($db_file, READ_WRITE)
            or die ("Could not open dbf file <i>$db_file</i>."); 
      $sql	  = " select a.i_area, a.i_ttb, a.d_ttb, a.i_customer, a.i_salesman, a.i_bbm, a.d_bbm, a.n_ttb_discount1, a.n_ttb_discount2, 
                  a.f_ttb_pkp, a.f_ttb_plusppn, a.f_ttb_plusdiscount, a.v_ttb_gross, a.v_ttb_discounttotal, a.v_ttb_netto, 
                  a.e_ttb_remark as remark1, c.e_alasan_returname as alasan, a.f_ttb_cancel, a.d_receive1, a.d_receive2, a.d_entry, 
                  a.d_update, b.i_product1, b.i_product2, b.n_quantity, a.i_price_group, b.e_ttb_remark as remark2, b.n_quantity_receive, 
                  b.v_unit_price
                  from tm_ttbretur a, tm_ttbretur_item b, tr_alasan_retur c
                  where a.i_area=b.i_area and a.i_ttb=b.i_ttb and a.n_ttb_year=b.n_ttb_year
                  and to_char(a.d_ttb,'yyyymm')= '$iperiode' and a.i_alasan_retur=c.i_alasan_retur
                  order by b.n_item_no";
      $rspb		= $this->db->query($sql);
      foreach($rspb->result() as $rowpb){
        $KODEAREA = $rowpb->i_area;
        $NOTTB    = $rowpb->i_ttb;
        $ipricegroup = $rowpb->i_price_group;
        $TGLTTB   = substr($rowpb->d_ttb,0,4).substr($rowpb->d_ttb,5,2).substr($rowpb->d_ttb,8,2);
        $KODELANG = $rowpb->i_customer;
        $KELOMPOK = '';
        $KODESALES= $rowpb->i_salesman;
        $NOBBM    = substr($rowpb->i_bbm,9,6);
        $TGLBBM   = substr($rowpb->d_bbm,0,4).substr($rowpb->d_bbm,5,2).substr($rowpb->d_bbm,8,2);
        $DISC1    = $rowpb->n_ttb_discount1;
        $DISC2    = $rowpb->n_ttb_discount2;
        if($rowpb->f_ttb_pkp=='f') 
          $PKP= 'F';
        else 
          $PKP= 'T';
        if($rowpb->f_ttb_plusppn=='f') 
          $PLUSPPN= 'F'; 
        else 
          $PLUSPPN= 'T';
        if($rowpb->f_ttb_plusdiscount=='f') 
          $PLUSDISC= 'F'; 
        else 
          $PLUSDISC= 'T';
        $JKOTOR   = $rowpb->v_ttb_gross;
        $JPOTONGAN= $rowpb->v_ttb_discounttotal;
        $JBERSIH  = $rowpb->v_ttb_netto;
        if(trim($rowpb->remark1)==''){
          $KET    = $rowpb->alasan;
        }else{
          $KET    = $rowpb->remark1.' '.$rowpb->alasan;
        }
        $KET2     = $rowpb->remark2;
        if($rowpb->f_ttb_cancel=='f') 
          $BATAL= 'F'; 
        else 
          $BATAL= 'T';
        $TGLTRM1  = substr($rowpb->d_receive1,0,4).substr($rowpb->d_receive1,5,2).substr($rowpb->d_receive1,8,2);
        $TGLTRM2  = substr($rowpb->d_receive2,0,4).substr($rowpb->d_receive2,5,2).substr($rowpb->d_receive2,8,2);
        $USERID   = '';
        $TGLBUAT    = substr($rowpb->d_entry,0,4).substr($rowpb->d_entry,5,2).substr($rowpb->d_entry,8,2);
        if($rowpb->d_entry!=''){
          $JAMBUAT  = substr($rowpb->d_entry,11,2).':'.substr($rowpb->d_entry,14,2).':'.substr($rowpb->d_entry,17,2);
        }else{
          $JAMBUAT  = '';
        }
        $TGLUBAH    = substr($rowpb->d_update,0,4).substr($rowpb->d_update,5,2).substr($rowpb->d_update,8,2);
        if($rowpb->d_update!=''){
          $JAMUBAH    = substr($rowpb->d_update,11,2).':'.substr($rowpb->d_update,14,2).':'.substr($rowpb->d_update,17,2);
        }else{
          $JAMUBAH  = '';
        }
        $KODEPROD = $rowpb->i_product1.$ipricegroup;
        if($rowpb->i_product2!=''){
          $KODEPROD2= $rowpb->i_product2.$ipricegroup;
        }else{
          $KODEPROD2= $rowpb->i_product2;
        }
        $JUMLAH   = $rowpb->n_quantity;
        $JUMTRM   = $rowpb->n_quantity_receive;
        $HARGASAT = $rowpb->v_unit_price;

        $isi = array ($KODEAREA,$NOTTB,$TGLTTB,$KODELANG,$KELOMPOK,$KODESALES,$NOBBM,$TGLBBM,$DISC1,$DISC2,$PKP,$PLUSPPN,
                      $PLUSDISC,$JKOTOR,$JPOTONGAN,$JBERSIH,$KET,$KET2,$BATAL,$TGLTRM1,$TGLTRM2,$USERID,$TGLBUAT,$JAMBUAT,$TGLUBAH,
                      $JAMUBAH,$KODEPROD,$KODEPROD2,$JUMLAH,$JUMTRM,$HARGASAT);
        dbase_add_record ($id, $isi) or die ("Data TIDAK bisa ditambahkan ke file <i>$db_file</i>."); 
      }
      dbase_close($id);
      @chmod($db_file, 0777);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Transfer ke TTB Retur lama Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses']			= true;
			$data['inomor']			= $pesan;
			$this->load->view('nomor',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
