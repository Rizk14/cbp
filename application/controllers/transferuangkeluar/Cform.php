<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu165')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transferk');
			$this->load->model('transferuangkeluar/mmaster');
			$data['ikuk']='';
			$this->load->view('transferuangkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu165')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transferk');
			$this->load->view('transferuangkeluar/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu165')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transferk')." update";
			if(
				($this->uri->segment(4)!='')
			  ){
				$data['ikuk']			= $this->uri->segment(4);
				$data['nkukyear']	= $this->uri->segment(5);
				$data['isupplier']= $this->uri->segment(6);
				$data['dfrom']		= $this->uri->segment(7);
				$data['dto']			= $this->uri->segment(8);
				$ikuk							= $this->uri->segment(4);
				$ikuk			        = str_replace('%20','',$ikuk);
				$nkukyear					= $this->uri->segment(5);
				$isupplier				= $this->uri->segment(6);
				$dfrom						= $this->uri->segment(7);
				$dto							= $this->uri->segment(8);
				$this->load->model("transferuangkeluar/mmaster");
				$data['isi']=$this->mmaster->baca($ikuk,$nkukyear);
		 		$this->load->view('transferuangkeluar/vformupdate',$data);
			}else{
				$this->load->view('transferuangkeluar/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu165')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikuk 	= $this->input->post('ikuk', TRUE);
      $ikuk = str_replace('%20','',$ikuk);
			$dkuk	= $this->input->post('dkuk', TRUE);
			if($dkuk!=''){
				$tmp=explode("-",$dkuk);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkuk=$th."-".$bl."-".$hr;
				$tahun=$th;
			}
			$ebankname			= $this->input->post('ebankname', TRUE);
			$eareaname			= $this->input->post('eareaname', TRUE);
			$isupplier			= $this->input->post('isupplier', TRUE);
			$esuppliername		= $this->input->post('esuppliername', TRUE);
			$eremark			= $this->input->post('eremark', TRUE);
			$vjumlah			= $this->input->post('vjumlah', TRUE);
			$vjumlah			= str_replace(',','',$vjumlah);
			$vsisa				= $this->input->post('vsisa', TRUE);
			$vsisa				= str_replace(',','',$vsisa);
			if (
				($ikuk != '') && ($tahun!='')
			   )
			{
				$this->load->model('transferuangkeluar/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($ikuk,$dkuk,$tahun,$ebankname,$isupplier,
									   $eremark,$vjumlah,$vsisa);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update Transfer Uang Keluar Supplier '.$isupplier.' No:'.$ikuk;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ikuk;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu165')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikuk 	= $this->input->post('ikuk', TRUE);
			$dkuk	= $this->input->post('dkuk', TRUE);
			if($dkuk!=''){
				$tmp=explode("-",$dkuk);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkuk=$th."-".$bl."-".$hr;
				$tahun=$th;
			}
			$ebankname			= $this->input->post('ebankname', TRUE);
			$eareaname			= $this->input->post('eareaname', TRUE);
			$isupplier			= $this->input->post('isupplier', TRUE);
			$esuppliername		= $this->input->post('esuppliername', TRUE);
			$eremark			= $this->input->post('eremark', TRUE);
			$vjumlah			= $this->input->post('vjumlah', TRUE);
			$vjumlah			= str_replace(',','',$vjumlah);
			$vsisa				= $this->input->post('vsisa', TRUE);
			$vsisa				= str_replace(',','',$vsisa);
			if (
				($ikuk != '') && ($tahun!='')
			   )
			{
				$this->load->model('transferuangkeluar/mmaster');
				$this->db->trans_begin();
				$cek=$this->mmaster->cek($ikuk,$tahun);
				if(!$cek){							
					$this->mmaster->insert($ikuk,$dkuk,$tahun,$ebankname,$isupplier,
										   $eremark,$vjumlah,$vsisa);
				}else{
					$nomor="Bukti transfer ".$ikuk." sudah ada, untuk mengedit lewat menu edit Transfer";
				}
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input Transfer Uang Keluar Supplier '.$isupplier.' No:'.$ikuk;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ikuk;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu165')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transferuangkeluar/cform/area/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tr_area ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transferuangkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('transferuangkeluar/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu165')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transferuangkeluar/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_area
									   	where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transferuangkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('transferuangkeluar/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu165')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transferuangkeluar/cform/supplier/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tr_supplier ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transferuangkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('transferuangkeluar/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu165')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/transferuangkeluar/cform/supplier/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tr_supplier where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transferuangkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('transferuangkeluar/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
