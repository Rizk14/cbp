<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu284')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/printcustomerbarukhusus/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('printcustomerbaru'). " (Khusus)";
			$data['cari'] = '';
      		$data['dfrom']= '';
			$data['dto']  = '';
			$data['isi']  = '';
			$this->load->view('printcustomerbarukhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu284')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printcustomerbarukhusus/cform/area/index/';
			$iuser = $this->session->userdata('user_id');
			$area1	= $this->session->userdata('i_area');
			/*$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');*/
						
			if($area1=='00'/* or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'*/){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area in(select i_area from tm_user_area where i_user='$iuser')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('printcustomerbarukhusus/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$iuser);
			$this->load->view('printcustomerbarukhusus/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu284')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$iuser = $this->session->userdata('user_id');
			/*$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');*/
			$config['base_url'] = base_url().'index.php/printcustomerbarukhusus/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' /*or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'*/){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area in(select i_area from tm_user_area where i_user='$iuser'))",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printcustomerbarukhusus/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$iuser);
			$this->load->view('printcustomerbarukhusus/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) && ($this->session->userdata('menu284')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari	  			 = strtoupper($this->input->post('cari'));
			$dfrom				 = $this->input->post('dfrom');
			$dto	  			 = $this->input->post('dto');
			$iarea				 = $this->input->post('iarea');
			if($dfrom=='')$dfrom = $this->uri->segment(4);
			if($dto=='')$dto 	 = $this->uri->segment(5);
			if($iarea=='')$iarea = $this->uri->segment(6);
			$config['base_url']  = base_url().'index.php/printcustomerbarukhusus/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
      
	      if($iarea=='NA'){
				$query = $this->db->query(" select a.i_spb from tr_customer_tmp a, tm_spb b
											where upper(a.i_customer) like '%000' and (upper(a.e_customer_name) like '%$cari%'
											or upper(a.i_spb) like '%$cari%') and a.i_spb=b.i_spb and a.i_area=b.i_area
											and b.d_spb >= to_date('$dfrom','dd-mm-yyyy') and b.d_spb <= to_date('$dto','dd-mm-yyyy')
											",false);
	      }else{
				$query = $this->db->query(" select a.i_spb from tr_customer_tmp a, tm_spb b
											where upper(a.i_customer) like '%000' and (upper(a.e_customer_name) like '%$cari%'
											or upper(a.i_spb) like '%$cari%') and a.i_spb=b.i_spb and a.i_area=b.i_area
											and b.d_spb >= to_date('$dfrom','dd-mm-yyyy') and b.d_spb <= to_date('$dto','dd-mm-yyyy')
											and a.i_area='$iarea'",false);
	      }

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);

			$data['page_title'] 	= $this->lang->line('printcustomerbaru'). " (Khusus)";
			$this->load->model('printcustomerbarukhusus/mmaster');
      		$data['dfrom'] 			= $dfrom;
			$data['dto']  			= $dto;
			$data['iarea'] 			= $iarea;
			$data['isi'] 			= $this->mmaster->bacasemua($iarea,$cari,$config['per_page'],$this->uri->segment(7),$dfrom,$dto);
			$this->load->view('printcustomerbarukhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu284')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dfrom				 = $this->input->post('dfrom');
			$dto	  			 = $this->input->post('dto');
			$iarea				 = $this->input->post('iarea');
			$ispb 				 = $this->uri->segment(4);
      		if($iarea=='')$iarea = $this->uri->segment(5);
      		if($dfrom=='')$dfrom = $this->uri->segment(6);
      		if($dto=='')$dto 	 = $this->uri->segment(7);
			$this->load->model('printcustomerbarukhusus/mmaster');
      		
      		$data['dfrom']		 = $dfrom;
			$data['dto']  		 = $dto;
			$data['iarea']		 = $iarea;
			$data['ispb']		 = $ispb;
			$data['page_title']  = $this->lang->line('printcustomerbaru'). " (Khusus)";
			$data['lang']		 = $this->mmaster->bacalang($ispb,$iarea);
#			$data['isi']=$this->mmaster->baca($ispb,$iarea);
#      $data['detail']=$this->mmaster->bacadetail($ispb,$iarea);

      		$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak Pelanggan Baru Area:'.$iarea.' No:'.$ispb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printcustomerbarukhusus/vformrpt', $data);
#      $this->mmaster->close($iarea,$ispb);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu284')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printcustomerbaru'). " (Khusus)";
			$this->load->view('printcustomerbarukhusus/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu284')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printcustomerbarukhusus/cform/index/';
			$cari 				= $this->input->post('cari', FALSE);
			$iarea 				= $this->input->post('iarea', FALSE);
			$cari 				= strtoupper($cari);
			$iuser 				= $this->session->userdata('user_id');
			
			if($iarea=='NA'){
				$query = $this->db->query(" select a.*, b.e_customer_name from tm_spb a, tr_customer b
											where a.i_customer=b.i_customer
											and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
											or upper(a.i_spb) like '%$cari%')
											",false);
			}else{
				$query = $this->db->query(" select a.*, b.e_customer_name from tm_spb a, tr_customer b
											where a.i_customer=b.i_customer
											and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
											or upper(a.i_spb) like '%$cari%')
											and a.i_area in(select i_area from tm_user_area where i_user='$iuser')
											",false);
			}

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('printcustomerbarukhusus/mmaster');
			$data['isi']			= $this->mmaster->cari($cari,$iuser,$iarea,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] 	= $this->lang->line('printcustomerbaru'). " (Khusus)";
			$data['cari'] 			= $cari;
			$data['ispb']			= '';
			$data['detail']			= '';
	 		$this->load->view('printcustomerbarukhusus/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
