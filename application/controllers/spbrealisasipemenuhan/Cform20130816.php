<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu406')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spbrealisasipemenuhan/cform/index/';
			$cari	= strtoupper($this->input->post('cari', FALSE));
			$area1= $this->session->userdata('i_area');
			$area2= $this->session->userdata('i_area2');
			$area3= $this->session->userdata('i_area3');
			$area4= $this->session->userdata('i_area4');
			$area5= $this->session->userdata('i_area5');
			$allarea	= $this->session->userdata('allarea');
			if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
			{
				$query = $this->db->query(" select * from tm_spb 
						inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
						inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman) 
						inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer) 
						inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group) 
						where 
						 (
						 (not tm_spb.i_approve1 isnull 
						  and not tm_spb.i_approve2 isnull 
						  and tm_spb.i_store isnull 
						  and tm_spb.i_store_location isnull
						  and f_spb_stockdaerah='f'
						  and tm_spb.f_spb_cancel='f'
              
						 ) 
						or 
						 (not tm_spb.i_approve1 isnull 
						  and not tm_spb.i_approve2 isnull 
						  and tm_spb.i_store isnull 
						  and tm_spb.i_store_location isnull
						  and f_spb_stockdaerah='t'
						  and tm_spb.f_spb_cancel='f'
						  and (tm_spb.i_area='$area1' or tm_spb.i_area='$area2' 
							or tm_spb.i_area='$area3' or tm_spb.i_area='$area4' 
							or tm_spb.i_area='$area5')
						 )
						 )
              and tm_spb.i_product_group='00'
						  and (upper(tm_spb.i_spb) like '%$cari%' or upper(tm_spb.i_customer) like '%$cari%')
                    order by tm_spb.i_area, tm_spb.i_spb
						",false);
			}else{
				$query = $this->db->query(" select * from tm_spb 
						inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
						inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman) 
						inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer) 
						inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group) 
						where 
							not tm_spb.i_approve1 isnull 
						  and not tm_spb.i_approve2 isnull 
						  and tm_spb.i_store isnull 
						  and tm_spb.i_store_location isnull
						  and f_spb_stockdaerah='t'
						  and tm_spb.f_spb_cancel='f'
              and tm_spb.i_product_group='00'
						  and (upper(tm_spb.i_spb) like '%$cari%' or upper(tm_spb.i_customer) like '%$cari%')
						  and (tm_spb.i_area='$area1' or tm_spb.i_area='$area2' 
							or tm_spb.i_area='$area3' or tm_spb.i_area='$area4' 
							or tm_spb.i_area='$area5')
                  order by tm_spb.i_area, tm_spb.i_spb
						",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('spbrealisasi');
			$data['ispb']='';
			$data['jmlitem'] = ''; 				
			$data['detail']='';
			$this->load->model('spbrealisasipemenuhan/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('spbrealisasipemenuhan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu406')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_spb');
			$this->load->view('spbrealisasipemenuhan/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu406')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spbrealisasi');
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
			  )
			{
				$ispb = $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$query 	= $this->db->query("select i_spb from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispb'] = $ispb;
				$this->load->model('spbrealisasipemenuhan/mmaster');
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea);
		 		$this->load->view('spbrealisasipemenuhan/vmainform',$data);
			}else{
				$this->load->view('spbrealisasipemenuhan/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu406')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb 		= $this->input->post('ispb', TRUE);
			$dspb 		= $this->input->post('dspb', TRUE);
			$dsj 			= $this->input->post('dsj', TRUE);
			if($dsj=='')$dsj=null;
			$isj=null;
			if($dspb!=''){
				$tmp=explode("-",$dspb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspb=$th."-".$bl."-".$hr;
			}
			$iarea			= $this->input->post('iarea', TRUE);
			$istore			= $this->input->post('istore', TRUE);
			$istorelocation		= $this->input->post('istorelocation', TRUE);
			$estorename		= $this->input->post('estorename', TRUE);
			$isalesman 		= $this->input->post('isalesman', TRUE);
			$icustomer		= $this->input->post('icustomer', TRUE);
			if($istore=='AA'){
				$fspbstockdaerah	= 'f';
				$fspbsiapnotagudang	= 'f';
				$fspbsiapnotasales	= 'f';
			}else{
				$fspbstockdaerah	= 't';
				$fspbsiapnotagudang	= 't';
				$fspbsiapnotasales	= 't';
			}

			$ispbpo			= $this->input->post('ispbpo', TRUE);
			$ipricegroup		= $this->input->post('ipricegroup', TRUE);
			$dspbreceive		= $this->input->post('$dspbreceive', TRUE);
			$fspbop			= 'f';
			$ecustomerpkpnpwp	= $this->input->post('ecustomerpkpnpwp',TRUE);
			if($ecustomerpkpnpwp!=''){
				$fspbpkp		= 't';
			}else{
				$fspbpkp		= 'f';
				$ecustomerpkpnpwp=null;
			}
			$fspbconsigment		= $this->input->post('fspbconsigment',TRUE);
			if($fspbconsigment!='')
				$fspbconsigment = "t";
			else
				$fspbconsigment = "f";
			$fspbplusppn		  = $this->input->post('fspbplusppn',TRUE);
			$fspbplusdiscount	= $this->input->post('fspbplusdiscount',TRUE);
			$nspbtoplength		= $this->input->post('nspbtoplength', TRUE);
			$nspbtoplength		= str_replace(',','',$nspbtoplength);

			$fspbvalid		    = 'f';
			$fspbcancel		    = 'f';
			$nspbdiscount1		= $this->input->post('ncustomerdiscount1',TRUE);
			$nspbdiscount2		= $this->input->post('ncustomerdiscount2',TRUE);
			$nspbdiscount3		= $this->input->post('ncustomerdiscount3',TRUE);
			$vspbdiscount1		= $this->input->post('vcustomerdiscount1',TRUE);
			$vspbdiscount2		= $this->input->post('vcustomerdiscount2',TRUE);
			$vspbdiscount3		= $this->input->post('vcustomerdiscount3',TRUE);
			$vspbdiscounttotal	= $this->input->post('vspbdiscounttotal',TRUE);
			$vspbdiscounttotalafter	= $this->input->post('vspbdiscounttotalafter',TRUE);
      if($vspbdiscounttotalafter=='')$vspbdiscounttotalafter=0;
			$vspbgross		    = $this->input->post('vspb',TRUE);
			$vspbafter		    = $this->input->post('vspbafter',TRUE);
      if($vspbafter=='')$vspbafter=0;
			$nspbdiscount1		= str_replace(',','',$nspbdiscount1);
			$nspbdiscount2		= str_replace(',','',$nspbdiscount2);
			$nspbdiscount3		= str_replace(',','',$nspbdiscount3);
			$vspbdiscount1		= str_replace(',','',$vspbdiscount1);
			$vspbdiscount2		= str_replace(',','',$vspbdiscount2);
			$vspbdiscount3		= str_replace(',','',$vspbdiscount3);
			$vspbdiscounttotal	= str_replace(',','',$vspbdiscounttotal);
			$vspbgross		= str_replace(',','',$vspbgross);
			$vspbdiscounttotalafter	= str_replace(',','',$vspbdiscounttotalafter);
			$vspbafter		= str_replace(',','',$vspbafter);
			$vspbnetto		= $vspbgross-$vspbdiscounttotal;
			$vspb			= $vspbnetto;
			$jml			= $this->input->post('jml', TRUE);
			if($estorename!='')
			{
				$this->db->trans_begin();
				$this->load->model('spbrealisasipemenuhan/mmaster');
				if($istore=='AA'){
					$this->mmaster->updateheader($dspb, $icustomer, $ispbpo, $nspbtoplength, $isalesman, 
								                       $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp, 
								                       $fspbplusppn, $fspbplusdiscount, $fspbvalid,
								                       $nspbdiscount1, $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, 
								                       $vspbdiscount2, $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment,
								                       $ispb,$iarea,$istore,$istorelocation,$fspbstockdaerah,
								                       $fspbsiapnotagudang, $fspbcancel, $fspbvalid,$fspbsiapnotasales,
								                       $vspbdiscounttotalafter,$vspbafter);
					$dbbk			        = $dspb;
					$istore			      = 'AA';
					$istorelocation	  = '01';
					$istorelocationbin= '00';
					$eremark		      = 'SPB';
					$ibbktype		      = '05';
				}else{
					$isj	= '';
					if($dsj!=''){
						$tmp=explode("-",$dsj);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dsj=$th."-".$bl."-".$hr;
					}
					$this->mmaster->updateheadernsj($ispb,$iarea,$istore,$istorelocation,$fspbstockdaerah,
									$fspbsiapnotagudang, $fspbcancel, $fspbvalid,$fspbsiapnotasales,$isj,$dsj);
				}
				$langsung=true;
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductstatus	= $this->input->post('iproductstatus'.$i, TRUE);
				  $iproductgrade  = $this->input->post('grade'.$i, TRUE);
				  $eproductname	  = $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		  = $this->input->post('vproductretail'.$i, TRUE);
				  $vunitprice		  = str_replace(',','',$vunitprice);
				  $norder			    = $this->input->post('norder'.$i, TRUE);
				  $norder			    = str_replace(',','',$norder);
				  $ndeliver			  = $this->input->post('ndeliver'.$i, TRUE);
				  $ndeliver			  = str_replace(',','',$ndeliver);
				  $nstock			    = $this->input->post('nstock'.$i, TRUE);
				  $nstock			    = str_replace(',','',$nstock);
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
          $eremark        = $this->input->post('eremark'.$i, TRUE);
				  $this->mmaster->deletedetail($iproduct, $iarea, $iproductgrade, $ispb, $iproductmotif);
				  $this->mmaster->insertdetail($ispb,$iarea,$iproduct,$iproductgrade,$eproductname,$norder,
											   $vunitprice,$ndeliver,$iproductmotif,$nstock,$eremark,$i,$iproductstatus);
				  if( ($ndeliver<$norder) && ($iproductstatus!='4') ) $langsung=false;
				}
				if($langsung==true) $this->mmaster->langsungnota($ispb,$iarea);
				if ( ($this->db->trans_status() === FALSE) )
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Realisasi SPB Area '.$iarea.' No:'.$ispb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );
					$data['sukses']			= true;
					$data['inomor']			= $ispb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function isistore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu406')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spbrealisasi');
			if($this->input->post('ispbedit')){
				$ispb = $this->input->post('ispbedit');
				$iarea	= $this->input->post('iareaedit');				
				$query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispb'] = $ispb;
				$this->load->model('spbrealisasipemenuhan/mmaster');
				$data['isi']=$this->mmaster->baca($ispb);
				$data['detail']=$this->mmaster->bacadetail($ispb);
		 		$this->load->view('spbrealisasipemenuhan/vmainform',$data);
			}else{
				$this->load->view('spbrealisasipemenuhan/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu406')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');			
			
			$data['jml']=$this->uri->segment(4);
			$jml =$this->uri->segment(4);
			$data['spb']=$this->uri->segment(5);
			$spb =$this->uri->segment(5);
			$data['stockdaerah']=$this->uri->segment(6);
			$stockdaerah =$this->uri->segment(6);
			$data['area']=$this->uri->segment(7);
			$area =$this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/spbrealisasipemenuhan/cform/store/'.$jml.'/'.$spb.'/'.$stockdaerah.'/'.$area.'/index/';
			if($stockdaerah=='f'){
			$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
									   where a.i_store in(select i_store from tr_area 
									   where i_area='00')
									   and a.i_store=b.i_store
									   order by a.i_store",false);
			}else{
			
			$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
									   where a.i_store in(select i_store from tr_area 
									   where i_area='$area')
									   and a.i_store=b.i_store
									   order by a.i_store",false);
			}
			if($query->num_rows()==0){
				$data['area']=$this->session->userdata('i_area');
				$area 		 =$this->session->userdata('i_area');
				if($stockdaerah=='f'){
				$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
										   where a.i_store in(select i_store from tr_area 
										   where i_area='00')
										   and a.i_store=b.i_store
										   order by a.i_store",false);
				}else{
				$query = $this->db->query("sselect a.*, b.* from tr_store a, tr_store_location b
										   where a.i_store in(select i_store from tr_area 
										   where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area3' or i_area='$area4'))
										   and a.i_store=b.i_store
										   order by a.i_store",false);
				}

			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('spbrealisasipemenuhan/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(9),$area,$stockdaerah,$area1,$area2,$area3,$area4,$area5);
			$this->load->view('spbrealisasipemenuhan/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu406')=='t')) ||

			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['jml']=$this->input->post('jml', FALSE);
			$jml =$this->input->post('jml', FALSE);
			$data['spb']=$this->input->post('ispb', FALSE);
			$spb =$this->input->post('ispb', FALSE);
			$data['stockdaerah']=$this->input->post('fspbstockdaerah', FALSE);
			$stockdaerah =$this->input->post('fspbstockdaerah', FALSE);
			$config['base_url'] = base_url().'index.php/spbrealisasipemenuhan/cform/store/'.$jml.'/'.$spb.'/'.$stockdaerah.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			if($stockdaerah=='f'){
			$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
									   where a.i_store in(select i_store from tr_area 
									   where i_area='00')
									   and a.i_store=b.i_store and (upper(a.i_store) like '%$cari%' 
						      		   or upper(a.e_store_name) like '%$cari%') order by a.i_store",false);
			}else{
			$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
									   where a.i_store in(select i_store from tr_area 
									   where i_area='$area')
									   and a.i_store=b.i_store and (upper(a.i_store) like '%$cari%' 
						      		   or upper(a.e_store_name) like '%$cari%') order by a.i_store",false);
			}
			if($query->num_rows()==0){
				$data['area']=$this->session->userdata('i_area');
				$area 		 =$this->session->userdata('i_area');
				if($stockdaerah=='f'){
				$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
										   where a.i_store in(select i_store from tr_area 
										   where i_area='00')
										   and a.i_store=b.i_store and (upper(a.i_store) like '%$cari%' 
							      		   or upper(a.e_store_name) like '%$cari%')
										   order by a.i_store",false);
				}else{
				$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
										   where a.i_store in(select i_store from tr_area 
										   where i_area='$area')
										   and a.i_store=b.i_store and (upper(a.i_store) like '%$cari%' 
							      		   or upper(a.e_store_name) like '%$cari%')
										   order by a.i_store",false);
				}

			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('spbrealisasipemenuhan/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(8),$stockdaerah);
			$this->load->view('spbrealisasipemenuhan/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu406')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spbrealisasipemenuhan/cform/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$area1= $this->session->userdata('i_area');
			$area2= $this->session->userdata('i_area2');
			$area3= $this->session->userdata('i_area3');
			$area4= $this->session->userdata('i_area4');
			$area5= $this->session->userdata('i_area5');
			$allarea	= $this->session->userdata('allarea');
			if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
			{
				$query = $this->db->query(" select * from tm_spb 
						inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
						inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman) 
						inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer) 
						inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group) 
						where 
						 (
						 (not tm_spb.i_approve1 isnull 
						  and not tm_spb.i_approve2 isnull 
						  and tm_spb.i_store isnull 
						  and tm_spb.i_store_location isnull
						  and f_spb_stockdaerah='f'
						  and tm_spb.f_spb_cancel='f'
						 ) 
						or 
						 (not tm_spb.i_approve1 isnull 
						  and not tm_spb.i_approve2 isnull 
						  and tm_spb.i_store isnull 
						  and tm_spb.i_store_location isnull
						  and f_spb_stockdaerah='t'
						  and tm_spb.f_spb_cancel='f'
						  and (tm_spb.i_area='$area1' or tm_spb.i_area='$area2' 
						    or tm_spb.i_area='$area3' or tm_spb.i_area='$area4' 
						    or tm_spb.i_area='$area5')
						 )
						 )
              and tm_spb.i_product_group='00'
						  and (upper(tm_spb.i_spb) like '%$cari%' or upper(tm_spb.i_customer) like '%$cari%')
						",false);
			}else{
				$query = $this->db->query(" select * from tm_spb 
						inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
						inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman) 
						inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer) 
						inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group) 
						where 
  			      not tm_spb.i_approve1 isnull 
						  and not tm_spb.i_approve2 isnull 
						  and tm_spb.i_store isnull 
						  and tm_spb.i_store_location isnull
						  and f_spb_stockdaerah='t'
						  and tm_spb.f_spb_cancel='f'
              and tm_spb.i_product_group='00'
						  and (upper(tm_spb.i_spb) like '%$cari%' or upper(tm_spb.i_customer) like '%$cari%')
						  and (tm_spb.i_area='$area1' or tm_spb.i_area='$area2' 
							or tm_spb.i_area='$area3' or tm_spb.i_area='$area4' 
							or tm_spb.i_area='$area5')
						",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('spbrealisasipemenuhan/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('spbrealisasi');
			$data['ispb']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('spbrealisasipemenuhan/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
