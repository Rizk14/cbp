<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu316')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exp-ap');
			$data['iperiode']	= '';
			$this->load->view('exp-ap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu316')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-ap/mmaster');
			#$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }

      $a=substr($iperiode,0,4);
	    $b=substr($iperiode,4,2);
		  $peri=mbulan($b)." - ".$a;

      $this->db->select(" a.i_dtap,
									a.i_area,
									a.i_supplier,
									i_pajak,
									a.d_dtap,
									d_bayar,
									d_due_date,
									d_pajak,
									f_pkp,
									n_discount,
									a.v_gross,
									a.v_discount,
									a.v_ppn,
									a.v_netto,
									a.v_sisa,
									f_dtap_cancel,
									n_print,
									a.n_dtap_year,
									min(b.i_do) as i_do, 
									c.e_area_name, 
									d.e_supplier_name
								from tm_dtap a, tm_dtap_item b, tr_area c, tr_supplier d
								where a.i_dtap=b.i_dtap and a.i_area=b.i_area and a.i_supplier=b.i_supplier
									and a.i_area=c.i_area and a.i_supplier=d.i_supplier
									and to_char(a.d_dtap,'yyyymm')='$iperiode'
									and a.f_dtap_cancel='f'
								group by a.i_dtap,
											a.i_area,
											a.i_supplier,
											i_pajak,
											a.d_dtap,
											d_bayar,
											d_due_date,
											d_pajak,
											f_pkp,
											n_discount,
											a.v_gross,
											a.v_discount,
											a.v_ppn,
											a.v_netto,
											a.v_sisa,
											f_dtap_cancel,
											n_print,
											a.n_dtap_year,
											c.e_area_name, 
											d.e_supplier_name
								order by a.d_dtap, a.i_dtap",false);
		  	$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("PEMBELIAN ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'PEMBELIAN '.NmPerusahaan);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,13,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Bulan : '.$peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,13,3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:N5'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Tgl Dok');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'JTempo');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'No Faktur');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'No DO');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Nm Supplier');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Kd Supplier');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Bruto');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Disc');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'PPN');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'JBersih');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Dpp');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'FPajak');
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Tgl Pajak');
				$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

				$i=6;
				$j=6;
        $no=0;

        #$dttemp='';
				foreach($query->result() as $row){
          #if($dttemp!=$row->i_dtap){
            $no++;
            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' => array(
						    'name'	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    )
				    ),
				    'A'.$i.':N'.$i
				    );

            $ap=$row->i_dtap;
            $query = $this->db->query(" select * from tm_dtap where i_dtap='$ap' and i_supplier='$row->i_supplier'
                                        and i_area='$row->i_area'");
            if($query->num_rows()>0){
              foreach($query->result() as $raw){
                $kotor=$raw->v_gross;
                $disc=$raw->v_discount;
                $bersih=$raw->v_netto;
                $ppn=$raw->v_ppn;
              }
            }
            if($disc==0){
              $dpp=$kotor-$disc;
            }
            if($ppn==0){
              $dpp=$bersih;
            }else{
              $dpp=$bersih/1.1;
            }

            if($row->d_dtap){
			        $tmp=explode('-',$row->d_dtap);
			        $tgl=$tmp[2];
			        $bln=$tmp[1];
			        $thn=$tmp[0];
			        $row->d_dtap=$tgl.'-'.$bln.'-'.$thn;
            }
            if($row->d_due_date){
			        $tmp=explode('-',$row->d_due_date);
			        $tgl=$tmp[2];
			        $bln=$tmp[1];
			        $thn=$tmp[0];
			        $row->d_due_date=$tgl.'-'.$bln.'-'.$thn;
            }
            if($row->d_pajak){
			        $tmp=explode('-',$row->d_pajak);
			        $tgl=$tmp[2];
			        $bln=$tmp[1];
			        $thn=$tmp[0];
			        $row->d_pajak=$tgl.'-'.$bln.'-'.$thn;
            }
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->d_dtap, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->d_due_date, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_dtap, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_do, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->i_supplier, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->v_gross, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->v_discount, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->v_ppn, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->v_netto, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $dpp, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->i_pajak, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->d_pajak, Cell_DataType::TYPE_STRING);
					  $i++;
					  $j++;
          #}
          #$dttemp=$row->i_dtap;
				}
				$x=$i-1;
			} 
			  $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $nama='AP'.$iperiode.'.xls';
			  $objWriter->save('excel/00/'.$nama); 
			  $data['sukses'] = true;
			  $data['inomor']	= "Export Laporan AP";
			  $this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
