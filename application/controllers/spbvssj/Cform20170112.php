<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu496')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spbvssj');
			$data['dfrom']	= '';
			$data['dto']	= '';
			$data['isupplier']	= '';
			$this->load->view('spbvssj/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu496')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		
			$userid = $this->session->userdata('user_id');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			// 23-05-2015
			$is_groupbrg = $this->input->post('is_groupbrg');
			
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($is_groupbrg=='') $is_groupbrg=$this->uri->segment(6);
			if($cari=='') $cari=$this->uri->segment(7);
			
			if ($cari == '')
				$cari = "all";

			$config['base_url'] = base_url().'index.php/spbvssj/cform/view/'.$dfrom.'/'.$dto.'/'.$is_groupbrg.'/'.$cari.'/';
			
			// -----------------------------------------------------------------------------------------------------
			//cekareapusat
			$isadapusat = 0; $areanya = "";
										
			if ($cari == "all")
				$keywordcari = "";
			else
				$keywordcari = $cari;
			$sqlnya	= $this->db->query(" SELECT a.* FROM tm_user_area a, tr_area b 
										WHERE a.i_area = b.i_area AND a.i_user = '$userid' 
										AND UPPER(b.e_area_name) like '%".strtoupper($keywordcari)."%' 
										AND a.i_area <> 'XX' order by a.i_area  ");
			if ($sqlnya->num_rows() > 0){
				$hasilnya=$sqlnya->result();
				foreach ($hasilnya as $rownya) {
					if ($rownya->i_area == '00')
						$isadapusat = '1';
					$areanya.= $rownya->i_area.";";
				}
			}
			
			// 25-05-2015
			if ($is_groupbrg == '2')
				$datagrup = '';
			else {
				// ambil grup brg dari tr_product_group, relasi ke tr_product_type
				$queryxx = $this->db->query(" SELECT i_product_group, e_product_groupname FROM tr_product_group 
										ORDER BY i_product_group ");
				if ($queryxx->num_rows() > 0){
					$datagrup = array();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$i_product_group = $rowxx->i_product_group;
						$e_product_groupname = $rowxx->e_product_groupname;
						$datagrup[] = array( 
											'i_product_group'=> $i_product_group,
											'e_product_groupname'=> $e_product_groupname,
											'totalspb'=> 0,
											'totalsj'=> 0,
											'totalnota'=> 0
										);
					}
				}
			} // end if is_groupbrg
			
			// query utk ambil data di tm_spb berdasarkan areanya. Jika isadapusat = 1 maka ga pake filter area
			$outputdata = array();
			if ($isadapusat == '0') {
				$listarea = explode(";", $areanya);
				foreach ($listarea as $rowarea) {
					$rowarea = trim($rowarea);
					if ($rowarea != '') {
						// query ambil nama area
						$queryxx = $this->db->query(" SELECT e_area_name from tr_area where i_area = '$rowarea' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$namaarea = $hasilxx->e_area_name;
						}
						else
							$namaarea = '';
						
						// 23-05-2015 pisah berdasarkan group brg.
						if ($is_groupbrg == '1') {
							$datanilaispb = array();
							$datanilaisj = array();
							$datanilainota = array();
							// ambil grup brg dari tr_product_group, relasi ke tr_product_type
							$queryxx = $this->db->query(" SELECT i_product_group, e_product_groupname FROM tr_product_group 
												ORDER BY i_product_group ");
							if ($queryxx->num_rows() > 0){
								$datadetail = array();
								$hasilxx=$queryxx->result();
								foreach ($hasilxx as $rowxx) {
									$i_product_group = $rowxx->i_product_group;
									$e_product_groupname = $rowxx->e_product_groupname;
									
									// spb
									$queryxx = $this->db->query(" SELECT sum(ax.n_order*ax.v_unit_price) as v_spb_gross from tm_spb a
											  INNER JOIN tm_spb_item ax ON (a.i_spb = ax.i_spb AND a.i_area=ax.i_area)
											  inner join tr_area b on(a.i_area=b.i_area)
											  where 
											  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
											  AND a.i_area = '$rowarea' and a.f_spb_cancel='f'
											  AND ax.i_product IN 
											 ( select i_product FROM tr_product xx INNER JOIN tr_product_type yy ON xx.i_product_type = yy.i_product_type
											   INNER JOIN tr_product_group zz ON zz.i_product_group=yy.i_product_group
											   WHERE zz.i_product_group='$i_product_group'
											  ) ");
											  
									if ($queryxx->num_rows() > 0){
										$hasilxx = $queryxx->row();
										$nilaispb = $hasilxx->v_spb_gross;
									}
									else
										$nilaispb = 0;
									
									$datanilaispb[] = array( 
										'i_product_group'=> $i_product_group,
										'nilaispb'=> $nilaispb
									);
									
									// sj
									$queryxx = $this->db->query(" SELECT sum(ax.n_deliver*ax.v_unit_price) as v_sj_gross from tm_nota a
										  INNER JOIN tm_nota_item ax ON (a.i_sj=ax.i_sj AND a.i_area=ax.i_area)
										  inner join tr_area b on(a.i_area=b.i_area)
										  where 
										  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
										  and a.f_nota_cancel='f' AND a.i_area = '$rowarea'
										  AND ax.i_product IN 
											 ( select i_product FROM tr_product xx INNER JOIN tr_product_type yy ON xx.i_product_type = yy.i_product_type
											   INNER JOIN tr_product_group zz ON zz.i_product_group=yy.i_product_group
											   WHERE zz.i_product_group='$i_product_group'
											  )
										   ");
											  
									if ($queryxx->num_rows() > 0){
										$hasilxx = $queryxx->row();
										$nilaisj = $hasilxx->v_sj_gross;
									}
									else
										$nilaisj = 0;
									
									$datanilaisj[] = array( 
										'i_product_group'=> $i_product_group,
										'nilaisj'=> $nilaisj
									);
									
									// nota
									$queryxx = $this->db->query(" SELECT sum(ax.n_deliver*ax.v_unit_price) as v_nota_gross from tm_nota a
										INNER JOIN tm_nota_item ax ON (a.i_nota=ax.i_nota AND a.i_area=ax.i_area)
										  inner join tr_area b on(a.i_area=b.i_area)
										  where 
										  a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
										  and a.f_nota_cancel='f' and not a.i_nota isnull AND a.i_area = '$rowarea'
										  AND ax.i_product IN 
											 ( select i_product FROM tr_product xx INNER JOIN tr_product_type yy ON xx.i_product_type = yy.i_product_type
											   INNER JOIN tr_product_group zz ON zz.i_product_group=yy.i_product_group
											   WHERE zz.i_product_group='$i_product_group'
											  )
										   ");
											  
									if ($queryxx->num_rows() > 0){
										$hasilxx = $queryxx->row();
										$nilainota = $hasilxx->v_nota_gross;
									}
									else
										$nilainota = 0;
									
									$datanilainota[] = array( 
										'i_product_group'=> $i_product_group,
										'nilainota'=> $nilainota
									);
								} // end foreach
							}
							$outputdata[] = array(	'i_area'=> $rowarea,	
												'namaarea'=> $namaarea,	
												'nilaispb'=> $datanilaispb,
												'nilaisj'=> $datanilaisj,
												'nilainota'=> $datanilainota
												);
							$datanilaispb = array();					
							$datanilaisj = array();
							$datanilainota = array();					
						} // end jika berdasarkan group brg
						
						else {
							$queryxx = $this->db->query(" SELECT sum(a.v_spb) as v_spb_gross from tm_spb a
									  inner join tr_area b on(a.i_area=b.i_area)
									  where 
									  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
									  AND a.i_area = '$rowarea' and a.f_spb_cancel='f' ");
									  
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$nilaispb = $hasilxx->v_spb_gross;
							}
							else
								$nilaispb = 0;
							
							// sj
							$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_sj_gross from tm_nota a
								  inner join tr_area b on(a.i_area=b.i_area)
								  where 
								  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
								  and a.f_nota_cancel='f' AND a.i_area = '$rowarea' ");
									  
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$nilaisj = $hasilxx->v_sj_gross;
							}
							else
								$nilaisj = 0;
							
							// nota
							$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_nota_gross from tm_nota a
								  inner join tr_area b on(a.i_area=b.i_area)
								  where 
								  a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
								  and a.f_nota_cancel='f' and not a.i_nota isnull AND a.i_area = '$rowarea' ");
									  
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$nilainota = $hasilxx->v_nota_gross;
							}
							else
								$nilainota = 0;
							
							$outputdata[] = array(	'i_area'=> $rowarea,	
												'namaarea'=> $namaarea,	
												'nilaispb'=> $nilaispb,
												'nilaisj'=> $nilaisj,
												'nilainota'=> $nilainota
												);
						}

					} // end if
				}
			}
			else {
				$sqlnya	= $this->db->query(" SELECT a.* FROM tm_user_area a, tr_area b 
										WHERE a.i_area = b.i_area AND a.i_user = '$userid' 
										AND UPPER(b.e_area_name) like '%".strtoupper($keywordcari)."%' 
										AND a.i_area <> 'XX' order by a.i_area ");
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						// query ambil nama area
						$queryxx = $this->db->query(" SELECT e_area_name from tr_area where i_area = '".$rownya->i_area."' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$namaarea = $hasilxx->e_area_name;
						}
						else
							$namaarea = '';
						
						// =================================================================================================
						// 25-05-2015 pisah berdasarkan group brg.
						if ($is_groupbrg == '1') {
							$datanilaispb = array();
							$datanilaisj = array();
							$datanilainota = array();
							// ambil grup brg dari tr_product_group, relasi ke tr_product_type
							$queryxx = $this->db->query(" SELECT i_product_group, e_product_groupname FROM tr_product_group 
												ORDER BY i_product_group ");
							if ($queryxx->num_rows() > 0){
								$datadetail = array();
								$hasilxx=$queryxx->result();
								foreach ($hasilxx as $rowxx) {
									$i_product_group = $rowxx->i_product_group;
									$e_product_groupname = $rowxx->e_product_groupname;
									
									// spb
									$queryxx = $this->db->query(" SELECT sum(ax.n_order*ax.v_unit_price) as v_spb_gross from tm_spb a
											  INNER JOIN tm_spb_item ax ON (a.i_spb = ax.i_spb AND a.i_area=ax.i_area)
											  inner join tr_area b on(a.i_area=b.i_area)
											  where 
											  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
											  AND a.i_area = '".$rownya->i_area."' and a.f_spb_cancel='f'
											  AND ax.i_product IN 
											 ( select i_product FROM tr_product xx INNER JOIN tr_product_type yy ON xx.i_product_type = yy.i_product_type
											   INNER JOIN tr_product_group zz ON zz.i_product_group=yy.i_product_group
											   WHERE zz.i_product_group='$i_product_group'
											  ) ");
											  
									if ($queryxx->num_rows() > 0){
										$hasilxx = $queryxx->row();
										$nilaispb = $hasilxx->v_spb_gross;
									}
									else
										$nilaispb = 0;
									
									$datanilaispb[] = array( 
										'i_product_group'=> $i_product_group,
										'nilaispb'=> $nilaispb
									);
									
									// sj
									$queryxx = $this->db->query(" SELECT sum(ax.n_deliver*ax.v_unit_price) as v_sj_gross from tm_nota a
										  INNER JOIN tm_nota_item ax ON (a.i_sj=ax.i_sj AND a.i_area=ax.i_area)
										  inner join tr_area b on(a.i_area=b.i_area)
										  where 
										  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
										  and a.f_nota_cancel='f' AND a.i_area = '".$rownya->i_area."'
										  AND ax.i_product IN 
											 ( select i_product FROM tr_product xx INNER JOIN tr_product_type yy ON xx.i_product_type = yy.i_product_type
											   INNER JOIN tr_product_group zz ON zz.i_product_group=yy.i_product_group
											   WHERE zz.i_product_group='$i_product_group'
											  )
										   ");
											  
									if ($queryxx->num_rows() > 0){
										$hasilxx = $queryxx->row();
										$nilaisj = $hasilxx->v_sj_gross;
									}
									else
										$nilaisj = 0;
									
									$datanilaisj[] = array( 
										'i_product_group'=> $i_product_group,
										'nilaisj'=> $nilaisj
									);
									
									// nota
									$queryxx = $this->db->query(" SELECT sum(ax.n_deliver*ax.v_unit_price) as v_nota_gross from tm_nota a
										INNER JOIN tm_nota_item ax ON (a.i_nota=ax.i_nota AND a.i_area=ax.i_area)
										  inner join tr_area b on(a.i_area=b.i_area)
										  where 
										  a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
										  and a.f_nota_cancel='f' and not a.i_nota isnull AND a.i_area = '".$rownya->i_area."'
										  AND ax.i_product IN 
											 ( select i_product FROM tr_product xx INNER JOIN tr_product_type yy ON xx.i_product_type = yy.i_product_type
											   INNER JOIN tr_product_group zz ON zz.i_product_group=yy.i_product_group
											   WHERE zz.i_product_group='$i_product_group'
											  )
										   ");
											  
									if ($queryxx->num_rows() > 0){
										$hasilxx = $queryxx->row();
										$nilainota = $hasilxx->v_nota_gross;
									}
									else
										$nilainota = 0;
									
									$datanilainota[] = array( 
										'i_product_group'=> $i_product_group,
										'nilainota'=> $nilainota
									);
								} // end foreach
							}
							$outputdata[] = array(	'i_area'=> $rownya->i_area,	
												'namaarea'=> $namaarea,	
												'nilaispb'=> $datanilaispb,
												'nilaisj'=> $datanilaisj,
												'nilainota'=> $datanilainota
												);
							$datanilaispb = array();					
							$datanilaisj = array();
							$datanilainota = array();					
						} // end jika berdasarkan group brg
						
						else {
							// spb
							$queryxx = $this->db->query(" SELECT sum(a.v_spb) as v_spb_gross from tm_spb a
									  inner join tr_area b on(a.i_area=b.i_area)
									  where 
									  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
									  AND a.i_area = '".$rownya->i_area."' and a.f_spb_cancel='f' ");
									  
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$nilaispb = $hasilxx->v_spb_gross;
							}
							else
								$nilaispb = 0;
							
							// sj
							$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_sj_gross from tm_nota a
								  inner join tr_area b on(a.i_area=b.i_area)
								  where 
								  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
								  and a.f_nota_cancel='f' AND a.i_area = '".$rownya->i_area."' ");
									  
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$nilaisj = $hasilxx->v_sj_gross;
							}
							else
								$nilaisj = 0;
							
							// nota
							$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_nota_gross from tm_nota a
								  inner join tr_area b on(a.i_area=b.i_area)
								  where 
								  a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
								  and a.f_nota_cancel='f' and not a.i_nota isnull AND a.i_area = '".$rownya->i_area."' ");
									  
							if ($queryxx->num_rows() > 0){
								$hasilxx = $queryxx->row();
								$nilainota = $hasilxx->v_nota_gross;
							}
							else
								$nilainota = 0;
									  
							$outputdata[] = array(	'i_area'=> $rownya->i_area,	
													'namaarea'=> $namaarea,	
													'nilaispb'=> $nilaispb,
													'nilaisj'=> $nilaisj,
													'nilainota'=> $nilainota
													);
						}
						
						// ====== end 25-05-2015 =======================================================================
					}
				}
				else
					$outputdata = '';
			}
			//print_r($outputdata); die();
			// -----------------------------------------------------------------------------------------------------
			
			//$config['total_rows'] = $query->num_rows(); 
			$config['total_rows'] = count($outputdata); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('spbvssj/mmaster');
			$data['page_title'] = $this->lang->line('spbvssj');
			if ($cari == "all")
				$data['cari'] = '';
			else
				$data['cari'] = $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			//$data['isi']		= $this->mmaster->bacaperiode($userid,$dfrom,$dto,$config['per_page'],$this->uri->segment(7),$keywordcari);
			$data['isi']		= $this->mmaster->bacaperiode($userid,$dfrom,$dto, $is_groupbrg, $keywordcari);
			//print_r($data['isi']); die();
			//echo count($data['isi']); die();
			
			$data['datagrup'] = $datagrup;
			$data['is_groupbrg']= $is_groupbrg;
			$this->load->view('spbvssj/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	// 20-12-2013
	function detailpersales()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu496')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
					
			$iarea = $this->uri->segment(4);
			$dfrom = $this->uri->segment(5);
			$dto = $this->uri->segment(6);
			$is_groupbrg = $this->uri->segment(7);
			$cari = $this->uri->segment(8);
			
			// -----------------------------------------------------------------------------------------------------
			// query ambil nama area
			$queryxx = $this->db->query(" SELECT e_area_name from tr_area where i_area = '".$iarea."' ");
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$namaarea = $hasilxx->e_area_name;
			}
			else
				$namaarea = '';
							
			// query utk ambil data di tm_spb atau di tm_nota berdasarkan sales di area tsb dan dari range tglnya
			
			/*	$sqlnya	= $this->db->query(" SELECT distinct a.i_salesman, b.e_salesman_name FROM tm_spb a, tr_salesman b 
											where a.i_salesman = b.i_salesman AND a.i_area = '$iarea' 
											AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
											ORDER BY a.i_salesman ");
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						// spb
						$queryxx = $this->db->query(" SELECT sum(a.v_spb) as v_spb_gross from tm_spb a
								  inner join tr_area b on(a.i_area=b.i_area)
								  where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' and a.f_spb_cancel='f' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilaispb = $hasilxx->v_spb_gross;
						}
						else
							$nilaispb = 0;
						
						// sj
						$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_sj_gross from tm_nota a
							  inner join tr_area b on(a.i_area=b.i_area)
							  where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
							  and a.f_nota_cancel='f' 
							  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilaisj = $hasilxx->v_sj_gross;
						}
						else
							$nilaisj = 0;
						
						// nota
						$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_nota_gross from tm_nota a
							  inner join tr_area b on(a.i_area=b.i_area)
							  where 
							  a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
							  and a.f_nota_cancel='f' and not a.i_nota isnull 
							  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilainota = $hasilxx->v_nota_gross;
						}
						else
							$nilainota = 0;
								  
						$outputdata[] = array(	'i_area'=> $iarea,	
												'namaarea'=> $namaarea,	
												'i_salesman'=> $rownya->i_salesman,	
												'e_salesman_name'=> $rownya->e_salesman_name,	
												'nilaispb'=> $nilaispb,
												'nilaisj'=> $nilaisj,
												'nilainota'=> $nilainota
												);
					}
				} */
				//else { 
					// ini utk data salesman yg ga ada acuan SPB/SJ/NOTA
					//$outputdata = '';
				
			// 27-05-2015
			if ($is_groupbrg == '2')
				$datagrup = '';
			else {
				// ambil grup brg dari tr_product_group, relasi ke tr_product_type
				$queryxx = $this->db->query(" SELECT i_product_group, e_product_groupname FROM tr_product_group 
										ORDER BY i_product_group ");
				if ($queryxx->num_rows() > 0){
					$datagrup = array();
					$hasilxx=$queryxx->result();
					foreach ($hasilxx as $rowxx) {
						$i_product_group = $rowxx->i_product_group;
						$e_product_groupname = $rowxx->e_product_groupname;
						$datagrup[] = array( 
											'i_product_group'=> $i_product_group,
											'e_product_groupname'=> $e_product_groupname,
											'totalspb'=> 0,
											'totalsj'=> 0,
											'totalnota'=> 0
										);
					}
				}
			}
			
					$sqlnya	= $this->db->query(" SELECT distinct a.i_salesman, b.e_salesman_name FROM tm_nota a, tr_salesman b 
												where a.i_salesman = b.i_salesman AND a.i_area = '$iarea' 
												AND ((a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) 
												OR (a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')) 
												OR (a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy') ))
												ORDER BY a.i_salesman ");
					if ($sqlnya->num_rows() > 0){
						$hasilnya=$sqlnya->result();
						foreach ($hasilnya as $rownya) {
							if ($is_groupbrg == '2') { // non-grup
								// spb
								$queryxx = $this->db->query(" SELECT sum(a.v_spb) as v_spb_gross from tm_spb a
										  inner join tr_area b on(a.i_area=b.i_area)
										  where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
										  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' and a.f_spb_cancel='f' ");
										  
								if ($queryxx->num_rows() > 0){
									$hasilxx = $queryxx->row();
									$nilaispb = $hasilxx->v_spb_gross;
								}
								else
									$nilaispb = 0;
								
								// sj
								$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_sj_gross from tm_nota a
									  inner join tr_area b on(a.i_area=b.i_area)
									  where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
									  and a.f_nota_cancel='f' 
									  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' ");
										  
								if ($queryxx->num_rows() > 0){
									$hasilxx = $queryxx->row();
									$nilaisj = $hasilxx->v_sj_gross;
								}
								else
									$nilaisj = 0;
								
								// nota
								$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_nota_gross from tm_nota a
									  inner join tr_area b on(a.i_area=b.i_area)
									  where 
									  a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
									  and a.f_nota_cancel='f' and not a.i_nota isnull 
									  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' ");
										  
								if ($queryxx->num_rows() > 0){
									$hasilxx = $queryxx->row();
									$nilainota = $hasilxx->v_nota_gross;
								}
								else
									$nilainota = 0;
										  
								$outputdata[] = array(	'i_area'=> $iarea,	
														'namaarea'=> $namaarea,	
														'i_salesman'=> $rownya->i_salesman,	
														'e_salesman_name'=> $rownya->e_salesman_name,	
														'nilaispb'=> $nilaispb,
														'nilaisj'=> $nilaisj,
														'nilainota'=> $nilainota
														);
							}
							else { // by grup brg
								$datanilaispb = array();
								$datanilaisj = array();
								$datanilainota = array();
								
								// ambil grup brg dari tr_product_group, relasi ke tr_product_type
								$queryxx = $this->db->query(" SELECT i_product_group, e_product_groupname FROM tr_product_group 
													ORDER BY i_product_group ");
								if ($queryxx->num_rows() > 0){
									$datadetail = array();
									$hasilxx=$queryxx->result();
									foreach ($hasilxx as $rowxx) {
										$i_product_group = $rowxx->i_product_group;
										$e_product_groupname = $rowxx->e_product_groupname;
										
										// spb
										$queryxx = $this->db->query(" SELECT sum(ax.n_order*ax.v_unit_price) as v_spb_gross from tm_spb a
												  INNER JOIN tm_spb_item ax ON (a.i_spb = ax.i_spb AND a.i_area=ax.i_area)
												  inner join tr_area b on(a.i_area=b.i_area)
												  where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
												  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."' and a.f_spb_cancel='f' 
												  AND ax.i_product IN 
												 ( select i_product FROM tr_product xx INNER JOIN tr_product_type yy ON xx.i_product_type = yy.i_product_type
												   INNER JOIN tr_product_group zz ON zz.i_product_group=yy.i_product_group
												   WHERE zz.i_product_group='$i_product_group'
												  )
												  ");
										  
										if ($queryxx->num_rows() > 0){
											$hasilxx = $queryxx->row();
											$nilaispb = $hasilxx->v_spb_gross;
										}
										else
											$nilaispb = 0;
										
										$datanilaispb[] = array( 
												'i_product_group'=> $i_product_group,
												'nilaispb'=> $nilaispb
											);
											
										// sj
										$queryxx = $this->db->query(" SELECT sum(ax.n_deliver*ax.v_unit_price) as v_sj_gross from tm_nota a
											  INNER JOIN tm_nota_item ax ON (a.i_sj=ax.i_sj AND a.i_area=ax.i_area)
											  inner join tr_area b on(a.i_area=b.i_area)
											  where a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
											  and a.f_nota_cancel='f' 
											  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."'
											  AND ax.i_product IN 
													 ( select i_product FROM tr_product xx INNER JOIN tr_product_type yy ON xx.i_product_type = yy.i_product_type
													   INNER JOIN tr_product_group zz ON zz.i_product_group=yy.i_product_group
													   WHERE zz.i_product_group='$i_product_group'
													  )
											   ");
												  
										if ($queryxx->num_rows() > 0){
											$hasilxx = $queryxx->row();
											$nilaisj = $hasilxx->v_sj_gross;
										}
										else
											$nilaisj = 0;
										
										$datanilaisj[] = array( 
												'i_product_group'=> $i_product_group,
												'nilaisj'=> $nilaisj
											);
										
										// nota
										$queryxx = $this->db->query(" SELECT sum(ax.n_deliver*ax.v_unit_price) as v_nota_gross from tm_nota a
											  INNER JOIN tm_nota_item ax ON (a.i_nota=ax.i_nota AND a.i_area=ax.i_area)
											  inner join tr_area b on(a.i_area=b.i_area)
											  where 
											  a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
											  and a.f_nota_cancel='f' and not a.i_nota isnull 
											  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$rownya->i_salesman."'
											  AND ax.i_product IN 
													 ( select i_product FROM tr_product xx INNER JOIN tr_product_type yy ON xx.i_product_type = yy.i_product_type
													   INNER JOIN tr_product_group zz ON zz.i_product_group=yy.i_product_group
													   WHERE zz.i_product_group='$i_product_group'
													  )
											   ");
												  
										if ($queryxx->num_rows() > 0){
											$hasilxx = $queryxx->row();
											$nilainota = $hasilxx->v_nota_gross;
										}
										else
											$nilainota = 0;
										
										$datanilainota[] = array( 
												'i_product_group'=> $i_product_group,
												'nilainota'=> $nilainota
											);
									
									} // end foreach
								}
								
								$outputdata[] = array(	'i_area'=> $iarea,	
														'namaarea'=> $namaarea,	
														'i_salesman'=> $rownya->i_salesman,	
														'e_salesman_name'=> $rownya->e_salesman_name,	
														'nilaispb'=> $datanilaispb,
														'nilaisj'=> $datanilaisj,
														'nilainota'=> $datanilainota
														);
							} // end is_groupbrg = 2
						}
					}
					else
						$outputdata = '';
				//} // end if
			
			// -----------------------------------------------------------------------------------------------------
			
			$data['page_title'] = $this->lang->line('spbvssj');
			$data['iarea']		= $iarea;
			$data['namaarea']		= $namaarea;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			// 27-05-2015
			$data['is_groupbrg'] = $is_groupbrg;
			$data['datagrup'] = $datagrup;
			$data['cari']		= $cari;
			$data['isi']		= $outputdata;
			
			// 27-05-2015
			if ($is_groupbrg == '2')
				$this->load->view('spbvssj/vdetailpersales',$data);
			else
				$this->load->view('spbvssj/vdetailpersalespergrup',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	// 21-12-2013
	function detailperdata()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu496')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
					
			$isalesman = $this->uri->segment(4);
			$dfrom = $this->uri->segment(5);
			$dto = $this->uri->segment(6);
			$iarea = $this->uri->segment(7);
			
			// -----------------------------------------------------------------------------------------------------
			// query ambil nama area
			$queryxx = $this->db->query(" SELECT e_area_name from tr_area where i_area = '".$iarea."' ");
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$namaarea = $hasilxx->e_area_name;
			}
			else
				$namaarea = '';
			
			// query ambil nama salesman
			$queryxx = $this->db->query(" SELECT e_salesman_name from tr_salesman where i_salesman = '".$isalesman."' ");
			if ($queryxx->num_rows() > 0){
				$hasilxx = $queryxx->row();
				$namasalesman = $hasilxx->e_salesman_name;
			}
			else
				$namasalesman = '';
							
			// query utk ambil data no SPB, SJ, dan NOTA berdasarkan sales di area tsb
			
			/*	// 1. spb
				$listspb = array();
				$sqlnya	= $this->db->query(" SELECT a.i_spb, a.d_spb, a.i_customer, b.e_customer_name  FROM tm_spb a, tr_customer b 
									where a.i_customer = b.i_customer AND a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
									AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$isalesman."' and a.f_spb_cancel='f'
								  ORDER BY a.d_spb DESC, a.i_spb DESC ");
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						$ispb = $rownya->i_spb;
						$dspb = $rownya->d_spb;
						$icustomer = $rownya->i_customer;
						$ecustomername = $rownya->e_customer_name;
						
						$listspb[] = array(		'ispb'=> $ispb,	
												'dspb'=> $dspb,	
												'icustomer'=> $icustomer,	
												'ecustomername'=> $ecustomername
												);
					} // end for
				} // end if
				else
					$listspb = '';
			
			// 2. sj
				$listsj = array();
				$sqlnya	= $this->db->query(" SELECT a.i_sj, a.d_sj, a.i_customer, b.e_customer_name  FROM tm_nota a, tr_customer b 
									where a.i_customer = b.i_customer AND a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
									AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
								  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$isalesman."' and a.f_nota_cancel='f'
								  ORDER BY a.d_sj DESC, a.i_sj DESC ");
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						$isj = $rownya->i_sj;
						$dsj = $rownya->d_sj;
						$icustomer = $rownya->i_customer;
						$ecustomername = $rownya->e_customer_name;
						
						$listsj[] = array(		'isj'=> $isj,	
												'dsj'=> $dsj,	
												'icustomer'=> $icustomer,	
												'ecustomername'=> $ecustomername
												);
					}
				}
				else
					$listsj = '';
				
			// 3. nota
				$listnota = array();
				$sqlnya	= $this->db->query(" SELECT a.i_nota, a.d_nota, a.i_customer, b.e_customer_name  FROM tm_nota a, tr_customer b 
									where a.i_customer = b.i_customer AND a.d_nota >= to_date('$dfrom','dd-mm-yyyy') 
									AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
								  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$isalesman."' 
								  and a.f_nota_cancel='f' and not a.i_nota isnull ORDER BY a.d_nota DESC, a.i_nota DESC  ");
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						$inota = $rownya->i_nota;
						$dnota = $rownya->d_nota;
						$icustomer = $rownya->i_customer;
						$ecustomername = $rownya->e_customer_name;
						
						$listnota[] = array(	'inota'=> $inota,	
												'dnota'=> $dnota,	
												'icustomer'=> $icustomer,	
												'ecustomername'=> $ecustomername
												);
					}
				}
				else
					$listnota = ''; */
			
			$listdata = array();
				$sqlnya	= $this->db->query(" SELECT a.i_spb, a.d_spb, a.i_sj, a.d_sj, a.i_nota, a.d_nota, a.v_nota_gross,
									a.i_customer, b.e_customer_name  FROM tm_nota a, tr_customer b 
									where a.i_customer = b.i_customer AND (
									(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
									AND a.d_spb <= to_date('$dto','dd-mm-yyyy')) OR
									(a.d_sj >= to_date('$dfrom','dd-mm-yyyy') 
									AND a.d_sj <= to_date('$dto','dd-mm-yyyy')) OR (a.d_nota >= to_date('$dfrom','dd-mm-yyyy') 
									AND a.d_nota <= to_date('$dto','dd-mm-yyyy')))
								  AND a.i_area = '".$iarea."' AND a.i_salesman = '".$isalesman."' and a.f_nota_cancel='f'
								  ORDER BY a.d_spb DESC, a.i_spb DESC ");
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						$ispb = $rownya->i_spb;
						$dspb = $rownya->d_spb;
						$isj = $rownya->i_sj;
						$dsj = $rownya->d_sj;
						$inota = $rownya->i_nota;
						$dnota = $rownya->d_nota;
						$vnotagross = $rownya->v_nota_gross;
						$icustomer = $rownya->i_customer;
						$ecustomername = $rownya->e_customer_name;
						
						$listdata[] = array(		'ispb'=> $ispb,	
												'dspb'=> $dspb,	
												'isj'=> $isj,	
												'dsj'=> $dsj,	
												'inota'=> $inota,	
												'dnota'=> $dnota,	
												'vnotagross'=> $vnotagross,	
												'icustomer'=> $icustomer,	
												'ecustomername'=> $ecustomername
												);
					}
				}
				else
					$listdata = '';
			
			// -----------------------------------------------------------------------------------------------------
			//print_r($listnota); die();
			$data['page_title'] = $this->lang->line('spbvssj');
			$data['iarea']		= $iarea;
			$data['namaarea']	= $namaarea;
			$data['isalesman']		= $isalesman;
			$data['namasalesman']	= $namasalesman;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['listdata']	= $listdata;

			$this->load->view('spbvssj/vdetailperdata',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	// 16-07-2014
	function export_excel()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu496')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		
			$userid = $this->session->userdata('user_id');
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$is_groupbrg		= $this->input->post('is_groupbrg');
			
			// ambil grup brg dari tr_product_group, relasi ke tr_product_type
			$queryxx = $this->db->query(" SELECT i_product_group, e_product_groupname FROM tr_product_group 
									ORDER BY i_product_group ");
			if ($queryxx->num_rows() > 0){
				$datagrup = array();
				$hasilxx=$queryxx->result();
				foreach ($hasilxx as $rowxx) {
					$i_product_group = $rowxx->i_product_group;
					$e_product_groupname = $rowxx->e_product_groupname;
					$datagrup[] = array( 
										'i_product_group'=> $i_product_group,
										'e_product_groupname'=> $e_product_groupname,
										'totalspb'=> 0,
										'totalsj'=> 0,
										'totalnota'=> 0
									);
				}
			}
			
			// -----------------------------------------------------------------------------------------------------
			//cekareapusat
		/*	$isadapusat = 0; $areanya = "";
										
			$sqlnya	= $this->db->query(" SELECT a.* FROM tm_user_area a, tr_area b 
										WHERE a.i_area = b.i_area AND a.i_user = '$userid' 
										AND a.i_area <> 'XX' order by a.i_area  ");
			if ($sqlnya->num_rows() > 0){
				$hasilnya=$sqlnya->result();
				foreach ($hasilnya as $rownya) {
					if ($rownya->i_area == '00')
						$isadapusat = '1';
					$areanya.= $rownya->i_area.";";
				}
			}
			
			// query utk ambil data di tm_spb berdasarkan areanya. Jika isadapusat = 1 maka ga pake filter area
			if ($isadapusat == '0') {
				$outputdata = array();
				$listarea = explode(";", $areanya);
				foreach ($listarea as $rowarea) {
					$rowarea = trim($rowarea);
					if ($rowarea != '') {
						// query ambil nama area
						$queryxx = $this->db->query(" SELECT e_area_name from tr_area where i_area = '$rowarea' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$namaarea = $hasilxx->e_area_name;
						}
						else
							$namaarea = '';
						
						// spb
						$queryxx = $this->db->query(" SELECT sum(a.v_spb) as v_spb_gross from tm_spb a
								  inner join tr_area b on(a.i_area=b.i_area)
								  where 
								  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  AND a.i_area = '$rowarea' and a.f_spb_cancel='f' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilaispb = $hasilxx->v_spb_gross;
						}
						else
							$nilaispb = 0;
						
						// sj
						$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_sj_gross from tm_nota a
							  inner join tr_area b on(a.i_area=b.i_area)
							  where 
							  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
							  and a.f_nota_cancel='f' AND a.i_area = '$rowarea' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilaisj = $hasilxx->v_sj_gross;
						}
						else
							$nilaisj = 0;
						
						// nota
						$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_nota_gross from tm_nota a
							  inner join tr_area b on(a.i_area=b.i_area)
							  where 
							  a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
							  and a.f_nota_cancel='f' and not a.i_nota isnull AND a.i_area = '$rowarea' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilainota = $hasilxx->v_nota_gross;
						}
						else
							$nilainota = 0;
								  
						$outputdata[] = array(	'i_area'=> $rowarea,	
												'namaarea'=> $namaarea,	
												'nilaispb'=> $nilaispb,
												'nilaisj'=> $nilaisj,
												'nilainota'=> $nilainota
												);
					} // end if
				}
			}
			else {
				$sqlnya	= $this->db->query(" SELECT a.* FROM tm_user_area a, tr_area b 
										WHERE a.i_area = b.i_area AND a.i_user = '$userid' 
										AND a.i_area <> 'XX' order by a.i_area ");
				if ($sqlnya->num_rows() > 0){
					$hasilnya=$sqlnya->result();
					foreach ($hasilnya as $rownya) {
						// query ambil nama area
						$queryxx = $this->db->query(" SELECT e_area_name from tr_area where i_area = '".$rownya->i_area."' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$namaarea = $hasilxx->e_area_name;
						}
						else
							$namaarea = '';
							
						// spb
						$queryxx = $this->db->query(" SELECT sum(a.v_spb) as v_spb_gross from tm_spb a
								  inner join tr_area b on(a.i_area=b.i_area)
								  where 
								  a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND a.d_spb <= to_date('$dto','dd-mm-yyyy')
								  AND a.i_area = '".$rownya->i_area."' and a.f_spb_cancel='f' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilaispb = $hasilxx->v_spb_gross;
						}
						else
							$nilaispb = 0;
						
						// sj
						$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_sj_gross from tm_nota a
							  inner join tr_area b on(a.i_area=b.i_area)
							  where 
							  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sj <= to_date('$dto','dd-mm-yyyy')
							  and a.f_nota_cancel='f' AND a.i_area = '".$rownya->i_area."' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilaisj = $hasilxx->v_sj_gross;
						}
						else
							$nilaisj = 0;
						
						// nota
						$queryxx = $this->db->query(" SELECT sum(a.v_nota_gross) as v_nota_gross from tm_nota a
							  inner join tr_area b on(a.i_area=b.i_area)
							  where 
							  a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
							  and a.f_nota_cancel='f' and not a.i_nota isnull AND a.i_area = '".$rownya->i_area."' ");
								  
						if ($queryxx->num_rows() > 0){
							$hasilxx = $queryxx->row();
							$nilainota = $hasilxx->v_nota_gross;
						}
						else
							$nilainota = 0;
								  
						$outputdata[] = array(	'i_area'=> $rownya->i_area,	
												'namaarea'=> $namaarea,	
												'nilaispb'=> $nilaispb,
												'nilaisj'=> $nilaisj,
												'nilainota'=> $nilainota
												);
					}
				}
				else
					$outputdata = '';
			} */
			
			// -----------------------------------------------------------------------------------------------------
			
			$this->load->model('spbvssj/mmaster');
			$query = $this->mmaster->bacaperiode($userid,$dfrom,$dto,$is_groupbrg,"all");
			
			$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='4' align='center'>LAPORAN SPB VS SJ</th>
				 </tr>
				 <tr>
					<th colspan='4' align='center'>Periode: $dfrom s.d $dto</th>
				 </tr></table><br>";
			
			if ($is_groupbrg == '2') { // yg biasa
				$html_data .= "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
					<tr>
						<td>Area</td>
						<td>Nilai SPB</td>
						<td>Nilai SJ</td>
						<td>Nilai Nota</td>
					</tr>";
				
				$totalspb = 0; $totalsj = 0; $totalnota = 0;
				if (is_array($query)) {
					for($a=0;$a<count($query);$a++){
						if ($query[$a]['nilaispb'] == '')
							$query[$a]['nilaispb'] = 0;
						if ($query[$a]['nilaisj'] == '')
							$query[$a]['nilaisj'] = 0;
						if ($query[$a]['nilainota'] == '')
							$query[$a]['nilainota'] = 0;
						
						$totalspb+=$query[$a]['nilaispb'];
						$totalsj+=$query[$a]['nilaisj'];
						$totalnota+=$query[$a]['nilainota'];
						
						$html_data.="<tr>
							<td>".$query[$a]['i_area']." - ".$query[$a]['namaarea']."</td>
							<td>".$query[$a]['nilaispb']."</td>
							<td>".$query[$a]['nilaisj']."</td>
							<td>".$query[$a]['nilainota']."</td>
						</tr>";
					}
				}
				$html_data.= "<tr>
					<td align='center'><b>TOTAL SELURUH AREA/NASIONAL</b></td>
					<td>".$totalspb."</td>
					<td>".$totalsj."</td>
					<td>".$totalnota."</td>
				</tr>";
				$html_data.="</table><br>";
			}
			else {
				$html_data = "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				 <tr>
					<th colspan='7' align='center'>LAPORAN SPB VS SJ</th>
				 </tr>
				 <tr>
					<th colspan='7' align='center'>Periode: $dfrom s.d $dto</th>
				 </tr></table><br>";
			
			$html_data .= "<table border='1' cellpadding= '1' cellspacing = '1' width='100%'>
				<tr>
					<td rowspan='2'>Area</td>
					<td colspan='".count($datagrup)."'>Nilai SPB</td>
					<td colspan='".count($datagrup)."'>Nilai SJ</td>
					<td colspan='".count($datagrup)."'>Nilai Nota</td>
				</tr>
				<tr>";
				
				for($xx=0;$xx<count($datagrup);$xx++){
					$html_data .= "<td>".$datagrup[$xx]['e_product_groupname']."</td>";
				}
				
				for($xx=0;$xx<count($datagrup);$xx++){
					$html_data .= "<td>".$datagrup[$xx]['e_product_groupname']."</td>";
				}
				
				for($xx=0;$xx<count($datagrup);$xx++){
					$html_data .= "<td>".$datagrup[$xx]['e_product_groupname']."</td>";
				}
				
				$html_data.="</tr>";
			
			$totalspb = 0; $totalsj = 0; $totalnota = 0;
			if (is_array($query)) {
				for($a=0;$a<count($query);$a++){
					$spbpergrup = $query[$a]['nilaispb'];
					$sjpergrup = $query[$a]['nilaisj'];
					$notapergrup = $query[$a]['nilainota'];
					
					$html_data.="<tr><td>".$query[$a]['i_area']." - ".$query[$a]['namaarea']."</td>";
					for($j1=0;$j1<count($spbpergrup);$j1++){
						if ($spbpergrup[$j1]['nilaispb'] == '')
							$spbpergrup[$j1]['nilaispb'] = 0;
							
						//$totalspb+=$spbpergrup[$j1]['nilaispb'];
						if ($datagrup[$j1]['i_product_group'] == $spbpergrup[$j1]['i_product_group']) {
							$datagrup[$j1]['totalspb']+= $spbpergrup[$j1]['nilaispb'];
						}
						
						$html_data.= "<td align='right'>".$spbpergrup[$j1]['nilaispb']."</td>";
					}
						
					for($j1=0;$j1<count($sjpergrup);$j1++){
						if ($sjpergrup[$j1]['nilaisj'] == '')
							$sjpergrup[$j1]['nilaisj'] = 0;
						
						//$totalsj+=$sjpergrup[$j1]['nilaisj'];
						if ($datagrup[$j1]['i_product_group'] == $sjpergrup[$j1]['i_product_group']) {
							$datagrup[$j1]['totalsj']+= $sjpergrup[$j1]['nilaisj'];
						}
						
						$html_data.= "<td align='right'>".$sjpergrup[$j1]['nilaisj']."</td>";
					}
						
					for($j1=0;$j1<count($notapergrup);$j1++){
						if ($notapergrup[$j1]['nilainota'] == '')
							$notapergrup[$j1]['nilainota'] = 0;
						
						//$totalnota+=$notapergrup[$j1]['nilainota'];
						if ($datagrup[$j1]['i_product_group'] == $notapergrup[$j1]['i_product_group']) {
							$datagrup[$j1]['totalnota']+= $notapergrup[$j1]['nilainota'];
						}
						$html_data.= "<td align='right'>".$notapergrup[$j1]['nilainota']."</td>";
					}
										
					$html_data.="</tr>";
				}
			}
			$html_data.= "<tr>
				<td align='center'><b>TOTAL SELURUH AREA/NASIONAL</b></td>";
				/*<td colspan='".count($datagrup)."'>".$totalspb."</td>
				<td colspan='".count($datagrup)."'>".$totalsj."</td>
				<td colspan='".count($datagrup)."'>".$totalnota."</td> */
			
				for($xx=0;$xx<count($datagrup);$xx++){
					$html_data.= "<td align='right'><b>".$datagrup[$xx]['totalspb']."</b></td>";
				}
				
				for($xx=0;$xx<count($datagrup);$xx++){
					$html_data.= "<td align='right'><b>".$datagrup[$xx]['totalsj']."</b></td>";
				}
				
				for($xx=0;$xx<count($datagrup);$xx++){
					$html_data.= "<td align='right'><b>".$datagrup[$xx]['totalnota']."</b></td>";
				}
			
			$html_data.="</tr>";
			$html_data.="</table><br>";
			}
			
			$nama_file = "laporan_spbvssj.xls";
			$data = $html_data;
			header('Content-Disposition: attachment; filename="' . $nama_file . '"');
			print_r($data);
			return true;
			
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
