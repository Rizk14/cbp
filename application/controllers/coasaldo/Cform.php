<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu115')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('coasaldo');
			$this->load->model('coasaldo/mmaster');
			$data['icoa']='';
			$data['iperiode']='';
			$this->load->view('coasaldo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu115')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('coasaldo');
			$this->load->view('coasaldo/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu115')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('coasaldo')." update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5)
			  ){
				$data['iperiode'] 	= $this->uri->segment(4);
				$data['icoa']		= $this->uri->segment(5);
				$iperiode 			= $this->uri->segment(4);
				$icoa				= $this->uri->segment(5);
				$this->load->model("coasaldo/mmaster");
				$data['isi']=$this->mmaster->baca($iperiode,$icoa);
		 		$this->load->view('coasaldo/vformupdate',$data);
			}else{
				$this->load->view('coasaldo/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu115')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode		= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodeblx', TRUE);
			$icoa 			= $this->input->post('icoa', TRUE);
			$ecoaname		= $this->input->post('ecoaname', TRUE);
			$vsaldoawal		= str_replace(',','',$this->input->post('vsaldoawal', TRUE));
			$vmutasidebet	= str_replace(',','',$this->input->post('vmutasidebet', TRUE));
			$vmutasikredit	= str_replace(',','',$this->input->post('vmutasikredit', TRUE));
			$vsaldoakhir	= str_replace(',','',$this->input->post('vsaldoakhir', TRUE));
			$ientry			= $this->session->userdata('user_id');
			if (
				(isset($iperiode) && $iperiode != '') && 
				(isset($icoa) && $icoa != '') &&
				(isset($vsaldoawal) && $vsaldoawal != '')
			   )
			{
				$this->load->model('coasaldo/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update(	$iperiode,$icoa,$ecoaname,$vsaldoawal,$vmutasidebet,$vmutasikredit,$vsaldoakhir,$ientry);
				$nomor=$iperiode." - ".$icoa." - ".$ecoaname;
			}
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
          while($row=pg_fetch_assoc($rs)){
	          $ip_address	  = $row['ip_address'];
	          break;
          }
        }else{
          $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
          $now	  = $row['c'];
        }
        $pesan='Update CoA Saldo Periode:'.$iperiode.' CoA:('.$icoa.')-'.$ecoaname;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $nomor;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function coa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu115')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/coasaldo/cform/coa/index/';
			$query = $this->db->query("select * from tr_coa where f_coa_status='t'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('coasaldo/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->bacacoa($config['per_page'],$this->uri->segment(5));
			$this->load->view('coasaldo/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricoa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu115')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/coasaldo/cform/coa/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_coa
									   	 where f_coa_status='t' and (upper(i_coa) like '%$cari%' or upper(e_coa_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('coasaldo/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->caricoa($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('coasaldo/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu115')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode		= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$icoa 			= $this->input->post('icoa', TRUE);
			$ecoaname		= $this->input->post('ecoaname', TRUE);
			$vsaldoawal		= str_replace(',','',$this->input->post('vsaldoawal', TRUE));
			$vmutasidebet	= str_replace(',','',$this->input->post('vmutasidebet', TRUE));
			$vmutasikredit	= str_replace(',','',$this->input->post('vmutasikredit', TRUE));
			$vsaldoakhir	= str_replace(',','',$this->input->post('vsaldoakhir', TRUE));
			$ientry			= $this->session->userdata('user_id');
			if (
				(isset($iperiode) && $iperiode != '') && 
				(isset($icoa) && $icoa != '') &&
				(isset($vsaldoawal) && $vsaldoawal != '')
			   )
			{
				$this->load->model('coasaldo/mmaster');
				$this->db->trans_begin();
				$cek=$this->mmaster->cek(	$iperiode,$icoa);
				if(!$cek){							
					$this->mmaster->insert(	$iperiode,$icoa,$ecoaname,$vsaldoawal,$vmutasidebet,$vmutasikredit,$vsaldoakhir,$ientry);
					$nomor=$iperiode." - ".$icoa." - ".$ecoaname;
				}else{
					$nomor="Saldo periode ".$iperiode." - ".$icoa." sudah ada, untuk mengubah lewat menu update saldo account";
				}
			}
			if ($this->db->trans_status() === FALSE)
			{
		    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
          while($row=pg_fetch_assoc($rs)){
	          $ip_address	  = $row['ip_address'];
	          break;
          }
        }else{
          $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
          $now	  = $row['c'];
        }
        $pesan='Input CoA Saldo Periode:'.$iperiode.' CoA:('.$icoa.')-'.$ecoaname;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $nomor;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
}
?>
