<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exportspbvsnota');
#			$data['iperiode']='';
			$data['dfrom']='';
			$data['dto']='';
			$data['iarea']='';
			$this->load->view('exportspbvsnota-allcv/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu155')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea  	= $this->input->post("iarea");
			$istore  	= $this->input->post("istore");
			$dfrom    = $this->input->post("dfrom");
			$dto      = $this->input->post("dto");
			
			$jml = $this->input->post('jml', TRUE);
			$this->db->trans_begin();
			$this->load->model('exportspbvsnota/mmaster');
			$isi		  = $this->mmaster->bacaperiode($dfrom,$dto,$iarea);
#	    $query=$this->mmaster->bacaperiode($dfrom,$dto,$iarea,$interval);
      $nama='SPB vs NOTA '.$iarea.' Periode '.$dfrom.' '.$dto.'.csv';
      
      if($iarea=='NA'){
        $path = 'excel/'.'Informasi'.'/'.$nama;
      }elseif($iarea!='NA'){
        $path = 'excel/'.$iarea.'/'.$nama;
      }
			$fp	= fopen($path,'w');
      if(count($isi)>0){
        foreach($isi as $row){
                    if($row->n_deliver = null) $row->n_deliver='';
                    if($row->n_spb_discount1 = null) $row->n_spb_discount1='';
                    if($row->n_spb_discount2 = null) $row->n_spb_discount2='';
                    if($row->n_spb_discount3 = null) $row->n_spb_discount3='';
                    if($row->v_spb_disc1 = null) $row->v_spb_disc1='';
                    if($row->v_spb_disc2 = null) $row->v_spb_disc2 ='';
                    if($row->v_spb_disc3 = null) $row->v_spb_disc1 ='';
                    if($row->v_spb_netto = null) $row->v_spb_netto= '';
                    if($row->i_nota = null) $row->i_nota='';
                    if($row->d_nota = null) $row->d_nota='';
                    if($row->nota_kotor = null) $row->nota_kotor='';
                    if($row->n_nota_discount1 = null) $row->n_nota_discount1 = '';
                    if($row->n_nota_discount2 = null) $row->n_nota_discount2  = '';
                    if($row->n_nota_discount3 = null) $row->n_nota_discount3 = '';
                    if($row->v_nota_disc1 = null) $row->v_nota_disc1 = '';
                    if($row->v_nota_disc2 = null) $row->v_nota_disc2 = '';
                    if($row->v_nota_disc3 = null) $row->v_nota_disc3 = '';
                    if($row->v_nota_netto = null) $row->v_nota_netto = '';
					
 					$list=array(
                    $row->i_spb, 
                    $row->d_spb, 
                    $row->i_customer, 
                    $row->e_customer_name, 
                    $row->n_spb_toplength, 
                    $row->i_salesman, 
                    $row->e_salesman_name, 
                    $row->product, 
                    $row->i_product, 
                    $row->e_product_name, 
                    $row->v_unit_price, 
                    $row->n_order, 
                    $row->n_deliver, 
                    $row->spb_kotor, 
                    $row->n_spb_discount1, 
                    $row->n_spb_discount2, 
                    $row->n_spb_discount3, 
                    $row->v_spb_disc1,
                    $row->v_spb_disc2, 
                    $row->v_spb_disc3, 
                    $row->v_spb_netto, 
                    $row->i_nota, 
                    $row->d_nota, 
                    $row->nota_kotor, 
                    $row->n_nota_discount1,
                    $row->n_nota_discount2, 
                    $row->n_nota_discount3, 
                    $row->v_nota_disc1, 
                    $row->v_nota_disc2, 
                    $row->v_nota_disc3, 
                    $row->v_nota_netto,
                    $row->i_supplier, 
                    $row->e_supplier_name, 
                    $row->i_price_group, 
                    $row->i_sj, 
                    $row->d_sj);
					fputcsv($fp, $list,'|');
        }
      }
			fclose($fp);
      
      
      
      
      
      
      
      
      
      
      
      
      
/*      
######################################################################################      
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$iarea 		= $this->input->post('iarea'.$i, TRUE);
					$ikk 		= $this->input->post('ikk'.$i, TRUE);
					$iperiode 	= $this->input->post('iperiode'.$i, TRUE);
					$icoa 		= $this->input->post('icoa'.$i, TRUE);
					$ikendaraan = $this->input->post('ikendaraan'.$i, TRUE);
					$vkk 		= $this->input->post('vkk'.$i, TRUE);
					$dkk 		= $this->input->post('dkk'.$i, TRUE);
					$ecoaname 	= $this->input->post('ecoaname'.$i, TRUE);
					$edescription = $this->input->post('edescription'.$i, TRUE);
					$ejamin 	= $this->input->post('ejamin'.$i, TRUE);
					$ejamout 	= $this->input->post('ejamout'.$i, TRUE);
					$nkm 		= $this->input->post('nkm'.$i, TRUE);
					$dentry 	= $this->input->post('dentry'.$i, TRUE);
					$dupdate 	= $this->input->post('dupdate'.$i, TRUE);
					$fposting 	= $this->input->post('fposting'.$i, TRUE);
					$fclose 	= $this->input->post('fclose'.$i, TRUE);
					$etempat 	= $this->input->post('etempat'.$i, TRUE);
					$fdebet 	= $this->input->post('fdebet'.$i, TRUE);
					$dbukti 	= $this->input->post('dbukti'.$i, TRUE);
					$enamatoko 	= $this->input->post('enamatoko'.$i, TRUE);
					$list=array($iarea,$ikk,$iperiode,$icoa,$ikendaraan,$vkk,$dkk,
							   $ecoaname,$edescription,$ejamin,$ejamout,$nkm,$dentry,
							   $dupdate,$fposting,$fclose,$etempat,$fdebet,$dbukti,$enamatoko);
					fputcsv($fp, $list,'|');
				}
			}
			fclose($fp);*/

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='SPB vs NOTA '.$iarea.' Periode '.$dfrom.' '.$dto;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );
			    $data['sukses']			= true;
			    $this->load->view('status',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('exportspbvsnota/mmaster');
			/*$iarea  	= $this->input->post("iarea");
			$istore  	= $this->input->post("istore");*/
			$dfrom    = $this->input->post("dfrom");
			$dto      = $this->input->post("dto");

			$iarea 	= 'NA';
			$istore	= 'NA';
			/*$dfrom  = '01-08-2018';
			$dto    = '05-08-2018';*/


			$this->load->model('exportspbvsnota/mmaster');
			$data['page_title'] = $this->lang->line('exportspbvsnota');
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['istore']	= $istore;
      		if($dfrom!=''){
		    $tmp=explode("-",$dfrom);
		    $blasal=$tmp[1];
        settype($bl,'integer');
	    }
      $bl=$blasal;
      
			$interval	= $this->mmaster->interval($dfrom,$dto);
      		$data['interval'] = $interval;

			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');

			$objPHPExcel = new PHPExcel();


			$isi		  = $this->mmaster->bacaperiodeOMI($dfrom,$dto,$iarea,$interval);
			$objPHPExcel->getProperties()->setTitle("SPB vs Nota")->setDescription(NmPerusahaan);
			//$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->setActiveSheetIndex(0)->setTitle("SvN OMILAND");

		      if(count($isi)>0){
						include 'include.php';
		      }

			$objPHPExcel->createSheet();

		    $isi		  = $this->mmaster->bacaperiodeAIM($dfrom,$dto,$iarea,$interval);
			$objPHPExcel->getProperties()->setTitle("SPB vs Nota")->setDescription(NmPerusahaan);
			//$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->setActiveSheetIndex(1)->setTitle("SvN AIM");

		      if(count($isi)>0){
						include 'include.php';
		      }

		    $objPHPExcel->createSheet();

		    $isi		  = $this->mmaster->bacaperiodeBINABA($dfrom,$dto,$iarea,$interval);
			$objPHPExcel->getProperties()->setTitle("SPB vs Nota")->setDescription(NmPerusahaan);
			//$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->setActiveSheetIndex(2)->setTitle("SvN BINABA");

		      if(count($isi)>0){
						include 'include.php';
		      }

		    $objPHPExcel->createSheet();

		    $isi		  = $this->mmaster->bacaperiodeBCL($dfrom,$dto,$iarea,$interval);
			$objPHPExcel->getProperties()->setTitle("SPB vs Nota")->setDescription(NmPerusahaan);
			//$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->setActiveSheetIndex(3)->setTitle("SvN BCL");

		      if(count($isi)>0){
						include 'include.php';
		      }

		    $objPHPExcel->createSheet();

		    $isi		  = $this->mmaster->bacaperiodeKPU($dfrom,$dto,$iarea,$interval);
			$objPHPExcel->getProperties()->setTitle("SPB vs Nota")->setDescription(NmPerusahaan);
			//$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->setActiveSheetIndex(4)->setTitle("SvN KPU");

		      if(count($isi)>0){
						include 'include.php';
		      }

		    $objPHPExcel->createSheet();

		    $isi		  = $this->mmaster->bacaperiodePKA($dfrom,$dto,$iarea,$interval);
			$objPHPExcel->getProperties()->setTitle("SPB vs Nota")->setDescription(NmPerusahaan);
			//$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->setActiveSheetIndex(5)->setTitle("SvN PKA");

		      if(count($isi)>0){
						include 'include.php';
		      }

		    $objPHPExcel->createSheet();

		    $isi		  = $this->mmaster->bacaperiodeCKN($dfrom,$dto,$iarea,$interval);
			$objPHPExcel->getProperties()->setTitle("SPB vs Nota")->setDescription(NmPerusahaan);
			//$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->setActiveSheetIndex(6)->setTitle("SvN CKN");

		      if(count($isi)>0){
						include 'include.php';
		      }

		    $objPHPExcel->createSheet();

		    $isi		  = $this->mmaster->bacaperiodeCVKAB($dfrom,$dto,$iarea,$interval);
			$objPHPExcel->getProperties()->setTitle("SPB vs Nota")->setDescription(NmPerusahaan);
			//$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->setActiveSheetIndex(7)->setTitle("SvN CVKAB");

		      if(count($isi)>0){
						include 'include.php';
		      }

		    $objPHPExcel->createSheet();

		    $isi		  = $this->mmaster->bacaperiodeBTN($dfrom,$dto,$iarea,$interval);
			$objPHPExcel->getProperties()->setTitle("SPB vs Nota")->setDescription(NmPerusahaan);
			//$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->setActiveSheetIndex(8)->setTitle("SvN BTN");

		      if(count($isi)>0){
						include 'include.php';
		      }



			/*//$databases = array("OMI","AIM", "BINABA", "BCL", "KPU", "PKA", "CKN", "CVKAB", "BTN");
			$databases = array("OMI","AIM","BINABA");
			$in=0;
			foreach($databases as $dbase){
				$per="bacaperiode$dbase";
			$isi		  = $this->mmaster->$per($dfrom,$dto,$iarea,$interval);
			$objPHPExcel->getProperties()->setTitle("SPB vs Nota")->setDescription(NmPerusahaan);
			//$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->setActiveSheetIndex($in)->setTitle("SvN $dbase");

		      if(count($isi)>0){
						include 'include.php';
		      }
		      $objPHPExcel->createSheet();
		      $in=$in+1;
		}*/



      		/*$objPHPExcel->createSheet();
      		
      		$isi = $this->mmaster->bacaperiodeaim($dfrom,$dto,$iarea,$interval);
			$objPHPExcel->getProperties()->setTitle("SPB vs Nota")->setDescription(NmPerusahaan);

			$objPHPExcel->setActiveSheetIndex(1)->setTitle("SvN AIM");

      if(count($isi)>0){
				include 'include.php';
      }*/



			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='SPB vs NOTA '.$iarea.' Periode '.$dfrom.' '.$dto.'.xls';
      if(file_exists('excel/'.$nama)){
        @chmod('excel/Informasi/'.$nama, 0777);
        unlink('excel/Informasi/'.$nama);
      }
      if($iarea=='NA'){
        $objWriter->save('excel/Informasi'.'/'.$nama);
        @chmod('excel/Informasi/'.$nama, 0777);
      }elseif($iarea!='NA'){
        $objWriter->save('excel/'.$iarea.'/'.$nama);
        @chmod('excel/'.$nama, 0777);
      }

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
		  $data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='EXPORT SPB vs NOTA '.$iarea.' Periode '.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
  } 

	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exportspbvsnota');
			$this->load->view('exportspbvsnota-allcv/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dto		= $this->uri->segment(8);
			$dfrom	= $this->uri->segment(7);
			$iarea	= $this->uri->segment(6);
			$ispb		= $this->uri->segment(5);
			$inota	= $this->uri->segment(4);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$this->load->model('exportspbvsnota/mmaster');
			$this->mmaster->delete($inota,$ispb,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus Penjualan Per Pelanggan dari tanggal:'.$dfrom.' sampai:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$config['base_url'] = base_url().'index.php/exportspbvsnota-allcv/cform/view/'.$inota.'/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_exportspbvsnota_name from tm_nota a, tr_exportspbvsnota b
																	where a.i_exportspbvsnota=b.i_exportspbvsnota 
																	and a.f_ttb_tolak='f'
                                  and a.f_nota_koreksi='f'
																	and not a.i_nota isnull
																	and (upper(a.i_nota) like '%$cari%' 
																		or upper(a.i_spb) like '%$cari%' 
																		or upper(a.i_exportspbvsnota) like '%$cari%' 
																		or upper(b.e_exportspbvsnota_name) like '%$cari%')
																	and a.i_area='$iarea' and
																	a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('exportspbvsnota');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['keyword']	= '';	
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('exportspbvsnota-allcv/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($cari=='') $cari	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/exportspbvsnota-allcv/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_exportspbvsnota_name from tm_nota a, tr_exportspbvsnota b
										where a.i_exportspbvsnota=b.i_exportspbvsnota 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$cari%' 
										  or upper(a.i_spb) like '%$cari%' 
										  or upper(a.i_exportspbvsnota) like '%$cari%' 
										  or upper(b.e_exportspbvsnota_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('exportspbvsnota/mmaster');
			$data['page_title'] = $this->lang->line('exportspbvsnota');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('exportspbvsnota-allcv/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$keyword = strtoupper($this->uri->segment(7));	
			$config['base_url'] = base_url().'index.php/exportspbvsnota-allcv/cform/cariperpages/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$keyword.'/index/';
			$query = $this->db->query(" select a.*, b.e_exportspbvsnota_name from tm_nota a, tr_exportspbvsnota b
										where a.i_exportspbvsnota=b.i_exportspbvsnota 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'                    
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$keyword%' 
										  or upper(a.i_spb) like '%$keyword%' 
										  or upper(a.i_exportspbvsnota) like '%$keyword%' 
										  or upper(b.e_exportspbvsnota_name) like '%$keyword%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('exportspbvsnota/mmaster');
			$data['page_title'] = $this->lang->line('exportspbvsnota');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$keyword);
			$this->load->view('exportspbvsnota-allcv/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exportspbvsnota-allcv/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
        else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exportspbvsnota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('exportspbvsnota-allcv/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exportspbvsnota-allcv/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exportspbvsnota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('exportspbvsnota-allcv/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
