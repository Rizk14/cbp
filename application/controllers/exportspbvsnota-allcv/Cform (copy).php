<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exportspbvsnota');
#			$data['iperiode']='';
			$data['dfrom']='';
			$data['dto']='';
			$data['iarea']='';
			$this->load->view('exportspbvsnota/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu155')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea  	= $this->input->post("iarea");
			$istore  	= $this->input->post("istore");
			$dfrom    = $this->input->post("dfrom");
			$dto      = $this->input->post("dto");
			
			$jml = $this->input->post('jml', TRUE);
			$this->db->trans_begin();
			$this->load->model('exportspbvsnota/mmaster');
			$isi		  = $this->mmaster->bacaperiode($dfrom,$dto,$iarea);
#	    $query=$this->mmaster->bacaperiode($dfrom,$dto,$iarea,$interval);
      $nama='SPB vs NOTA '.$iarea.' Periode '.$dfrom.' '.$dto.'.csv';
      
      if($iarea=='NA'){
        $path = 'excel/'.'Informasi'.'/'.$nama;
      }elseif($iarea!='NA'){
        $path = 'excel/'.$iarea.'/'.$nama;
      }
			$fp	= fopen($path,'w');
      if(count($isi)>0){
        foreach($isi as $row){
                    if($row->n_deliver = null) $row->n_deliver='';
                    if($row->n_spb_discount1 = null) $row->n_spb_discount1='';
                    if($row->n_spb_discount2 = null) $row->n_spb_discount2='';
                    if($row->n_spb_discount3 = null) $row->n_spb_discount3='';
                    if($row->v_spb_disc1 = null) $row->v_spb_disc1='';
                    if($row->v_spb_disc2 = null) $row->v_spb_disc2 ='';
                    if($row->v_spb_disc3 = null) $row->v_spb_disc1 ='';
                    if($row->v_spb_netto = null) $row->v_spb_netto= '';
                    if($row->i_nota = null) $row->i_nota='';
                    if($row->d_nota = null) $row->d_nota='';
                    if($row->nota_kotor = null) $row->nota_kotor='';
                    if($row->n_nota_discount1 = null) $row->n_nota_discount1 = '';
                    if($row->n_nota_discount2 = null) $row->n_nota_discount2  = '';
                    if($row->n_nota_discount3 = null) $row->n_nota_discount3 = '';
                    if($row->v_nota_disc1 = null) $row->v_nota_disc1 = '';
                    if($row->v_nota_disc2 = null) $row->v_nota_disc2 = '';
                    if($row->v_nota_disc3 = null) $row->v_nota_disc3 = '';
                    if($row->v_nota_netto = null) $row->v_nota_netto = '';
					
 					$list=array(
                    $row->i_spb, 
                    $row->d_spb, 
                    $row->i_customer, 
                    $row->e_customer_name, 
                    $row->n_spb_toplength, 
                    $row->i_salesman, 
                    $row->e_salesman_name, 
                    $row->product, 
                    $row->i_product, 
                    $row->e_product_name, 
                    $row->v_unit_price, 
                    $row->n_order, 
                    $row->n_deliver, 
                    $row->spb_kotor, 
                    $row->n_spb_discount1, 
                    $row->n_spb_discount2, 
                    $row->n_spb_discount3, 
                    $row->v_spb_disc1,
                    $row->v_spb_disc2, 
                    $row->v_spb_disc3, 
                    $row->v_spb_netto, 
                    $row->i_nota, 
                    $row->d_nota, 
                    $row->nota_kotor, 
                    $row->n_nota_discount1,
                    $row->n_nota_discount2, 
                    $row->n_nota_discount3, 
                    $row->v_nota_disc1, 
                    $row->v_nota_disc2, 
                    $row->v_nota_disc3, 
                    $row->v_nota_netto,
                    $row->i_supplier, 
                    $row->e_supplier_name, 
                    $row->i_price_group, 
                    $row->i_sj, 
                    $row->d_sj);
					fputcsv($fp, $list,'|');
        }
      }
			fclose($fp);
      
      
      
      
      
      
      
      
      
      
      
      
      
/*      
######################################################################################      
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$iarea 		= $this->input->post('iarea'.$i, TRUE);
					$ikk 		= $this->input->post('ikk'.$i, TRUE);
					$iperiode 	= $this->input->post('iperiode'.$i, TRUE);
					$icoa 		= $this->input->post('icoa'.$i, TRUE);
					$ikendaraan = $this->input->post('ikendaraan'.$i, TRUE);
					$vkk 		= $this->input->post('vkk'.$i, TRUE);
					$dkk 		= $this->input->post('dkk'.$i, TRUE);
					$ecoaname 	= $this->input->post('ecoaname'.$i, TRUE);
					$edescription = $this->input->post('edescription'.$i, TRUE);
					$ejamin 	= $this->input->post('ejamin'.$i, TRUE);
					$ejamout 	= $this->input->post('ejamout'.$i, TRUE);
					$nkm 		= $this->input->post('nkm'.$i, TRUE);
					$dentry 	= $this->input->post('dentry'.$i, TRUE);
					$dupdate 	= $this->input->post('dupdate'.$i, TRUE);
					$fposting 	= $this->input->post('fposting'.$i, TRUE);
					$fclose 	= $this->input->post('fclose'.$i, TRUE);
					$etempat 	= $this->input->post('etempat'.$i, TRUE);
					$fdebet 	= $this->input->post('fdebet'.$i, TRUE);
					$dbukti 	= $this->input->post('dbukti'.$i, TRUE);
					$enamatoko 	= $this->input->post('enamatoko'.$i, TRUE);
					$list=array($iarea,$ikk,$iperiode,$icoa,$ikendaraan,$vkk,$dkk,
							   $ecoaname,$edescription,$ejamin,$ejamout,$nkm,$dentry,
							   $dupdate,$fposting,$fclose,$etempat,$fdebet,$dbukti,$enamatoko);
					fputcsv($fp, $list,'|');
				}
			}
			fclose($fp);*/

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='SPB vs NOTA '.$iarea.' Periode '.$dfrom.' '.$dto;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );
			    $data['sukses']			= true;
			    $this->load->view('status',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('exportspbvsnota/mmaster');
			$iarea  	= $this->input->post("iarea");
			$istore  	= $this->input->post("istore");
			$dfrom    = $this->input->post("dfrom");
			$dto      = $this->input->post("dto");
			$this->load->model('exportspbvsnota/mmaster');
			$data['page_title'] = $this->lang->line('exportspbvsnota');
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['istore']	= $istore;
      if($dfrom!=''){
		    $tmp=explode("-",$dfrom);
		    $blasal=$tmp[1];
        settype($bl,'integer');
	    }
      $bl=$blasal;
      
			$interval	= $this->mmaster->interval($dfrom,$dto);
      $data['interval'] = $interval;
			$isi		  = $this->mmaster->bacaperiode($dfrom,$dto,$iarea,$interval);
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("SPB vs Nota")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      if(count($isi)>0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(15);
		  $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(15);
#				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(11);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:AM1'
				);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'NO SPB');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'TGL SPB');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'KODE LANG');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'NAMA TOKO');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'TOP');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'KODE SALES');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'SALES');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'PRODUK');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'KODE BARANG');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'NAMA BARANG');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('K1', 'HARGA');
			  $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('L1','JML PESAN');
    		$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('M1', 'JML KIRIM');
			  $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('N1', 'NILAI KOTOR SPB');
			  $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('O1', 'DISC SPB 1');
			  $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('P1', 'DISC SPB 2');
			  $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('Q1', 'DISC SPB 3');
			  $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('R1', 'NILAI DISC SPB 1');
			  $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('S1','NILAI DISC SPB 2');
    		$objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('T1', 'NILAI DISC SPB 3');
			  $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('U1','NILAI BERSIH SPB');
    		$objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('V1', 'NO NOTA');
			  $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('W1','TGL NOTA');
    		$objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('X1', 'NILAI NOTA');
			  $objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('Y1','DISC NOTA 1');
    		$objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('Z1', 'DISC NOTA 2');
			  $objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('AA1','DISC NOTA 3');
    		$objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('AB1', 'NILAI DISC NOTA 1');
			  $objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('AC1','NILAI DISC NOTA 2');
    		$objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
				$objPHPExcel->getActiveSheet()->setCellValue('AD1', 'NILAI DISC NOTA 3');
				$objPHPExcel->getActiveSheet()->getStyle('AD1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
    		$objPHPExcel->getActiveSheet()->setCellValue('AE1','NILAI BERSIH NOTA');
    		$objPHPExcel->getActiveSheet()->getStyle('AE1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('AF1','KODE SUPPLIER');
    		$objPHPExcel->getActiveSheet()->getStyle('AF1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
				$objPHPExcel->getActiveSheet()->setCellValue('AG1', 'SUPPLIER');
				$objPHPExcel->getActiveSheet()->getStyle('AG1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
    		$objPHPExcel->getActiveSheet()->setCellValue('AH1','KODE HARGA');
    		$objPHPExcel->getActiveSheet()->getStyle('AH1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
				$objPHPExcel->getActiveSheet()->setCellValue('AI1', 'NO SJ');
				$objPHPExcel->getActiveSheet()->getStyle('AI1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
    		$objPHPExcel->getActiveSheet()->setCellValue('AJ1','TGL SJ');
    		$objPHPExcel->getActiveSheet()->getStyle('AJ1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('AK1','PEMENUHAN STOK');
    		$objPHPExcel->getActiveSheet()->getStyle('AK1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('AL1','STATUS');
    		$objPHPExcel->getActiveSheet()->getStyle('AL1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
		  $objPHPExcel->getActiveSheet()->setCellValue('AM1','KOTA');
    		$objPHPExcel->getActiveSheet()->getStyle('AM1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
        $i=2;
        $j=1;
        foreach($isi as $row){
        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			  array(
				  'font' => array(
					  'name'	=> 'Arial',
					  'bold'  => false,
					  'italic'=> false,
					  'size'  => 10
				  )
			  ),
			  'A'.$i.':AK'.$i
			  );
			  
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_spb, Cell_DataType::TYPE_STRING);
          $tmp=explode("-",$row->d_spb);
          $hr=$tmp[2];
          $bl=$tmp[1];
          $th=$tmp[0];
          $row->d_spb=$hr.'-'.$bl.'-'.$th;
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->d_spb, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_customer, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->n_spb_toplength, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->i_salesman, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->e_salesman_name, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->product, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->i_product, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->e_product_name, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->v_unit_price, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->n_order, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->n_deliver, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->spb_kotor, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $row->n_spb_discount1, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $row->n_spb_discount2, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $row->n_spb_discount3, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->v_spb_disc1, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $row->v_spb_disc2, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $row->v_spb_disc3, Cell_DataType::TYPE_NUMERIC);
        #$objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->n_pajak_print, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $row->v_spb_netto, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('V'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
        if($row->d_nota!=null){
          $tmp=explode("-",$row->d_nota);
          $hr=$tmp[2];
          $bl=$tmp[1];
          $th=$tmp[0];
          $row->d_nota=$hr.'-'.$bl.'-'.$th;}
     		if(
					  ($row->f_spb_cancel == 't') 
				 ){
				$status='Batal';
				}elseif(
					  ($row->i_approve1 == null) && ($row->i_notapprove == null)
				 ){
				$status='Sales';
				}elseif(
				  ($row->i_approve1 == null) && ($row->i_notapprove != null)
				 ){
				$status='Reject (sls)';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 == null) &
				  ($row->i_notapprove == null)
				 ){
				$status='Keuangan';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 == null) && 
				  ($row->i_notapprove != null)
				 ){
				$status='Reject (ar)';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store == null)
				 ){
				$status='Gudang';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
				  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
				 ){
				$status='Pemenuhan SPB';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
				  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
				 ){
				$status='Proses OP';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
				  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
				 ){
				$status='OP Close';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
				  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
				 ){
				$status='Siap SJ (sales)';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
				  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
				 ){
				#			  	$status='Siap SJ (gudang)';
				$status='Siap SJ';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
				  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
				 ){
				$status='Siap SJ';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
				  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
				 ){
				$status='Siap DKB';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
				  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
				 ){
				$status='Siap Nota';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && 
				  ($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
				 ){
				$status='Siap SJ';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) && 
				  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
				 ){
				$status='Siap DKB';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) && 
				  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
				 ){
				$status='Siap Nota';
				}elseif(
				  ($row->i_approve1 != null) && 
					  ($row->i_approve2 != null) &&
					  ($row->i_store != null) && 
				  ($row->i_nota != null) 
				 ){
				$status='Sudah dinotakan';			  
				}elseif(($row->i_nota != null)){
				$status='Sudah dinotakan';
				}else{
				$status='Unknown';		
				}
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('W'.$i, $row->d_nota, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('X'.$i, $row->nota_kotor, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('Y'.$i, $row->n_nota_discount1, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('Z'.$i, $row->n_nota_discount2, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AA'.$i, $row->n_nota_discount3, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AB'.$i, $row->v_nota_disc1, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AC'.$i, $row->v_nota_disc2, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AD'.$i, $row->v_nota_disc3, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AE'.$i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AF'.$i, $row->i_supplier, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AG'.$i, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AH'.$i, $row->i_price_group, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AI'.$i, $row->i_sj, Cell_DataType::TYPE_STRING);
        if($row->d_sj!=null){
          $tmp=explode("-",$row->d_sj);
          $hr=$tmp[2];
          $bl=$tmp[1];
          $th=$tmp[0];
          $row->d_sj=$hr.'-'.$bl.'-'.$th;}
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AJ'.$i, $row->d_sj, Cell_DataType::TYPE_STRING);
        if($row->f_spb_stockdaerah=='f')
        {$pemenuhan="PUSAT";}
        else{$pemenuhan="CABANG";}
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AK'.$i, $pemenuhan, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AL'.$i, $status, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AM'.$i, $row->e_city_name, Cell_DataType::TYPE_STRING);
			
        
#        echo $j.'  '.$row->i_spb.' '.$row->i_product.'<br>';
        $j++;
        $i++;
        }
      }
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='SPB vs NOTA '.$iarea.' Periode '.$dfrom.' '.$dto.'.xls';
      if(file_exists('excel/'.$nama)){
        @chmod('excel/'.$nama, 0777);
        @unlink('excel/'.$nama);
      }
      if($iarea=='NA'){
        $objWriter->save('excel/'.'Informasi'.'/'.$nama);
      }elseif($iarea!='NA'){
        $objWriter->save('excel/'.$iarea.'/'.$nama);
      }

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
		  $data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='EXPORT SPB vs NOTA '.$iarea.'Periode '.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$data['sukses'] = true;
			$data['inomor']	= "EXPORT SPB vs NOTA $iarea Periode $dfrom s/d $dto";
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
  } 

	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exportspbvsnota');
			$this->load->view('exportspbvsnota/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dto		= $this->uri->segment(8);
			$dfrom	= $this->uri->segment(7);
			$iarea	= $this->uri->segment(6);
			$ispb		= $this->uri->segment(5);
			$inota	= $this->uri->segment(4);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$this->load->model('exportspbvsnota/mmaster');
			$this->mmaster->delete($inota,$ispb,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus Penjualan Per Pelanggan dari tanggal:'.$dfrom.' sampai:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$config['base_url'] = base_url().'index.php/exportspbvsnota/cform/view/'.$inota.'/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_exportspbvsnota_name from tm_nota a, tr_exportspbvsnota b
																	where a.i_exportspbvsnota=b.i_exportspbvsnota 
																	and a.f_ttb_tolak='f'
                                  and a.f_nota_koreksi='f'
																	and not a.i_nota isnull
																	and (upper(a.i_nota) like '%$cari%' 
																		or upper(a.i_spb) like '%$cari%' 
																		or upper(a.i_exportspbvsnota) like '%$cari%' 
																		or upper(b.e_exportspbvsnota_name) like '%$cari%')
																	and a.i_area='$iarea' and
																	a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('exportspbvsnota');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['keyword']	= '';	
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('exportspbvsnota/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($cari=='') $cari	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/exportspbvsnota/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_exportspbvsnota_name from tm_nota a, tr_exportspbvsnota b
										where a.i_exportspbvsnota=b.i_exportspbvsnota 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$cari%' 
										  or upper(a.i_spb) like '%$cari%' 
										  or upper(a.i_exportspbvsnota) like '%$cari%' 
										  or upper(b.e_exportspbvsnota_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('exportspbvsnota/mmaster');
			$data['page_title'] = $this->lang->line('exportspbvsnota');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('exportspbvsnota/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$keyword = strtoupper($this->uri->segment(7));	
			$config['base_url'] = base_url().'index.php/exportspbvsnota/cform/cariperpages/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$keyword.'/index/';
			$query = $this->db->query(" select a.*, b.e_exportspbvsnota_name from tm_nota a, tr_exportspbvsnota b
										where a.i_exportspbvsnota=b.i_exportspbvsnota 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'                    
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$keyword%' 
										  or upper(a.i_spb) like '%$keyword%' 
										  or upper(a.i_exportspbvsnota) like '%$keyword%' 
										  or upper(b.e_exportspbvsnota_name) like '%$keyword%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('exportspbvsnota/mmaster');
			$data['page_title'] = $this->lang->line('exportspbvsnota');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$keyword);
			$this->load->view('exportspbvsnota/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exportspbvsnota/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
        else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exportspbvsnota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('exportspbvsnota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exportspbvsnota/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exportspbvsnota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('exportspbvsnota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
