<?php 
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(15);
		  $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(15);
#				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(11);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:AM1'
				);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'NO SPB');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'TGL SPB');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'KODE LANG');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'NAMA TOKO');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'TOP');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'KODE SALES');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'SALES');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'PRODUK');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'KODE BARANG');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'NAMA BARANG');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('K1', 'HARGA');
			  $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('L1','JML PESAN');
    		$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('M1', 'JML KIRIM');
			  $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('N1', 'NILAI KOTOR SPB');
			  $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('O1', 'DISC SPB 1');
			  $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('P1', 'DISC SPB 2');
			  $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('Q1', 'DISC SPB 3');
			  $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('R1', 'NILAI DISC SPB 1');
			  $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('S1','NILAI DISC SPB 2');
    		$objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('T1', 'NILAI DISC SPB 3');
			  $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('U1','NILAI BERSIH SPB');
    		$objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('V1', 'NO NOTA');
			  $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('W1','TGL NOTA');
    		$objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('X1', 'NILAI NOTA');
			  $objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('Y1','DISC NOTA 1');
    		$objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('Z1', 'DISC NOTA 2');
			  $objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('AA1','DISC NOTA 3');
    		$objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('AB1', 'NILAI DISC NOTA 1');
			  $objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('AC1','NILAI DISC NOTA 2');
    		$objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
				$objPHPExcel->getActiveSheet()->setCellValue('AD1', 'NILAI DISC NOTA 3');
				$objPHPExcel->getActiveSheet()->getStyle('AD1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
    		$objPHPExcel->getActiveSheet()->setCellValue('AE1','NILAI BERSIH NOTA');
    		$objPHPExcel->getActiveSheet()->getStyle('AE1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('AF1','KODE SUPPLIER');
    		$objPHPExcel->getActiveSheet()->getStyle('AF1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
				$objPHPExcel->getActiveSheet()->setCellValue('AG1', 'SUPPLIER');
				$objPHPExcel->getActiveSheet()->getStyle('AG1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
    		$objPHPExcel->getActiveSheet()->setCellValue('AH1','KODE HARGA');
    		$objPHPExcel->getActiveSheet()->getStyle('AH1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
				$objPHPExcel->getActiveSheet()->setCellValue('AI1', 'NO SJ');
				$objPHPExcel->getActiveSheet()->getStyle('AI1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
    		$objPHPExcel->getActiveSheet()->setCellValue('AJ1','TGL SJ');
    		$objPHPExcel->getActiveSheet()->getStyle('AJ1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('AK1','PEMENUHAN STOK');
    		$objPHPExcel->getActiveSheet()->getStyle('AK1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('AL1','STATUS');
    		$objPHPExcel->getActiveSheet()->getStyle('AL1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
		  $objPHPExcel->getActiveSheet()->setCellValue('AM1','KOTA');
    		$objPHPExcel->getActiveSheet()->getStyle('AM1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
        $i=2;
        $j=1;
        foreach($isi as $row){
        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			  array(
				  'font' => array(
					  'name'	=> 'Arial',
					  'bold'  => false,
					  'italic'=> false,
					  'size'  => 10
				  )
			  ),
			  'A'.$i.':AK'.$i
			  );
			  
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_spb, Cell_DataType::TYPE_STRING);
          $tmp=explode("-",$row->d_spb);
          $hr=$tmp[2];
          $bl=$tmp[1];
          $th=$tmp[0];
          $row->d_spb=$hr.'-'.$bl.'-'.$th;
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->d_spb, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_customer, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->n_spb_toplength, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->i_salesman, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->e_salesman_name, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->product, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->i_product, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->e_product_name, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->v_unit_price, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->n_order, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->n_deliver, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->spb_kotor, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $row->n_spb_discount1, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $row->n_spb_discount2, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $row->n_spb_discount3, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->v_spb_disc1, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $row->v_spb_disc2, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $row->v_spb_disc3, Cell_DataType::TYPE_NUMERIC);
        #$objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->n_pajak_print, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $row->v_spb_netto, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('V'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
        if($row->d_nota!=null){
          $tmp=explode("-",$row->d_nota);
          $hr=$tmp[2];
          $bl=$tmp[1];
          $th=$tmp[0];
          $row->d_nota=$hr.'-'.$bl.'-'.$th;}
     		if(
					  ($row->f_spb_cancel == 't') 
				 ){
				$status='Batal';
				}elseif(
					  ($row->i_approve1 == null) && ($row->i_notapprove == null)
				 ){
				$status='Sales';
				}elseif(
				  ($row->i_approve1 == null) && ($row->i_notapprove != null)
				 ){
				$status='Reject (sls)';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 == null) &
				  ($row->i_notapprove == null)
				 ){
				$status='Keuangan';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 == null) && 
				  ($row->i_notapprove != null)
				 ){
				$status='Reject (ar)';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store == null)
				 ){
				$status='Gudang';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
				  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')
				 ){
				$status='Pemenuhan SPB';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
				  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')
				 ){
				$status='Proses OP';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
				  ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')
				 ){
				$status='OP Close';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
				  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')
				 ){
				$status='Siap SJ (sales)';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') &&
				  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
				 ){
				#			  	$status='Siap SJ (gudang)';
				$status='Siap SJ';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
				  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)
				 ){
				$status='Siap SJ';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
				  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
				 ){
				$status='Siap DKB';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && 
				  ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)
				 ){
				$status='Siap Nota';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && 
				  ($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)
				 ){
				$status='Siap SJ';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) && 
				  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
				 ){
				$status='Siap DKB';
				}elseif(
				  ($row->i_approve1 != null) && ($row->i_approve2 != null) && 
					  ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) && 
				  ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)
				 ){
				$status='Siap Nota';
				}elseif(
				  ($row->i_approve1 != null) && 
					  ($row->i_approve2 != null) &&
					  ($row->i_store != null) && 
				  ($row->i_nota != null) 
				 ){
				$status='Sudah dinotakan';			  
				}elseif(($row->i_nota != null)){
				$status='Sudah dinotakan';
				}else{
				$status='Unknown';		
				}
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('W'.$i, $row->d_nota, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('X'.$i, $row->nota_kotor, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('Y'.$i, $row->n_nota_discount1, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('Z'.$i, $row->n_nota_discount2, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AA'.$i, $row->n_nota_discount3, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AB'.$i, $row->v_nota_disc1, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AC'.$i, $row->v_nota_disc2, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AD'.$i, $row->v_nota_disc3, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AE'.$i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AF'.$i, $row->i_supplier, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AG'.$i, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AH'.$i, $row->i_price_group, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AI'.$i, $row->i_sj, Cell_DataType::TYPE_STRING);
        if($row->d_sj!=null){
          $tmp=explode("-",$row->d_sj);
          $hr=$tmp[2];
          $bl=$tmp[1];
          $th=$tmp[0];
          $row->d_sj=$hr.'-'.$bl.'-'.$th;}
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AJ'.$i, $row->d_sj, Cell_DataType::TYPE_STRING);
        if($row->f_spb_stockdaerah=='f')
        {$pemenuhan="PUSAT";}
        else{$pemenuhan="CABANG";}
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AK'.$i, $pemenuhan, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AL'.$i, $status, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('AM'.$i, $row->e_city_name, Cell_DataType::TYPE_STRING);
			
        
#        echo $j.'  '.$row->i_spb.' '.$row->i_product.'<br>';
        $j++;
        $i++;
        }
?>