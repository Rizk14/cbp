<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('pkk');
			$data['iarea']='';
			$data['iperiode']='';
			$data['ikk']='';
			$this->load->view('akt-pkk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('pkk');
			$this->load->view('akt-pkk/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('pkk')." update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5) && $this->uri->segment(6)
			  ){
				$ikk 		= $this->uri->segment(4);
				$iperiode	= $this->uri->segment(5);
				$iarea		= $this->uri->segment(6);
				$dfrom		= $this->uri->segment(7);
				$dto		= $this->uri->segment(8);
				$this->load->model("akt-pkk/mmaster");
				$data['isi']=$this->mmaster->baca($ikk,$iperiode,$iarea);
				$data['iarea'] = $iarea;
				$data['dfrom'] = $dfrom;
				$data['dto']   = $dto;

		 		$this->load->view('akt-pkk/vformupdate',$data);
			}else{
				$this->load->view('akt-pkk/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode		= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$ikk			= $this->input->post('ikk', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$vkk			= $this->input->post('vkk', TRUE);
			$vkk			= str_replace(',','',$vkk);
			$edescription	= $this->input->post('edescription', TRUE);
			if($edescription=="") $edescription=null;
			$dkk			= $this->input->post('dkk', TRUE);
			if($dkk!=''){
				$tmp=explode("-",$dkk);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkk=$th."-".$bl."-".$hr;
			}
			$dbukti			= $this->input->post('dbukti', TRUE);
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
			}
#			$icoa		= '111.2'.$iarea;
      $irvtype  	= '00';
			$fdebet		= 'f';
			$icoa			= Penyesuaian;
			if (
				(isset($ikk) && $ikk != '') &&
				(isset($iperiode) && $iperiode != '') && 
				(isset($iarea) && $iarea != '') &&
				(isset($vkk) && ($vkk != '')) &&
				(isset($dkk) && $dkk != '') &&
				(isset($dbukti) && $dbukti != '')
			   )
			{
				$this->load->model('akt-pkk/mmaster');
				$this->db->trans_begin();
				$ecoaname	= $this->mmaster->namaacc($icoa);
				$this->mmaster->update($iarea,$ikk,$iperiode,$icoa,$vkk,$dkk,$dbukti,$ecoaname,$edescription,$fdebet,$dbukti,$irvtype);
				###########posting##########
        $eremark		= $edescription;
        $fclose			= 'f';
			  $this->mmaster->inserttransheader($ikk,$iarea,$eremark,$fclose,$dkk);
			  $this->mmaster->updatekk($ikk,$iarea,$iperiode);
			  if($fdebet=='t'){
				  $accdebet		  = $icoa;
				  $namadebet		= $ecoaname;
				  $acckredit		= KasKecil.$iarea;
				  $namakredit		= $this->mmaster->namaacc($acckredit);
			  }else{
				  $acckredit		= $icoa;
				  $namakredit		= $ecoaname;
				  $accdebet		  = KasKecil.$iarea;
				  $namadebet		= $this->mmaster->namaacc($accdebet);
			  }
			  $this->mmaster->inserttransitemdebet($accdebet,$ikk,$namadebet,'t','t',$iarea,$eremark,$vkk,$dkk);
			  $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vkk);
			  $this->mmaster->inserttransitemkredit($acckredit,$ikk,$namakredit,'f','t',$iarea,$eremark,$vkk,$dkk);
			  $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vkk);
			  $this->mmaster->insertgldebet($accdebet,$ikk,$namadebet,'t',$iarea,$vkk,$dkk,$eremark);
			  $this->mmaster->insertglkredit($acckredit,$ikk,$namakredit,'f',$iarea,$vkk,$dkk,$eremark);
        ###########end of posting##########
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Update Pengisian Kas kecil No:'.$ikk.' Periode:'.$iperiode.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );
					$this->db->trans_commit();
					
					$data['sukses']			= true;
					$data['inomor']			= $ikk;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-kk/cform/area/index/';
			$query = $this->db->query("select * from tr_area",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('akt-pkk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-pkk/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-kk/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_area
									   	 where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-pkk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-pkk/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function coa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-kk/cform/coa/index/';
			$query = $this->db->query("select * from tr_coa where i_coa like '61%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('akt-pkk/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->bacacoa($config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-pkk/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricoa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-kk/cform/coa/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_coa
									   	 where (upper(i_coa) like '61%$cari%' or (upper(e_coa_name) like '%$cari%' and upper(i_coa) like '61%'))",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-pkk/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->caricoa($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-pkk/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kendaraan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/akt-kk/cform/kendaraan/'.$area.'/index/';
			$query = $this->db->query("	select * from tr_kendaraan a
										inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
										inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
										where a.i_area='$area'
										",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('akt-pkk/mmaster');
			$data['page_title'] = $this->lang->line('list_kendaraan');
			$data['isi']=$this->mmaster->bacakendaraan($area,$config['per_page'],$this->uri->segment(6));
			$this->load->view('akt-pkk/vlistkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carikendaraan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/akt-kk/cform/kendaraan/'.$area.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_kendaraan
								   	 	where (upper(i_kendaraan) like '%$cari%' or upper(e_pengguna) like '%$cari%' 
										or upper(e_kendaraan_jenis) like '%$cari%' or upper(e_kendaraan_bbm) like '%$cari%')
										and a.i_area='$area'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('akt-pkk/mmaster');
			$data['page_title'] = $this->lang->line('list_kendaraan');
			$data['isi']=$this->mmaster->carikendaraan($area,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-pkk/vlistkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea		= $this->input->post('iarea', TRUE);
			$iperiode	= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$irvtype  = '00';
			$tah			= substr($this->input->post('iperiodeth', TRUE),2,2);
			$bul			= $this->input->post('iperiodebl', TRUE);
			$vkk			= $this->input->post('vkk', TRUE);
			$vkk			= str_replace(',','',$vkk);
			$edescription	= $this->input->post('edescription', TRUE);
			if($edescription=="") $edescription=null;
			$dkk			= $this->input->post('dkk', TRUE);
			if($dkk!=''){
				$tmp=explode("-",$dkk);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkk=$th."-".$bl."-".$hr;
			}
			$dbukti			= $this->input->post('dbukti', TRUE);
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
			}
			$fdebet		= 'f';
#			$icoa			= '111.100';
			$icoa			= Penyesuaian;
			if (
				(isset($iperiode) && $iperiode != '') && 
				(isset($iarea) && $iarea != '') &&
				(isset($vkk) && (($vkk != 0) || ($vkk != ''))) &&
				(isset($dkk) && $dkk != '') &&
				(isset($dbukti) && $dbukti != '')
			   )
			{
				$this->load->model('akt-pkk/mmaster');
				$ecoaname	= $this->mmaster->namaacc($icoa);
				$this->db->trans_begin();
				$irv=$this->mmaster->runningnumberrv($tah,$bul,$iarea,$irvtype);
				$ikk=$this->mmaster->runningnumberkk($tah,$bul,$iarea);
				$this->mmaster->insert($iarea,$ikk,$iperiode,$icoa,$vkk,$dkk,$dbukti,$ecoaname,$edescription,$fdebet);
				$nomor=$ikk;
###########posting##########
        $eremark		= $edescription;
        $fclose			= 'f';
			  $this->mmaster->inserttransheader($ikk,$iarea,$eremark,$fclose,$dkk);
			  $this->mmaster->updatekk($ikk,$iarea,$iperiode);
			  if($fdebet=='t'){
				  $accdebet		  = $icoa;
				  $namadebet		= $ecoaname;
				  $acckredit		= KasKecil.$iarea;
				  $namakredit		= $this->mmaster->namaacc($acckredit);
			  }else{
				  $acckredit		= $icoa;
				  $namakredit		= $ecoaname;
				  $accdebet		  = KasKecil.$iarea;
				  $namadebet		= $this->mmaster->namaacc($accdebet);
			  }
			  $this->mmaster->inserttransitemdebet($accdebet,$ikk,$namadebet,'t','t',$iarea,$eremark,$vkk,$dkk);
			  $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vkk);
			  $this->mmaster->inserttransitemkredit($acckredit,$ikk,$namakredit,'f','t',$iarea,$eremark,$vkk,$dkk);
			  $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vkk);
			  $this->mmaster->insertgldebet($accdebet,$ikk,$namadebet,'t',$iarea,$vkk,$dkk,$eremark);
			  $this->mmaster->insertglkredit($acckredit,$ikk,$namakredit,'f',$iarea,$vkk,$dkk,$eremark);
###########end of posting##########
        $this->mmaster->insertrvitem( $irv,$iarea,$icoa,$ecoaname,$vkk,$edescription,$ikk,$irvtype,$iarea);
        $icoa=KasKecil.$iarea;
				$this->mmaster->insertrv( $irv,$iarea,$iperiode,$icoa,$dkk,$vkk,$eremark,$irvtype);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Input Pengisian Kas kecil No:'.$ikk.' Periode:'.$iperiode.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );
#					$this->db->trans_rollback();
					$this->db->trans_commit();
					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function start()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	= $this->input->post('iarea', TRUE);
			$periode= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$tanggal= $this->input->post('dkk', TRUE);
			if($tanggal!=''){
				$tmp=explode("-",$tanggal);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$tgl=$th."-".$bl."-".$hr;
			}
			$dbukti= $this->input->post('dbukti', TRUE);
/*
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
			}
*/
			$this->load->model('akt-pkk/mmaster');

			$tmp = explode("-", $tgl);
			$det	= $tmp[2];
			$mon	= $tmp[1];
			$yir 	= $tmp[0];
			$dtos	= $yir."/".$mon."/".$det;
			$dtos	= $this->mmaster->dateAdd("d",1,$dtos);
			$tmp 	= explode("-", $dtos);
			$det1	= $tmp[2];
			$mon1	= $tmp[1];
			$yir1 	= $tmp[0];
			$dtos	= $yir1."-".$mon1."-".$det1;

			$data['saldo']=$this->mmaster->bacasaldo($area,$periode,$dtos);
			$data['page_title'] = $this->lang->line('pkk');
			$data['iarea']=$area;
			$data['eareaname']=$this->mmaster->area($area);
			$data['iperiode']=$periode;
			$data['tanggal']=$tanggal;
      $data['dbukti']=$dbukti;
			$data['ikk']='';
			$this->load->view('akt-pkk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function rv()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){		
      $cari 	= strtoupper($this->input->post('cari', FALSE));
			$area	=$this->input->post('area', TRUE);
			$periode=$this->input->post('periode', TRUE);
      if($area=='') $area	= $this->uri->segment(4);
			if($periode=='') $periode = $this->uri->segment(5);
      if($cari=='') $cari = $this->uri->segment(6);
      if($cari=='') $cari='sikasep';
			$config['base_url'] = base_url().'index.php/akt-pkk/cform/rv/'.$area.'/'.$periode.'/'.$cari.'/';
      if($cari=='sikasep'){
			  $query = $this->db->query("	select i_rv, v_rv from tm_rv
							                      where i_area='$area' and i_periode='$periode' and i_rv_type='00'",false);
      }else{
			  $query = $this->db->query("	select i_rv, v_rv from tm_rv
							                      where i_area='$area' and i_periode='$periode' and i_rv_type='00'
                                    and (upper(i_rv) like '%$cari%') ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
      $data['baris']=$this->uri->segment(7);
			$this->load->model('akt-pkk/mmaster');
			$data['page_title'] = $this->lang->line('list_rv');
			$data['area']	=$area;
			$data['periode']=$periode;
			$data['isi']=$this->mmaster->bacarvprint($cari,$area,$periode,$config['per_page'],$this->uri->segment(7));
			$this->load->view('akt-pkk/vlistrv', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carirv()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	=$this->input->post('area', TRUE);
            $baris 	= $this->input->post('xbaris', FALSE);
			$periode=$this->input->post('periode', TRUE);
			$config['base_url'] = base_url().'index.php/akt-kb-multi/cform/kendaraan/'.$area.'/'.$periode.'/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("	select * from tr_kendaraan a
							inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)

							inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							where (upper(a.i_kendaraan) like '%$cari%' or upper(a.e_pengguna) like '%$cari%')
							and a.i_area='$area'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
        
			$this->pagination->initialize($config);
			$this->load->model('akt-kb-multi/mmaster');
			$data['area']=$area;
            $data['baris']=$baris;
			$data['periode']=$periode;
			$data['page_title'] = $this->lang->line('list_kendaraan');
			$data['isi']=$this->mmaster->carikendaraan($area,$periode,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-kb-multi/vlistkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu151')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea	= $this->uri->segment(4);
			$irv	  = $this->uri->segment(5);
			$this->load->model('akt-pkk/mmaster');
      $data['iarea']= $iarea;
			$data['irv']  = $irv;
			$data['page_title'] = $this->lang->line('printrv');
			$data['isi']=$this->mmaster->bacarv($irv,$iarea);
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$sql	= "select e_area_name from tr_area where i_area='$iarea'";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
			     $data['eareaname']=$tes->e_area_name;
			  }
			}
			$pesan='Cetak RV Area:'.$iarea.' No:'.$irv;
			$this->load->model('logger');
#			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('akt-pkk/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
