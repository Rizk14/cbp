<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu267')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kartupiutangpertgl');
			$data['iarea']='';
      $data['dfrom']='';
      $data['dto']='';
			$this->load->view('kartupiutangpertgl/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu267')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	= strtoupper($this->input->post('cari'));
			$iarea= $this->input->post('iarea');
      $dfrom= $this->input->post('dfrom');
      $dto	= $this->input->post('dto');
			if($iarea=='') $iarea	= $this->uri->segment(4);
			if($dfrom=='') $dfrom	= $this->uri->segment(5);
			if($dto=='') $dto	= $this->uri->segment(6);
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dtox=$th."-".$bl."-".$hr;
			}
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfromx=$th."-".$bl."-".$hr;
			}
			$config['per_page'] = '10';
			 $iuser   = $this->session->userdata('user_id');

			$this->load->model('kartupiutangpertgl/mmaster');
  		$data['page_title'] = $this->lang->line('kartupiutangpertgl');
			$data['iarea']	= $iarea;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['hal'] = $this->uri->segment(7);
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfromx,$dtox);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Kartu Piutang per tgl dari:'.$dfrom.' s/d '.$dto.' - Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$this->load->view('kartupiutangpertgl/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu267')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kartupiutangpertgl');
			$this->load->view('kartupiutangpertgl/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu267')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kartupiutangpertgl/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kartupiutangpertgl/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('kartupiutangpertgl/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu267')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kartupiutangpertgl/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kartupiutangpertgl/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('kartupiutangpertgl/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
     	 ($this->session->userdata('menu267')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dto	    = $this->input->post('dto');
			$iarea= $this->input->post('iarea');
      if($iarea=='')$icustomer=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$this->load->model('kartupiutangpertgl/mmaster');
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$data['dto']  = $dto;
			$data['iarea']= $iarea;
			$data['page_title'] = $this->lang->line('printopnnotaarea');
			$data['isi']=$this->mmaster->baca($iarea,$dto);
			$data['user']	= $this->session->userdata('user_id');
      $sess = $this->session->userdata('session_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Cetak Kartu Piutang sampai tanggal:'.$dto.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$this->load->view('kartupiutangpertgl/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu267')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$icustomer= $this->input->post('icustomer');
      $dfrom	= $this->input->post('dfrom');
      $dto	= $this->input->post('dto');
      $saldo	= $this->input->post('saldo');
			if($icustomer=='') $icustomer	= $this->uri->segment(4);
			if($dfrom=='') $dfrom	= $this->uri->segment(5);
			if($dto=='') $dto	= $this->uri->segment(6);
			if($saldo=='') $saldo	= $this->uri->segment(7);
			if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dtox=$th."-".$bl."-".$hr;
			}
			if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfromx=$th."-".$bl."-".$hr;
			}
			$config['base_url'] = base_url().'index.php/kartupiutangpertgl/cform/detail/'.$icustomer.'/'.$dfrom.'/'.$dto.'/'.$saldo.'/';
      $query = $this->db->query(" select * from v_ar_kartu_piutang where i_customer_groupbayar='$icustomer' and doc_date>='$dfromx' and doc_date<='$dtox'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = 'all';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->paginationxx->initialize($config);
			$this->load->model('kartupiutangpertgl/mmaster');
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
  		$data['page_title'] = $this->lang->line('kartupiutangpertgl');
			$data['icustomer']	= $icustomer;
			$data['dfrom']	= $dfrom;
			$data['dto']=$dto;
			$data['saldo']	= $saldo;
			$data['isi']		= $this->mmaster->bacadetail($icustomer,$dfrom,$dto,$config['per_page'],$this->uri->segment(8));
			$this->load->view('kartupiutangpertgl/vformdetail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
