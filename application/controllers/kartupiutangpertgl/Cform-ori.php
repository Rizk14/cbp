<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kartupiutang');
			$data['iarea']='';
      $data['djt']='';
			$this->load->view('kartupiutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	= strtoupper($this->input->post('cari'));
			$iarea= $this->input->post('iarea');
      $djt	= $this->input->post('djt');
			if($iarea=='') $iarea	= $this->uri->segment(4);
			if($djt=='') $djt	= $this->uri->segment(5);
      $per='';
      if($djt==''){
        $tmp 	= explode("-", $djt);
        $mon	= $tmp[1];
        $yir 	= $tmp[2];
        $per	= $yir.$mon;
      }
			$config['base_url'] = base_url().'index.php/kartupiutang/cform/view/'.$iarea.'/'.$djt.'/';
      $query = $this->db->query(" select i_customer, (nota+dn)-(pl+kn)as saldo, nota, dn, pl, kn from(
                                  select i_customer, sum(nota) as nota, sum(dn) as dn, sum(pl) as pl, sum(kn) as kn from(
                                  select b.i_customer_groupar as i_customer ,sum(a.v_nota_netto) as nota, 0 as dn, 0 as pl, 0 as kn 
                                  from tm_nota a, tr_customer_groupar b
                                  where f_nota_cancel='f' and a.i_customer=b.i_customer and a.d_nota <= to_date('$djt','dd-mm-yyyy')
                                  and a.i_area='$iarea'
                                  group by b.i_customer_groupar
                                  union all
                                  select i_customer, 0 as nota, sum(v_netto) as dn, 0 as pl, 0 as kn from tm_kn
                                  where upper(substring(i_kn,1,1))='D' and f_kn_cancel='f' and d_kn <= to_date('$djt','dd-mm-yyyy')
                                  and a.i_area='$iarea'
                                  group by i_customer 
                                  union all
                                  select i_customer, 0 as nota, 0 as dn, sum(v_jumlah) as pl, 0 as kn from tm_pelunasan
                                  where f_pelunasan_cancel='f' and f_giro_tolak='f' and a.f_giro_batal='f' 
                                  and d_bukti <= to_date('$djt','dd-mm-yyyy')
                                  and i_jenis_bayar<>'04' and a.i_area='$iarea'
                                  group by i_customer
                                  union all
                                  select i_customer, 0 as nota, 0 as dn, 0 as pl, sum(v_netto) as kn from tm_kn
                                  where upper(substring(i_kn,1,1))='K' and f_kn_cancel='f' and d_kn <= to_date('$djt','dd-mm-yyyy')
                                  and a.i_area='$iarea'
                                  group by i_customer
                                  ) as a
                                  group by i_customer
                                  order by i_customer
                                  ) as b",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('kartupiutang/mmaster');
  		$data['page_title'] = $this->lang->line('kartupiutang');
			$data['cari']		= $cari;
			$data['iarea']	= $iarea;
			$data['djt']	= $djt;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$djt,$config['per_page'],$this->uri->segment(6),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Kartu Piutang Jatuh Tempo:'.$djt.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$this->load->view('kartupiutang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kartupiutang');
			$this->load->view('kartupiutang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kartupiutang/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kartupiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('kartupiutang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kartupiutang/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kartupiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('kartupiutang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
     	 ($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dto	    = $this->input->post('dto');
			$iarea= $this->input->post('iarea');
      if($iarea=='')$icustomer=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$this->load->model('kartupiutang/mmaster');
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$data['dto']  = $dto;
			$data['iarea']= $iarea;
			$data['page_title'] = $this->lang->line('printopnnotaarea');
			$data['isi']=$this->mmaster->baca($iarea,$dto);
			$data['user']	= $this->session->userdata('user_id');
      $sess = $this->session->userdata('session_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Cetak Kartu Piutang sampai tanggal:'.$dto.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$this->load->view('kartupiutang/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu266')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$icustomer= $this->input->post('icustomer');
			$ecustomername= $this->input->post('ecustomername');
      $djt	= $this->input->post('djt');
			if($icustomer=='') $icustomer	= $this->uri->segment(4);
			if($djt=='') $djt	= $this->uri->segment(5);
			if($ecustomername=='') $ecustomername	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/kartupiutang/cform/detail/'.$icustomer.'/'.$djt.'/'.$ecustomername.'/';
      $ecustomername=str_replace('%20',' ',$ecustomername);
      $ecustomername=str_replace('tandakurungbuka','(',$ecustomername);
      $ecustomername=str_replace('tandakurungtutup',')',$ecustomername);
      $ecustomername=str_replace('tandadan','&',$ecustomername);
      $ecustomername=str_replace('tandatitik','.',$ecustomername);
      $ecustomername=str_replace('tandakoma',',',$ecustomername);
      $ecustomername=str_replace('tandagaring','/',$ecustomername);

      $query = $this->db->query(" select a.v_sisa from tm_nota a, tr_customer b
																	  where a.i_customer=b.i_customer 
																	  and a.f_ttb_tolak='f'
																	  and a.f_nota_koreksi='f'
																	  and not a.i_nota isnull
																	  and a.i_customer='$icustomer' 
                                    and a.v_sisa>0
                                    and a.f_nota_cancel='f'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);
			$this->load->model('kartupiutang/mmaster');
  		$data['page_title'] = $this->lang->line('kartupiutang');
			$data['icustomer']	= $icustomer;
			$data['ecustomername']	= $ecustomername;
			$data['djt']	= $djt;
			$data['isi']		= $this->mmaster->bacadetail($icustomer,$djt,$config['per_page'],$this->uri->segment(7));
			$this->load->view('kartupiutang/vformdetail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
