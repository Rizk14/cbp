<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu329')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('hitunginsentif');
			$data['iperiode']	= '';
			$this->load->view('hitunginsentifnew/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu329')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('hitunginsentifnew/mmaster');
			$iperiode	= $this->input->post('iperiode');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			$config['base_url'] = base_url().'index.php/hitunginsentifnew/cform/view/'.$iperiode.'/';
			$data['page_title'] = $this->lang->line('hitunginsentif');
			$data['iperiode']	  = $iperiode;
			$data['omset']		    = $this->mmaster->bacaomset($iperiode);
#			$data['collectioncash']		= $this->mmaster->bacacollectioncash($iperiode);
#			$data['collectioncredit']		= $this->mmaster->bacacollectioncredit($iperiode);
#			$data['effectivecall']= $this->mmaster->bacaeffectivecall($iperiode);
			$this->load->view('hitunginsentifnew/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function viewx()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu329')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('hitunginsentifnew/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			$data['page_title'] = $this->lang->line('hitunginsentif');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']		= $this->mmaster->baca($iperiode);
			$this->load->view('hitunginsentifnew/vmainform',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
  function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu329')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('hitunginsentifnew/mmaster');
			$iperiode	= $this->input->post('iperiode');
      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			if($iarea=='') $iarea=$this->uri->segment(5);
      if(strlen($iarea)==1) $iarea='0'.$iarea;
      $query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
      $st=$query->row();
      $store=$st->i_store;
			$config['base_url'] = base_url().'index.php/hitunginsentifnew/cform/view/'.$iperiode.'/'.$store.'/';
			$query = $this->db->query(" select i_product from tm_mutasi where e_mutasi_periode = '$iperiode' and i_store='$store'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('hitunginsentif');
      $cari='';
      $data['cari']=$cari;
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['istore']	= $store;
			$data['isi']		= $this->mmaster->baca($iperiode,$store,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('hitunginsentifnew/vmainform',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu329')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$dto	= $this->input->post('dto');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/hitunginsentifnew/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_target a, tr_area b
										where a.i_periode = '$iperiode'
										and a.i_area=b.i_area
										and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('hitunginsentifnew/mmaster');
			$data['page_title'] = $this->lang->line('hitunginsentif');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('hitunginsentifnew/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu329')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/hitunginsentifnew/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    group by a.i_store, b.e_store_name",false);
			}else{
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                    or a.i_area = '$area4' or a.i_area = '$area5')
                                    group by a.i_store, b.e_store_name",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('hitunginsentifnew/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('hitunginsentifnew/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu329')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/hitunginsentifnew/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store and (upper(a.i_area) like '%$cari%' or upper(a.e_area_name) like '%$cari%')
                                    group by a.i_store, b.e_store_name",false);
			}else{
				$query = $this->db->query("select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                    or a.i_area = '$area4' or a.i_area = '$area5')
                                    and (upper(a.i_area) like '%$cari%' or upper(a.e_area_name) like '%$cari%')
                                    group by a.i_store, b.e_store_name
                                    order by a.i_store, b.e_store_name",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('hitunginsentifnew/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('hitunginsentifnew/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu329')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $iperiode = $this->uri->segment(4);
      $iarea = $this->uri->segment(5);
			$this->load->model('hitunginsentifnew/mmaster');     
			$data['page_title'] = $this->lang->line('hitunginsentif');
			$data['iperiode']	  = $iperiode;
			$data['iarea']	    = $iarea;
			$data['detail']	    = $this->mmaster->detail($iarea,$iperiode);
			$this->load->view('hitunginsentifnew/vmainform',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetakdetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu329')=='t')) ||
			(($this->session->userdata('logged_in')) &&

			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('hitunginsentifnew/mmaster');
      $iperiode = $this->uri->segment(4);
      $iarea    = $this->uri->segment(5);
      $iproduct = $this->uri->segment(6);
      $saldo    = $this->uri->segment(7);
      $istorelocation  = $this->uri->segment(8);
			$this->load->model('hitunginsentifnew/mmaster');     
			$data['page_title'] = $this->lang->line('hitunginsentif');
			$data['iperiode']	  = $iperiode;
			$data['iarea']	    = $iarea;
			$data['iproduct']	  = $iproduct;
			$data['saldo']	    = $saldo;
			$data['istorelocation'] = $istorelocation;
			$data['detail']	    = $this->mmaster->detail($istorelocation,$iperiode,$iarea,$iproduct);
			$this->load->view('hitunginsentifnew/vformprintdetail',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu219')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('hitunginsentifnew/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('pperiode');
      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			if($iarea=='') $iarea=$this->uri->segment(5);
      $query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
      $st=$query->row();
      $store=$st->i_store;
#      $query=$this->mmaster->bacaexcel($iperiode,$store,$cari);
      $this->db->select("	a.*, b.e_product_name from tm_mutasi a, tr_product b
						              where e_mutasi_periode = '$iperiode' and a.i_product=b.i_product
						              and i_store='$store' order by b.e_product_name ",false);#->limit($num,$offset);
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan Mutasi ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN MUTASI STOK GUDANG REGULER');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,11,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Area :                     Bulan :                    Hal :');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,11,3);
				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'PENERIMAAN');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5,6,6,6);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'PENGELUARAN');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Stok');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', '+/-');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        $objPHPExcel->getActiveSheet()->setCellValue('A7', 'Urut');
				$objPHPExcel->getActiveSheet()->getStyle('A7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B7', 'Barang');
				$objPHPExcel->getActiveSheet()->getStyle('B7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C7', 'Barang');
				$objPHPExcel->getActiveSheet()->getStyle('C7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Awal');
				$objPHPExcel->getActiveSheet()->getStyle('D7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E7', 'd/Pusat');
				$objPHPExcel->getActiveSheet()->getStyle('E7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F7', 'Penj.');
				$objPHPExcel->getActiveSheet()->getStyle('F7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G7', 'KePusat');
				$objPHPExcel->getActiveSheet()->getStyle('G7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H7', 'Akhir');
				$objPHPExcel->getActiveSheet()->getStyle('H7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I7', 'Opnam');
				$objPHPExcel->getActiveSheet()->getStyle('I7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J7', '( pc )');
				$objPHPExcel->getActiveSheet()->getStyle('J7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i=7;
				$j=7;
				$xarea='';
				$saldo=0;
        $no=0;
				foreach($query->result() as $row){
          $no++;
          $selisih=$row->n_saldo_stockopname-$row->n_saldo_akhir;
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_product);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->n_saldo_awal);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->n_mutasi_bbm);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->n_mutasi_penjualan);
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->n_mutasi_bbk);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->n_saldo_akhir);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->n_saldo_stockopname);
					$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $selisih);
					$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$i++;
					$j++;
				}
				$x=$i-1;
				$objPHPExcel->getActiveSheet()->getStyle('G7:J'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
			}
			$objPHPExcel->getActiveSheet()->getStyle('A6:I6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='SO-'.$iperiode.'.xls';
			$objWriter->save("excel/".$iarea.'/'.$nama); 
			$data['sukses'] = true;
			$data['inomor']	= "Laporan Mutasi Stok Reguler";
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
