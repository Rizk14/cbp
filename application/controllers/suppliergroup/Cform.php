<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu1')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$config['base_url'] = base_url().'index.php/suppliergroup/cform/index/';
			$cari 	= $this->input->post('cari', TRUE);
			$cari	= strtoupper($cari);
			$query	= $this->db->query("select i_supplier_group, e_supplier_groupname, 
							e_supplier_groupnameprint1, e_supplier_groupnameprint2 
							from tr_supplier_group 
							where upper(i_supplier_group) like '%$cari%' 
							or upper(e_supplier_groupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('master_suppliergroup');
			$data['isuppliergroup']='';
			$this->load->model('suppliergroup/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('suppliergroup/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu1')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$isuppliergroup 			= $this->input->post('isuppliergroup', TRUE);
			$esuppliergroupname 		= $this->input->post('esuppliergroupname', TRUE);
			$esuppliergroupnameprint1	= $this->input->post('esuppliergroupnameprint1', TRUE);
			$esuppliergroupnameprint2	= $this->input->post('esuppliergroupnameprint2', TRUE);

			if ($isuppliergroup != '' && $esuppliergroupname != '')
			{
				$this->load->model('suppliergroup/mmaster');
				$this->mmaster->insert($isuppliergroup,$esuppliergroupname,$esuppliergroupnameprint1,$esuppliergroupnameprint2);
				$config['base_url'] = base_url().'index.php/suppliergroup/cform/index/';
				$cari 				= $this->input->post('cari', TRUE);
				$cari				= strtoupper($cari);
				$query				= $this->db->query("select i_supplier_group, e_supplier_groupname, 
														e_supplier_groupnameprint1, e_supplier_groupnameprint2 
														from tr_supplier_group 
														where upper(i_supplier_group) like '%$cari%' 
														or upper(e_supplier_groupname) like '%$cari%'",false);
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->pagination->initialize($config);
				$data['page_title'] = $this->lang->line('master_suppliergroup');
				$data['isuppliergroup']='';
				$this->load->model('suppliergroup/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('suppliergroup/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu1')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_suppliergroup');
			$this->load->view('suppliergroup/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu1')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_suppliergroup')." update";
			if($this->uri->segment(4)){
				$isuppliergroup = $this->uri->segment(4);
				$data['isuppliergroup'] = $isuppliergroup;
				$this->load->model('suppliergroup/mmaster');
				$data['isi']=$this->mmaster->baca($isuppliergroup);
		 		$this->load->view('suppliergroup/vmainform',$data);
			}else{
				$this->load->view('suppliergroup/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu1')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isuppliergroup				= $this->input->post('isuppliergroup', TRUE);
			$esuppliergroupname 		= $this->input->post('esuppliergroupname', TRUE);
			$esuppliergroupnameprint1	= $this->input->post('esuppliergroupnameprint1', TRUE);
			$esuppliergroupnameprint2	= $this->input->post('esuppliergroupnameprint2', TRUE);
			$this->load->model('suppliergroup/mmaster');
			$this->mmaster->update($isuppliergroup,$esuppliergroupname,$esuppliergroupnameprint1,$esuppliergroupnameprint2);
			$config['base_url'] = base_url().'index.php/suppliergroup/cform/index/';
			$cari 				= $this->input->post('cari', TRUE);
			$cari				= strtoupper($cari);
			$query				= $this->db->query("select i_supplier_group, e_supplier_groupname, 
													e_supplier_groupnameprint1, e_supplier_groupnameprint2 
													from tr_supplier_group 
													where upper(i_supplier_group) like '%$cari%' 
													or upper(e_supplier_groupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('master_suppliergroup');
			$data['isuppliergroup']='';
			$this->load->model('suppliergroup/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('suppliergroup/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu1')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isuppliergroup	= $this->uri->segment(4);
			$this->load->model('suppliergroup/mmaster');
			$this->mmaster->delete($isuppliergroup);

			$config['base_url'] = base_url().'index.php/suppliergroup/cform/index/';
			$cari 				= $this->input->post('cari', TRUE);
			$cari				= strtoupper($cari);
			$query				= $this->db->query("select i_supplier_group, e_supplier_groupname, 
													e_supplier_groupnameprint1, e_supplier_groupnameprint2 
													from tr_supplier_group 
													where upper(i_supplier_group) like '%$cari%' 
													or upper(e_supplier_groupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('master_suppliergroup');
			$data['isuppliergroup']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('suppliergroup/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		$config['base_url'] = base_url().'index.php/suppliergroup/cform/index/';
		$cari 				= $this->input->post('cari', TRUE);
		$cari				= strtoupper($cari);
		$query				= $this->db->query("select i_supplier_group, e_supplier_groupname, 
												e_supplier_groupnameprint1, e_supplier_groupnameprint2 
												from tr_supplier_group 
												where upper(i_supplier_group) like '%$cari%' 
												or upper(e_supplier_groupname) like '%$cari%'",false);
		$config['total_rows'] = $query->num_rows();
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(4);
		$this->pagination->initialize($config);
		$data['page_title'] = $this->lang->line('master_suppliergroup');
		$data['isuppliergroup']='';
		$this->load->model('suppliergroup/mmaster');
		$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
		$this->load->view('suppliergroup/vmainform', $data);
	}
}
?>
