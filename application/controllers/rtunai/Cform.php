<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('rtunai');
			$this->load->model('rtunai/mmaster');
			$data['ikum']='';
			$this->load->view('rtunai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('rtunai');
			$this->load->view('rtunai/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (($this->session->userdata('logged_in'))
		){
			$data['page_title'] = $this->lang->line('rtunai')." update";
			if(
				($this->uri->segment(4)!='')
			  ){
        $irtunai          = $this->uri->segment(4);
				$iarea		      = $this->uri->segment(5);
				$dfrom		      = $this->uri->segment(6);
				$dto			      = $this->uri->segment(7);
				$data['irtunai']  = $irtunai;
				$data['iarea']		= $iarea;
				$data['dfrom']		= $dfrom;
				$data['dto']		= $dto;
        $data['pst']	= $this->session->userdata('i_area');
				$this->load->model("rtunai/mmaster");
				$data['isi']=$this->mmaster->baca($iarea,$irtunai);
				$data['detail']=$this->mmaster->bacadetail($iarea,$irtunai);
		 		$this->load->view('rtunai/vformupdate',$data);
			}else{
				$this->load->view('rtunai/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		  $irtunai  = $this->input->post('irtunai', TRUE);
			$drtunai	= $this->input->post('drtunai', TRUE);
			if($drtunai!=''){
				$tmp=explode("-",$drtunai);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$drtunai=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
				$tahun=$th;
			}
			$iarea			      = $this->input->post('iarea', TRUE);
			$ibank			      = $this->input->post('ibank', TRUE);
			$xiarea			      = $this->input->post('xiarea', TRUE);
			$eremark      		= $this->input->post('eremark', TRUE);
			$vjumlah      		= $this->input->post('vjumlah', TRUE);
			$vjumlah      		= str_replace(',','',$vjumlah);
			$jml          		= $this->input->post('jml', TRUE);
			$jml          		= str_replace(',','',$jml);
			if (
				($drtunai != '') && ($iarea!='') && ($vjumlah!='') && ($vjumlah!='0') && ($ibank!='')
			   )
			{
				$this->load->model('rtunai/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($irtunai,$drtunai,$iarea,$xiarea,$eremark,$vjumlah,$ibank);
				for($i=1;$i<=$jml;$i++){
				  $itunai			= $this->input->post('itunai'.$i, TRUE);
				  $iareatunai	= $this->input->post('iarea'.$i, TRUE);
				  $vjumlah		= $this->input->post('vjumlah'.$i, TRUE);
				  $vjumlah		= str_replace(',','',$vjumlah);
				  $vjumlahasallagi	= $this->input->post('vjumlahasallagi'.$i, TRUE);
				  $vjumlahasallagi	= str_replace(',','',$vjumlahasallagi);
#				  $this->mmaster->insertdetail($irtunai,$iarea,$itunai,$iareatunai,$vjumlah,$i);
				  $this->mmaster->updatedetail($irtunai,$iarea,$xiarea,$itunai,$iareatunai,$vjumlah,$i);
 				  $this->mmaster->updatetunaix($irtunai,$iarea,$itunai,$iareatunai,$vjumlahasallagi);
				  $this->mmaster->updatetunai($irtunai,$iarea,$itunai,$iareatunai,$vjumlah);
				}		
				if( ($this->db->trans_status() === FALSE) && ($cek) )
				{
					$this->db->trans_rollback();
				}else{
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input Setor Tunai Area '.$iarea.' No:'.$irtunai;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $irtunai;
					$this->db->trans_commit();
#					$this->db->trans_rollback();
				  $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$drtunai	= $this->input->post('drtunai', TRUE);
			if($drtunai!=''){
				$tmp=explode("-",$drtunai);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$drtunai=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
				$tahun=$th;
			}
			$iarea			      = $this->input->post('iarea', TRUE);
			$ibank			      = $this->input->post('ibank', TRUE);
			$eremark      		= $this->input->post('eremark', TRUE);
			$vjumlah      		= $this->input->post('vjumlah', TRUE);
			$vjumlah      		= str_replace(',','',$vjumlah);
			$jml          		= $this->input->post('jml', TRUE);
			$jml          		= str_replace(',','',$jml);
			if (
				($drtunai != '') && ($iarea!='') && ($vjumlah!='') && ($vjumlah!='0') && ($ibank!='')
			   )
			{
				$this->load->model('rtunai/mmaster');
				$this->db->trans_begin();
			  $irtunai = $this->mmaster->runningnumber($iarea,$thbl);
				$this->mmaster->insert($irtunai,$drtunai,$iarea,$eremark,$vjumlah,$ibank);
				for($i=1;$i<=$jml;$i++){
				  $itunai			= $this->input->post('itunai'.$i, TRUE);
				  $iareatunai	= $this->input->post('iarea'.$i, TRUE);
				  $vjumlah		= $this->input->post('vjumlah'.$i, TRUE);
				  $vjumlah		= str_replace(',','',$vjumlah);
				  $this->mmaster->insertdetail($irtunai,$iarea,$itunai,$iareatunai,$vjumlah,$i);
				  $this->mmaster->updatetunai($irtunai,$iarea,$itunai,$iareatunai,$vjumlah);
				}		
				if( ($this->db->trans_status() === FALSE) && ($cek) )
				{
					$this->db->trans_rollback();
				}else{
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input Setor Tunai Area '.$iarea.' No:'.$irtunai;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $irtunai;
					$this->db->trans_commit();
#					$this->db->trans_rollback();
				  $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/rtunai/cform/area/index/';
			$config['per_page'] = '10';
			$area	= $this->session->userdata('i_area');
			$iuser   = $this->session->userdata('user_id');
			if($area=='00'){
  			$query = $this->db->query("	select * from tr_area ",false);
  	  }else{
  	    $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')", false);
  	  }
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('rtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('rtunai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/rtunai/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$area	= $this->session->userdata('i_area');
			$iuser   = $this->session->userdata('user_id');
			if($area=='00'){
			  $query = $this->db->query("select * from tr_area
									     	where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' ",false);
      }else{
  	    $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') 
  	                                and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')", false);
  	  }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('rtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('rtunai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bank()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/rtunai/cform/bank/index/';
			$config['per_page'] = '10';
 			$query = $this->db->query("	select * from tr_bank ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('rtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_bank');
			$data['isi']=$this->mmaster->bacabank($config['per_page'],$this->uri->segment(5));
			$this->load->view('rtunai/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		  $iarea = $this->input->post('iarea');
			if($iarea=='') $iarea 	= $this->uri->segment(4);
		  $tgl = $this->input->post('tgl');
			if($tgl=='') $tgl = $this->uri->segment(5);
			$tmp=explode('-',$tgl);
			$yy=$tmp[2];
			$mm=$tmp[1];
			$per=$yy.$mm;
		  $cari = strtoupper($this->input->post('cari'));
		  if($cari=='' && $this->uri->segment(6)=='sikasep'){
		    $cari = '';
		  }elseif($cari==''){
		    $cari=$this->uri->segment(6);
		  }
		  if($cari==''){
	  		$config['base_url'] = base_url().'index.php/rtunai/cform/customer/'.$iarea.'/'.$tgl.'/sikasep/';
      }else{			
  			$config['base_url'] = base_url().'index.php/rtunai/cform/customer/'.$iarea.'/'.$tgl.'/'.$cari.'/';
      }
			$config['per_page'] = '10';
			$query = $this->db->query("	select distinct on (a.i_customer, a.e_customer_name, c.e_salesman_name) a.*, b.i_customer_groupar, 
			              c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
										left join tr_customer_groupar b on(a.i_customer=b.i_customer)
										left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area and c.e_periode='$per')
										left join tr_customer_owner d on(a.i_customer=d.i_customer)
										where a.i_area = '$iarea'
										and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%' or upper(c.e_salesman_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('rtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$per,$cari,$config['per_page'],$this->uri->segment(7));
			$data['iarea']=$iarea;
      $data['cari']=$cari;
      $data['tgl']=$tgl;
			$this->load->view('rtunai/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 	= $this->uri->segment(4);
			$cari 	= strtoupper($this->input->post('cari', FALSE));
      if( ($cari=='') && ($this->uri->segment(6)!='') )$cari=$this->uri->segment(5);
      $cari=str_replace("%20"," ",$cari);
      if($cari!='')
  			$config['base_url'] = base_url().'index.php/rtunai/cform/caricustomer/'.$iarea.'/'.$cari.'/';
      else
  			$config['base_url'] = base_url().'index.php/rtunai/cform/caricustomer/'.$iarea.'/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
								left join tr_customer_groupar b on(a.i_customer=b.i_customer)
								left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
								left join tr_customer_owner d on(a.i_customer=d.i_customer)

								where a.i_area='$iarea' and ( 
									upper(a.i_customer) like '%$cari%' or 
									upper(a.e_customer_name) like '%$cari%' or 
									upper(d.e_customer_setor) like '%$cari%' )",false);
			$config['total_rows'] = $query->num_rows();
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
      if($cari!='')
  			$config['cur_page'] = $this->uri->segment(6);
      else
   			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('rtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
      if($cari!='')
  			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
      else
  			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
      $data['cari']=$cari;
			$this->load->view('rtunai/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function tunai()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $baris=$this->input->post('baris', FALSE);
      $area=$this->input->post('iarea', FALSE);
      $drtunai=$this->input->post('drtunai', FALSE);
      $eremark		  = $this->uri->segment(11);
	  $eremark	= str_replace(" ","'",$eremark);
			if($baris=='') $baris=$this->uri->segment(4);
			$data['baris']=$baris;
      if($area=='')	$area=$this->uri->segment(5);
      if($drtunai=='')	$drtunai=$this->uri->segment(6);
      $tmp=explode("-",$drtunai);
      $dd=$tmp[0];
      $mm=$tmp[1];
      $yy=$tmp[2];
      $drtunaix=$yy.'-'.$mm.'-'.$dd;
      $areax1 = $this->session->userdata('i_area');
			$areax2	= $this->session->userdata('i_area2');
			$areax3	= $this->session->userdata('i_area3');
			$areax4	= $this->session->userdata('i_area4');
			$areax5	= $this->session->userdata('i_area5');
			$data['area']=$area;
			$data['drtunai']=$drtunai;
			$data['baris']=$baris;
      $cari 	= strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/rtunai/cform/tunai/'.$baris.'/'.$area.'/'.$drtunai.'/';
      if($areax1=='00'){
		    $query = $this->db->query(" select a.i_tunai from tm_tunai a, tr_customer b, tr_area c
                                    where a.i_rtunai isnull and a.d_tunai<='$drtunaix'
                                    and a.f_tunai_cancel='f' and a.i_customer=b.i_customer and a.i_area=c.i_area and a.v_sisa > 0
                                    and (upper(a.i_tunai) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                    or upper(a.i_customer) like '%$cari%')",false);
      }else{
		    $query = $this->db->query(" select a.i_tunai from tm_tunai a, tr_customer b, tr_area c
                                    where a.i_rtunai isnull and a.d_tunai<='$drtunaix'
                                    and (a.i_area='$areax1' or a.i_area='$areax2' or a.i_area='$areax3' or a.i_area='$areax4' 
                                    or a.i_area='$areax5')
                                    and a.f_tunai_cancel='f' and a.i_customer=b.i_customer and a.i_area=c.i_area and a.v_sisa > 0
                                    and (upper(a.i_tunai) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                    or upper(a.i_customer) like '%$cari%')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('rtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_tunai');
			$data['isi']=$this->mmaster->bacatunai($drtunaix,$cari,$area,$config['per_page'],$this->uri->segment(7,0),$areax1,$areax2,$areax3,$areax4,$areax5);
			$this->load->view('rtunai/vlisttunai', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		  $data['page_title'] = $this->lang->line('rtunai')." update";
      $irtunai	  = $this->uri->segment(4);
			$iarea		  = $this->uri->segment(5);
      $itunai 	  = $this->uri->segment(6);
			$iareatunai = $this->uri->segment(7);
			$dfrom		  = $this->uri->segment(8);
			$dto			  = $this->uri->segment(9);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$this->load->model('rtunai/mmaster');
			$this->mmaster->deletedetail($irtunai,$iarea,$itunai,$iareatunai);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Hapus Item Setor Tunai Area '.$iarea.' No setor:'.$irtunai.'  No tunai:'.$itunai;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['irtunai']  = $irtunai;
			$data['iarea']		= $iarea;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
      $data['pst']	= $this->session->userdata('i_area');
			$data['isi']=$this->mmaster->baca($iarea,$irtunai);
			$data['detail']=$this->mmaster->bacadetail($iarea,$irtunai);
	 		$this->load->view('rtunai/vformupdate',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
#21232f297a57a5a743894a0e4a801fc3
?>
