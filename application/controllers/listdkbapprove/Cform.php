<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu439')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdkbapprove');
			$data['dfrom']='';
			$data['dto']='';
      $data['idkb']='';
			$this->load->view('listdkbapprove/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu439')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea  = $this->input->post('iarea');
      #$iarea  = $this->session->userdata("i_area");
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listdkbapprove/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.i_dkb from tm_dkb a, tr_area b
										              where a.i_area=b.i_area
										              and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										              or upper(a.i_dkb) like '%$cari%')
										              and a.i_area='$iarea'
                                  and not a.i_approve1 isnull
                                  and a.f_dkb_batal='f'
                                  and
										              a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
										              a.d_dkb <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdkbapprove');
			$this->load->model('listdkbapprove/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['idkb']   = '';
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listdkbapprove/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu439')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdkbapprove');
			$this->load->view('listdkbapprove/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu439')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea  = $this->input->post('iarea');
      #$iarea  = $this->session->userdata("i_area");
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listdkbapprove/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.i_dkb from tm_dkb a, tr_area b
										              where a.i_area=b.i_area
										              and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										              or upper(a.i_dkb) like '%$cari%')
										              and a.i_area='$iarea'
                                  and not a.i_approve1 isnull
                                  and a.f_dkb_batal='f'
                                  and
										              a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
										              a.d_dkb <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdkbapprove');
			$this->load->model('listdkbapprove/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['idkb']   = '';
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listdkbapprove/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function delete()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu439')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $cari    = strtoupper($this->input->post('cari'));
         $idkb = $this->uri->segment(4);
         $iarea   = $this->uri->segment(5);
         $dfrom      = $this->input->post('dfrom');
         $dto     = $this->input->post('dto');
         if($idkb=='') $idkb=$this->uri->segment(4);
         if($iarea=='') $iarea=$this->uri->segment(5);
         if($dfrom=='') $dfrom=$this->uri->segment(6);
         if($dto=='') $dto=$this->uri->segment(7);
         $this->load->model('listdkbapprove/mmaster');
         $this->mmaster->delete($idkb,$iarea);

         $sess=$this->session->userdata('session_id');
         $id=$this->session->userdata('user_id');
         $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
         $rs      = pg_query($sql);
         if(pg_num_rows($rs)>0){
            while($row=pg_fetch_assoc($rs)){
               $ip_address   = $row['ip_address'];
               break;
            }
         }else{
            $ip_address='kosong';
         }
         $query   = pg_query("SELECT current_timestamp as c");
       while($row=pg_fetch_assoc($query)){
         $now    = $row['c'];
         }
         $pesan='Hapus Approve DKB Area '.$iarea.'No:'.$idkb;
         $this->load->model('logger');
         $this->logger->write($id, $ip_address, $now , $pesan );

         $config['base_url'] = base_url().'index.php/listdkbapprove/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
         $query = $this->db->query(" select a.i_dkb from tm_dkb a, tr_area b
										              where a.i_area=b.i_area
										              and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										              or upper(a.i_dkb) like '%$cari%')
										              and a.i_area='$iarea'
                                  and not a.i_approve1 isnull
                                  and a.f_dkb_batal='f' and
										              a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
										              a.d_dkb <= to_date('$dto','dd-mm-yyyy')",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $data['page_title'] = $this->lang->line('listdkbapprove');
         $this->load->model('listdkbapprove/mmaster');
         $data['cari']  = $cari;
         $data['dfrom']= $dfrom;
         $data['dto']   = $dto;
         $data['iarea']= $iarea;
         $data['isi']   = $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
         $this->load->view('listdkbapprove/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu439')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listdkbapprove/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listdkbapprove/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listdkbapprove/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu439')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listdkbapprove/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listdkbapprove/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listdkbapprove/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu439')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdkbapprove');
			if($this->uri->segment(4)!=''){
				$idkb   = $this->uri->segment(4);
				$dfrom  = $this->uri->segment(5);
				$dto 	  = $this->uri->segment(6);
        $iarea  = $this->uri->segment(7);
    		$user   = $this->session->userdata("user");
				$query  = $this->db->query("select * from tm_dkb_item where i_dkb='$idkb'");
				$data['jmlitem']= $query->num_rows(); 				
				$data['idkb'] 	= $idkb;
				$data['iarea']	= $iarea;
				$data['user']	  = $user;
				$data['dfrom']  = $dfrom;
				$data['dto']	  = $dto;
				$query = $this->db->query("select * from tm_dkb_ekspedisi where i_dkb='$idkb'");
				$data['jmlitemx'] = $query->num_rows();	
				$this->load->model('listdkbapprove/mmaster');
				$data['isi']	= $this->mmaster->baca($idkb,$iarea);
				$data['detail']	= $this->mmaster->bacadetail($idkb,$iarea);
				$qrunningjml	= $this->mmaster->bacadetailrunningjml($idkb,$iarea);
				if($qrunningjml->num_rows()>0){
					$row_jmlx	= $qrunningjml->row();
					$data['jmlx']	= $row_jmlx->v_total;
				}else{
					$data['jmlx']	= 0;	
				}				
				$data['detailx']= $this->mmaster->bacadetailx($idkb,$iarea);
		 		$this->load->view('listdkbapprove/vmainform',$data);
			}else{
				$this->load->view('listdkbapprove/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu439')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      #$iarea	= $this->session->userdata('i_area');
      $iarea= $this->input->post('iarea', TRUE);
			$idkb	= $this->input->post('idkb', TRUE);
			$ddkb	= $this->input->post('ddkb', TRUE);
			if($ddkb!=''){
				$tmp=explode("-",$ddkb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$ddkb=$th."-".$bl."-".$hr;
        $thbl=substr($th,2,2).$bl;
			}
			$jml	  = $this->input->post('jml', TRUE);
			if($idkb!='' && $iarea!='')
			{
				$gaono=true;
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					if($cek=='on'){
						$gaono=false;
					}
					if(!$gaono) break;
				}
				if(!$gaono){
				  $this->db->trans_begin();
				  $this->load->model('listdkbapprove/mmaster');
				  $this->mmaster->updateheader($idkb, $iarea);
          for($i=1;$i<=$jml;$i++){
            $cek  = $this->input->post('chk'.$i, TRUE);
            if($cek=='on'){
						  $isj    = $this->input->post('isj'.$i, TRUE);
              $fkirim =1;
				      $this->mmaster->updatedetail($idkb,$iarea,$isj,$ddkb,$fkirim);
				      $this->mmaster->updatedkb($idkb,$isj,$iarea,$ddkb);
            }else{
              $isj	= $this->input->post('isj'.$i, TRUE);
              $fkirim=0;
				      $this->mmaster->updatedetail($idkb,$iarea,$isj,$ddkb,$fkirim);
				      $this->mmaster->updatesj($idkb,$isj,$iarea,$ddkb);
            }
				  }
				  if ( ($this->db->trans_status() === FALSE) )
				  {
				    $this->db->trans_rollback();
				  }else{
				    $this->db->trans_commit();
					  $data['sukses']	= true;
					  $data['inomor']	= $idkb;
					  $this->load->view('nomor',$data);
				  }
        }
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
