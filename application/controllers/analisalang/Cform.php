<?php
class Cform extends CI_Controller
{
	public $title  = "Analisa Pelanggan";
	public $folder = "analisalang";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->helper(array('file', 'directory', 'fusioncharts'));
		require_once("php/fungsi.php");
		$this->load->model('analisalang/mmaster');
	}

	function index()
	{
		$data['page_title'] = $this->title;
		$data['periode1']	= date('Y-m');

		$this->load->view('analisalang/vform', $data);
	}

	function view()
	{
		$icustomer 	= $this->input->post('icustomer', TRUE);
		$tahun 		= $this->input->post('tahun', TRUE);

		$data = [
			'page_title'	=> $this->title,
			'isi'       	=> $this->mmaster->bacaperiode($icustomer, $tahun), /* PENJUALAN RP */
			'isi2'       	=> $this->mmaster->bacaperiode_pernota($icustomer, $tahun), /* PENJUALAN RP (PER NOTA) */
			'isi3'       	=> $this->mmaster->baca_carabyr($icustomer, $tahun), /* JENIS/CARA BAYAR */
			'isi4'		  	=> $this->mmaster->last_order($icustomer), /* ORDER TERAKHIR (NOTA) */
			'lama'       	=> $this->mmaster->baca_lamabyr($icustomer, $tahun), /* LAMA BAYAR */
			'bulan'       	=> $this->mmaster->bacabulan(),
		];

		$this->logger->writenew('Analisa Pelanggan : ' . $icustomer . " Tahun " . $tahun);

		$this->load->view('analisalang/vformview', $data);
	}

	public function customer()
	{
		$cari       = strtoupper($this->input->post('cari', false));
		$cari 		= str_replace("'", "", $cari);
		$is_cari	= $this->input->post('is_cari');
		$iarea      = strtoupper($this->input->post('iarea', false));
		$dspb       = strtoupper($this->input->post('dspb', false));

		if ($is_cari == '') $is_cari = $this->uri->segment(5);
		if ($cari == '' && $is_cari == "1") {
			$cari = $this->uri->segment(4);
			if ($cari == '') $is_cari = "";
		}

		if ($is_cari == "1") {
			$config['base_url'] = base_url() . 'index.php/analisalang/cform/customer/' . $cari . '/' . $is_cari . '/index/';
		} else {
			$config['base_url'] = base_url() . 'index.php/analisalang/cform/customer/' . '/index/';
		}

		if ($is_cari != "1") {
			$query = $this->db->query("	SELECT i_customer FROM tr_customer WHERE f_approve='t' AND f_customer_aktif = 't' ", false);
		} else {
			$query = $this->db->query(" SELECT i_customer FROM tr_customer WHERE f_approve='t' AND f_customer_aktif = 't' AND (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ", false);
		}

		$config['total_rows']   = $query->num_rows();
		$config['per_page']     = '10';
		$config['first_link']   = 'Awal';
		$config['last_link']    = 'Akhir';
		$config['next_link']    = 'Selanjutnya';
		$config['prev_link']    = 'Sebelumnya';

		if ($is_cari == "1") {
			$config['cur_page'] = $this->uri->segment(7);
		} else {
			$config['cur_page'] = $this->uri->segment(5);
		}

		$this->pagination->initialize($config);

		$data['page_title'] = $this->lang->line('list_customer');

		if ($is_cari == "1") {
			$data['isi'] = $this->mmaster->bacacustomer($cari, $config['per_page'], $this->uri->segment(7));
		} else {
			$data['isi'] = $this->mmaster->bacacustomer($cari, $config['per_page'], $this->uri->segment(5));
		}

		$data['cari']   = $cari;

		$this->load->view('analisalang/vlistcustomer', $data);
	}
}
