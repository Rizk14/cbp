<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu21')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_country');
			$data['icountry']='';
			$this->load->model('country/mmaster');
			$data['isi']=$this->mmaster->bacasemua();

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Negara";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('country/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu21')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icountry 	= $this->input->post('icountry', TRUE);
			$ecountryname 	= $this->input->post('ecountryname', TRUE);

			if ((isset($icountry) && $icountry != '') && (isset($ecountryname) && $ecountryname != ''))
			{
				$this->load->model('country/mmaster');
				$this->mmaster->insert($icountry,$ecountryname);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Negara:('.$icountry.') -'.$ecountryname;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		        $data['page_title'] = $this->lang->line('master_country');
				$data['icountry']='';
				$this->load->model('country/mmaster');
				$data['isi']=$this->mmaster->bacasemua();
				$this->load->view('country/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu21')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_country');
			$this->load->view('country/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu21')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_country')." update";
			if($this->uri->segment(4)){
				$icountry = $this->uri->segment(4);
				$data['icountry'] = $icountry;
				$this->load->model('country/mmaster');
				$data['isi']=$this->mmaster->baca($icountry);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Negara:('.$icountry.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('country/vmainform',$data);
			}else{
				$this->load->view('country/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu21')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icountry	= $this->input->post('icountry', TRUE);
			$ecountryname 	= $this->input->post('ecountryname', TRUE);
			$this->load->model('country/mmaster');
			$this->mmaster->update($icountry,$ecountryname);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Negara:('.$icountry.') -'.$ecountryname;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

	        $data['page_title'] = $this->lang->line('master_country');
			$data['icountry']='';
			$this->load->model('country/mmaster');
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('country/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu21')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icountry = $this->uri->segment(4);
			$this->load->model('country/mmaster');
			$this->mmaster->delete($icountry);

			$sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
		        while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
		        }
		      }else{
		        $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
		        $now	  = $row['c'];
		      }
		      $pesan='Delete Master Negara:'.$icountry;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now, $pesan);
      
			$data['page_title'] = $this->lang->line('master_country');
			$data['icountry']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('country/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
