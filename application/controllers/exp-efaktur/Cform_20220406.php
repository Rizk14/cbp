<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    //$this->load->dbutil();
    $this->load->helper('file');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu511')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-efaktur');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('exp-efaktur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu511')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-efaktur');
			$iperiodeawal	  = $this->input->post('iperiodeawal');
			$iperiodeakhir		= $this->input->post('iperiodeakhir');
			if($iperiodeawal=='') $dfrom=$this->uri->segment(4);
			if($iperiodeakhir=='') $dto=$this->uri->segment(5);
			$this->load->model('exp-efaktur/mmaster');
			$data['iperiodeawal'] = $iperiodeawal;
			$data['iperiodeakhir'] = $iperiodeakhir;
			$data['isi']		= $this->mmaster->bacaperiode($iperiodeawal,$iperiodeakhir);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghitung plafond Periode:'.$iperiodeawal.' s/d '.$iperiodeakhir;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('exp-efaktur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu511')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-efaktur');
			$this->load->view('exp-efaktur/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
  function bank()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu511')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-efaktur/cform/bank/index/';
			$query = $this->db->query("select * from tr_bank",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-efaktur/mmaster');
			$data['page_title'] = $this->lang->line('list_bank');
			$data['isi']=$this->mmaster->bacabank($config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-efaktur/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caribank()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu511')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-efaktur/cform/bank/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query("select * from tr_bank where (upper(e_bank_name) like '%$cari%' or upper(i_bank) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-efaktur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->caribank($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-efaktur/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu511')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			  $dfrom		= $this->input->post('dfrom');
			    if($dfrom!=''){
				    $tmp=explode("-",$dfrom);
				    $th=$tmp[2];
				    $bl=$tmp[1];
				    $hr=$tmp[0];
				    $dfrom=$th."-".$bl."-".$hr;
			    }
			  $dto		  = $this->input->post('dto');			  
			    if($dto!=''){
				    $tmp=explode("-",$dto);
				    $th=$tmp[2];
				    $bl=$tmp[1];
				    $hr=$tmp[0];
				    $dto=$th."-".$bl."-".$hr;
			    }
			  $this->load->model('exp-efaktur/mmaster');
			  $query=$this->mmaster->bacaheader($dfrom, $dto);
	      $filename = "efaktur(".$dfrom."_".$dto.").csv";
        $out	= fopen('pajak/'.$filename,'w');
        fputcsv($out, array("FK","KD_JENIS_TRANSAKSI", "FG_PENGGANTI", "NOMOR_FAKTUR", "MASA_PAJAK", "TAHUN_PAJAK", "TANGGAL_FAKTUR", "NPWP", "NAMA", "ALAMAT_LENGKAP", "JUMLAH_DPP", "JUMLAH_PPN", "JUMLAH_PPNMBM", "ID_KETERANGAN_TAMBAHAN", "FG_UANG_MUKA", "UANG_MUKA_DPP", "UANG_MUKA_PPN", "UANG_MUKA_PPNBM", "REFERENSI", "KODE_DOKUMEN_PENDUKUNG"));
        fputcsv($out, array("LT","NPWP", "NAMA", "JALAN", "BLOK", "NOMOR", "RT", "RW", "KECAMATAN", "KELURAHAN", "KABUPATEN", "PROPINSI", "KODE_POS", "NOMOR_TELEPON"));
        fputcsv($out, array("OF","KODE_OBJEK", "NAMA", "HARGA_SATUAN", "JUMLAH_BARANG", "HARGA_TOTAL", "DISKON", "DPP", "PPN", "TARIF_PPNBM", "PPNBM"));

	      if (is_array($query)) {
		      foreach($query as $qu){
   		      $pkp= trim($qu->e_customer_pkpnpwp);
		        if($pkp==''){
		          $custname = $qu->e_customer_name;
		          $custaddress = $qu->e_customer_address; 
		        }else{
		          $custname = $qu->e_customer_pkpname;
		          $custaddress = $qu->e_customer_pkpaddress;
		        }
		        $inota=$qu->i_nota;
		        $dpp=$qu->dpp;
		        $ppn=$qu->ppn;
            if($qu->f_pajak_pengganti=='t')
    		      $fg = '1';
    	      else
    		      $fg = '0';
    		    $qu->i_seri_pajak=substr($qu->i_seri_pajak,3,strlen($qu->i_seri_pajak)-1);
    		    $qu->i_seri_pajak=str_replace('.','',$qu->i_seri_pajak);
    		    $qu->i_seri_pajak=str_replace('-','',$qu->i_seri_pajak);
    		    if($qu->e_customer_pkpnpwp=='')$qu->e_customer_pkpnpwp='000000000000000';
    		    $qu->e_customer_pkpnpwp=str_replace('.','',$qu->e_customer_pkpnpwp);
    		    $qu->e_customer_pkpnpwp=str_replace('-','',$qu->e_customer_pkpnpwp);
			      fputcsv($out, array("FK","01", $fg, $qu->i_seri_pajak, $qu->masa_pajak, $qu->tahun_pajak, $qu->tgl_pajak, 
			                           $qu->e_customer_pkpnpwp, $custname, $custaddress, $dpp, $ppn, "0","","0","0","0","0", 
			                           $qu->i_faktur_komersial." / ".$qu->i_nota." / NIK-".$qu->i_nik,"0" ));  
            $this->db->select("	b.n_deliver, (b.v_unit_price/1.1) as v_unit_price, b.e_product_name, 
                                (b.n_deliver * b.v_unit_price) / 1.1 as sub, 
                                a.n_nota_discount1 as disc1, a.n_nota_discount2 as disc2,
                                a.n_nota_discount3 as disc3, a.n_nota_discount4 as disc4,
                                a.*, (b.n_deliver * (b.v_unit_price/1.1) )as v_subtotal, a.i_nota, a.i_seri_pajak, b.i_product
                                from tm_nota  a, tm_nota_item b
                                where a.f_nota_cancel='false' and (d_pajak >='$dfrom' and d_pajak <='$dto')
                                and a.i_nota = b.i_nota and (a.i_nota='$inota') order by b.n_item_no",false);
      		  $queri = $this->db->get();
			      $totaldis = 0;
			      $totalbayaritem =0;
				foreach($queri->result() as $row)
				{
				
          $total = $row->sub;
          $totaldis   = ($total*$qu->diskon)/100;
         $totalbayaritem = $total - $totaldis;
          $dpp=$totalbayaritem;
          $ppn=$totalbayaritem*0.1;
		  
          fputcsv($out, array("OF",$row->i_product, $row->e_product_name, $row->v_unit_price, $row->n_deliver, $row->sub, $totaldis, $dpp, $ppn, "0", "0"));

			      }
	      }
      }

	fclose($out);
	$data['sukses']			= true;
	$data['folder']		= "exp-efaktur";
	$this->load->view('status',$data);
#	die();
	}
	else{
			$this->load->view('awal/index.php');
		}
		}
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	###########################################################################################################################################################		  
		/*	  $this->load->model('exp-efaktur/mmaster');
			  $isi      = $this->mmaster->bacaperiode($icoabank,$dfrom,$dto,$ibank);
			    if($dfrom!=''){
				    $tmp=explode("-",$dfrom);
				    $th=$tmp[2];
				    $bl=$tmp[1];
				    $hr=$tmp[0];
				    $tgl=$th."-".$bl."-".$hr;
			    }
			    $tmp = explode("-", $tgl);
			    $det	= $tmp[2];
			    $mon	= $tmp[1];
			    $yir 	= $tmp[0];
			    $dsaldo	= $yir."/".$mon."/".$det;
    			$dtos	= $this->mmaster->dateAdd("d",-1,$dsaldo);
			    $tmp 	= explode("-", $dtos);
			    $det1	= $tmp[2];
			    $mon1	= $tmp[1];
			    $yir1 	= $tmp[0];
			    $dtos	= $yir1."-".$mon1."-".$det1;
			
  			$saldoawal= $this->mmaster->bacasaldo($dtos,$icoabank);
  			$this->load->library('PHPExcel');
	  		$this->load->library('PHPExcel/IOFactory');
	  		$objPHPExcel = new PHPExcel();
        $namabank='';
        foreach($isi as $row){
          $namabank=$row->e_bank_name;
          break;
        }
	  		$objPHPExcel->getProperties()->setTitle("Kas Bank ".$namabank." Periode ".$dfrom." s/d ".$dto)->setDescription(NmPerusahaan);
	  		$objPHPExcel->setActiveSheetIndex(0);
        if(count($isi)>0){
				  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:I3'
				);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'NO');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'TANGGAL');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'NO.');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C2', 'REFF');
			  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'KETERANGAN');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'NO');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E2', 'PERK');
			  $objPHPExcel->getActiveSheet()->setCellValue('F1', ' ');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('G1', 'DEBET');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('G2', '(RP)');
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'KREDIT');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H2', '(RP)');
			  $objPHPExcel->getActiveSheet()->setCellValue('I1', 'SALDO');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

        $objPHPExcel->getActiveSheet()->setCellValue('D3', 'Saldo Awal Per '.substr($dfrom,0,2).' '.mbulan(substr($dfrom,3,2)).' '.substr($dfrom,6,4));
				$objPHPExcel->getActiveSheet()->getStyle('D3')->applyFromArray(
					array(
						'borders' => array(
						)
					)
				);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('I3', $saldoawal, Cell_DataType::TYPE_NUMERIC);

        $i=4;
        $j=1;
			  $cell='B';
			  $saldo=0;
        foreach($isi as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
		      array(
			      'font' => array(
				      'name'	=> 'Arial',

				      'bold'  => false,
				      'italic'=> false,
				      'size'  => 10
			      )
		      ),

		      'A'.$i.':I'.$i
		      );
		      if($j==1)$saldo = $saldoawal;
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $j, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->d_bank, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_reff, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->e_description, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->i_coa, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
		      $saldo = ($saldo + $row->v_debet)-$row->v_kredit;
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->v_debet, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->v_kredit, Cell_DataType::TYPE_NUMERIC);          
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $saldo, Cell_DataType::TYPE_NUMERIC);
			    $i++;
          $j++;
          $ebank = $row->e_bank_name;
          }
          
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
		      'A'.$i.':I'.$i
				  );
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Saldo Akhir');
				$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
					array(
						'borders' => array(
						)
					)
				);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $saldo, Cell_DataType::TYPE_NUMERIC);

			  $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $nama='Bank '.$ebank.' periode'.$dfrom.'  '.$dto.'.xls';
        if(file_exists('excel/'.$nama)){
          @chmod('excel/'.$nama, 0777);
          @unlink('excel/'.$nama);
        }
			  $objWriter->save('excel/00/'.$nama);
			  $sess=$this->session->userdata('session_id');

			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){

				  $now	  = $row['c'];
			  }
			  $pesan='Export Bank ('.$ebank.') periode '.$dfrom.' s/d '.$dto;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['sukses']			= true;
			  $data['inomor']			= $pesan;
			  $this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
  		}
    }
  }*/
#}
?>
