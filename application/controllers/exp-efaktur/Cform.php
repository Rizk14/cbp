<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		//$this->load->dbutil();
		$this->load->helper('file');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu511') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('exp-efaktur');
			$data['dfrom'] = '';
			$data['dto'] = '';
			$this->load->view('exp-efaktur/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu511') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('exp-efaktur');
			$this->load->view('exp-efaktur/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu511') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			/* $dfrom		= $this->input->post('dfrom');
			if ($dfrom != '') {
				$tmp = explode("-", $dfrom);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dfrom = $th . "-" . $bl . "-" . $hr;
			}

			$dto		= $this->input->post('dto');
			if ($dto != '') {
				$tmp = explode("-", $dto);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dto = $th . "-" . $bl . "-" . $hr;
			} */

			$dfrom 	 = date('Y-m-d', strtotime($this->uri->segment(4)));
			$dto 	 = date('Y-m-d', strtotime($this->uri->segment(5)));

			$periode = "";

			if ($dfrom <> '' && $dto <> '') {

				$this->load->model('exp-efaktur/mmaster');

				$get = $this->db->query(" SELECT DISTINCT to_char(d_nota,'yyyymm') as periodenota FROM tm_nota WHERE d_pajak BETWEEN '$dfrom' AND '$dto' ");

				if ($get->num_rows() > 1) {
					echo "<script language='javascript' type='text/javascript'>alert ('Harap Pilih Tanggal dalam 1 Periode yang Sama !!!');</script>";

					$data['page_title'] = $this->lang->line('exp-efaktur');
					$data['dfrom'] = '';
					$data['dto'] = '';
					$this->load->view('exp-efaktur/vmainform', $data);
				} else {
					if ($get->num_rows() > 0) {

						$periode = $get->row()->periodenota;

						if ($periode > '202203') {
							$query	= $this->mmaster->bacaheader_newexc($dfrom, $dto);
						} else {
							$query = $this->mmaster->bacaheader($dfrom, $dto);
						}

						// force download  
						header("Content-Type: application/force-download");
						header("Content-Type: application/octet-stream");
						header("Content-Type: application/download");

						$filename 	= "efaktur(" . $dfrom . "_" . $dto . ").csv";

						// disposition / encoding on response body
						header("Content-Disposition: attachment;filename={$filename}");
						header("Content-Transfer-Encoding: binary");

						// $out		= fopen('pajak/' . $filename, 'w');
						$out = fopen('php://output', 'w');

						fputcsv($out, array("FK", "KD_JENIS_TRANSAKSI", "FG_PENGGANTI", "NOMOR_FAKTUR", "MASA_PAJAK", "TAHUN_PAJAK", "TANGGAL_FAKTUR", "NPWP", "NAMA", "ALAMAT_LENGKAP", "JUMLAH_DPP", "JUMLAH_PPN", "JUMLAH_PPNMBM", "ID_KETERANGAN_TAMBAHAN", "FG_UANG_MUKA", "UANG_MUKA_DPP", "UANG_MUKA_PPN", "UANG_MUKA_PPNBM", "REFERENSI", "KODE_DOKUMEN_PENDUKUNG"));
						fputcsv($out, array("LT", "NPWP", "NAMA", "JALAN", "BLOK", "NOMOR", "RT", "RW", "KECAMATAN", "KELURAHAN", "KABUPATEN", "PROPINSI", "KODE_POS", "NOMOR_TELEPON"));
						fputcsv($out, array("OF", "KODE_OBJEK", "NAMA", "HARGA_SATUAN", "JUMLAH_BARANG", "HARGA_TOTAL", "DISKON", "DPP", "PPN", "TARIF_PPNBM", "PPNBM"));

						if (is_array($query)) {
							foreach ($query as $qu) {
								$pkp = trim($qu->e_customer_pkpnpwp);
								if ($pkp == '') {
									$custname 	 = $qu->e_customer_name;
									$custaddress = $qu->e_customer_address;
								} else {
									$custname 	 = $qu->e_customer_pkpname;
									$custaddress = $qu->e_customer_pkpaddress;
								}

								$inota	= $qu->i_nota;
								$dpp	= $qu->dpp;
								$ppn	= $qu->ppn;

								if ($qu->f_pajak_pengganti == 't') {
									$fg = '1';
								} else {
									$fg = '0';
								}

								$qu->i_seri_pajak = substr($qu->i_seri_pajak, 3, strlen($qu->i_seri_pajak) - 1);
								$qu->i_seri_pajak = str_replace('.', '', $qu->i_seri_pajak);
								$qu->i_seri_pajak = str_replace('-', '', $qu->i_seri_pajak);

								if ($qu->e_customer_pkpnpwp == '') $qu->e_customer_pkpnpwp = '000000000000000';
								$qu->e_customer_pkpnpwp = str_replace('.', '', $qu->e_customer_pkpnpwp);
								$qu->e_customer_pkpnpwp = str_replace('-', '', $qu->e_customer_pkpnpwp);

								fputcsv($out, array(
									"FK", "01", $fg, $qu->i_seri_pajak, $qu->masa_pajak, $qu->tahun_pajak, $qu->tgl_pajak,
									$qu->e_customer_pkpnpwp, $custname, $custaddress, $dpp, $ppn, "0", "", "0", "0", "0", "0",
									$qu->i_faktur_komersial . " / " . $qu->i_nota, "0"
								));  /* ." / NIK-".$qu->i_nik */

								if ($periode > '202203') {
									$queri	= $this->mmaster->bacadetail_newexc($dfrom, $dto, $inota);
								} else {
									$this->db->select("	b.n_deliver, (b.v_unit_price/1.1) as v_unit_price, b.e_product_name, 
														(b.n_deliver * b.v_unit_price) / 1.1 as sub, 
														a.n_nota_discount1 as disc1, a.n_nota_discount2 as disc2,
														a.n_nota_discount3 as disc3, a.n_nota_discount4 as disc4,
														a.*, (b.n_deliver * (b.v_unit_price/1.1) )as v_subtotal, a.i_nota, a.i_seri_pajak, b.i_product
														from tm_nota  a, tm_nota_item b
														where a.f_nota_cancel='false' and (d_pajak >='$dfrom' and d_pajak <='$dto')
														and a.i_nota = b.i_nota and (a.i_nota='$inota') order by b.n_item_no", false);
									$queri = $this->db->get();
								}

								$totaldis 		= 0;
								$totalbayaritem = 0;

								foreach ($queri->result() as $row) {
									if ($periode > '202203') {
										$totaldis 			  = round($row->v_nota_discount / $row->excl_divider);
										$total				  = ($row->v_dpp + $totaldis);
										$row->v_unit_price 	  = round($row->v_unit_price / $row->excl_divider);
										$dpp 				  = $row->v_dpp;
										$ppn 				  = $row->v_ppn;
									} else {
										$total 				  = $row->sub;
										$totaldis   		  = ($total * $qu->diskon) / 100;
										$totalbayaritem 	  = $total - $totaldis;
										$dpp				  = $totalbayaritem;
										$ppn				  = $totalbayaritem * 0.1;
									}

									$row->e_product_name = str_replace(";", "", $row->e_product_name);

									if ($row->i_nota == $inota) {
										fputcsv($out, array("OF", $row->i_product, $row->e_product_name, $row->v_unit_price, $row->n_deliver, $total, $totaldis, $dpp, $ppn, "0", "0"));
										// fputcsv($out, array("OF",$row->i_product, $row->e_product_name, $row->v_unit_price, $row->n_deliver, $row->sub, $totaldis, $dpp, $ppn, "0", "0"));
									}
								}
							}
						}

						fclose($out);

						// $data['sukses']	= true;
						// $data['folder']	= "exp-efaktur";

						// $this->load->view('statusexport',$data);
					} else {
						echo "<script language='javascript' type='text/javascript'>alert ('Belum Ada Data !!!');self.close();</script>";

						/* $data['page_title'] = $this->lang->line('exp-efaktur');
						$data['dfrom'] = '';
						$data['dto'] = '';
						$this->load->view('exp-efaktur/vmainform', $data); */
					}
				}
			} else {
				echo "<script language='javascript' type='text/javascript'>alert ('Pilih Tanggal Terlebih Dahulu !!!');</script>";

				$data['page_title'] = $this->lang->line('exp-efaktur');
				$data['dfrom'] = '';
				$data['dto'] = '';
				$this->load->view('exp-efaktur/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
