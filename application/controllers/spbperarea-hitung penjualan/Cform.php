<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu94')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spbperarea');
			$data['iperiode']='';
			$data['dto']	= '';
			$this->load->view('spbperarea/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu94')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('spbperarea/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			$config['base_url'] = base_url().'index.php/spbperarea/cform/view/'.$iperiode.'/';
			$data['page_title'] = $this->lang->line('spbperarea');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']		= $this->mmaster->baca($iperiode);

      $sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Membuka Data Hitung Penjualan SPB Per Area periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('spbperarea/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu94')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/spbperarea/cform/view/'.$dfrom.'/'.$dto.'/index/';
			$query = $this->db->query(" select sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, a.i_area, b.e_area_name
										from tm_spb a, tr_area b
										where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_spb <= to_date('$dto','dd-mm-yyyy') and a.f_spb_cancel='f'
										and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')
										and a.i_area=b.i_area
										group by a.i_area, b.e_area_name",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('spbperarea/mmaster');
			$data['page_title'] = $this->lang->line('spbperarea');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isi']	= $this->mmaster->cariperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);
			$this->load->view('spbperarea/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function paging()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu94')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/spbperarea/cform/paging/'.$dfrom.'/'.$dto.'/';
			$query = $this->db->query(" select sum(a.v_spb) as v_spb, sum(a.v_spb_discounttotal) as v_spb_discounttotal, a.i_area, b.e_area_name
										from tm_spb a, tr_area b
										where a.d_spb >= to_date('$dfrom','dd-mm-yyyy') 
										and a.d_spb <= to_date('$dto','dd-mm-yyyy') 
										and a.i_area=b.i_area and a.f_spb_cancel='f'
										group by a.i_area, b.e_area_name",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('spbperarea/mmaster');
			$data['page_title'] = $this->lang->line('spbperarea');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isi']	= $this->mmaster->bacaperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('spbperarea/vformview',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
