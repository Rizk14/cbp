<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu215')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjtolak');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('sjtolak/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu215')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/sjtolak/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      $area1	= $this->session->userdata('i_area');
			if($area1=='00'){
			  $query = $this->db->query(" select a.i_sj from tm_nota a, tr_area b
                                    where a.i_area=b.i_area and 
                                    (upper(a.i_sj) like '%$cari%') and 
                                    substring(a.i_sj,9,2)='$iarea' and 
                                    (a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')) 
                                    order by a.i_sj asc, a.d_sj desc ",false);
      }else{
			  $query = $this->db->query(" select a.i_sj from tm_nota a, tr_area b
                                    where a.i_area=b.i_area and 
                                    (upper(a.i_sj) like '%$cari%') and 
                                    substring(a.i_sj,9,2)='$iarea' and 
                                    (a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')) 
                                    order by a.i_sj asc, a.d_sj desc ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('sjtolak');
			$this->load->model('sjtolak/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['isj']		= '';
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('sjtolak/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}	
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu215')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjtolak');
			$this->load->view('sjtolak/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu215')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area'); // jadikan area_from // area login
			$isj	= $this->uri->segment(4);
			$iarea= $this->uri->segment(5); // area_to
			$dfrom= $this->uri->segment(6);
			$dto	= $this->uri->segment(7);
			$areafrom	= $this->uri->segment(8);
			$this->load->model('sjtolak/mmaster');
			//$this->mmaster->delete($isj,$iarea,$area1);
			$this->mmaster->delete($isj,$iarea,$areafrom);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/sjtolak/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_sj a, tr_area b
																	where a.i_area_from=b.i_area and a.i_sj_type='04' 
																	and (upper(a.i_sj) like '%$cari%')
																	and a.i_area_to='$iarea' and
																	a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_sj <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('sjtolak');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('sjtolak/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu215')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/sjtolak/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      $area1	= $this->session->userdata('i_area');
			if($area1=='00'){
			  $query = $this->db->query(" select a.*, b.e_area_name from tm_sj a, tr_area b
																	  where a.i_area_to=b.i_area and a.i_sj_type='04' 
																	  and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
																	  and a.i_area_to='$iarea' 
																	  and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
																	  a.d_sj <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
			  $query = $this->db->query(" select a.*, b.e_area_name from tm_sj a, tr_area b
																	  where a.i_area_from=b.i_area and a.i_sj_type='04' 
																	  and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
																	  and a.i_area_to='$iarea'
                                    and a.f_sj_daerah='t'
                                    and (a.i_sj like '%$cari%' or a.i_sj_old like '%$cari%')
                                    and                                    
																	  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
																	  a.d_sj <= to_date('$dto','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('sjtolak');
			$this->load->model('sjtolak/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('sjtolak/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu215')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/sjtolak/cform/area/'.$baris.'/sikasep/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
        	$data['cari']='';
			$this->load->model('sjtolak/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6),$iuser);
			$this->load->view('sjtolak/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu215')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris   = $this->input->post('baris', FALSE);
	        if($baris=='')$baris=$this->uri->segment(4);
	        $cari   = ($this->input->post('cari', FALSE));
	        if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
	        if($cari!='sikasep')
	        $config['base_url'] = base_url().'index.php/sjtolak/cform/cariarea/'.$baris.'/'.$cari.'/';
	          else
	        $config['base_url'] = base_url().'index.php/sjtolak/cform/cariarea/'.$baris.'/sikasep/';

			$iuser   	= $this->session->userdata('user_id');

			$query   	= $this->db->query("select * from tr_area
			                      where (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%' 
			                      and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('sjtolak/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['baris']=$baris;
         	$data['cari']=$cari;
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6),$iuser);
			$this->load->view('sjtolak/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function tolak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu215')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjtolak');
			if($this->uri->segment(4)!=''){
				$isj 	  = $this->uri->segment(4);
				$iarea  = $this->uri->segment(5);
				$dfrom	= $this->uri->segment(6);
				$dto 	  = $this->uri->segment(7);
        $iareasj= substr($isj,8,2);
				$data['isj'] 	  = $isj;
				$data['iarea']	= $iarea;
 		 		$data['iareasj']= substr($isj,8,2);
				$data['dfrom']	= $dfrom;
				$data['dto']	  = $dto;
				$query 	= $this->db->query("select i_sj from tm_nota_item where i_sj = '$isj'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('sjtolak/mmaster');
				$data['isi']=$this->mmaster->baca($isj,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isj,$iarea);
				$this->db->select("	a.*, b.e_area_name from tm_nota a, tr_area b
									          where a.i_sj ='$isj' 
									          and a.i_area=b.i_area", false);#and substring(a.i_sj,9,2)='$iarea'
				$query = $this->db->get();
				if ($query->num_rows() > 0){
					foreach($query->result() as $row){
						$data['dsj']=$row->d_sj;
						$data['ispb']=$row->i_spb;
						$data['dspb']=$row->d_spb;
						if($iareasj=='00'){
							$data['istore']='AA';
						}else{
							$data['istore']=$iareasj;
						}
						$data['isjold']=$row->i_sj_old;
						$data['eareaname']=$row->e_area_name;
						$data['vsjgross']=$row->v_nota_gross;
						$data['nsjdiscount1']=$row->n_nota_discount1;
						$data['nsjdiscount2']=$row->n_nota_discount2;
						$data['nsjdiscount3']=$row->n_nota_discount3;
						$data['vsjdiscount1']=$row->v_nota_discount1;
						$data['vsjdiscount2']=$row->v_nota_discount2;
						$data['vsjdiscount3']=$row->v_nota_discount3;
						$data['vsjdiscounttotal']=$row->v_nota_discounttotal;
						$data['vsjnetto']=$row->v_nota_netto;
						$data['icustomer']=$row->i_customer;
						$data['isalesman']=$row->i_salesman;
					}
				}
		 		$this->load->view('sjtolak/vmainform',$data);
			}else{
				$this->load->view('sjtolak/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu215')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj	  = $this->input->post('isj', TRUE);
			$dsj   	= $this->input->post('dsj', TRUE);
			$dsjt  	= $this->input->post('dsjt', TRUE);
 			$iarea	= $this->input->post('iarea', TRUE);
			$ispb 	= $this->input->post('ispb', TRUE);
			$dspb	  = $this->input->post('dspb', TRUE);
      if($dsjt!=''){
				$tmp	= explode("-",$dsjt);
				$th	= $tmp[2];
				$bl	= $tmp[1];
				$hr	= $tmp[0];
				$dsjt	= $th."-".$bl."-".$hr;
        $thbl=$th.$bl;
			}
			if($dsj!=''){
				$tmp	= explode("-",$dsj);
				$th	= $tmp[2];
				$bl	= $tmp[1];
				$hr	= $tmp[0];
				$dsj	= $th."-".$bl."-".$hr;
			}
			if($dspb!=''){
				$tmp=explode("-",$dspb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspb=$th."-".$bl."-".$hr;
			}
			$eareaname	= $this->input->post('eareaname', TRUE);
			$isalesman		= $this->input->post('isalesman',TRUE);
			$icustomer		= $this->input->post('icustomer',TRUE);
			$nsjdiscount1	= $this->input->post('nsjdiscount1',TRUE);
			$nsjdiscount1	= str_replace(',','',$nsjdiscount1);
			$nsjdiscount2	= $this->input->post('nsjdiscount2',TRUE);
			$nsjdiscount2	= str_replace(',','',$nsjdiscount2);
			$nsjdiscount3	= $this->input->post('nsjdiscount3',TRUE);
			$nsjdiscount3	= str_replace(',','',$nsjdiscount3);
			$vsjdiscount1	= $this->input->post('vsjdiscount1',TRUE);
			$vsjdiscount1	= str_replace(',','',$vsjdiscount1);
			$vsjdiscount2	= $this->input->post('vsjdiscount2',TRUE);
			$vsjdiscount2	= str_replace(',','',$vsjdiscount2);
			$vsjdiscount3	= $this->input->post('vsjdiscount3',TRUE);
			$vsjdiscount3	= str_replace(',','',$vsjdiscount3);
			$vsjdiscounttotal	= $this->input->post('vsjdiscounttotal',TRUE);
			$vsjdiscounttotal	= str_replace(',','',$vsjdiscounttotal);
			$vsjgross	= $this->input->post('vsjgross',TRUE);
			$vsjgross	= str_replace(',','',$vsjgross);
			$vsjnetto	= $this->input->post('vsjnetto',TRUE);
			$vsjnetto	= str_replace(',','',$vsjnetto);

			$vsjtdiscount1	= $this->input->post('vsjtdiscount1',TRUE);
			$vsjtdiscount1	= str_replace(',','',$vsjtdiscount1);
			$vsjtdiscount2	= $this->input->post('vsjtdiscount2',TRUE);
			$vsjtdiscount2	= str_replace(',','',$vsjtdiscount2);
			$vsjtdiscount3	= $this->input->post('vsjtdiscount3',TRUE);
			$vsjtdiscount3	= str_replace(',','',$vsjtdiscount3);
			$vsjtdiscounttotal	= $this->input->post('vsjtdiscounttotal',TRUE);
			$vsjtdiscounttotal	= str_replace(',','',$vsjtdiscounttotal);
			$vsjtgross	= $this->input->post('vsjtgross',TRUE);
			$vsjtgross	= str_replace(',','',$vsjtgross);
			$vsjtnetto	= $this->input->post('vsjtnetto',TRUE);
			$vsjtnetto	= str_replace(',','',$vsjtnetto);

			$jml	  = $this->input->post('jml', TRUE);
			if( ($vsjtnetto!='0' || $vsjtnetto!='') && $dsjt!='')
			{
				$gaono=true;
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					if($cek=='on'){
						$gaono=false;
					}
					if(!$gaono) break;
				}
				if(!$gaono){
					$this->db->trans_begin();
					$this->load->model('sjtolak/mmaster');
					$istore	= $this->input->post('istore', TRUE);
					if($istore=='AA'){
						$istorelocation		= '01';
					}else{
						$istorelocation		= '00';
					}
					$istorelocationbin	= '00';
					$eremark		= 'SPB';
          $isjt	 		= $this->mmaster->runningnumbersj($iarea,$thbl);
				  $this->mmaster->updatesjheader( $ispb,$dspb,$isj,$dsj,$iarea,$isalesman,$icustomer,
							                            $nsjdiscount1,$nsjdiscount2,$nsjdiscount3,$vsjdiscount1, 
						                              $vsjdiscount2,$vsjdiscount3,$vsjdiscounttotal,$vsjgross,$vsjnetto);
          $this->mmaster->insertsjtheader($isj,$dsj,$iarea,$isjt,$dsjt,$nsjtdiscount1,$nsjtdiscount2,$nsjtdiscount3,$vsjtdiscount1,
                                          $vsjtdiscount2,$vsjtdiscount3,$vsjtdiscounttotal,$vsjtgross,$vsjtnetto);
#					$this->mmaster->updatespb($ispb,$iarea,$isj,$dsj);
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
						$iproduct	= $this->input->post('iproduct'.$i, TRUE);
						$iproductgrade= 'A';
						$iproductmotif= $this->input->post('motif'.$i, TRUE);
						$ntmp		= $this->input->post('ntmp'.$i, TRUE);
						$ntmp		= str_replace(',','',$ntmp);
#						$this->mmaster->deletesjdetail($iproduct,$iproductgrade,$iproductmotif,$isj,$iarea);

						$th=substr($dsj,0,4);
						$bl=substr($dsj,5,2);
						$emutasiperiode=$th.$bl;
						$tra=$this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj);
					  $this->mmaster->updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ntmp,$emutasiperiode);
					  $this->mmaster->updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ntmp);
						if($cek=='on'){
						  $ndeliver		= $this->input->post('ndeliver'.$i, TRUE);
						  $ndeliver		= str_replace(',','',$ndeliver);
						  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice	= $this->input->post('vproductmill'.$i, TRUE);
						  $vunitprice	= str_replace(',','',$vunitprice);
						  $norder		= $this->input->post('norder'.$i, TRUE);
						  $norderx		= str_replace(',','',$norder);
						  
						  if($ndeliver>0){	
							  $this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                                    			     $vunitprice,$isj,$iarea,$i);

						  $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
						  if(isset($trans)){
							foreach($trans as $itrans)
							{
							  $q_aw =$itrans->n_quantity_stock;
							  $q_ak =$itrans->n_quantity_stock;
							  $q_in =0;
							  $q_out=0;
							  break;
							}
						  }else{
							$q_aw=0;
							$q_ak=0;
							$q_in=0;
							$q_out=0;
						  }	
							$this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$ndeliver,$q_aw,$q_ak,$tra);
							
							if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
							{
							  $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
							}else{
							  $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode,$q_aw);
							}
							if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
							{
							  $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$q_ak);
							}else{
							  $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ndeliver,$q_aw);
							}
							$this->mmaster->updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$iarea);
						  }
						}else{
              $this->mmaster->updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,0,$iarea);
            }
					}
					if (($this->db->trans_status() === FALSE))
					{
						$this->db->trans_rollback();
					}else{
#						$this->db->trans_commit();
						$this->db->trans_rollback();

            $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		=  $this->db->query($sql);
			      if($rs->num_rows>0){
				      foreach($rs->result() as $tes){
					      $ip_address	  = $tes->ip_address;
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }

			      $data['user']	= $this->session->userdata('user_id');
      #			$data['host']	= $this->session->userdata('printerhost');
			      $data['host']	= $ip_address;
			      $data['uri']	= $this->session->userdata('printeruri');
			      $query 	= pg_query("SELECT current_timestamp as c");
			      while($row=pg_fetch_assoc($query)){
				      $now	  = $row['c'];
			      }
			      $pesan='SJ tolak No:'.$isj;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan );

						$data['sukses']			= true;
						if($sjnew=1){
							$data['inomor']			= $newsj;
						}else{
							$data['inomor']			= $isj;
						}	
						$this->load->view('nomor',$data);
					}
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
