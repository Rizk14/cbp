<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu375')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listdoallarea/cform/index/';
			$data['page_title'] = $this->lang->line('listdo');
			$this->load->model('listdoallarea/mmaster');
			$data['iperiode']='';
			$data['isi']='';
			$data['cari'] = '';
			$this->load->view('listdoallarea/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu375')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari			= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$query = $this->db->query(" select a.f_do_cancel, a.i_do, a.d_do, a.i_op, a.i_area, c.i_spb, c.i_spb_old, d.i_spmb, d.i_spmb_old, e.e_supplier_name, 
                            a.i_supplier from tm_do a, tr_supplier e,
                            tm_op b left join tm_spb c on(b.i_reff=c.i_spb and b.i_area=c.i_area) 
                            left join tm_spmb d on(b.i_reff=d.i_spmb and b.i_area=d.i_area)
                            where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                            and a.i_op=b.i_op and a.i_area=b.i_area and a.i_supplier=e.i_supplier and
                            (upper(a.i_supplier) like '%$cari%' or upper(e.e_supplier_name) like '%$cari%'
							              or upper(a.i_do) like '%$cari%' or upper(b.i_op) like '%$cari%'
							              or upper(d.i_spmb) like '%$cari%' or upper(c.i_spb) like '%$cari%'
							              or upper(d.i_spmb_old) like '%$cari%' or upper(c.i_spb_old) like '%$cari%')
				                    order by a.d_do",false);
			$config['base_url'] = base_url().'index.php/listdoallarea/cform/view/'.$iperiode.'/';
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdo');
			$this->load->model('listdoallarea/mmaster');
			$data['isi']=$this->mmaster->bacasemua($iperiode,$cari,$config['per_page'],$this->uri->segment(5));
			$data['cari'] = $cari;
			$data['iperiode'] = $iperiode;

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data DO All Area Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listdoallarea/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu375')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdo');
			$this->load->view('listdoallarea/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu375')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listdoallarea/cform/view/'.$iperiode.'/';
  	  $query = $this->db->query("select a.f_do_cancel, a.i_do, a.d_do, a.i_op, a.i_area, c.i_spb, c.i_spb_old, d.i_spmb, d.i_spmb_old, e.e_supplier_name, 
                            a.i_supplier from tm_do a, tr_supplier e,
                            tm_op b left join tm_spb c on(b.i_reff=c.i_spb and b.i_area=c.i_area) 
                            left join tm_spmb d on(b.i_reff=d.i_spmb and b.i_area=d.i_area)
                            where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode' and
                            (upper(a.i_supplier) like '%$cari%' or upper(e.e_supplier_name) like '%$cari%'
							              or upper(a.i_do) like '%$cari%' or upper(b.i_op) like '%$cari%'
							              or upper(d.i_spmb) like '%$cari%' or upper(c.i_spb) like '%$cari%'
							              or upper(d.i_spmb_old) like '%$cari%' or upper(c.i_spb_old) like '%$cari%')
                            and a.i_op=b.i_op and a.i_area=b.i_area and a.i_supplier=e.i_supplier
				                    order by a.d_do",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdo');
			$this->load->model('listdoallarea/mmaster');
			$data['isi']=$this->mmaster->cari($iperiode,$cari,$config['per_page'],$this->uri->segment(5));
			$data['cari'] = $cari;
			$data['iperiode'] = $iperiode;
			//$data['area1']=$area1;
			$this->load->view('listdoallarea/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu375')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdoallarea');
			if(
				($this->uri->segment(4)!=='') && ($this->uri->segment(5)!=='')
			  ){			
          $ido	  	  = $this->uri->segment(4);					
		      $ido          = str_replace('%20',' ',$ido);
					$isupplier 	  = $this->uri->segment(5);
					$data['iperiode']= $this->uri->segment(6);
					$query 	      = $this->db->query("select * from tm_do_item where i_do = '$ido' and i_supplier='$isupplier'");
					$data['jmlitem'] = $query->num_rows(); 				
					$data['ido'] = $ido;
					$data['isupplier']=$isupplier;
					$this->load->model('listdoallarea/mmaster');
					$data['isi']=$this->mmaster->baca($ido,$isupplier);
					$data['detail']=$this->mmaster->bacadetail($ido, $isupplier);
					$this->load->view('listdoallarea/vformupdate',$data);
			}else{
				$this->load->view('listdoallarea/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
