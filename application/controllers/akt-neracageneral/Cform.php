<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('neracageneral');
			$data['periode']	= '';
			$this->load->view('akt-neracageneral/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$periode	= $this->uri->segment(4);
			if(
				($periode!='')
			  )
			{
				$this->load->model('akt-neracageneral/mmaster');
				$data['page_title'] = $this->lang->line('neracageneral');
        $tahun=substr($periode,0,4);
        $bulan=substr($periode,4,2);
        $tahun2=substr($periode,0,4);
        $tanggal='01';
				$periode=$tahun.$bulan;
				if($bulan=='01'){
					$bulan2='12';
					$tahun2=$tahun-1;
				}else{
					$bulan2=$bulan-1;
					if(strlen($bulan2)<2){
						$bulan2='0'.$bulan2;
						$tahun2=$tahun;
					}
				}
				$periodelalu=$tahun2.$bulan2;
				$data['periodelalu']=$tahun2.$bulan2;
				$kiwari		= $tahun."/".$bulan."/01";
				$namabulan	= $this->mmaster->NamaBulan($bulan);
				$data['periode']	= $periode;
				$dfrom				= $tahun."-".$bulan."-01";
				$dto				= $tahun."-".$bulan."-".$tanggal;
				if($periode=='201601'){
  			  $data['bank']		= $this->mmaster->bacabank($periode);
        }else{
  			  $data['bankbcacmh']		= $this->mmaster->bacabankbcacmh($periode);
  			  $data['bankbcajkt']		= $this->mmaster->bacabankbcajkt($periode);
  			  $data['bankbcadago']		= $this->mmaster->bacabankbcadago($periode);
  			  $data['bankbri']		= $this->mmaster->bacabankbri($periode);
  			  $data['bankpermata']		= $this->mmaster->bacabankpermata($periode);
        }
			  $data['dfrom']		= $dfrom;
			  $data['dto']		= $tahun."-".$bulan."-".$tanggal;
			  $data['tanggal']	= $tanggal;
			  $data['namabulan']	= $namabulan;
			  $data['tahun']		= $tahun;
			  $data['kaskecil']		= $this->mmaster->bacakaskecil($periode);
			  $data['kasbesar']		= $this->mmaster->bacakasbesar($periode);
			  $data['bankbcajkt']		= $this->mmaster->bacabankbcajkt($periode);
			  $data['piutang']	= $this->mmaster->bacapiutang($periode);
			  $data['piutangkaryawan']		= $this->mmaster->bacapiutangkaryawan($periode);
			  $data['hutangdagang']= $this->mmaster->bacahutangdagang($periode);
			  $data['modal']		= $this->mmaster->bacamodal($periode);
			  $data['piutanglain']		= $this->mmaster->bacapiutanglain($periode);
			  $data['piutangsementara']		= $this->mmaster->bacapiutangsementara($periode);
			  $data['biayadibayardimuka']		= $this->mmaster->bacabiayadibayardimuka($periode);
			  $data['persediaanbarangdagang']	= $this->mmaster->bacapersediaanbarangdagang($periode);
			  $data['possementaradebet']		= $this->mmaster->bacapossementaradebet($periode);
			  $data['aktivatetapkel1']		= $this->mmaster->bacaaktivatetapkel1($periode);
			  $data['akumaktiva1']			= $this->mmaster->bacaakumaktiva1($periode);
			  $data['aktivatetapkel2']		= $this->mmaster->bacaaktivatetapkel2($periode);
			  $data['akumaktiva2']			= $this->mmaster->bacaakumaktiva2($periode);
			  $data['akumamortisasi']			= $this->mmaster->bacaakumamortisasi($periode);
			  $data['biayayangditangguhkan']	= $this->mmaster->bacabiayayangditangguhkan($periode);
			  $data['akumamortisasiyangditangguhkan']	= $this->mmaster->bacaakumamortisasiyangditangguhkan($periode);
			  $data['hutangbank']				= $this->mmaster->bacahutangbank($periode);
			  $data['pajakbyrdmk']				= $this->mmaster->bacapajakbyrdmk($periode);
			  $data['sewabyrdmk']				= $this->mmaster->bacasewabyrdmk($periode);
			  $data['hutangpajakpph21']		= $this->mmaster->bacahutangpajakpph21($periode);
			  $data['hutangpajakpph22']		= $this->mmaster->bacahutangpajakpph22($periode);
			  $data['hutangpajakpph23']		= $this->mmaster->bacahutangpajakpph23($periode);
			  $data['hutangpajakpph24']		= $this->mmaster->bacahutangpajakpph24($periode);
			  $data['hutangpajakppn']			= $this->mmaster->bacahutangpajakppn($periode);
			  $data['hutanglain']				= $this->mmaster->bacahutanglain($periode);
			  $data['possementara']			= $this->mmaster->bacapossementara($periode);
			  $data['hutangbankjangkapanjang']= $this->mmaster->bacahutangbankjangkapanjang($periode);
			  $data['modalyangdisetor']		= $this->mmaster->bacamodalyangdisetor($periode);
			  $data['labarugiyangditahantahunberjalan']		= $this->mmaster->bacalabarugiyangditahantahunberjalan($periode);
			  $data['labarugiyangditahan']					= $this->mmaster->bacalabarugiyangditahan($periode);
			  $data['labarugiyangditahanbulanberjalan']		= $this->mmaster->bacalabarugiyangditahanbulanberjalan($periode);
			  $data['pajakpph21ydm']		= $this->mmaster->bacapajakpph21ydm($periode);
			  $data['pajakpph22ydm']		= $this->mmaster->bacapajakpph22ydm($periode);
			  $data['pajakpph23ydm']		= $this->mmaster->bacapajakpph23ydm($periode);
			  $data['pajakpph25ydm']		= $this->mmaster->bacapajakpph25ydm($periode);
			  $data['uangmukaleasing']	= $this->mmaster->bacauangmukaleasing($periode);
			  $data['yhmdibayar']			= $this->mmaster->bacayhmdibayar($periode);
			  $data['hutangpajak']			= $this->mmaster->bacahutangpajak($periode);
			  $data['asskendbyrdmk']			= $this->mmaster->bacaasskendbyrdmk($periode);
			  $data['hutangsementara']			= $this->mmaster->bacahutangsementara($periode);
	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='Buka Laporan Neraca periode:'.$periode;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan );

				$this->load->view('akt-neracageneral/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
