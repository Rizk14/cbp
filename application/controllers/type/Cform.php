<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu13')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/type/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_product_type');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_type');
			$data['itype']='';
			$this->load->model('type/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('type/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu13')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$itype 	= $this->input->post('itype');
			$etypename 	= $this->input->post('etypename');
			$igroup 	= $this->input->post('igroup');
			$etypenameprint1= $this->input->post('etypenameprint1');
			$etypenameprint2= $this->input->post('etypenameprint2');

			if ($itype != '' && $etypename != '' && $igroup != '')
			{
				$this->load->model('type/mmaster');
				$this->mmaster->insert($itype,$etypename,$igroup,$etypenameprint1,$etypenameprint2);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu13')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_type');
			$this->load->view('type/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu13')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_type')." update";
			if($this->uri->segment(4)){
				$itype = $this->uri->segment(4);
				$data['itype'] = $itype;
				$this->load->model('type/mmaster');
				$data['isi']=$this->mmaster->baca($itype);
		 		$this->load->view('type/vmainform',$data);
			}else{
				$this->load->view('type/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu13')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$itype 		= $this->input->post('itype', TRUE);
			$etypename 	= $this->input->post('etypename', TRUE);
			$igroup 	= $this->input->post('igroup', TRUE);
			$egroupname = $this->input->post('egroupname', TRUE);
			$etypenameprint1 = $this->input->post('etypenameprint1', TRUE);
			$etypenameprint2 = $this->input->post('etypenameprint2', TRUE);
			$this->load->model('type/mmaster');
			$this->mmaster->update($itype,$etypename,$igroup,$etypenameprint1,$etypenameprint2);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu13')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$itype = $this->uri->segment(4);
			$this->load->model('type/mmaster');
			$this->mmaster->delete($itype);
			$config['base_url'] = base_url().'index.php/type/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_product_type');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_type');
			$data['itype']='';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('type/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function group()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu13')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/type/cform/group/index/';
			$config['total_rows'] = $this->db->count_all('tr_product_group');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('type/mmaster');
			$data['page_title'] = $this->lang->line('list_group');
			$data['isi']=$this->mmaster->bacagroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('type/vlistgroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu13')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/type/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);

			$query 	= $this->db->query("select a.i_product_type, a.e_product_typename, a.i_product_group, a.e_product_typenameprint1, a.e_product_typenameprint2, b.e_product_groupname 
										from tr_product_type a, tr_product_group b 
										where (upper(a.e_product_typename) like '%$cari%' 
										or upper(a.i_product_type) like '%$cari%' 
										or upper(b.e_product_groupname) like '%$cari%' 
										or upper(a.i_product_group) like '%$cari%') 
										and a.i_product_group=b.i_product_group", FALSE);
			$config['total_rows'] =  $query->num_rows();

			$config['total_rows'] = $this->db->count_all('tr_product_type');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			
			$this->load->model('type/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_type');
			$data['itype']='';
	 		$this->load->view('type/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function carigroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu467')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/type/cform/carigroup/index/'; 
			$cari = $this->input->get_post('cari');
			$cari = strtoupper($cari);  
			$query = $this->db->query("select i_product_group from tr_product_group where i_product_group ilike '%$cari%' or e_product_groupname ilike '%$cari%'",false); 
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('type/mmaster');
			$data['page_title'] 	= $this->lang->line('list_group');
			$data['isi']			= $this->mmaster->carigroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('type/vlistgroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
