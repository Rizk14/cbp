<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu15')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productmotif/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select a.*, b.e_product_name from tr_product_motif a, tr_product b
						    where (upper(a.i_product) like '%$cari%' or upper(a.i_product_motif) like '%$cari%' 
						    or upper(a.e_product_motifname) like '%$cari%') and a.i_product=b.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);				
			
			$data['page_title'] = $this->lang->line('master_productmotif');
			$data['iproductmotif']='';
			$data['iproductbase']='';
			$this->load->model('productmotif/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('productmotif/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu15')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct		= $this->input->post('iproduct', TRUE);
			$eproductname		= $this->input->post('eproductname', TRUE);
			$iproductmotif 		= $this->input->post('iproductmotif', TRUE);
			$eproductmotifname 	= $this->input->post('eproductmotifname', TRUE);
			if ((isset($iproductmotif) && $iproductmotif != '') && (isset($eproductmotifname) && $eproductmotifname != '') && (isset($eproductname) && $eproductname != ''))
			{
				$this->load->model('productmotif/mmaster');
				$query = $this->db->query(" select * from tr_product_motif
											where i_product='$iproduct' and i_product_motif='$iproductmotif'",false);
				if($query->num_rows()>0){
					$this->mmaster->update($iproductmotif,$eproductmotifname,$iproduct,$eproductname);
				}else{
					$this->mmaster->insert($iproductmotif,$eproductmotifname,$iproduct,$eproductname);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu15')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$data['page_title'] = $this->lang->line('master_productmotif');
			$this->load->view('productmotif/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu15')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_productmotif')." update";
			if(($this->uri->segment(4)) && ($this->uri->segment(5))){
				$iproductmotif 	= $this->uri->segment(4);
				$iproduct	= $this->uri->segment(5);
				$data['iproductmotif'] 	= $iproductmotif;
				$data['iproduct'] 	= $iproduct;
				$this->load->model('productmotif/mmaster');
				$data['isi']=$this->mmaster->baca($iproductmotif, $iproduct);
		 		$this->load->view('productmotif/vmainform',$data);
			}else{
				$this->load->view('productmotif/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu15')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproductmotif		= $this->input->post('iproductmotif', TRUE);
			$eproductmotifname 	= $this->input->post('eproductmotifname', TRUE);
			$iproduct		= $this->input->post('iproduct', TRUE);
			$eproductname 		= $this->input->post('eproductname', TRUE);
			$this->load->model('productmotif/mmaster');
			$this->mmaster->update($iproductmotif,$eproductmotifname,$iproduct,$eproductname);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu15')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproductmotif	= $this->uri->segment(4);
			$iproduct	= $this->uri->segment(5);
			$this->load->model('productmotif/mmaster');
			$this->mmaster->delete($iproductmotif, $iproduct);
			$config['base_url'] = base_url().'index.php/productmotif/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_motif
						    where upper(i_product_motif) like '%$cari%' 
						    or upper(e_product_motifname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);				
			
			$data['page_title'] = $this->lang->line('master_productmotif');
			$data['iproductmotif']='';
			$data['iproduct']='';
			$this->load->model('productmotif/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('productmotif/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu15')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productmotif/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_motif
						    where upper(i_product_motif) like '%$cari%' or upper(i_product) like '%$cari%'
						    or upper(e_product_motifname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(4);
			$this->paginationxx->initialize($config);				
			
			$data['page_title'] = $this->lang->line('master_productmotif');
			$data['iproductmotif']='';
			$data['iproductbase']='';
			$this->load->model('productmotif/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('productmotif/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productbase()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu15')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/productmotif/cform/productbase/'.$baris.'/sikasep/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query(" select * from tr_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('productmotif/mmaster');
			$data['baris']=$baris;
			$data['cari']='';
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6));
			$this->load->view('productmotif/vlistproductbase', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu15')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/productmotif/cform/productbase/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/productmotif/cform/cariproduct/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/productmotif/cform/cariproduct/'.$baris.'/sikasep/';

			$query = $this->db->query(" select a.*, b.e_product_name from tr_product_motif a, tr_product b
						    where (upper(a.i_product) ilike '%$cari%' or upper(a.i_product_motif) ilike '%$cari%' 
						    or upper(a.e_product_motifname) ilike '%$cari%') and a.i_product=b.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('productmotif/mmaster');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproductbase($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('productmotif/vlistproductbase', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari_barang() {
		$nbrg	= $this->input->post('nbrg')?$this->input->post('nbrg'):$this->input->get_post('nbrg');
		$nbrg2	= $this->input->post('nbrg2')?$this->input->post('nbrg2'):$this->input->get_post('nbrg2');
		$this->load->model('productmotif/mmaster');
		$qnbrg	= $this->mmaster->cari_brg($nbrg,$nbrg2);
		if($qnbrg->num_rows()>0) {
					$data['konfirm']	= true;
					$data['message']	= "Maaf,Kode Brg sudah ada.";
					$this->load->view('konfirm',$data);
		}
	}	
}
?>
