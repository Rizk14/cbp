<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listcustomercard');
#			$data['iperiode']='';
			$data['dfrom']='';
			$data['dto']='';
			$data['iarea']='';
			$this->load->view('listcustomercardpersupprp/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto  		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			if($isupplier=='') $isupplier	= $this->uri->segment(4);
			if($dfrom=='') $dfrom	= $this->uri->segment(5);
			if($dto=='') $dto	= $this->uri->segment(6);
			$this->load->model('listcustomercardpersupprp/mmaster');
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$iarea='NA';

			$interval	= $this->mmaster->interval($dfrom,$dto);
      $data['interval'] = $interval;
			$data['isi']		  = $this->mmaster->bacaperiode($dfrom,$dto,$iarea,$interval,$isupplier);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Customer Card Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listcustomercardpersupprp/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('listcustomercardpersupprp/mmaster');
			$iarea  	= 'NA';
			$isupplier  	= $this->input->post("isupplier");
			$istore  	= $this->input->post("istore");
			$dfrom    = $this->input->post("dfrom");
			$dto      = $this->input->post("dto");
			$this->load->model('listcustomercardpersupprp/mmaster');
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['isupplier']	= $isupplier;
			$data['istore']	= $istore;
      if($dfrom!=''){
		    $tmp=explode("-",$dfrom);
		    $blasal=$tmp[1];
        settype($bl,'integer');
	    }
      $bl=$blasal;
      
			$interval	= $this->mmaster->interval($dfrom,$dto);
      $data['interval'] = $interval;
			$isi		  = $this->mmaster->bacaperiode($dfrom,$dto,$iarea,$interval,$isupplier);
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Customer Card")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      if(count($isi)>0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(24);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(11);
#				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(11);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:w2'
				);
        
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Area');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,0,2);
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'K-LANG');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(1,1,1,2);
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Kota/Kab');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(2,1,2,2);
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Salesman');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3,1,3,2);
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Jenis');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4,1,4,2);
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Nama Lang');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5,1,5,2);
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Alamat');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6,1,6,2);
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Kode');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7,1,7,2);
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Product');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8,1,8,2);
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Supplier');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(9,1,9,2);
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('K1', 'Nota');
			  $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('K2','Jan');
    		$objPHPExcel->getActiveSheet()->getStyle('K2')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('L1', 'Nota');
			  $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('L2','Feb');
    		$objPHPExcel->getActiveSheet()->getStyle('L2')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('M1', 'Nota');
			  $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('M2','Mar');
    		$objPHPExcel->getActiveSheet()->getStyle('M2')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('N1', 'Nota');
			  $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('N2','April');
    		$objPHPExcel->getActiveSheet()->getStyle('N2')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('O1', 'Nota');
			  $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('O2','Mei');
    		$objPHPExcel->getActiveSheet()->getStyle('O2')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('P1', 'Nota');
			  $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('P2','Juni');
    		$objPHPExcel->getActiveSheet()->getStyle('P2')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Nota');
			  $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('Q2','Jul');
    		$objPHPExcel->getActiveSheet()->getStyle('Q2')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('R1', 'Nota');
			  $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('R2','Aug');
    		$objPHPExcel->getActiveSheet()->getStyle('R2')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('S1', 'Nota');
			  $objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('S2','Sep');
    		$objPHPExcel->getActiveSheet()->getStyle('S2')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('T1', 'Nota');
			  $objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('T2','Okt');
    		$objPHPExcel->getActiveSheet()->getStyle('T2')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('U1', 'Nota');
			  $objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('U2','Nov');
    		$objPHPExcel->getActiveSheet()->getStyle('U2')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
			  $objPHPExcel->getActiveSheet()->setCellValue('V1', 'Nota');
			  $objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
    		$objPHPExcel->getActiveSheet()->setCellValue('V2','Des');
    		$objPHPExcel->getActiveSheet()->getStyle('V2')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
				$objPHPExcel->getActiveSheet()->setCellValue('W1', 'Total Nota');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(23,1,23,2);
				$objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
#			  $objPHPExcel->getActiveSheet()->setCellValue('X1', 'Nilai');
#        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(24,1,24,2);
#				$objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray(
#					array(
#						'borders' => array(
#							'top' 	=> array('style' => Style_Border::BORDER_THIN)
#						),
#					)
#				);
        $total=0;
        $subtotjan=0;
        $subtotfeb=0;
        $subtotmar=0;
        $subtotapr=0;
        $subtotmay=0;
        $subtotjun=0;
        $subtotjul=0;
        $subtotaug=0;
        $subtotsep=0;
        $subtotokt=0;
        $subtotnov=0;
        $subtotdes=0;
#        $vsubtotjan=0;
#        $vsubtotfeb=0;
#        $vsubtotmar=0;
#        $vsubtotapr=0;
#        $vsubtotmay=0;
#        $vsubtotjun=0;
#        $vsubtotjul=0;
#        $vsubtotaug=0;
#        $vsubtotsep=0;
#        $vsubtotokt=0;
#        $vsubtotnov=0;
#        $vsubtotdes=0;
        
        $totareajan=0;
        $totareafeb=0;
        $totareamar=0;
        $totareaapr=0;
        $totareamay=0;
        $totareajun=0;
        $totareajul=0;
        $totareaaug=0;
        $totareasep=0;
        $totareaokt=0;
        $totareanov=0;
        $totareades=0;
        
        $grandtotjan=0;
        $grandtotfeb=0;
        $grandtotmar=0;
        $grandtotapr=0;
        $grandtotmay=0;
        $grandtotjun=0;
        $grandtotjul=0;
        $grandtotaug=0;
        $grandtotsep=0;
        $grandtotokt=0;
        $grandtotnov=0;
        $grandtotdes=0;
        $icity='';
        $kode='';
        $totalnotakota=0;
#        $vtotalnotakota=0;
        $totarea=0;
#        $vtotarea=0;
        $grandtotalnotakota=0;
#        $vgrandtotalnotakota=0;
				$i=3;
				$product='';
        foreach($isi as $row){
          $totalnota = 0;
          $totaljan = 0;
          $totalfeb = 0;
          $totalmar = 0;
          $totalapr = 0;
          $totalmay = 0;
          $totaljun = 0;
          $totaljul = 0;
          $totalaug = 0;
          $totalsep = 0;
          $totalokt = 0;
          $totalnov = 0;
          $totaldes = 0;
        
        $m=$i+4;
        if($icity=='' || ($icity==$row->icity && $kode==substr($row->kode,0,2))){
        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			  array(
				  'font' => array(
					  'name'	=> 'Arial',
					  'bold'  => false,
					  'italic'=> false,
					  'size'  => 10
				  )
			  ),
			  'A'.$i.':W'.$i
			  );
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, substr($row->kode,0,2)."-".$row->area, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->kode, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->kota, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->sales, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->jenis, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->nama, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->alamat, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->product, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->productname, Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->supplier, Cell_DataType::TYPE_STRING);
        $bl=$blasal;
        for($n=1;$n<=$interval;$n++){
          switch ($bl){
            case '1' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->notajan, Cell_DataType::TYPE_NUMERIC);
              $notajan = $row->notajan;
              $totaljan=$totaljan+$row->notajan;
              $subtotjan=$subtotjan+$row->notajan;
#              $vsubtotjan=$vsubtotjan+($row->notajan*$row->harga);
              $grandtotjan=$grandtotjan+$row->notajan;
              $totalnotakota=$totalnotakota+$row->notajan;
#              $vtotalnotakota=$vtotalnotakota+($row->notajan*$row->harga);
              $totareajan=$totareajan+$row->notajan;
              $totarea=$totarea+$row->notajan;
#              $vtotarea=$vtotarea+($row->notajan*$row->harga);
              break;
            case '2' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->notafeb, Cell_DataType::TYPE_NUMERIC);
              $totalfeb=$totalfeb+$row->notafeb;
              $notablnfeb= $row->notafeb;
              $subtotfeb=$subtotfeb+$row->notafeb;
#              $vsubtotfeb=$vsubtotfeb+($row->notafeb*$row->harga);
              $grandtotfeb=$grandtotfeb+$row->notafeb;
              $totalnotakota=$totalnotakota+$row->notafeb;
#              $vtotalnotakota=$vtotalnotakota+($row->notafeb*$row->harga);
              $totareafeb=$totareafeb+$row->notafeb;
              $totarea=$totarea+$row->notafeb;
#              $vtotarea=$vtotarea+($row->notafeb*$row->harga);
              break;
            case '3' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->notamar, Cell_DataType::TYPE_NUMERIC);
              $notablnmar = $row->notamar;
              $totalmar=$totalmar+$row->notamar;
              $subtotmar=$subtotmar+$row->notamar;
#              $vsubtotmar=$vsubtotmar+($row->notamar*$row->harga);
              $grandtotmar=$grandtotmar+$row->notamar;
              $totalnotakota=$totalnotakota+$row->notamar;
#              $vtotalnotakota=$vtotalnotakota+($row->notamar*$row->harga);
              $totareamar=$totareamar+$row->notamar;
              $totarea=$totarea+$row->notamar;
#              $vtotarea=$vtotarea+($row->notamar*$row->harga);
              break;
            case '4' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->notaapr, Cell_DataType::TYPE_NUMERIC);
              $totalapr=$totalapr+$row->notaapr;
              $notablnapr = $row->notaapr;
              $subtotapr=$subtotapr+$row->notaapr;
#              $vsubtotapr=$vsubtotapr+($row->notaapr*$row->harga);
              $grandtotapr=$grandtotapr+$row->notaapr;
              $totalnotakota=$totalnotakota+$row->notaapr;
#              $vtotalnotakota=$vtotalnotakota+($row->notaapr*$row->harga);
              $totareaapr=$totareaapr+$row->notaapr;
              $totarea=$totarea+$row->notaapr;
#              $vtotarea=$vtotarea+($row->notaapr*$row->harga);
              break;
            case '5' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $row->notamay, Cell_DataType::TYPE_NUMERIC);
              $notablnmay = $row->notamay;
              $totalmay=$totalmay+$row->notamay;
              $subtotmay=$subtotmay+$row->notamay;
#              $vsubtotmay=$vsubtotmay+($row->notamay*$row->harga);
              $grandtotmay=$grandtotmay+$row->notamay;
              $totalnotakota=$totalnotakota+$row->notamay;
#              $vtotalnotakota=$vtotalnotakota+($row->notamay*$row->harga);
              $totareamay=$totareamay+$row->notamay;
              $totarea=$totarea+$row->notamay;
#              $vtotarea=$vtotarea+($row->notamay*$row->harga);
              break;
            case '6' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $row->notajun, Cell_DataType::TYPE_NUMERIC);
              $notablnjun = $row->notajun;
              $totaljun=$totaljun+$row->notajun;
              $subtotjun=$subtotjun+$row->notajun;
#              $vsubtotjun=$vsubtotjun+($row->notajun*$row->harga);
              $grandtotjun=$grandtotjun+$row->notajun;
              $totalnotakota=$totalnotakota+$row->notajun;
#              $vtotalnotakota=$vtotalnotakota+($row->notajun*$row->harga);
              $totareajun=$totareajun+$row->notajun;
              $totarea=$totarea+$row->notajun;
#              $vtotarea=$vtotarea+($row->notajun*$row->harga);
              break;
            case '7' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $row->notajul, Cell_DataType::TYPE_NUMERIC);
              $totaljul=$totaljul+$row->notajul;
              $notablnjul = $row->notajul;
              $subtotjul=$subtotjul+$row->notajul;
#              $vsubtotjul=$vsubtotjul+($row->notajul*$row->harga);
              $grandtotjul=$grandtotjul+$row->notajul;
              $totalnotakota=$totalnotakota+$row->notajul;
#              $vtotalnotakota=$vtotalnotakota+($row->notajul*$row->harga);
              $totareajul=$totareajul+$row->notajul;
              $totarea=$totarea+$row->notajul;
#              $vtotarea=$vtotarea+($row->notajul*$row->harga);
              break;
            case '8' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->notaaug, Cell_DataType::TYPE_NUMERIC);
              $totalaug=$totalaug+$row->notaaug;
              $notablnaug = $row->notaaug;
              $subtotaug=$subtotaug+$row->notaaug;
#              $vsubtotaug=$vsubtotaug+($row->notaaug*$row->harga);
              $grandtotaug=$grandtotaug+$row->notaaug;
              $totalnotakota=$totalnotakota+$row->notaaug;
#              $vtotalnotakota=$vtotalnotakota+($row->notaaug*$row->harga);
              $totareaaug=$totareaaug+$row->notaaug;
              $totarea=$totarea+$row->notaaug;
#              $vtotarea=$vtotarea+($row->notaaug*$row->harga);
              break;
            case '9' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $row->notasep, Cell_DataType::TYPE_NUMERIC);
              $totalsep=$totalsep+$row->notasep;
              $notablnsep = $row->notasep;
              $subtotsep=$subtotsep+$row->notasep;
#              $vsubtotsep=$vsubtotsep+($row->notasep*$row->harga);
              $grandtotsep=$grandtotsep+$row->notasep;
              $totalnotakota=$totalnotakota+$row->notasep;
#              $vtotalnotakota=$vtotalnotakota+($row->notasep*$row->harga);
              $totareasep=$totareasep+$row->notasep;
              $totarea=$totarea+$row->notasep;
#              $vtotarea=$vtotarea+($row->notasep*$row->harga);
              break;
            case '10' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $row->notaokt, Cell_DataType::TYPE_NUMERIC);
              $totalokt=$totalokt+$row->notaokt;
              $notablnokt = $row->notaokt;
              $subtotokt=$subtotokt+$row->notaokt;
#              $vsubtotokt=$vsubtotokt+($row->notaokt*$row->harga);
              $grandtotokt=$grandtotokt+$row->notaokt;
              $totalnotakota=$totalnotakota+$row->notaokt;
#              $vtotalnotakota=$vtotalnotakota+($row->notaokt*$row->harga);
              $totareaokt=$totareaokt+$row->notaokt;
              $totarea=$totarea+$row->notaokt;
#              $vtotarea=$vtotarea+($row->notaokt*$row->harga);
              break;
            case '11' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $row->notanov, Cell_DataType::TYPE_NUMERIC);
              $totalnov=$totalnov+$row->notanov;
              $notablnnov = $row->notanov;
              $subtotnov=$subtotnov+$row->notanov;
#              $vsubtotnov=$vsubtotnov+($row->notanov*$row->harga);
              $grandtotnov=$grandtotnov+$row->notanov;
              $totalnotakota=$totalnotakota+$row->notanov;
              $totalnotakota=$totalnotakota+$row->notanov;
#              $vtotalnotakota=$vtotalnotakota+($row->notanov*$row->harga);
              $totareanov=$totareanov+$row->notanov;
              $totarea=$totarea+$row->notanov;
#              $vtotarea=$vtotarea+($row->notanov*$row->harga);
              break;
            case '12' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('V'.$i, $row->notades, Cell_DataType::TYPE_NUMERIC);
              $totaldes=$totaldes+$row->notades;
              $notablndes = $row->notades;
              $subtotdes=$subtotdes+$row->notades;
#              $vsubtotdes=$vsubtotdes+($row->notades*$row->harga);
              $grandtotdes=$grandtotdes+$row->notades;
              $totalnotakota=$totalnotakota+$row->notades;
#              $vtotalnotakota=$vtotalnotakota+($row->notades*$row->harga);
              $totareades=$totareades+$row->notades;
              $totarea=$totarea+$row->notades;
#              $vtotarea=$vtotarea+($row->notades*$row->harga);
              break;
            }
            $bl++;
            if($bl==13)$bl=1;
          }
            $totalnota = $totaljan+$totalfeb+$totalmar+$totalapr+$totalmay+$totaljun+$totaljul+$totalaug+$totalsep+$totalokt+$totalnov+$totaldes;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('W'.$i, $totalnota, Cell_DataType::TYPE_NUMERIC);
#            $objPHPExcel->getActiveSheet()->setCellValueExplicit('X'.$i, $totalnota*$row->harga, Cell_DataType::TYPE_NUMERIC);
        }elseif( $kode!=substr($row->kode,0,2) || ($icity!='' && $icity!=$row->icity)){
				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A'.$i.':W'.$i
				  );
          $totalnota  = $totaljan+$totalfeb+$totalmar+$totalapr+$totalmay+$totaljun+$totaljul+$totalaug+$totalsep+$totalokt+$totalnov+$totaldes;
			    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'TOTAL KOTA');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,9,$i);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $subtotjan, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $subtotfeb, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $subtotmar, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $subtotapr, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $subtotmay, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $subtotjun, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $subtotjul, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $subtotaug, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $subtotsep, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $subtotokt, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $subtotnov, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('V'.$i, $subtotdes, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('W'.$i, $totalnotakota, Cell_DataType::TYPE_NUMERIC);
#          $objPHPExcel->getActiveSheet()->setCellValueExplicit('X'.$i, $vtotalnotakota, Cell_DataType::TYPE_NUMERIC);
          $grandtotalnotakota=$grandtotalnotakota+$totalnotakota;
#          $vgrandtotalnotakota=$vgrandtotalnotakota+$vtotalnotakota;
#        $vtotalnotakota=0;
          $totalnotakota=0;
          $total=0;
          $subtotjan=0;
          $subtotfeb=0;
          $subtotmar=0;
          $subtotapr=0;
          $subtotmay=0;
          $subtotjun=0;
          $subtotjul=0;
          $subtotaug=0;
          $subtotsep=0;
          $subtotokt=0;
          $subtotnov=0;
          $subtotdes=0;
#          $vsubtotjan=0;
#          $vsubtotfeb=0;
#          $vsubtotmar=0;
#          $vsubtotapr=0;
#          $vsubtotmay=0;
#          $vsubtotjun=0;
#          $vsubtotjul=0;
#          $vsubtotaug=0;
#          $vsubtotsep=0;
#          $vsubtotokt=0;
#          $vsubtotnov=0;
#          $vsubtotdes=0;
          $i++;
          if( $kode!=substr($row->kode,0,2) ){
				      $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				      array(
					      'font' => array(
						      'name'	=> 'Arial',
						      'bold'  => true,
						      'italic'=> false,
						      'size'  => 10
					      ),
					      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => true
					      )
				      ),
				      'A'.$i.':M'.$i
				      );
			        $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'TOTAL AREA');
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,9,$i);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $totareajan, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $totareafeb, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $totareamar, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $totareaapr, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $totareamay, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $totareajun, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $totareajul, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $totareaaug, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $totareasep, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $totareaokt, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $totareanov, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('V'.$i, $totareades, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('W'.$i, $totarea, Cell_DataType::TYPE_NUMERIC);
#              $objPHPExcel->getActiveSheet()->setCellValueExplicit('X'.$i, $vtotarea, Cell_DataType::TYPE_NUMERIC);
#              $vtotarea=0;
              $totarea=0;
              $grandtotalnotakota=$grandtotalnotakota+$totalnotakota;
#              $vgrandtotalnotakota=$vgrandtotalnotakota+$vtotalnotakota;
              $i++;
            }
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			    array(
				    'font' => array(
					    'name'	=> 'Arial',
					    'bold'  => false,
					    'italic'=> false,
					    'size'  => 10
				    )
			    ),
			    'A'.$i.':W'.$i
			    );

          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, substr($row->kode,0,2)."-".$row->area, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->kode, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->kota, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->sales, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->jenis, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->nama, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->alamat, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->product, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->productname, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->supplier, Cell_DataType::TYPE_STRING);
        $bl=$blasal;
        for($n=1;$n<=$interval;$n++){
          switch ($bl){
            case '1' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->notajan, Cell_DataType::TYPE_NUMERIC);
              $notajan = $row->notajan;
              $totaljan=$totaljan+$row->notajan;
              $subtotjan=$subtotjan+$row->notajan;
#              $vsubtotjan=$vsubtotjan+($row->notajan*$row->harga);
              $grandtotjan=$grandtotjan+$row->notajan;
              $totalnotakota=$totalnotakota+$row->notajan;
#              $vtotalnotakota=$vtotalnotakota+($row->notajan*$row->harga);
              $totareajan=$totareajan+$row->notajan;
              $totarea=$totarea+$row->notajan;
#              $vtotarea=$vtotarea+($row->notajan*$row->harga);
              break;
            case '2' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->notafeb, Cell_DataType::TYPE_NUMERIC);
              $totalfeb=$totalfeb+$row->notafeb;
              $notablnfeb= $row->notafeb;
              $subtotfeb=$subtotfeb+$row->notafeb;
#              $vsubtotfeb=$vsubtotfeb+($row->notafeb*$row->harga);
              $grandtotfeb=$grandtotfeb+$row->notafeb;
              $totalnotakota=$totalnotakota+$row->notafeb;
#              $vtotalnotakota=$vtotalnotakota+($row->notafeb*$row->harga);
              $totareafeb=$totareafeb+$row->notafeb;
              $totarea=$totarea+$row->notafeb;
#              $vtotarea=$vtotarea+($row->notafeb*$row->harga);
              break;
            case '3' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->notamar, Cell_DataType::TYPE_NUMERIC);
              $notablnmar = $row->notamar;
              $totalmar=$totalmar+$row->notamar;
              $subtotmar=$subtotmar+$row->notamar;
#              $vsubtotmar=$vsubtotmar+($row->notamar*$row->harga);
              $grandtotmar=$grandtotmar+$row->notamar;
              $totalnotakota=$totalnotakota+$row->notamar;
#              $vtotalnotakota=$vtotalnotakota+($row->notamar*$row->harga);
              $totareamar=$totareamar+$row->notamar;
              $totarea=$totarea+$row->notamar;
#              $vtotarea=$vtotarea+($row->notamar*$row->harga);
              break;
            case '4' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->notaapr, Cell_DataType::TYPE_NUMERIC);
              $totalapr=$totalapr+$row->notaapr;
              $notablnapr = $row->notaapr;
              $subtotapr=$subtotapr+$row->notaapr;
#              $vsubtotapr=$vsubtotapr+($row->notaapr*$row->harga);
              $grandtotapr=$grandtotapr+$row->notaapr;
              $totalnotakota=$totalnotakota+$row->notaapr;
#              $vtotalnotakota=$vtotalnotakota+($row->notaapr*$row->harga);
              $totareaapr=$totareaapr+$row->notaapr;
              $totarea=$totarea+$row->notaapr;
#              $vtotarea=$vtotarea+($row->notaapr*$row->harga);
              break;
            case '5' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $row->notamay, Cell_DataType::TYPE_NUMERIC);
              $notablnmay = $row->notamay;
              $totalmay=$totalmay+$row->notamay;
              $subtotmay=$subtotmay+$row->notamay;
#              $vsubtotmay=$vsubtotmay+($row->notamay*$row->harga);
              $grandtotmay=$grandtotmay+$row->notamay;
              $totalnotakota=$totalnotakota+$row->notamay;
#              $vtotalnotakota=$vtotalnotakota+($row->notamay*$row->harga);
              $totareamay=$totareamay+$row->notamay;
              $totarea=$totarea+$row->notamay;
#              $vtotarea=$vtotarea+($row->notamay*$row->harga);
              break;
            case '6' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $row->notajun, Cell_DataType::TYPE_NUMERIC);
              $notablnjun = $row->notajun;
              $totaljun=$totaljun+$row->notajun;
              $subtotjun=$subtotjun+$row->notajun;
#              $vsubtotjun=$vsubtotjun+($row->notajun*$row->harga);
              $grandtotjun=$grandtotjun+$row->notajun;
              $totalnotakota=$totalnotakota+$row->notajun;
#              $vtotalnotakota=$vtotalnotakota+($row->notajun*$row->harga);
              $totareajun=$totareajun+$row->notajun;
              $totarea=$totarea+$row->notajun;
#              $vtotarea=$vtotarea+($row->notajun*$row->harga);
              break;
            case '7' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $row->notajul, Cell_DataType::TYPE_NUMERIC);
              $totaljul=$totaljul+$row->notajul;
              $notablnjul = $row->notajul;
              $subtotjul=$subtotjul+$row->notajul;
#              $vsubtotjul=$vsubtotjul+($row->notajul*$row->harga);
              $grandtotjul=$grandtotjul+$row->notajul;
              $totalnotakota=$totalnotakota+$row->notajul;
#              $vtotalnotakota=$vtotalnotakota+($row->notajul*$row->harga);
              $totareajul=$totareajul+$row->notajul;
              $totarea=$totarea+$row->notajul;
#              $vtotarea=$vtotarea+($row->notajul*$row->harga);
              break;
            case '8' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->notaaug, Cell_DataType::TYPE_NUMERIC);
              $totalaug=$totalaug+$row->notaaug;
              $notablnaug = $row->notaaug;
              $subtotaug=$subtotaug+$row->notaaug;
#              $vsubtotaug=$vsubtotaug+($row->notaaug*$row->harga);
              $grandtotaug=$grandtotaug+$row->notaaug;
              $totalnotakota=$totalnotakota+$row->notaaug;
#              $vtotalnotakota=$vtotalnotakota+($row->notaaug*$row->harga);
              $totareaaug=$totareaaug+$row->notaaug;
              $totarea=$totarea+$row->notaaug;
#              $vtotarea=$vtotarea+($row->notaaug*$row->harga);
              break;
            case '9' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $row->notasep, Cell_DataType::TYPE_NUMERIC);
              $totalsep=$totalsep+$row->notasep;
              $notablnsep = $row->notasep;
              $subtotsep=$subtotsep+$row->notasep;
#              $vsubtotsep=$vsubtotsep+($row->notasep*$row->harga);
              $grandtotsep=$grandtotsep+$row->notasep;
              $totalnotakota=$totalnotakota+$row->notasep;
#              $vtotalnotakota=$vtotalnotakota+($row->notasep*$row->harga);
              $totareasep=$totareasep+$row->notasep;
              $totarea=$totarea+$row->notasep;
#              $vtotarea=$vtotarea+($row->notasep*$row->harga);
              break;
            case '10' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $row->notaokt, Cell_DataType::TYPE_NUMERIC);
              $totalokt=$totalokt+$row->notaokt;
              $notablnokt = $row->notaokt;
              $subtotokt=$subtotokt+$row->notaokt;
#              $vsubtotokt=$vsubtotokt+($row->notaokt*$row->harga);
              $grandtotokt=$grandtotokt+$row->notaokt;
              $totalnotakota=$totalnotakota+$row->notaokt;
#              $vtotalnotakota=$vtotalnotakota+($row->notaokt*$row->harga);
              $totareaokt=$totareaokt+$row->notaokt;
              $totarea=$totarea+$row->notaokt;
#              $vtotarea=$vtotarea+($row->notaokt*$row->harga);
              break;
            case '11' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $row->notanov, Cell_DataType::TYPE_NUMERIC);
              $totalnov=$totalnov+$row->notanov;
              $notablnnov = $row->notanov;
              $subtotnov=$subtotnov+$row->notanov;
#              $vsubtotnov=$vsubtotnov+($row->notanov*$row->harga);
              $grandtotnov=$grandtotnov+$row->notanov;
              $totalnotakota=$totalnotakota+$row->notanov;
              $totalnotakota=$totalnotakota+$row->notanov;
#              $vtotalnotakota=$vtotalnotakota+($row->notanov*$row->harga);
              $totareanov=$totareanov+$row->notanov;
              $totarea=$totarea+$row->notanov;
#              $vtotarea=$vtotarea+($row->notanov*$row->harga);
              break;
            case '12' :
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('V'.$i, $row->notades, Cell_DataType::TYPE_NUMERIC);
              $totaldes=$totaldes+$row->notades;
              $notablndes = $row->notades;
              $subtotdes=$subtotdes+$row->notades;
#              $vsubtotdes=$vsubtotdes+($row->notades*$row->harga);
              $grandtotdes=$grandtotdes+$row->notades;
              $totalnotakota=$totalnotakota+$row->notades;
#              $vtotalnotakota=$vtotalnotakota+($row->notades*$row->harga);
              $totareades=$totareades+$row->notades;
              $totarea=$totarea+$row->notades;
#              $vtotarea=$vtotarea+($row->notades*$row->harga);
              break;
            }
            $bl++;
            if($bl==13)$bl=1;
          }
            $totalnota  = $totaljan+$totalfeb+$totalmar+$totalapr+$totalmay+$totaljun+$totaljul+$totalaug+$totalsep+$totalokt+$totalnov+$totaldes;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('W'.$i, $totalnota, Cell_DataType::TYPE_NUMERIC);
#            $objPHPExcel->getActiveSheet()->setCellValueExplicit('X'.$i, $totalnota*$row->harga, Cell_DataType::TYPE_NUMERIC);
        }
        $i++;
        $product=$row->product;
        $icity=$row->icity;
        $kode=substr($row->kode,0,2);
        }
        $x=$i-1;
				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A'.$i.':M'.$i
				  );

		        $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'TOTAL KOTA');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,9,$i);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $subtotjan, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $subtotfeb, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $subtotmar, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $subtotapr, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $subtotmay, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $subtotjun, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $subtotjul, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $subtotaug, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $subtotsep, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $subtotokt, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $subtotnov, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('V'.$i, $subtotdes, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('W'.$i, $totalnotakota, Cell_DataType::TYPE_NUMERIC);
#            $objPHPExcel->getActiveSheet()->setCellValueExplicit('X'.$i, $vtotalnotakota, Cell_DataType::TYPE_NUMERIC);
            $grandtotalnotakota=$grandtotalnotakota+$totalnotakota;
#            $vgrandtotalnotakota=$vgrandtotalnotakota+$vtotalnotakota;
#            $vtotalnotakota=0;
            $totalnotakota=0;
            $total=0;
            $subtotjan=0;
            $subtotfeb=0;
            $subtotmar=0;
            $subtotapr=0;
            $subtotmay=0;
            $subtotjun=0;
            $subtotjul=0;
            $subtotaug=0;
            $subtotsep=0;
            $subtotokt=0;
            $subtotnov=0;
            $subtotdes=0;
#            $vsubtotjan=0;
#            $vsubtotfeb=0;
#            $vsubtotmar=0;
#            $vsubtotapr=0;
#            $vsubtotmay=0;
#            $vsubtotjun=0;
#            $vsubtotjul=0;
#            $vsubtotaug=0;
#            $vsubtotsep=0;
#            $vsubtotokt=0;
#            $vsubtotnov=0;
#            $vsubtotdes=0;
            $i++;
				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A'.$i.':M'.$i
				  );
          $grandtotalnotakota=$grandtotalnotakota+$totalnotakota;
#          $vgrandtotalnotakota=$vgrandtotalnotakota+$vtotalnotakota;
			    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'TOTAL AREA');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,9,$i);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $totareajan, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $totareafeb, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $totareamar, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $totareaapr, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $totareamay, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $totareajun, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $totareajul, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $totareaaug, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $totareasep, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $totareaokt, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $totareanov, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('V'.$i, $totareades, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('W'.$i, $totarea, Cell_DataType::TYPE_NUMERIC);
#          $objPHPExcel->getActiveSheet()->setCellValueExplicit('X'.$i, $vtotarea, Cell_DataType::TYPE_NUMERIC);
#          $vtotarea=0;
          $totarea=0;
          $grandtotalnotakota=$grandtotalnotakota+$totalnotakota;
#          $vgrandtotalnotakota=$vgrandtotalnotakota+$vtotalnotakota;
          $i++;
				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A'.$i.':M'.$i
				  );
			    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'GRAND TOTAL');
          $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,9,$i);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $grandtotjan, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $grandtotfeb, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $grandtotmar, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $grandtotapr, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $grandtotmay, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $grandtotjun, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $grandtotjul, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $grandtotaug, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $grandtotsep, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $grandtotokt, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $grandtotnov, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('V'.$i, $grandtotdes, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('W'.$i, $grandtotalnotakota, Cell_DataType::TYPE_NUMERIC);
#          $objPHPExcel->getActiveSheet()->setCellValueExplicit('X'.$i, $vgrandtotalnotakota, Cell_DataType::TYPE_NUMERIC);
      }
      
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='Customer Card '.$iarea.'.xls';
      if(file_exists('customer/'.$nama)){
        @chmod('customer/'.$nama, 0777);
        @unlink('customer/'.$nama);
      }
      if($iarea=='NA'){
        $objWriter->save('excel/'.'00'.'/'.$nama);
      }elseif($iarea!='NA'){
        $objWriter->save('excel/'.$istore.'/'.$nama);
      }

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
		  $data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Customer Card '.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$data['sukses'] = true;
			$data['inomor']	= "Customer Card";
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
  } 
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listcustomercard');
			$this->load->view('listcustomercardpersupprp/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dto		= $this->uri->segment(8);
			$dfrom	= $this->uri->segment(7);
			$iarea	= $this->uri->segment(6);
			$ispb		= $this->uri->segment(5);
			$inota	= $this->uri->segment(4);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$this->load->model('listcustomercardpersupprp/mmaster');
			$this->mmaster->delete($inota,$ispb,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus Penjualan Per Pelanggan dari tanggal:'.$dfrom.' sampai:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$config['base_url'] = base_url().'index.php/listcustomercardpersupprp/cform/view/'.$inota.'/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
																	where a.i_customer=b.i_customer 
																	and a.f_ttb_tolak='f'
                                  and a.f_nota_koreksi='f'
																	and not a.i_nota isnull
																	and (upper(a.i_nota) like '%$cari%' 
																		or upper(a.i_spb) like '%$cari%' 
																		or upper(a.i_customer) like '%$cari%' 
																		or upper(b.e_customer_name) like '%$cari%')
																	and a.i_area='$iarea' and
																	a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['keyword']	= '';	
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listcustomercardpersupprp/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($cari=='') $cari	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/listcustomercardpersupprp/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$cari%' 
										  or upper(a.i_spb) like '%$cari%' 
										  or upper(a.i_customer) like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('listcustomercardpersupprp/mmaster');
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listcustomercardpersupprp/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$keyword = strtoupper($this->uri->segment(7));	
			$config['base_url'] = base_url().'index.php/listcustomercardpersupprp/cform/cariperpages/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$keyword.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and a.f_ttb_tolak='f'
                    and a.f_nota_koreksi='f'                    
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$keyword%' 
										  or upper(a.i_spb) like '%$keyword%' 
										  or upper(a.i_customer) like '%$keyword%' 
										  or upper(b.e_customer_name) like '%$keyword%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('listcustomercardpersupprp/mmaster');
			$data['page_title'] = $this->lang->line('listcustomercard');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$keyword);
			$this->load->view('listcustomercardpersupprp/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomercardpersupprp/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
        else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomercardpersupprp/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listcustomercardpersupprp/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomercardpersupprp/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomercardpersupprp/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listcustomercardpersupprp/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listcustomercardpersupprp/cform/supplier/index/';
			$query = $this->db->query("select * from tr_supplier ",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10'; 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomercardpersupprp/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomercardpersupprp/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu422')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listcustomercardpersupprp/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%' 
						      	  or upper(e_supplier_name) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listcustomercardpersupprp/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomercardpersupprp/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
