<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu446')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('rekaporderkons');
			$data['dfrom']='';
			$data['dto']='';
			$data['icustomer']='';
			$this->load->view('rekaporderkons/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu446')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('rekaporderkons');
			$this->load->view('rekaporderkons/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu446')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/rekaporderkons/cform/customer/'.$baris.'/sikasep/';;
      $area1	= $this->session->userdata('i_area');
      $area2	= $this->session->userdata('i_area2');
      $area3	= $this->session->userdata('i_area3');
      $area4	= $this->session->userdata('i_area4');
      $area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
			  $query = $this->db->query(" select a.*, b.e_customer_name from tr_spg a, tr_customer b
                    where a.i_customer=b.i_customer order by a.i_customer",false);
      }else{
        $query = $this->db->query(" select a.*, b.e_customer_name from tr_spg a, tr_customer b
                    where a.i_customer=b.i_customer and (b.i_area='$area1' or b.i_area='$area2' or
                    b.i_area='$area3' or b.i_area='$area4' or b.i_area='$area5') 
                    order by a.i_customer",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
         	$data['cari']='';
			$this->load->model('rekaporderkons/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(6));
			$this->load->view('rekaporderkons/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu446')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/rekaporderkons/cform/customer/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));*/

			$baris   = $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
				$cari   = ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
				$config['base_url'] = base_url().'index.php/rekaporderkons/cform/caricustomer/'.$baris.'/'.$cari.'/';
			else
				$config['base_url'] = base_url().'index.php/rekaporderkons/cform/caricustomer/'.$baris.'/sikasep/';

			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00'){
				  $query 	= $this->db->query("select a.*, b.e_customer_name from tr_spg a, tr_customer b
			                    where a.i_customer=b.i_customer and (upper(a.i_customer) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%'
			                    or upper(a.i_spg) ilike '%$cari%' or upper(a.e_spg_name) ilike '%$cari%')
							                order by a.i_customer",false);
			}else{
				  $query 	= $this->db->query("select a.*, b.e_customer_name from tr_spg a, tr_customer b
			                    where a.i_customer=b.i_customer and (upper(a.i_customer) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%'
			                    or upper(a.i_spg) ilike '%$cari%' or upper(a.e_spg_name) ilike '%$cari%') and (b.i_area='$area1' or b.i_area='$area2' or
			            b.i_area='$area3' or b.i_area='$area4' or b.i_area='$area5') 
							                order by a.i_customer",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('rekaporderkons/mmaster');
			$data['baris']=$baris;
         	$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('rekaporderkons/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu446')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$icustomer	= $this->input->post('icustomer');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($icustomer=='') $icustomer=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/rekaporderkons/cform/view2/'.$dfrom.'/'.$dto.'/'.$icustomer.'/index/';
			$this->load->model('rekaporderkons/mmaster');
			$data['page_title'] = $this->lang->line('rekaporderkons');
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['icustomer'] = $icustomer;
			$data['isi']	= $this->mmaster->bacaperiode($icustomer,$dfrom,$dto);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Order Konsinyasi untuk Rekap SPMB Customer '.$icustomer.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('rekaporderkons/vmainform',$data);			

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function rekap()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu446')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml  = $this->input->post('jml', TRUE);
			$dfrom= $this->input->post('dfrom', TRUE);
			$dto  = $this->input->post('dto', TRUE);
			$this->db->trans_begin();
      $dspmb=date('Y-m-d');
      $tmp=explode("-",$dspmb);
      $th=$tmp[0];
      $bl=$tmp[1];
      $dt=$tmp[2];
      $thbl=substr($th,2,2).$bl;
      $ispmb  = '';
      $ispmbx = '';
			$this->load->model('rekaporderkons/mmaster');
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$iorderpb     = $this->input->post('iorderpb'.$i, TRUE);
					$iarea        = $this->input->post('iarea'.$i, TRUE);
					$icustomer    = $this->input->post('icustomer'.$i, TRUE);
          if($ispmb=='')$ispmb=$this->mmaster->runningnumberspmb($thbl);
          if($ispmbx==''){
            $this->mmaster->insertheader($ispmb, $dspmb, $iarea, null);
          }
					$this->mmaster->updateorderpb($iorderpb,$icustomer,$iarea,$ispmb);
          $this->mmaster->insertdetail($iorderpb,$icustomer,$iarea,$ispmb);
          $ispmbx=$ispmb;
				}
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
#				$this->db->trans_rollback();
				$this->db->trans_commit();
  
        $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		=  $this->db->query($sql);
		    if($rs->num_rows>0){
			    foreach($rs->result() as $tes){
				    $ip_address	  = $tes->ip_address;
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }

		    $data['user']	= $this->session->userdata('user_id');
		    $data['host']	= $ip_address;
		    $data['uri']	= $this->session->userdata('printeruri');
		    $query 	= pg_query("SELECT current_timestamp as c");
		    while($row=pg_fetch_assoc($query)){
			    $now	  = $row['c'];
		    }
		    $pesan='Input SPMB Konsinyasi (rekap-order) Area :'.$iarea.' No:'.$ispmb;
		    $this->load->model('logger');
		    $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $ispmb;
				$this->load->view('nomor',$data);

			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
