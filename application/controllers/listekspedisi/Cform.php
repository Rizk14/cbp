<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu257')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');

			$iekspedisi			= $this->input->post('iekspedisi', TRUE);
			$iarea				= $this->input->post('iarea', TRUE);
			$eekspedisi			= $this->input->post('eekspedisi', TRUE);
			$eekspedisiaddress		= $this->input->post('eekspedisiaddress', TRUE);

			  $cari=strtoupper($this->input->post("cari",false));
			  $config['base_url'] = base_url().'index.php/listekspedisi/cform/index/';
        if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		      $query=$this->db->query("select i_ekspedisi from tr_ekspedisi where (upper(i_ekspedisi) like '%$cari%' or upper(e_ekspedisi) like '%$cari%')", false);
        }else{
          $query=$this->db->query("select i_ekspedisi from tr_ekspedisi where (upper(i_ekspedisi) like '%$cari%' or upper(e_ekspedisi) like '%$cari%')
                                   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')", false);
        }
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
        $this->pagination->initialize($config);
				$data['page_title'] = $this->lang->line('master_ekspedisi');
				$data['iekspedisi']='';
				$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);
				$this->load->model('listekspedisi/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4),$area1,$area2,$area3,$area4,$area5);

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Membuka Data Ekspedisi No:'.$iekspedisi.' Area:'.$iarea;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

				$this->load->view('listekspedisi/vmainform', $data);
			
		}else{
			$this->load->view('awal/index.php');
		}
	}
	/*function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu257')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$area 	= $this->input->post('iareaawal', FALSE);
      if($area=='') $area=$this->uri->segment(4);
      if($area=='') $area=$this->input->post('iareacari', FALSE);
			$config['base_url'] = base_url().'index.php/listekspedisi/cform/view/'.$area.'/';
			$query	= $this->db->query(" select * from tr_ekspedisi 
                                   where i_area='$area' 
                                   order by i_ekspedisi ",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page']		= $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('master_ekspedisi');
			$data['iekspedisi']	= '';
      $data['area']=$area;
			$this->load->model('listekspedisi/mmaster');
			$data['isi']	= $this->mmaster->bacasemua($area,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listekspedisi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	} */
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu257')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iekspedisi 	 	= $this->input->post('iekspedisi');
			$eekspedisiname = $this->input->post('eekspedisiname');
			$iarea 		 			= $this->input->post('iarea');
			$eekspedisiaddress	= $this->input->post('eekspedisiaddress');
			$eekspedisicity 	 	= $this->input->post('eekspedisicity');
			$eekspedisifax 	= $this->input->post('eekspedisifax');
			$eekspedisiphone= $this->input->post('eekspedisiphone');
			if (
          ($iekspedisi != '') && ($eekspedisiname != '') && ($iarea != '')
         )
			{
				$this->load->model('exp/mmaster');
				$this->mmaster->insert($iekspedisi,$eekspedisiname,$iarea,$eekspedisiaddress,$eekspedisicity,$eekspedisifax,$eekspedisiphone);

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Input Ekspedisi No:'.$iekspedisi.' Area:'.$iarea;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			}else{
        echo "errrooooooorrrrr";
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu257')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_ekspedisi');
			$this->load->view('listekspedisi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu257')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_ekspedisi')." update";
			if($this->uri->segment(4)){
				$iekspedisi = $this->uri->segment(4);
				$iarea = $this->uri->segment(5);
				$data['iekspedisi'] = $iekspedisi;
				$data['iarea'] 		= $iarea;
				$this->load->model('listekspedisi/mmaster');
				$data['isi']=$this->mmaster->baca($iekspedisi,$iarea);
				$qarea	= $this->mmaster->bacaekspedisiarea($iekspedisi, $iarea);
				if($qarea->num_rows()>0){
					$row_eksarea	= $qarea->row();
					$data['areanameeks']	= $row_eksarea->e_area_name;
					$data['iareaeks']		= $row_eksarea->i_area;
				}else{
					$data['areanameeks']		= "";
					$data['iareaeks']		= "";
				}
		 		$this->load->view('listekspedisi/vmainform',$data);
			}else{
				$this->load->view('listekspedisi/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu257')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iekspedisi 	 	= $this->input->post('iekspedisi');
			$eekspedisiname 	= $this->input->post('eekspedisiname');
			$iarea 		 	= $this->input->post('iarea');
			$eekspedisiaddress	= $this->input->post('eekspedisiaddress');
			$eekspedisicity 	= $this->input->post('eekspedisicity');
			$eekspedisifax 		= $this->input->post('eekspedisifax');
			$eekspedisiphone	= $this->input->post('eekspedisiphone');
			$this->load->model('listekspedisi/mmaster');
			$this->mmaster->update($iekspedisi,$eekspedisiname,$iarea,$eekspedisiaddress,$eekspedisicity,$eekspedisifax,$eekspedisiphone);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Update Data Ekspedisi No:'.$iekspedisi.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu257')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iekspedisi = $this->uri->segment(4);
			$iarea = $this->uri->segment(4);
			$this->load->model('listekspedisi/mmaster');
			$this->mmaster->delete($iekspedisi,$iarea);
			
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus Ekspedisi No:'.$iekspedisi.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$config['base_url'] = base_url().'index.php/listekspedisi/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select a.* from tr_ekspedisi a where (upper(a.i_ekspedisi) like '%$cari%' or 
													upper(a.e_ekspedisi) like '%$cari%' or 
													upper(a.e_ekspedisi_city) like '%$cari%' or 
													upper(a.e_ekspedisi_phone) like '%$cari%') order by a.i_ekspedisi ",false);
			$config['total_rows']	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link']	= 'Awal';
			$config['last_link']	= 'Akhir';
			$config['next_link']	= 'Selanjutnya';
			$config['prev_link']	= 'Sebelumnya';
			$config['cur_page']		= $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('listekspedisi/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_ekspedisi');
			$data['iekspedisi']	= '';
	 		$this->load->view('listekspedisi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu257')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listekspedisi/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		    $query=$this->db->query("select * from tr_ekspedisi 
                                 where (upper(i_ekspedisi) like '%$cari%' or upper(e_ekspedisi) like '%$cari%')", false);
      }else{
        $query=$this->db->query("select * from tr_ekspedisi 
                                 where (upper(i_ekspedisi) like '%$cari%' or upper(e_ekspedisi) like '%$cari%') 
                                 and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                                 or i_area = '$area4' or i_area = '$area5')", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('listekspedisi/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$data['page_title'] = $this->lang->line('master_ekspedisi');
			$data['iekspedisi']	= '';
	 		$this->load->view('listekspedisi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu257')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listekspedisi/cform/area/index/';
			$allarea= $this->session->userdata('allarea');	
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($allarea=='t'){
				$query = $this->db->query(" select * from tr_area order by i_area", false);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$query = $this->db->query(" select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false);
			}else{
				$query = $this->db->query(" select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false);
			}
		
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listekspedisi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listekspedisi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu257')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$allarea= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listekspedisi/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($allarea=='t'){
				$query = $this->db->query(" select * from tr_area order by i_area", false);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$query = $this->db->query(" select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false);
			}else{
				$query = $this->db->query(" select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false);			
			}
			
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('listekspedisi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(4),$allarea,$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listekspedisi/vlistarea', $data);
		}else{
			$this->load->view('listekspedisi/index.php');
		}
	}
	function areaawal()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu257')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listekspedisi/cform/areaawal/index/';
			$allarea= $this->session->userdata('allarea');	
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($allarea=='t'){
				$query = $this->db->query(" select * from tr_area order by i_area", false);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$query = $this->db->query(" select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') order by i_area", false);
			}else{
				$query = $this->db->query(" select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' order by i_area", false);
			}
		
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listekspedisi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listekspedisi/vlistareaawal', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariareaawal()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu257')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$allarea= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listekspedisi/cform/areaawal/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($allarea=='t'){
				$query = $this->db->query(" select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') order by i_area", false);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$query = $this->db->query(" select * from tr_area  where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') order by i_area", false);
			}else{
				$query = $this->db->query(" select * from tr_area where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5') and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') order by i_area", false);			
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listekspedisi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$area1,$area2,$area3,$area4,$area5);
			$this->load->view('exp/vlistareaawal', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
