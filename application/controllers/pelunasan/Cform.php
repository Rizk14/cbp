<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationxx');
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('pelunasan');
         $data['dfrom']='';
         $data['dto']  ='';
         $data['ipl']  ='';
         $data['idt']  ='';
         $data['iarea']='';
         $data['isi']  ='';

         $this->load->view('pelunasan/vmainform', $data);

      }elseif($this->session->userdata('logged_in')){
         $this->load->view('errorauthority');
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('listspb');
         $this->load->view('pelunasan/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function delete()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb = $this->input->post('ispbdelete', TRUE);
         $this->load->model('pelunasan/mmaster');
         $this->mmaster->delete($ispb);
         $data['page_title'] = $this->lang->line('listspb');
         $data['ispb']='';
         $data['isi']=$this->mmaster->bacasemua();
         $this->load->view('pelunasan/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cari()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $config['base_url'] = base_url().'index.php/pelunasan/cform/index/';
         $config['per_page'] = '10';
         $limo=$config['per_page'];
         $ofso=$this->uri->segment(4);
         if($ofso=='')
            $ofso=0;
         $query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
                              where a.i_customer=b.i_customer
                              and a.f_lunas = 'f'
                              and (upper(a.i_customer) like '%$cari%'
                                or upper(b.e_customer_name) like '%$cari%'
                                or upper(a.i_spb) like '%$cari%')",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(4);
         $this->pagination->initialize($config);
         $this->load->model('pelunasan/mmaster');
         $data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
         $data['page_title'] = $this->lang->line('pelunasan');
         $data['ispb']='';
         $data['ittb']='';
         $data['inota']='';
         $this->load->view('pelunasan/vmainform',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function approve()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('pelunasan');
         if(
            ($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
           )
         {
            $idt        = str_replace('tandagaring','/',$this->uri->segment(4));
            $iarea      = $this->uri->segment(5);
            $eareaname= $this->uri->segment(6);
            $eareaname= str_replace('%20', ' ',$eareaname);
            $vsisa      = $this->uri->segment(7);
            $dfrom      = $this->uri->segment(8);
            $dto     = $this->uri->segment(9);
            $ddt     = $this->uri->segment(10);
//          $query = $this->db->query("select * from tm_nota_item where i_nota = '$idt' and i_area = '$iarea'");
            $data['jmlitem'] = 0;//$query->num_rows();
            $data['ipl'] = '';
            $data['idt'] = $idt;
            $data['iarea']=$iarea;
            $data['eareaname']=$eareaname;
            $data['vsisa']=$vsisa;
            $data['dfrom']=$dfrom;
            $data['dto']=$dto;
            $data['ddt']=$ddt;
            $this->load->model('pelunasan/mmaster');
            $data['isi']='';//$this->mmaster->baca($idt,$iarea);
            $data['detail']='';//$this->mmaster->bacadetail($idt,$iarea);
            $this->load->view('pelunasan/vmainform',$data);
         }else{
            $this->load->view('pelunasan/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function update()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('pelunasan/mmaster');
         $idt  = $this->input->post('idt', TRUE);
         $ddt  = $this->input->post('ddt', TRUE);
         if($ddt!=''){
            $tmp=explode("-",$ddt);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $ddt=$th."-".$bl."-".$hr;
         }
      $dbukti  = $this->input->post('dbukti', TRUE);
         if($dbukti!=''){
            $tmp=explode("-",$dbukti);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dbukti=$th."-".$bl."-".$hr;
         }
         $djt  = $this->input->post('djt', TRUE);
         if($djt!=''){
            $tmp=explode("-",$djt);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $djt=$th."-".$bl."-".$hr;
         }
         $dcair   = $this->input->post('dcair', TRUE);
         if($dcair!=''){
            $tmp=explode("-",$dcair);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dcair=$th."-".$bl."-".$hr;
         }
         $dgiro   = $this->input->post('dgiro', TRUE);
         if($dgiro!=''){
            $tmp=explode("-",$dgiro);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dgiro=$th."-".$bl."-".$hr;
         }
         $icustomer     = $this->input->post('icustomer', TRUE);
         $ecustomername = $this->input->post('ecustomername', TRUE);
         $ecustomeraddress= $this->input->post('ecustomeraddress', TRUE);
         $ecustomercity = $this->input->post('ecustomercity', TRUE);
         $iarea         = $this->input->post('iarea', TRUE);
         $eareaname     = $this->input->post('eareaname', TRUE);
         $ijenisbayar   = $this->input->post('ijenisbayar',TRUE);
         $ejenisbayarname= $this->input->post('ejenisbayarname',TRUE);
         $egirobank     = $this->input->post('egirobank',TRUE);
         if(($egirobank==Null) && ($ijenisbayar!='05')){
            $egirobank  = '-';
            $igiro      = $this->input->post('igiro',TRUE);
         }else if(($egirobank==Null) && ($ijenisbayar=='05')){
            $egirobank  = $this->input->post('igiro',TRUE);
            $igiro      ='';
         }else if(($egirobank!=Null) && ($ijenisbayar=='01')){
            $igiro      = $this->input->post('igiro',TRUE);
         }else if(($egirobank!=Null) && ($ijenisbayar=='03')){
            $nkuyear = $this->input->post('nkuyear',TRUE);
            $igiro      = $this->input->post('igiro',TRUE);
         }else if($ijenisbayar=='04'){
            $igiro      = $this->input->post('igiro',TRUE);
         }else{
            $igiro      ='';
         }
         $vjumlah    = $this->input->post('vjumlah',TRUE);
         $vjumlah    = str_replace(',','',$vjumlah);
#      if($ijenisbayar=='04'||$ijenisbayar=='05'){
#           $vlebih        = '0';
#      }else{
         $vlebih     = $this->input->post('vlebih',TRUE);
#      }
         $vlebih        = str_replace(',','',$vlebih);
#      $ipelunasanremark = $this->input->post('ipelunsanremark',TRUE);
#      $eremark    = $this->input->post('eremark',TRUE);
         $jml          = $this->input->post('jml', TRUE);
         $ada=false;
         if(($ddt!='') && ($dbukti!='') && ($idt!='') && ($vjumlah!='') && ($vjumlah!='0') && ($jml!='0') && ($ijenisbayar!='') && ($icustomer!='') ){
        for($i=1;$i<=$jml;$i++){
          $vjumla = $this->input->post('vjumlah'.$i, TRUE);
             $vsisa  = $this->input->post('vsisa'.$i, TRUE);
             $vjumla = str_replace(',','',$vjumla);
             $vsisa  = str_replace(',','',$vsisa);


             $vsisa  = $vsisa-$vjumla;
          $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
          if( ($vsisa>0) && ($ipelunasanremark=='') ){
            $ada=true;
            break;
          }
        }
        if(!$ada){
          $this->db->trans_begin();
             $inotax = $this->input->post('inota1', TRUE);
              $ipl=$this->mmaster->runningnumberpl($idt,$icustomer,$inotax);
              switch($ijenisbayar){
              case '01':
                 $ipl=$ipl.'B';
                 break;
              case '02':
                 $ipl=$ipl.'A';
                 break;
              case '03':
                 $ipl=$ipl.'C';
                 break;
              case '04':
                 $ipl=$ipl.'D';
                 break;
              case '05':
                 $ipl=$ipl.'E';
                 break;
              case '06':
                 $ipl=$ipl.'F';
                 break;
              case '07':
                 $ipl=$ipl.'G';
                 break;
              case '08':
                 $ipl=$ipl.'H';
                 break;
              case '09':
                 $ipl=$ipl.'I';
                 break;
              case '10':
                 $ipl=$ipl.'J';
                 break;
              }
          $ipl=$this->mmaster->cekpl($iarea,$ipl,$idt);
              $this->mmaster->insertheader(  $ipl,$idt,$iarea,$ijenisbayar,$igiro,$icustomer,$dgiro,$ddt,$dbukti,$dcair,$egirobank,
                                    $vjumlah,$vlebih);
              $asal=0;
              $pengurang=$vjumlah-$vlebih;

          $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
            $group='xxx';
            foreach($query->result() as $row){
               $group=$row->i_customer_groupbayar;
            }

              if($ijenisbayar=='01'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updategiro($group,$iarea,$igiro,$pengurang,$asal);
              }elseif($ijenisbayar=='03'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updateku($group,$iarea,$igiro,$pengurang,$asal,$nkuyear);
              }elseif($ijenisbayar=='04'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updatekn($group,$iarea,$igiro,$pengurang,$asal);
              }elseif($ijenisbayar=='05'){
            $this->mmaster->updatelebihbayar($group,$iarea,$egirobank,$pengurang,$asal);
          }
          $this->mmaster->updatesaldo($group,$icustomer,$pengurang);
              for($i=1;$i<=$jml;$i++){
                $inota              = $this->input->post('inota'.$i, TRUE);
                $dnota              = $this->input->post('dnota'.$i, TRUE);
                if($dnota!=''){
                   $tmp=explode("-",$dnota);
                   $th=$tmp[2];
                   $bl=$tmp[1];
                   $hr=$tmp[0];
                   $dnota=$th."-".$bl."-".$hr;
                }
  /*
  a=giro/cek -- 01
  b=tunai -- 02
  c=transfer/ku -- 03
  d=kn -- 04
  e=lebih -- 05
  */
                $vjumlah= $this->input->post('vjumlah'.$i, TRUE);
                $vsisa  = $this->input->post('vsisa'.$i, TRUE);
                $vsiso  = $this->input->post('vsisa'.$i, TRUE);
                $vjumlah= str_replace(',','',$vjumlah);
                 $vsisa = str_replace(',','',$vsisa);
                 $vsiso = str_replace(',','',$vsiso);
#               $vsisa  = $vsisa-$vjumlah;
                $vsiso  = $vsiso-$vjumlah;
            $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
            $eremark= $this->input->post('eremark'.$i,TRUE);
                $this->mmaster->insertdetail(   $ipl,$idt,$iarea,$inota,$dnota,$ddt,$vjumlah,$vsisa,$i,$ipelunasanremark,$eremark);
                #$this->mmaster->updatedt($idt,$iarea,$ddt,$inota,$vsisa);
#               $this->mmaster->updatenota($inota,$vsisa);
#               $this->mmaster->updatenota($inota,$vsiso);
                $this->mmaster->updatenota($inota,$vjumlah);
              }
#             $nilai=$this->mmaster->hitungsisadt($idt,$iarea,$ddt);
#             if($nilai==0){
#                $this->mmaster->updatestatusdt($idt,$iarea,$ddt);
#             }
          if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                 $this->db->trans_commit();
#                 $this->db->trans_rollback();

                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Input Pelunasan No:'.$ipl.' Area:'.$iarea;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );

                 $data['sukses'] = true;
                 $data['inomor'] = $ipl;
                 $this->load->view('nomor',$data);
              }
        }
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function edit()
   {
      if ( (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
      $area1   = $this->session->userdata('i_area');
         $data['page_title'] = $this->lang->line('pelunasan')." update";
         if(
            ($this->uri->segment(4)) && ($this->uri->segment(5))
           )
         {
            $ipl  = $this->uri->segment(4);
            $iarea= $this->uri->segment(5);
            $idt  = $this->uri->segment(6);
            $dfrom= $this->uri->segment(7);
            $dto  = $this->uri->segment(8);

        if($ipl!=''){
               $ipl1=$ipl;
              $tmp=explode("-",$ipl);
              $siji=$tmp[0];
              $loro=$tmp[1];
              $telu=$tmp[2];
              $ipl=$siji."/".$loro."/".$telu;
           }
        if($idt!=''){
               $idt1=$idt;
              $tmp=explode("-",$idt);
              $siji=$tmp[0];
              $loro=$tmp[1];
              $idt=$siji."/".$loro;
           }
            $this->load->model('pelunasan/mmaster');
            $query   = $this->db->query("select * from tm_pelunasan_item
                                                      where i_pelunasan = '$ipl' and i_area = '$iarea' and i_dt='$idt' ");
            if($query->num_rows()==0){
               $query   = $this->db->query("select * from tm_pelunasan_item
                                                         where i_pelunasan = '$ipl1' and i_area = '$iarea' and i_dt='$idt1' ");
               $data['jmlitem']     = $query->num_rows();
               $data['vsisa']=$this->mmaster->sisa($iarea,$ipl1,$idt1);
               $data['isi']=$this->mmaster->bacapl($iarea,$ipl1,$idt1);
               $data['detail']=$this->mmaster->bacadetailpl($iarea,$ipl1,$idt1);

          $query = $this->db->query("select i_cek from tm_pelunasan where i_pelunasan = '$ipl1' and i_area = '$iarea' and i_dt='$idt1'");
              if($query->num_rows()>0){
            foreach($query->result() as $tmp){
              if($tmp->i_cek=='')
                $data['bisaedit']=true;
              else
                $data['bisaedit']=false;
            }
          }else{
            $data['bisaedit']=false;
          }

            }else{
               $data['jmlitem']     = $query->num_rows();
               $data['vsisa']=$this->mmaster->sisa($iarea,$ipl,$idt);
               $data['isi']=$this->mmaster->bacapl($iarea,$ipl,$idt);
               $data['detail']=$this->mmaster->bacadetailpl($iarea,$ipl,$idt);

          $query = $this->db->query("select i_cek from tm_pelunasan where i_pelunasan = '$ipl' and i_area = '$iarea' and i_dt='$idt'");
              if($query->num_rows()>0){
            foreach($query->result() as $tmp){
              if($tmp->i_cek=='')
                $data['bisaedit']=true;
              else
                $data['bisaedit']=false;
            }
          }else{
            $data['bisaedit']=false;
          }

            }

            $data['ipl']      = $ipl;
            $data['iarea'] = $iarea;
            $data['idt']      = $idt;
            $data['dfrom']  = $dfrom;
            $data['dto']     = $dto;
        $data['area1']  = $area1;
            $this->load->view('pelunasan/vmainform',$data);
         }else{
            $this->load->view('pelunasan/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function updatepelunasan()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('pelunasan/mmaster');
         $ipl  = $this->input->post('ipelunasan', TRUE);
         $idt  = $this->input->post('idt', TRUE);
         $ddt  = $this->input->post('ddt', TRUE);
         if($ddt!=''){
            $tmp=explode("-",$ddt);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $ddt=$th."-".$bl."-".$hr;
         }
      $dbukti  = $this->input->post('dbukti', TRUE);
         if($dbukti!=''){
            $tmp=explode("-",$dbukti);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dbukti=$th."-".$bl."-".$hr;
         }
         $djt  = $this->input->post('djt', TRUE);
         if($djt!=''){
            $tmp=explode("-",$djt);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $djt=$th."-".$bl."-".$hr;
         }
         $dcair   = $this->input->post('dcair', TRUE);
         if($dcair!=''){
            $tmp=explode("-",$dcair);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dcair=$th."-".$bl."-".$hr;
         }
         $dgiro   = $this->input->post('dgiro', TRUE);
         if($dgiro!=''){
            $tmp=explode("-",$dgiro);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dgiro=$th."-".$bl."-".$hr;
         }
         $icustomer     = $this->input->post('icustomer', TRUE);
         $ecustomername = $this->input->post('ecustomername', TRUE);
         $ecustomeraddress= $this->input->post('ecustomeraddress', TRUE);
         $ecustomercity = $this->input->post('ecustomercity', TRUE);
         $iarea         = $this->input->post('iarea', TRUE);
         $eareaname     = $this->input->post('eareaname', TRUE);
         $ijenisbayar   = $this->input->post('ijenisbayar',TRUE);
         $ejenisbayarname= $this->input->post('ejenisbayarname',TRUE);
         $egirobank     = $this->input->post('egirobank',TRUE);
         if(($egirobank==Null) && ($ijenisbayar!='05')){
            $egirobank  = '-';
            $igiro      = $this->input->post('igiro',TRUE);
         }else if(($egirobank==Null) && ($ijenisbayar=='05')){
            $egirobank  = $this->input->post('igiro',TRUE);
            $igiro      ='';
         }else if(($egirobank!=Null) && ($ijenisbayar=='01')){
            $igiro      = $this->input->post('igiro',TRUE);
         }else if(($egirobank!=Null) && ($ijenisbayar=='03')){
            $nkuyear = $this->input->post('nkuyear',TRUE);
            $igiro      = $this->input->post('igiro',TRUE);
         }else if($ijenisbayar=='04'){
            $igiro      = $this->input->post('igiro',TRUE);
         }else{
            $igiro      ='';
         }
         $vjumlah    = $this->input->post('vjumlah',TRUE);
         $vjumlah    = str_replace(',','',$vjumlah);
#        $vlebih        = $this->input->post('vlebih',TRUE);
#      if($ijenisbayar=='04'||$ijenisbayar=='05'){
#           $vlebih        = '0';
#      }else{
         $vlebih        = $this->input->post('vlebih',TRUE);
#      }
         $vlebih        = str_replace(',','',$vlebih);
#      $ipelunasanremark = $this->input->post('ipelunsanremark',TRUE);
#      $eremark    = $this->input->post('eremark',TRUE);
         $jml        = $this->input->post('jml', TRUE);
      $ada=false;
         if(($ddt!='') && ($dbukti!='') && ($idt!='') && ($vjumlah!='') && ($vjumlah!='0') && ($jml!='0') && ($ijenisbayar!='') && ($icustomer!='') ){
        for($i=1;$i<=$jml;$i++){
          $vjumla = $this->input->post('vjumlah'.$i, TRUE);
             $vsisa  = $this->input->post('vsisa'.$i, TRUE);
             $vjumla = str_replace(',','',$vjumla);
             $vsisa  = str_replace(',','',$vsisa);
             $vsisa  = $vsisa-$vjumla;
          $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
#          if( ($vsisa-$vjumlah>0) && ($ipelunasanremark=='') ){
          if( ($vsisa>0) && ($ipelunasanremark=='') ){
            $ada=true;
          }
          if($ada) break;
        }
        if(!$ada){
              $this->db->trans_begin();
              $asalkn   = $this->mmaster->jmlasalkn($ipl,$idt,$iarea,$ddt);
              foreach($asalkn as $asl){
                 $jmlpl = $asl->v_jumlah;
                 $lbhpl = $asl->v_lebih;
              }
              $asal  = $jmlpl-$lbhpl;
              $this->mmaster->deleteheader(  $ipl,$idt,$iarea,$ddt);
              $this->mmaster->insertheader(  $ipl,$idt,$iarea,$ijenisbayar,$igiro,$icustomer,$dgiro,$ddt,$dbukti,$dcair,$egirobank,
                                      $vjumlah,$vlebih);
              $pengurang=$vjumlah-$vlebih;
          $igiro=trim($igiro);

          $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
            $group='xxx';
            foreach($query->result() as $row){
               $group=$row->i_customer_groupbayar;
            }

              if($ijenisbayar=='01'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updategiro($group,$iarea,$igiro,$pengurang,$asal);
              }elseif($ijenisbayar=='03'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updateku($group,$iarea,$igiro,$pengurang,$asal,$nkuyear);
              }elseif($ijenisbayar=='04'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updatekn($group,$iarea,$igiro,$pengurang,$asal);
              }elseif($ijenisbayar=='05'){
            $this->mmaster->updatelebihbayar($group,$iarea,$egirobank,$pengurang,$asal);
          }
              for($i=1;$i<=$jml;$i++){
                $inota              = $this->input->post('inota'.$i, TRUE);
                $dnota              = $this->input->post('dnota'.$i, TRUE);
                if($dnota!=''){
                   $tmp=explode("-",$dnota);
                   $th=$tmp[2];
                   $bl=$tmp[1];
                   $hr=$tmp[0];
                   $dnota=$th."-".$bl."-".$hr;
              }
  /*
  a=giro/cek -- 01
  b=tunai -- 02
  c=transfer/ku -- 03
  d=kn -- 04
  e=lebih -- 05
  */
                $vjumlah   = $this->input->post('vjumlah'.$i, TRUE);
                $vasal  = $this->input->post('vasal'.$i, TRUE);
                $vasol  = $this->input->post('vasal'.$i, TRUE);
                if($vasal==''){
                  $vasal   = $this->input->post('vsisa'.$i, TRUE);
                  $vasol   = $this->input->post('vsisa'.$i, TRUE);
                }
                $vjumlah= str_replace(',','',$vjumlah);
                 $vasal = str_replace(',','',$vasal);
                 $vasol = str_replace(',','',$vasol);
#               $vsisa  = $vasal-$vjumlah;
                $vsisa  = $vasal;
                $vsiso  = $vasal-$vjumlah;
            $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
            $eremark    = $this->input->post('eremark'.$i,TRUE);

                $this->mmaster->deletedetail(   $ipl,$idt,$iarea,$inota,$ddt);
                $this->mmaster->insertdetail(   $ipl,$idt,$iarea,$inota,$dnota,$ddt,$vjumlah,$vsisa,$i,$ipelunasanremark,$eremark);
#           $this->mmaster->updatedt($idt,$iarea,$ddt,$inota,$vsisa);
#               $this->mmaster->updatenota($inota,$vsisa);
#               $this->mmaster->updatenota($inota,$vsiso);
                $this->mmaster->updatenota($inota,$vjumlah);
              }
#             $nilai=$this->mmaster->hitungsisadt($idt,$iarea,$ddt);
#             if($nilai==0){
#                $this->mmaster->updatestatusdt($idt,$iarea,$ddt);
#             }
              if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                 $this->db->trans_commit();
  #               $this->db->trans_rollback();

                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Update Pelunasan No:'.$ipl.' Area:'.$iarea;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );

                 $data['sukses'] = true;
                 $data['inomor'] = $ipl;
                 $this->load->view('nomor',$data);
              }
        }
         }

      }else{
         $this->load->view('awal/index.php');
      }
   }
   function customer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $idt  = str_replace('tandagaring','/',$this->uri->segment(4));
         $iarea   = $this->uri->segment(5);
         $config['base_url'] = base_url().'index.php/pelunasan/cform/customer/'.$idt.'/'.$iarea.'/index/';
         $config['per_page'] = '10';
         $query = $this->db->query("   select distinct(a.i_customer), b.* from tm_dt_item a, tr_customer b
                              where a.i_customer=b.i_customer and a.v_sisa>0
                              and a.i_dt = '$idt' and a.i_area = '$iarea'",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);

         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->bacacustomer($idt,$iarea,$config['per_page'],$this->uri->segment(7));
         $data['idt']=$idt;
         $data['iarea']=$iarea;
         $this->load->view('pelunasan/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function caricustomer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $idt = $this->uri->segment(4);
         $iarea = $this->uri->segment(5);
         $config['base_url'] = base_url().'index.php/pelunasan/cform/customer/'.$idt.'/'.$iarea.'/';
         $cari    = strtoupper($this->input->post('cari', FALSE));
         $query   = $this->db->query("select distinct(a.i_customer) from tm_dt_item a, tr_customer b
                                           where a.i_customer=b.i_customer
                                           and a.v_sisa>0
                                           and a.i_dt = '$idt' and a.i_area = '$iarea'
                                           and (upper(b.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%') ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->caricustomer($cari,$idt,$iarea,$config['per_page'],$this->uri->segment(6));
      $data['idt']=$idt;
         $data['iarea']=$iarea;
         $this->load->view('pelunasan/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function girocek()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/pelunasan/cform/girocek/index/';
         $config['per_page'] = '10';
         if($this->session->userdata('i_area')=='00' || $this->session->userdata('i_area')=='PB'){
           $query = $this->db->query("select * from tr_jenis_bayar",false);
         }else{
           $query = $this->db->query("select * from tr_jenis_bayar where i_jenis_bayar<>'05'",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('pelunasan/mmaster');
         $data['area']=$this->session->userdata('i_area');
         $data['page_title'] = $this->lang->line('list_girocek');
         $data['isi']=$this->mmaster->bacagirocek($config['per_page'],$this->uri->segment(5),$this->session->userdata('i_area'));
         $this->load->view('pelunasan/vlistgirocek', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function nota()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['baris']    = $this->uri->segment(4);
         $data['iarea']    = $this->uri->segment(5);
         $data['idt']        = str_replace('tandagaring','/',$this->uri->segment(6));
         $data['icustomer']= $this->uri->segment(7);
         $data['dku']        = $this->uri->segment(8);
         $baris                = $this->uri->segment(4);
         $iarea                = $this->uri->segment(5);
         $idt                 = str_replace('tandagaring','/',$this->uri->segment(6));
         $icustomer          = $this->uri->segment(7);
         $dku                 = $this->uri->segment(8);
         $config['base_url'] = base_url().'index.php/pelunasan/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/'.$dku.'/index/';
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupbayar;
         }
         $query = $this->db->query("select a.i_dt from tm_dt_item a, tr_customer_groupbayar c
                                    where c.i_customer_groupbayar='$group' and a.i_customer=c.i_customer
                                    and a.i_dt='$idt'
                                    and a.i_area='$iarea'" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(10);
         $this->pagination->initialize($config);
         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(10),$group);
         $this->load->view('pelunasan/vlistnota', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carinota()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari              = strtoupper($this->input->post('cari', FALSE));
         $baris             = $this->input->post('baris', FALSE)?$this->input->post('baris', FALSE):$this->uri->segment(4);
         $iarea            = $this->input->post('iarea', FALSE)?$this->input->post('iarea', FALSE):$this->uri->segment(5);
         $idt           = $this->input->post('idt', FALSE)?$this->input->post('idt', FALSE):$this->uri->segment(6);
         $icustomer        = $this->input->post('icustomer', FALSE)?$this->input->post('icustomer', FALSE):$this->uri->segment(7);
         $dku              = $this->input->post('dku', FALSE)?$this->input->post('dku', FALSE):$this->uri->segment(8);
         $data['baris']    = $baris;
         $data['iarea']    = $iarea;
         $data['idt']      = $idt;
         $data['icustomer']   = $icustomer;
         $data['dku']      = $dku;
         $config['base_url'] = base_url().'index.php/pelunasan/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/'.$dku.'/index/';
      $group='xxx';
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
      foreach($query->result() as $row){
        $group=$row->i_customer_groupbayar;
      }
         $query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer_groupbayar b
                                            where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                                            and a.i_dt='$idt'
                                            and a.i_area='$iarea'
                                            and (upper(a.i_nota) like '%$cari%' or upper(a.i_customer) like '%$cari%')" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(10);
         $this->pagination->initialize($config);
         $this->load->model('pelunasan/mmaster');

         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->carinota($cari,$iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(10),$group);
         $data['baris']=$baris;
         $this->load->view('pelunasan/vlistnota', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function giro()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = $this->uri->segment(4);
         $iarea      = $this->uri->segment(5);
         $dbukti     = $this->uri->segment(6);
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/pelunasan/cform/giro/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $config['per_page'] = '10';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
#        $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
#           $group=$row->i_customer_groupbayar;
         }
         $query = $this->db->query("   select a.i_giro from tm_giro  a, tr_customer_groupar b
                                       where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                                       and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                                       and not a.d_giro_cair isnull and a.d_giro_cair<='$xdbukti'",false);
/*
         $query = $this->db->query("   select a.i_giro from tm_giro  a, tr_customer_groupbayar b
                                          where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                                          and (a.f_giro_tolak='f' or a.f_giro_batal='f')",false);
#                                           and a.i_area='$iarea'
*/
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_giro');
         $data['isi']=$this->mmaster->bacagiro($icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('pelunasan/vlistgiro', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
	function carigiro()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer = $this->input->post('icustomer', FALSE);
			$iarea= $this->input->post('iarea', FALSE);
			$dbukti     = strtoupper($this->input->post('dbukti', FALSE));
      if($dbukti!=''){
        $tmp=explode('-',$dbukti);
        $hr=$tmp[0];
        $bl=$tmp[1];
        $th=$tmp[2];
        $xdbukti=$th.'-'.$bl.'-'.$hr;
      }
      $cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/pelunasan/cform/giro/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
			$group='xxx';
			foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
			}
			$query = $this->db->query("	select a.i_giro from tm_giro  a, tr_customer_groupar b
									              	where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
							                    and (a.f_giro_tolak='f' and a.f_giro_batal='f') and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                                  and (upper(a.i_giro) like '%$cari%') and not a.d_giro_cair isnull and a.d_giro_cair<='$xdbukti'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_giro');
			$data['isi']=$this->mmaster->carigiro($cari,$icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$data['dbukti']=$dbukti;
			$this->load->view('pelunasan/vlistgiro', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function lebihbayar()

   {

      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = $this->uri->segment(4);
         $iarea      = $this->uri->segment(5);
         $dbukti     = $this->uri->segment(6);
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/pelunasan/cform/lebihbayar/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $config['per_page'] = '10';
          $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
          $group='xxx';
          foreach($query->result() as $row){
            $group=$row->i_customer_groupar;
          }
/*
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupbayar;
         }
*/
         $query = $this->db->query("SELECT   a.i_dt, min(a.v_jumlah), min(a.v_lebih), a.i_area, a.i_customer,
                                    a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2))
                                    from tm_pelunasan_lebih  a, tr_customer_groupar b
                                    where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and i_area='$iarea'
                                    and v_lebih>0 and a.f_pelunasan_cancel='f' and a.d_bukti<='$xdbukti'
                                    group by i_dt, d_bukti, i_area, a.i_customer",false);
/*
         $query = $this->db->query("   SELECT   a.i_dt, min(a.v_jumlah), min(a.v_lebih), a.i_area,
                                             a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2))
                                            from tm_pelunasan_lebih  a, tr_customer_groupbayar b
                                            where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                                            and v_lebih>0 and a.f_pelunasan_cancel='f'
                                            group by i_dt, d_bukti, i_area",false);
*/
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_pelunasan');
         $data['isi']=$this->mmaster->bacapelunasan($icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('pelunasan/vlistpelunasan', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carilebihbayar()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = strtoupper($this->input->post('icustomer', FALSE));
         $iarea      = strtoupper($this->input->post('iarea', FALSE));
         $dbukti     = strtoupper($this->input->post('dbukti', FALSE));
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/pelunasan/cform/lebihbayar/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
          $group='xxx';
          foreach($query->result() as $row){
            $group=$row->i_customer_groupar;
          }
         $query   = $this->db->query("SELECT a.i_dt, min(a.v_jumlah), min(a.v_lebih), a.i_area, a.i_customer,
                                             a.d_bukti,a.i_dt||'-'||max(substr(a.i_pelunasan,9,2))
                                            from tm_pelunasan_lebih  a, tr_customer_groupar b
                                            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and i_area='$iarea'
                                            and v_lebih>0 and a.f_pelunasan_cancel='f' and a.d_bukti<='$xdbukti'
                                            and (upper(a.i_pelunasan) like '%$cari%') 
                                            group by i_dt, d_bukti, i_area, a.i_customer",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(7);
         $this->pagination->initialize($config);
         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_pelunasan');
         $data['isi']=$this->mmaster->caripelunasan($cari,$icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('pelunasan/vlistpelunasan', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function kn()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = $this->uri->segment(4);
         $iarea      = $this->uri->segment(5);
         $dbukti     = $this->uri->segment(6);
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/pelunasan/cform/kn/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
        $group='xxx';
        foreach($query->result() as $row){
          $group=$row->i_customer_groupbayar;
        }
/*
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupbayar;
         }
*/
         /*$query = $this->db->query(" select a.* from tm_kn a, tr_customer_groupar b
                                            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                            and a.v_sisa>0",false);
         */

         $query = $this->db->query("select a.* from tm_kn a, tr_customer_groupbayar b
                                    where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                                    and a.v_sisa>0 and d_kn<='$xdbukti' and a.f_kn_cancel='f'",false);

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_kn');
         $data['isi']=$this->mmaster->bacakn($icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('pelunasan/vlistkn', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carikn()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = strtoupper($this->input->post('icustomer', FALSE));
         $iarea      = strtoupper($this->input->post('iarea', FALSE));
         $dbukti     = strtoupper($this->input->post('dbukti', FALSE));
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/pelunasan/cform/carikn/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
        $group='xxx';
        foreach($query->result() as $row){
          $group=$row->i_customer_groupar;
        }
         $query   = $this->db->query("select a.* from tm_kn a, tr_customer_groupar b
                                      where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                      and a.v_sisa>0 and (upper(i_kn) like '%$cari%')
                                      and d_kn<='$xdbukti' and a.f_kn_cancel='f' ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_kn');
         $data['isi']=$this->mmaster->carikn($cari,$icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('pelunasan/vlistkn', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function ku()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $icustomer  = $this->uri->segment(4);
         $iarea      = $this->uri->segment(5);
         $dbukti     = $this->uri->segment(6);
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/pelunasan/cform/ku/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $config['per_page'] = '10';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupar;
         }
/*
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
         $group='xxx';
         foreach($query->result() as $row){
            $group=$row->i_customer_groupbayar;
         }
*/
         $query = $this->db->query("  select a.* from tm_kum a, tr_customer_groupar b
                                      where b.i_customer_groupar='$group' and a.i_customer=b.i_customer and a.i_area='$iarea'
                                      and a.v_sisa>0 and a.v_sisa=a.v_jumlah and a.f_close='f' and a.f_kum_cancel='f'
                                      and d_kum<='$xdbukti'",false);
/*
         $query = $this->db->query("   select a.* from tm_kum a, tr_customer_groupbayar b
                              where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                              and a.v_sisa>0 and a.v_sisa=a.v_jumlah
                              and a.f_close='f' ",false);
*/
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_ku');
         $data['isi']=$this->mmaster->bacaku($icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('pelunasan/vlistku', $data);
      }else{
         $this->load->view('awal/index.php');
      }

   }
   function cariku()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari       = strtoupper($this->input->post('cari', FALSE));
         $icustomer  = strtoupper($this->input->post('customer', FALSE));
         $iarea      = strtoupper($this->input->post('area', FALSE));
         $dbukti     = strtoupper($this->input->post('dbukti', FALSE));
         if($dbukti!=''){
          $tmp=explode('-',$dbukti);
          $hr=$tmp[0];
          $bl=$tmp[1];
          $th=$tmp[2];
          $xdbukti=$th.'-'.$bl.'-'.$hr;
         }
         $config['base_url'] = base_url().'index.php/pelunasan/cform/ku/'.$icustomer.'/'.$iarea.'/'.$dbukti.'/index/';
         $config['per_page'] = '10';

        $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
        $group='xxx';
        foreach($query->result() as $row){
          $group=$row->i_customer_groupbayar;
        }
#        if($group!='G0000'){
             $query = $this->db->query(" select a.* from tm_kum a, tr_customer_groupbayar b
                                  where b.i_customer_groupbayar='$group' and a.i_customer=b.i_customer
                                  and a.v_sisa>0 and d_kum<='$xdbukti'
                                  and a.f_close='f' and a.f_kum_cancel='f'",false);
#        }else{
#          $query = $this->db->query("   select a.* from tm_kum a
#                               where a.i_customer='$icustomer'
#                               and a.i_area='$iarea'
#                               and a.v_sisa>0
#                               and a.f_close='f' ",false);
#        }
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_ku');
#        if($group!='G0000'){
         $data['isi']=$this->mmaster->cariku($cari,$icustomer,$iarea,$config['per_page'],$this->uri->segment(8),$group,$xdbukti);
#      }else{
#           $data['isi']=$this->mmaster->bacaku2($icustomer,$iarea,$config['per_page'],$this->uri->segment(7),$group);
#      }
         $data['icustomer']=$icustomer;
         $data['iarea']=$iarea;
         $data['dbukti']=$dbukti;
         $this->load->view('pelunasan/vlistku', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function notaupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['baris']    = $this->uri->segment(4);
         $data['iarea']    = $this->uri->segment(5);
         $data['idt']      = $this->uri->segment(6);
         $data['icustomer']   = $this->uri->segment(7);
         //$data['dku']    = $this->uri->segment(8);
         $baris            = $this->uri->segment(4);
         $iarea            = $this->uri->segment(5);
         $idt           = $this->uri->segment(6);
         $icustomer        = $this->uri->segment(7);
         //$dku            = $this->uri->segment(8);
         $config['base_url'] = base_url().'index.php/pelunasan/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
      }
         $query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer_groupar b
                                            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                            and a.i_dt='$idt'
                                            and a.i_area='$iarea'" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         //$config['cur_page']   = $this->uri->segment(10);
         $config['cur_page']  = $this->uri->segment(9);
         $this->pagination->initialize($config);
         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
         $this->load->view('pelunasan/vlistnotaupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carinotaupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari             = strtoupper($this->input->post('cari', FALSE));
         $data['baris']    = $this->input->post('baris', FALSE);
         $data['iarea']    = $this->input->post('iarea', FALSE);
         $data['idt']      = $this->input->post('idt', FALSE);
         $data['icustomer']   = $this->input->post('icustomer', FALSE);
         //$data['dku']    = $this->uri->segment(8);
         $baris            = $this->input->post('baris', FALSE);
         $iarea            = $this->input->post('iarea', FALSE);
         $idt           = $this->input->post('idt', FALSE);
         $icustomer        = $this->input->post('icustomer', FALSE);
         //$dku            = $this->uri->segment(8);
         $config['base_url'] = base_url().'index.php/pelunasan/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
      }
         $query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer_groupar b
                                            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                            and a.i_dt='$idt'
                                            and a.i_area='$iarea'" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         //$config['cur_page']   = $this->uri->segment(10);
         $config['cur_page']  = $this->uri->segment(9);
         $this->pagination->initialize($config);
         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
         $this->load->view('pelunasan/vlistnotaupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/pelunasan/cform/area/index/';
         $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
         $this->load->view('pelunasan/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/pelunasan/cform/area/index/';
         $iuser   	= $this->session->userdata('user_id');
         $cari    	= $this->input->post('cari', FALSE);
         $cari 		  = strtoupper($cari);
         $query   	= $this->db->query("select * from tr_area
                                  where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                  and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
         $this->load->view('pelunasan/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function view()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $cari    = strtoupper($this->input->post('cari'));
         $dfrom      = $this->input->post('dfrom');
         $dto     = $this->input->post('dto');
         $iarea      = $this->input->post('iarea');
         if($dfrom=='') $dfrom=$this->uri->segment(4);
         if($dto=='') $dto=$this->uri->segment(5);
         if($iarea=='') $iarea   = $this->uri->segment(6);
         $config['base_url'] = base_url().'index.php/pelunasan/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';

         $query = $this->db->query(" select a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah,
                                            b.e_area_name, sum(c.v_sisa) as v_sisa from tm_dt a, tr_area b, tm_dt_item c
                                            where a.i_area=b.i_area and a.f_dt_cancel='false'
                                            and a.f_sisa = 't'
                                            and a.i_dt=c.i_dt and a.i_area=c.i_area
                                            and a.d_dt >= to_date('$dfrom','dd-mm-yyyy')
                                            and a.d_dt <= to_date('$dto','dd-mm-yyyy')
                                            and upper(a.i_area)='$iarea'
                                  and (upper(a.i_dt) like '%$cari%')
                                            group by a.i_dt, a.i_area, a.d_dt, a.f_sisa, a.v_jumlah, b.e_area_name",false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(8);
         $this->paginationxx->initialize($config);
         $data['page_title'] = $this->lang->line('pelunasan');
         $this->load->model('pelunasan/mmaster');
         $data['cari']  = $cari;
         $data['dfrom'] = $dfrom;
         $data['dto']   = $dto;
         $data['iarea'] = $iarea;
         $data['isi']   = $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
         $data['idt']   = '';
         $data['ipl']   = '';
         $data['c']     = $dfrom.'/'.$dto.'/'.$iarea.'/'.$this->uri->segment(8);
         $this->load->view('pelunasan/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function remark()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $baris = $this->uri->segment(4);
         $config['base_url'] = base_url().'index.php/pelunasan/cform/remark/'.$baris.'/';
         $config['per_page'] = '10';
         $query = $this->db->query("   select i_pelunasan_remark from tr_pelunasan_remark",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('pelunasan/mmaster');
         $data['page_title'] = $this->lang->line('list_pelunasan_remark');
         $data['isi']=$this->mmaster->bacaremark($config['per_page'],$this->uri->segment(5));
      $data['baris']=$baris;
         $this->load->view('pelunasan/vlistremark', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
