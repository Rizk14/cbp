<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu304')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnknretur');
      $data['iarea']='';
			$this->load->view('opnknretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu304')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$iarea		= $this->input->post('iarea');
			if($iarea=='') $iarea	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/opnknretur/cform/view/'.$iarea.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_kn a, tr_customer b
                                  where a.i_customer=b.i_customer and a.f_kn_cancel='f'
                                  and a.i_kn_type='01' and a.v_sisa>0
                                  and (upper(a.i_kn) like '%$cari%'
                                  or upper(a.i_customer) like '%$cari%' 
                                  or upper(b.e_customer_name) like '%$cari%')
                                  and a.i_area='$iarea' order by a.i_kn",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('opnknretur/mmaster');
			$data['page_title'] = $this->lang->line('opnknretur');
			$data['cari']		= $cari;
			$data['keyword']	= '';
			$data['iarea']		= $iarea;
      $data['isi']		= $this->mmaster->bacaperiode($iarea,$config['per_page'],$this->uri->segment(5),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Lebih Bayar Area '.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('opnknretur/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu304')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnknretur');
			$this->load->view('opnknretur/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu304')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$iarea		= $this->input->post('iarea');
			if($iarea=='') $iarea	= $this->uri->segment(4);
			if($cari=='') $cari	= $this->uri->segment(5);
      $config['base_url'] = base_url().'index.php/opnknretur/cform/cari/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_kn a, tr_customer b
                                  where a.i_customer=b.i_customer
                                  and a.i_kn_type='01' and a.v_sisa>0
                                  and (upper(a.i_kn) like '%$cari%'
                                  or upper(a.i_customer) like '%$cari%' 
                                  or upper(b.e_customer_name) like '%$cari%')
                                  and a.i_area='$iarea' order by a.i_kn",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('opnknretur/mmaster');
			$data['page_title'] = $this->lang->line('opnknretur');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['iarea']		= $iarea;
      $data['isi']		= $this->mmaster->bacaperiode($iarea,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('opnknretur/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu304')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea		= $this->input->post('iarea');
			if($iarea=='') $iarea	= $this->uri->segment(4);
			$keyword = strtoupper($this->uri->segment(5));
      $config['base_url'] = base_url().'index.php/opnknretur/cform/cariperpages/'.$iarea.'/'.$keyword.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_kn a, tr_customer b
                                  where a.i_customer=b.i_customer
                                  and a.i_kn_type='01' and a.v_sisa>0
                                  and (upper(a.i_kn) like '%$keyword%'
                                  or upper(a.i_customer) like '%$keyword%' 
                                  or upper(b.e_customer_name) like '%$keyword%')
                                  and a.i_area='$iarea' order by a.i_kn",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('opnknretur/mmaster');
			$data['page_title'] = $this->lang->line('opnknretur');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['iarea']		= $iarea;
      $data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$config['per_page'],$this->uri->segment(7),$keyword);
			$this->load->view('opnknretur/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu304')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opnknretur/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('opnknretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('opnknretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu304')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opnknretur/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
      if($iuser=='arpusat4'){
      $query   	= $this->db->query("select * from tr_area
                                where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
      }else{
      $query   	= $this->db->query("select * from tr_area
                              where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('opnknretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('opnknretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
