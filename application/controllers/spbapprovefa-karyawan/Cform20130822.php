<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spbapprovefa/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$allarea= $this->session->userdata('allarea');
			//if( ($allarea=='t') || ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
			if( ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
			{
				$query = $this->db->query(" 	select a.*, b.e_customer_name, c.e_area_name 
								from tm_spb a, tr_customer b, tr_area c
								where 
								a.i_customer=b.i_customer 
								and a.i_area=c.i_area
								and a.i_approve2 isnull  
								and a.i_notapprove isnull
								and a.f_spb_cancel='f'
								and not a.i_customer like '%000'
								and
							     ( ( a.f_spb_stockdaerah='t' and 
							         (  a.i_area='$area1' or 
							            a.i_area='$area2' or 
							            a.i_area='$area3' or 
							            a.i_area='$area4' or 
							            a.i_area='$area5'
							          )
							        )
								or	
								  (a.f_spb_stockdaerah='f' and not a.i_approve1 isnull)) order by a.i_spb desc ",false);
			}else{
				$query = $this->db->query(" 	select a.*, b.e_customer_name, c.e_area_name 
								from tm_spb a, tr_customer b, tr_area c
								where 
								a.i_customer=b.i_customer 
								and a.i_area=c.i_area
								and a.i_approve1 isnull
								and a.i_approve2 isnull  
								and a.i_notapprove isnull
								and a.f_spb_cancel='f'
								and not a.i_customer like '%000'
								and (a.f_spb_stockdaerah='t' and (a.i_area='$area1' or a.i_area='$area2' 
									or a.i_area='$area3' or a.i_area='$area4' 
									or a.i_area='$area5')) order by a.i_spb desc ",false);
			}
/*
			if( ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
			{
				$query = $this->db->query(" 	select a.*, b.e_customer_name, c.e_area_name 
								from tm_spb a, tr_customer b, tr_area c
								where 
								a.i_customer=b.i_customer 
								and a.i_area=c.i_area
								and not a.i_approve1 isnull

								and a.i_approve2 isnull  
								and a.i_notapprove isnull
								and a.f_spb_cancel='f'

								and not a.i_customer like '%000'
								and
							     ( ( a.f_spb_stockdaerah='t' and 
							         (  a.i_area='$area1' or 

							            a.i_area='$area2' or 
							            a.i_area='$area3' or 
							            a.i_area='$area4' or 
							            a.i_area='$area5'

							          )
							        )
								or	
								  (a.f_spb_stockdaerah='f')) order by a.i_spb desc ",false);
			}else{
				$query = $this->db->query(" 	select a.*, b.e_customer_name, c.e_area_name 
								from tm_spb a, tr_customer b, tr_area c
								where 
								a.i_customer=b.i_customer 
								and a.i_area=c.i_area
								and not a.i_approve1 isnull
								and a.i_approve2 isnull  
								and a.i_notapprove isnull
								and a.f_spb_cancel='f'
								and not a.i_customer like '%000'
								and (a.f_spb_stockdaerah='t' and (a.i_area='$area1' or a.i_area='$area2' 
									or a.i_area='$area3' or a.i_area='$area4' 
									or a.i_area='$area5')) order by a.i_spb desc ",false);
			}
*/
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listspb');
			$this->load->model('spbapprovefa/mmaster');
			$data['ispb']='';
			$data['isi']=$this->mmaster->bacasemua($area1,$area2,$area3,$area4,$area5,$allarea,$cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('spbapprovefa/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listspb');
			$this->load->view('spbapprovefa/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb	= $this->input->post('ispbdelete', TRUE);
			$iarea	= $this->input->post('iareadelete', TRUE);
			$this->load->model('spbapprovefa/mmaster');
			$this->mmaster->delete($ispb);
			$config['base_url'] = base_url().'index.php/spbapprovefa/cform/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query(" 	select a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
							where a.i_customer=b.i_customer and a.i_approve2 isnull
							and not a.i_approve1 isnull and a.i_area=c.i_area
							and a.f_spb_stockdaerah='f'
							and a.i_notapprove isnull
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%') order by a.i_spb desc ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listspb');
			$this->load->model('spbapprovefa/mmaster');
			$data['ispb']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('spbapprovefa/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/spbapprovefa/cform/index/';
			$config['per_page'] = '10';
			$limo=$config['per_page'];
			$ofso=$this->uri->segment(4);
			if($ofso=='')
				$ofso=0;
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$allarea= $this->session->userdata('allarea');
			//if($allarea=='t'){
			if( ($area1=='00') || ($area2=='00') || ($area3=='00') || ($area4=='00') || ($area5=='00') )
			{	
				$query = $this->db->query(" 	select a.*, b.e_customer_name, c.e_area_name 
							from tm_spb a, tr_customer b, tr_area c
							where 
							a.i_customer=b.i_customer 
							and a.i_area=c.i_area
							and not a.i_approve1 isnull
							and a.i_approve2 isnull  
							and a.i_notapprove isnull
							and a.f_spb_cancel='f'
							and not a.i_customer like '%000'
							and (upper(a.i_spb) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
							     upper(a.i_customer) like '%$cari%' or upper(a.i_spb_old) like '%$cari')
							and
							  ((a.f_spb_stockdaerah='t')
							or	
							  (a.f_spb_stockdaerah='f' and not a.i_approve1 isnull)) order by a.i_spb desc ",false);
				/*
				$query = $this->db->query(" 	select a.*, b.e_customer_name, c.e_area_name 
								from tm_spb a, tr_customer b, tr_area c
								where 
								a.i_customer=b.i_customer 
								and a.i_area=c.i_area
								and not a.i_approve1 isnull
								and a.i_approve2 isnull  
								and a.i_notapprove isnull
								and a.f_spb_cancel='f'
								and (upper(a.i_spb) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
								     upper(a.i_customer) like '%$cari%')
								and
								  ((a.f_spb_stockdaerah='t')
								or	
								  (a.f_spb_stockdaerah='f'))",false);
				*/
			}else{
				$query = $this->db->query(" 	select a.*, b.e_customer_name, c.e_area_name 
								from tm_spb a, tr_customer b, tr_area c
								where 
								a.i_customer=b.i_customer 
								and a.i_area=c.i_area
								and a.i_approve1 isnull
								and a.i_approve2 isnull  
								and a.i_notapprove isnull
								and a.f_spb_cancel='f'
								and not a.i_customer like '%000'
								and (upper(a.i_spb) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or 
								     upper(a.i_customer) like '%$cari%')
								and
								  ((a.f_spb_stockdaerah='t'
								  and (a.i_area='$area1' or a.i_area='$area2' 
									or a.i_area='$area3' or a.i_area='$area4' 
									or a.i_area='$area5'))
								  ) order by a.i_spb desc ",false);
			}
			/*
			$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
							where a.i_customer=b.i_customer and a.i_area=c.i_area
							and a.i_approve2 isnull
							and not a.i_approve1 isnull
							and a.f_spb_stockdaerah='f'
							and a.i_notapprove isnull
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%')",false);
			*/
			$config['total_rows']	= $query->num_rows();	
			$config['first_link']	= 'Awal';
			$config['last_link']	= 'Akhir';
			$config['next_link']	= 'Selanjutnya';
			$config['prev_link']	= 'Sebelumnya';
			$config['cur_page']	= $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('spbapprovefa/mmaster');
			$data['isi']=$this->mmaster->cari($area1,$area2,$area3,$area4,$area5,$allarea,$cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('listspb');
			$data['ispb']='';
	 		$this->load->view('spbapprovefa/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spbapprovefa');
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
			  )
			{
				$ispb = $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispb'] = $ispb;
				$this->load->model('spbapprovefa/mmaster');
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea);
		 		$this->load->view('spbapprovefa/vmainform',$data);
			}else{
				$this->load->view('spbapprovefa/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb 		= $this->input->post('ispb', TRUE);
			$iarea		= $this->input->post('iarea',TRUE);
			$eapprove2	= $this->input->post('eapprove2',TRUE);
			if($eapprove2=='')
				$eapprove2=null;
			$user		=$this->session->userdata('user_id');
			$this->db->trans_begin();
			$this->load->model('spbapprovefa/mmaster');
			$this->mmaster->approve($ispb, $iarea, $eapprove2,$user);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Approve SPB Area '.$iarea.' No:'.$ispb;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );
				$data['sukses']			= true;
				$data['inomor']			= $ispb;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function notapprove()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu49')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb 		= $this->input->post('nospb', TRUE);
			$iarea		= $this->input->post('kdarea',TRUE);
			$eapprove	= $this->input->post('enotapprove',TRUE);
			if($eapprove=='')
				$eapprove=null;
			$user		=$this->session->userdata('user_id');
			$this->db->trans_begin();
			$this->load->model('spbapprovesales/mmaster');
			$this->mmaster->notapprove($ispb, $iarea, $eapprove,$user);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
				$data['sukses']			= true;
				$data['inomor']			= $ispb;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
