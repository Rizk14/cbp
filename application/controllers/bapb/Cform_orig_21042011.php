<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea1	= $this->session->userdata('i_area');
			$data['page_title'] = $this->lang->line('bapb');
			$this->load->model('bapb/mmaster');
			$data['isi']=$this->mmaster->bacasemua();
			$data['ibapb']='';
			$data['detail']="";
			$data['jmlitem']="";
			$data['detailx']="";
			$data['jmlitemx']="";
      			$data['tgl']=date('d-m-Y');
			$data['iarea1'] = $iarea1;
			$this->load->view('bapb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      			$iarea1	= $this->session->userdata('i_area');
				$dbapb 	= $this->input->post('dbapb', TRUE);
				if($dbapb!=''){
					$tmp=explode("-",$dbapb);
					$th=$tmp[2];
					$bl=$tmp[1];
					$hr=$tmp[0];
					$dbapb=$th."-".$bl."-".$hr;
					$thbl=substr($th,2,2).$bl;
				}
			$ibapbold 	= $this->input->post('ibapbold', TRUE);
			$ibapb	 	= $this->input->post('ibapb', TRUE);
			$eareaname 	= $this->input->post('eareaname', TRUE);
			$iareax	 	= $this->input->post('iareax', TRUE);
			//$iarea	 = $this->input->post('iarea', TRUE);
			$edkbkirim 	= $this->input->post('edkbkirim', TRUE);
			$idkbkirim 	= $this->input->post('idkbkirim', TRUE);
			$ecustomername	= $this->input->post('ecustomername', TRUE);
			$icustomer	= $this->input->post('icustomer', TRUE);
			$nbal		= $this->input->post('nbal', TRUE);
			$nbal		= str_replace(',','',$nbal);
			$jml		= $this->input->post('jml', TRUE);
			$jmlx		= $this->input->post('jmlx', TRUE);
			if($dbapb!='' && $iareax!='' && $idkbkirim!='' && $nbal!='0' && $nbal!='' && $jml!='0' && $jmlx!='0')
			{
				$this->db->trans_begin();

				if($iarea1=='00'){
				  $iareacode	= $iareax;
				  $daer='f';
				}else{
				  $iareacode	= $iarea1;	
				  $daer='t';
				}
				$this->load->model('bapb/mmaster');
				$ibapb	=$this->mmaster->runningnumber($iareacode,$thbl);
				$this->mmaster->insertheader($ibapb, $dbapb, $iarea1, $idkbkirim, $icustomer, $nbal, $ibapbold,$iareacode);
				//$this->mmaster->insertheader($ibapb, $dbapb, $iarea1, $idkbkirim, $icustomer, $nbal, $ibapbold,$iarea);
				//print $ibapb."-".$dbapb."-".$iareacode."-".$idkbkirim."-".$icustomer."-".$nbal."-".$ibapbold."-".$iarea;

				for($i=1;$i<=$jml;$i++){
				  $isj	= $this->input->post('isj'.$i, TRUE);
				  $dsj	= $this->input->post('dsj'.$i, TRUE);
				  if($dsj!=''){
						$tmp=explode("-",$dsj);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dsj=$th."-".$bl."-".$hr;
				  }
				  $eremark	= $this->input->post('eremark'.$i, TRUE);
					if($eremark=='') $eremark=null;
					$this->mmaster->insertdetail($ibapb,$iarea1,$isj,$dbapb,$dsj,$eremark, $iareacode);
					$this->mmaster->updatesj($ibapb,$isj,$iareacode,$dbapb,$daer);
					//print $ibapb."-".$isj."-".$iareacode."-".$dbapb."-".$daer."<br>";
				}

				for($i=1;$i<=$jmlx;$i++){
				  $iekspedisi	= $this->input->post('iekspedisi'.$i, TRUE);
					$eremark	= $this->input->post('eremarkx'.$i, TRUE);
				  $this->mmaster->insertdetailekspedisi($ibapb,$iarea1,$iekspedisi,$dbapb,$eremark);
				  //print $ibapb."-".$iareacode."-".$iekspedisi."-".$dbapb."-".$eremark."<br>";
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();
						$data['sukses']	= true;
						$data['inomor']	= $ibapb;
						$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bapb');
			$this->load->view('bapb/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bapb')." update";
			if($this->uri->segment(4)!=''){
				$ibapb 		= $this->uri->segment(4);
				$iarea		= $this->uri->segment(5);
				$dfrom		= $this->uri->segment(6);
				$dto 			= $this->uri->segment(7);
				$icustomer= $this->uri->segment(8);
				$iareasj		= $this->uri->segment(9);
				$query = $this->db->query("select * from tm_bapb_item where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ibapb'] 		= $ibapb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				$data['iareasj']	= $iareasj;
				$data['icustomer']= $icustomer;
				$query = $this->db->query("select * from tm_bapb_ekspedisi where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows(); 				
				$this->load->model('bapb/mmaster');
				$data['isi']=$this->mmaster->baca($ibapb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ibapb,$iarea);
				$data['detailx']=$this->mmaster->bacadetailx($ibapb,$iarea);
		 		$this->load->view('bapb/vmainform',$data);
			}else{
				$this->load->view('bapb/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $iarea1	= $this->session->userdata('i_area');
			$dbapb 		= $this->input->post('dbapb', TRUE);
			if($dbapb!=''){
				$tmp=explode("-",$dbapb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbapb=$th."-".$bl."-".$hr;
			}
			$ibapbold 			= $this->input->post('ibapbold', TRUE);
			$ibapb	 				= $this->input->post('ibapb', TRUE);
			$eareaname 			= $this->input->post('eareaname', TRUE);
			$iarea	 				= $this->input->post('iarea', TRUE);
			$edkbkirim 			= $this->input->post('edkbkirim', TRUE);
			$idkbkirim 			= $this->input->post('idkbkirim', TRUE);
			$ecustomername	= $this->input->post('ecustomername', TRUE);
			$icustomer			= $this->input->post('icustomer', TRUE);		
			$nbal						= $this->input->post('nbal', TRUE);		
			$nbal						= str_replace(',','',$nbal);
			$jml						= $this->input->post('jml', TRUE);
			$jmlx						= $this->input->post('jmlx', TRUE);
			if($dbapb!='' && $iarea!='' && $idkbkirim!='' && $nbal!='0' && $nbal!='' && $jml!='0' && $jmlx!='0')
			{
				$this->db->trans_begin();
#        $area1	= $this->session->userdata('i_area');
        if($iarea1=='00'){
          $daer='f';
        }else{
          $daer='t';
        }
				$this->load->model('bapb/mmaster');
				$this->mmaster->deleteheader($ibapb, $iarea1);
				$this->mmaster->insertheader($ibapb, $dbapb, $iarea1, $idkbkirim, $icustomer, $nbal, $ibapbold,$iarea);
				for($i=1;$i<=$jml;$i++){
				  $isj			= $this->input->post('isj'.$i, TRUE);
				  $dsj			= $this->input->post('dsj'.$i, TRUE);
					if($dsj!=''){
						$tmp=explode("-",$dsj);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dsj=$th."-".$bl."-".$hr;
					}
				  $this->mmaster->deletedetail($ibapb,$iarea1,$isj,$daer);
				  $eremark	= $this->input->post('eremark'.$i, TRUE);
					if($eremark=='') $eremark=null;
				  $this->mmaster->insertdetail($ibapb,$iarea1,$isj,$dbapb,$dsj,$eremark,$iarea);
				  $this->mmaster->updatesj($ibapb,$isj,$iarea,$dbapb,$daer);
				}
				for($i=1;$i<=$jmlx;$i++){
				  $iekspedisi	= $this->input->post('iekspedisi'.$i, TRUE);
					$eremark	= $this->input->post('eremarkx'.$i, TRUE);
					$this->mmaster->deletedetailekspedisi($ibapb,$iarea1,$iekspedisi);
				  $this->mmaster->insertdetailekspedisi($ibapb,$iarea1,$iekspedisi,$dbapb,$eremark);
				}				
				if ( ($this->db->trans_status() === FALSE) )
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();
					$data['sukses']			= true;
					$data['inomor']			= $ibapb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispmb	= $this->input->post('ispmbdelete', TRUE);
			$this->load->model('bapb/mmaster');
			$this->mmaster->delete($ispmb);
			$data['page_title'] = $this->lang->line('bapb');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('bapb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibapb		= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
  		$isj			= $this->uri->segment(6);
  		$dfrom		= $this->uri->segment(7);
  		$dto			= $this->uri->segment(8);
  		$icustomer= $this->uri->segment(9);
			$this->db->trans_begin();
      $area1	= $this->session->userdata('i_area');
      if($area1=='00'){
        $daer='f';
      }else{
        $daer='t';
      }
			$this->load->model('bapb/mmaster');
			$this->mmaster->deletedetail($ibapb, $iarea, $isj, $daer);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
				$data['page_title'] = $this->lang->line('bapb')." update";
				$query = $this->db->query("select * from tm_bapb_item where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ibapb'] 		= $ibapb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				$data['icustomer']= $icustomer;
				$query = $this->db->query("select * from tm_bapb_ekspedisi where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows(); 				
				$this->load->model('bapb/mmaster');
				$data['isi']=$this->mmaster->baca($ibapb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ibapb,$iarea);
				$data['detailx']=$this->mmaster->bacadetailx($ibapb,$iarea);
		 		$this->load->view('bapb/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetailekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibapb			= $this->uri->segment(4);
			$iarea			= $this->uri->segment(5);
  		$iekspedisi	= $this->uri->segment(6);
  		$dfrom			= $this->uri->segment(7);
  		$dto				= $this->uri->segment(8);
  		$icustomer	= $this->uri->segment(9);
			$this->db->trans_begin();
			$this->load->model('bapb/mmaster');
			$this->mmaster->deletedetailekspedisi($ibapb, $iarea, $iekspedisi);
			if ($this->db->trans_status() === FALSE)
			{
			  $this->db->trans_rollback();
			}else{
			   $this->db->trans_commit();
				$data['page_title'] = $this->lang->line('bapb')." update";
				$query = $this->db->query("select * from tm_bapb_item where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ibapb'] 		= $ibapb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				$data['icustomer']= $icustomer;
				$query = $this->db->query("select * from tm_bapb_ekspedisi where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows(); 				
				$this->load->model('bapb/mmaster');
				$data['isi']=$this->mmaster->baca($ibapb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ibapb,$iarea);
				$data['detailx']=$this->mmaster->bacadetailx($ibapb,$iarea);
		 		$this->load->view('bapb/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function sj()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['area']=$this->uri->segment(5);
			$area=$this->uri->segment(5);
			if($this->uri->segment(6) && $this->uri->segment(6)!='index'){
				$data['customer']=$this->uri->segment(6);
				$icustomer=$this->uri->segment(6);
			}else{
				$icustomer='';
			}
			$data['icustomer']=$icustomer;
			$config['base_url'] = base_url().'index.php/bapb/cform/sj/'.$baris.'/'.$area.'/index/';
      $area1	= $this->session->userdata('i_area');
			if($this->uri->segment(6) && $this->uri->segment(6)!='index'){			
			  if($area1=='00'){
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											  where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											  and a.i_customer='$icustomer'
                        and a.i_bapb isnull
											  and a.i_customer=b.i_customer
                        and a.f_sj_daerah='f' ",false);
#											  and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
        }else{
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											  where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											  and a.i_customer='$icustomer'
                        and a.i_bapb isnull
											  and a.i_customer=b.i_customer
                        and a.f_sj_daerah='t' ",false);
#											  and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
        }
			}else{
			  if($area1=='00'){
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											  where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
                        and a.i_bapb isnull
											  and a.i_customer=b.i_customer
                        and a.f_sj_daerah='f' ",false);
#											  and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
        }else{
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											  where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
                        and a.i_bapb isnull
											  and a.i_customer=b.i_customer
                        and a.f_sj_daerah='t' ",false);
#											  and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
        }
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			if(isset($icustomer) && ($icustomer!='')){
				$data['isi']=$this->mmaster->bacasj($icustomer,$area,$config['per_page'],$this->uri->segment(7));
			}else{
				$data['isi']=$this->mmaster->bacasj2($area,$config['per_page'],$this->uri->segment(7));
			}
			$data['baris']=$baris;
			$this->load->view('bapb/vlistsj', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisj()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      			$area1	= $this->session->userdata('i_area');
			$baris		= $this->input->post('baris', TRUE);
			$area			= $this->input->post('iarea', TRUE);
			$icustomer	= $this->input->post('icustomer', TRUE);

			if($icustomer!=''){
				$data['customer']=$icustomer;
			}else{
				$icustomer='';
			}
			$data['icustomer']=$icustomer;

			$config['base_url'] = base_url().'index.php/bapb/cform/sj/'.$baris.'/'.$area.'/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
#			$query = $this->db->query("	select a.i_sj from tm_sj a, tr_customer b 
#							where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
#							and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
#							and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_customer) like '%$cari%')
#							and a.i_customer=b.i_customer ",false);
			if($icustomer!=''){			
			  if($area1=='00'){
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											                where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											                and a.i_customer='$icustomer'
                                      and a.i_bapb isnull
											                and a.i_customer=b.i_customer 
											                and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_customer) like '%$cari%')
											                and a.i_customer=b.i_customer ",false);
#											                and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
        }else{
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											                where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											                and a.i_customer='$icustomer'
                                      and a.i_bapb isnull
											                and a.i_customer=b.i_customer 
											                and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_customer) like '%$cari%')
											                and a.i_customer=b.i_customer ",false);
#											                and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
        }
			}else{
			  if($area1=='00'){
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											                where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											                and a.i_customer=b.i_customer 
                                      and a.i_bapb isnull
											                and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_customer) like '%$cari%')
											                and a.i_customer=b.i_customer ",false);
#											                and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
        }else{
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											                where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											                and a.i_customer=b.i_customer 
                                      and a.i_bapb isnull
											                and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_customer) like '%$cari%')
											                and a.i_customer=b.i_customer ",false);
#											                and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
        }
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->carisj($icustomer,$area,$cari,$config['per_page'],$this->uri->segment(7));
			$data['area']=$area;
			$data['baris']=$baris;
			$this->load->view('bapb/vlistsj', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/bapb/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('bapb/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('bapb/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bapb/cform/area/index/';
		      	$area1	= $this->session->userdata('i_area');
		        $area2	= $this->session->userdata('i_area2');
		        $area3	= $this->session->userdata('i_area3');
		        $area4	= $this->session->userdata('i_area4');
		        $area5	= $this->session->userdata('i_area5');
		        if($area1=='00')
		        {
					  $query = $this->db->query("select * from tr_area",false);
		        }else{
					  $query = $this->db->query("select * from tr_area 
				                   where (upper(i_area) = '$area1' or upper(i_area) = '$area2' or upper(i_area) = '$area3' or upper(i_area) = '$area4' 
				                   or upper(i_area) = '$area5')",false);
		        }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('bapb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bapb/cform/area/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
		        $area1	= $this->session->userdata('i_area');
		        $area2	= $this->session->userdata('i_area2');
		        $area3	= $this->session->userdata('i_area3');
		        $area4	= $this->session->userdata('i_area4');
		        $area5	= $this->session->userdata('i_area5');
		        if($area1=='00')
		        {
					  $query = $this->db->query("select * from tr_area
			      						   where upper(e_area_name) like '%$cari%'",false);
		        }else{
					  $query = $this->db->query("select * from tr_area
			      						   where upper(e_area_name) like '%$cari%' and (upper(i_area) = '$area1' or upper(i_area) = '$area2' or upper(i_area) = '$area3' 
				             or upper(i_area) = '$area4' or upper(i_area) = '$area5')",false);
		        }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('bapb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kirim()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bapb/cform/kirim/index/';
			$query = $this->db->query("select * from tr_dkb_kirim ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('listkirim');
			$data['isi']=$this->mmaster->bacakirim($config['per_page'],$this->uri->segment(5));
			$this->load->view('bapb/vlistkirim', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carikirim()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bapb/cform/kirim/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_dkb_kirim
						   where upper(i_dkb_kirim) like '%$cari%' 
						      or upper(e_dkb_kirim) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('listkirim');
			$data['isi']=$this->mmaster->carikirim($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('bapb/vlistkirim', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function via()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bapb/cform/via/index/';
			$query = $this->db->query("select * from tr_dkb_via ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('listvia');
			$data['isi']=$this->mmaster->bacavia($config['per_page'],$this->uri->segment(5));
			$this->load->view('bapb/vlistvia', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carivia()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bapb/cform/via/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_dkb_via
						   where upper(i_dkb_via) like '%$cari%' 
						      or upper(e_dkb_via) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('listvia');
			$data['isi']=$this->mmaster->carivia($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('bapb/vlistvia', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function ekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bapb/cform/ekspedisi/'.$baris.'/';
			$query = $this->db->query("select * from tr_ekspedisi ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('listekspedisi');
			$data['isi']=$this->mmaster->bacaekspedisi($config['per_page'],$this->uri->segment(5));
			$data['baris']=$baris;
			$this->load->view('bapb/vlistekspedisi', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris	=	$this->input->post('baris', FALSE);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/bapb/cform/ekspedisi/'.$baris.'/';
			$query = $this->db->query("select * from tr_ekspedisi
																 where upper(i_ekspedisi) like '%$cari%' 
																		or upper(e_ekspedisi) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('listekspedisi');
			$data['isi']=$this->mmaster->cariekspedisi($cari,$config['per_page'],$this->uri->segment(5));
			$data['baris']=$baris;
			$this->load->view('bapb/vlistekspedisi', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bapb/cform/customer/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_customer
									   where i_area = '$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('bapb/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/bapb/cform/customer/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$iarea = $this->uri->segment(4);
			$query = $this->db->query("select * from tr_customer
						   where i_area='$iarea' 
							 and (upper(i_customer) like '%$cari%' 
						      	  or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
			$this->load->view('bapb/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
/*
	function sjupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['area']=$this->uri->segment(5);
			$area=$this->uri->segment(5);
			if($this->uri->segment(6) && $this->uri->segment(6)!='index'){
				$data['customer']=$this->uri->segment(6);
				$icustomer=$this->uri->segment(6);
			}else{
				$icustomer='';
			}
			$data['icustomer']=$icustomer;
			$config['base_url'] = base_url().'index.php/bapb/cform/sjupdate/'.$baris.'/'.$area.'/index/';
			if($this->uri->segment(6) && $this->uri->segment(6)!='index'){			
				$query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											and a.i_customer='$icustomer'
											and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
											and a.i_customer=b.i_customer ",false);
			}else{
				$query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
											and a.i_customer=b.i_customer ",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			if(isset($icustomer)){
				$data['isi']=$this->mmaster->bacasj($icustomer,$area,$config['per_page'],$this->uri->segment(7));
			}else{
				$data['isi']=$this->mmaster->bacasj2($area,$config['per_page'],$this->uri->segment(7));
			}
			$data['baris']=$baris;
			$this->load->view('bapb/vlistsjupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisjupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris		= $this->input->post('baris', TRUE);
			$area			= $this->input->post('iarea', TRUE);
			$icustomer	= $this->input->post('icustomer', TRUE);
			
			if($icustomer!=''){
				$data['customer']=$icustomer;
			}else{
				$icustomer='';
			}
			$data['icustomer']=$icustomer;

			$config['base_url'] = base_url().'index.php/bapb/cform/sjupdate/'.$baris.'/'.$area.'/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			if($icustomer!=''){			
				$query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											and a.i_customer='$icustomer'
											and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
											and a.i_customer=b.i_customer 
											and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
											and a.i_customer=b.i_customer ",false);

			}else{
				$query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
											and a.i_customer=b.i_customer 
											and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
											and a.i_customer=b.i_customer ",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->carisj($icustomer,$area,$cari,$config['per_page'],$this->uri->segment(7));
			$data['area']=$area;
			$data['baris']=$baris;
			$this->load->view('bapb/vlistsjupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
*/
	function sjupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['area']=$this->uri->segment(5);
			$area=$this->uri->segment(5);
			if($this->uri->segment(6) && $this->uri->segment(6)!='index'){
				$data['customer']=$this->uri->segment(6);
				$icustomer=$this->uri->segment(6);
			}else{
				$icustomer='';
			}
			$data['icustomer']=$icustomer;
			$config['base_url'] = base_url().'index.php/bapb/cform/sj/'.$baris.'/'.$area.'/index/';
      $area1	= $this->session->userdata('i_area');
			if($this->uri->segment(6) && $this->uri->segment(6)!='index'){			
			  if($area1=='00'){
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											  where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											  and a.i_customer='$icustomer'
                        and a.i_bapb isnull
											  and a.i_customer=b.i_customer
                        and a.f_sj_daerah='f' ",false);
#											  and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
        }else{
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											  where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											  and a.i_customer='$icustomer'
                        and a.i_bapb isnull
											  and a.i_customer=b.i_customer
                        and a.f_sj_daerah='t' ",false);
#											  and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
        }
			}else{
			  if($area1=='00'){
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											  where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
                        and a.i_bapb isnull
											  and a.i_customer=b.i_customer
                        and a.f_sj_daerah='f' ",false);
#											  and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
        }else{
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											  where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
                        and a.i_bapb isnull
											  and a.i_customer=b.i_customer
                        and a.f_sj_daerah='t' ",false);
#											  and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
        }
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			if(isset($icustomer) && ($icustomer!='')){
				$data['isi']=$this->mmaster->bacasj($icustomer,$area,$config['per_page'],$this->uri->segment(7));
			}else{
				$data['isi']=$this->mmaster->bacasj2($area,$config['per_page'],$this->uri->segment(7));
			}
			$data['baris']=$baris;
			$this->load->view('bapb/vlistsjupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisjupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	= $this->session->userdata('i_area');
			$baris		= $this->input->post('baris', TRUE);
			$area			= $this->input->post('iarea', TRUE);
			$icustomer	= $this->input->post('icustomer', TRUE);

			if($icustomer!=''){
				$data['customer']=$icustomer;
			}else{
				$icustomer='';
			}
			$data['icustomer']=$icustomer;

			$config['base_url'] = base_url().'index.php/bapb/cform/sj/'.$baris.'/'.$area.'/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
#			$query = $this->db->query("	select a.i_sj from tm_sj a, tr_customer b 
#							where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
#							and a.i_sj not in(select i_sj from tm_bapb_item where i_area='$area')
#							and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
#							and a.i_customer=b.i_customer ",false);
			if($icustomer!=''){			
			  if($area1=='00'){
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											                where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											                and a.i_customer='$icustomer'
                                      and a.i_bapb isnull
											                and a.i_customer=b.i_customer 
											                and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
											                and a.i_customer=b.i_customer ",false);
        }else{
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											                where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											                and a.i_customer='$icustomer'
                                      and a.i_bapb isnull
											                and a.i_customer=b.i_customer 
											                and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
											                and a.i_customer=b.i_customer ",false);
        }
			}else{
			  if($area1=='00'){
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											                where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											                and a.i_customer=b.i_customer 
                                      and a.i_bapb isnull
											                and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
											                and a.i_customer=b.i_customer ",false);
        }else{
				  $query = $this->db->query(" select a.i_sj from tm_sj a, tr_customer b 
											                where a.i_nota isnull and a.i_sj_type='04' and a.i_area_from='$area' 
											                and a.i_customer=b.i_customer 
                                      and a.i_bapb isnull
											                and (upper(a.i_sj) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
											                and a.i_customer=b.i_customer ",false);
        }
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->carisj($icustomer,$area,$cari,$config['per_page'],$this->uri->segment(7));
			$data['area']=$area;
			$data['baris']=$baris;
			$this->load->view('bapb/vlistsjupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function ekspedisiupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bapb/cform/ekspedisiupdate/'.$baris.'/';
			$query = $this->db->query("select * from tr_ekspedisi ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('listekspedisi');
			$data['isi']=$this->mmaster->bacaekspedisi($config['per_page'],$this->uri->segment(5));
			$data['baris']=$baris;
			$this->load->view('bapb/vlistekspedisiupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariekspedisiupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu172')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris	=	$this->input->post('baris', FALSE);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/bapb/cform/ekspedisiupdate/'.$baris.'/';
			$query = $this->db->query("select * from tr_ekspedisi
																 where upper(i_ekspedisi) like '%$cari%' 
																		or upper(e_ekspedisi) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bapb/mmaster');
			$data['page_title'] = $this->lang->line('listekspedisi');
			$data['isi']=$this->mmaster->cariekspedisi($cari,$config['per_page'],$this->uri->segment(5));
			$data['baris']=$baris;
			$this->load->view('bapb/vlistekspedisiupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
