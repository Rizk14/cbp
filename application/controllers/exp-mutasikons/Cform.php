<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu369') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('exp-mutasikons');
			$data['iperiode']	  = '';
			$data['icustomer']  = '';
			$this->load->view('exp-mutasikons/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu369') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari	= strtoupper($this->input->post('cari'));
			$area1	= $this->session->userdata('i_area');
			if ($cari == '' && $this->uri->segment(4) != 'zxasqw') $cari = $this->uri->segment(4);
			if ($cari == '') {
				$config['base_url'] = base_url() . 'index.php/exp-mutasikons/cform/customer/zxasqw/';
			} else {
				$config['base_url'] = base_url() . 'index.php/exp-mutasikons/cform/customer/' . $cari . '/';
			}
			if ($area1 == 'PB' || $area1 == '00') {
				$query = $this->db->query(" select b.i_customer from tr_spg a, tr_customer b
                                    where a.i_customer=b.i_customer
                                    and (upper(a.i_customer)like'%$cari%' or upper(b.e_customer_name)like'%$cari%')", false);
			} else {
				$query = $this->db->query(" select a.*, b.e_customer_name from tr_spg a, tr_customer b
                                    where a.i_customer=b.i_customer and a.i_area='$area1'
                                    and (upper(a.i_customer)like'%$cari%' or upper(b.e_customer_name)like'%$cari%')", FALSE);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-mutasikons/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi'] = $this->mmaster->bacacustomer($config['per_page'], $this->uri->segment(5), $cari);
			$this->load->view('exp-mutasikons/vlistcustomer', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu369') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-mutasikons/cform/customer/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$query 	= $this->db->query("select a.*, b.e_customer_name from tr_spg a, tr_customer b
                          where a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                          or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
						              order by a.i_customer", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-mutasikons/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi'] = $this->mmaster->caricustomer($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-mutasikons/vlistcustomer', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu369') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-mutasikons/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$icustomer = $this->input->post('icustomer');
			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}
			define('BOOLEAN_FIELD',   'L');
			define('CHARACTER_FIELD', 'C');
			define('DATE_FIELD',      'D');
			define('NUMBER_FIELD',    'N');
			define('READ_ONLY',  '0');
			define('WRITE_ONLY', '1');
			define('READ_WRITE', '2');

			$a = substr($iperiode, 0, 4);
			$b = substr($iperiode, 4, 2);
			$peri = mbulan($b) . " - " . $a;

			if ($icustomer == '') $icustomer = $this->uri->segment(5);
			$query = $this->db->query("select a.i_customer, a.i_area, b.i_spg, a.e_customer_name
                                 from tr_customer a, tr_spg b
                                 where a.i_customer=b.i_customer and a.i_area=b.i_area
                                 and a.i_customer='$icustomer'");
			$st = $query->row();
			$custname = $st->e_customer_name;
			$iarea = $st->i_area;

			$db_file = 'spb/' . $iarea . '/gd' . $icustomer . $iperiode . '.dbf';
			if (file_exists($db_file)) {
				@chmod($db_file, 0777);
				@unlink($db_file);
			}

			//   Untuk SO
			if ($iperiode > '201512') {
				$this->db->select(" a.i_product, a.i_product_grade, sum(a.n_saldo_awal) as n_saldo_awal, sum(a.n_mutasi_daripusat) as n_mutasi_daripusat, 
									sum(a.n_mutasi_darilang) as n_mutasi_darilang, sum(a.n_mutasi_kepusat) as n_mutasi_kepusat, 
									sum(a.n_mutasi_penjualan) as n_mutasi_penjualan, sum(a.n_saldo_akhir) as n_saldo_akhir, 
									a.e_mutasi_periode, a.i_customer, sum(a.n_saldo_stockopname) as n_saldo_stockopname,
									a.e_product_name, a.e_customer_name, b.v_product_retail	                      
									from f_mutasi_stock_mo_cust_all_saldoakhir('$iperiode') a
									left join tr_product_price b on (a.i_product=b.i_product)
									where i_customer='$icustomer' and b.i_price_group='00'
									group by a.i_product, a.e_mutasi_periode, a.i_customer, a.e_product_name, a.e_customer_name, b.v_product_retail, a.i_product_grade
									order by a.i_product, a.e_product_name, a.e_mutasi_periode, a.i_customer, a.e_customer_name", false); #->limit($num,$offset);      
			} else {
				$this->db->select("	* from f_mutasi_stock_mo_cust('$iperiode','$icustomer')", false); #->limit($num,$offset);
			}

			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:P6'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'KODEPROD');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'NAMAPROD');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'STOCKOPNAM');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'GRADE');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i = 2;
				$j = 2;
				$xarea = '';
				$saldo = 0;
				$no = 0;
				$group = '';
				foreach ($query->result() as $row) {
					$no++;
					$saldoawal = '00';
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':D' . $i
					);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->i_product, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->e_product_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $saldoawal, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_product_grade, Cell_DataType::TYPE_STRING);
					$i++;
					$j++;
				}
				//die();
				$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
				$nama = 'so' . $icustomer . $iperiode . '.xls';
				if (file_exists('spb/' . $iarea . '/' . $nama)) {
					@chmod('spb/' . $iarea . '/' . $nama, 0777);
					@unlink('spb/' . $iarea . '/' . $nama);
				}
				$objWriter->save("spb/" . $iarea . '/' . $nama);

				$data['sukses'] = true;
				$data['folder'] = 'spb/' . $iarea;
				$data['inomor']	= $nama; /* "Laporan Mutasi Stok" */

				@chmod('spb/' . $iarea . '/' . $nama, 0777);
				// @chmod($db_file, 0777);
				$this->load->view('nomorurl', $data);
			}

			// UNTUK MUTASI
			if ($iperiode > '201512') {
				$this->db->select(" a.i_product,  sum(a.n_saldo_awal) as n_saldo_awal, sum(a.n_mutasi_daripusat) as n_mutasi_daripusat, 
		                        sum(a.n_mutasi_darilang) as n_mutasi_darilang, sum(a.n_mutasi_kepusat) as n_mutasi_kepusat, 
		                        sum(a.n_mutasi_penjualan) as n_mutasi_penjualan, sum(a.n_saldo_akhir) as n_saldo_akhir, 
		                        a.e_mutasi_periode, a.i_customer, sum(a.n_saldo_stockopname) as n_saldo_stockopname,
		                        a.e_product_name, a.e_customer_name, b.v_product_retail	                      
		                        from f_mutasi_stock_mo_cust_all_saldoakhir('$iperiode') a
		                        left join tr_product_price b on (a.i_product=b.i_product)
		                        where i_customer='$icustomer' and b.i_price_group='00'
		                        group by a.i_product, a.e_mutasi_periode, a.i_customer, a.e_product_name, a.e_customer_name, b.v_product_retail
		                        order by a.i_product, a.e_product_name, a.e_mutasi_periode, a.i_customer, a.e_customer_name", false); #->limit($num,$offset);      
			} else {
				$this->db->select("	* from f_mutasi_stock_mo_cust('$iperiode','$icustomer')", false); #->limit($num,$offset);
			}
			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan Mutasi Konsinyasi")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(6);
				// $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(6);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN MUTASI STOK KONSINYASI');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 11, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Customer : ' . $custname . '     Bulan : ' . $peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 11, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:L6'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, 5, 6, 5);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Dari');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, 5, 6, 5);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Dari');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, 5, 6, 5);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'PENGELUARAN');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, 5, 6, 5);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Ke');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Sld');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Sld');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Selisih');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Urut');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Barang');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Barang');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Awal');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Pusat');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Lang');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Penjualan');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Pusat');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Akhir');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Opname');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K6', '');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				// $objPHPExcel->getActiveSheet()->setCellValue('L6', 'GiT');
				// $objPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
				// 	array(
				// 		'borders' => array(
				// 			'bottom'=> array('style' => Style_Border::BORDER_THIN)
				// 		)
				// 	)
				// );
				$objPHPExcel->getActiveSheet()->setCellValue('L6', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

				$i = 7;
				$j = 7;
				$xarea = '';
				$saldo = 0;
				$no = 0;

				$prodtemp = '';
				foreach ($query->result() as $row) {
					if ($prodtemp != $row->i_product) {
						$no++;
						$selisih = $row->n_saldo_stockopname - $row->n_saldo_akhir;
						$objPHPExcel->getActiveSheet()->duplicateStyleArray(
							array(
								'font' => array(
									'name'	=> 'Arial',
									'bold'  => false,
									'italic' => false,
									'size'  => 10
								)
							),
							'A' . $i . ':L' . $i
						);


						$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $no);
						$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->i_product);
						$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->e_product_name);
						$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->n_saldo_awal);
						$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $row->n_mutasi_daripusat);
						$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $row->n_mutasi_darilang);
						$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row->n_mutasi_penjualan);
						$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->n_mutasi_kepusat);
						$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->n_saldo_akhir);
						$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->n_saldo_stockopname);
						$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $selisih);
						// $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row->n_mutasi_git);
						$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row->v_product_retail);
						$i++;
						$j++;

						$isi = array($row->i_product, $row->e_product_name, 0);
					}
					$prodtemp = $row->i_product;
				}
				// @chmod($db_file, 0777);
				$x = $i - 1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama = 'gd' . $icustomer . $iperiode . '.xls';
			if (file_exists('spb/' . $iarea . '/' . $nama)) {
				@chmod('spb/' . $iarea . '/' . $nama, 0777);
				@unlink('spb/' . $iarea . '/' . $nama);
			}
			$objWriter->save("spb/" . $iarea . '/' . $nama);
			// @chmod('spb/' . $iarea . '/' . $nama, 0777);

			$this->logger->writenew('Export Mutasi Konsinyasi Periode:' . $iperiode . ' Customer:' . $icustomer);

			// $data['sukses'] = true;
			// $data['inomor']	= "Laporan Mutasi Stok Konsinyasi -" . $db_file;
			// $data['folder'] = "exp-mutasikons";
			// $this->load->view('nomorexport', $data);

			$data['sukses'] = true;
			$data['folder'] = 'spb/' . $iarea;
			$data['inomor']	= $nama; /* "Laporan Mutasi Stok" */

			@chmod('spb/' . $daerah . '/' . $nama, 0777);
			// @chmod($db_file, 0777);
			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
