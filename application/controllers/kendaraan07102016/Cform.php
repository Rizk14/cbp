<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kendaraan');
			$this->load->model('kendaraan/mmaster');
			$data['ikendaraan']='';
			$data['iperiode']='';
			$this->load->view('kendaraan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kendaraan');
			$this->load->view('kendaraan/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kendaraan')." update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5)
			  ){
				$data['iperiode'] 	= $this->uri->segment(4);
				$data['ikendaraan']	= str_replace('%20',' ',$this->uri->segment(5));
				$iperiode 			= $this->uri->segment(4);
				$ikendaraan			= str_replace('%20',' ',$this->uri->segment(5));
				$this->load->model("kendaraan/mmaster");
				$data['isi']=$this->mmaster->baca($iperiode,$ikendaraan);
		 		$this->load->view('kendaraan/vformupdate',$data);
			}else{
				$this->load->view('kendaraan/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode		= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$ikendaraan		= $this->input->post('ikendaraan', TRUE);
			$ikendaraan		= strtoupper($ikendaraan);
			$iarea			= $this->input->post('iarea', TRUE);
			$ikendaraanjenis= $this->input->post('ikendaraanjenis', TRUE);
			$ikendaraanbbm	= $this->input->post('ikendaraanbbm', TRUE);
			$epengguna		= $this->input->post('epengguna', TRUE);
			$dpajak			= $this->input->post('dpajak', TRUE);
			if($dpajak!=''){
				$tmp=explode("-",$dpajak);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpajak=$th."-".$bl."-".$hr;
			}else{
        $dpajak=null;
      }
			if (
				(isset($iperiode) && $iperiode != '') && 
				(isset($ikendaraan) && $ikendaraan != '') 
			   )
			{
				$this->load->model('kendaraan/mmaster');
				$this->db->trans_begin();
				$cek=$this->mmaster->cek($iperiode,$ikendaraan);
				if($cek){							
					$this->mmaster->update($iperiode,$ikendaraan,$iarea,$ikendaraanjenis,$ikendaraanbbm,$epengguna,$dpajak);
					$nomor=$ikendaraan." - ".$iperiode." - ".$epengguna;
					if ($this->db->trans_status() === FALSE)
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
	          while($row=pg_fetch_assoc($query)){
	          	$now	  = $row['c'];
			      }
			      $pesan='Update Kendaraan Plat:'.$ikendaraan.' Periode:'.$iperiode.' Area:'.$iarea;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan ); 

						$data['sukses']			= true;
						$data['inomor']			= $nomor;
						$this->load->view('nomor',$data);
					}
				}else{
					$nomor="Periode ".$iperiode." - ".$ikendaraan." sudah ada, untuk mengubah lewat menu update kendaraan";
				}
			}

		}else{
			$this->load->view('awal/index.php');

		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kendaraan/cform/area/index/';
			$query = $this->db->query("select * from tr_area",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('kendaraan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('kendaraan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kendaraan/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_area
									   	 where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kendaraan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('kendaraan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function jeniskendaraan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kendaraan/cform/jeniskendaraan/index/';
			$query = $this->db->query("select * from tr_kendaraan_jenis",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('kendaraan/mmaster');
			$data['page_title'] = $this->lang->line('list_jeniskendaraan');
			$data['isi']=$this->mmaster->bacajeniskendaraan($config['per_page'],$this->uri->segment(5));
			$this->load->view('kendaraan/vlistjeniskendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carijeniskendaraan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kendaraan/cform/jeniskendaraan/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_kendaraan_jenis
									   	 where (upper(i_kendaraan_jenis) like '%$cari%' or upper(e_kendaraan_jenis) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kendaraan/mmaster');
			$data['page_title'] = $this->lang->line('list_jeniskendaraan');
			$data['isi']=$this->mmaster->carijeniskendaraan($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('kendaraan/vlistjeniskendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bbmkendaraan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kendaraan/cform/bbmkendaraan/index/';
			$query = $this->db->query("select * from tr_kendaraan_bbm",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('kendaraan/mmaster');
			$data['page_title'] = $this->lang->line('list_bbmkendaraan');
			$data['isi']=$this->mmaster->bacabbmkendaraan($config['per_page'],$this->uri->segment(5));
			$this->load->view('kendaraan/vlistbbmkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caribbmkendaraan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/kendaraan/cform/bbmkendaraan/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_kendaraan_bbm
									   	 where (upper(i_kendaraan_bbm) like '%$cari%' or upper(e_kendaraan_bbm) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('kendaraan/mmaster');
			$data['page_title'] = $this->lang->line('list_bbmkendaraan');
			$data['isi']=$this->mmaster->caribbmkendaraan($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('kendaraan/vlistbbmkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode		= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$ikendaraan		= $this->input->post('ikendaraan', TRUE);
			$ikendaraan		= strtoupper($ikendaraan);
			$iarea			= $this->input->post('iarea', TRUE);
			$ikendaraanjenis= $this->input->post('ikendaraanjenis', TRUE);
			$ikendaraanbbm	= $this->input->post('ikendaraanbbm', TRUE);
			$epengguna		= $this->input->post('epengguna', TRUE);
			$dpajak			= $this->input->post('dpajak', TRUE);
			if($dpajak!=''){
				$tmp=explode("-",$dpajak);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dpajak=$th."-".$bl."-".$hr;
			}else{
        $dpajak=null;
      }
			if (
				(isset($iperiode) && $iperiode != '') && 
				(isset($ikendaraan) && $ikendaraan != '') 
			   )
			{
				$this->load->model('kendaraan/mmaster');
				$this->db->trans_begin();
				$cek=$this->mmaster->cek($iperiode,$ikendaraan);
				if(!$cek){							
					$this->mmaster->insert($iperiode,$ikendaraan,$iarea,$ikendaraanjenis,$ikendaraanbbm,$epengguna,$dpajak);
					$nomor=$ikendaraan." - ".$iperiode." - ".$epengguna;
					if ($this->db->trans_status() === FALSE)
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
	          while($row=pg_fetch_assoc($query)){
	          	$now	  = $row['c'];
			      }
			      $pesan='Input Kendaraan Plat:'.$ikendaraan.' Periode:'.$iperiode.' Area:'.$iarea;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan ); 

						$data['sukses']			= true;
						$data['inomor']			= $nomor;
						$this->load->view('nomor',$data);
					}
				}else{
					$nomor="Periode ".$iperiode." - ".$ikendaraan." sudah ada, untuk mengubah lewat menu update kendaraan";
				}
			}

		}else{
			$this->load->view('awal/index.php');

		}
	}
}
?>
