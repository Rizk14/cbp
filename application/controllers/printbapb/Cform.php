<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu139')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $iuser   = $this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/printbapb/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			
			$query = $this->db->query(" select a.i_bapb from tm_bapb a 
										left join tr_customer b on (a.i_customer=b.i_customer and a.i_area=b.i_area) 
										inner join tr_area c on (a.i_area=c.i_area) where 
										(upper(a.i_customer) like '%$cari%' or upper(a.i_bapb) like '%$cari%') and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
										",false);
													
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printbapb');
			$this->load->model('printbapb/mmaster');
			$data['ibapb']='';
			$data['cari']=$cari;
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4),$iuser);
			$this->load->view('printbapb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu139')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ibapb= str_replace("%20"," ",$this->uri->segment(4));
			$area = $this->uri->segment(5);
			$this->load->model('printbapb/mmaster');
			$data['ibapb']=$ibapb;
			$data['page_title'] = $this->lang->line('printbapb');
			$data['isi']=$this->mmaster->baca($ibapb,$area);
			$data['detail']='';
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$data['iarea']	= $area;
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak BAPB Area '.$area.' No:'.$ibapb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printbapb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu139')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printbapb');
			$this->load->view('printbapb/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu139')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printbapb/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
      $iuser   = $this->session->userdata('user_id');
      
			/* Disabled 12042011			
			$query = $this->db->query(" select a.i_bapb from tm_bapb a, tr_customer b, tr_area c
										              where a.i_customer=b.i_customer and a.i_area=c.i_area
										              and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										              or upper(a.i_bapb) like '%$cari%') and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3' or a.i_area = '$area4' or a.i_area = '$area5') ",false);
			*/ 
													
/*			$query = $this->db->query(" select a.i_bapb from tm_bapb a 
										left join tr_customer b on (a.i_customer=b.i_customer and a.i_area=b.i_area) 
										inner join tr_area c on (a.i_area=c.i_area) where (upper(a.i_customer) like '%$cari%' or upper(a.i_bapb) like '%$cari%') 
										a.i_area in ( select i_area from tm_user_area where i_user='$iuser') ",false);			*/

			$query = $this->db->query(" select a.i_bapb from tm_bapb a 
										left join tr_customer b on (a.i_customer=b.i_customer and a.i_area=b.i_area) 
										inner join tr_area c on (a.i_area=c.i_area) where 
										(upper(a.i_customer) like '%$cari%' or upper(a.i_bapb) like '%$cari%') and a.i_area in ( select i_area from tm_user_area where i_user='$iuser')
										",false);
										
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printbapb');
			$this->load->model('printbapb/mmaster');
			$data['ibapb']='';
			$data['cari']=$cari;
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4),$iuser);
			$this->load->view('printbapb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
