<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu370')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$user=$this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/terimaspb/cform/index/';
			$cari = ($this->input->post('cari', FALSE));
			$query = $this->db->query(" 	select * from tm_spb 
                                    inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
                                    inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman) 
                                    inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer) 
                                    inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group) 
                                    where 
                                    ((tm_spb.f_spb_stockdaerah='f'
                                    and not tm_spb.i_approve1 is null
                                    and not tm_spb.i_approve2 is null
                                    and tm_spb.i_store isnull 
                                    and tm_spb.i_store_location isnull
                                    and tm_spb.d_spb_storereceive isnull)
                                    or
                                    (tm_spb.f_spb_stockdaerah='t'
                                    and tm_spb.d_spb_storereceive isnull))
                                    and tm_spb.f_spb_cancel='f'
                                    and tm_spb.i_nota is null
                                    and (upper(tm_spb.i_spb) ilike '%$cari%' or upper(tr_customer.e_customer_name) ilike '%$cari%')
                                    and tm_spb.i_area in(select i_area from tm_user_area where i_user='$user')
                                    order by tm_spb.i_area, tm_spb.i_spb",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['ada']	= 'xx';
      $data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('terimaspb');
			$this->load->model('terimaspb/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4),$user);
			$this->load->view('terimaspb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function closing()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu370')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml = $this->input->post('jml', TRUE);
			$this->db->trans_begin();
			$this->load->model('terimaspb/mmaster');
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$ispb = $this->input->post('ispb'.$i, TRUE);
					$iarea= $this->input->post('iarea'.$i, TRUE);
					$this->mmaster->updatespb($ispb, $iarea);
				}
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Gudang Terima SPB No:'.$ispb.' Area:'.$iarea;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );

			}
			$user=$this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/terimaspb/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));

			$query = $this->db->query("   select * from tm_spb 
                                    inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
                                    inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman) 
                                    inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer) 
                                    inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group) 
                                    where 
                                    ((tm_spb.f_spb_stockdaerah='f'
                                    and not tm_spb.i_approve1 is null
                                    and not tm_spb.i_approve2 is null
                                    and tm_spb.i_store isnull 
                                    and tm_spb.i_store_location isnull
                                    and tm_spb.d_spb_storereceive isnull)
                                    or
                                    (tm_spb.f_spb_stockdaerah='t'
                                    and tm_spb.d_spb_storereceive isnull))
                                    and tm_spb.f_spb_cancel='f'
                                    and tm_spb.i_nota is null
                                    and (upper(tm_spb.i_spb) like '%$cari%' or upper(tm_spb.i_customer) like '%$cari%')
                                    and tm_spb.i_area in(select i_area from tm_user_area where i_user='$user')
                                    order by tm_spb.i_area, tm_spb.i_spb",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['ada']	= 'xx';
      $data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('terimaspb');
			$this->load->model('terimaspb/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('terimaspb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu370')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('terimaspb');
			$this->load->view('terimaspb/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
	    	($this->session->userdata('menu370')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$user=$this->session->userdata('user_id');
			/*$config['base_url'] = base_url().'index.php/terimaspb/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));*/

			$cari=($this->input->post("cari",false));
          	if($cari=='')$cari=$this->uri->segment(4);
         	if($cari=='zxvf'){
            	$cari='';
            	$config['base_url'] = base_url().'index.php/terimaspb/cform/cari/zxvf/';
          	}else{
            	$config['base_url'] = base_url().'index.php/terimaspb/cform/cari/'.$cari.'/';
          	}

			$query = $this->db->query("   select * from tm_spb 
                          inner join tr_customer on (tm_spb.i_customer=tr_customer.i_customer)
                          inner join tr_salesman on (tm_spb.i_salesman=tr_salesman.i_salesman) 
                          inner join tr_customer_area on (tm_spb.i_customer=tr_customer_area.i_customer) 
                          inner join tr_price_group on (tm_spb.i_price_group=tr_price_group.i_price_group) 
                          where 
                          ((tm_spb.f_spb_stockdaerah='f'
                          and not tm_spb.i_approve1 is null
                          and not tm_spb.i_approve2 is null
                          and tm_spb.i_store isnull 
                          and tm_spb.i_store_location isnull
                          and tm_spb.d_spb_storereceive isnull)
                          or
                          (tm_spb.f_spb_stockdaerah='t'
                          and tm_spb.d_spb_storereceive isnull))
                          and tm_spb.f_spb_cancel='f'
                          and tm_spb.i_nota is null
                          and (upper(tm_spb.i_spb) ilike '%$cari%' or upper(tr_customer.e_customer_name) ilike '%$cari%')
                          and tm_spb.i_area in(select i_area from tm_user_area where i_user='$user')
                          order by tm_spb.i_area, tm_spb.i_spb",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('terimaspb/mmaster');
			$data['ada']	= 'xx';
      		$data['cari'] = $cari;
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('terimaspb');
			$data['iop']='';
	 		$this->load->view('terimaspb/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu370')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spb');
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
			  )
			{
				$ispb 	= $this->uri->segment(4);
				$iarea	= $this->uri->segment(5);
				$query 	= $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem']	= $query->num_rows(); 				
				$data['ispb'] 		= $ispb;
				$data['departement']= $this->session->userdata('departement');
				$this->load->model('terimaspb/mmaster');
				$data['ada']	= '';
      	$data['cari'] = '';
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea);
		 		$this->load->view('terimaspb/vmainform',$data);
			}else{
				$this->load->view('terimaspb/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
