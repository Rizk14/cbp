<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu125')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title']	= $this->lang->line('gj');
			$data['ijurnal']	= '';
			$this->load->model('akt-gj/mmaster');
			$data['isi']		= "";
			$data['detail']		= "";
			$data['jmlitem']	= "";
			$this->load->view('akt-gj/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan(){
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu125')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$djurnal 	= $this->input->post('djurnal', TRUE);
			if($djurnal!=''){
				$tmp=explode("-",$djurnal);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$djurnal=$th."-".$bl."-".$hr;
        $thbl=substr($th,2,2).$bl;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$vdebet		= $this->input->post('vdebet', TRUE);
			$vdebet		= str_replace(',','',$vdebet);
			$vkredit	= $this->input->post('vkredit', TRUE);
			$vkredit	= str_replace(',','',$vkredit);
			$jml		= $this->input->post('jml', TRUE);
			$edescription	= $this->input->post('edescription', TRUE);
			$fposting	='f';
			$fclose		='f';
			if(
				($iarea!='')
				&& ($djurnal!='')
				&& ($vdebet!='0')
				&& ($vkredit!='0')
			  )
			{
				if(
					($vdebet==$vkredit)
				  ){
					$this->db->trans_begin();
					$this->load->model('akt-gj/mmaster');
					$ijurnal=$this->mmaster->runningnumberjurnal($thbl);
					for($i=1;$i<=$jml;$i++){
					  	$icoa		= $this->input->post('icoa'.$i, TRUE);
					  	$ecoaname	= $this->input->post('ecoaname'.$i, TRUE);
						$vmdebet	= $this->input->post('vdebet'.$i, TRUE);
						$vmdebet	= str_replace(',','',$vmdebet);
						$vmkredit	= $this->input->post('vkredit'.$i, TRUE);
						$vmkredit	= str_replace(',','',$vmkredit);
						if($vmdebet>0)
							$fdebet='t';
						else
							$fdebet='f';
						$irefference=null;
						$drefference=null;
						$this->mmaster->insertdetail($ijurnal,$icoa,$ecoaname,$fdebet,$vmdebet,$vmkredit,$iarea,$irefference,$drefference);
					}
					$this->mmaster->insertheader($ijurnal,$iarea,$djurnal,$edescription,$vdebet,$vkredit,$fposting,$fclose);
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
			      while($row=pg_fetch_assoc($query)){
				      $now	  = $row['c'];
			      }
			      $pesan='Input Jurnal Umum No:'.$ijurnal.' Area:'.$iarea;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan );

						$data['sukses']			= true;
						$data['inomor']			= $ijurnal;
						$this->load->view('nomor',$data);
					}
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu125')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('gj');
			$this->load->view('akt-gj/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu125')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('gj')." update";
			if(
				($this->uri->segment(4)!='')
			  ){
				$ijurnal			= $this->uri->segment(4);
				$query 				= $this->db->query("select * from tm_general_jurnalitem where i_jurnal = '$ijurnal' ");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ijurnal']	= $ijurnal;
				$this->load->model('akt-gj/mmaster');
				$data['isi']		= $this->mmaster->baca($ijurnal);
				$data['detail']		= $this->mmaster->bacadetail($ijurnal);
		 		$this->load->view('akt-gj/vmainform',$data);
			}else{
				$this->load->view('akt-gj/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu125')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ijurnal 	= $this->input->post('ijurnal', TRUE);
			$djurnal 	= $this->input->post('djurnal', TRUE);
			if($djurnal!=''){
				$tmp=explode("-",$djurnal);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$djurnal=$th."-".$bl."-".$hr;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$vdebet		= $this->input->post('vdebet', TRUE);
			$vdebet		= str_replace(',','',$vdebet);
			$vkredit	= $this->input->post('vkredit', TRUE);
			$vkredit	= str_replace(',','',$vkredit);
			$jml		= $this->input->post('jml', TRUE);
			$edescription	= $this->input->post('edescription', TRUE);
			$fposting	='f';
			$fclose		='f';
			if(
				($iarea!='')
				&& ($ijurnal!='')
				&& ($djurnal!='')
				&& ($vdebet!='0')
				&& ($vkredit!='0')
			  )
			{
				if(
					($vdebet==$vkredit)
				  ){
					$this->db->trans_begin();
					$this->load->model('akt-gj/mmaster');
					for($i=1;$i<=$jml;$i++){
					  	$icoa		= $this->input->post('icoa'.$i, TRUE);
					  	$ecoaname	= $this->input->post('ecoaname'.$i, TRUE);
						$vmdebet	= $this->input->post('vdebet'.$i, TRUE);
						$vmdebet	= str_replace(',','',$vmdebet);
						$vmkredit	= $this->input->post('vkredit'.$i, TRUE);
						$vmkredit	= str_replace(',','',$vmkredit);
						if($vmdebet>0)
							$fdebet='t';
						else
							$fdebet='f';
						$irefference=null;
						$drefference=null;
						$this->mmaster->deletedetail($ijurnal,$icoa,$vdebet,$vkredit);
						$this->mmaster->insertdetail($ijurnal,$icoa,$ecoaname,$fdebet,$vmdebet,$vmkredit,$iarea,$irefference,$drefference);
					}
					$this->mmaster->deleteheader($ijurnal);
					$this->mmaster->insertheader($ijurnal,$iarea,$djurnal,$edescription,$vdebet,$vkredit,$fposting,$fclose);
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
			      while($row=pg_fetch_assoc($query)){
				      $now	  = $row['c'];
			      }
			      $pesan='Update Jurnal Umum No:'.$ijurnal.' Area:'.$iarea;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan );

						$data['sukses']			= true;
						$data['inomor']			= $ijurnal;
						$this->load->view('nomor',$data);
					}
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu125')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ijurnal		= $this->uri->segment(4);
			$icoa			= $this->uri->segment(5);
			$debet			= $this->uri->segment(6);
			$kredit			= $this->uri->segment(7);
			$this->db->trans_begin();
			$this->load->model('akt-gj/mmaster');
			$this->mmaster->deletedetail($ijurnal,$icoa,$debet,$kredit);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='Delete Item Jurnal Umum No:'.$ijurnal.' CoA:'.$icoa;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan );

				$data['page_title'] = $this->lang->line('gj')." Update";
				$query 				= $this->db->query("select * from tm_general_jurnalitem where i_jurnal = '$ijurnal' ");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ijurnal']	= $ijurnal;
				$data['isi']		= $this->mmaster->baca($ijurnal);
				$data['detail']		= $this->mmaster->bacadetail($ijurnal);
		 		$this->load->view('akt-gj/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function coa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu125')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']		= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/akt-gj/cform/coa/'.$baris.'/index/';
			$query = $this->db->query(" select * from tr_coa " ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('akt-gj/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->bacacoa($config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('akt-gj/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricoa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu125')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 				= $this->input->post('cari', FALSE);
			$cari				= strtoupper($cari);
			$data['baris']		= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/akt-gj/cform/coa/'.$baris.'/index/';
			$query = $this->db->query(" select * from tr_coa where upper(i_coa) like '%$cari%' or e_coa_name like '%$cari%'" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('akt-gj/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->caricoa($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('akt-gj/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu125')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-gj/cform/area/index/';
			$config['per_page'] = '10';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
      else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-gj/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('akt-gj/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu125')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-gj/cform/area/index/';
			$config['per_page'] = '10';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-gj/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('akt-gj/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu125')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-gj/cform/index/';
			$query=$this->db->query("select * from tm_dt",false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(4);
			$this->pagination->initialize($config);
			$cari 				= $this->input->post('cari', FALSE);
			$cari				= strtoupper($cari);
			$this->load->model('akt-gj/mmaster');
			$data['isi']		= $this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title']	= $this->lang->line('gj');
			$data['ijurnal']		= '';
			$data['iarea']	= '';
			$data['jmlitem']	= '';
			$data['detail']		= '';
	 		$this->load->view('akt-gj/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
