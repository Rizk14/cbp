<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu171')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('ikhpkeluar');
			$this->load->model('ikhpkeluar/mmaster');
			$data['iikhpkeluar']='';
			$this->load->view('ikhpkeluar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu171')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('ikhpkeluar');
			$this->load->view('ikhpkeluar/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu171')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('ikhpkeluar')." update";
      $area1	= $this->session->userdata('i_area');
			if(
				$this->uri->segment(4)
			  ){
				$data['iikhp']= $this->uri->segment(4);
				$iikhpkeluar				= $this->uri->segment(4);
        $data['dfrom']      = $this->uri->segment(5);
        $data['dto']      = $this->uri->segment(6);
        $data['iarea']      = $this->uri->segment(7);
				$this->load->model("ikhpkeluar/mmaster");
				$data['isi']=$this->mmaster->baca($iikhpkeluar);
				$query=$this->db->query("	select i_periode from tm_periode ",false);
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
            $periode=$row->i_periode;
				  }
			  }	
        $query = $this->db->query("select i_cek, d_bukti from tm_ikhp where i_ikhp = $iikhpkeluar");
			  if($query->num_rows()>0){
          foreach($query->result() as $row){
            $dbukti=substr($row->d_bukti,0,4).substr($row->d_bukti,5,2);
            if($row->i_cek=='' && $periode<=$dbukti)
              $data['bisaedit']=true;
            else
              $data['bisaedit']=false;
          }
        }else{
          $data['bisaedit']=false;
        }        
        $data['area1']  = $area1;
		 		$this->load->view('ikhpkeluar/vformupdate',$data);
			}else{
				$this->load->view('ikhpkeluar/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu171')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iikhp	 			= $this->input->post('iikhp', TRUE);
			$iarea	 			= $this->input->post('iarea', TRUE);
			$dbukti 			= $this->input->post('dbukti', TRUE);
			$dtmp					= $dbukti;
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
			}
			$ibukti				= $this->input->post('ibukti', TRUE);
			if($ibukti=='') $ibukti=null;
			$eikhptypename= $this->input->post('eikhptypename', TRUE);
			$icoa					= $this->input->post('icoa', TRUE);
			$ecoaname 		= $this->input->post('ecoaname', TRUE);
			$iikhptype		= $this->input->post('iikhptype', TRUE);
			$vterimatunai = $this->input->post('vterimatunai', TRUE);
			if($vterimatunai=='') $vterimatunai=0;
			$vterimatunai		= str_replace(',','',$vterimatunai);
			$vterimagiro	= $this->input->post('vterimagiro', TRUE);
			if($vterimagiro=='') $vterimagiro=0;
			$vterimagiro		= str_replace(',','',$vterimagiro);
			$vkeluartunai = $this->input->post('vkeluartunai', TRUE);
			if($vkeluartunai=='') $vkeluartunai=0;
			$vkeluartunai		= str_replace(',','',$vkeluartunai);
			$vkeluargiro	= $this->input->post('vkeluargiro', TRUE);
			if($vkeluargiro=='') $vkeluargiro=0;
			$vkeluargiro		= str_replace(',','',$vkeluargiro);
			if (
				($dbukti != '') && ($eikhptypename != '') && ($iarea != '') &&
				( ($vterimatunai!='') || ($vterimagiro!='') || ($vkeluartunai!='') || ($vkeluargiro!='') )
			   )
			{
				$this->load->model('ikhpkeluar/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($iikhp,$iarea,$dbukti,$ibukti,$icoa,$iikhptype,$vterimatunai,$vterimagiro,$vkeluartunai,$vkeluargiro);
				$nomor=$dtmp." - ".$eikhptypename;
				if ($this->db->trans_status() === FALSE)
				{
					  $this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	        	$now	  = $row['c'];
			    }
			    $pesan='Update IKHP Pengeluaran No:'.$iikhp.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu171')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea	 			= $this->input->post('iarea', TRUE);
			$dbukti 			= $this->input->post('dbukti', TRUE);
			$dtmp					= $dbukti;
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
			}
			$ibukti				= $this->input->post('ibukti', TRUE);
			if($ibukti=='') $ibukti=null;
			$eikhptypename= $this->input->post('eikhptypename', TRUE);
			$icoa					= $this->input->post('icoa', TRUE);
			$ecoaname 		= $this->input->post('ecoaname', TRUE);
			$iikhptype		= $this->input->post('iikhptype', TRUE);
			$vterimatunai = $this->input->post('vterimatunai', TRUE);
			if($vterimatunai=='') $vterimatunai=0;
			$vterimatunai		= str_replace(',','',$vterimatunai);
			$vterimagiro	= $this->input->post('vterimagiro', TRUE);
			if($vterimagiro=='') $vterimagiro=0;
			$vterimagiro		= str_replace(',','',$vterimagiro);
			$vkeluartunai = $this->input->post('vkeluartunai', TRUE);
			if($vkeluartunai=='') $vkeluartunai=0;
			$vkeluartunai		= str_replace(',','',$vkeluartunai);
			$vkeluargiro	= $this->input->post('vkeluargiro', TRUE);
			if($vkeluargiro=='') $vkeluargiro=0;
			$vkeluargiro		= str_replace(',','',$vkeluargiro);
			if (
				($dbukti != '') && ($eikhptypename != '') && ($iarea != '') &&
				( ($vterimatunai!='') || ($vterimagiro!='') || ($vkeluartunai!='') || ($vkeluargiro!='') )
			   )
			{
				$this->load->model('ikhpkeluar/mmaster');
				$this->db->trans_begin();
        $this->db->select(" i_ikhp from tm_ikhp
                            where i_bukti='$ibukti' and i_area='$iarea'");
		    $query = $this->db->get();
		    if ($query->num_rows() >0){
          $nomor='Nomor bukti sudah ada !!!!!';
        }else{
  				$this->mmaster->insert($iarea,$dbukti,$ibukti,$icoa,$iikhptype,$vterimatunai,$vterimagiro,$vkeluartunai,$vkeluargiro);
  				$nomor=$dtmp." - ".$eikhptypename;
        }
				if ($this->db->trans_status() === FALSE)
				{
					  $this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	        	$now	  = $row['c'];
			    }
			    $pesan='Input IKHP Pengeluaran No Bukti:'.$ibukti.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan ); 

					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function uraian()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu171')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/ikhpkeluar/cform/uraian/index/';
			$query = $this->db->query("select * from tr_ikhp_type ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('ikhpkeluar/mmaster');
			$data['page_title'] = $this->lang->line('listuraian');
			$data['isi']=$this->mmaster->bacauraian($config['per_page'],$this->uri->segment(5));
			$this->load->view('ikhpkeluar/vlisturaian', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariuraian()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu171')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/ikhpkeluar/cform/uraian/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query("select * from tr_ikhp_type
						   where upper(i_uraian) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('ikhpuraian/mmaster');
			$data['page_title'] = $this->lang->line('listuraian');
			$data['isi']=$this->mmaster->cariuraian($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('cekikhpkeluar/vlisturaian', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu171')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/ikhpkeluar/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('ikhpkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('ikhpkeluar/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu171')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/ikhpkeluar/cform/area/index/';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                              where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('ikhpkeluar/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('ikhpkeluar/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
