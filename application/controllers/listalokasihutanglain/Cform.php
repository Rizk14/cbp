<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu527')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listalokasihutanglain');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listalokasihutanglain/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu527')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      		$area1	= $this->session->userdata('i_area');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			$is_cari= $this->input->post('is_cari'); 
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);

			if ($is_cari == '')
				$is_cari= $this->uri->segment(8);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(7);
			
			if ($is_cari=="1") { 
				$config['base_url'] = base_url().'index.php/listalokasihutanglain/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/'.$is_cari.'/index/';
			}
			else { 
				$config['base_url'] = base_url().'index.php/listalokasihutanglain/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			} 
			// if($iarea!='NA'){
			// 	$sql .=" and a.i_area='$iarea'";
			//   }
			// $config['base_url'] = base_url().'index.php/listalokasihutanglain/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';

			if ($is_cari != "1") {
				$sql= " select a.i_alokasi
							                    from tm_alokasihl a, tr_area b, tr_customer c
							                    where
							                    a.i_area=b.i_area and a.i_customer=c.i_customer 
							            		and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
												a.d_alokasi <= to_date('$dto','dd-mm-yyyy')";
								if($iarea!='NA'){
									$sql .=" and a.i_area='$iarea'";
								}
			}else{
				$sql= " select a.i_alokasi
							                    from tm_alokasihl a, tr_area b, tr_customer c
							                    where
							                    a.i_area=b.i_area and a.i_customer=c.i_customer
							                    and (upper(a.i_customer) like '%$cari%' or upper(a.i_alokasi) like '%$cari%') 
							                    AND a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
												a.d_alokasi <= to_date('$dto','dd-mm-yyyy')";
								if($iarea!='NA'){
									$sql .=" and a.i_area='$iarea'";
								}
			}	
											
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if ($is_cari=="1")
				$config['cur_page'] = $this->uri->segment(10);
			else
				$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listalokasihutanglain');
			$this->load->model('listalokasihutanglain/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['area1']  = $area1;
			  if ($is_cari=="1")
				$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			else
				// $data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
				   $data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Alokasi Hutang lain2 Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listalokasihutanglain/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu527')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listalokasihutanglain');
			$this->load->view('listalokasihutanglain/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu527')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	  = $this->session->userdata('i_area');
			$ialokasi	= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$dfrom		= $this->input->post('dfrom');
			$dto  		= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(6);
			if($dto=='') $dto=$this->uri->segment(7);
			$this->load->model('listalokasihutanglain/mmaster');
      $this->db->trans_begin();
			$this->mmaster->delete($ialokasi,$iarea);
      if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
			}else{
		    $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		= pg_query($sql);
		    if(pg_num_rows($rs)>0){
			    while($row=pg_fetch_assoc($rs)){
				    $ip_address	  = $row['ip_address'];
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }
		    $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
        	$now	  = $row['c'];
		    }
		    $pesan='Menghapus Alokasi Hutang lain-lain Area:'.$iarea.' No:'.$ialokasi;
		    $this->load->model('logger');
		    $this->logger->write($id, $ip_address, $now , $pesan );  

#				$this->db->trans_rollback();
				$this->db->trans_commit();
        $cari		= strtoupper($this->input->post('cari'));
        $config['base_url'] = base_url().'index.php/listalokasihutanglain/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			  $query = $this->db->query(" select a.i_alokasi
							                      from tm_alokasihl a, tr_area b, tr_customer c
							                      where
							                      a.i_area=b.i_area and a.i_customer=c.i_customer
							                      and (upper(a.i_customer) like '%$cari%' or upper(a.i_alokasi) like '%$cari%') 
							                      and a.i_area='$iarea' and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
							                      a.d_alokasi <= to_date('$dto','dd-mm-yyyy')",false);
			  $config['total_rows'] = $query->num_rows(); 
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(8);
			  $this->pagination->initialize($config);

			  $data['page_title'] = $this->lang->line('listalokasihutanglain');
			  $this->load->model('listalokasihutanglain/mmaster');
			  $data['cari']		= $cari;
			  $data['dfrom']	= $dfrom;
			  $data['dto']		= $dto;
			  $data['iarea']	= $iarea;
        $data['area1']  = $area1;
			  $data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
	      	$now	  = $row['c'];
			  }
			  $pesan='Membuka Data Alokasi Hutang lain2 Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;

			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );  
			  $this->load->view('listalokasihutanglain/vmainform', $data);
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu527')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	= $this->session->userdata('i_area');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listalokasihutanglain/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_pelunasan a 
										              inner join tr_area on(a.i_area=tr_area.i_area)
										              inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										              where (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%')
										              and a.i_area='$iarea' and
										              a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
										              a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listalokasihutanglain');
			$this->load->model('listalokasihutanglain/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['area1']  = $area1;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listalokasihutanglain/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu527')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listalokasihutanglain/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listalokasihutanglain/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listalokasihutanglain/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu527')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listalokasihutanglain/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listalokasihutanglain/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listalokasihutanglain/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
