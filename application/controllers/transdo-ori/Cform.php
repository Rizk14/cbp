<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu65')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transdo');
			$data['isi']= '';#directory_map('./data/');
			$data['file']='';
			$this->load->view('transdo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu65')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dfrom= $this->input->post('dfrom', TRUE);
			$dto= $this->input->post('dto', TRUE);
			$data['dfrom']=$dfrom;
			$data['dto']=$dto;
			$this->load->view('transdo/vfile', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu65')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml= $this->input->post('jml', TRUE);
			$this->load->model('transdo/mmaster');
			$idox					= '';
			$isupplierx		= '';
			$data['jml']	= $jml;
			$data['kosong']	= 0;
			$tmp[0]					= null;
			$istore					= 'AA';
			$istorelocation		= '01';
			$istorelocationbin= '00';
			$eremark			= 'DO Transfer';
			for($i=0;$i<$jml;$i++){
				$cek					=$this->input->post('chk'.$i, TRUE);
				if($cek!=''){
					$ido					= $this->input->post('nodok'.$i, TRUE);
					$iop					= $this->input->post('noop'.$i, TRUE);
					$iarea				= $this->input->post('wila'.$i, TRUE);
					$ddo					= $this->input->post('tgldok'.$i, TRUE);
					if($ddo!=''){
						$tmp=explode("-",$ddo);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$ddo=$th."-".$bl."-".$hr;
						$thbl=substr($th,2,2).$bl;
					}
					$ido='DO-'.$thbl.'-DT'.substr($ido,2,4);
					$iproduct				= $this->input->post('kodeprod'.$i, TRUE);
					$iproduct				= substr($iproduct,0,7);
					$iproductgrade	= 'A';
					$iproductmotif	= $this->input->post('kodeprod'.$i, TRUE);
					$iproductmotif	= substr($iproductmotif,7,2);
					$isupplier			= $this->input->post('kodelang'.$i, TRUE);
					$vproductmill		= $this->input->post('hargasat'.$i, TRUE);
					$vproductmill		= str_replace(',','',$vproductmill);
					$jumlah					= $this->input->post('jumlah'.$i, TRUE);
					$jumlah					= str_replace(',','',$jumlah);
					$eproductname		= null;
					$this->mmaster->inserttmp( $ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
													  $vproductmill,$iproductmotif,$isupplier,$ddo,$iarea,$iop);
				}
			}
			$this->db->trans_begin();
			$dotmp	= '';
			$xx		= 0;
      $xxx  = array();
			$yy		= 0;
			for($i=0;$i<$jml;$i++){
				$cek					=$this->input->post('chk'.$i, TRUE);
				if($cek!=''){
					$ido					= $this->input->post('nodok'.$i, TRUE);
					$iop					= $this->input->post('noop'.$i, TRUE);
					$iarea				= $this->input->post('wila'.$i, TRUE);
					$ddo					= $this->input->post('tgldok'.$i, TRUE);
					if($ddo!=''){
						$tmp=explode("-",$ddo);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$ddo=$th."-".$bl."-".$hr;
					}
					$thbl=substr($th,2,2).$bl;
					$ido='DO-'.$thbl.'-DT'.substr($ido,2,4);
					$iproduct				= $this->input->post('kodeprod'.$i, TRUE);
					$iproduct				= substr($iproduct,0,7);
					$iproductgrade	= 'A';
					$iproductmotif	= $this->input->post('kodeprod'.$i, TRUE);
					$iproductmotif	= substr($iproductmotif,7,2);
					$isupplier			= $this->input->post('kodelang'.$i, TRUE);
					$vproductmill		= $this->input->post('hargasat'.$i, TRUE);
					$vproductmill		= str_replace(',','',$vproductmill);
					$jumlah					= $this->input->post('jumlah'.$i, TRUE);
					$query= $this->db->query("select e_product_name from tr_product where i_product='$iproduct'");
					foreach($query->result() as $riw){
						$eproductname	=$riw->e_product_name;
					}
					$adaop	= $this->mmaster->cekadaop($iop,$iproduct,$iproductmotif,$iproductgrade,$iarea);
					$adaitem=1;
					if($adaop!=0){
						$adaitem= $this->mmaster->cekdataitem($ido,$isupplier,$iproduct,$iproductmotif,$iproductgrade,$thbl);
						if($adaitem==0){
							$sudahada=false;
							$ono= $this->mmaster->cekdata($ido,$isupplier,$thbl);
							if( ($dotmp!= $ido) && ($ono==0) ){
								$dotmp= $ido;
								$query= $this->db->query("select sum(v_product_mill) as v_product_mill, i_do, i_supplier, d_do, 
                              i_area, i_op
														  from tt_do where i_do='$ido' and i_supplier='$isupplier'
														  group by i_do, i_supplier, d_do, i_area, i_op ", false);
								foreach($query->result() as $row){
                  $row->i_op=trim($row->i_op);
									$qq= $this->db->query("select i_op from tm_op where i_op_old like '%$row->i_op%' and i_area='$iarea'", false);
									foreach($qq->result() as $rr){
										$re= $this->db->query("select i_product from tm_op_item where i_op='$rr->i_op' and i_product='$iproduct'", false);
										if($re->num_rows() >0){
											$ada	= $this->mmaster->cekdata($row->i_do,$row->i_supplier,$thbl);
											$opbaru	= '';
											$siop	= 0;
											$saldo	= $jumlah;
											$qy		= $this->db->query("select i_op from tm_op where i_op_old like '%$row->i_op%' and i_area='$iarea'", false);
											foreach($qy->result() as $rw){
												if($opbaru!=$rw->i_op){
													if($ada==0){
														if($siop==1){
															$row->i_do=trim($row->i_do).'-A';
														}elseif($siop==2){
															$row->i_do=trim($row->i_do).'-B';
														}elseif($siop==0){
															$this->mmaster->insertheader($row->i_do,$row->i_supplier,$rw->i_op,$row->i_area,$row->d_do,
																						 $row->v_product_mill);
														}
													}else{
														if($siop==1){
															$qy= $this->db->query("select i_product from tm_op_item where i_op='$rw->i_op' and i_product='$iproduct'", false);
															if($qy->num_rows() >0){
															}
														}elseif($siop==2){
															$qy= $this->db->query("select i_product from tm_op_item where i_op='$rw->i_op' and i_product='$iproduct'", false);
															if($qy->num_rows() >0){
															}
														}else{
															$this->mmaster->updateheader($row->i_do,$row->i_supplier,$rw->i_op,$row->i_area,$row->d_do,
																						 $row->v_product_mill);
														}
													}
													$opbaru=$rw->i_op;
													$siop++;
												}
												$qy= $this->db->query("select i_product, n_order from tm_op_item where i_op='$opbaru' and i_product='$iproduct'", false);
												if($qy->num_rows() >0){
													foreach($qy->result() as $rwz){
														$saldo=$saldo-$rwz->n_order;
														if($saldo>=0){
#															if($ido=='DO-1107-DT4299'){
#																$this->mmaster->insertdetails($opbaru,$ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
#																			 $vproductmill,$iproductmotif,$isupplier);
#															}else{
																$this->mmaster->insertdetail($opbaru,$ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
																			 $vproductmill,$iproductmotif,$isupplier);
#															}
															$this->mmaster->updateopdetail($opbaru,$iproduct,$iproductgrade,$iproductmotif,$jumlah);
															$this->mmaster->updatespbdetail($opbaru,$iproduct,$iproductgrade,$iproductmotif,$jumlah);
#######################################
															$trans=$this->mmaster->lasttrans(	$iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																																$istorelocationbin);
															if(isset($trans)){
																foreach($trans as $itrans)
																{
																	$q_aw =$itrans->n_quantity_awal;
																	$q_ak =$itrans->n_quantity_akhir;
																	$q_in =$itrans->n_quantity_in;
																	$q_out=$itrans->n_quantity_out;
																	break;
																}
															}else{
																$trans=$this->mmaster->qic( $iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																														$istorelocationbin);
																if(isset($trans)){
																	foreach($trans as $itrans)
																	{
																		$q_aw =$itrans->n_quantity_stock;
																		$q_ak =$itrans->n_quantity_stock;
																		$q_in =0;
																		$q_out=0;
																		break;
																	}
																}else{
																	$q_aw=0;
																	$q_ak=0;
																	$q_in=0;
																	$q_out=0;
																}
															}
															$this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																													 $istorelocationbin,$eproductname,$ido,$q_in,$q_out,$jumlah,$q_aw,$q_ak);
															$th=substr($ddo,0,4);
															$bl=substr($ddo,5,2);
															$emutasiperiode=$th.$bl;
															if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																													 $istorelocationbin,$emutasiperiode))
															{
																$this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																														  $istorelocationbin,$jumlah,$emutasiperiode);
															}else{
																$this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																															$istorelocationbin,$jumlah,$emutasiperiode);
															}
															if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																											 $istorelocationbin))
															{
																$this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																													$istorelocationbin,$jumlah,$q_ak);
															}else{
																$this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																													$istorelocationbin,$eproductname,$jumlah);
															}
#######################################
														}
													}
													$sudahada=true;
												}
											}
										}
									}
								}
							}
							if(!$sudahada){
								if($ono>0){
#									$opbaru= $this->mmaster->cekono($ido,$isupplier);
									$opbaru= $this->mmaster->cekono($iop,$iarea);
									$saldo=$jumlah;
									$qy= $this->db->query("select i_product, n_order from tm_op_item where i_op='$opbaru' and i_product='$iproduct'", false);
									if($qy->num_rows() >0){
										foreach($qy->result() as $rwz){
											$saldo=$saldo-$rwz->n_order;
											if($saldo>=0){
#												if($ido=='DO-1107-DT4299'){
#												$this->mmaster->insertdetailz($opbaru,$ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
#																 $vproductmill,$iproductmotif,$isupplier);
#												}else{
												$this->mmaster->insertdetail($opbaru,$ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
																 $vproductmill,$iproductmotif,$isupplier);
#												}
												$this->mmaster->updateopdetail($opbaru,$iproduct,$iproductgrade,$iproductmotif,$jumlah);
												$this->mmaster->updatespbdetail($opbaru,$iproduct,$iproductgrade,$iproductmotif,$jumlah);
#######################################
												$trans=$this->mmaster->lasttrans(	$iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																													$istorelocationbin);
												if(isset($trans)){
													foreach($trans as $itrans)
													{
														$q_aw =$itrans->n_quantity_awal;
														$q_ak =$itrans->n_quantity_akhir;
														$q_in =$itrans->n_quantity_in;
														$q_out=$itrans->n_quantity_out;
														break;
													}
												}else{
													$trans=$this->mmaster->qic( $iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																											$istorelocationbin);
													if(isset($trans)){
														foreach($trans as $itrans)
														{
															$q_aw =$itrans->n_quantity_stock;
															$q_ak =$itrans->n_quantity_stock;
															$q_in =0;
															$q_out=0;
															break;
														}
													}else{
														$q_aw=0;
														$q_ak=0;
														$q_in=0;
														$q_out=0;
													}
												}
												$this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																										 $istorelocationbin,$eproductname,$ido,$q_in,$q_out,$jumlah,$q_aw,$q_ak);
												$th=substr($ddo,0,4);
												$bl=substr($ddo,5,2);
												$emutasiperiode=$th.$bl;
												if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																										 $istorelocationbin,$emutasiperiode))
												{
													$this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																											  $istorelocationbin,$jumlah,$emutasiperiode);
												}else{
													$this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																												$istorelocationbin,$jumlah,$emutasiperiode);
												}
												if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																								 $istorelocationbin))
												{
													$this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																										$istorelocationbin,$jumlah,$q_ak);
												}else{
													$this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																										$istorelocationbin,$eproductname,$jumlah);
												}
#######################################
											}
										}
									}
								}else{
#									$opbaru= $this->mmaster->cekono($ido,$isupplier);
									$opbaru= $this->mmaster->cekono($iop,$iarea);
#									if($ido=='DO-1107-DT4299'){
#										$this->mmaster->insertdetail($opbaru,$ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
#																	 $vproductmill,$iproductmotif,$isupplier);
#									}else{
										$this->mmaster->insertdetail($opbaru,$ido,$iproduct,$iproductgrade,$eproductname,$jumlah,
																	 $vproductmill,$iproductmotif,$isupplier);
#									}
									$this->mmaster->updateopdetail($opbaru,$iproduct,$iproductgrade,$iproductmotif,$jumlah);
									$this->mmaster->updatespbdetail($opbaru,$iproduct,$iproductgrade,$iproductmotif,$jumlah);
#######################################
									$trans=$this->mmaster->lasttrans(	$iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																										$istorelocationbin);
									if(isset($trans)){
										foreach($trans as $itrans)
										{
											$q_aw =$itrans->n_quantity_awal;
											$q_ak =$itrans->n_quantity_akhir;
											$q_in =$itrans->n_quantity_in;
											$q_out=$itrans->n_quantity_out;
											break;
										}
									}else{
										$trans=$this->mmaster->qic( $iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																								$istorelocationbin);
										if(isset($trans)){
											foreach($trans as $itrans)
											{
												$q_aw =$itrans->n_quantity_stock;
												$q_ak =$itrans->n_quantity_stock;
												$q_in =0;
												$q_out=0;
												break;
											}
										}else{
											$q_aw=0;
											$q_ak=0;
											$q_in=0;
											$q_out=0;
										}
									}
									$this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																							 $istorelocationbin,$eproductname,$ido,$q_in,$q_out,$jumlah,$q_aw,$q_ak);
									$th=substr($ddo,0,4);
									$bl=substr($ddo,5,2);
									$emutasiperiode=$th.$bl;
									if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																							 $istorelocationbin,$emutasiperiode))
									{
										$this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																								  $istorelocationbin,$jumlah,$emutasiperiode);
									}else{
										$this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																									$istorelocationbin,$jumlah,$emutasiperiode);
									}
									if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																					 $istorelocationbin))
									{
										$this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																							$istorelocationbin,$jumlah,$q_ak);
									}else{
										$this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,
																							$istorelocationbin,$eproductname,$jumlah);
									}
#######################################
								}
								$sudahada=false;
							}	
						}
					}else{
						$yy++;
						$xx++;
						$xxx[$yy]		= $iop.' - '.$iproduct.$iproductmotif.' - '.$eproductname.'  -  Tidak Ada di OP';
						$data['kosong']	= $xx;
					}
				}
			}
			$data['error']=$xxx;
			$this->db->query("delete from tt_do");
			if (
				($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
				$this->load->view('transdo/vformgagal',$data);
			}else{
				$this->db->trans_commit();
#				$this->db->trans_rollback();

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Transfer ke DO Berhasil';#Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

				$this->load->view('transdo/vformsukses',$data);
			}			

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
