<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu287')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exp-kum');
			$data['datefrom']='';
			$data['dateto']	='';
			$this->load->view('exp-kum/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu287')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-kum/mmaster');
			$datefrom = $this->input->post('datefrom');
			$dateto	  = $this->input->post('dateto');
			if($datefrom!=''){
				$tmp=explode("-",$datefrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$datefrom=$th."-".$bl."-".$hr;
				$periodeawal	= $hr."/".$bl."/".$th;
			}
            if($dateto!='')
            {
               $tmp=explode("-",$dateto);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $dateto=$th."-".$bl."-".$hr;
               $periodeakhir   = $hr."/".$bl."/".$th;
            }
			$data['page_title'] = $this->lang->line('exp-kum');
			$this->db->select("	distinct a.i_customer, a.d_kum, a.f_kum_cancel, a.i_kum, d.e_area_name,
                          a.d_kum, a.e_bank_name,c.e_customer_name, e.e_customer_setor, a.e_remark,
                          a.v_jumlah, a.v_sisa, a.f_close, a.n_kum_year, d.i_area, g.i_dt, g.d_dt, f.i_giro
                          from tm_kum a
                          left join tr_customer c on(a.i_customer=c.i_customer)
                          left join tr_area d on(a.i_area=d.i_area)
                          left join tr_customer_owner e on(a.i_customer=e.i_customer)
                          left join tm_pelunasan f on(a.i_kum=f.i_giro and a.d_kum=f.d_giro and f.f_pelunasan_cancel='f' 
                          and f.f_giro_tolak='f' and f.f_giro_batal='f' and a.i_area=f.i_area)
                          left join tm_dt g on(f.i_dt=g.i_dt and f.d_dt=g.d_dt and f.i_area=g.i_area 
                          and f.f_pelunasan_cancel='f' and f.f_giro_tolak='f' and f.f_giro_batal='f' and g.i_area=a.i_area)
                          where (upper(a.i_kum) like '%%') and
                          ((f.i_jenis_bayar!='02' and 
                          f.i_jenis_bayar!='01' and 
                          f.i_jenis_bayar!='04' and 
                          f.i_jenis_bayar='03') or ((f.i_jenis_bayar='03') is null)) and 
                          (a.d_kum >= to_date('$datefrom','yyyy-mm-dd') and
                          a.d_kum <= to_date('$dateto','yyyy-mm-dd')) and a.f_kum_cancel='f'",false);
		    $query = $this->db->get();
			  $this->load->library('PHPExcel');
			  $this->load->library('PHPExcel/IOFactory');
			  $objPHPExcel = new PHPExcel();
			  $objPHPExcel->getProperties()->setTitle("Daftar Transfer Uang Masuk")->setDescription("PT. Dialogue Garmindo Utama");
			  $objPHPExcel->setActiveSheetIndex(0);
			  if ($query->num_rows() > 0){
				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A2:A4'
				  );
				  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(14);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);

				  $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar Transfer Uang Masuk');
				  $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,10,2);
				  $objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$periodeawal.' - '.$periodeakhir);
				  $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,10,3);

				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A5:K5'
				  );


				  $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Area');
				  $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  )

					  )
				  );

				  $objPHPExcel->getActiveSheet()->setCellValue('B5', 'No Bukti');
				  $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('C5', 'No DT');
				  $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );

				  $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Tgl DT');
				  $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );

				  $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Tgl Transfer');
				  $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Nama Bank');
				  $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  )
					  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Pelanggan');
				  $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  )
					  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Penyetor/Ket');
				  $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Jumlah');
				  $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  )
					  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Sisa');
				  $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  )

					  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('K5', 'KodeLang');
				  $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  )

					  )
				  );

				  $i=6;
				  $j=6;
          $no=0;
         
				  foreach($query->result() as $row){
            $no++;
            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' => array(
						    'name'	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    )
				    ),
				    'A'.$i.':K'.$i
				    );
					if($row->d_dt!=''){
						$tmp=explode("-",$row->d_dt);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$row->d_dt=$hr."-".$bl."-".$th;
					}
					if($row->d_kum!=''){
						$tmp=explode("-",$row->d_kum);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$row->d_kum=$hr."-".$bl."-".$th;
					}
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->e_area_name, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_kum, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_dt, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->d_dt, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->d_kum, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->e_bank_name, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->e_remark, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->v_jumlah, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->v_sisa, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->i_customer, Cell_DataType::TYPE_STRING);
					  $i++;
					  $j++;
				  }
				  $x=$i-1;
			  }
			  $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $nama='kum-'.substr($datefrom,5,2).'-'.substr($datefrom,0,4).'.xls';
			  $objWriter->save('excel/00/'.$nama); 

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
        	$now	  = $row['c'];
	      }
	      $pesan='Export Transfer Uang Masuk tanggal:'.$datefrom.' sampai:'.$dateto;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan ); 

			  $data['sukses'] = true;
			  $data['inomor']	= "Transfer Uang Masuk";
			  $this->load->view('nomor',$data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	  }
}
?>
