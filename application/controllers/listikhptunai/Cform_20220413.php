<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
#			($this->session->userdata('menu525')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listikhp');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listikhptunai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
#			($this->session->userdata('menu525')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			
#			listikhptunai/cform/view/08-02-2016/13-02-2016/03/287933/71218500/index/10
			
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$this->load->model('listikhptunai/mmaster');
			$data['page_title'] = $this->lang->line('listikhp');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
			$data['start_skrg']	= $this->uri->segment(10);
			if ($data['start_skrg'] == '') {
				$saldoawal	= $this->mmaster->bacasaldo($iarea,$dfrom,$dto);
				if ($saldoawal) {
					foreach($saldoawal as $cek1){
					  if(!empty($cek1->d_bukti)) {
					    $this->db->select("	sum(a.v_jumlah) as v_terima_giro
								    from tm_giro a, tm_dt b
									    where 
									    a.i_area='$iarea' AND a.i_dt=b.i_dt and a.i_area=b.i_area AND
                      			 a.d_giro_terima > '$cek1->d_bukti'  and a.f_giro_batal_input='0' AND 
									    a.d_giro_terima < to_date('$dfrom','dd-mm-yyyy') group by a.i_area",false);
					    $querys = $this->db->get();
					    $ghiro=0;
					    if ($querys->num_rows() > 0){
						    foreach($querys->result() as $siw){
							    $ghiro=$siw->v_terima_giro;
						    }
					    }
      		    $this->db->select(" sum(a.v_jumlah) as v_terima_tunai from tm_tunai a
                                  where a.f_tunai_cancel='f' and a.d_tunai>'$cek1->d_bukti' 
                                  AND a.d_tunai < to_date('$dfrom', 'dd-mm-yyyy') and a.i_area='$iarea'
                                  and (upper(a.i_tunai) like '%$cari%')",false);
              $querys = $this->db->get();
					    $cunai=0;
					    if ($querys->num_rows() > 0){
						    foreach($querys->result() as $siw){
							    $cunai=$siw->v_terima_tunai;
						    }
					    }
      		    $this->db->select(" sum(a.v_jumlah) as v_terima_ku from tm_kum a
                                  where a.d_kum>'$cek1->d_bukti' and a.f_kum_cancel='f' and a.i_area='$iarea'
                                  and (upper(a.i_kum) like '%$cari%')",false);
              $querys = $this->db->get();
					    $khau=0;
					    if ($querys->num_rows() > 0){
						    foreach($querys->result() as $siw){
							    $khau=$siw->v_terima_ku;
						    }
					    }
              $this->db->select("	sum(a.v_terima_tunai) as ttunai, sum(a.v_terima_giro) as tgiro, sum(a.v_keluar_tunai) as ktunai, 
                                  sum(a.v_keluar_giro) as kgiro from tm_ikhp a, tr_ikhp_type b 
                                  where a.i_ikhp_type=b.i_ikhp_type and
									                a.i_area='$iarea' and a.d_bukti > '$cek1->d_bukti' AND a.d_bukti < to_date('$dfrom','dd-mm-yyyy') 
                                  group by a.i_area",false);
					    $query = $this->db->get();
					    $kcunai=0;
					    $kgiro=0;
					    if ($query->num_rows() > 0){
						    foreach($query->result() as $siw){
                  $cunai=$cunai+$siw->ttunai;
                  $ghiro=$ghiro+$siw->tgiro;
							    $kcunai=$siw->ktunai;
							    $kgiro=$siw->kgiro;
						    }
					    }
					    
					    $this->db->select(" distinct on (c.i_rtunai) (c.v_jumlah) as v_keluar_tunai from tm_rtunai_item b, tm_rtunai c
                                  where b.i_rtunai=c.i_rtunai and b.i_area=c.i_area and c.f_rtunai_cancel='f' 
                                  and c.d_rtunai>'$cek1->d_bukti' AND c.d_rtunai < to_date('$dfrom', 'dd-mm-yyyy') and c.i_area='$iarea'
                                  and (upper(b.i_tunai) like '%$cari%')",false);
              $querys = $this->db->get();
					    if ($querys->num_rows() > 0){
						    foreach($querys->result() as $siw){
							    $kcunai=$kcunai+$siw->v_keluar_tunai;
						    }
					    }
					    
					    $sahirt=($cek1->v_saldo_akhirtunai+$cunai)-$kcunai;
					    $sahirg=($cek1->v_saldo_akhirgiro+$ghiro)-$kgiro;
					    $sahirk=0;
					    $sawalt=($cek1->v_saldo_akhirtunai+$cunai)-$kcunai;
					    $sawalg=($cek1->v_saldo_akhirgiro+$ghiro)-$kgiro;
					    $sawalk=0;
				    }
				  }
			  }else {
					$this->db->select("	sum(a.v_jumlah) as v_terima_giro from tm_giro a, tm_dt b
								              where 
									            a.i_area='$iarea' AND a.i_dt=b.i_dt and a.i_area=b.i_area AND
                              (a.d_dt > to_date('$dto','dd-mm-yyyy') and a.d_dt < to_date('$dfrom','dd-mm-yyyy'))
								              group by a.i_area",false);
					$querys = $this->db->get();
					$ghiro=0;
					if ($querys->num_rows() > 0){
						foreach($querys->result() as $siw){
							$ghiro=$siw->v_terima_giro;
						}
					}
      		    $this->db->select(" sum(a.v_jumlah) as v_terima_tunai from tm_tunai a
                                  where a.f_tunai_cancel='f' and a.i_area='$iarea' and 
                                  (a.d_tunai > to_date('$dto','dd-mm-yyyy') and a.d_tunai < to_date('$dfrom','dd-mm-yyyy'))
                                  and (upper(a.i_tunai) like '%$cari%')",false);
              $querys = $this->db->get();
					    $cunai=0;
					    if ($querys->num_rows() > 0){
						    foreach($querys->result() as $siw){
							    $cunai=$siw->v_terima_tunai;
						    }
					    }
      		    $this->db->select(" sum(a.v_jumlah) as v_terima_ku from tm_kum a
                                  where (a.d_kum > to_date('$dto','dd-mm-yyyy') and a.d_kum < to_date('$dfrom','dd-mm-yyyy')) 
                                  and a.f_kum_cancel='f' and a.i_area='$iarea'
                                  and (upper(a.i_kum) like '%$cari%')",false);
              $querys = $this->db->get();
					    $khau=0;
					    if ($querys->num_rows() > 0){
						    foreach($querys->result() as $siw){
							    $khau=$siw->v_terima_ku;
						    }
					    }
          $this->db->select("	sum(a.v_terima_tunai) as ttunai, sum(a.v_terima_giro) as tgiro, sum(a.v_keluar_tunai) as ktunai, 
                              sum(a.v_keluar_giro) as kgiro from tm_ikhp a, tr_ikhp_type b
							                where a.i_ikhp_type=b.i_ikhp_type and
							                a.i_area='$iarea' and a.d_bukti > '$cek1->d_bukti' AND a.d_bukti < to_date('$dfrom','dd-mm-yyyy') 
                              group by a.i_area",false);
			    $query = $this->db->get();
			    $kcunai=0;
			    $kgiro=0;
			    if ($query->num_rows() > 0){
				    foreach($query->result() as $siw){
              $cunai=$cunai+$siw->ttunai;
              $ghiro=$ghiro+$siw->tgiro;
					    $kcunai=$siw->ktunai;
					    $kgiro=$siw->kgiro;
				    }
			    }
			    
			    $this->db->select(" distinct on (c.i_rtunai) (c.v_jumlah) as v_keluar_tunai from tm_rtunai_item b, tm_rtunai c
                              where b.i_rtunai=c.i_rtunai and b.i_area=c.i_area
                              and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f' and c.d_rtunai>'$cek1->d_bukti'
                              AND c.d_rtunai < to_date('$dfrom', 'dd-mm-yyyy') and c.i_area='$iarea'
                              and (upper(b.i_tunai) like '%$cari%')",false);
          $querys = $this->db->get();
			    if ($querys->num_rows() > 0){
				    foreach($querys->result() as $siw){
					    $kcunai=$kcunai+$siw->v_keluar_tunai;
				    }
			    }

					$sahirt=$cunai-$kcunai;
					$sahirg=$ghiro-$kgiro;
					$sahirk=0;
					$sawalt=$cunai-$kcunai;
					$sawalg=$ghiro-$kgiro;
					$sawalk=0;
			  }
#			  echo '1. '.$sahirt.'<br>';
				$data['sahirt'] = $sahirt;
				$data['sahirg'] = $sahirg;
				$data['sahirk'] = $sahirk;
				$data['sawalt'] = $sawalt;
				$data['sawalg'] = $sawalg;
				$data['sawalk'] = $sawalk;
			}else{
				$saldoawal	= $this->mmaster->bacasaldo($iarea,$dfrom,$dto);
				if ($saldoawal) {
					foreach($saldoawal as $cek1){
					    $this->db->select("	sum(a.v_jumlah) as v_terima_giro
								    from tm_giro a, tm_dt b
									    where 
									    a.i_area='$iarea' AND a.i_dt=b.i_dt and a.i_area=b.i_area AND
                      			 a.d_giro_terima > '$cek1->d_bukti'  and a.f_giro_batal_input='0' AND 
									    a.d_giro_terima < to_date('$dfrom','dd-mm-yyyy') group by a.i_area",false);
					    $querys = $this->db->get();
					    $ghiro=0;
					    if ($querys->num_rows() > 0){
						    foreach($querys->result() as $siw){
							    $ghiro=$siw->v_terima_giro;
						    }
					    }
      		    $this->db->select(" sum(a.v_jumlah) as v_terima_tunai from tm_tunai a
                                  where a.f_tunai_cancel='f' and a.d_tunai>'$cek1->d_bukti' 
                                  AND a.d_tunai < to_date('$dfrom', 'dd-mm-yyyy') and a.i_area='$iarea'
                                  and (upper(a.i_tunai) like '%$cari%')",false);
              $querys = $this->db->get();
					    $cunai=0;
					    if ($querys->num_rows() > 0){
						    foreach($querys->result() as $siw){
							    $cunai=$siw->v_terima_tunai;
						    }
					    }
      		    $this->db->select(" sum(a.v_jumlah) as v_terima_ku from tm_kum a
                                  where a.d_kum>'$cek1->d_bukti' and a.f_kum_cancel='f' and a.i_area='$iarea'
                                  and (upper(a.i_kum) like '%$cari%')",false);
              $querys = $this->db->get();
					    $khau=0;
					    if ($querys->num_rows() > 0){
						    foreach($querys->result() as $siw){
							    $khau=$siw->v_terima_ku;
						    }
					    }
              $this->db->select("	sum(a.v_terima_tunai) as ttunai, sum(a.v_terima_giro) as tgiro, sum(a.v_keluar_tunai) as ktunai, 
                                  sum(a.v_keluar_giro) as kgiro from tm_ikhp a, tr_ikhp_type b 
                                  where a.i_ikhp_type=b.i_ikhp_type and
									                a.i_area='$iarea' and a.d_bukti > '$cek1->d_bukti' AND a.d_bukti < to_date('$dfrom','dd-mm-yyyy') 
                                  group by a.i_area",false);
					    $query = $this->db->get();
					    $kcunai=0;
					    $kgiro=0;
					    if ($query->num_rows() > 0){
						    foreach($query->result() as $siw){
                  $cunai=$cunai+$siw->ttunai;
                  $ghiro=$ghiro+$siw->tgiro;
							    $kcunai=$siw->ktunai;
							    $kgiro=$siw->kgiro;
						    }
					    }
					    
					    $this->db->select(" distinct on (c.i_rtunai) (c.v_jumlah) as v_keluar_tunai from tm_rtunai_item b, tm_rtunai c
                                  where b.i_rtunai=c.i_rtunai and b.i_area=c.i_area and c.f_rtunai_cancel='f' 
                                  and c.d_rtunai>'$cek1->d_bukti' AND c.d_rtunai < to_date('$dfrom', 'dd-mm-yyyy') and c.i_area='$iarea'
                                  and (upper(b.i_tunai) like '%$cari%')",false);
              $querys = $this->db->get();
					    if ($querys->num_rows() > 0){
						    foreach($querys->result() as $siw){
							    $kcunai=$kcunai+$siw->v_keluar_tunai;
						    }
					    }
					    
					    $sahirt=($cek1->v_saldo_akhirtunai+$cunai)-$kcunai;
					    $sahirg=($cek1->v_saldo_akhirgiro+$ghiro)-$kgiro;
					    $sahirk=0;
					    $sawalt=($cek1->v_saldo_akhirtunai+$cunai)-$kcunai;
					    $sawalg=($cek1->v_saldo_akhirgiro+$ghiro)-$kgiro;
					    $sawalk=0;
					  }
/*
    		    $this->db->select(" sum(a.v_jumlah) as v_terima_tunai from tm_tunai a
                                where a.f_tunai_cancel='f' and a.i_area='$iarea' and 
                                (a.d_tunai > to_date('$cek1->d_bukti','dd-mm-yyyy') and a.d_tunai < to_date('$dfrom','dd-mm-yyyy'))
                                and (upper(a.i_tunai) like '%$cari%')",false);
            $querys = $this->db->get();
				    $cunai=0;
				    if ($querys->num_rows() > 0){
					    foreach($querys->result() as $siw){
						    $cunai=$siw->v_terima_tunai;
					    }
				    }
				    if($cunai=='')$cunai=0;
      		    $this->db->select(" sum(a.v_jumlah) as v_terima_ku from tm_kum a
                                  where (a.d_kum > to_date('$cek1->d_bukti','dd-mm-yyyy') and a.d_kum < to_date('$dfrom','dd-mm-yyyy')) 
                                  and a.f_kum_cancel='f' and a.i_area='$iarea'
                                  and (upper(a.i_kum) like '%$cari%')",false);
              $querys = $this->db->get();
					    $khau=0;
					    if ($querys->num_rows() > 0){
						    foreach($querys->result() as $siw){
							    $khau=$siw->v_terima_ku;
						    }
					    }
					    if($khau=='')$khau=0;
					  $this->db->select("	sum(a.v_jumlah) as v_terima_giro
								                from tm_giro a, tm_dt b
								                where 
								                a.i_area='$iarea' and a.i_dt=b.i_dt and a.i_area=b.i_area  and a.f_giro_batal_input='0' AND
  							                a.d_giro_terima > to_date('$cek1->d_bukti','dd-mm-yyyy') and a.d_giro_terima < to_date('$dfrom','dd-mm-yyyy')
  							                group by a.i_area",false);
					  $querys = $this->db->get();
					  $ghiro=0;
					  if ($querys->num_rows() > 0){
						  foreach($querys->result() as $siw){
							  $ghiro=$siw->v_terima_giro;
						  }
					  }
					  if($ghiro=='')$ghiro=0;
            $this->db->select("	sum(v_terima_tunai) as ttunai, sum(v_terima_giro) as tgiro, sum(v_keluar_tunai) as ktunai, 
                                sum(v_keluar_giro) as kgiro from tm_ikhp
							                  where i_area='$iarea' and d_bukti > to_date('$cek1->d_bukti','dd-mm-yyyy') 
							                  AND d_bukti < to_date('$dfrom','dd-mm-yyyy') group by i_area",false);
			      $query = $this->db->get();
			      $kcunai=0;
			      $kgiro=0;
			      if ($query->num_rows() > 0){
				      foreach($query->result() as $siw){
                $cunai=$cunai+$siw->ttunai;
                $ghiro=$ghiro+$siw->tgiro;
					      $kcunai=$siw->ktunai;
					      $kgiro=$siw->kgiro;
				      }
			      }

			      $this->db->select(" distinct on (c.i_rtunai) (c.v_jumlah) as v_keluar_tunai from tm_tunai a, tm_rtunai_item b, tm_rtunai c
                                where a.i_tunai=b.i_tunai and a.i_area=b.i_area_tunai and b.i_rtunai=c.i_rtunai and b.i_area=c.i_area
                                and a.f_tunai_cancel='f' and c.f_rtunai_cancel='f' and c.d_rtunai>to_date('$cek1->d_bukti','dd-mm-yyyy')
                                AND c.d_rtunai < to_date('$dfrom', 'dd-mm-yyyy') and a.i_area='$iarea'
                                and (upper(a.i_tunai) like '%$cari%')",false);
            $querys = $this->db->get();
			      if ($querys->num_rows() > 0){
				      foreach($querys->result() as $siw){
					      $kcunai=$kcunai+$siw->v_keluar_tunai;
				      }
			      }

				    $sahirt=($cek1->v_saldo_akhirtunai+$cunai)-$kcunai;
				    $sahirg=($cek1->v_saldo_akhirgiro+$ghiro)-$kgiro;
  					$sahirk=0;
				    $sawalt=($cek1->v_saldo_akhirtunai+$cunai)-$kcunai;
				    $sawalg=($cek1->v_saldo_akhirgiro+$ghiro)-$kgiro;
					  $sawalk=0;
				  }
*/				  
#				  echo $cek1->v_saldo_akhirtunai.'<br>';
#				  echo $cunai.'<br>';
#				  echo $kcunai.'<br>';die;
			  }else{
    		    $this->db->select(" sum(a.v_jumlah) as v_terima_tunai from tm_tunai a
                                where a.f_tunai_cancel='f' and a.i_area='$iarea' and 
                                (a.d_tunai > to_date('$dto','dd-mm-yyyy') and a.d_tunai < to_date('$dfrom','dd-mm-yyyy'))
                                and (upper(a.i_tunai) like '%$cari%')",false);
            $querys = $this->db->get();
				    $cunai=0;
				    if ($querys->num_rows() > 0){
					    foreach($querys->result() as $siw){
						    $cunai=$siw->v_terima_tunai;
					    }
				    }
      		    $this->db->select(" sum(a.v_jumlah) as v_terima_ku from tm_kum a
                                  where (a.d_kum > to_date('$dto','dd-mm-yyyy') and a.d_kum < to_date('$dfrom','dd-mm-yyyy')) 
                                  and a.f_kum_cancel='f' and a.i_area='$iarea'
                                  and (upper(a.i_kum) like '%$cari%')",false);
              $querys = $this->db->get();
					    $khau=0;
					    if ($querys->num_rows() > 0){
						    foreach($querys->result() as $siw){
							    $khau=$siw->v_terima_ku;
						    }
					    }
					$this->db->select("	sum(a.v_jumlah) as v_terima_giro
								              from tm_giro a, tm_dt b
                							where 
								              a.i_area='$iarea' and a.i_dt=b.i_dt and a.i_area=b.i_area  and a.f_giro_batal_input='0' AND
								              (a.d_giro_terima > to_date('$dto','dd-mm-yyyy') and a.d_giro_terima < to_date('$dfrom','dd-mm-yyyy'))
                  						group by a.i_area",false);
					$querys = $this->db->get();
					$ghiro=0;
					if ($querys->num_rows() > 0){
						foreach($querys->result() as $siw){
							$ghiro=$siw->v_terima_giro;
						}
					}

          $this->db->select("	sum(v_terima_tunai) as ttunai, sum(v_terima_giro) as tgiro, sum(v_keluar_tunai) as ktunai, 
                              sum(v_keluar_giro) as kgiro from tm_ikhp
						                  where 
						                  i_area='$iarea' and d_bukti > '$cek1->d_bukti' AND d_bukti < to_date('$dfrom','dd-mm-yyyy') group by i_area",false);
		      $query = $this->db->get();
		      $kcunai=0;
		      $kgiro=0;
		      if ($query->num_rows() > 0){
			      foreach($query->result() as $siw){
              $cunai=$cunai+$siw->ttunai;
              $ghiro=$ghiro+$siw->tgiro;
				      $kcunai=$siw->ktunai;
				      $kgiro=$siw->kgiro;
			      }
		      }

			    $sahirt=$cunai-$kcunai;
			    $sahirg=$ghiro-$kgiro;
			    $sawalt=$cunai-$kcunai;
			    $sawalg=$ghiro-$kgiro;
          $sahirk=0;
          $sawalk=0;
			   }
#			  echo '3. '.$sahirt.'<br>';
				$data['sahirt'] = $sahirt;
				$data['sahirk'] = $sahirk;
				$data['sahirg'] = $sahirg;
			}

      $config['base_url'] = base_url().'index.php/listikhptunai/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$sahirt.'/'.$sahirg.'/index/';
			$jum_isi_baru= array();
			$buktitmp='xxx';
			$namatmp ='yyy';
			$coatmp  ='zzz';
			$terimatunai= 0;
			$terimagiro	= 0;
			$terimaku	  = 0;
			$keluartunai= 0;
			$keluargiro	= 0;
			$keluarku	  = 0;
			$cek_datatanpalimit= $this->mmaster->bacaperiodetanpalimit($iarea,$dfrom,$dto);
			
			$tempsahirt=$sahirt;
			$tempsahirg=$sahirg;
			$tempsahirk=$sahirk;
      $dbuktitmp='';
      $coa='';
#		  echo '4. '.$cek1->v_saldo_akhirtunai.'<br>';
      if($cek_datatanpalimit){
			  foreach($cek_datatanpalimit as $cek3) {
				  $tmp=explode('-',$cek3->d_bukti);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $cek3->d_bukti=$tgl.'-'.$bln.'-'.$thn;
				   
				  if(($buktitmp!=substr($cek3->i_bukti,0,8)) && ($buktitmp=='xxx') ){
				    $bukti=substr(trim($cek3->i_bukti),0,7);
				    $nama = $cek3->e_ikhp_typename;
					  if(strlen($cek3->i_coa)==5){
						  $coa  = $cek3->i_coa.$iarea;
				    }else{
			        $coa  = $cek3->i_coa;
					  }
					  $terimatunai= $terimatunai+$cek3->v_terima_tunai;
					  $terimagiro	= $terimagiro+$cek3->v_terima_giro;
					  $terimaku	= $terimaku+$cek3->v_terima_ku;
					  $keluartunai= $keluartunai+$cek3->v_keluar_tunai;
					  $keluargiro	= $keluargiro+$cek3->v_keluar_giro;
#					  $keluarku	= $keluarku+$cek3->v_keluar_ku;
					  $keluarku	= $keluarku+$cek3->v_terima_ku;
				    $tempsahirt			= $tempsahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
				    $tempsahirg			= $tempsahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
				    $tempsahirk			= 0;#$tempsahirk+$cek3->v_terima_ku-$cek3->v_keluar_ku;
#            echo '5. '.$tempsahirt.'<br>';
			    }elseif( ($buktitmp==substr($cek3->i_bukti,0,8)) && ($buktitmp!='xxx') && ($buktitmp!='') ){
				    $bukti=substr(trim($cek3->i_bukti),0,7);
				    $nama = $cek3->e_ikhp_typename;
				    if(strlen($cek3->i_coa)==5){
					    $coa  = $cek3->i_coa.$iarea;
				    }else{
			        $coa  = $cek3->i_coa;
				    }
				    $terimatunai= $terimatunai+$cek3->v_terima_tunai;
				    $terimagiro	= $terimagiro+$cek3->v_terima_giro;
				    $terimaku	  = $terimaku+$cek3->v_terima_ku;
				    $keluartunai= $keluartunai+$cek3->v_keluar_tunai;
				    $keluargiro	= $keluargiro+$cek3->v_keluar_giro;
#				    $keluarku	  = $keluarku+$cek3->v_keluar_ku;
				    $keluarku	  = $keluarku+$cek3->v_terima_ku;
				    $tempsahirt			= $tempsahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
				    $tempsahirg			= $tempsahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
				    $tempsahirk			= 0;#$tempsahirk+$cek3->v_terima_ku-$cek3->v_keluar_ku;
#					  echo '6. '.$tempsahirt.'<br>';
			    }elseif( ($buktitmp==substr($cek3->i_bukti,0,8)) && ($buktitmp!='xxx') && ($buktitmp=='') ){
				    $nama = $cek3->e_ikhp_typename;
				    if(strlen($cek3->i_coa)==5){
					    $coa  = $cek3->i_coa.$iarea;
				    }else{
			        $coa  = $cek3->i_coa;
				    }
			    	$jum_isi_baru[] = array('dbuktitmp'=> $dbuktitmp,
										  'buktitmp'=> $buktitmp,
										  'namatmp'=> $namatmp,
										  'coatmp'=> $coatmp,
										  'e_area_name'=> $cek3->e_area_name,
										  'terimatunai'=> $terimatunai,
										  'terimagiro'=> $terimagiro,
										  'terimaku'=> $terimaku,
										  'keluartunai'=> $keluartunai,
										  'keluargiro'=> $keluargiro,
										  'keluarku'=> $keluarku,
										  'sahirt'=> $sahirt,
										  'sahirg'=> $sahirg,
										  'sahirk'=> $sahirk
									  );
					  $terimatunai= 0;
					  $terimagiro	= 0;
					  $terimaku 	= 0;
					  $keluartunai= 0;
					  $keluargiro	= 0;
					  $keluarku 	= 0;
					  $terimatunai= $terimatunai+$cek3->v_terima_tunai;
					  $terimagiro	= $terimagiro+$cek3->v_terima_giro;
					  $terimaku 	= $terimaku+$cek3->v_terima_ku;
					  $keluartunai= $keluartunai+$cek3->v_keluar_tunai;
					  $keluargiro	= $keluargiro+$cek3->v_keluar_giro;
#					  $keluarku 	= $keluarku+$cek3->v_keluar_ku;
					  $keluarku 	= $keluarku+$cek3->v_terima_ku;
				    $tempsahirt	= $tempsahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
				    $tempsahirg	= $tempsahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
				    $tempsahirk	= 0;#$tempsahirk+$cek3->v_terima_ku-$cek3->v_keluar_ku;
#				    echo '7. '.$tempsahirt.'<br>';
			    }elseif( ($buktitmp!=substr($cek3->i_bukti,0,8)) && ($buktitmp!='xxx') ){
				    $nama = $cek3->e_ikhp_typename;
				    if(strlen($cek3->i_coa)==5){
					    $coa  = $cek3->i_coa.$iarea;
				    }else{
			        $coa  = $cek3->i_coa;
				    }
			    	$jum_isi_baru[] = array('dbuktitmp'=> $dbuktitmp,
										  'buktitmp'=> $buktitmp,
										  'namatmp'=> $namatmp,
										  'coatmp'=> $coatmp,
										  'e_area_name'=> $cek3->e_area_name,
										  'terimatunai'=> $terimatunai,
										  'terimagiro'=> $terimagiro,
										  'terimaku'=> $terimaku,
										  'keluartunai'=> $keluartunai,
										  'keluargiro'=> $keluargiro,
										  'keluarku'=> $keluarku,
										  'sahirt'=> $sahirt,
										  'sahirg'=> $sahirg,
										  'sahirk'=> $sahirk
									  );
					
					  $terimatunai= 0;
					  $terimagiro	= 0;
					  $terimaku	  = 0;
					  $keluartunai= 0;
					  $keluargiro	= 0;
					  $keluarku	  = 0;
					  $terimatunai= $terimatunai+$cek3->v_terima_tunai;
					  $terimagiro	= $terimagiro+$cek3->v_terima_giro;
					  $terimaku	= $terimaku+$cek3->v_terima_ku;
					  $keluartunai= $keluartunai+$cek3->v_keluar_tunai;
					  $keluargiro	= $keluargiro+$cek3->v_keluar_giro;
#					  $keluarku	= $keluarku+$cek3->v_keluar_ku;
					  $keluarku	= $keluarku+$cek3->v_terima_ku;
				    $tempsahirt	= $tempsahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
				    $tempsahirg	= $tempsahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
				    $tempsahirk	= 0;#$tempsahirk+$cek3->v_terima_ku-$cek3->v_keluar_ku;
#				    echo '8. sahir='.$tempsahirt.'+'.$cek3->v_terima_tunai.'-'.$cek3->v_keluar_tunai.'<br>';
			    } 
			    $buktitmp=substr(trim($cek3->i_bukti),0,8);
			    $namatmp =$cek3->e_ikhp_typename;
          if(strlen($cek3->i_coa)==5){
            $coatmp  = substr($cek3->i_coa,0,5).$iarea;
          }else{
            $coatmp  = $cek3->i_coa;
          }
			    $dbuktitmp=$cek3->d_bukti;
			  }
#			  die;
      }
      if(!isset($cek3->e_area_name)){
        $this->db->select("* from tr_area where i_area = '$iarea'");
        $query = $this->db->get();
	      foreach($query->result() as $vv){
          $name=$vv->e_area_name;
        }
        $cek3->e_area_name=$name;
      }
			$jum_isi_baru[] = array('dbuktitmp'=> $dbuktitmp,
				'buktitmp'=> $buktitmp,
				'namatmp'=> $namatmp,
        'coatmp'=> $coatmp,
				'e_area_name'=> $cek3->e_area_name,
				'terimatunai'=> $terimatunai,
				'terimagiro'=> $terimagiro,
				'terimaku'=> $terimaku,
				'keluartunai'=> $keluartunai,
				'keluargiro'=> $keluargiro,
				'keluarku'=> $keluarku,
				'sahirt'=> $sahirt,
				'sahirg'=> $sahirg,
				'sahirk'=> $sahirk
			);
#			  echo $sahirt.'<br>';die;
			$data['tempsahirt'] = $tempsahirt;
			$data['tempsahirg'] = $tempsahirg;
			$data['tempsahirk'] = $tempsahirk;
			$cek_datatanpalimit= $this->mmaster->bacaperiodetanpalimit($iarea,$dfrom,$dto);
				
			$hitung_row = 0;
			$isi_baru = array();
			$batas_awal = $this->uri->segment(10);
			if ($batas_awal == '') $batas_awal = 0;
			$batas_akhir = $batas_awal+8; 
		  $buktitmp='xxx';
		  $namatmp ='yyy';
      $coatmp='zzz';
			$terimatunai= 0;
			$terimagiro	= 0;
			$terimaku 	= 0;
			$keluartunai= 0;
			$keluargiro	= 0;
			$keluarku 	= 0;
      if($cek_datatanpalimit){
			  foreach($cek_datatanpalimit as $cek3) {
			    if( ($hitung_row > $batas_akhir) && ($buktitmp!=substr($cek3->i_bukti,0,8)) )break;
				  $tmp=explode('-',$cek3->d_bukti);
				  $tgl=$tmp[2];
				  $bln=$tmp[1];
				  $thn=$tmp[0];
				  $cek3->d_bukti=$tgl.'-'.$bln.'-'.$thn;

#          echo $tempsahirt.'<br>';die;

  		    if(($buktitmp!=substr($cek3->i_bukti,0,8)) && $buktitmp=='xxx'){
					  $bukti=substr($cek3->i_bukti,0,7);
					  $nama = $cek3->e_ikhp_typename;
					  if(strlen($cek3->i_coa)==5){
						  $coa  = $cek3->i_coa.$iarea;
					  }else{
			        $coa  = $cek3->i_coa;
					  }
					  $terimatunai= $terimatunai+$cek3->v_terima_tunai;
					  $terimagiro	= $terimagiro+$cek3->v_terima_giro;
					  $terimaku	= $terimaku+$cek3->v_terima_ku;
					  $keluartunai= $keluartunai+$cek3->v_keluar_tunai;
					  $keluargiro	= $keluargiro+$cek3->v_keluar_giro;
#					  $keluarku	= $keluarku+$cek3->v_keluar_ku;
					  $keluarku	= $keluarku+$cek3->v_terima_ku;
					  $sahirt			= $sahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
					  $sahirg			= $sahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
					  $sahirk			= 0;#$sahirk+$cek3->v_terima_ku-$cek3->v_keluar_ku;
          }elseif( ($buktitmp==substr($cek3->i_bukti,0,8)) && ($buktitmp!='xxx') && ($buktitmp!='') ){
				    $bukti=substr($cek3->i_bukti,0,7);
				    $nama = $cek3->e_ikhp_typename;
					  if(strlen($cek3->i_coa)==5){
						  $coa  = $cek3->i_coa.$iarea;
					  }else{
              $coa  = $cek3->i_coa;
            }
					  $terimatunai= $terimatunai+$cek3->v_terima_tunai;
					  $terimagiro	= $terimagiro+$cek3->v_terima_giro;
					  $terimaku	= $terimaku+$cek3->v_terima_ku;
					  $keluartunai= $keluartunai+$cek3->v_keluar_tunai;
					  $keluargiro	= $keluargiro+$cek3->v_keluar_giro;
#					  $keluarku	= $keluarku+$cek3->v_keluar_ku;
					  $keluarku	= $keluarku+$cek3->v_terima_ku;
					  $sahirt			= $sahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
					  $sahirg			= $sahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
					  $sahirk			= 0;#$sahirk+$cek3->v_terima_ku-$cek3->v_keluar_ku;
			    }elseif( ($buktitmp==substr($cek3->i_bukti,0,8)) && ($buktitmp!='xxx') && ($buktitmp=='') ){
					  $nama = $cek3->e_ikhp_typename;
					  if(strlen($cek3->i_coa)==5){
						  $coa  = $cek3->i_coa.$iarea;
					  }else{
			        $coa  = $cek3->i_coa;
			      }
					  if ($hitung_row >= $batas_awal) {
						  $isi_baru[] = array('dbuktitmp'=> $dbuktitmp,
												          'buktitmp'=> $buktitmp,
												          'namatmp'=> $namatmp,
												          'coatmp'=> $coatmp,
												          'e_area_name'=> $cek3->e_area_name,
												          'terimatunai'=> $terimatunai,
												          'terimagiro'=> $terimagiro,
												          'terimaku'=> $terimaku,
												          'keluartunai'=> $keluartunai,
												          'keluargiro'=> $keluargiro,
												          'keluarku'=> $keluarku,
												          'sahirt'=> $sahirt,
												          'sahirg'=> $sahirg,
												          'sahirk'=> $sahirk
						  );
						}
					  $terimatunai= 0;
					  $terimagiro	= 0;
					  $terimaku 	= 0;
					  $keluartunai= 0;
					  $keluargiro	= 0;
					  $keluarku 	= 0;
					  $terimatunai= $terimatunai+$cek3->v_terima_tunai;
					  $terimagiro	= $terimagiro+$cek3->v_terima_giro;
					  $terimaku 	= $terimaku+$cek3->v_terima_ku;
					  $keluartunai= $keluartunai+$cek3->v_keluar_tunai;
					  $keluargiro	= $keluargiro+$cek3->v_keluar_giro;
#					  $keluarku 	= $keluarku+$cek3->v_keluar_ku;
					  $keluarku 	= $keluarku+$cek3->v_terima_ku;
				    $sahirt			= $sahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
				    $sahirg			= $sahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
				    $sahirk			= 0;#$sahirk+$cek3->v_terima_ku-$cek3->v_keluar_ku;
				    $hitung_row++;
          }elseif( ($buktitmp!=substr($cek3->i_bukti,0,8)) && ($buktitmp!='xxx') ){
					  $nama = $cek3->e_ikhp_typename;
					  if(strlen($cek3->i_coa)==5){
						  $coa  = $cek3->i_coa.$iarea;
					  }else{
			        $coa  = $cek3->i_coa;
			      }
				    if ($hitung_row >= $batas_awal) {
					    $isi_baru[] = array('dbuktitmp'=> $dbuktitmp,
								                  'buktitmp'=> $buktitmp,
								                  'namatmp'=> $namatmp,
								                  'coatmp'=> $coatmp,
								                  'e_area_name'=> $cek3->e_area_name,
								                  'terimatunai'=> $terimatunai,
								                  'terimagiro'=> $terimagiro,
								                  'terimaku'=> $terimaku,
								                  'keluartunai'=> $keluartunai,
								                  'keluargiro'=> $keluargiro,
								                  'keluarku'=> $keluarku,
								                  'sahirt'=> $sahirt,
								                  'sahirg'=> $sahirg,
								                  'sahirk'=> $sahirk
					    );
				    }
					  $terimatunai= 0;
					  $terimagiro	= 0;
					  $terimaku 	= 0;
					  $keluartunai= 0;
					  $keluargiro	= 0;
					  $keluarku	= 0;
					  $terimatunai= $terimatunai+$cek3->v_terima_tunai;
					  $terimagiro	= $terimagiro+$cek3->v_terima_giro;
					  $terimaku	= $terimaku+$cek3->v_terima_ku;
					  $keluartunai= $keluartunai+$cek3->v_keluar_tunai;
					  $keluargiro	= $keluargiro+$cek3->v_keluar_giro;
#					  $keluarku	= $keluarku+$cek3->v_keluar_ku;
					  $keluarku	= $keluarku+$cek3->v_terima_ku;
				    $sahirt			= $sahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
				    $sahirg			= $sahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
				    $sahirk			= 0;#$sahirk+$cek3->v_terima_ku-$cek3->v_keluar_ku;
  			    $hitung_row++;
			    } 
			    $buktitmp=substr($cek3->i_bukti,0,8);
			    $namatmp =$cek3->e_ikhp_typename;
			    $dbuktitmp=$cek3->d_bukti;
          if(strlen($cek3->i_coa)==5){
				    $coatmp=substr($cek3->i_coa,0,5).$iarea;
          }else{
				    $coatmp=$cek3->i_coa;
          }
  		  }
        $isi_baru[] = array('dbuktitmp'=> $dbuktitmp,
												    'buktitmp'=> $buktitmp,
												    'namatmp'=> $namatmp,
												    'coatmp'=> $coatmp,
												    'e_area_name'=> $cek3->e_area_name,
												    'terimatunai'=> $terimatunai,
												    'terimagiro'=> $terimagiro,
												    'terimaku'=> $terimaku,
												    'keluartunai'=> $keluartunai,
												    'keluargiro'=> $keluargiro,
												    'keluarku'=> $keluarku,
												    'sahirt'=> $sahirt,
												    'sahirg'=> $sahirg,
                            'sahirk'=> $sahirk
											    );
      }
      
#		  echo $sahirt.'<br>';die;      
      
			$config['total_rows'] = count($jum_isi_baru); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(10);
			$this->pagination->initialize($config);					 
			$data['isi_baru'] = $isi_baru;
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data IKHP Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listikhptunai/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
#			($this->session->userdata('menu525')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listikhp');
			$this->load->view('listikhptunai/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
#			($this->session->userdata('menu525')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dto		= $this->uri->segment(8);
			$dfrom	= $this->uri->segment(7);
			$iarea	= $this->uri->segment(6);
			$ispb		= $this->uri->segment(5);
			$inota	= $this->uri->segment(4);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$this->load->model('listikhptunai/mmaster');
			$this->mmaster->delete($inota,$ispb,$iarea);
			$config['base_url'] = base_url().'index.php/listikhptunai/cform/view/'.$inota.'/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							or upper(a.i_spb) like '%$cari%' 
							or upper(a.i_customer) like '%$cari%' 
							or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listikhp');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listikhptunai/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
#			($this->session->userdata('menu525')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listikhptunai/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
							where a.i_customer=b.i_customer 
							and a.f_ttb_tolak='f'
							and not a.i_nota isnull
							and (upper(a.i_nota) like '%$cari%' 
							  or upper(a.i_spb) like '%$cari%' 
							  or upper(a.i_customer) like '%$cari%' 
							  or upper(b.e_customer_name) like '%$cari%')
							and a.i_area='$iarea' and
							a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
							a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '13';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('listikhptunai/mmaster');
			$data['page_title'] = $this->lang->line('listikhp');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listikhptunai/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
#			($this->session->userdata('menu525')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listikhptunai/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
        $query = $this->db->query("select * from tr_area ",false);
      }else{			
        $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									   or i_area = '$area4' or i_area = '$area5'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listikhptunai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listikhptunai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
#			($this->session->userdata('menu525')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listikhptunai/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
      if($area1=='00'){
			  $query 	= $this->db->query("select * from tr_area
							    	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
							     	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
								  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
							     	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listikhptunai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listikhptunai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
