<?php
class Cform extends CI_Controller
{
   public $title  = "Daftar Kiriman Barang (SJP)";
   public $folder = "listdkbsjp";
   public $tujuan = "dkbsjp";

   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->model($this->folder . '/mmaster');
   }

   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu163') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data = [
            'page_title' => $this->title,
            'folder'     => $this->folder,
            'tujuan'     => $this->tujuan,
            'dfrom'      => '',
            'dto'        => '',
         ];

         $this->logger->writenew("Membuka Menu " . $this->title);

         $this->load->view($this->folder . '/vform', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function view()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu163') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $cari    = strtoupper($this->input->post('cari'));
         $dfrom   = $this->input->post('dfrom');
         $dto     = $this->input->post('dto');
         $iarea   = $this->input->post('iarea');

         if ($dfrom == '') $dfrom = $this->uri->segment(4);
         if ($dto == '') $dto = $this->uri->segment(5);
         if ($iarea == '') $iarea = $this->uri->segment(6);

         $config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/view/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';

         if ($iarea == "NA") {
            $query = $this->db->query("   SELECT
                                             a.i_dkb,
                                             a.d_dkb,
                                             b.e_area_name,
                                             a.e_sopir_name,
                                             a.i_kendaraan,
                                             sum(v_jumlah) AS jumlah,
                                             a.f_dkb_batal
                                          FROM
                                             tr_area b,
                                             tm_dkb_sjp a,
                                             tm_dkb_sjp_item c
                                          WHERE
                                             a.i_area = b.i_area
                                             AND a.i_dkb = c.i_dkb
                                             AND a.i_area = c.i_area
                                             AND b.i_area = c.i_area
                                             AND a.d_dkb BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
                                             AND (upper(a.i_area) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' OR upper(a.i_dkb) LIKE '%$cari%')
                                          GROUP BY
                                             a.i_dkb,
                                             a.d_dkb,
                                             b.e_area_name,
                                             a.e_sopir_name,
                                             a.i_kendaraan,
                                             a.f_dkb_batal
                                          ORDER BY
                                             a.i_dkb DESC ", false);
         } else {
            $query = $this->db->query("   SELECT
                                             a.i_dkb,
                                             a.d_dkb,
                                             b.e_area_name,
                                             a.e_sopir_name,
                                             a.i_kendaraan,
                                             sum(v_jumlah) AS jumlah,
                                             a.f_dkb_batal
                                          FROM
                                             tr_area b,
                                             tm_dkb_sjp a,
                                             tm_dkb_sjp_item c
                                          WHERE
                                             a.i_area = b.i_area
                                             AND a.i_dkb = c.i_dkb
                                             AND a.i_area = c.i_area
                                             AND b.i_area = c.i_area
                                             AND a.i_area = '$iarea'
                                             AND a.d_dkb BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
                                             AND (upper(a.i_area) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' OR upper(a.i_dkb) LIKE '%$cari%')
                                          GROUP BY
                                             a.i_dkb,
                                             a.d_dkb,
                                             b.e_area_name,
                                             a.e_sopir_name,
                                             a.i_kendaraan,
                                             a.f_dkb_batal
                                          ORDER BY
                                             a.i_dkb DESC ", false);
         }

         $config['total_rows']   = $query->num_rows();
         $config['per_page']     = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']     = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $data = [
            'page_title'   => $this->title,
            'folder'       => $this->folder,
            'tujuan'       => $this->tujuan,
            'cari'         => $cari,
            'dfrom'        => $dfrom,
            'dto'          => $dto,
            'iarea'        => $iarea,
            'isi'          => $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari),
         ];

         $this->logger->writenew('Membuka Data DKB Area ' . $iarea . ' Periode:' . $dfrom . ' s/d ' . $dto);

         $this->load->view($this->folder . '/vformview', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function cari()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu163') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $cari    = strtoupper($this->input->post('cari'));
         $dfrom   = $this->input->post('dfrom');
         $dto     = $this->input->post('dto');
         $iareasj = $this->input->post('iarea');
         $iarea   = $this->session->userdata("i_area");

         if ($dfrom == '') $dfrom = $this->uri->segment(4);
         if ($dto == '') $dto = $this->uri->segment(5);
         if ($iarea == '') $iarea   = $this->uri->segment(6);

         $config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/view/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';

         if ($iarea == "NA") {
            $query = $this->db->query("   SELECT
                                             a.i_dkb,
                                             a.d_dkb,
                                             b.e_area_name,
                                             a.e_sopir_name,
                                             a.i_kendaraan,
                                             sum(v_jumlah) AS jumlah,
                                             a.f_dkb_batal
                                          FROM
                                             tr_area b,
                                             tm_dkb_sjp a,
                                             tm_dkb_sjp_item c
                                          WHERE
                                             a.i_area = b.i_area
                                             AND a.i_dkb = c.i_dkb
                                             AND a.i_area = c.i_area
                                             AND b.i_area = c.i_area
                                             AND a.d_dkb BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
                                             AND (upper(a.i_area) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' OR upper(a.i_dkb) LIKE '%$cari%')
                                          GROUP BY
                                             a.i_dkb,
                                             a.d_dkb,
                                             b.e_area_name,
                                             a.e_sopir_name,
                                             a.i_kendaraan,
                                             a.f_dkb_batal
                                          ORDER BY
                                             a.i_dkb DESC ", false);
         } else {
            $query = $this->db->query("   SELECT
                                             a.i_dkb,
                                             a.d_dkb,
                                             b.e_area_name,
                                             a.e_sopir_name,
                                             a.i_kendaraan,
                                             sum(v_jumlah) AS jumlah,
                                             a.f_dkb_batal
                                          FROM
                                             tr_area b,
                                             tm_dkb_sjp a,
                                             tm_dkb_sjp_item c
                                          WHERE
                                             a.i_area = b.i_area
                                             AND a.i_dkb = c.i_dkb
                                             AND a.i_area = c.i_area
                                             AND b.i_area = c.i_area
                                             AND a.i_area = '$iarea'
                                             AND a.d_dkb BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
                                             AND (upper(a.i_area) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' OR upper(a.i_dkb) LIKE '%$cari%')
                                          GROUP BY
                                             a.i_dkb,
                                             a.d_dkb,
                                             b.e_area_name,
                                             a.e_sopir_name,
                                             a.i_kendaraan,
                                             a.f_dkb_batal
                                          ORDER BY
                                             a.i_dkb DESC ", false);
         }

         $config['total_rows']   = $query->num_rows();
         $config['per_page']     = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']     = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $data = [
            'page_title'   => $this->title,
            'folder'       => $this->folder,
            'tujuan'       => $this->tujuan,
            'cari'         => $cari,
            'dfrom'        => $dfrom,
            'dto'          => $dto,
            'iarea'        => $iarea,
            'isi'          => $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari, $iareasj),
         ];

         $this->load->view($this->folder . '/vmainform', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu163') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data['page_title'] = $this->title;

         $this->load->view($this->folder . '/vinsert_fail', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function delete()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu163') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $cari    = strtoupper($this->input->post('cari'));
         $dfrom   = $this->input->post('dfrom');
         $dto     = $this->input->post('dto');
         $idkb    = $this->input->post('idkb');
         $iarea   = $this->input->post('iarea');
         $iareax  = $this->input->post('iareax');

         if ($idkb == '') $idkb = $this->uri->segment(4);
         if ($iarea == '') $iarea = $this->uri->segment(5);
         if ($dfrom == '') $dfrom = $this->uri->segment(6);
         if ($dto == '') $dto = $this->uri->segment(7);
         if ($iareax == '') $iareax = $this->uri->segment(8);

         $this->mmaster->delete($idkb, $iarea);

         $this->logger->writenew('Hapus DKB Area ' . $iarea . 'No:' . $idkb);

         $config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/view/' . $dfrom . '/' . $dto . '/' . $iareax . '/index/';

         if ($iareax == "NA") {
            $query = $this->db->query("   SELECT
                                             a.i_dkb,
                                             a.d_dkb,
                                             b.e_area_name,
                                             a.e_sopir_name,
                                             a.i_kendaraan,
                                             sum(v_jumlah) AS jumlah,
                                             a.f_dkb_batal
                                          FROM
                                             tr_area b,
                                             tm_dkb_sjp a,
                                             tm_dkb_sjp_item c
                                          WHERE
                                             a.i_area = b.i_area
                                             AND a.i_dkb = c.i_dkb
                                             AND a.i_area = c.i_area
                                             AND b.i_area = c.i_area
                                             AND a.d_dkb BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
                                             AND (upper(a.i_area) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' OR upper(a.i_dkb) LIKE '%$cari%')
                                          GROUP BY
                                             a.i_dkb,
                                             a.d_dkb,
                                             b.e_area_name,
                                             a.e_sopir_name,
                                             a.i_kendaraan,
                                             a.f_dkb_batal
                                          ORDER BY
                                             a.i_dkb DESC ", false);
         } else {
            $query = $this->db->query("   SELECT
                                             a.i_dkb,
                                             a.d_dkb,
                                             b.e_area_name,
                                             a.e_sopir_name,
                                             a.i_kendaraan,
                                             sum(v_jumlah) AS jumlah,
                                             a.f_dkb_batal
                                          FROM
                                             tr_area b,
                                             tm_dkb_sjp a,
                                             tm_dkb_sjp_item c
                                          WHERE
                                             a.i_area = b.i_area
                                             AND a.i_dkb = c.i_dkb
                                             AND a.i_area = c.i_area
                                             AND b.i_area = c.i_area
                                             AND a.i_area = '$iareax'
                                             AND a.d_dkb BETWEEN to_date('$dfrom', 'dd-mm-yyyy') AND to_date('$dto', 'dd-mm-yyyy')
                                             AND (upper(a.i_area) LIKE '%$cari%' OR upper(b.e_area_name) LIKE '%$cari%' OR upper(a.i_dkb) LIKE '%$cari%')
                                          GROUP BY
                                             a.i_dkb,
                                             a.d_dkb,
                                             b.e_area_name,
                                             a.e_sopir_name,
                                             a.i_kendaraan,
                                             a.f_dkb_batal
                                          ORDER BY
                                             a.i_dkb DESC ", false);
         }

         $config['total_rows']   = $query->num_rows();
         $config['per_page']     = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']     = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $data['page_title']     = $this->title;
         $data['cari']           = $cari;
         $data['dfrom']          = $dfrom;
         $data['dto']            = $dto;
         $data['iarea']          = $iarea;
         $data['iareax']         = $iareax;
         $data['isi']            = $this->mmaster->bacaperiode($iareax, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari);

         $this->load->view($this->folder . '/vmainform', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }


   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu163') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/area/index/';

         $iuser   = $this->session->userdata('user_id');
         $query   = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $data = [
            'page_title'   => $this->lang->line('list_area'),
            'folder'       => $this->folder,
            'tujuan'       => $this->tujuan,
            'cari'         => '',
            'isi'          => $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser),
         ];

         $this->load->view($this->folder . '/vlistarea', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu163') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/area/index/';

         $cari    = $this->input->post('cari', FALSE);
         $cari    = strtoupper($cari);
         $iuser   = $this->session->userdata('user_id');

         $query   = $this->db->query(" select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
                                       and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']     = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']     = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $data = [
            'page_title'   => $this->lang->line('list_area'),
            'folder'       => $this->folder,
            'tujuan'       => $this->tujuan,
            'cari'         => $cari,
            'isi'          => $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser),
         ];

         $this->load->view($this->folder . '/vlistarea', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
}
