<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu181')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opbbmap/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query(" 	select distinct(b.i_spb) as no, a.d_spb as tgl, a.i_spb_old as asal, 
							b.i_area as i_area, c.e_area_name as e_area_name ,d.e_customer_name as e_customer_name
							from tm_spb_item b, tm_spb a, tr_customer_area c, tr_customer d
							where not a.i_approve1 isnull
							and not a.i_approve2 isnull
							and not a.i_store isnull
							and not a.i_store_location isnull
							and a.f_spb_op = 't'
							and a.f_spb_stockdaerah='f'
							and a.i_nota isnull
							and (upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%' 
							  or upper(d.i_customer) like '%$cari' or  upper(d.e_customer_name) like '%$cari%')
							and a.i_spb=b.i_spb and a.i_area=b.i_area and (b.n_order>b.n_deliver and b.i_op isnull)
							and d.i_customer=c.i_customer and d.i_customer=a.i_customer
							and a.i_customer=c.i_customer
						
							union all

							select distinct(b.i_spmb) as no, a.d_spmb as tgl, 
							a.i_spmb_old as asal, a.i_area as i_area, c.e_area_name as e_area_name, '' as e_customer_name
							from tm_spmb_item b, tm_spmb a, tr_area c
							where not a.i_approve2 isnull
							and not a.i_store isnull
							and not a.i_store_location isnull
							and a.f_op = 't'
							and (b.i_op isnull and b.n_deliver<b.n_order)
							and upper(a.i_spmb) like '%$cari%' 
							and a.i_spmb=b.i_spmb 
							and (upper(c.i_area) like '%$cari%'
							or upper(c.e_area_name) like '%$cari%')
							and a.i_area=c.i_area
							and a.f_spmb_close='f' ",false);
#not a.i_approve1 isnull
#							and
#							and (upper(c.i_area) like '%$cari%'
#							or upper(c.e_area_name) like '%$cari%')
#							and (b.i_op isnull and b.n_stock<b.n_order)
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			
			$data['page_title'] = $this->lang->line('opbbmap');
			$data['ispb']='';
			$data['iop']	= '';
			$data['jmlitem'] = ''; 				
			$data['detail']='';
			$this->load->model('opbbmap/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('opbbmap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu181')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_spb');
			$this->load->view('opbbmap/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
		    (($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu181')=='t')) ||
		    (($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('opbbmap');
			if($this->uri->segment(5)){
				$ispb 			= $this->uri->segment(4);
				$iarea			= $this->uri->segment(9);
				$isupplier		= $this->uri->segment(5);
				$isuppliergroup	= $this->uri->segment(8);
				$esuppliername 	= $this->uri->segment(6);
        $esuppliername=str_replace("%20"," ",$esuppliername);
				$nsuppliertoplength = $this->uri->segment(7);
				$tmp=explode('-',$ispb);
/*				if($isuppliergroup=='G0000'){
					if($tmp[0]=='SPB'){
						$query 	= $this->db->query("select a.* from tm_spb_item a, tr_product b, tr_supplier c
													where a.i_spb = '$ispb'
													and a.i_area='$iarea'
													and a.i_product=b.i_product
													and b.i_supplier=c.i_supplier
													and c.i_supplier_group='$isuppliergroup'
													and a.i_op isnull");
					}else if($tmp[0]=='SPMB'){
						$query 	= $this->db->query("select a.* from tm_spmb_item a, tr_product b, tr_supplier c
													where a.i_spmb = '$ispb'
													and a.i_product=b.i_product
													and b.i_supplier=c.i_supplier
													and c.i_supplier_group='$isuppliergroup'
													and a.i_op isnull");
					}
				}else{
*/
					if($tmp[0]=='SPB'){
						$query 	= $this->db->query("select a.* from tm_spb_item a, tr_product b
													where a.i_spb = '$ispb'
													and a.i_area='$iarea'
													and a.i_product=b.i_product
													and a.n_deliver<a.n_order
													and a.i_op isnull");
					}else if($tmp[0]=='SPMB'){
						$query 	= $this->db->query("select a.* from tm_spmb_item a, tr_product b
													where a.i_spmb = '$ispb'
													and a.i_product=b.i_product
													and a.n_deliver<a.n_order
													and a.i_op isnull");
						$data['ispmbold'] 	  = $ispb;
					}
//				}
//													and b.i_supplier='$isupplier'
				$data['jmlitem']  = $query->num_rows(); 				
				$data['iop']	  = '';
				$data['ispb'] 	  = $ispb;
				$data['isupplier']= $isupplier;
				$data['esuppliername']	    = $esuppliername;
				$data['nsuppliertoplength'] = $nsuppliertoplength;
				$this->load->model('opbbmap/mmaster');
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea,$isupplier,$isuppliergroup);
		 		$this->load->view('opbbmap/vmainform',$data);
			}else{
				$this->load->view('opbbmap/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function editop()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu181')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('op')." Update";
			if(	
				($this->uri->segment(4)) && ($this->uri->segment(5)) 
			  )
			{
				$ispb   	= $this->uri->segment(4);
				$iop		= $this->uri->segment(5);
				$isupplier	= $this->uri->segment(6);
				$area		= $this->uri->segment(7);
				$query 		= $this->db->query("select * from tm_op_item where i_op = '$iop'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ispb'] 		= $ispb;
				$data['iop'] 		= $iop;
				$data['supplier']	= $isupplier;
				$this->load->model('opbbmap/mmaster');
				$data['isi']=$this->mmaster->bacaop($iop,$area);
				$data['detail']=$this->mmaster->bacadetailop($iop,$area);
		 		$this->load->view('opbbmap/vmainform',$data);
			}else{
				$this->load->view('opbbmap/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu181')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iop		= $this->input->post('iop', TRUE);
			$iopold		= $this->input->post('iopold', TRUE);
			$isupplier 	= $this->input->post('isupplier', TRUE);
			$iarea 		= $this->input->post('iarea', TRUE);
			$iopstatus 	= $this->input->post('iopstatus', TRUE);
			$ireff		= $this->input->post('ispb', TRUE);
			$dop		= $this->input->post('dop', TRUE);
			$old		= $this->input->post('asal', TRUE);
			if($dop!=''){
				$tmp=explode("-",$dop);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dop=$th."-".$bl."-".$hr;
#        $thbl=substr($th,2,2).$bl;
        $thbl=$th.$bl;
			}
			$dreff		= $this->input->post('dspb', TRUE);
			if($dreff!=''){
				$tmp=explode("-",$dreff);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dreff=$th."-".$bl."-".$hr;
			}
			$eopremark		= $this->input->post('eopremark', TRUE);
			if($eopremark=='')
				$eopremark=null;
			$ndeliverylimit	= $this->input->post('ndeliverylimit', TRUE);
			$ntoplength		= $this->input->post('ntoplength', TRUE);
			$jml			= $this->input->post('jml', TRUE);
			if(($isupplier!='') && ($iopstatus!='') && ($dop!=''))
			{
				$this->db->trans_begin();
				$this->load->model('opbbmap/mmaster');
				$iop	= $this->mmaster->runningnumber($thbl);
				$this->mmaster->insertheader($iop, $dop, $isupplier, $iarea, $iopstatus, $ireff, 
							     $eopremark, $ndeliverylimit, $ntoplength, $dreff, $old, $iopold);
				for($i=1;$i<=$jml;$i++){
				  $norder				=$this->input->post('norder'.$i, TRUE);
				  if($norder!='0'){
					$iproduct			=$this->input->post('iproduct'.$i, TRUE);
					$iproductgrade			= 'A';
					$iproductmotif			= $this->input->post('motif'.$i, TRUE);
					$eproductname			= $this->input->post('eproductname'.$i, TRUE);
					$vproductmill			= $this->input->post('vproductmill'.$i, TRUE);
					$vproductmill			= str_replace(',','',$vproductmill);
					$norder				= $this->input->post('norder'.$i, TRUE);
					$nquantitystock			= $this->input->post('nquantitystock'.$i, TRUE);
					$this->mmaster->insertdetail( $iop,$iproduct,$iproductgrade,$eproductname,$norder,
												$vproductmill,$iproductmotif,$i);
					$this->mmaster->updatespb($ireff,$iop,$iproduct,$iproductgrade,$iproductmotif,$nquantitystock,$iarea);
				  }
				}
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		=  $this->db->query($sql);
			    if($rs->num_rows>0){
				    foreach($rs->result() as $tes){
					    $ip_address	  = $tes->ip_address;
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }

			    $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
			    $data['host']	= $ip_address;
			    $data['uri']	= $this->session->userdata('printeruri');
			    $data['iarea']	= $iarea;
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Input OP BBM AP Area '.$iarea.' No:'.$iop;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $iop;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function updateop()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu181')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iop		= $this->input->post('iop', TRUE);
			$iopold		= $this->input->post('iopold', TRUE);
			$isupplier 	= $this->input->post('isupplier', TRUE);
			$iarea 		= $this->input->post('iarea', TRUE);
			$iopstatus 	= $this->input->post('iopstatus', TRUE);
			$ireff		= $this->input->post('ispb', TRUE);
			$dop		= $this->input->post('dop', TRUE);
			$old		= $this->input->post('asal', TRUE);
			if($dop!=''){
				$tmp=explode("-",$dop);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dop=$th."-".$bl."-".$hr;
			}
			$dreff		= $this->input->post('dspb', TRUE);
			if($dreff!=''){
				$tmp=explode("-",$dreff);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dreff=$th."-".$bl."-".$hr;
			}
			$eopremark		= $this->input->post('eopremark', TRUE);
			if($eopremark=='')
				$eopremark=null;
			$ndeliverylimit	= $this->input->post('ndeliverylimit', TRUE);
			$ntoplength		= $this->input->post('ntoplength', TRUE);
			$jml			= $this->input->post('jml', TRUE);
			if(($isupplier!='') && ($iopstatus!='') && ($dop!=''))
				$benar			= 'false';
				$this->db->trans_begin();
				$this->load->model('opbbmap/mmaster');
#				$iop	= $this->mmaster->runningnumber();
				$this->mmaster->updateheader($iop, $dop, $isupplier, $iarea, $iopstatus, $ireff, 
											 $eopremark, $ndeliverylimit, $ntoplength, $dreff, $old, $iopold);
				for($i=1;$i<=$jml;$i++){
				  $norder					=$this->input->post('norder'.$i, TRUE);
				  if($norder!='0'){
					$iproduct				=$this->input->post('iproduct'.$i, TRUE);
					$iproductgrade			='A';
					$iproductmotif			=$this->input->post('motif'.$i, TRUE);
					$eproductname			=$this->input->post('eproductname'.$i, TRUE);
					$vproductmill			=$this->input->post('vproductmill'.$i, TRUE);
					$vproductmill			=str_replace(',','',$vproductmill);
					$norder					=$this->input->post('norder'.$i, TRUE);
					$data['iproduct']		=$iproduct;
					$data['iproductgrade']	=$iproductgrade;
					$data['iproductmotif']	=$iproductmotif;
					$data['eproductname']	=$eproductname;
					$data['vproductmill']	=$vproductmill;
					$data['norder']			=$norder;
					$this->mmaster->deletedetail($iproduct, $iproductgrade, $iop, $iproductmotif);
					$this->mmaster->insertdetail( $iop,$iproduct,$iproductgrade,$eproductname,$norder,
												$vproductmill,$iproductmotif,$i);
					$this->mmaster->updatespb($ireff,$iop,$iproduct,$iproductgrade,$iproductmotif,$iarea);
				  }
				}
#				$this->mmaster->updatespb($ireff,$iop);
				$benar='true';
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		=  $this->db->query($sql);
			    if($rs->num_rows>0){
				    foreach($rs->result() as $tes){
					    $ip_address	  = $tes->ip_address;
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }

			    $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
			    $data['host']	= $ip_address;
			    $data['uri']	= $this->session->userdata('printeruri');
			    $data['iarea']	= $area;
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Update OP BBM Ap Area '.$iarea.' No:'.$iop;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $iop;
					$this->load->view('nomor',$data);
				}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
    	($this->session->userdata('menu181')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari=strtoupper($this->input->post("cari"));
      $spb=strtoupper($this->input->post("spb"));
      $area=strtoupper($this->input->post("area"));
      if($spb==''){
  			$spb=$this->uri->segment(4);
      }
			$data['spb']=$spb;
      if($area==''){
  			$area=$this->uri->segment(5);
      }      
			$data['area']=$area;
			$config['base_url'] = base_url().'index.php/opbbmap/cform/supplier/'.$spb.'/'.$area.'/';
			$tmp=explode('-',$spb);
      $query = $this->db->query(" select i_supplier from tr_supplier where i_supplier_group='G0000'
                                  and (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('opbbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('opbbmap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu181')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opbbmap/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_supplier
									   where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%')
                     and i_supplier_group='G0000'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('opbbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('opbbmap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu181')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/opbbmap/cform/index/';*/

			$cari=str_replace("%20", " ", $this->input->post("cari",false));
			if($cari=='')$cari=str_replace("%20", " ", $this->uri->segment(4));
			if($cari=='zxvf'){
				$cari='';
				$config['base_url'] = base_url().'index.php/opbbmap/cform/cari/zxvf/';
			}else{
				$config['base_url'] = base_url().'index.php/opbbmap/cform/cari/'.$cari.'/';
			}

			$query = $this->db->query("	select distinct(b.i_spb) as no, b.i_area, c.e_area_name, d.e_customer_name 
										from tm_spb_item b, tm_spb a, tr_customer_area c, tr_customer d
										where not a.i_approve1 isnull
										and not a.i_approve2 isnull
										and not a.i_store isnull
										and not a.i_store_location isnull
										and a.f_spb_op = 't'
										and b.i_op isnull
										and a.i_nota isnull
										and (upper(a.i_spb) ilike '%$cari%' or upper(a.i_spb_old) ilike '%$cari%' or upper(c.e_area_name) ilike '%$cari' or  upper(d.e_customer_name) ilike '%$cari%')
										and a.i_spb=b.i_spb and b.n_order>b.n_deliver
										and d.i_customer=c.i_customer and d.i_customer=a.i_customer
										and a.i_customer=c.i_customer and a.i_area=b.i_area

										union all

										select distinct(b.i_spmb) as no , a.i_area, c.e_area_name as name, '' as e_customer_name
										from tm_spmb_item b, tm_spmb a, tr_area c
										where not a.i_approve2 isnull
										and not a.i_store isnull
										and not a.i_store_location isnull
										and a.f_op = 't'
										and b.i_op isnull
										and (upper(a.i_spmb) ilike '%$cari%' or upper(c.e_area_name) ilike '%$cari%' or upper(a.i_spmb_old) ilike '%$cari%')
										and a.i_spmb=b.i_spmb 
										and b.n_order>b.n_deliver
										and a.i_area=c.i_area
										and a.f_spmb_close='f' ",false);
# not a.i_approve1 isnull
#							and 
#										and b.n_order>b.n_stock
#										and (upper(c.i_area) like '%$cari%'
#										or upper(c.e_area_name) like '%$cari%')
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('opbbmap/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('opbbmap');
			$data['ispb']='';
			$data['iop']	= '';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('opbbmap/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function status()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu181')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opbbmap/cform/status/index/';
			$query = $this->db->query("select * from tr_op_status",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('opbbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_opstatus');
			$data['isi']=$this->mmaster->bacastatus($config['per_page'],$this->uri->segment(5));
			$this->load->view('opbbmap/vliststatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
