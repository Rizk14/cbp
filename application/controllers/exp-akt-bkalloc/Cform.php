<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-bk').'+Alokasi';
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('exp-akt-bkalloc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$lepel		= $this->session->userdata('i_area');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$icoabank		= $this->input->post('icoa');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($icoabank=='') $icoabank	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/exp-akt-bkalloc/cform/view/'.$dfrom.'/'.$dto.'/'.$icoabank.'/index/';
			
			$query = $this->db->query(" select	a.i_kbank, a.i_area, a.d_bank, a.i_coa, a.e_description, a.v_bank, a.i_periode, a.f_debet, a.f_posting, 
			                            a.i_cek, e.e_area_name, d.i_bank from tr_area e, tm_kbank a
			                            left join tm_pv_item b on(a.i_kbank=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='02')
			                            left join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv)
			                            left join tr_bank d on(c.i_coa=d.i_coa or a.i_coa_bank=d.i_coa)
			                            where (upper(a.i_kbank) like '%$cari%') and a.i_area=e.i_area and a.f_kbank_cancel='f'
			                            and a.d_bank >= to_date('$dfrom','dd-mm-yyyy') AND a.d_bank <= to_date('$dto','dd-mm-yyyy') 
			                            and a.i_coa_bank='$icoabank' or c.i_coa='$icoabank'
			                            ORDER BY a.i_kbank",false);

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->paginationxx->initialize($config);
			$data['page_title'] 	= $this->lang->line($icoabank);
			$this->load->model('exp-akt-bkalloc/mmaster');
			$data['cari']			= $cari;
			$data['dfrom']  		= $dfrom;
			$data['dto']			= $dto;
			$data['icoabank']		= $icoabank;
			$data['lepel']			= $lepel;
			$data['isi']			= $this->mmaster->bacaperiode($icoabank,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Kas Kecil Area '.$icoabank.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('exp-akt-bkalloc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			
			$lepel		= $this->session->userdata('i_area');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$icoabank	= $this->input->post('icoa');
			$ebankname	= $this->input->post('ebankname');

			if($dfrom=='') $dfrom 		= $this->uri->segment(4);
			if($dto=='') $dto 			= $this->uri->segment(5);
			if($icoabank=='') $icoabank	= $this->uri->segment(6);

			$this->load->model('exp-akt-bkalloc/mmaster');
      		$isi = $this->mmaster->bacaperiode($icoabank,$dfrom,$dto);
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();

			$objPHPExcel->getProperties()->setTitle("Bank ".$ebankname.'  '.$dfrom ." - ".$dto)->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      	if(count($isi)>0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12); //No voucher
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10); //No ref
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25); //Entry Bank
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25); //Keterangan
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10); //No perk
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8); //Debet
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8); //Kredit
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8); //Saldo
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10); //No Aloakasi
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10); //Tanggal Alokasi
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(13); //No Nota
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8); //Nilai Alokasi
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8); //Sisa
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8); //Area
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15); //Nama Area
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(25); //Entry
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:R1'
				);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(

							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Tanggal');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'No Voucher');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'No Referensi');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Entry Bank');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);				
			  $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'No Perkiraaan');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Debet');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Kredit');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('J1', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K1', 'No Alokasi');
				$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Tgl Alokasi');
				$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M1', 'No Nota');
				$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Jumlah');
				$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O1', 'Sisa');
				$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P1', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Nama Area');
				$objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('R1', 'Entry');
				$objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

        $i=2;
        $j=1;
        foreach($isi as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
		      array(
			      'font' => array(
				      'name'	=> 'Arial',
				      'bold'  => false,
				      'italic'=> false,
				      'size'  => 10
			      )
		      ),

		      'A'.$i.':R'.$i
		      );
		      
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $j, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->d_bank, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_rvb, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_kbank, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->d_bank, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->e_description, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->i_coa, Cell_DataType::TYPE_STRING);
          if($row->f_debet=='t'){
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->v_bank, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, 0, Cell_DataType::TYPE_NUMERIC);
          }elseif($row->f_debet=='f'){
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, 0, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->v_bank, Cell_DataType::TYPE_NUMERIC);
          }
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->f_debet, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->i_alokasi, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->d_alokasi, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->v_jumlah, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $row->v_sisa, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $row->e_area_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->d_entry, Cell_DataType::TYPE_STRING);
          	$j++;
			$i++;
          }

			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        	$nama='Bank+Alokasi'.$ebankname.' '.$dfrom .' - '.$dto.'.xls';
        if(file_exists('excel/'.$nama)){
          @chmod('excel/'.$nama, 0777);
          @unlink('excel/'.$nama);
        }
			  $objWriter->save('excel/00/'.$nama);
			  @chmod('excel/00/'.$nama, 0777);
			 
			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){

					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{

				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){

				  $now	  = $row['c'];
			  }
			  $pesan='Export Bank+Alokasi '.$ebankname.' '.$dfrom .' - '.$dto;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['sukses']			= true;
			  $data['inomor']			= $pesan;
			  $this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
  		}
    }
  }
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listgj');
			$this->load->view('exp-akt-bkalloc/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikb		= $this->uri->segment(4);
			$iperiode	= $this->uri->segment(5);
			$iarea		= $this->uri->segment(6);
			$dfrom	= $this->uri->segment(7);
			$dto		= $this->uri->segment(8);
      $lepel		= $this->session->userdata('i_area');
			$this->load->model('exp-akt-bkalloc/mmaster');
      $this->db->trans_begin();
			$this->mmaster->delete($iperiode,$iarea,$ikb);
      if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}
			else
			{
			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
	      	$now	  = $row['c'];
			  }
			  $pesan='Hapus Kas Kecil No:'.$ikb.' Area:'.$iarea;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan ); 
#				$this->db->trans_rollback();
				$this->db->trans_commit();
      }
			$cari		= strtoupper($this->input->post('cari'));
			if($dfrom=='') $dfrom=$this->uri->segment(7);
			if($dto=='') $dto=$this->uri->segment(8);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/exp-akt-bkalloc/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_kb
										              where (upper(i_kb) like '%$cari%') and f_close='f' 
										              and i_area='$iarea' and f_kb_cancel='f' and
										              d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
										              d_kb <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('listbk');
			$data['cari']		  = $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		  = $dto;
			$data['iarea']		= $iarea;
			$data['lepel']		= $lepel;
			$data['isi']		  = $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('exp-akt-bkalloc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/exp-akt-bkalloc/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_kb
										              where (upper(i_kb) like '%$cari%') and f_close='f' 
										              and i_area='$iarea' and f_kb_cancel='f' and
										              d_kb >= to_date('$dfrom','dd-mm-yyyy') AND
										              d_kb <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('listbk');
			$this->load->model('exp-akt-bkalloc/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('exp-akt-bkalloc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bank()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-akt-bkalloc/cform/bank/index/';
			$query = $this->db->query("select * from tr_bank",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-akt-bkalloc/mmaster');
			$data['page_title'] = $this->lang->line('list_bank');
			$data['isi']=$this->mmaster->bacabank($config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-akt-bkalloc/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caribank()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu502')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-akt-bkalloc/cform/bank/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query("select * from tr_bank where (upper(e_bank_name) like '%$cari%' or upper(i_bank) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-akt-bkalloc/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->caribank($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-akt-bkalloc/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
