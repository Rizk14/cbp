<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_knalokasi');
			$data['dfrom']    = '';
			$data['dto']      = '';
			$this->load->view('cekknalokasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select i_alokasi from tm_alokasikn a 
										              inner join tr_area on(a.i_area=tr_area.i_area)
										              inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										              where (upper(a.i_alokasi) like '%$cari%' or upper(a.i_customer) like '%$cari%')
										              and a.i_area='$iarea' and a.f_alokasi_cancel='f' and
										              a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
										              a.d_alokasi <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('list_knalokasi');
			$this->load->model('cekknalokasi/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
			$data['ialokasi']	= '';
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('cekknalokasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listpelunasan');
			$this->load->view('cekknalokasi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('cekknalokasi');
			if(
				($this->uri->segment(4)) && ($this->uri->segment(5))			
			  )
			{
				$ialokasi	= $this->uri->segment(4);
				$iarea    = $this->uri->segment(5);
				$ikn      = $this->uri->segment(6);
				$dfrom    = $this->uri->segment(7);
				$dto 	    = $this->uri->segment(8);
				$query 		= $this->db->query("select * from tm_alokasikn_item where i_alokasi = '$ialokasi' and i_area = '$iarea' and i_kn='$ikn'");
				$data['jmlitem']  = $query->num_rows(); 				
				$data['ialokasi']	= $ialokasi;
				$data['iarea']		= $iarea;
				$data['ikn']  		= $ikn;
				$data['dfrom']    = $dfrom;
				$data['dto']	    = $dto;
				$this->load->model('cekknalokasi/mmaster');
				$data['isi']=$this->mmaster->bacapl($iarea,$ialokasi,$ikn);
				$data['detail']=$this->mmaster->bacadetailpl($iarea,$ialokasi,$ikn);
		 		$this->load->view('cekknalokasi/vmainform',$data);
			}else{
				$this->load->view('cekknalokasi/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_alokasi a 
										inner join tr_area on(a.i_area=tr_area.i_area)
										inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										where (upper(a.i_customer) like '%$cari%' or upper(a.i_alokasi) like '%$cari%')
										and a.i_area='$iarea' and a.f_alokasi_cancel='f' and a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') 
										AND a.d_alokasi <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_knalokasi');

			$this->load->model('cekknalokasi/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
			$data['ialokasi']	= '';
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('cekknalokasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/area/index/';
      		$iuser   = $this->session->userdata('user_id');
     		$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('cekknalokasi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('cekknalokasi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/area/index/';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                              where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('cekknalokasi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('cekknalokasi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function updatepelunasan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('cekknalokasi/mmaster');
			$ialokasi 	= $this->input->post('ialokasi', TRUE);
			$ikbank 	= $this->input->post('ikbank', TRUE);
			$iarea	= $this->input->post('iarea', TRUE);
			$ecek1	= $this->input->post('ecek1',TRUE);
			if($ecek1=='')
				$ecek1=null;
			$user		=$this->session->userdata('user_id');
			$this->mmaster->updatecek($ecek1,$user,$ialokasi,$ikbank,$iarea);
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
	        while($row=pg_fetch_assoc($rs)){
		        $ip_address	  = $row['ip_address'];
		        break;
	        }
        }else{
	        $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
	        $now	  = $row['c'];
        }
        $pesan='Cek Alokasi No:'.$ialokasi.' Area:'.$iarea.' Bank='.$ikbank;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses'] = true;
				$data['inomor']	= $ialokasi;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function girocek()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/girocek/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_jenis_bayar",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('cekknalokasi/mmaster');
			$data['page_title'] = $this->lang->line('list_girocek');
			$data['isi']=$this->mmaster->bacagirocek($config['per_page'],$this->uri->segment(5));
			$this->load->view('cekknalokasi/vlistgirocek', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$idt 	= $this->uri->segment(4);
			$iarea 	= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/customer/'.$idt.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select distinct(a.i_customer), b.* from tm_dt_item a, tr_customer b
										where a.i_customer=b.i_customer
										and a.v_sisa>0
										and a.i_dt = '$idt' and a.i_area = '$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($idt,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['idt']=$idt;
			$data['iarea']=$iarea;
			$this->load->view('cekknalokasi/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/customer/'.$iarea.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_customer
									   	where i_area='$iarea' 
										and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
			$this->load->view('cekknalokasi/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function notaupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']		= $this->uri->segment(4);
			$data['iarea']		= $this->uri->segment(5);
			$data['idt']		= $this->uri->segment(6);
			$data['icustomer']	= $this->uri->segment(7);
			$baris				= $this->uri->segment(4);
			$iarea				= $this->uri->segment(5);
			$idt				= $this->uri->segment(6);
			$icustomer			= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
			$query = $this->db->query(" select i_customer_group from tr_customer where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_group;
      }
			$query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer b
										              where b.i_customer_group='$group' and a.i_customer=b.i_customer
										              and a.i_dt='$idt'
										              and a.i_area='$iarea'" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(9);
			$this->pagination->initialize($config);
			$this->load->model('cekknalokasi/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
			$this->load->view('cekknalokasi/vlistnotaupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function nota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']		= $this->uri->segment(4);
			$data['iarea']		= $this->uri->segment(5);
			$data['idt']		= $this->uri->segment(6);
			$data['icustomer']	= $this->uri->segment(7);
			$baris				= $this->uri->segment(4);
			$iarea				= $this->uri->segment(5);
			$idt				= $this->uri->segment(6);
			$icustomer			= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
			$query = $this->db->query(" select i_customer_group from tr_customer where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_group;
      }
			$query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer b
										              where b.i_customer_group='$group' and a.i_customer=b.i_customer
										              and a.i_dt='$idt'
										              and a.i_area='$iarea'" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(9);
			$this->paginationxx->initialize($config);
			$this->load->model('cekknalokasi/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
			$this->load->view('cekknalokasi/vlistnota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function giro()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/giro/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tm_giro 
										where i_customer = '$icustomer' 
										and i_area='$iarea'
										and (f_giro_tolak='f' and f_giro_batal='f')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('cekknalokasi/mmaster');
			$data['page_title'] = $this->lang->line('list_giro');
			$data['isi']=$this->mmaster->bacagiro($icustomer,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('cekknalokasi/vlistgiro', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kn()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/kn/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select * from tm_kn 
										where i_customer = '$icustomer' 
										and i_area='$iarea'
										and v_sisa>0",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('pelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_kn');
			$data['isi']=$this->mmaster->bacakn($icustomer,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('cekknalokasi/vlistkn', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function ku()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/ku/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';

      $query = $this->db->query(" select i_customer_group from tr_customer where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_group;
      }
      if($group!='G0000'){
			  $query = $this->db->query("	select a.* from tm_kum a, tr_customer b
										  where b.i_customer_group='$group' and a.i_customer=b.i_customer
										  and a.i_area='$iarea'
										  and a.v_sisa>0
										  and a.f_close='f' ",false);
			}else{
        $query = $this->db->query("	select a.* from tm_kum a
										  where a.i_customer='$icustomer'
										  and a.i_area='$iarea'
										  and a.v_sisa>0
										  and a.f_close='f' ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('cekknalokasi/mmaster');
			$data['page_title'] = $this->lang->line('list_ku');
      if($group!='G0000'){
  			$data['isi']=$this->mmaster->bacaku($icustomer,$iarea,$config['per_page'],$this->uri->segment(7),$group);
      }else{
  			$data['isi']=$this->mmaster->bacaku2($icustomer,$iarea,$config['per_page'],$this->uri->segment(7),$group);
      }
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('cekknalokasi/vlistku', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function lebihbayar()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 	= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cekknalokasi/cform/lebihbayar/'.$icustomer.'/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	SELECT 	i_dt, min(v_jumlah), min(v_lebih), i_area,
											d_bukti,i_dt||'-'||max(substr(i_pelunasan,9,2))
										from tm_pelunasan_lebih
										where i_customer = '$icustomer'
										and i_area='$iarea'
										and v_lebih>0
										group by i_dt, d_bukti, i_area",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('cekknalokasi/mmaster');
			$data['page_title'] = $this->lang->line('list_knalokasi');
			$data['isi']=$this->mmaster->bacapelunasan($icustomer,$iarea,$config['per_page'],$this->uri->segment(7));
			$data['icustomer']=$icustomer;
			$data['iarea']=$iarea;
			$this->load->view('cekknalokasi/vlistpelunasan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
