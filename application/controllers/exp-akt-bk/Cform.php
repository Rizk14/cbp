<?php
class Cform extends CI_Controller
{
    public $title   = "Export Bank";
    public $folder  = "exp-akt-bk";

    public function __construct()
    {
        parent::__construct();
        require_once "php/fungsi.php";
        $this->load->library('pagination');
        $this->load->helper('file');
        $this->load->model($this->folder . '/mmaster');
    }

    public function index()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu520') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $data = [
                'page_title'    => $this->title,
                'dfrom'         => '',
                'dto'           => '',
                'folder'        => $this->folder,
            ];

            $this->logger->writenew("Membuka Menu " . $this->title);

            $this->load->view($this->folder . '/vform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }

    public function bank()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu520') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $config['base_url'] = base_url() . 'index.php/exp-akt-bk/cform/bank/index/';
            $query = $this->db->query("select * from tr_bank", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page']     = $this->uri->segment(5);
            $this->pagination->initialize($config);

            $data['page_title'] = $this->lang->line('list_bank');
            $data['isi'] = $this->mmaster->bacabank($config['per_page'], $this->uri->segment(5));
            $this->load->view($this->folder . '/vlistbank', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }

    public function bankold()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu520') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $config['base_url'] = base_url() . 'index.php/exp-akt-bk/cform/bank/index/';
            $query = $this->db->query("select * from tr_bank", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(5);
            $this->pagination->initialize($config);

            $data['page_title'] = $this->lang->line('list_bank');
            $data['isi'] = $this->mmaster->bacabankold($config['per_page'], $this->uri->segment(5));
            $this->load->view($this->folder . '/vlistbank', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }

    public function caribank()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu520') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $config['base_url'] = base_url() . 'index.php/exp-akt-bk/cform/bank/index/';
            $cari = strtoupper($this->input->post('cari', false));
            $query = $this->db->query("select * from tr_bank where (upper(e_bank_name) like '%$cari%' or upper(i_bank) like '%$cari%')", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(5);
            $this->pagination->initialize($config);

            $data['page_title'] = $this->lang->line('list_area');
            $data['isi'] = $this->mmaster->caribank($cari, $config['per_page'], $this->uri->segment(5));
            $this->load->view($this->folder . '/vlistbank', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }

    public function export()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu520') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $dfrom      = $this->uri->segment(4);
            $dto        = $this->uri->segment(5);
            $icoabank   = $this->uri->segment(6);
            $ibank      = $this->uri->segment(7);

            $isi = $this->mmaster->bacaperiode($icoabank, $dfrom, $dto, $ibank);

            if ($dfrom != '') {
                $tmp = explode("-", $dfrom);
                $th = $tmp[2];
                $bl = $tmp[1];
                $hr = $tmp[0];
                $tgl = $th . "-" . $bl . "-" . $hr;
            }

            $tmp    = explode("-", $tgl);
            $det    = $tmp[2];
            $mon    = $tmp[1];
            $yir    = $tmp[0];
            $dsaldo = $yir . "/" . $mon . "/" . $det;
            $dtos   = $this->mmaster->dateAdd("d", -1, $dsaldo);

            $tmp    = explode("-", $dtos);
            $det1   = $tmp[2];
            $mon1   = $tmp[1];
            $yir1   = $tmp[0];
            $dtos   = $yir1 . "-" . $mon1 . "-" . $det1;

            $saldoawal = $this->mmaster->bacasaldo($dtos, $icoabank);

            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            $objPHPExcel = new PHPExcel();

            $namabank = '';

            #        foreach($isi as $row){
            #          $namabank=$row->e_bank_name;
            #          break;
            #        }
            $namabank = $this->mmaster->namabank($icoabank);

            $objPHPExcel->getProperties()->setTitle("Kas Bank " . $namabank . " Periode " . $dfrom . " s/d " . $dto)->setDescription(NmPerusahaan);
            $objPHPExcel->setActiveSheetIndex(0);

            if ($isi) {
                #    if (count($isi) > 0) {
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
                $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                    array(
                        'font' => array(
                            'name' => 'Arial',
                            'bold' => true,
                            'italic' => false,
                            'size' => 10,
                        ),
                        'alignment' => array(
                            'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                            'vertical' => Style_Alignment::VERTICAL_CENTER,
                            'wrap' => true,
                        ),
                    ),
                    'A1:K3'
                );

                $objPHPExcel->getActiveSheet()->setCellValue('A1', 'NO');
                $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('B1', 'TANGGAL');
                $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('C1', 'TANGGAL INPUT');
                $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('D1', 'NO.');
                $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('D2', 'REFF');
                $objPHPExcel->getActiveSheet()->setCellValue('E1', 'KETERANGAN');
                $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(

                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );

                $objPHPExcel->getActiveSheet()->setCellValue('F1', 'AREA');
                $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                // $objPHPExcel->getActiveSheet()->setCellValue('H2', 'AREA');

                $objPHPExcel->getActiveSheet()->setCellValue('G1', 'NO');
                $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('G2', 'PERK');

                $objPHPExcel->getActiveSheet()->setCellValue('H1', 'NAMA');
                $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('H2', 'COA');

                $objPHPExcel->getActiveSheet()->setCellValue('I1', 'DEBET');
                $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('I2', '(RP)');

                $objPHPExcel->getActiveSheet()->setCellValue('J1', 'KREDIT');
                $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('J2', '(RP)');

                $objPHPExcel->getActiveSheet()->setCellValue('K1', 'SALDO');
                $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(

                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );

                $objPHPExcel->getActiveSheet()->setCellValue('E3', 'Saldo Awal Per ' . substr($dfrom, 0, 2) . ' ' . mbulan(substr($dfrom, 3, 2)) . ' ' . substr($dfrom, 6, 4));
                $objPHPExcel->getActiveSheet()->getStyle('E3')->applyFromArray(
                    array(
                        'borders' => array(),
                    )
                );

                $objPHPExcel->getActiveSheet()->setCellValue('K3', $saldoawal);

                $i          = 4;
                $j          = 1;
                $cell       = 'B';
                $saldo      = 0;
                $vdebettmp  = 0;
                $vkredittmp = 0;
                $icoatmp    = '';
                $desctmp    = '';
                $dbanktmp   = '';

                foreach ($isi as $row) {
                    $dbanktmpx = $row->d_bank;

                    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                        array(
                            'font' => array(
                                'name' => 'Arial',

                                'bold' => false,
                                'italic' => false,
                                'size' => 10,
                            ),
                        ),

                        'A' . $i . ':K' . $i
                    );

                    if ($j == 1) {
                        $saldo = $saldoawal;
                    }

                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $j);
                    $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, date("d-m-Y", strtotime($row->d_bank)));
                    $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, date("d-m-Y", strtotime($row->d_entry)));
                    $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->i_reff);
                    $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $row->e_description);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->i_area . " - " . $row->e_area_name, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row->i_coa);
                    $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->e_coa_name);

                    $saldo = ($saldo + $row->v_debet) - $row->v_kredit;

                    $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->v_debet);
                    $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->v_kredit);
                    $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $saldo);

                    $vdebettmp  = $vdebettmp + $row->v_debet;
                    $vkredittmp = $vkredittmp + $row->v_kredit;

                    $i++;
                    $j++;
                }

                $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $vdebettmp);
                $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $vkredittmp);
                $objPHPExcel->getActiveSheet()->getStyle('I3:K' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);

                $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                    array(
                        'font' => array(
                            'name' => 'Arial',
                            'bold' => true,
                            'italic' => false,
                            'size' => 10,
                        ),
                        'alignment' => array(
                            'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                            'vertical' => Style_Alignment::VERTICAL_CENTER,
                            'wrap' => true,
                        ),
                    ),
                    'A' . $i . ':K' . $i
                );

                $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, 'Saldo Akhir');
                $objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray(
                    array(
                        'borders' => array(),
                    )
                );

                // $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $saldo);

                $objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
                $nama       = 'Lap.Bank_' . $namabank . '(' . $dfrom . '_sd_' . $dto . ').xls';

                if (file_exists('excel/' . $nama)) {
                    @chmod('excel/' . $nama, 0777);
                    @unlink('excel/' . $nama);
                }
                $objWriter->save('excel/00/' . $nama);

                $this->logger->writenew('Export Bank (' . $namabank . ') Tanggal ' . $dfrom . ' s/d ' . $dto);

                // Proses file excel    
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
                header('Cache-Control: max-age=0');

                $objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output', 'w');
            } else {
                $data['page_title'] = $this->title;
                $this->load->view($this->folder . '/vformfail', $data);
            }
        }
    }
}
