<?php 
class Cform extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        //$this->load->dbutil();
        $this->load->helper('file');
        require_once "php/fungsi.php";
    }
    public function index()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu520') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $data['page_title'] = $this->lang->line('exp-akt-bk');
            $data['dfrom'] = '';
            $data['dto'] = '';
            $this->load->view('exp-akt-bk/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function view()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu520') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $data['page_title'] = $this->lang->line('exp-akt-bk');
            $iperiodeawal = $this->input->post('iperiodeawal');
            $iperiodeakhir = $this->input->post('iperiodeakhir');
            if ($iperiodeawal == '') {
                $dfrom = $this->uri->segment(4);
            }

            if ($iperiodeakhir == '') {
                $dto = $this->uri->segment(5);
            }

            $this->load->model('exp-akt-bk/mmaster');
            $data['iperiodeawal'] = $iperiodeawal;
            $data['iperiodeakhir'] = $iperiodeakhir;
            $data['isi'] = $this->mmaster->bacaperiode($iperiodeawal, $iperiodeakhir);
            $sess = $this->session->userdata('session_id');
            $id = $this->session->userdata('user_id');
            $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs = pg_query($sql);
            if (pg_num_rows($rs) > 0) {
                while ($row = pg_fetch_assoc($rs)) {
                    $ip_address = $row['ip_address'];
                    break;
                }
            } else {
                $ip_address = 'kosong';
            }
            $query = pg_query("SELECT current_timestamp as c");
            while ($row = pg_fetch_assoc($query)) {
                $now = $row['c'];
            }
            $pesan = 'Menghitung plafond Periode:' . $iperiodeawal . ' s/d ' . $iperiodeakhir;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now, $pesan);
            $this->load->view('exp-akt-bk/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function insert_fail()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu520') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $data['page_title'] = $this->lang->line('exp-akt-bk');
            $this->load->view('exp-akt-bk/vinsert_fail', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function bank()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu520') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $config['base_url'] = base_url() . 'index.php/exp-akt-bk/cform/bank/index/';
            $query = $this->db->query("select * from tr_bank", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(5);
            $this->pagination->initialize($config);
            $this->load->model('exp-akt-bk/mmaster');
            $data['page_title'] = $this->lang->line('list_bank');
            $data['isi'] = $this->mmaster->bacabank($config['per_page'], $this->uri->segment(5));
            $this->load->view('exp-akt-bk/vlistbank', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function caribank()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu520') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $config['base_url'] = base_url() . 'index.php/exp-akt-bk/cform/bank/index/';
            $cari = strtoupper($this->input->post('cari', false));
            $query = $this->db->query("select * from tr_bank where (upper(e_bank_name) like '%$cari%' or upper(i_bank) like '%$cari%')", false);
            $config['total_rows'] = $query->num_rows();
            $config['per_page'] = '10';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Selanjutnya';
            $config['prev_link'] = 'Sebelumnya';
            $config['cur_page'] = $this->uri->segment(5);
            $this->pagination->initialize($config);
            $this->load->model('exp-akt-bk/mmaster');
            $data['page_title'] = $this->lang->line('list_area');
            $data['isi'] = $this->mmaster->caribank($cari, $config['per_page'], $this->uri->segment(5));
            $this->load->view('exp-akt-bk/vlistbank', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function export()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu520') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $dfrom = $this->input->post('dfrom');
            $dto = $this->input->post('dto');
            $icoabank = $this->input->post('icoa');
            $ibank = $this->input->post('ibank');
            $this->load->model('exp-akt-bk/mmaster');
            $isi = $this->mmaster->bacaperiode($icoabank, $dfrom, $dto, $ibank);
            if ($dfrom != '') {
                $tmp = explode("-", $dfrom);
                $th = $tmp[2];
                $bl = $tmp[1];
                $hr = $tmp[0];
                $tgl = $th . "-" . $bl . "-" . $hr;
            }
            $tmp = explode("-", $tgl);
            $det = $tmp[2];
            $mon = $tmp[1];
            $yir = $tmp[0];
            $dsaldo = $yir . "/" . $mon . "/" . $det;
            $dtos = $this->mmaster->dateAdd("d", -1, $dsaldo);
            $tmp = explode("-", $dtos);
            $det1 = $tmp[2];
            $mon1 = $tmp[1];
            $yir1 = $tmp[0];
            $dtos = $yir1 . "-" . $mon1 . "-" . $det1;

            $saldoawal = $this->mmaster->bacasaldo($dtos, $icoabank);
            $this->load->library('excel');
  			//$this->load->library('PHPExcel');
			 // $this->load->library('PHPExcel/IOFactory');
	  		$objPHPExcel = new PHPExcel();
            $namabank = '';
#        foreach($isi as $row){
            #          $namabank=$row->e_bank_name;
            #          break;
            #        }
            $namabank = $this->mmaster->namabank($icoabank);
            $objPHPExcel->getProperties()->setTitle("Kas Bank " . $namabank . " Periode " . $dfrom . " s/d " . $dto)->setDescription(NmPerusahaan);
            $objPHPExcel->setActiveSheetIndex(0);
            if (count($isi) > 0) {
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
                $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                    array(
                        'font' => array(
                            'name' => 'Arial',
                            'bold' => true,
                            'italic' => false,
                            'size' => 10,
                        ),
                        'alignment' => array(
                            'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                            'vertical' => Style_Alignment::VERTICAL_CENTER,
                            'wrap' => true,
                        ),
                    ),
                    'A1:J3'
                );

                $objPHPExcel->getActiveSheet()->setCellValue('A1', 'NO');
                $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('B1', 'TANGGAL');
                $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('C1', 'TANGGAL INPUT');
                $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('D1', 'NO.');
                $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('D2', 'REFF');
                $objPHPExcel->getActiveSheet()->setCellValue('E1', 'KETERANGAN');
                $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(

                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('F1', 'NO');
                $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('F2', 'PERK');
                $objPHPExcel->getActiveSheet()->setCellValue('G1', ' ');
                $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(

                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('H1', 'DEBET');
                $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(

                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('H2', '(RP)');
                $objPHPExcel->getActiveSheet()->setCellValue('I1', 'KREDIT');
                $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValue('I2', '(RP)');
                $objPHPExcel->getActiveSheet()->setCellValue('J1', 'SALDO');
                $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(

                    array(
                        'borders' => array(
                            'top' => array('style' => Style_Border::BORDER_THIN),
                        ),
                    )
                );

                $objPHPExcel->getActiveSheet()->setCellValue('E3', 'Saldo Awal Per ' . substr($dfrom, 0, 2) . ' ' . mbulan(substr($dfrom, 3, 2)) . ' ' . substr($dfrom, 6, 4));
                $objPHPExcel->getActiveSheet()->getStyle('E3')->applyFromArray(
                    array(
                        'borders' => array(
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('J3', $saldoawal, Cell_DataType::TYPE_NUMERIC);

                $i = 4;
                $j = 1;
                $cell = 'B';
                $saldo = 0;
                $icoatmp = '';
                $vdebettmp = 0;
                $vkredittmp = 0;
                $desctmp = '';
                $dbanktmp = '';
                foreach ($isi as $row) {
                    $dbanktmpx = $row->d_bank;
                    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                        array(
                            'font' => array(
                                'name' => 'Arial',

                                'bold' => false,
                                'italic' => false,
                                'size' => 10,
                            ),
                        ),

                        'A' . $i . ':J' . $i
                    );
                    if ($j == 1) {$saldo = $saldoawal;}
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $j, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, date("d-m-Y", strtotime($row->d_bank)), Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, date("d-m-Y", strtotime($row->d_entry)), Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_reff, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->e_description, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->i_coa, Cell_DataType::TYPE_STRING);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->i_area, Cell_DataType::TYPE_STRING);
                    $saldo = ($saldo + $row->v_debet) - $row->v_kredit;
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->v_debet, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row->v_kredit, Cell_DataType::TYPE_NUMERIC);
                    $objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $saldo, Cell_DataType::TYPE_NUMERIC);
                    $i++;
                    $j++;

                }

                $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                    array(
                        'font' => array(
                            'name' => 'Arial',
                            'bold' => true,
                            'italic' => false,
                            'size' => 10,
                        ),
                        'alignment' => array(
                            'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                            'vertical' => Style_Alignment::VERTICAL_CENTER,
                            'wrap' => true,
                        ),
                    ),
                    'A' . $i . ':J' . $i
                );
                $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, 'Saldo Akhir');
                $objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
                    array(
                        'borders' => array(
                        ),
                    )
                );
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $saldo, Cell_DataType::TYPE_NUMERIC);

                $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
                $nama = 'Bank ' . $namabank . ' periode' . $dfrom . '  ' . $dto . '.xls';
                if (file_exists('excel/' . $nama)) {
                    @chmod('excel/' . $nama, 0777);
                    @unlink('excel/' . $nama);
                }
                $objWriter->save('excel/00/' . $nama);
                $sess = $this->session->userdata('session_id');

                $id = $this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs = pg_query($sql);
                if (pg_num_rows($rs) > 0) {
                    while ($row = pg_fetch_assoc($rs)) {
                        $ip_address = $row['ip_address'];
                        break;
                    }
                } else {
                    $ip_address = 'kosong';
                }
                $query = pg_query("SELECT current_timestamp as c");
                while ($row = pg_fetch_assoc($query)) {

                    $now = $row['c'];
                }
                $pesan = 'Export Bank (' . $namabank . ') periode ' . $dfrom . ' s/d ' . $dto;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now, $pesan);

                $data['sukses'] = true;
                $data['inomor'] = $pesan;
                $data['folder'] = "exp-akt-bk";
                $this->load->view('nomor', $data);
            } else {
                $this->load->view('awal/index.php');
            }
        }
    }
}
