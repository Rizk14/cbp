<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu130')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('bb');
			$data['dfrom']		= '';
			$data['dto']		= '';
			$data['icoafrom']	= '';
			$data['icoato']	= '';
			$this->load->view('akt-bb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu130')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dfrom		= $this->uri->segment(4);
			$dto		= $this->uri->segment(5);
			$icoafrom	= $this->uri->segment(6);
			$icoato		= $this->uri->segment(7);
			if(
				($dfrom!='')&&($dto!='')&&($icoafrom!='')&&($icoato!='')
			  )
			{
				$this->load->model('akt-bb/mmaster');
				$tmp = explode("-", $dto);
				$det	= $tmp[0];
				$mon	= $tmp[1];
				$yir 	= $tmp[2];
				$dtos	= $yir."/".$mon."/".$det;
				$dtos	=$this->mmaster->dateAdd("d",1,$dtos);
				$tmp 	= explode("-", $dtos);
				$det1	= $tmp[2];
				$mon1	= $tmp[1];
				$yir1 	= $tmp[0];
				$dtos	= $det1."-".$mon1."-".$yir1;
				$data['page_title'] = $this->lang->line('bb');
				$data['dfrom']		= $dfrom;
				$data['dto']		= $dto;
				$data['icoafrom']	= $icoafrom;
				$data['icoato']		= $icoato;
				$data['isi']		= $this->mmaster->bacaperiode($dfrom,$dtos,$icoafrom,$icoato);

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Buka Buku Besar periode:'.$dfrom.' s/d '.$dto.' CoA:'.$icoafrom.' s/d '.$icoato;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

				$this->load->view('akt-bb/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function coafrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu130')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-bb/cform/coafrom/index/';
			$query = $this->db->query(" select * from tr_coa " ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-bb/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->bacacoa($config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-bb/vlistcoafrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricoafrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu130')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 				= $this->input->post('cari', FALSE);
			$cari				= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/akt-bb/cform/coafrom/index/';
			$query = $this->db->query(" select * from tr_coa where upper(i_coa) like '%$cari%' or e_coa_name like '%$cari%'" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-bb/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->caricoa($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-bb/vlistcoafrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function coato()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu130')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-bb/cform/coato/index/';
			$query = $this->db->query(" select * from tr_coa " ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-bb/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->bacacoa($config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-bb/vlistcoato', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricoato()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu130')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 				= $this->input->post('cari', FALSE);
			$cari				= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/akt-bb/cform/coato/index/';
			$query = $this->db->query(" select * from tr_coa where upper(i_coa) like '%$cari%' or e_coa_name like '%$cari%'" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-bb/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->caricoa($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('akt-bb/vlistcoato', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
