<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu279')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transdobl');
			$data['iperiode']	= '';
#			$data['iarea']	  = '';
			$this->load->view('transdobl/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu279')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
#      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
#			if($iarea=='') $iarea=$this->uri->segment(5);
			$data['page_title'] = $this->lang->line('transdobl');
			$data['iperiode']	= $iperiode;
      $tahun=substr($iperiode,0,4);      
      $per=substr($iperiode,2,4);
#			$data['iarea']	= $iarea;

      define ('BOOLEAN_FIELD',   'L');
      define ('CHARACTER_FIELD', 'C');
      define ('DATE_FIELD',      'D');
      define ('NUMBER_FIELD',    'N');
      define ('READ_ONLY',  '0');
      define ('WRITE_ONLY', '1');
      define ('READ_WRITE', '2');

      if(file_exists('beli/dopb'.$per.'.dbf'))
      {
         @chmod('beli/dopb'.$per.'.dbf', 0777);
         @unlink('beli/dopb'.$per.'.dbf');
      }

      $db_file = 'beli/dopb'.$per.'.dbf';
      $dbase_definition = array (
         array ('KODETRAN',  CHARACTER_FIELD,  2),
         array ('NODOK',  CHARACTER_FIELD,  6),
         array ('TGLDOK',  DATE_FIELD),
         array ('NOOP',  CHARACTER_FIELD,  6),
         array ('WILA',  CHARACTER_FIELD, 2),
         array ('NOLANG',  CHARACTER_FIELD,  3),
         array ('KOTOR',  NUMBER_FIELD,10, 0),
         array ('NOFAK',  CHARACTER_FIELD, 10),
         array ('KODEAREA',  CHARACTER_FIELD,  3),
         array ('KODEPROD',  CHARACTER_FIELD,  9),
         array ('JUMLAH', NUMBER_FIELD, 6, 0),
         array ('HARGASAT', NUMBER_FIELD, 8, 0)
      );
      /* $create = @ dbase_create($db_file, $dbase_definition) */
                /* or die ("Could not create dbf file <i>$db_file</i>."); */
      /* $id = @ dbase_open ($db_file, READ_WRITE) */
            /* or die ("Could not open dbf file <i>$db_file</i>.");  */
#      $per=substr($iperiode,2,4);
      $dicari="DO-".$per."-%";
      $sql	  = " select a.i_do, a.d_do, a.i_op, a.i_area, a.i_supplier, a.v_do_gross, a.f_do_cancel,
                  b.i_product, b.n_deliver, b.v_product_mill, b.e_product_name,
                  c.e_product_motifname
                  from tm_do a, tm_do_item b, tr_product_motif c, tm_op d
                  where a.i_do=b.i_do and a.i_do like '$dicari' and to_char(a.d_do,'yyyymm')='$iperiode' 
                  and d.i_reff like 'SPB%' and a.i_op=d.i_op and a.f_do_cancel='f' and a.v_do_gross>0
                  and b.i_product=c.i_product and b.i_product_motif=c.i_product_motif";
#and a.f_op_cancel='f' 
      $query=$this->db->query($sql);
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
	        $kodetran         = '01';
	        $nodok            = substr($rowsj->i_do,8,6);
	        $tgldok           = substr($rowsj->d_do,0,4).substr($rowsj->d_do,5,2).substr($rowsj->d_do,8,2);
	        $noop             = substr($rowsj->i_op,8,6);
	        $wila             = 'SP';#substr($rowsj->i_supplier,0,2);
	        $nolang           = substr($rowsj->i_supplier,2,3);
	        $kotor            = $rowsj->v_do_gross;
	        $nofak            = '';
	        $kodearea         = $rowsj->i_area;
          if($rowsj->f_do_cancel=='f'){ 
            $batal='F'; 
          }else{ 
            $batal='T';
          }
	        $kodeprod         = $rowsj->i_product.'00';
	        $jumlah           = $rowsj->n_deliver;
	        $hargasat         = $rowsj->v_product_mill;
	        $motif            = $rowsj->e_product_motifname;

          $isi = array ($kodetran,$nodok,$tgldok,$noop,$wila,$nolang,$kotor,$nofak,$kodearea,$kodeprod,$jumlah,$hargasat);
          /* dbase_add_record ($id, $isi) or die ("Gagal transfer ke file <i>$db_file</i>.");  */
        }
      }
      /* dbase_close($id); */
      @chmod('beli/dopb'.$per.'.dbf', 0777);      

      if(file_exists('beli/dost'.$per.'.dbf'))
      {
         @chmod('beli/dost'.$per.'.dbf', 0777);
         @unlink('beli/dost'.$per.'.dbf');
      }

      $db_file = 'beli/dost'.$per.'.dbf';

      $dbase_definition = array (
         array ('KODETRAN',  CHARACTER_FIELD,  2),
         array ('NODOK',  CHARACTER_FIELD,  6),
         array ('TGLDOK',  DATE_FIELD),
         array ('NOOP',  CHARACTER_FIELD,  6),
         array ('WILA',  CHARACTER_FIELD, 2),
         array ('NOLANG',  CHARACTER_FIELD,  3),
         array ('KOTOR',  NUMBER_FIELD,10, 0),
         array ('NOFAK',  CHARACTER_FIELD, 10),
         array ('KODEAREA',  CHARACTER_FIELD,  3),
         array ('KODEPROD',  CHARACTER_FIELD,  9),
         array ('JUMLAH', NUMBER_FIELD, 6, 0),
         array ('HARGASAT', NUMBER_FIELD, 8, 0)
      );
      /* $create = @ dbase_create($db_file, $dbase_definition) */
                /* or die ("Could not create dbf file <i>$db_file</i>."); */
      /* $id = @ dbase_open ($db_file, READ_WRITE) */
            /* or die ("Could not open dbf file <i>$db_file</i>.");  */
      $per=substr($iperiode,2,4);
      $dicari="DO-".$per."-%";
      $sql	  = " select a.i_do, a.d_do, a.i_op, a.i_area, a.i_supplier, a.v_do_gross, a.f_do_cancel,
                  b.i_product, b.n_deliver, b.v_product_mill, b.e_product_name,
                  c.e_product_motifname
                  from tm_do a, tm_do_item b, tr_product_motif c, tm_op d
                  where a.i_do=b.i_do and a.i_do like '$dicari' and to_char(a.d_do,'yyyymm')='$iperiode' 
                  and d.i_reff like 'SPMB%' and a.i_op=d.i_op and a.f_do_cancel='f' and a.v_do_gross>0
                  and b.i_product=c.i_product and b.i_product_motif=c.i_product_motif";
#and a.f_op_cancel='f' 
      $query=$this->db->query($sql);
  		if ($query->num_rows() > 0){
  			foreach($query->result() as $rowsj){
	        $kodetran         = '01';
	        $nodok            = substr($rowsj->i_do,8,6);
	        $tgldok           = substr($rowsj->d_do,0,4).substr($rowsj->d_do,5,2).substr($rowsj->d_do,8,2);
	        $noop             = substr($rowsj->i_op,8,6);
	        $wila             = 'SP';#$rowsj->i_area;
	        $nolang           = substr($rowsj->i_supplier,2,3);
	        $kotor            = $rowsj->v_do_gross;
	        $nofak            = '';
	        $kodearea         = $rowsj->i_area;
          if($rowsj->f_do_cancel=='f'){ 
            $batal='F'; 
          }else{ 
            $batal='T';
          }
	        $kodeprod         = $rowsj->i_product.'00';
	        $jumlah           = $rowsj->n_deliver;
	        $hargasat         = $rowsj->v_product_mill;
	        $motif            = $rowsj->e_product_motifname;

          $isi = array ($kodetran,$nodok,$tgldok,$noop,$wila,$nolang,$kotor,$nofak,$kodearea,$kodeprod,$jumlah,$hargasat);
          /* dbase_add_record ($id, $isi) or die ("Gagal transfer ke file <i>$db_file</i>.");  */
        }
      }
      /* dbase_close($id); */
      @chmod('beli/dost'.$per.'.dbf', 0777);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Transfer ke DO lama Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses']			= true;
			$data['inomor']			= $pesan;
			$this->load->view('nomor',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu279')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listspmb/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspmb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu279')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listspmb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspmb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
