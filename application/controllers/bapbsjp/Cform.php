<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bapbsjp');
			$this->load->model('bapbsjp/mmaster');
			$data['isi']    = '';
			$data['ibapb']  = '';
			$data['detail'] = '';
			$data['jmlitem']  ='';
			$data['detailx']  ='';
			$data['jmlitemx'] ='';
      $data['tgl']=date('d-m-Y');
			$this->load->view('bapbsjp/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dbapb 		= $this->input->post('dbapb', TRUE);
			if($dbapb!=''){
				$tmp=explode("-",$dbapb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbapb=$th."-".$bl."-".$hr;
        $thbl=substr($th,2,2).$bl;
			}
			$ibapbold 			= $this->input->post('ibapbold', TRUE);
			$ibapb	 				= $this->input->post('ibapb', TRUE);
			$eareaname 			= $this->input->post('eareaname', TRUE);
			$iarea	 				= $this->input->post('iarea', TRUE);
			$edkbkirim 			= $this->input->post('edkbkirim', TRUE);
			$idkbkirim 			= $this->input->post('idkbkirim', TRUE);
			$nbal						= $this->input->post('nbal', TRUE);		
			$nbal						= str_replace(',','',$nbal);
			$jml						= $this->input->post('jml', TRUE);
			$jmlx						= $this->input->post('jmlx', TRUE);
			$vbapb	= $this->input->post('vbapb', TRUE);
			$vbapb	= str_replace(',','',$vbapb);
			$vkirim	= $this->input->post('vkirim', TRUE);
			if($dbapb!='' && $iarea!='' && $idkbkirim!='' && $nbal!='' && $jml!='0' && $vbapb!='0' )# && $jmlx!='0')
			{
				$this->db->trans_begin();
				$this->load->model('bapbsjp/mmaster');
				$ibapb	=$this->mmaster->runningnumber($iarea,$thbl);
				$this->mmaster->insertheader($ibapb, $dbapb, $iarea, $idkbkirim, $nbal, $ibapbold, $vbapb, $vkirim);
				for($i=1;$i<=$jml;$i++){
				  $isj			= $this->input->post('isj'.$i, TRUE);
				  $dsj			= $this->input->post('dsj'.$i, TRUE);
				  if($dsj!=''){
						$tmp=explode("-",$dsj);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dsj=$th."-".$bl."-".$hr;
				  }
				  $vsj	= $this->input->post('vsj'.$i, TRUE);
    			$vsj	= str_replace(',','',$vsj);
				  $eremark	= $this->input->post('eremark'.$i, TRUE);
					if($eremark=='') $eremark=null;
				  $this->mmaster->insertdetail($ibapb,$iarea,$isj,$dbapb,$dsj,$eremark,$vsj);
				  $this->mmaster->updatesj($ibapb,$isj,$iarea,$dbapb);
				}
				for($i=1;$i<=$jmlx;$i++){
				  $iekspedisi	= $this->input->post('iekspedisi'.$i, TRUE);
					$eremark	= $this->input->post('eremarkx'.$i, TRUE);
				  $this->mmaster->insertdetailekspedisi($ibapb,$iarea,$iekspedisi,$dbapb,$eremark);
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();
#				    $this->db->trans_rollback();

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
			      while($row=pg_fetch_assoc($query)){
				      $now	  = $row['c'];
			      }
			      $pesan='Input BAPB-SJP No:'.$ibapb.' Area:'.$iarea;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan );

						$data['sukses']			= true;
						$data['inomor']			= $ibapb;
						$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bapbsjp');
			$this->load->view('bapbsjp/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$data['page_title'] = $this->lang->line('bapbsjp')." update";
			if($this->uri->segment(4)!=''){
				$ibapb 		= $this->uri->segment(4);
				$iarea		= $this->uri->segment(5);
				$dfrom		= $this->uri->segment(6);
				$dto 			= $this->uri->segment(7);
//				$icustomer= $this->uri->segment(8);
				$query = $this->db->query("select * from tm_bapbsjp_item where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ibapb'] 		= $ibapb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
//				$data['icustomer']= $icustomer;
				$query = $this->db->query("select * from tm_bapbsjp_ekspedisi where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows(); 				
				$this->load->model('bapbsjp/mmaster');
				$data['isi']=$this->mmaster->baca($ibapb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ibapb,$iarea);
				$data['detailx']=$this->mmaster->bacadetailx($ibapb,$iarea);
		 		$this->load->view('bapbsjp/vmainform',$data);
			}else{
				$this->load->view('bapbsjp/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$dbapb 		= $this->input->post('dbapb', TRUE);
			if($dbapb!=''){
				$tmp=explode("-",$dbapb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbapb=$th."-".$bl."-".$hr;
			}
			$ibapbold 			= $this->input->post('ibapbold', TRUE);
			$ibapb	 				= $this->input->post('ibapb', TRUE);
			$eareaname 			= $this->input->post('eareaname', TRUE);
			$iarea	 				= $this->input->post('iarea', TRUE);
			$edkbkirim 			= $this->input->post('edkbkirim', TRUE);
			$idkbkirim 			= $this->input->post('idkbkirim', TRUE);
//			$ecustomername	= $this->input->post('ecustomername', TRUE);
//			$icustomer			= $this->input->post('icustomer', TRUE);		
			$nbal						= $this->input->post('nbal', TRUE);		
			$nbal						= str_replace(',','',$nbal);
			$jml						= $this->input->post('jml', TRUE);
			$jmlx						= $this->input->post('jmlx', TRUE);
			$vbapb	= $this->input->post('vbapb', TRUE);
			$vbapb	= str_replace(',','',$vbapb);
			$vkirim	= $this->input->post('vkirim', TRUE);
			if($dbapb!='' && $iarea!='' && $idkbkirim!='' && $nbal!='' && $jml!='0' && $jmlx!='0' && $vbapb!='0')
			{
				$this->db->trans_begin();
        $area1	= $this->session->userdata('i_area');
        if($area1=='00'){
          $daer='f';
        }else{
          $daer='t';
        }
				$this->load->model('bapbsjp/mmaster');
				$this->mmaster->deleteheader($ibapb, $iarea);
				$this->mmaster->insertheader($ibapb, $dbapb, $iarea, $idkbkirim, $nbal, $ibapbold, $vbapb, $vkirim);
				for($i=1;$i<=$jml;$i++){
				  $isj			= $this->input->post('isj'.$i, TRUE);
				  $dsj			= $this->input->post('dsj'.$i, TRUE);
					if($dsj!=''){
						$tmp=explode("-",$dsj);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dsj=$th."-".$bl."-".$hr;
					}
				  $this->mmaster->deletedetail($ibapb,$iarea,$isj,$daer);
				  $vsj	= $this->input->post('vsj'.$i, TRUE);
    			$vsj	= str_replace(',','',$vsj);
				  $eremark	= $this->input->post('eremark'.$i, TRUE);
					if($eremark=='') $eremark=null;
				  $this->mmaster->insertdetail($ibapb,$iarea,$isj,$dbapb,$dsj,$eremark,$vsj);
				  $this->mmaster->updatesj($ibapb,$isj,$iarea,$dbapb);
				}
				for($i=1;$i<=$jmlx;$i++){
				  $iekspedisi	= $this->input->post('iekspedisi'.$i, TRUE);
					$eremark	= $this->input->post('eremarkx'.$i, TRUE);
					$this->mmaster->deletedetailekspedisi($ibapb,$iarea,$iekspedisi);
				  $this->mmaster->insertdetailekspedisi($ibapb,$iarea,$iekspedisi,$dbapb,$eremark);
				}				
				if ( ($this->db->trans_status() === FALSE) )
				{
			    $this->db->trans_rollback();
				}else{
			    $this->db->trans_commit();

		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Update BAPB-SJP No:'.$ibapb.' Area:'.$iarea;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ibapb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispmb	= $this->input->post('ispmbdelete', TRUE);
			$this->load->model('bapbsjp/mmaster');
			$this->mmaster->delete($ispmb);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';

      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
	      $now	  = $row['c'];
      }
      $pesan='Delete BAPB-SJP No:'.$ibapb.' Area:'.$iarea;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('bapbsjp');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('bapbsjp/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibapb		= $this->uri->segment(4);
			$ibapb  	= str_replace('%20','',$ibapb);
			$iarea		= $this->uri->segment(5);
  		$isj			= $this->uri->segment(6);
  		$dfrom		= $this->uri->segment(7);
  		$dto			= $this->uri->segment(8);
//  		$icustomer= $this->uri->segment(9);
			$this->db->trans_begin();
      $area1	= $this->session->userdata('i_area');
      if($area1=='00'){
        $daer='f';
      }else{
        $daer='t';
      }
			$this->load->model('bapbsjp/mmaster');
			$this->mmaster->deletedetail($ibapb, $iarea, $isj, $daer);
			if ($this->db->trans_status() === FALSE)
			{
		    $this->db->trans_rollback();
			}else{
		    $this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
          while($row=pg_fetch_assoc($rs)){
	          $ip_address	  = $row['ip_address'];
	          break;
          }
        }else{
          $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
          $now	  = $row['c'];
        }
        $pesan='Delete item bapb-sjp no:'.$ibapb.' Area:'.$iarea;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

				$data['page_title'] = $this->lang->line('bapbsjp')." update";
				$query = $this->db->query("select * from tm_bapbsjp_item where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ibapb'] 		= $ibapb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				//$data['icustomer']= $icustomer;
				$query = $this->db->query("select * from tm_bapbsjp_ekspedisi where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows(); 				
				$this->load->model('bapbsjp/mmaster');
				$data['isi']=$this->mmaster->baca($ibapb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ibapb,$iarea);
				$data['detailx']=$this->mmaster->bacadetailx($ibapb,$iarea);
		 		$this->load->view('bapbsjp/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetailekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibapb			= $this->uri->segment(4);
			$ibapb  	= str_replace('%20','',$ibapb);
			$iarea			= $this->uri->segment(5);
  		$iekspedisi	= $this->uri->segment(6);
  		$dfrom			= $this->uri->segment(7);
  		$dto				= $this->uri->segment(8);
//  		$icustomer	= $this->uri->segment(9);
			$this->db->trans_begin();
			$this->load->model('bapbsjp/mmaster');
			$this->mmaster->deletedetailekspedisi($ibapb, $iarea, $iekspedisi);
			if ($this->db->trans_status() === FALSE)
			{
			  $this->db->trans_rollback();
			}else{
			  $this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
          while($row=pg_fetch_assoc($rs)){
	          $ip_address	  = $row['ip_address'];
	          break;
          }
        }else{
          $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
          $now	  = $row['c'];
        }
        $pesan='Delete item ekspedisi bapb-sjp no:'.$ibapb.' Area:'.$iarea;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

				$data['page_title'] = $this->lang->line('bapbsjp')." update";
				$query = $this->db->query("select * from tm_bapbsjp_item where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ibapb'] 		= $ibapb;
				$data['iarea']		= $iarea;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
//				$data['icustomer']= $icustomer;
				$query = $this->db->query("select * from tm_bapbsjp_ekspedisi where i_bapb = '$ibapb' and i_area='$iarea'");
				$data['jmlitemx'] = $query->num_rows(); 				
				$this->load->model('bapbsjp/mmaster');
				$data['isi']=$this->mmaster->baca($ibapb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ibapb,$iarea);
				$data['detailx']=$this->mmaster->bacadetailx($ibapb,$iarea);
		 		$this->load->view('bapbsjp/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function sj()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['area']=$this->uri->segment(5);
			$area=$this->uri->segment(5);
/*
			if($this->uri->segment(6) && $this->uri->segment(6)!='index'){
				$data['customer']=$this->uri->segment(6);
				$icustomer=$this->uri->segment(6);
			}else{
				$icustomer='';
			}
			$data['icustomer']=$icustomer;
*/
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/sj/'.$baris.'/'.$area.'/index/';
      $area1	= $this->session->userdata('i_area');
		  if($area1=='00'){
			  $query = $this->db->query(" select a.i_sjp from tm_sjp a 
										  where a.i_area='$area' and a.i_bapb isnull ",false);
      }else{
			  $query = $this->db->query(" select a.i_sjp from tm_sjp a
										  where a.i_area='$area' and a.i_bapb isnull",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
//			if(isset($icustomer) && ($icustomer!='')){
//				$data['isi']=$this->mmaster->bacasj($icustomer,$area,$config['per_page'],$this->uri->segment(7));
//			}else{
				$data['isi']=$this->mmaster->bacasj2($area,$config['per_page'],$this->uri->segment(7));
//			}
			$data['baris']=$baris;
			$this->load->view('bapbsjp/vlistsj', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisj()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	= $this->session->userdata('i_area');
			$baris		= $this->input->post('baris', TRUE);
			$area			= $this->input->post('iarea', TRUE);
/*
			$icustomer	= $this->input->post('icustomer', TRUE);
			if($icustomer!=''){
				$data['customer']=$icustomer;
			}else{
				$icustomer='';
			}
			$data['icustomer']=$icustomer;
*/
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/sj/'.$baris.'/'.$area.'/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
		  if($area1=='00'){
			  $query = $this->db->query(" select a.i_sjp from tm_sjp a
										                where a.i_area='$area' and a.i_bapb isnull
										                and (upper(a.i_sjp) like '%$cari%')",false);
      }else{
			  $query = $this->db->query(" select a.i_sjp from tm_sjp a
										                where a.i_area='$area' and a.i_bapb isnull
										                and (upper(a.i_sjp) like '%$cari%')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->carisj($area,$cari,$config['per_page'],$this->uri->segment(7));
			$data['area']=$area;
			$data['baris']=$baris;
			$this->load->view('bapbsjp/vlistsj', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('bapbsjp/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('bapbsjp/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/area/'.$baris.'/sikasep/';
			$iuser = $this->session->userdata('user_id');
            $query = $this->db->query("select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area ", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
            $data['cari']='';
			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6),$iuser);
			$this->load->view('bapbsjp/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris   = $this->input->post('baris', FALSE);
            if($baris=='')$baris=$this->uri->segment(4);
            $cari   = ($this->input->post('cari', FALSE));
            if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
            if($cari!='sikasep')
            $config['base_url'] = base_url().'index.php/bapbsjp/cform/cariarea/'.$baris.'/'.$cari.'/';
              else
            $config['base_url'] = base_url().'index.php/bapbsjp/cform/cariarea/'.$baris.'/sikasep/';

            $iuser      = $this->session->userdata('user_id');
            $query = $this->db->query("select * from tr_area
                              where (upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['baris']=$baris;
            $data['cari']=$cari;
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6),$iuser);
			$this->load->view('bapbsjp/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kirim()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/kirim/index/';
			$query = $this->db->query("select * from tr_dkb_kirim ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('listkirim');
			$data['isi']=$this->mmaster->bacakirim($config['per_page'],$this->uri->segment(5));
			$this->load->view('bapbsjp/vlistkirim', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carikirim()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/kirim/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_dkb_kirim
						   where upper(i_dkb_kirim) like '%$cari%' 
						      or upper(e_dkb_kirim) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('listkirim');
			$data['isi']=$this->mmaster->carikirim($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('bapbsjp/vlistkirim', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function via()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/via/index/';
			$query = $this->db->query("select * from tr_dkb_via ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('listvia');
			$data['isi']=$this->mmaster->bacavia($config['per_page'],$this->uri->segment(5));
			$this->load->view('bapbsjp/vlistvia', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carivia()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/via/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_dkb_via
						   where upper(i_dkb_via) like '%$cari%' 
						      or upper(e_dkb_via) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('listvia');
			$data['isi']=$this->mmaster->carivia($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('bapbsjp/vlistvia', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function ekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/ekspedisi/'.$baris.'/';
			$query = $this->db->query("select * from tr_ekspedisi ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('listekspedisi');
			$data['isi']=$this->mmaster->bacaekspedisi($config['per_page'],$this->uri->segment(5));
			$data['baris']=$baris;
			$this->load->view('bapbsjp/vlistekspedisi', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariekspedisi()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris	=	$this->input->post('baris', FALSE);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/ekspedisi/'.$baris.'/';
			$query = $this->db->query("select * from tr_ekspedisi
																 where upper(i_ekspedisi) like '%$cari%' 
																		or upper(e_ekspedisi) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('listekspedisi');
			$data['isi']=$this->mmaster->cariekspedisi($cari,$config['per_page'],$this->uri->segment(5));
			$data['baris']=$baris;
			$this->load->view('bapbsjp/vlistekspedisi', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/customer/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_customer
									   where i_area = '$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('bapbsjp/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/customer/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$iarea = $this->uri->segment(4);
			$query = $this->db->query("select * from tr_customer
						   where i_area='$iarea' 
							 and (upper(i_customer) like '%$cari%' 
						      	  or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
			$this->load->view('bapbsjp/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function sjupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['area']=$this->uri->segment(5);
			$area=$this->uri->segment(5);
//			if($this->uri->segment(6) && $this->uri->segment(6)!='index'){
//				$data['customer']=$this->uri->segment(6);
//				$icustomer=$this->uri->segment(6);
//			}else{
//				$icustomer='';
//			}
//			$data['icustomer']=$icustomer;
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/sjupdate/'.$baris.'/'.$area.'/index/';
      $area1	= $this->session->userdata('i_area');
		  if($area1=='00'){
			  $query = $this->db->query(" select a.i_sj from tm_sj a 
										  where a.i_nota isnull and a.i_sj_type='01' and a.i_area_to='$area' 
                      and a.i_bapb isnull
                      and a.f_sj_daerah='f' ",false);
      }else{
			  $query = $this->db->query(" select a.i_sj from tm_sj a
										  where a.i_nota isnull and a.i_sj_type='01' and a.i_area_to='$area' 
                      and a.i_bapb isnull
                      and a.f_sj_daerah='t' ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
  		$data['isi']=$this->mmaster->bacasj2($area,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$this->load->view('bapbsjp/vlistsjupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisjupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris		= $this->input->post('baris', TRUE);
			$area			= $this->input->post('iarea', TRUE);
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/sjupdate/'.$baris.'/'.$area.'/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
      $area1	= $this->session->userdata('i_area');
		  if($area1=='00'){
			  $query = $this->db->query(" select a.i_sj from tm_sj a 
										  where a.i_nota isnull and a.i_sj_type='01' and a.i_area_to='$area' 
                      and a.i_bapb isnull and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
                      and a.f_sj_daerah='f' ",false);
      }else{
			  $query = $this->db->query(" select a.i_sj from tm_sj a
										  where a.i_nota isnull and a.i_sj_type='01' and a.i_area_to='$area' 
                      and a.i_bapb isnull and (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%')
                      and a.f_sj_daerah='t' ",false);
      }

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('list_sj');
			$data['isi']=$this->mmaster->carisj($area,$cari,$config['per_page'],$this->uri->segment(7));
			$data['area']=$area;
			$data['baris']=$baris;
			$this->load->view('bapbsjp/vlistsjupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function ekspedisiupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/ekspedisiupdate/'.$baris.'/';
			$query = $this->db->query("select * from tr_ekspedisi ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('listekspedisi');
			$data['isi']=$this->mmaster->bacaekspedisi($config['per_page'],$this->uri->segment(5));
			$data['baris']=$baris;
			$this->load->view('bapbsjp/vlistekspedisiupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariekspedisiupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu179')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris	=	$this->input->post('baris', FALSE);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/bapbsjp/cform/ekspedisiupdate/'.$baris.'/';
			$query = $this->db->query("select * from tr_ekspedisi
																 where upper(i_ekspedisi) like '%$cari%' 
																		or upper(e_ekspedisi) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bapbsjp/mmaster');
			$data['page_title'] = $this->lang->line('listekspedisi');
			$data['isi']=$this->mmaster->cariekspedisi($cari,$config['per_page'],$this->uri->segment(5));
			$data['baris']=$baris;
			$this->load->view('bapbsjp/vlistekspedisiupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
