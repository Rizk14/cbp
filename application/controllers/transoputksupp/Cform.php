<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu281')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transopkeexcel');
			$data['iperiode']	= '';
#			$data['iarea']	  = '';
			$this->load->model('transoputksupp/mmaster');
			$data['list_supplier'] = $this->mmaster->get_supplier();
			$this->load->view('transoputksupp/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu281')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
			$i_supplier	= $this->input->post('i_supplier');
			$areanya	= $this->input->post('area');
			/*if($i_supplier=='SP002'){
			$iarea = $this->input->post('iarea');
			}*/
			$iarea = $this->input->post('iarea');
			if($iperiode=='' && $i_supplier == '' && $areanya == ''){
				$iperiode=$this->uri->segment(4);
				$i_supplier=$this->uri->segment(5);
				$areanya=$this->uri->segment(6);
		    }
			$data['page_title'] = $this->lang->line('transoputksupp');
			$data['iperiode']	= $iperiode;
			$data['i_supplier']	= $i_supplier;
			$data['areanya']	= $areanya;
			/*if($i_supplier=='SP002'){
			$data['iarea']		= $iarea;
			}*/
			$data['iarea']		= $iarea;
			$tahun=substr($iperiode,0,4);
			$per=substr($iperiode,2,4);
			$dicari="OP-".$per."-%";
			$sql	  = " select a.i_supplier, a.i_op, a.d_op, a.d_op + cast(a.n_top_length as integer) as d_jt,
						a.i_reff, d_reff, a.n_delivery_limit, a.n_top_length, a.i_area ||' - ' || r.e_area_name /*||' - BCL ('||r.e_area_name ||')'*/ as i_area,
						a.n_op_print, a.i_op_status, a.e_op_remark, a.d_entry, a.f_op_cancel,
						b.i_product, b.n_order, b.n_delivery, b.v_product_mill, b.e_product_name,
						c.e_product_motifname
						from tm_op a, tm_op_item b, tr_product_motif c, tr_area r
						where a.i_op=b.i_op and a.i_op like '$dicari' and to_char(a.d_op,'yyyymm')='$iperiode' 
						and b.i_product=c.i_product and b.i_product_motif=c.i_product_motif
						and a.i_area=r.i_area
						AND a.i_supplier = '$i_supplier' ";
			if ($areanya == '1')
				$sql.= " AND a.i_area <> 'PB' ";
			else if ($areanya == '2')
				$sql.= " AND a.i_area = 'PB' ";
						if ($iarea == 'NA')
							$sql.= "AND a.i_area like '%%'";
						else if ($iarea != '' or $iarea != null)
							$sql.= " AND a.i_area = '$iarea' ";
				
			#and a.f_op_cancel='f' 
			$query=$this->db->query($sql);
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Export OP")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(8);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:Q1'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'KODELANG');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'KODEAREA');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOSPMB');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'TGLSPMB');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'NODOK');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'TGLDOK');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'KODEPROD');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'JUMLAH');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'HARGASAT');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'MOTIF');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K1', 'TGLJT');
				$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L1', 'TEMPO');
				$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M1', 'KET');
				$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N1', 'CETAK');
				$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O1', 'BTSKIRIM');
				$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P1', 'STAT');
				$objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'TGLPROSES');
				$objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('R1', 'JAM');
				$objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('S1', 'BATAL');
				$objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i=2;
				foreach($query->result() as $rowsj){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':Q'.$i
				  );
          
				$kodesupp         = $rowsj->i_supplier;
                $nodok            = substr($rowsj->i_op,8,6);
				$tmp=explode('-',$rowsj->d_op);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
                $tgldok           = $rowsj->d_op=$th.'-'.$bl.'-'.$hr;
                $nospmb           = substr($rowsj->i_reff,9,6);
                $tmp=explode('-',$rowsj->d_reff);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
                $tglspmb           = $rowsj->d_reff=$hr.'-'.$bl.'-'.$th;
                $tmp=explode('-',$rowsj->d_jt);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
                $tgljt           = $rowsj->d_jt=$hr.'-'.$bl.'-'.$th;
                $btskirim         = $rowsj->n_delivery_limit;
                $top              = $rowsj->n_top_length;
                $kodearea         = $rowsj->i_area;
                $cetak            = $rowsj->n_op_print;
                $stat             = $rowsj->i_op_status;
                $keterangan       = $rowsj->e_op_remark;
                $tglproses        = substr($rowsj->d_entry,0,4).substr($rowsj->d_entry,5,2).substr($rowsj->d_entry,8,2);
                $jam              = substr($rowsj->d_entry,11,2).":".substr($rowsj->d_entry,14,2).":".substr($rowsj->d_entry,17,2);
            if($rowsj->f_op_cancel=='f'){ 
                $batal='F';
            }
            else{ 
                $batal='T';
            }
            $kodeprod         = $rowsj->i_product.'00';
            $jumlah           = $rowsj->n_order;
            $jkirim           = $rowsj->n_delivery;
            $hargasat         = $rowsj->v_product_mill;
            $motif            = $rowsj->e_product_motifname;


          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $kodesupp, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $kodearea, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $nospmb, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $tglspmb, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $nodok, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $tgldok, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $kodeprod, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $jumlah, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $hargasat, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $motif, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $tgljt, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $top, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $keterangan, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $cetak, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $btskirim, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $stat, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $tglproses, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $jam, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $batal, Cell_DataType::TYPE_STRING);
					$i++;
				}
        $x=$i-1;
			}
      $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='op'.$kodesupp.' '.$iperiode.' '.$iarea.'.xls';
       if(file_exists('beli/Transfer/'.$nama)){
        @chmod('beli/Transfer/'.$nama, 0777);
        unlink('beli/Transfer/'.$nama);
     }
			$objWriter->save('beli/Transfer/'.$nama);
/*
      if(file_exists('excel/'.$area.'/'.$nama)){
        @chmod('excel/'.$area.'/'.$nama, 0777);
        unlink('excel/'.$area.'/'.$nama);
*/      

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
		  $data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Export OP '.$kodesupp.' Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$data['area']   = 'Transfer';
           	$data['ex']  = 'beli';
           	$data['folder']  = 'transoputksupp';
			$this->load->view('nomorexport',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
	      $iuser   = $this->session->userdata('user_id');
	      $isupplier = strtoupper($this->input->post('isupplier', FALSE));
	      $iperiode = strtoupper($this->input->post('iperiode', FALSE));
	      if($isupplier=='') $isupplier=$this->uri->segment(4);
	      if($iperiode=='') $iperiode=$this->uri->segment(5);
	      $periope = 'OP-'.substr($iperiode, 2,4).'-%';
			$config['base_url'] = base_url().'index.php/transoputksupp/cform/area/index/';
	      //$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
	      /*$query = $this->db->query("select * from tm_op where i_op like '$periope' and i_supplier='$isupplier' and i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area",false);*/
	      $query = $this->db->query("select a.i_area,a.e_area_name from tr_area a where a.i_area in (select i_area from tm_user_area where i_user='$iuser')
						and a.i_area in (select i_area from tm_op where i_op like '$periope' and i_supplier='$isupplier') order by a.i_area",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '12';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('transoputksupp/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6),$iuser,$isupplier,$periope);
			$this->load->view('transoputksupp/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu57')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transoputksupp/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transoputksupp/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('transoputksupp/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
