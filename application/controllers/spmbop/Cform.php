<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu51')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spmbop/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query= $this->db->query(" 	select distinct(b.i_spmb) as no ,a.* from tm_spmb_item b, tm_spmb a
							inner join tr_area on (a.i_area=tr_area.i_area) 
							where not a.i_approve2 isnull
							and not a.i_store isnull
							and not a.i_store_location isnull
							and a.f_op = 'f' and a.f_spmb_pemenuhan='f'
							and a.f_spmb_close = 'f'
							and a.f_spmb_cancel = 'f'
							and upper(a.i_spmb) like '%$cari%' 
							and a.i_spmb=b.i_spmb and b.n_acc>b.n_stock",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			
			$data['page_title'] = $this->lang->line('spmbop2');
			$data['ispmb']='';
			$data['jmlitem'] = ''; 				
			$data['detail']='';
			$this->load->model('spmbop/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('spmbop/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('master_spmb');
			$this->load->view('spmbop/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('opreq');
			if($this->uri->segment(4)!=''){
				$ispmb = $this->uri->segment(4);
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ispmb'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispmb'] = $ispmb;
				$this->load->model('spmbop/mmaster');
				$data['isi']=$this->mmaster->baca($ispmb);
				$data['detail']=$this->mmaster->bacadetail($ispmb);
		 		$this->load->view('spmbop/vmainform',$data);
			}else{
				$this->load->view('spmbop/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function spmbsiapsjp() 
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('opreq');

			if($this->uri->segment(4)!=''){
				$ispmb = $this->uri->segment(4);

				$config['base_url'] = base_url().'index.php/spmbop/cform/index/';
				$cari = strtoupper($this->input->post('cari', FALSE));
				$query= $this->db->query(" 	select distinct(b.i_spmb) as no ,a.* from tm_spmb_item b, tm_spmb a
								inner join tr_area on (a.i_area=tr_area.i_area) 
								where not a.i_approve2 isnull
								and not a.i_store isnull
								and not a.i_store_location isnull
								and a.f_op = 'f' and a.f_spmb_pemenuhan='f'
								and a.f_spmb_close = 'f'
								and a.f_spmb_cancel = 'f'
								and upper(a.i_spmb) like '%$cari%' 
								and a.i_spmb=b.i_spmb and b.n_acc>b.n_stock",false);
				$config['total_rows'] = $query->num_rows(); 
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(5);
				$this->pagination->initialize($config);
				
				$data['page_title'] = $this->lang->line('spmbop2');
				$data['ispmb']='';
				$data['jmlitem'] = ''; 				
				$data['detail']='';
				$tmp_ispmb=$ispmb;
				$this->load->model('spmbop/mmaster');
				$this->mmaster->spmbsiapsj($tmp_ispmb);
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5));
				$this->load->view('spmbop/vmainform', $data);

			}else{
				$this->load->view('spmbop/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if ($this->session->userdata('logged_in')){
			$ispmb 			= $this->input->post('ispmb', TRUE);
			$iarea 			= $this->input->post('iarea', TRUE);
			$fspmbop		= 't';
			$fspmbclose		= 'f';
			$fspmbcancel	= 'f';
			$this->db->trans_begin();
			$this->load->model('spmbop/mmaster');
			$this->mmaster->updateheader($ispmb,$fspmbop,$fspmbclose,$fspmbcancel);
			if ( ($this->db->trans_status() === FALSE) )
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();

      $sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Pemenuhan SPMB Area:'.$iarea.' No:'.$ispmb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $ispmb;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function isistore()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('spmbop2');
			if($this->input->post('ispmbedit')){
				$ispmb = $this->input->post('ispmbedit');
				$query = $this->db->query("select * from tm_spmb_item
							   where i_spmb = '$ispmb'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispmb'] = $ispmb;
				$this->load->model('spmbop/mmaster');
				$data['isi']=$this->mmaster->baca($ispmb);
				$data['detail']=$this->mmaster->bacadetail($ispmb);
		 		$this->load->view('spmbop/vmainform',$data);
			}else{
				$this->load->view('spmbop/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if ($this->session->userdata('logged_in')){
			$area=$this->session->userdata('i_area');
			$data['jml']=$this->uri->segment(4);
			$jml =$this->uri->segment(4);
			$data['spmb']=$this->uri->segment(5);
			$spmb =$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/spmbop/cform/store/'.$jml.'/'.$spmb.'/index/';
			$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
									   where a.i_store in(select i_store from tr_area 
									   where i_area='$area' or i_area='00')
									   and a.i_store=b.i_store
									   order by a.i_store",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('spmbop/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(7),$area);
			$this->load->view('spmbop/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/spmbop/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select a.*, b.* from tr_store a, tr_store_location b
									   where a.i_store in(select i_store from tr_area 
									   where i_area='$area' or i_area='00')
									   and a.i_store=b.i_store and upper(a.i_store) like '%cari%' 
						      		   or upper(a.e_store_name) like '%cari%' order by a.i_store",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spmbop/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('spmbop/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/spmbop/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query= $this->db->query(" 	select distinct(b.i_spmb) as no ,a.* from tm_spmb_item b, tm_spmb a
							inner join tr_area on (a.i_area=tr_area.i_area) 
							where not a.i_approve1 isnull
							and not a.i_approve2 isnull
							and not a.i_store isnull
							and not a.i_store_location isnull
							and a.f_op = 'f' and a.f_spmb_pemenuhan='f'
							and a.f_spmb_close = 'f'
							and a.f_spmb_cancel = 'f'
							and upper(a.i_spmb) like '%$cari%' 
							and a.i_spmb=b.i_spmb and b.n_acc>b.n_stock",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('spmbop2');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
      			$this->load->model('spmbop/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
	 		$this->load->view('spmbop/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
