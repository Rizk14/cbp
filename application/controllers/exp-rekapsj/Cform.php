<?php
class Cform extends CI_Controller
{

	public $folder = "exp-rekapsj";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
		$this->load->model('exp-rekapsj/mmaster');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu264') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('exprekapsj');
			$data['iperiode']	= '';
			$this->load->view('exp-rekapsj/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu264') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iperiode	= $this->input->post('iperiode');
			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}

			$a 		= substr($iperiode, 2, 2);
			$b 		= substr($iperiode, 4, 2);
			$no 	= 'SJ-' . $a . $b . '-%';

			$peri 	= mbulan($b) . " - " . $a;

			$this->db->select("	substring(a.i_sj, 9, 2) as i_area, a.i_sj, a.d_sj, a.i_nota, a.d_nota, c.d_approve2, c.d_approve1, a.i_spb, a.d_spb, 
								to_char(c.d_spb_storereceive,'yyyy-mm-dd') as d_spb_receive, a.v_nota_netto ,c.v_spb , a.i_customer, b.e_customer_name, a.d_sj_receive,
								CASE WHEN a.n_nota_toplength ISNULL THEN b.n_customer_toplength ELSE a.n_nota_toplength END AS n_nota_toplength, a.d_jatuh_tempo,
								a.d_dkb
								from tm_nota a, tr_customer b , tm_spb c
								where a.i_customer=b.i_customer and a.i_sj=c.i_sj and a.i_sj like '$no'
								and a.f_nota_cancel = 'f' and c.f_spb_cancel = 'f'
								order by substring(a.i_sj,9,2), i_sj ", false);
			$query = $this->db->get();

			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');

			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Rekap SJ ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(9);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A1:P1'
				);

				$style_row = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic' => false,
						'size'  => 10
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						// 'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical' => Style_Alignment::VERTICAL_CENTER,
						'wrap' => false
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'AREA');
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'NO SJ');
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'TGL SJ');
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'NO NOTA');
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'TGL DKB');
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'TGL TERIMA TOKO');

				// UNTUK DEPARTEMEN KEUANGAN & LEVEL ADMINISTRATOR
				if ($this->session->userdata('departement') == '4' || $this->session->userdata('level') == '0') {
					$objPHPExcel->getActiveSheet()->setCellValue('G1', 'TOP'); /* KEUANGAN */
					$objPHPExcel->getActiveSheet()->setCellValue('H1', 'TGL JT'); /* KEUANGAN */
					$objPHPExcel->getActiveSheet()->setCellValue('I1', 'SPB APP');
					$objPHPExcel->getActiveSheet()->setCellValue('J1', 'SPB');
					$objPHPExcel->getActiveSheet()->setCellValue('K1', 'TGL SPB');
					$objPHPExcel->getActiveSheet()->setCellValue('L1', 'TGL TERIMA SPB');
					$objPHPExcel->getActiveSheet()->setCellValue('M1', 'KODE PELANGGAN');
					$objPHPExcel->getActiveSheet()->setCellValue('N1', 'NAMA PELANGGAN');
					$objPHPExcel->getActiveSheet()->setCellValue('O1', 'NILAI SPB');
					$objPHPExcel->getActiveSheet()->setCellValue('P1', 'NILAI NOTA');
				} else {
					$objPHPExcel->getActiveSheet()->setCellValue('G1', 'SPB APP');
					$objPHPExcel->getActiveSheet()->setCellValue('H1', 'SPB');
					$objPHPExcel->getActiveSheet()->setCellValue('I1', 'TGL SPB');
					$objPHPExcel->getActiveSheet()->setCellValue('J1', 'TGL TERIMA SPB');
					$objPHPExcel->getActiveSheet()->setCellValue('K1', 'KODE PELANGGAN');
					$objPHPExcel->getActiveSheet()->setCellValue('L1', 'NAMA PELANGGAN');
					$objPHPExcel->getActiveSheet()->setCellValue('M1', 'NILAI SPB');
					$objPHPExcel->getActiveSheet()->setCellValue('N1', 'NILAI NOTA');
				}

				$i = 2;
				foreach ($query->result() as $row) {
					$row->d_sj 			= $row->d_sj != "" ? date('d-m-Y', strtotime($row->d_sj)) : "";
					$row->d_sj_receive 	= $row->d_sj_receive != "" ? date('d-m-Y', strtotime($row->d_sj_receive)) : "";
					$row->d_spb_receive = $row->d_spb_receive != "" ? date('d-m-Y', strtotime($row->d_spb_receive)) : "";

					$jt = $row->d_sj_receive == "" ? $row->d_sj : $row->d_sj_receive; /* JIKA TANGGAL TERIMA KOSONG YG DIAMBIL TGL SJ */
					$row->d_jatuh_tempo = $row->d_jatuh_tempo == "" ? date('d-m-Y', strtotime("+" . $row->n_nota_toplength . " day", strtotime($jt))) : date('d-m-Y', strtotime($row->d_jatuh_tempo));

					// Untuk Tanggal Approve
					if ($row->d_approve1 > $row->d_approve2) {
						$tglapprovespb = $row->d_approve1;
					} elseif ($row->d_approve2 > $row->d_approve1) {
						$tglapprovespb = $row->d_approve2;
					} else {
						$tglapprovespb = $row->d_approve1;
					}

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->i_area, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->i_sj);
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->d_sj);
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->i_nota);
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, date('d-m-Y', strtotime($row->d_dkb)));
					$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $row->d_sj_receive);

					if ($this->session->userdata('departement') == '4' || $this->session->userdata('level') == '0') {
						$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row->n_nota_toplength);
						$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->d_jatuh_tempo);
						$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, date('d-m-Y', strtotime($tglapprovespb)));
						$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->i_spb);
						$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, date('d-m-Y', strtotime($row->d_spb)));
						$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row->d_spb_receive);
						$objPHPExcel->getActiveSheet()->setCellValueExplicit('M' . $i, $row->i_customer, Cell_DataType::TYPE_STRING);
						$objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $row->e_customer_name);
						$objPHPExcel->getActiveSheet()->setCellValueExplicit('O' . $i, $row->v_spb, Cell_DataType::TYPE_NUMERIC);
						$objPHPExcel->getActiveSheet()->setCellValueExplicit('P' . $i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
					} else {
						$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, date('d-m-Y', strtotime($tglapprovespb)));
						$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->i_spb);
						$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, date('d-m-Y', strtotime($row->d_spb)));
						$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->d_spb_receive);
						$objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . $i, $row->i_customer, Cell_DataType::TYPE_STRING);
						$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row->e_customer_name);
						$objPHPExcel->getActiveSheet()->setCellValueExplicit('M' . $i, $row->v_spb, Cell_DataType::TYPE_NUMERIC);
						$objPHPExcel->getActiveSheet()->setCellValueExplicit('N' . $i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
					}

					$i++;
				}
				$x = $i - 1;
			}

			$objPHPExcel->getActiveSheet()->getStyle('A1:P' . $i)->applyFromArray($style_row);

			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama = 'rekapsj' . $iperiode . '.xls';
			if (file_exists('excel/00/' . $nama)) {
				@chmod('excel/00/' . $nama, 0777);
				unlink('excel/00/' . $nama);
			}
			$objWriter->save('excel/00/' . $nama);
			@chmod('excel/00/' . $nama, 0777);

			$this->logger->writenew('Export Rekap SJ Periode' . $iperiode);

			$data['sukses']	= true;
			$data['folder']	= "excel/00";
			$data['inomor'] = $nama;

			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
