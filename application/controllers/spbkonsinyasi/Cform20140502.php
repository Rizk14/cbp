<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('spbkonsinyasi');
			$data['iperiode']='';
			$this->load->view('spbkonsinyasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode		= $this->input->post('iperiode');
			if($iperiode=='') $iperiode	= $this->uri->segment(4);
			$this->load->model('spbkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('spbkonsinyasi');
			$data['iperiode']		= $iperiode;
      $data['diskon']     = $this->mmaster->bacadiskon($iperiode);
			$data['isi']		    = $this->mmaster->bacaperiode($iperiode);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Buka Rekap SPB Konsinyasi Periode: '.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
      $data['ispb']='';
			$this->load->view('spbkonsinyasi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('spbkonsinyasi');
			$this->load->view('spbkonsinyasi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
		  	($this->session->userdata('allmenu')=='t'))
		   ){
			$jml  = $this->input->post('jml', TRUE);
			$iperiode= $this->input->post('iperiode', TRUE);
			$this->db->trans_begin();
      $dspb=date('Y-m-d');
      $tmp=explode("-",$dspb);
      $th=$tmp[0];
      $bl=$tmp[1];
      $dt=$tmp[2];
      $thbl=substr($th,2,2).$bl;
      $ispb  = '';
      $ispbx = '';
			$this->load->model('spbkonsinyasi/mmaster');
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$iarea     = $this->input->post('iarea'.$i, TRUE);
					$ecustomername= $this->input->post('ecustomername'.$i, TRUE);
					$icustomer= $this->input->post('icustomer'.$i, TRUE);
          $awalnama=substr($ecustomername,0,5);
          if($awalnama=='GRIYA' or $awalnama=='SUSAN' or $awalnama=='ASIA '){
            $namagrup='YOG';
          }elseif($awalnama=='TIARA'){
            $namagrup='CLA';
          }elseif($awalnama=='PT. K'){
            $namagrup='PT.';
          }else{
            $namagrup=substr($ecustomername,0,3);
          }
          $enama_group =$ecustomername;
          $qdis=$this->db->query("select distinct(n_notapb_discount) as disc from tm_notapb
                                  where to_char(d_notapb, 'yyyymm')='$iperiode' and i_customer='$icustomer' 
                                  and not i_cek is null and not i_cek='' and f_spb_rekap='f' and f_notapb_cancel='f'
                                  order by n_notapb_discount");
          $nomorspb='';
          if($qdis->num_rows()>0){
            foreach($qdis->result() as $rew){
              $disc=$rew->disc;
#              $xx=explode('.',$disc);
#              if(count($xx)>0){
#                $disc=$xx[0];
#              }
              $iks=$this->mmaster->runningnumberiks($thbl,$namagrup,$iarea,$enama_group);
              $ispb=$this->mmaster->runningnumberspb($thbl,$namagrup,$iarea,$enama_group);
              $this->mmaster->insertheader($iks,$ispb, $dspb, $iarea, $icustomer);
              $this->mmaster->updatebon($icustomer,$iperiode,$ispb);
              if($nomorspb==''){
                $nomorspb=$ispb;
              }else{
                $nomorspb=$nomorspb.' - '.$ispb;
              }
             	$konek 	= "host=192.168.0.100 user=dedy dbname=dialogue port=5432 password=dedyalamsyah";
            	$db    	= pg_connect($konek);
            	$isi    = "  select a.i_area, a.i_product, a.e_product_name, c.v_product_retail as v_unit_price, a.i_product_grade,
                           a.i_product_motif, b.f_spb_rekap, sum(a.n_quantity) as quantity
                           from tm_notapb_item a, tm_notapb b, tr_product_price c
                           where a.i_notapb=b.i_notapb and a.i_customer=b.i_customer and a.i_area=b.i_area
                           and not b.i_cek isnull and not b.i_cek='' and a.i_price_groupco=c.i_price_group and a.i_product=c.i_product
                           and a.i_product_grade=c.i_product_grade
                           and a.i_customer='$icustomer' and b.n_notapb_discount=$disc
                           and to_char(a.d_notapb,'yyyymm')='$iperiode' and b.f_spb_rekap='f'
                           group by a.i_area, a.i_product,a.e_product_name,  c.v_product_retail, a.i_product_grade,
                           a.i_product_motif, b.f_spb_rekap
                           order by a.i_product";
            	$tes=pg_query($isi);
              if(pg_num_rows($tes)>0){
                $x=0;
                $j=0;
                while($row= pg_fetch_assoc($tes)){
                  $j++;
                  $iproduct=$row['i_product'];
                  $eproductname=$row['e_product_name'];
                  $iproductmotif=$row['i_product_motif'];
                  $iproductgrade=$row['i_product_grade'];
                  $harga=$row['v_unit_price'];
                  $quantity=$row['quantity'];
                  if( ($j%21==0 && $j>20) ) {
                    $x=0;
                    $iks=$this->mmaster->runningnumberiks($thbl,$namagrup,$iarea,$enama_group);
                    $ispb=$this->mmaster->runningnumberspb($thbl,$namagrup,$iarea,$enama_group);
                    $this->mmaster->insertheader($iks, $ispb, $dspb, $iarea, $icustomer);
                    $this->mmaster->updatebon($icustomer,$iperiode,$ispb);
                    if($nomorspb==''){
                      $nomorspb=$ispb;
                    }else{
                      $nomorspb=$nomorspb.' - '.$ispb;
                    }
                  }
                  $x++;
                  $this->mmaster->insertdetail($iks,$ispb,$iarea,$iproduct,$eproductname,$iproductgrade,$iproductmotif,$harga,$quantity,$x);
					        $this->mmaster->updatespb($iks,$ispb,$iarea);
                  $ispbx=$ispb;
                }
              }else{
/*                echo "select a.i_area, a.i_product, a.e_product_name, c.v_product_retail as v_unit_price, a.i_product_grade,
                           a.i_product_motif, b.f_spb_rekap, sum(a.n_quantity) as quantity
                           from tm_notapb_item a, tm_notapb b, tr_product_price c
                           where a.i_notapb=b.i_notapb and a.i_customer=b.i_customer and a.i_area=b.i_area
                           and not b.i_cek isnull and not b.i_cek='' and a.i_price_groupco=c.i_price_group and a.i_product=c.i_product
                           and a.i_product_grade=c.i_product_grade
                           and a.i_customer='$icustomer' and b.n_notapb_discount=$disc
                           and to_char(a.d_notapb,'yyyymm')='$iperiode' and b.f_spb_rekap='f'
                           group by a.i_area, a.i_product,a.e_product_name,  c.v_product_retail, a.i_product_grade,
                           a.i_product_motif, b.f_spb_rekap
                           order by a.i_product";*/
                             
                echo 'Ada error';
                die();
              }
            }
          }
				}
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
        $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		=  $this->db->query($sql);
		    if($rs->num_rows>0){
			    foreach($rs->result() as $tes){
				    $ip_address	  = $tes->ip_address;
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }

		    $data['user']	= $this->session->userdata('user_id');
		    $data['host']	= $ip_address;
		    $query 	= pg_query("SELECT current_timestamp as c");
		    while($row=pg_fetch_assoc($query)){
			    $now	  = $row['c'];
		    }
		    $pesan='Input SPB Konsinyasi (rekap-bon) No:'.$nomorspb;
		    $this->load->model('logger');
		    $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $nomorspb;
				$this->load->view('nomor',$data);
			  $this->db->trans_commit();
#				$this->db->trans_rollback();
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function edit()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu442')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('spbkonsinyasi')." update";
         if(
            ($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
           )
         {
            $ispb        = $this->uri->segment(4);
            $iarea       = $this->uri->segment(5);
            $dfrom       = $this->uri->segment(6);
            $dto         = $this->uri->segment(7);
            $ipricegroup = $this->uri->segment(8);
            $query = $this->db->query("select * from tm_spbkonsinyasi_item where i_spb = '$ispb' and i_area='$iarea'");
            $data['jmlitem']= $query->num_rows();
            $data['ispb']  = $ispb;
            $data['departement']=$this->session->userdata('departement');
            $this->load->model('spbkonsinyasi/mmaster');
            $data['isi']   = $this->mmaster->baca($ispb,$iarea);
            $data['detail']   = $this->mmaster->bacadetail($ispb,$iarea,$ipricegroup);

            $qnilaispb  = $this->mmaster->bacadetailnilaispb($ispb,$iarea,$ipricegroup);
            if($qnilaispb->num_rows()>0){
               $row_nilaispb  = $qnilaispb->row();
               $data['nilaispb'] = $row_nilaispb->nilaispb;
            }else{
               $data['nilaispb'] = 0;
            }
            $qnilaiorderspb   = $this->mmaster->bacadetailnilaiorderspb($ispb,$iarea,$ipricegroup);
            if($qnilaiorderspb->num_rows()>0){
               $row_nilaiorderspb   = $qnilaiorderspb->row();
               $data['nilaiorderspb']  = $row_nilaiorderspb->nilaiorderspb;
            }else{
               $data['nilaiorderspb']  = 0;
            }
            $qeket   = $this->db->query(" SELECT e_remark1 as keterangan from tm_spbkonsinyasi where i_spb ='$ispb' and i_area='$iarea' ");
            if($qeket->num_rows()>0){
               $row_eket   = $qeket->row();
               $data['keterangan']  = $row_eket->keterangan;
            }

            $data['iperiode'] = 'edit';
            $data['dfrom'] = $dfrom;
            $data['dto'] = $dto;
#            $data['iarea_awal'] = $iarea_awal;

            $this->load->view('spbkonsinyasi/vmainform',$data);
         }else{
            $this->load->view('spbkonsinyasi/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');

      }
   }
   function update()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu442')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb    = $this->input->post('ispb', TRUE);
         $iarea              = $this->input->post('iarea', TRUE);
         $nspbdiscount1 = $this->input->post('ncustomerdiscount1',TRUE);
         $vspbdiscount1 = $this->input->post('vcustomerdiscount1',TRUE);
         $vspbdiscounttotal   = $this->input->post('vspbdiscounttotal',TRUE);
         $vspb             = $this->input->post('vspb',TRUE);
         $nspbdiscount1 = str_replace(',','',$nspbdiscount1);
         $vspbdiscount1 = str_replace(',','',$vspbdiscount1);
         $vspbdiscounttotal   = str_replace(',','',$vspbdiscounttotal);
         $vspb      = str_replace(',','',$vspb);
         if(($iarea!='') && ($ispb!=''))
         {
            $benar="false";
            $this->db->trans_begin();
            $this->load->model('spbkonsinyasi/mmaster');
            $this->mmaster->updateheader($ispb, $iarea, $nspbdiscount1, $vspbdiscount1, $vspbdiscounttotal, $vspb);
            if ( ($this->db->trans_status() === FALSE) )
            {
                $this->db->trans_rollback();
            }else{
               $sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
                  $now    = $row['c'];
               }
               $pesan='Update SPB Konsinyasi Area '.$iarea.' No:'.$ispb;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );
               $this->db->trans_commit();
               $data['sukses']         = true;
               $data['inomor']         = $ispb;
               $this->load->view('nomor',$data);
            }
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
