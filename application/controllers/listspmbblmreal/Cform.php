<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu342')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspmbblmreal');
			$data['iarea']='';
			$this->load->view('listspmbblmreal/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu342')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$iarea		= $this->input->post('iarea');
			if($iarea=='') $iarea	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listspmbblmreal/cform/view/'.$iarea.'/index/';
			$query = $this->db->query(" select distinct(a.i_spmb), a.d_spmb, a.i_area, a.f_spmb_consigment
                                  from tm_spmb a, tm_spmb_item b
                                  where a.i_spmb=b.i_spmb and a.i_area=b.i_area
                                  and not a.i_approve2 is null
                                  and not a.i_store is null
                                  and b.n_deliver<b.n_saldo
                                  and (upper(a.i_spmb) like '%$cari%')
                                  and a.i_area='$iarea'
                                  order by a.i_spmb",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listspmbblmreal');
			$this->load->model('listspmbblmreal/mmaster');
			$data['cari']		= $cari;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$config['per_page'],$this->uri->segment(6),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SPMB Tidak di Realisasi '.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listspmbblmreal/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}	
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu342')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspmbblmreal');
			$this->load->view('listspmbblmreal/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu342')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspmbblmreal');
			if($this->uri->segment(4)!=''){
				$ispmb = $this->uri->segment(4);
				$iarea = $this->uri->segment(5);

				$query = $this->db->query("select a.i_spmb, a.d_spmb, a.i_area, b.i_product, b.e_product_name, b.n_order,
                                  b.n_deliver, b.n_saldo
                                  from tm_spmb a, tm_spmb_item b
                                  where a.i_spmb=b.i_spmb and a.i_area=b.i_area
                                  and not a.i_approve2 is null and not a.i_store is null
                                  and b.n_deliver<b.n_saldo and a.i_area='$iarea'
                                  order by a.i_spmb");
			
				$data['jmlitem'] = $query->num_rows(); 
				$data['ispmb'] = $ispmb;
				$data['iarea'] = $iarea;			
				$this->load->model('listspmbblmreal/mmaster');
				$data['isi']=$this->mmaster->baca($ispmb);
				$data['detail']=$this->mmaster->bacadetail($ispmb);
		 		$this->load->view('listspmbblmreal/vformdetail',$data);
			}else{
				$this->load->view('listspmbblmreal/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu342')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$iarea		= $this->input->post('iarea');
			if($iarea=='') $iarea	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listspmbblmreal/cform/view/'.$iarea.'/index/';
	    $query = $this->db->query(" select distinct(a.i_spmb), a.d_spmb, a.i_area, a.f_spmb_consigment
                                  from tm_spmb a, tm_spmb_item b
                                  where a.i_spmb=b.i_spmb and a.i_area=b.i_area
                                  and not a.i_approve2 is null
                                  and not a.i_store is null
                                  and b.n_deliver<b.n_saldo
                                  and (upper(a.i_spmb) like '%$cari%')
                                  and a.i_area='$iarea'
                                  order by a.i_spmb ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listspmbblmreal');
			$this->load->model('listspmbblmreal/mmaster');
			$data['cari']		= $cari;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->cariperiode($iarea,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('listspmbblmreal/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu342')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listspmbblmreal/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listspmbblmreal/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listspmbblmreal/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu342')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listspmbblmreal/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listspmbblmreal/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listspmbblmreal/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
