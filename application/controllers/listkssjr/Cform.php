<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listkssjr');
			$data['iperiode']	= '';
			$this->load->view('listkssjr/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listkssjr/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select distinct tm_ic_convertion.i_refference,tm_ic_convertion.d_ic_convertion,tm_ic_convertion.f_ic_convertioncancel, tm_sjr.i_area, tr_area.e_area_name
										from tm_ic_convertion, tm_sjr, tr_area
										where tm_ic_convertion.i_refference= tm_sjr.i_sjr 
										and tm_sjr.i_area=tr_area.i_area
										and to_char(d_ic_convertion,'yyyymm') = '$iperiode'
                                  		",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('listkssjr/mmaster');
			$data['page_title'] = $this->lang->line('listkssjr');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->baca($iperiode,$cari,$config['per_page'],$this->uri->segment(6));

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Konversi Stock BBM Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listkssjr/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listkssjr/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select distinct tm_ic_convertion.i_refference,tm_ic_convertion.d_ic_convertion,tm_ic_convertion.f_ic_convertioncancel, tm_sjr.i_area, tr_area.e_area_name
										from tm_ic_convertion, tm_sjr, tr_area
										where tm_ic_convertion.i_refference= tm_sjr.i_sjr and tm_ic_convertion.i_refference like '%$cari%'
										and tm_sjr.i_area=tr_area.i_area 
										and to_char(d_ic_convertion,'yyyymm') = '$iperiode'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('listkssjr/mmaster');
			$data['page_title'] = $this->lang->line('listkssjr');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode,$cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('listkssjr/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	/*function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listkssjr/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tm_ic_convertion
										where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'
										or upper(i_ic_convertion) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listkssjr');
			$this->load->model('listkssjr/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$data['cari'] = $cari;

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Konversi Stock';
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listkssjr/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}*/
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listkssjr');
			$this->load->view('listkssjr/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listkssjr')." update";
			if($this->uri->segment(4)!=''){
				$irefference = $this->uri->segment(4);
				$query = $this->db->query("select distinct a.i_refference, a.d_ic_convertion, a.f_ic_convertioncancel, b.i_area, c.e_area_name from tm_ic_convertion a, tm_sjr b, tr_area c 
                      where a.i_refference = '$irefference' and a.i_refference = b.i_sjr and b.i_area=c.i_area");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['irefference'] = $irefference;
				$this->load->model('listkssjr/mmaster');
				$data['header']=$this->mmaster->bacaheaderdetail($irefference);

				$irefference = $this->uri->segment(4);
				$query = $this->db->query("select * from tm_ic_convertion where i_refference = '$irefference'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['irefference'] = $irefference;
				$this->load->model('listkssjr/mmaster');
				$data['detail']=$this->mmaster->bacadetail($irefference);
		 		$this->load->view('listkssjr/vformupdate',$data);
			}else{
				$this->load->view('listkssjr/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iicconvertion	= $this->uri->segment(4);
			$this->load->model('listkssjr/mmaster');
			$istore				= 'AA';
			$istorelocation		= '01';
			$istorelocationbin	= '00';
      $this->db->trans_begin();
			$this->mmaster->delete($iicconvertion,$istore,$istorelocation,$istorelocationbin);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus Konversi Stock No:'.$iicconvertion.' Gudang:'.$istore;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
      if ($this->db->trans_status()===false)
			{
				$this->db->trans_rollback();
			}else{
#        $this->db->trans_rollback();
		    $this->db->trans_commit();
      }    
			$data['page_title'] = $this->lang->line('listkssjr');
			$config['base_url'] = base_url().'index.php/listkssjr/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tm_ic_convertion
										where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'
										or upper(i_ic_convertion) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listkssjr');
			$this->load->model('listkssjr/mmaster');
			$data['cari'] = $cari;
      $iperiode=$this->uri->segment(5);
      $data['iperiode']=$iperiode;
#			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
      $data['isi']	= $this->mmaster->baca($iperiode,$cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('listkssjr/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	/*function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listkssjr/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tm_ic_convertion
										where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'
										or upper(i_ic_convertion) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listkssjr');
			$this->load->model('listkssjr/mmaster');
			$data['cari'] = $cari;
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('listkssjr/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}*/
}
?>
