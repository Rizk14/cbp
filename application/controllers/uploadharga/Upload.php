<?php 

class Upload extends CI_Controller {
	
	function Upload()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	
	function index()
	{	
		if ($this->session->userdata('logged_in')){
			$this->load->view('uploadharga/upload_form', array('error' => ' ' ));
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function upload_harga()
	{
		if ($this->session->userdata('logged_in')){
			$config['upload_path'] = './beli/';
			$config['allowed_types'] = '*';
			$config['overwrite']	= 'TRUE';
			$config['max_size']	= '0';
			$this->load->library('upload', $config);
      $bener=false;
      $field='userfile';
		  if ( ! $this->upload->do_upload($field))
		  {
			  $error = array('error' => $this->upload->display_errors());
        $bener=false;
			  $this->load->view('uploadharga/upload_form', $error);
		  }	
		  else
		  {
        $bener=true;
		  }
      if($bener){
			  $data = array('upload_data' => $this->upload->data());
			  $this->load->view('uploadharga/upload_success', $data);
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}	
}
?>
