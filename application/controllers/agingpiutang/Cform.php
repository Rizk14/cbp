<?php
class Cform extends CI_Controller
{
	public $title	= "Keterlambatan Percustomer";
	public $folder 	= "agingpiutang";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('fungsi');
		$this->load->model('agingpiutang/mmaster');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu258') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->title;
			$data['iarea'] 		= '';
			$data['djt'] 		= '';

			$this->load->view('agingpiutang/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu258') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari	= strtoupper($this->input->post('cari'));
			$iarea	= $this->input->post('iarea');
			$djt	= $this->input->post('djt');

			if ($iarea == '') $iarea	= $this->uri->segment(4);
			if ($djt == '') $djt	= $this->uri->segment(5);

			if ($cari == '' && ($this->uri->segment(6) == 'index' or $this->uri->segment(6) == '')) {
				$config['base_url'] = base_url() . 'index.php/agingpiutang/cform/view/' . $iarea . '/' . $djt . '/index/';
			} else {
				if ($cari == '') $cari	= $this->uri->segment(6);
				$config['base_url'] = base_url() . 'index.php/agingpiutang/cform/view/' . $iarea . '/' . $djt . '/' . $cari . '/';
			}

			if ($iarea == 'NA') {
				$query = $this->db->query(" select sum(a.v_sisa) from tm_nota a, tr_customer b, tr_customer_groupar c
											where a.i_customer=b.i_customer and a.i_customer=c.i_customer 
											and a.f_ttb_tolak='f'
											and not a.i_nota isnull
											and (upper(a.i_nota) like '%$cari%' 
											or upper(a.i_spb) like '%$cari%' 
											or upper(a.i_customer) like '%$cari%'
											or upper(c.i_customer_groupar) like '%$cari%'  
											or upper(b.e_customer_name) like '%$cari%')
											and a.v_sisa>0
											and a.f_nota_cancel='f' group by a.i_customer", false);
			} else {
				$query = $this->db->query(" select sum(a.v_sisa) from tm_nota a, tr_customer b, tr_customer_groupar c
											where a.i_customer=b.i_customer and a.i_customer=c.i_customer 
											and a.f_ttb_tolak='f'
											and not a.i_nota isnull
											and (upper(a.i_nota) like '%$cari%' 
											or upper(a.i_spb) like '%$cari%' 
											or upper(a.i_customer) like '%$cari%'
											or upper(c.i_customer_groupar) like '%$cari%'  
											or upper(b.e_customer_name) like '%$cari%')
											and a.i_area='$iarea' 
											and a.v_sisa>0
											and a.f_nota_cancel='f' group by a.i_customer", false);
			}

			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);

			$data['page_title'] 	= $this->title;
			$data['cari']			= $cari;
			$data['iarea']			= $iarea;
			$data['djt']			= $djt;
			$data['isi']			= $this->mmaster->bacaperiode($iarea, $djt, $config['per_page'], $this->uri->segment(7), $cari);

			$this->logger->writenew('Membuka Aging Keteralambatan Percustomer Jatuh Tempo : ' . $djt . ' Area : ' . $iarea);

			$this->load->view('agingpiutang/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu258') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->title;
			$this->load->view('agingpiutang/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu258') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/agingpiutang/cform/area/index/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows();
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('agingpiutang/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu258') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/agingpiutang/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('agingpiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('agingpiutang/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu258') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$icustomergroupar	= $this->uri->segment(4);
			$djt				= $this->uri->segment(5);
			$ecustomername		= $this->uri->segment(6);

			$config['base_url'] = base_url() . 'index.php/agingpiutang/cform/detail/' . $icustomergroupar . '/' . $djt . '/' . $ecustomername . '/';

			$ecustomername = str_replace('%20', ' ', $ecustomername);
			$ecustomername = str_replace('tandakurungbuka', '(', $ecustomername);
			$ecustomername = str_replace('tandakurungtutup', ')', $ecustomername);
			$ecustomername = str_replace('tandadan', '&', $ecustomername);
			$ecustomername = str_replace('tandatitik', '.', $ecustomername);
			$ecustomername = str_replace('tandakoma', ',', $ecustomername);
			$ecustomername = str_replace('tandatikom', ';', $ecustomername);
			$ecustomername = str_replace('tandagaring', '/', $ecustomername);

			$query = $this->db->query(" SELECT a.v_sisa 
										from tm_nota a, tr_customer b, tr_customer_groupar c
										where a.i_customer=b.i_customer and a.i_customer=c.i_customer
										and a.f_ttb_tolak='f'
										and not a.i_nota isnull
										and c.i_customer_groupar = '$icustomergroupar' 
										and a.v_sisa>0
										and a.f_nota_cancel='f'", false); #and a.f_nota_koreksi='f'

			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->paginationxx->initialize($config);

			$data['page_title'] 		= $this->title;
			$data['icustomergroupar']	= $icustomergroupar;
			$data['ecustomername']		= $ecustomername;
			$data['djt']				= $djt;
			$data['isi']				= $this->mmaster->bacadetail($icustomergroupar, $djt, $config['per_page'], $this->uri->segment(7));

			$this->load->view('agingpiutang/vformdetail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function exportheader()
	{
		set_time_limit(3600000);
		ini_set("memory_limit", "512M");
		ini_set("max_execution_time", "3600000");

		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$djtx   = $this->uri->segment(4);
		$iarea 	= $this->uri->segment(5);

		$isi 	= $this->mmaster->bacaperiodeexport($iarea, $djtx);

		$phpexcel = new PHPExcel();
		$phpexcel->setActiveSheetIndex(0);

		$phpexcel->getDefaultStyle()->getFont()->setName('Arial');
		$phpexcel->getActiveSheet()->mergeCells('A2:K2');
		$phpexcel->getActiveSheet()->setCellValue('A2', ' Data Aging Keterlambatan Percustomer');
		$phpexcel->getActiveSheet()->mergeCells('A3:K3');
		$phpexcel->getActiveSheet()->setCellValue('A3', "Area : " . $iarea);
		$phpexcel->getActiveSheet()->mergeCells('A4:K4');
		$phpexcel->getActiveSheet()->setCellValue('A4', "Jatuh Tempo : " . $djtx);

		$header = array(
			'font' => array(
				'name' => 'Times New Roman',
				'bold' => true,
				'italic' => false,
				'size' => 12
			),
			'alignment' => array(
				'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => Style_Alignment::VERTICAL_CENTER,
				'wrap' => true
			)
		);



		$phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
		$phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(7);
		$phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);

		$style_col = array(
			'font' => array(
				'name'	=> 'Times New Roman',
				'bold'  => true,
				'italic' => false,
				'size'  => 10
			),
			'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom' => array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
			),
			'alignment' => array(
				'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER
			)
		);

		$style_row = array(
			'font' => array(
				'name'	=> 'Times New Roman',
				'bold'  => false,
				'italic' => false,
				'size'  => 9
			),
			'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom' => array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
			),
			'alignment' => array(
				// 'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER
			)
		);

		$style_row1 = array(
			'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom' => array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)

			),
			'font' => array(
				'name'	=> 'Times New Roman',
				'bold'  => false,
				'italic' => false,
				'size'  => 10
			),
		);

		$phpexcel->getActiveSheet()->setCellValue('A5', 'Kode Pelanggan');
		$phpexcel->getActiveSheet()->setCellValue('B5', 'Nama Pelanggan');
		$phpexcel->getActiveSheet()->setCellValue('C5', 'TOP');
		$phpexcel->getActiveSheet()->setCellValue('D5', 'Saldo Piutang');
		$phpexcel->getActiveSheet()->setCellValue('E5', 'BLM JT');
		$phpexcel->getActiveSheet()->setCellValue('F5', '1-7');
		$phpexcel->getActiveSheet()->setCellValue('G5', '8-30');
		$phpexcel->getActiveSheet()->setCellValue('H5', '31-60');
		$phpexcel->getActiveSheet()->setCellValue('I5', '61-90');
		$phpexcel->getActiveSheet()->setCellValue('J5', '>90');
		$phpexcel->getActiveSheet()->setCellValue('K5', 'Jenis Pelanggan');

		$n = 6;
		$no = 1;

		if ($djtx) {
			$tmp 	= explode("-", $djtx);
			$det	= $tmp[0];
			$mon	= $tmp[1];
			$yir 	= $tmp[2];
			$djtx	= $yir . "/" . $mon . "/" . $det;
		} else {
			$djtx = '';
		}

		$vtsisa 		= 0;
		$vtsisaaman 	= 0;
		$vtsisatujuh 	= 0;
		$vtsisalapan 	= 0;
		$vtsisatiga 	= 0;
		$vtsisaenam 	= 0;
		$vtsisalalan 	= 0;

		foreach ($isi as $row) {
			$query = $this->db->query(" select a.n_customer_toplength, b.e_customer_classname 
										from tr_customer a, tr_customer_class b
										where a.i_customer='$row->i_customer' and a.i_customer_class=b.i_customer_class");
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $tmp) {
					$top = $tmp->n_customer_toplength;
					$clas = $tmp->e_customer_classname;
				}
			}
			if ($top < 0) $top = $top * -1;
			$toptujuh 		= -7;
			$toplapan 		= -30;
			$toptigasatu 	= -60;
			$topenamsatu 	= -90;
			$dudet			= $this->fungsi->dateAdd("d", 0, $djtx);
			$djatuhtempo 	= $dudet;
			$sisa 			= ($row->v_sisa);

			$vsisaaman 		= 0;
			$query = $this->db->query(" SELECT sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
										and d_jatuh_tempo >= '$djatuhtempo' and v_sisa>0 and f_nota_cancel='f'
										and f_ttb_tolak='f' and not i_nota isnull group by i_customer");
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $tmp) {
					$vsisaaman = $tmp->v_sisa;
				}
			}
			$vsisaaman = ($vsisaaman);

			$dudettujuh	= $this->fungsi->dateAdd("d", $toptujuh, $djtx);
			$vsisatujuh = 0;
			$query = $this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
										and d_jatuh_tempo < '$djatuhtempo' and d_jatuh_tempo >= '$dudettujuh' 
										and v_sisa>0 and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
										group by i_customer");
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $tmp) {
					$vsisatujuh = $tmp->v_sisa;
				}
			}
			$vsisatujuh = ($vsisatujuh);

			$dudetlapan	= $this->fungsi->dateAdd("d", $toplapan, $djtx);
			$vsisalapan = 0;
			$query = $this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
										and d_jatuh_tempo < '$dudettujuh' and d_jatuh_tempo >= '$dudetlapan' and v_sisa>0 
										and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
										group by i_customer");
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $tmp) {
					$vsisalapan = $tmp->v_sisa;
				}
			}
			$vsisalapan = ($vsisalapan);

			$dudettigasatu	= $this->fungsi->dateAdd("d", $toptigasatu, $djtx);
			$vsisatiga = 0;
			$query = $this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
										and d_jatuh_tempo < '$dudetlapan' and d_jatuh_tempo >= '$dudettigasatu' and v_sisa>0 
										and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
										group by i_customer");
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $tmp) {
					$vsisatiga = $tmp->v_sisa;
				}
			}
			$vsisatiga = ($vsisatiga);

			$dudetenamsatu	= $this->fungsi->dateAdd("d", $topenamsatu, $djtx);
			$vsisaenam = 0;
			$query = $this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
										and d_jatuh_tempo < '$dudettigasatu' and d_jatuh_tempo >= '$dudetenamsatu' and v_sisa>0 
										and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
										group by i_customer");
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $tmp) {
					$vsisaenam = $tmp->v_sisa;
				}
			}
			$vsisaenam = ($vsisaenam);

			$dudetlalan	= $this->fungsi->dateAdd("d", $topenamsatu, $djtx);
			$vsisalalan = 0;
			$query = $this->db->query(" select sum(v_sisa) as v_sisa from tm_nota where i_customer='$row->i_customer'
										and d_jatuh_tempo < '$dudetenamsatu' and v_sisa>0 
										and f_nota_cancel='f' and f_ttb_tolak='f' and not i_nota isnull
										group by i_customer");
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $tmp) {
					$vsisalalan = $tmp->v_sisa;
				}
			}
			$vsisalalan = ($vsisalalan);

			$query = $this->db->query(" select i_customer_groupar
										from tr_customer_groupar
										where i_customer='$row->i_customer'");
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $tmp) {
					$groupar = $tmp->i_customer_groupar;
				}
			}
			$nama = $row->e_customer_name;
			$nama = str_replace("'", "", $nama);
			$nama = str_replace("(", "tandakurungbuka", $nama);
			$nama = str_replace("&", "tandadan", $nama);
			$nama = str_replace(")", "tandakurungtutup", $nama);
			$nama = str_replace('.', 'tandatitik', $nama);
			$nama = str_replace(',', 'tandakoma', $nama);
			$nama = str_replace('/', 'tandagaring', $nama);
			$nama = str_replace(';', 'tandatikom', $nama);

			$phpexcel->getActiveSheet()->setCellValueExplicit("A" . $n, $row->i_customer, Cell_DataType::TYPE_STRING);
			$phpexcel->getActiveSheet()->setCellValue("B" . $n, $row->e_customer_name);
			$phpexcel->getActiveSheet()->setCellValue("C" . $n, $top);
			$phpexcel->getActiveSheet()->setCellValue("D" . $n, $sisa);
			$phpexcel->getActiveSheet()->setCellValue("E" . $n, $vsisaaman);
			$phpexcel->getActiveSheet()->setCellValue("F" . $n, $vsisatujuh);
			$phpexcel->getActiveSheet()->setCellValue("G" . $n, $vsisalapan);
			$phpexcel->getActiveSheet()->setCellValue("H" . $n, $vsisatiga);
			$phpexcel->getActiveSheet()->setCellValue("I" . $n, $vsisaenam);
			$phpexcel->getActiveSheet()->setCellValue("J" . $n, $vsisalalan);
			$phpexcel->getActiveSheet()->setCellValue("K" . $n, $clas);

			$n++;
			$no++;

			$vtsisa			+= $sisa;
			$vtsisaaman		+= $vsisaaman;
			$vtsisatujuh	+= $vsisatujuh;
			$vtsisalapan	+= $vsisalapan;
			$vtsisatiga		+= $vsisatiga;
			$vtsisaenam		+= $vsisaenam;
			$vtsisalalan	+= $vsisalalan;
		}

		$phpexcel->getActiveSheet()->setCellValue('D' . $n, $vtsisa);
		$phpexcel->getActiveSheet()->setCellValue('E' . $n, $vtsisaaman);
		$phpexcel->getActiveSheet()->setCellValue('F' . $n, $vtsisatujuh);
		$phpexcel->getActiveSheet()->setCellValue('G' . $n, $vtsisalapan);
		$phpexcel->getActiveSheet()->setCellValue('H' . $n, $vtsisatiga);
		$phpexcel->getActiveSheet()->setCellValue('I' . $n, $vtsisaenam);
		$phpexcel->getActiveSheet()->setCellValue('J' . $n, $vtsisalalan);

		$phpexcel->getActiveSheet()->getStyle('A1:K4')->applyFromArray($header);
		$phpexcel->getActiveSheet()->getStyle('A5:K5')->applyFromArray($style_col);
		$phpexcel->getActiveSheet()->getStyle('A5:K5')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DFF1D0');
		$phpexcel->getActiveSheet()->getStyle('A6:K' . $n)->applyFromArray($style_row);
		$phpexcel->getActiveSheet()->getStyle('D6:J' . $n)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);

		$nama_file = 'keteralambatanpercust_' . $iarea . '.xls';
		if ($iarea == 'NA') {
			$iarea = '00';
		}

		// Proses file excel    
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=' . $nama_file . ''); // Set nama file excel nya    
		header('Cache-Control: max-age=0');

		$objWriter = IOFactory::createWriter($phpexcel, 'Excel5');
		// $objWriter->save('excel/' . $iarea . '/' . $nama_file);

		$objWriter->save('php://output', 'w');
	}

	function exportdetail()
	{
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$icustomergroupar   = $this->uri->segment(4);
		$djt 				= $this->uri->segment(5);
		$ecustomername		= str_replace('%20', ' ', $this->uri->segment(6));

		$isi 	= $this->mmaster->bacadetailnolimit($icustomergroupar, $djt);

		$phpexcel = new PHPExcel();
		$phpexcel->setActiveSheetIndex(0);

		$phpexcel->getDefaultStyle()->getFont()->setName('Arial');
		$phpexcel->getActiveSheet()->mergeCells('A2:K2');
		$phpexcel->getActiveSheet()->setCellValue('A2', $icustomergroupar . " - " . $ecustomername);
		$phpexcel->getActiveSheet()->mergeCells('A3:K3');
		$phpexcel->getActiveSheet()->setCellValue('A3', "Jatuh Tempo : " . date('d-m-Y', strtotime($djt)));

		$header = array(
			'font' => array(
				'name' => 'Times New Roman',
				'bold' => true,
				'italic' => false,
				'size' => 12
			),
			'alignment' => array(
				'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => Style_Alignment::VERTICAL_CENTER,
				'wrap' => true
			)
		);

		$phpexcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
		$phpexcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
		$phpexcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
		$phpexcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
		$phpexcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
		$phpexcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);

		$style_col = array(
			'font' => array(
				'name'	=> 'Times New Roman',
				'bold'  => true,
				'italic' => false,
				'size'  => 10
			),
			'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom' => array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
			),
			'alignment' => array(
				'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER
			)
		);

		$style_row = array(
			'font' => array(
				'name'	=> 'Times New Roman',
				'bold'  => false,
				'italic' => false,
				'size'  => 9
			),
			'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom' => array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)
			),
			'alignment' => array(
				// 'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
				'vertical'  => Style_Alignment::VERTICAL_CENTER
			)
		);

		$style_row1 = array(
			'borders' => array(
				'top' 	=> array('style' => Style_Border::BORDER_THIN),
				'bottom' => array('style' => Style_Border::BORDER_THIN),
				'left'  => array('style' => Style_Border::BORDER_THIN),
				'right' => array('style' => Style_Border::BORDER_THIN)

			),
			'font' => array(
				'name'	=> 'Times New Roman',
				'bold'  => false,
				'italic' => false,
				'size'  => 10
			),
		);

		$phpexcel->getActiveSheet()->setCellValue('A5', 'No');
		$phpexcel->getActiveSheet()->setCellValue('B5', 'Kode Pelanggan');
		$phpexcel->getActiveSheet()->setCellValue('C5', 'Nota');
		$phpexcel->getActiveSheet()->setCellValue('D5', 'Tgl Nota');
		$phpexcel->getActiveSheet()->setCellValue('E5', 'Tgl JT');
		$phpexcel->getActiveSheet()->setCellValue('F5', 'No SJ');
		$phpexcel->getActiveSheet()->setCellValue('G5', 'Jumlah');
		$phpexcel->getActiveSheet()->setCellValue('H5', 'Sisa');
		$phpexcel->getActiveSheet()->setCellValue('I5', 'Lama');
		$phpexcel->getActiveSheet()->setCellValue('J5', 'TOP');
		$phpexcel->getActiveSheet()->setCellValue('K5', 'Jenis');

		$n = 6;
		$no = 1;

		#####
		$pecah1 = explode("-", $djt);
		$date1 	= $pecah1[2];
		$month1 = $pecah1[1];
		$year1 	= $pecah1[0];
		$jd1 	= GregorianToJD($month1, $date1, $year1);
		#####

		$vtnota = 0;
		$vtsisa = 0;
		foreach ($isi as $row) {
			$tgl2 	= $row->d_jatuh_tempo;
			$pecah2 = explode("-", $tgl2);
			$date2 	= $pecah2[2];
			$month2 = $pecah2[1];
			$year2	= $pecah2[0];
			$jd2 	= GregorianToJD($month2, $date2, $year2);

			$lama = $jd1 - $jd2;

			if ($row->i_product_group == '00') {
				$jenis = 'Home';
			} elseif ($row->i_product_group == '01') {
				$jenis = 'Bd';
			} else {
				$jenis = 'NB';
			}

			$phpexcel->getActiveSheet()->setCellValue("A" . $n, $no);
			$phpexcel->getActiveSheet()->setCellValueExplicit("B" . $n, $row->i_customer, Cell_DataType::TYPE_STRING);
			$phpexcel->getActiveSheet()->setCellValue("C" . $n, $row->i_nota);
			$phpexcel->getActiveSheet()->setCellValue("D" . $n, date('d-m-Y', strtotime($row->d_nota)));
			$phpexcel->getActiveSheet()->setCellValue("E" . $n, date('d-m-Y', strtotime($row->d_jatuh_tempo)));
			$phpexcel->getActiveSheet()->setCellValue("F" . $n, $row->i_sj);
			$phpexcel->getActiveSheet()->setCellValue("G" . $n, $row->v_nota_netto);
			$phpexcel->getActiveSheet()->setCellValue("H" . $n, $row->v_sisa);
			$phpexcel->getActiveSheet()->setCellValue("I" . $n, $lama);
			$phpexcel->getActiveSheet()->setCellValue("J" . $n, $row->n_nota_toplength);
			$phpexcel->getActiveSheet()->setCellValue("K" . $n, $jenis);

			$n++;
			$no++;

			$vtnota += $row->v_nota_netto;
			$vtsisa += $row->v_sisa;
		}

		$phpexcel->getActiveSheet()->setCellValue('G' . $n, $vtnota);
		$phpexcel->getActiveSheet()->setCellValue('H' . $n, $vtsisa);

		$phpexcel->getActiveSheet()->getStyle('A1:K4')->applyFromArray($header);
		$phpexcel->getActiveSheet()->getStyle('A5:K5')->applyFromArray($style_col);
		$phpexcel->getActiveSheet()->getStyle('A5:K5')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DFF1D0');
		$phpexcel->getActiveSheet()->getStyle('A6:K' . $n)->applyFromArray($style_row);
		$phpexcel->getActiveSheet()->getStyle('G6:H' . $n)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);

		$nama_file = 'keteralambatanpercust_detail_' . $icustomergroupar . '.xls';

		// Proses file excel    
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=' . $nama_file . ''); // Set nama file excel nya    
		header('Cache-Control: max-age=0');

		$objWriter = IOFactory::createWriter($phpexcel, 'Excel5');
		// $objWriter->save('excel/' . $iarea . '/' . $nama_file);

		$objWriter->save('php://output', 'w');
	}
}
