<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu142')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-pelunasanap/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tm_pelunasanap
										where f_posting = 'f'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_pelunasanap');
			$this->load->model('akt-pelunasanap/mmaster');
			$data['ipelunasan']='';
			$data['dbukti']='';
			$data['iarea']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('akt-pelunasanap/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu142')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('list_pelunasanap');
			$this->load->view('akt-pelunasanap/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu142')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/akt-pelunasanap/cform/index/';
			$config['per_page'] = '10';
			$limo=$config['per_page'];
			$ofso=$this->uri->segment(4);
			if($ofso=='')
				$ofso=0;
			$query = $this->db->query(" select * from tm_pelunasan
										where f_posting = 'f'
										and (upper(i_pelunasan) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('akt-pelunasanap/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('list_rv');
			$data['ipelunasan']='';
			$data['iarea']='';
			$data['idt']='';
	 		$this->load->view('akt-pelunasanap/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu142')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('pelunasan-ap');
			if(
				($this->uri->segment(4)) && ($this->uri->segment(5))			
			  )
			{
				$ipl	= $this->uri->segment(4);
				$iarea	= $this->uri->segment(5);
				$dbukti	= $this->uri->segment(6);
				$query 	= $this->db->query("select * from tm_pelunasanap_item 
											where i_pelunasanap = '$ipl' and i_area = '$iarea' and d_bukti='$dbukti' ");
				$data['jmlitem'] 		= $query->num_rows(); 				
				$data['ipelunasan']		= $ipl;
				$data['iarea']			= $iarea;
				$data['dbukti']			= $dbukti;
				$this->load->model('akt-pelunasanap/mmaster');
				$data['vsisa']=$this->mmaster->sisa($ipl,$iarea,$dbukti);
				$data['isi']=$this->mmaster->bacapl($ipl,$iarea,$dbukti);
				$data['detail']=$this->mmaster->bacadetailpl($ipl,$iarea,$dbukti);
				$data['jumlah']=$this->uri->segment(7);;
		 		$this->load->view('akt-pelunasanap/vmainform',$data);
			}else{
				$this->load->view('akt-pelunasanap/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function posting()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu142')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('akt-pelunasanap/mmaster');
			$ipelunasan		= $this->input->post('ipelunasan', TRUE);
			$iarea 			= $this->input->post('iarea', TRUE);
			$suppname 		= $this->input->post('esuppliername', TRUE);
			$dbukti			= $this->input->post('dpelunasan', TRUE);
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
				$iperiode=$th.$bl;
			}
			$egirodescription="Pelunasan hutang kepada:".$suppname;
			$fclose			= 'f';
			$jml			= $this->input->post('jml', TRUE);
			$this->db->trans_begin();
			$this->mmaster->inserttransheader($ipelunasan,$iarea,$egirodescription,$fclose,$dbukti);
			$this->mmaster->updatepelunasan($ipelunasan,$iarea,$dbukti);
			for($i=1;$i<=$jml;$i++)
			{
				$vjumlah		= $this->input->post('vjumlah'.$i, TRUE);
				$vjumlah		= str_replace(',','',$vjumlah);
//				$accdebet		= '111.3'.$iarea;
				$accdebet		= '211.100';
				$namadebet		= $this->mmaster->namaacc($accdebet);
				$tmp			= $this->mmaster->carisaldo($accdebet,$iperiode);
				if($tmp) 
					$vsaldoaw1		= $tmp->v_saldo_awal;
				else 
					$vsaldoaw1		= 0;
				if($tmp) 
					$vmutasidebet1	= $tmp->v_mutasi_debet;
				else
					$vmutasidebet1	= 0;
				if($tmp) 
					$vmutasikredit1	= $tmp->v_mutasi_kredit;
				else
					$vmutasikredit1	= 0;
				if($tmp) 
					$vsaldoak1		= $tmp->v_saldo_akhir;
				else
					$vsaldoak1		= 0;
				
//				$acckredit		= '112.2'.$iarea;
				$acckredit		= '111.100';
				$namakredit		= $this->mmaster->namaacc($acckredit);
				$saldoawkredit	= $this->mmaster->carisaldo($acckredit,$iperiode);
				if($tmp) 
					$vsaldoaw2		= $tmp->v_saldo_awal;
				else
					$vsaldoaw2		= 0;
				if($tmp) 
					$vmutasidebet2	= $tmp->v_mutasi_debet;
				else
					$vmutasidebet2	= 0;
				if($tmp) 
					$vmutasikredit2	= $tmp->v_mutasi_kredit;
				else
					$vmutasikredit2	= 0;
				if($tmp) 
					$vsaldoak2		= $tmp->v_saldo_akhir;
				else
					$vsaldoak2		= 0;
				$this->mmaster->inserttransitemdebet($accdebet,$ipelunasan,$namadebet,'t','t',$iarea,$egirodescription,$vjumlah,$dbukti);
				$this->mmaster->updatesaldodebet($accdebet,$iperiode,$vjumlah);
				$this->mmaster->inserttransitemkredit($acckredit,$ipelunasan,$namakredit,'f','t',$iarea,$egirodescription,$vjumlah,$dbukti);
				$this->mmaster->updatesaldokredit($acckredit,$iperiode,$vjumlah);
				$this->mmaster->insertgldebet($accdebet,$ipelunasan,$namadebet,'t',$iarea,$vjumlah,$dbukti,$egirodescription);
				$this->mmaster->insertglkredit($acckredit,$ipelunasan,$namakredit,'f',$iarea,$vjumlah,$dbukti,$egirodescription);
			}
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='Posting Pelunasan AP No:'.$ipelunasan.' Area:'.$iarea;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses'] = true;
				$data['inomor']	= $ipelunasan;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
