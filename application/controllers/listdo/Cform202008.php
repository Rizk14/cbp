<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu59')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listdo/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('listdo');
			$data['cari'] = '';
      $data['dfrom']= '';
			$data['dto']  = '';
			$data['isi']  = '';
			$this->load->view('listdo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu59')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdo');
			$this->load->view('listdo/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu59')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$op			= $this->uri->segment(4);
			$ido		= str_replace('%20','',$this->uri->segment(5));
			$isupplier	= $this->uri->segment(6);
			$dfrom	= $this->uri->segment(7);
			$dto		= $this->uri->segment(8);
			$iarea	= $this->uri->segment(9);
			$this->load->model('listdo/mmaster');
      $this->db->trans_begin();
			$this->mmaster->delete($ido,$isupplier,$op);
      if (($this->db->trans_status() === FALSE))
			{
				$this->db->trans_rollback();
			}else{
#				$this->db->trans_rollback();
				$this->db->trans_commit();
			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Delete DO No:'.$ido.' Area:'.$iarea;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['page_title'] = $this->lang->line('listdo');
			  $config['base_url'] = base_url().'index.php/listdo/cform/index/';
        $config['base_url'] = base_url().'index.php/listdo/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			  $cari = strtoupper($this->input->post('cari', FALSE));
			  $sql="select a.i_do
              from tm_do a, tr_supplier e,
              tm_op b left join tm_spb c on(b.i_reff=c.i_spb and b.i_area=c.i_area) 
              left join tm_spmb d on(b.i_reff=d.i_spmb and b.i_area=d.i_area)
              where a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy')
              and a.i_op=b.i_op and a.i_area=b.i_area and a.i_supplier=e.i_supplier and a.i_area='$iarea'";
			  $query 	= $this->db->query($sql,false);
			  $config['total_rows'] = $query->num_rows();
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(8);
			  $this->pagination->initialize($config);
			  $data['page_title'] = $this->lang->line('listdo');
			  $data['cari']	= $cari;
			  $data['dfrom']	= $dfrom;
			  $data['dto']	= $dto;
			  $data['iarea'] = $iarea;
			  $data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
	      	$now	  = $row['c'];
			  }
			  $pesan='Delete Data DO Area '.$iarea.' Nomor:'.$ido;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );  
			  $this->load->view('listdo/vmainform',$data);	
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu59')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listdo/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from v_list_do a
										              left join tm_spb b on (b.i_spb=a.i_spb and b.i_area=a.i_spb_area)
										              left join tm_spmb c on (c.i_spmb=a.i_spmb and c.i_area=a.i_spmb_area)
										              where 
										              upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
										              or upper(i_do) like '%$cari%' or upper(i_op) like '%$cari%'
										              or upper(a.i_spmb) like '%$cari%' or upper(a.i_spb) like '%$cari%'
										              or upper(c.i_spmb_old) like '%$cari%' or upper(b.i_spb_old) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('listdo/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('listdo');
			$data['cari'] = '';
			$data['ido']='';
	 		$this->load->view('listdo/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu59')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listdo/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
        else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('listdo/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listdo/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu59')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listdo/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listdo/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listdo/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu59')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
				$cari	  = strtoupper($this->input->post('cari'));
				$dfrom	= $this->input->post('dfrom');
				$dto	  = $this->input->post('dto');
				$iarea		= $this->input->post('iarea');
				$is_cari	= $this->input->post('is_cari'); 
				
				if($dfrom=='') $dfrom=$this->uri->segment(4);
				if($dto=='') $dto=$this->uri->segment(5);
				if($iarea=='') $iarea=$this->uri->segment(6);
				
				if ($is_cari == '')
					$is_cari= $this->uri->segment(8);
				if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(7);
			
			if ($is_cari=="1") { 
				$config['base_url'] = base_url().'index.php/listdo/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/'.$is_cari.'/index/';
			}
			else { 
				$config['base_url'] = base_url().'index.php/listdo/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			} 
			if ($is_cari != "1") {
				$sql= " select a.i_do
            from tm_do a, tr_supplier e,
            tm_op b left join tm_spb c on(b.i_reff=c.i_spb and b.i_area=c.i_area) left join tm_spmb d on(b.i_reff=d.i_spmb and b.i_area=d.i_area)
            where a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy')
            and a.i_op=b.i_op and a.i_area=b.i_area and a.i_supplier=e.i_supplier and a.i_area='$iarea'";
			}
			else {
				$sql= " select a.i_do, a.d_do, a.i_op, a.i_area, c.i_spb, c.i_spb_old, d.i_spmb, d.i_spmb_old, e.e_supplier_name, a.i_supplier
            from tm_do a, tr_supplier e,
            tm_op b left join tm_spb c on(b.i_reff=c.i_spb and b.i_area=c.i_area) left join tm_spmb d on(b.i_reff=d.i_spmb and b.i_area=d.i_area)
            where a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy')
            and a.i_op=b.i_op and a.i_area=b.i_area and a.i_supplier=e.i_supplier and a.i_area='$iarea'
            and (upper(a.i_supplier) like '%$cari%' or upper(e.e_supplier_name) like '%$cari%'
							or upper(a.i_do) like '%$cari%' or upper(b.i_op) like '%$cari%'
							or upper(d.i_spmb) like '%$cari%' or upper(c.i_spb) like '%$cari%'
							or upper(d.i_spmb_old) like '%$cari%' or upper(c.i_spb_old) like '%$cari%')";
			}
			//echo $sql; die();
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if ($is_cari=="1")
				$config['cur_page'] = $this->uri->segment(10);
			else
				$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			
			$this->load->model('listdo/mmaster');
			$data['page_title'] = $this->lang->line('listdo');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;

			if ($is_cari=="1")
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(10),$cari);
			else
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data DO Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listdo/vmainform',$data);			

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
