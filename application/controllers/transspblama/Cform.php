<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
    require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu219')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transspblama');
			$data['isi']= directory_map('./spblama/');
			$data['file']='';
			$this->load->view('transspblama/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu219')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$file= $this->input->post('namafile', TRUE);
			$data['file']='spblama/'.$file;
			$data['periode']=$this->input->post('periode', TRUE);
			$data['baru']=$this->input->post('baru', TRUE);
			$this->load->view('transspblama/vfile', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu219')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml = $this->input->post('jml', TRUE);
      $filespb = $this->input->post('filespb', TRUE);
      $baru    = $this->input->post('baru', TRUE);
      $langdbf = $this->input->post('langdbf', TRUE);
      $langdbt = $this->input->post('langdbt', TRUE);
      $periode = $this->input->post('periode', TRUE);
      $this->load->model('transspblama/mmaster');
      $this->db->trans_begin();
      $data['kosong']=0;
      $nodok='';
			$spbxx='';
      for($i=0;$i<=$jml;$i++){
        $cek=$this->input->post('chk'.$i, TRUE);
        $spb=$this->input->post('ispb'.$i, TRUE);
        if($cek!=''){
          $db1 =dbase_open($filespb,2) or die ("Could not open dbf file <i>$filespb</i>.");
          if($db1){
            $record_numbers1=dbase_numrecords($db1);
            $noitem=0;
            for($j=1;$j<=$record_numbers1;$j++){
              $col=dbase_get_record_with_names($db1,$j);
              if((substr($col['TGLDOK'],0,6)==$periode) && (!$col['deleted'])) {
                if($col['NODOK']==$spb){
                  $nodok=$col['NODOK'];
                  $kodelang=$col['KODELANG'];
                  $iarea=substr($kodelang,0,2);
                  $dspb=$col['TGLDOK'];
                  $tgldokspb=$col['TGLDOK'];
                  $thbl=substr($dspb,0,6);
                  $dspb=substr($dspb,0,4).'-'.substr($dspb,4,2).'-'.substr($dspb,6,2);
                  $dspbreceive  = $dspb;
                  $ispb='SPB-'.substr($thbl,2,4).'-'.$nodok;
			            $po					  = $col['PO'];

			            $iproduct		  = substr($col['KODEPROD'],0,7);
			            $ipricegroup  = substr($col['KODEPROD'],7,2);
 			            $iproductgrade= 'A';
 			            $iproductmotif= '00';
			            $norder 		  = $col['JPESAN'];
			            $nkirim		    = $col['JKIRIM'];
			            $vunitprice   = $col['HARGASAT'];

			            $top				  = $col['TOP'];
			            $isalesman	  = $col['KODESALES'];
			            $nspbdiscount1= $col['DISCOUNT'];
			            $nspbdiscount2= $col['DISC2'];
			            $nspbdiscount3= $col['DISC3'];
			            $nspbdiscount4= $col['DISC4'];
			            $klp          = $col['KELOMPOK'];
                  if(trim($klp)=='1'){
                    $iproductgroup='00';
                  }else{
                    $iproductgroup='01';
                  }
			            $vspbdiscounttotal= $col['POTONGAN'];
			            $vspb         = $col['KOTOR'];
		              $dtmp1=$nspbdiscount1;
		              $dtmp2=$nspbdiscount2;
		              $dtmp3=$nspbdiscount3;
		              $vspbdiscount1=0;
		              $vspbdiscount2=0;
		              $vspbdiscount3=0;
		              $vspbdiscount4=0;
		              $vspbdiscount1=$vspbdiscount1+(($vspb*$dtmp1)/100);
		              $vspbdiscount2=$vspbdiscount2+((($vspb-$vspbdiscount1)*$dtmp2)/100);
		              $vspbdiscount3=$vspbdiscount3+((($vspb-($vspbdiscount1+$vspbdiscount2))*$dtmp3)/100);
			            $ispbold			= $nodok;
                  $fspbvalid    = 'f';
                  $fspbcancel   = 'f';
                  $eremarkx     = $col['MOTIF'];
                  $fspbstockdaerah  = $col['STOKDAERAH'];
                  if( ($fspbstockdaerah=='1')||($fspbstockdaerah==1) ){
                    $fspbstockdaerah='t';
                    $fspbop='f';
                    $fspbsiapnotagudang='t';
                  }else{
                    $fspbstockdaerah='f';
                    $fspbop='t';
                    $fspbsiapnotagudang='f';
                  }
                  $fspbconsigment = $col['KONSINYASI'];
                  if( ($fspbconsigment=='1')||($fspbconsigment==1) ){
                    $fspbconsigment='t';
#                    $fspbstockdaerah='f';
                    $fspbstockdaerah='t';
                    $fspbsiapnotagudang='t';
                    $fspbvalid    = 't';
                  }else{
                    $fspbconsigment='f';
#                    $fspbstockdaerah='t';
#                    $fspbsiapnotagudang='f';
                  }
                  $fspbprogram  = $col['PROGRAM'];
                  if( ($fspbprogram=='1')||($fspbprogram==1) ){
                    $fspbprogram='t';
                  }else{
                    $fspbprogram='f';
                  }
#			            $adaspb	= $this->mmaster->cekadaspb($ispb,$iarea);
                  if(substr($kodelang,2,3)!='000'){
                    $this->db->select(" * from tr_customer_pkp where i_customer='$kodelang'", false);
                    $query = $this->db->get();
                    if ($query->num_rows() > 0){
                      foreach($query->result() as $cs){
                        $npwp=$cs->e_customer_pkpnpwp;
                      }
                    }else{
                      $npwp='';
                    }
                    $this->db->select(" a.*, b.i_price_group 
                                        from tr_customer a, tr_price_group b 
                                        where a.i_customer='$kodelang'
                                        and (a.i_price_group=b.n_line or a.i_price_group=b.i_price_group)", false);
                    $query = $this->db->get();
                    if ($query->num_rows() > 0){
                      foreach($query->result() as $cs){
                        $pkp=$cs->f_customer_pkp;
                        $plusppn=$cs->f_customer_plusppn;
                        $plusdisc=$cs->f_customer_plusdiscount;
                        $kdhargax=$cs->i_price_group;
                      }
                    }else{
                        $pkp='t';
                        $plusppn='t';
                        $plusdisc='f';
                        $kdhargax='AA';
                    }
                    $this->db->select(" * from tr_customer_discount where i_customer='$kodelang'", false);
                    $query = $this->db->get();
                    if ($query->num_rows() > 0){
                      foreach($query->result() as $cs){
                        $disc1=$cs->n_customer_discount1;
                        $disc2=$cs->n_customer_discount2;
                        $disc3=$cs->n_customer_discount3;
                      }
                    }else{
                        $disc1=0;
                        $disc2=0;
                        $disc3=0;
                    }
                  }else{
                    if($baru!=''){
                      $disc1=0;
                      $disc2=0;
                      $disc3=0;
                      $dbf = new dbf_class($langdbf);
                      $num_rec=$dbf->dbf_num_rec;
                      $field_num=$dbf->dbf_num_field;
                      for($k=0; $k<$num_rec; $k++){
                        if ($row = $dbf->getRow($k)) {
                          for($l=0; $l<$field_num; $l++){
                            switch ($l){
                              case 0  :
                                $nodokx=$row[$l];
                                break;
                              case 1  :
                                $tgldok=$row[$l];
                                $tgldokx=$row[$l];
                                break;
                              case 2  :
                                $wila=$row[$l];
                                break;
                              case 3  :
                                $nolang=$row[$l];
                                break;
                              case 4  :
                                $nama=$row[$l];
                                break;
                              case 5  :
                                $alamat=$row[$l];
                                break;
                              case 6  :
                                $kota=$row[$l];
                                break;
                              case 7  :
                                $kodepos=$row[$l];
                                break;
                              case 8  :
                                $nmmilik=$row[$l];
                                break;
                              case 9 :
                                $almmilik=$row[$l];
                                break;
                              case 10 :
                                $npwp=$row[$l];
                                break;
                              case 11 :
                                $telepon=$row[$l];
                                break;
                              case 12 :
                                $fax=$row[$l];
                                break;
                              case 13 :
                                $telepon2=$row[$l];
                                break;
                              case 14 :
                                $jenis=$row[$l];
                                break;
                              case 15 :
                                $jenis2=$row[$l];
                                break;
                              case 16 :
                                $discount=$row[$l];
                                break;
                              case 17 :
                                $bayar=$row[$l];
                                break;
                              case 18 :
                                $pkp=$row[$l];
                                break;
                              case 19 :
                                $pkp2=$row[$l];
                                break;
                              case 20 :
                                $kodesales=$row[$l];
                                break;
                              case 21 :
                                $tgldaftar=$row[$l];
                                break;
                              case 22 :
                                $cperson=$row[$l];
                                break;
                              case 23 :
                                $email=$row[$l];
                                break;
                              case 24 :
                                $adamemo=$row[$l];
                                break;
                              case 25 :
                                $memo=$row[$l];
                                break;
                              case 26 :
                                $nonpwpbaru=$row[$l];
                                break;
                              case 27 :
                                $plusppn=$row[$l];
                                break;
                              case 28 :
                                $plusdisc=$row[$l];
                                break;
                              case 29 :
                                $npwpbaru=$row[$l];
                                break;
                              default:
                            }
                          }
                          if($plusppn=='')$plusppn='t';else $plusppn='f';
                          if($plusdisc=='')$plusdisc='t';else $plusdisc='f';
                          if($pkp='2') $pkp='f'; else $pkp='t';
                          if( ($nodokx==$nodok) && ($tgldokspb==$tgldokx) ){
#                            echo 'ada memo = '.$adamemo.'<br>';
#                            echo 'memo = '.$memo.'<br>';
                            $nama	  = str_replace("'","",$nama);
                            $alamat	= str_replace("'","",$alamat);
                            if($adamemo=='F') $memo='';
                            $this->mmaster->insert_xlang($nodok,$tgldok,$wila,$nolang,$nama,$alamat,$kota,$kodepos,
                                                         $nmmilik,$almmilik,$npwp,$telepon,$fax,$telepon2,$jenis,
                                                         $jenis2,$discount,$bayar,$pkp,$pkp2,$kodesales,$tgldaftar,
                                                         $cperson,$email,$adamemo,$memo,$nonpwpbaru,$plusppn,$plusdisc,
                                                         $npwpbaru);

                            $this->db->select(" n_line from tr_price_group where i_price_group='$ipricegroup'", false);
                            $query = $this->db->get();
                            if ($query->num_rows() > 0){
                              foreach($query->result() as $pg){
                                $kdhrg=$pg->n_line;
                              }
                            }

                            $this->mmaster->insert_lang
		                          ($nodok,$wila,$nolang,$nama,$alamat,$kota,$kodepos,
                               $nmmilik,$almmilik,$npwp,$telepon,$fax,$telepon2,$jenis,
                               $jenis2,$discount,$bayar,$pkp,$pkp2,$kodesales,$tgldaftar,
                               $cperson,$email,$adamemo,$memo,$nonpwpbaru,$plusppn,$plusdisc,
                               $npwpbaru,$thbl,$kdhrg
		                          );
                          }
                        }
                      }
                          
                    }
                  }

                  settype($nodok,"string");
                  $a=strlen($nodok);
                  while($a<6){
                    $nodok="0".$nodok;
                    $a=strlen($nodok);
                  }
                  $ispb	=$this->mmaster->runningnumber($iarea,$thbl,$nodok);
									$spbxx=$spbxx.' '.$ispb;
                  $que=$this->db->query("select i_spb from tm_spb where i_spb='$ispb' and i_area='$iarea'", false);
                  $sudahada=false;
                  if($que->num_rows()==0){
                    if($fspbprogram=='t'){
                      $isiquery="select i_promo from tm_promo 
                                 where d_promo_start<='$dspb' 
                                 and d_promo_finish>='$dspb'
                                 and i_price_group='$ipricegroup'
                                 and 
                                      (
                                        (f_all_area='t')
                                        or
                                        (f_all_customer='t')
                                        or
                                        (f_all_customer='f' 
                                         and i_promo in (select i_promo from tm_promo_customer
                                                         where i_customer='$kodelang'))
                                        or
                                        (f_all_area='f' 
                                         and i_promo in (select i_promo from tm_promo_customer
                                                         where i_customer='$kodelang') ";
                      if( ($nspbdiscount1==0)&&($nspbdiscount2==0)&&($nspbdiscount3==0)&&
                          ($ipricegroup!='00')&&($ipricegroup!='01')&&($ipricegroup!='02')&&($ipricegroup!='03')&&
                          ($ipricegroup!='04')&&($ipricegroup!='05')&&($ipricegroup!='G0')&&($ipricegroup!='G2')&&
                          ($ipricegroup!='G3')&&($ipricegroup!='G5') ){
                        $isiquery.="and i_promo_type='2' ";  
                      }elseif( ($nspbdiscount1==$disc1)&&($nspbdiscount2==$disc2)&&($nspbdiscount3==$disc3)&&
                          ($ipricegroup!='00')&&($ipricegroup!='01')&&($ipricegroup!='02')&&($ipricegroup!='03')&&
                          ($ipricegroup!='04')&&($ipricegroup!='05')&&($ipricegroup!='G0')&&($ipricegroup!='G2')&&
                          ($ipricegroup!='G3')&&($ipricegroup!='G5') ){
                        $isiquery.="and i_promo_type='4' ";  
                      }elseif( ($nspbdiscount1==$disc1)&&
                          ($ipricegroup!='00')&&($ipricegroup!='01')&&($ipricegroup!='02')&&($ipricegroup!='03')&&
                          ($ipricegroup!='04')&&($ipricegroup!='05')&&($ipricegroup!='G0')&&($ipricegroup!='G2')&&
                          ($ipricegroup!='G3')&&($ipricegroup!='G5') ){
                        $isiquery.="and i_promo_type='5' ";  
                      }elseif( ($nspbdiscount1!=$disc1)&&($nspbdiscount2!=$disc2)&&($nspbdiscount3!=$disc3)&&
                          (
                          ($ipricegroup!='00')||($ipricegroup!='01')||($ipricegroup!='02')||($ipricegroup!='03')||
                          ($ipricegroup!='04')||($ipricegroup!='05')||($ipricegroup!='G0')||($ipricegroup!='G2')||
                          ($ipricegroup!='G3')||($ipricegroup!='G5') 
                          ) ){
                        $isiquery.="and i_promo_type='1' ";  
                      }elseif( 
                          (
                            ($nspbdiscount1==$disc1)&&(($nspbdiscount2!=$disc2)||($nspbdiscount3!=$disc3))
                          ) &&
                          (
                          ($ipricegroup!='00')||($ipricegroup!='01')||($ipricegroup!='02')||($ipricegroup!='03')||
                          ($ipricegroup!='04')||($ipricegroup!='05')||($ipricegroup!='G0')||($ipricegroup!='G2')||
                          ($ipricegroup!='G3')||($ipricegroup!='G5') 
                          ) ){
                        $isiquery.="and i_promo_type='3' ";  
                      }

                      $isiquery.="     )
                                      )";
#                      echo $isiquery;
                      $que=$this->db->query($isiquery, false);
                      if($que->num_rows()>0){
                        $zzz=0;
                        foreach($que->result() as $xrow){
                          $zzz++;
                          if(!isset($kdhargax))$kdhargax=$ipricegroup;
                          $ipricegroup=$kdhargax;
                          if($zzz==1){
                            $this->mmaster->insertheaderpromo($ispb, $dspb, $kodelang, $iarea, $po, $top, $isalesman, 
                                                              $ipricegroup, $dspbreceive, $fspbop, $npwp, $pkp, 
                                                              $plusppn, $plusdisc, $fspbstockdaerah, $fspbprogram,
                                                              $fspbvalid, $fspbsiapnotagudang, $fspbcancel,
                                                              $nspbdiscount1, $nspbdiscount2, $nspbdiscount3,
                                                              $vspbdiscount1, $vspbdiscount2, $vspbdiscount3,
                                                              $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold,
                                                              $eremarkx,$iproductgroup, $xrow->i_promo
                                                              );
                          }
                        }
                      }else{
#####
                        $isiquery="select i_promo from tm_promo 
                                   where d_promo_start<='$dspb' 
                                   and d_promo_finish>='$dspb'
                                   and 
                                        (
                                          (f_all_area='t')
                                          or
                                          (f_all_customer='t')
                                          or
                                          (f_all_customer='f' 
                                           and i_promo in (select i_promo from tm_promo_customer
                                                           where i_customer='$kodelang'))
                                          or
                                          (f_all_area='f' 
                                           and i_promo in (select i_promo from tm_promo_customer
                                                           where i_customer='$kodelang') ";
                        if( ($nspbdiscount1==0)&&($nspbdiscount2==0)&&($nspbdiscount3==0)&&
                            ($ipricegroup!='00')&&($ipricegroup!='01')&&($ipricegroup!='02')&&($ipricegroup!='03')&&
                            ($ipricegroup!='04')&&($ipricegroup!='05')&&($ipricegroup!='G0')&&($ipricegroup!='G2')&&
                            ($ipricegroup!='G3')&&($ipricegroup!='G5') ){
                          $isiquery.="and i_promo_type='2' ";  
                        }elseif( ($nspbdiscount1==$disc1)&&($nspbdiscount2==$disc2)&&($nspbdiscount3==$disc3)&&
                            ($ipricegroup!='00')&&($ipricegroup!='01')&&($ipricegroup!='02')&&($ipricegroup!='03')&&
                            ($ipricegroup!='04')&&($ipricegroup!='05')&&($ipricegroup!='G0')&&($ipricegroup!='G2')&&
                            ($ipricegroup!='G3')&&($ipricegroup!='G5') ){
                          $isiquery.="and i_promo_type='4' ";  
                        }elseif( ($nspbdiscount1==$disc1)&&
                            ($ipricegroup!='00')&&($ipricegroup!='01')&&($ipricegroup!='02')&&($ipricegroup!='03')&&
                            ($ipricegroup!='04')&&($ipricegroup!='05')&&($ipricegroup!='G0')&&($ipricegroup!='G2')&&
                            ($ipricegroup!='G3')&&($ipricegroup!='G5') ){
                          $isiquery.="and i_promo_type='5' ";  
                        }elseif( ($nspbdiscount1!=$disc1)&&($nspbdiscount2!=$disc2)&&($nspbdiscount3!=$disc3)&&
                            (
                            ($ipricegroup!='00')||($ipricegroup!='01')||($ipricegroup!='02')||($ipricegroup!='03')||
                            ($ipricegroup!='04')||($ipricegroup!='05')||($ipricegroup!='G0')||($ipricegroup!='G2')||
                            ($ipricegroup!='G3')||($ipricegroup!='G5') 
                            ) ){
                          $isiquery.="and i_promo_type='1' ";  
                        }elseif( 
                            (
                              ($nspbdiscount1==$disc1)&&(($nspbdiscount2!=$disc2)||($nspbdiscount3!=$disc3))
                            ) &&
                            (
                            ($ipricegroup!='00')||($ipricegroup!='01')||($ipricegroup!='02')||($ipricegroup!='03')||
                            ($ipricegroup!='04')||($ipricegroup!='05')||($ipricegroup!='G0')||($ipricegroup!='G2')||
                            ($ipricegroup!='G3')||($ipricegroup!='G5') 
                            ) ){
                          $isiquery.="and i_promo_type='3' ";  
                        }
                        $isiquery.="     )
                                        )";
                        $que=$this->db->query($isiquery, false);
                        if($que->num_rows()>0){
                          $zzz=0;
                          foreach($que->result() as $xrow){
                            $zzz++;
                            $ipricegroup=$kdhargax;
                            if($zzz==1){
                              $this->mmaster->insertheaderpromo($ispb, $dspb, $kodelang, $iarea, $po, $top, $isalesman, 
                                                                $ipricegroup, $dspbreceive, $fspbop, $npwp, $pkp, 
                                                                $plusppn, $plusdisc, $fspbstockdaerah, $fspbprogram,
                                                                $fspbvalid, $fspbsiapnotagudang, $fspbcancel,
                                                                $nspbdiscount1, $nspbdiscount2, $nspbdiscount3,
                                                                $vspbdiscount1, $vspbdiscount2, $vspbdiscount3,
                                                                $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold,
                                                                $eremarkx,$iproductgroup, $xrow->i_promo
                                                                );
                            }
                          }
                        }
#####
                      }
                    }else{
                      if($fspbconsigment=='f'){
                        $this->mmaster->insertheader($ispb, $dspb, $kodelang, $iarea, $po, $top, $isalesman, 
                                                     $ipricegroup, $dspbreceive, $fspbop, $npwp, $pkp, 
                                                     $plusppn, $plusdisc, $fspbstockdaerah, $fspbprogram,
                                                     $fspbvalid, $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1, 
                                                     $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
                                                     $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment,
                                                     $ispbold, $eremarkx,$iproductgroup);
                      }else{
                        $ipricegroup=$kdhargax;
                        $this->mmaster->insertheaderkons($ispb, $dspb, $kodelang, $iarea, $po, $top, $isalesman, 
                                                     $ipricegroup, $dspbreceive, $fspbop, $npwp, $pkp, 
                                                     $plusppn, $plusdisc, $fspbstockdaerah, $fspbprogram,
                                                     $fspbvalid, $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1, 
                                                     $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
                                                     $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, 
                                                     $ispbold, $eremarkx,$iproductgroup);
                      }
                    }
		                
                  }else{
                    $sudahada=true;
                  }
                  /*  
                    else{
                    if($fspbprogram=='t'){
                      $isiquery="select i_promo from tm_promo 
                                 where d_promo_start<='$dspb' 
                                 and d_promo_finish>='$dspb'
                                 and i_price_group='$ipricegroup'
                                 and 
                                      (
                                        (f_all_area='t')
                                        or
                                        (f_all_area='f' 
                                         and i_promo in (select i_promo from tm_promo_customer
                                                         where i_customer='$kodelang') ";
                      if( ($nspbdiscount1==0)&&($nspbdiscount2==0)&&($nspbdiscount3==0)&&
                          ($ipricegroup!='00')&&($ipricegroup!='01')&&($ipricegroup!='02')&&($ipricegroup!='03')&&
                          ($ipricegroup!='04')&&($ipricegroup!='05')&&($ipricegroup!='G0')&&($ipricegroup!='G2')&&
                          ($ipricegroup!='G3')&&($ipricegroup!='G5') ){
                        $isiquery.="and i_promo_type='2' ";  
                      }elseif( ($nspbdiscount1==$disc1)&&($nspbdiscount2==$disc2)&&($nspbdiscount3==$disc3)&&
                          ($ipricegroup!='00')&&($ipricegroup!='01')&&($ipricegroup!='02')&&($ipricegroup!='03')&&
                          ($ipricegroup!='04')&&($ipricegroup!='05')&&($ipricegroup!='G0')&&($ipricegroup!='G2')&&
                          ($ipricegroup!='G3')&&($ipricegroup!='G5') ){
                        $isiquery.="and i_promo_type='4' ";  
                      }elseif( ($nspbdiscount1==$disc1)&&
                          ($ipricegroup!='00')&&($ipricegroup!='01')&&($ipricegroup!='02')&&($ipricegroup!='03')&&
                          ($ipricegroup!='04')&&($ipricegroup!='05')&&($ipricegroup!='G0')&&($ipricegroup!='G2')&&
                          ($ipricegroup!='G3')&&($ipricegroup!='G5') ){
                        $isiquery.="and i_promo_type='5' ";  
                      }elseif( ($nspbdiscount1!=$disc1)&&($nspbdiscount2!=$disc2)&&($nspbdiscount3!=$disc3)&&
                          (
                          ($ipricegroup!='00')||($ipricegroup!='01')||($ipricegroup!='02')||($ipricegroup!='03')||
                          ($ipricegroup!='04')||($ipricegroup!='05')||($ipricegroup!='G0')||($ipricegroup!='G2')||
                          ($ipricegroup!='G3')||($ipricegroup!='G5') 
                          ) ){
                        $isiquery.="and i_promo_type='1' ";
                      }elseif( 
                          (
                            ($nspbdiscount1==$disc1)&&(($nspbdiscount2!=$disc2)||($nspbdiscount3!=$disc3))
                          ) &&
                          (
                          ($ipricegroup!='00')||($ipricegroup!='01')||($ipricegroup!='02')||($ipricegroup!='03')||
                          ($ipricegroup!='04')||($ipricegroup!='05')||($ipricegroup!='G0')||($ipricegroup!='G2')||
                          ($ipricegroup!='G3')||($ipricegroup!='G5') 
                          ) ){
                        $isiquery.="and i_promo_type='3' ";  
                      }

                      $isiquery.="     )
                                      )";
                      $que=$this->db->query($isiquery, false);
                      if($que->num_rows()>0){
                        foreach($que->result() as $xrow){
                          $ipricegroup=$kdhargax;
                          $this->mmaster->updateheaderpromo($ispb, $dspb, $kodelang, $iarea, $po, $top, $isalesman, 
																                 $ipricegroup, $dspbreceive, $fspbop, $npwp, $pkp, 
																                 $plusppn, $plusdisc, $fspbstockdaerah, $fspbprogram,
                                                 $fspbvalid, $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1, 
																                 $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
																                 $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold,
                                                 $eremarkx,$iproductgroup,$xrow->i_promo);
                        }
                      }
                    }else{
                      if(isset($kdhargax)){
                        $ipricegroup=$kdhargax;
                      }
                      $this->mmaster->updateheader($ispb, $dspb, $kodelang, $iarea, $po, $top, $isalesman, 
																                 $ipricegroup, $dspbreceive, $fspbop, $npwp, $pkp, 
																                 $plusppn, $plusdisc, $fspbstockdaerah, $fspbprogram,
                                                 $fspbvalid, $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1, 
																                 $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
																                 $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold,
                                                 $eremarkx,$iproductgroup);
                    }
                  }
                  */
                  $que=$this->db->query("select e_product_name , i_product_status
                                         from tr_product where i_product='$iproduct'", false);
                  if($que->num_rows()>0){
                    foreach($que->result() as $trp){
                      $nami=$trp->e_product_name;
                      $iproductstatus=$trp->i_product_status;
                    }
                  }else{
                    $nami='';
                  }
                  $noitem++;
#                  $noitem=$j;
                  if(!$sudahada){
    	              $this->mmaster->insertdetail( $ispb,$iarea,$iproduct,$iproductstatus,$iproductgrade,$nami,$norder,$nkirim,
                          							          $vunitprice,$iproductmotif,$eremarkx,$noitem);
                  }else{
                    $que=$this->db->query("select i_spb from tm_spb_item where i_spb='$ispb' and i_area='$iarea'
                                           and i_product='$iproduct' and i_product_motif='$iproductmotif' 
                                           and i_product_grade='$iproductgrade' and i_product_status='$iproductstatus'
                                           and v_unit_price=$vunitprice", false);
                    if($que->num_rows()==0){                  
      	              $this->mmaster->insertdetail( $ispb,$iarea,$iproduct,$iproductstatus,$iproductgrade,$nami,$norder,$nkirim,
                            							          $vunitprice,$iproductmotif,$eremarkx,$noitem);
                    }
                  }
                }else{
                  $noitem=0;
                }
			        }
		        }
          }
        }
      }
#     $data['baru']=$Record;
      $data['baru']=$baru;
			if ( ($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
				$this->load->view('transspblama/vformgagal',$data);
			}else{
				$this->db->trans_commit();
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Transfer SPB Area '.$iarea.' No:'.$spbxx;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );
				$this->load->view('transspblama/vformsukses',$data);
			}			

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
