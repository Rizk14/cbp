<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu63')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printop');
			$data['opfrom']='';
			$data['opto']	='';
			$this->load->view('printopkelompok/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function opfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu63')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/printopkelompok/cform/opfrom/index/';
			$query = $this->db->query("	select i_op from tm_op
										where upper(i_op) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printopkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_op');
			$data['isi']=$this->mmaster->bacaop($config['per_page'],$this->uri->segment(5));
			$this->load->view('printopkelompok/vlistopfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariopfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu63')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/printopkelompok/cform/opfrom/index/';
			$query = $this->db->query("	select i_op from tm_op
										where upper(i_op) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printopkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_op');
			$data['isi']=$this->mmaster->cariop($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printopkelompok/vlistopfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function opto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu63')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/printopkelompok/cform/opto/index/';
			$query = $this->db->query("	select i_op from tm_op
										where upper(i_op) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printopkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_op');
			$data['isi']=$this->mmaster->bacaop($config['per_page'],$this->uri->segment(5));
			$this->load->view('printopkelompok/vlistopto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariopto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu63')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$config['base_url'] = base_url().'index.php/printopkelompok/cform/opto/index/';
			$query = $this->db->query("	select i_op from tm_op
										where upper(i_op) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printopkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_op');
			$data['isi']=$this->mmaster->cariop($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printopkelompok/vlistopto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu63')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	= $this->input->post('areafrom');
			$opfrom= $this->input->post('opfrom');
			$opto	= $this->input->post('opto');
			$this->load->model('printopkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['master']	=$this->mmaster->bacamaster($opfrom,$opto);
			$data['area']	=$area;
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak OP No:'.$opfrom.' s/d No:'.$opto;
#			$this->load->model('logger');
#			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printopkelompok/vformrpt',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
