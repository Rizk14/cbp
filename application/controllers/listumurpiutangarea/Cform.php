<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->dbutil();
    $this->load->helper('file');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu501')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listumurpiutangarea');
			$data['iperiode']='';
			$data['d_opname']='';
			$this->load->view('listumurpiutangarea/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu501')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listumurpiutangarea');
			$iperiode=$this->uri->segment(4);
			$d_opname=$this->uri->segment(5);
			  $tmp=explode("-",$d_opname);
			  $th=$tmp[2];
			  $bl=$tmp[1];
			  $hr=$tmp[0];
			  $d_opname=$th."-".$bl."-".$hr;
			  
			$this->load->model('listumurpiutangarea/mmaster');
			$data['iperiode'] = $iperiode;
			$data['d_opname'] = $d_opname;
			$data['isi']		= $this->mmaster->bacaperiode($iperiode,$d_opname);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Laporan Umur Piutang Per Area Periode '.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('listumurpiutangarea/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu501')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listumurpiutangarea');
			$this->load->view('listumurpiutangarea/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu501')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listumurpiutangarea/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    group by a.i_store, b.e_store_name",false);
			}else{
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                    or a.i_area = '$area4' or a.i_area = '$area5')
                                    group by a.i_store, b.e_store_name",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';

			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listumurpiutangarea/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listumurpiutangarea/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu501')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listumurpiutangarea/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store and (upper(a.i_area) like '%$cari%' or upper(a.e_area_name) like '%$cari%')
                                    group by a.i_store, b.e_store_name",false);
			}else{
				$query = $this->db->query("select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                    or a.i_area = '$area4' or a.i_area = '$area5')
                                    and (upper(a.i_area) like '%$cari%' or upper(a.e_area_name) like '%$cari%')
                                    group by a.i_store, b.e_store_name
                                    order by a.i_store, b.e_store_name",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listumurpiutangarea/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listumurpiutangarea/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
      $dopname = $this->input->post('d_opname');
		  $tmp=explode("-",$dopname);
		  $th=$tmp[2];
		  $bl=$tmp[1];
		  $hr=$tmp[0];
		  $dopname=$th."-".$bl."-".$hr;
			$data['page_title'] = $this->lang->line('listumurpiutangarea');
			$data['iperiode'] = $iperiode;
			$data['d_opname'] = $dopname;
			$this->load->model('listumurpiutangarea/mmaster');
			$isi		= $this->mmaster->bacaperiode($iperiode,$dopname);
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();

			$objPHPExcel->getProperties()->setTitle("Laporan Umur Piutang Per Area")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      if(count($isi)>0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(

						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,

						'wrap'      => true
					)
				),
				'A1:J1'
				);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(

							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'BLM JATUH TEMPO');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)

						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', '0-15');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),

					)
				);			
			  $objPHPExcel->getActiveSheet()->setCellValue('D1', '16-30');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', '31-45');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', '46-60');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)

				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', '61-75');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(

						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', '76-90');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', '>90');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'TOTAL');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(

							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

        $i=2;
	      $j=0;
	      $area='';
	      $total=0;
	      $grandtotal=0;
			  $cell='B';
        foreach($isi as $row){
            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			      array(
				      'font' => array(
					      'name'	=> 'Arial',

					      'bold'  => false,
					      'italic'=> false,
					      'size'  => 10
				      )
			      ),

			      'A'.$i.':J'.$i
			      );

            if($area==''){
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->area, Cell_DataType::TYPE_STRING);
         	  }elseif($area!='' && $area!=$row->area){
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $total, Cell_DataType::TYPE_NUMERIC);
           	    $i++;
           	    $total=0;
           	    $cell='B';
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->area, Cell_DataType::TYPE_STRING);      	    
         	  }
                $objPHPExcel->getActiveSheet()->setCellValueExplicit($cell.$i, $row->jumlah, Cell_DataType::TYPE_NUMERIC);
                $area=$row->area;
                $total = $total+$row->jumlah;
                $cell++;
          }
            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
		        array(
			        'font' => array(
				        'name'	=> 'Arial',
				        'bold'  => false,
				        'italic'=> false,
				        'size'  => 10
			        )
		        ),
		        'A'.$i.':J'.$i
		        );       
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $total, Cell_DataType::TYPE_NUMERIC);
            $i++;
            $sql = "select * from f_umur_piutang_nasional('$iperiode','$dopname')";
            $rs		= pg_query($sql);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, 'GRAND TOTAL', Cell_DataType::TYPE_STRING);
            $cell='B';
          	while($raw=pg_fetch_assoc($rs)){
          		$jumlah    = $raw['jumlah'];
              $grandtotal = $grandtotal + $jumlah;
              $objPHPExcel->getActiveSheet()->setCellValueExplicit($cell.$i, $jumlah, Cell_DataType::TYPE_NUMERIC);
              $cell++;
            }
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $grandtotal, Cell_DataType::TYPE_NUMERIC);
        }

			  $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $nama='Export Laporan Umur Piutang Per Area Periode'.$iperiode.'.xls';
        if(file_exists('excel/'.$nama)){
          @chmod('excel/'.$nama, 0777);
          @unlink('excel/'.$nama);
        }
			  $objWriter->save('excel/00/'.$nama);
			  $sess=$this->session->userdata('session_id');

			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){

					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{

				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){

				  $now	  = $row['c'];
			  }
			  $pesan='Export Laporan Umur Piutang Per Area Periode:'.$iperiode;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['sukses']			= true;
			  $data['inomor']			= $pesan;
			  $this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
