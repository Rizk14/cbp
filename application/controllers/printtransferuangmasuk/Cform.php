<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu200')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_kum');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('printtransferuangmasuk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()

	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu200')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printtransferuangmasuk/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			
			$query = $this->db->query(" select a.i_customer AS icustomer, 
				a.d_kum AS dkum, 
				a.f_kum_cancel AS fkumcancel, 
				a.i_kum AS ikum, 
				a.d_kum AS dkum, 
				a.e_bank_name AS ebankname,
				c.e_customer_name AS ecustomername,
				e.e_customer_setor AS ecustomersetor,
				a.e_remark AS eremark,
				a.v_jumlah AS vjumlah,
				a.v_sisa AS vsisa,
				a.f_close AS fclose,
				a.n_kum_year AS nkumyear,
				d.i_area AS iarea, 
				tm_dt.i_dt AS idt, 
				tm_dt.d_dt AS ddt, 
				tm_pelunasan.i_pelunasan AS ipelunasan, 
				tm_pelunasan.i_giro AS igiro
				
				from tm_kum a
				
						left join tr_customer c on(a.i_customer=c.i_customer)
					    left join tr_area d on(a.i_area=d.i_area)
						left join tr_customer_owner e on(a.i_customer=e.i_customer)
						left join tm_pelunasan on(a.i_kum=tm_pelunasan.i_giro)
						left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt)
											
						where (upper(a.i_kum) like '%$cari%') and
						((tm_pelunasan.i_jenis_bayar!='02' and 
						tm_pelunasan.i_jenis_bayar!='01' and 
						tm_pelunasan.i_jenis_bayar!='04' and 
						tm_pelunasan.i_jenis_bayar='03') or ((tm_pelunasan.i_jenis_bayar='03') is null)) and										    
						a.i_area='$iarea' and
						(a.d_kum >= to_date('$dfrom','dd-mm-yyyy') and
						a.d_kum <= to_date('$dto','dd-mm-yyyy')) and a.f_kum_cancel='f' ",false);
						
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_kum');
			$this->load->model('printtransferuangmasuk/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('printtransferuangmasuk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()

	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu200')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			
			$query = $this->db->query(" select a.i_customer AS icustomer, 
				a.d_kum AS dkum, 
				a.f_kum_cancel AS fkumcancel, 
				a.i_kum AS ikum, 
				a.d_kum AS dkum, 
				a.e_bank_name AS ebankname,
				c.e_customer_name AS ecustomername,
				e.e_customer_setor AS ecustomersetor,
				a.e_remark AS eremark,
				a.v_jumlah AS vjumlah,
				a.v_sisa AS vsisa,
				a.f_close AS fclose,
				a.n_kum_year AS nkumyear,
				d.i_area AS iarea, 
				tm_dt.i_dt AS idt, 
				tm_dt.d_dt AS ddt, 
				tm_pelunasan.i_pelunasan AS ipelunasan, 
				tm_pelunasan.i_giro AS igiro
				
				from tm_kum a
				
					left join tr_customer c on(a.i_customer=c.i_customer)
					left join tr_area d on(a.i_area=d.i_area)
					left join tr_customer_owner e on(a.i_customer=e.i_customer)
					left join tm_pelunasan on(a.i_kum=tm_pelunasan.i_giro)
					left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt)
											
					where (upper(a.i_kum) like '%$cari%') and
					((tm_pelunasan.i_jenis_bayar!='02' and 
					tm_pelunasan.i_jenis_bayar!='01' and 
					tm_pelunasan.i_jenis_bayar!='04' and 
					tm_pelunasan.i_jenis_bayar='03') or ((tm_pelunasan.i_jenis_bayar='03') is null)) and										    
					a.i_area='$iarea' and
					(a.d_kum >= to_date('$dfrom','dd-mm-yyyy') and
					a.d_kum <= to_date('$dto','dd-mm-yyyy')) and a.f_kum_cancel='f' ",false);
						
			$num	= $query->num_rows();
			
			$data['page_title'] = $this->lang->line('list_kum');
			$this->load->model('printtransferuangmasuk/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['item']		= $num;
			$data['isi']		= $this->mmaster->cetakku($iarea,$dfrom,$dto,$cari);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak KU masuk Area:'.$iarea.' Periode:'.$dfrom.' s/d No:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printtransferuangmasuk/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}	
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu200')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_kum');
			$this->load->view('printtransferuangmasuk/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu200')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printtransferuangmasuk/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
						
			$query = $this->db->query(" select a.i_customer AS icustomer, 
				a.d_kum AS dkum, 
				a.f_kum_cancel AS fkumcancel, 
				a.i_kum AS ikum, 
				a.d_kum AS dkum, 
				a.e_bank_name AS ebankname,
				c.e_customer_name AS ecustomername,
				e.e_customer_setor AS ecustomersetor,
				a.e_remark AS eremark,
				a.v_jumlah AS vjumlah,
				a.v_sisa AS vsisa,
				a.f_close AS fclose,
				a.n_kum_year AS nkumyear,
				d.i_area AS iarea,
				tm_dt.i_dt AS idt, 
				tm_dt.d_dt AS ddt, 
				tm_pelunasan.i_pelunasan AS ipelunasan, 
				tm_pelunasan.i_giro AS igiro
								
				from tm_kum a
				
					left join tr_customer c on(a.i_customer=c.i_customer)
					left join tr_area d on(a.i_area=d.i_area)
					left join tr_customer_owner e on(a.i_customer=e.i_customer)
					left join tm_pelunasan on(a.i_kum=tm_pelunasan.i_giro)
					left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt)
										
					where (upper(a.i_kum) like '%$cari%' or upper(a.i_customer) like '%$cari%') and
					((tm_pelunasan.i_jenis_bayar!='02' and 
					tm_pelunasan.i_jenis_bayar!='01' and 
					tm_pelunasan.i_jenis_bayar!='04' and 
					tm_pelunasan.i_jenis_bayar='03') or ((tm_pelunasan.i_jenis_bayar='03') is null)) and																			
					a.i_area='$iarea' and
					(a.d_kum >= to_date('$dfrom','dd-mm-yyyy') and
					a.d_kum <= to_date('$dto','dd-mm-yyyy')) and a.f_kum_cancel='f' ",false);
					
				$num	= $query->num_rows();	
				$config['total_rows'] = $num; 
				$config['per_page']   = '10';
				$config['first_link'] = 'Awal';
				$config['last_link']  = 'Akhir';
				$config['next_link']  = 'Selanjutnya';
				$config['prev_link']  = 'Sebelumnya';
				$config['cur_page']   = $this->uri->segment(8);
				$this->pagination->initialize($config);

				$data['page_title'] = $this->lang->line('list_kum');
				$this->load->model('printtransferuangmasuk/mmaster');
				$data['cari']		= $cari;
				$data['dfrom']		= $dfrom;
				$data['dto']		= $dto;
				$data['iarea']		= $iarea;
				$data['item']		= $num;
				$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
				$this->load->view('printtransferuangmasuk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu200')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printtransferuangmasuk/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('printtransferuangmasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printtransferuangmasuk/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu141')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printtransferuangmasuk/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printtransferuangmasuk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printtransferuangmasuk/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
