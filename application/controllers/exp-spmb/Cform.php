<?php




class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('spmb');
			$data['iperiode']	= '';
			$data['istore']	  = '';
			$data['ispmb']	  = '';
			$this->load->view('exp-spmb/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-spmb/cform/area/index/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('exp-spmb/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-spmb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('exp-spmb/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function spmb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$istore = $this->input->post("istore");
			#$area=$this->input->post("area");
			$periode = $this->input->post("periode");
			$cari = $this->input->post("cari");
			if ($istore == '') $istore = $this->uri->segment(4);
			#if($area=='')$area=$this->uri->segment(4);
			if ($periode == '') $periode = $this->uri->segment(5);
			$config['base_url'] = base_url() . 'index.php/exp-spmb/cform/spmb/' . $istore . '/' . $periode . '/';
			#$config['base_url'] = base_url().'index.php/exp-spmb/cform/spmb/'.$area.'/'.$periode.'/';
			$query = $this->db->query("select a.i_spmb from tm_spmb a, tr_area b
                                 where a.i_area=b.i_area and b.i_store = '$istore' and substring(a.i_spmb,6,4)='$periode'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('exp-spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_spmb');
			$data['isi'] = $this->mmaster->bacaspmb($config['per_page'], $this->uri->segment(6), $istore, $periode, $cari);
			#$data['isi']=$this->mmaster->bacaspmb($config['per_page'],$this->uri->segment(6),$area,$periode,$cari);
			$data['istore'] = $istore;
			#$data['area']=$area;
			$data['periode'] = $periode;
			$data['cari'] = $cari;
			$this->load->view('exp-spmb/vlistspmb', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-spmb/cform/store/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store");
			} else {
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store
                                  and (c.i_area = '$area1' or c.i_area = '$area2' or
                                  c.i_area = '$area3' or c.i_area = '$area4' or
                                  c.i_area = '$area5')");
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi'] = $this->mmaster->bacastore($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('exp-spmb/vliststore', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-spmb/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
				$query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')", false);
			} else {
				$query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')
                                    and (c.i_area = '$area1' or c.i_area = '$area2' or
                                    c.i_area = '$area3' or c.i_area = '$area4' or
                                    c.i_area = '$area5')", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi'] = $this->mmaster->caristore($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('exp-spmb/vliststore', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-spmb/mmaster');
			$istore    = $this->input->post('istore');
			$istoreloc = $this->input->post('istorelocation');
			#$iarea    = $this->input->post('iarea');
			$iperiode = $this->input->post('iperiode');
			$ispmb    = $this->input->post('ispmb');
			$a = substr($iperiode, 0, 4);
			$b = substr($iperiode, 4, 2);
			$peri = mbulan($b) . " - " . $a;

			if ($istore == 'AA') {
				$istoreloc = '01';
			}
			#####      
			$thak = substr($a, 2, 2);
			$blak = $b;
			$blaw = intval($b);
			$thaw = intval($a);
			for ($z = 1; $z <= 3; $z++) {
				$blaw = $blaw - 1;
				if ($blaw == 0) {
					$blaw = 12;
					$thaw = $thaw - 1;
				}
			}
			$thaw = strval($thaw);
			$thaw = substr($thaw, 2, 2);
			$blaw = substr($blaw, 0, 2);
			if (strlen($blaw) == 1) {
				$blaw = '0' . $blaw;
			}
			$peraw = $thaw . $blaw;
			$perak = $thak . $blak;
			$perkini = substr($iperiode, 2, 4) . '-';
			#      $perkini=substr($iperiode,2,4).'-'.$istore;
			/* $query = $this->db->query("select i_area,e_area_name from tr_area where i_store='$istore'"); */

			$query 		= $this->db->query("select i_area,e_area_name from tr_area where i_area = '$istore'");
			$st 		= $query->row();
			$namaarea 	= $st->e_area_name;
			$iarea 		= $st->i_area;

			$this->db->select("	d_spmb from tm_spmb where i_spmb = '$ispmb'", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $txt) {
					$dspmb = $txt->d_spmb;
				}
				if ($dspmb != '') {
					$tmp = explode("-", $dspmb);
					$th = $tmp[0];
					$bl = $tmp[1];
					$hr = $tmp[2];
					$dspmb = $hr . " " . mbulan($bl) . " " . $th;
				}
			}

			/* 			$this->db->select("	a.*, b.*, c.e_product_motifname FROM tm_spmb a, tm_spmb_item b, tr_product_motif c
								WHERE a.i_spmb = '$ispmb' and a.i_spmb=b.i_spmb and b.i_product=c.i_product
								and b.i_product_motif=c.i_product_motif
								order by b.n_item_no ", false); */

			/* QUERY UTAMA */
			$fpaw = 'FP-' . $peraw;
			$fpak = 'FP-' . $perak;

			if ($istore != "PB") {
				$query =	$this->db->query("	WITH jual AS (						              
													SELECT trunc(sum(a.n_deliver*a.v_unit_price)/3) as vrata, trunc(sum(a.n_deliver)/3) as nrata, 
													a.i_product 
													FROM tm_nota_item a, tm_nota b
													WHERE b.i_nota > '$fpaw' AND b.i_nota < '$fpak'
													AND b.i_area in (select i_area FROM tr_area WHERE i_area_parent='$istore')
													AND a.i_sj=b.i_sj AND a.i_area=b.i_area AND b.f_nota_cancel='f'
													GROUP BY a.i_product
												)
												SELECT
													a.*,
													b.*,
													c.e_product_motifname,
													/*COALESCE (cbg.n_mutasi_git,0) AS n_mutasi_git,*/
													/* COALESCE (si.n_quantity_deliver,0) AS n_mutasi_git, */
													COALESCE (det.git,0) AS n_mutasi_git,
													COALESCE (cbg.n_mutasi_penjualan,0) AS n_mutasi_penjualan,
													COALESCE (cbg.n_saldo_akhir,0) - COALESCE (cbg.n_mutasi_git,0) - COALESCE (cbg.n_git_penjualan,0) AS n_saldo_cabang,
													COALESCE (pst.n_saldo_akhir,0) - COALESCE (pst.n_mutasi_git,0) - COALESCE (pst.n_git_penjualan,0) AS n_saldo_pusat,
													COALESCE (jual.nrata,0) AS nrata,
													COALESCE (jual.vrata,0) AS vrata
												FROM
													tm_spmb a 
													INNER JOIN tm_spmb_item b ON a.i_spmb = b.i_spmb AND a.i_area = b.i_area
													LEFT JOIN f_mutasi_stock_daerah_saldoakhir2('$iperiode', '$istore', '$istoreloc') cbg ON b.i_product = cbg.i_product AND b.i_product_grade = cbg.i_product_grade 
													AND b.i_product_motif = cbg.i_product_motif AND cbg.i_store = a.i_area
													LEFT JOIN f_mutasi_stock_pusat_saldoakhir4('$iperiode') pst ON cbg.i_product = pst.i_product AND cbg.i_product_grade = pst.i_product_grade
													LEFT JOIN jual ON jual.i_product = b.i_product
													/* LEFT JOIN tm_sjp s ON a.i_spmb = s.i_spmb AND s.i_area = a.i_area 
													LEFT JOIN tm_sjp_item si ON s.i_sjp = si.i_sjp AND s.i_area = si.i_area AND si.i_product = b.i_product */
													LEFT JOIN(	SELECT 
																	b.i_area AS area1, a.product, a.i_product_grade, sum(git) AS git 
																FROM
																	vmutasidetailv2  a
																	INNER JOIN tm_sjp b ON a.ireff = b.i_sjp  
																WHERE
																	a.periode = '$iperiode'
																	AND a.area = 'AA'
																	AND a.i_product_grade = 'A'
																	AND a.loc = '01' GROUP BY 1,2,3
													) det ON (det.product = b.i_product AND det.i_product_grade = b.i_product_grade AND a.i_area = det.area1),
													tr_product_motif c 
												WHERE
													a.i_spmb = '$ispmb'
													AND b.i_product = c.i_product
													AND b.i_product_motif = c.i_product_motif
												ORDER BY
													b.n_item_no ", false);
			} else {
				$query =	$this->db->query("	WITH jual AS (						              
													SELECT trunc(sum(a.n_deliver*a.v_unit_price)/3) as vrata, trunc(sum(a.n_deliver)/3) as nrata, 
													a.i_product 
													FROM tm_nota_item a, tm_nota b
													WHERE b.i_nota > '$fpaw' AND b.i_nota < '$fpak'
													AND b.i_area in (select i_area FROM tr_area WHERE i_area_parent='$istore')
													AND a.i_sj=b.i_sj AND a.i_area=b.i_area AND b.f_nota_cancel='f'
													GROUP BY a.i_product
												)
												SELECT
													a.*,
													b.*,
													c.e_product_motifname,
													0 AS n_mutasi_git,
													0 AS n_mutasi_penjualan,
													COALESCE (cbg.n_saldo_akhir,0) AS n_saldo_cabang,
													COALESCE (pst.n_saldo_akhir,0) - COALESCE (pst.n_mutasi_git,0) - COALESCE (pst.n_git_penjualan,0) AS n_saldo_pusat,
													COALESCE (jual.nrata,0) AS nrata,
													COALESCE (jual.vrata,0) AS vrata
												FROM
													tm_spmb a 
													INNER JOIN tm_spmb_item b ON a.i_spmb = b.i_spmb AND a.i_area = b.i_area
													LEFT JOIN f_mutasi_stock_mo_pb_saldoakhir('$iperiode') cbg ON b.i_product = cbg.i_product AND b.i_product_grade = cbg.i_product_grade 
													AND b.i_product_motif = cbg.i_product_motif AND cbg.i_store = a.i_area
													LEFT JOIN f_mutasi_stock_pusat_saldoakhir4('$iperiode') pst ON cbg.i_product = pst.i_product AND cbg.i_product_grade = pst.i_product_grade
													LEFT JOIN jual ON jual.i_product = b.i_product
													LEFT JOIN tm_sjp s ON a.i_spmb = s.i_spmb AND s.i_area = a.i_area 
													LEFT JOIN tm_sjp_item si ON s.i_sjp = si.i_sjp AND s.i_area = si.i_area AND si.i_product = b.i_product
													INNER JOIN(SELECT substring(a.ireff2,19,2) AS area1, 
																		a.product, a.i_product_grade, sum(git) AS git 
													FROM
														vmutasidetailv2 a
																WHERE
																	a.periode = '$iperiode'
																	AND a.area = 'AA'
																	/* AND a.product = 'OM51011' */
																	AND a.i_product_grade = 'A'
																	AND a.loc = '01' GROUP BY 1,2,3) det ON (det.product = b.i_product 
																	AND det.i_product_grade = b.i_product_grade AND a.i_area = det.area1),
													tr_product_motif c
												WHERE
													a.i_spmb = '$ispmb'
													AND b.i_product = c.i_product
													AND b.i_product_motif = c.i_product_motif
												ORDER BY
													b.n_item_no ", false);
			}

			// $query = $this->db->get();
			/* *********************************************************************** */

			/* BACA NILAI SISA STOCK */
			// $nsisastock = 0;
			#####
			/* $wew = $this->db->query("	SELECT a.n_quantity_stock as jml, a.i_product, b.v_product_retail as harga
										FROM tm_ic a
										left join tr_product_price b on (a.i_product=b.i_product and i_price_group='00')
										WHERE a.i_store='$istore' and a.i_store_location='$istoreloc'");
			foreach ($wew->result() as $wow) {
				#          $zz=$this->db->query(" select * FROM f_stock_onhand('$istore','00','$wow->i_product')");
				#          foreach($zz->result() as $ww){
				#            $nsisastock=$nsisastock+($ww->qty*$wow->harga);            
				#          }
				$nsisastock = $nsisastock + ($wow->jml * $wow->harga);
			} */
			#####

			/** =====| Nilai Total Rp Sisa Stok Cabang |===== */
			$vt_rp_sisastok = $this->db->query(" 	SELECT
														sum((n_saldo_akhir - n_mutasi_git - n_git_penjualan) * a.v_product_retail) AS rpsaldoakhir
													FROM
														f_mutasi_stock_daerah_saldoakhir2('$iperiode','$istore','$istoreloc') a
														LEFT JOIN tr_product b ON a.i_product = b.i_product
														LEFT JOIN tr_product_price ppr ON a.i_product = ppr.i_product AND ppr.i_price_group = '00'
														LEFT JOIN tr_product_sales_category ctg ON (b.i_sales_category = ctg.i_sales_category)
													ORDER BY 1 ")->row()->rpsaldoakhir;
			/* *********************************************************************** */

			/** =====| Nilai Total Rp Rata2 Jual Cabang 3 bulan |===== */
			$vt_rp_rata2 = $this->db->query(" 	SELECT trunc(sum(a.n_deliver*a.v_unit_price)/3) as vrata
												from tm_nota_item a, tm_nota b
												where b.i_nota>'$fpaw' and b.i_nota<'$fpak' 
												and b.i_area in (select i_area from tr_area where i_area_parent='$istore')
												and a.i_sj=b.i_sj and a.i_area=b.i_area and b.f_nota_cancel='f' ")->row()->vrata;
			/* *********************************************************************** */

			// /******************************Qty Git Cabang (Pcs)  */
			// $git_cabang = $this->db->query("	SELECT a.out as out FROM vmutasidetailv2 a 
			// 									LEFT JOIN tr_product_price b ON a.product = b.i_product 
			// 									AND a.i_product_grade = b.i_product_grade  WHERE a.periode ='$iperiode' 
			// 									AND a.area = 'AA' AND a.loc = '$istoreloc' ")->row()->out;

			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("SPMB")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);

			$i 			= 11;
			$j 			= 11;
			$x 			= 0;
			$totorder 	= 0;
			$totstock 	= 0;
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'FORMULIR PERMINTAAN BARANG UNTUK STOCK CABANG');
				$objPHPExcel->getActiveSheet()->mergeCells('A1:Q2');
				$objPHPExcel->getActiveSheet()->mergeCells('A4:B4');

				/** =====| Pengaturan Judul |===== */
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'autosize' => true,
							'size'  => 14
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A1'
				);

				/** =====| Size Width By Column |===== */
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(7);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(7);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(9);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(6);
				/** =====| SpmB |===== */
				$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No SPmB');
				$objPHPExcel->getActiveSheet()->setCellValue('C4', ": $ispmb");
				$objPHPExcel->getActiveSheet()->mergeCells('A4:B4');
				/** =====| Area |===== */
				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Area');
				$objPHPExcel->getActiveSheet()->setCellValue('C5', ": $namaarea");
				$objPHPExcel->getActiveSheet()->mergeCells('A5:B5');
				/** =====| Date SpmB |===== */
				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Tanggal');
				$objPHPExcel->getActiveSheet()->setCellValue('C6', ": $dspmb");
				$objPHPExcel->getActiveSheet()->mergeCells('A6:B6');
				/** =====| Header 1 |===== */
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:C7'
				);

				/** =====| Merge Per Kolom |===== */
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A7:Q9'
				);

				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 8, 0, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('A8', 'No.');
				$objPHPExcel->getActiveSheet()->getStyle('A8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(1, 8, 1, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('B8', 'Kode Produk');
				$objPHPExcel->getActiveSheet()->getStyle('B8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(2, 8, 2, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('C8', 'Nama Barang');
				$objPHPExcel->getActiveSheet()->getStyle('C8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, 8, 3, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('D8', 'Qty Order (pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('D8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4, 8, 4, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('E8', 'Qty Stok Cabang (pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('E8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, 8, 5, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('F8', 'Qty GIT Cbg (pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('F8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6, 8, 6, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('G8', 'Qty Jual (pcs) *');
				$objPHPExcel->getActiveSheet()->getStyle('G8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7, 8, 7, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('H8', 'Rata2 Jual (pcs) **');
				$objPHPExcel->getActiveSheet()->getStyle('H8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, 8, 8, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('I8', 'Qty Stok Pusat (pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('I8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(9, 8, 9, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('J8', 'FC Bulan (pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('J8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(10, 8, 10, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('K8', 'FC Cabang (pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('K8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(11, 8, 11, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('L8', 'OP FC (pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('L8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(12, 8, 12, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('M8', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('M8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(13, 8, 13, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('N8', 'Total Nilai Order (Rupiah)');
				$objPHPExcel->getActiveSheet()->getStyle('N8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(14, 8, 14, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('O8', 'Total Nilai Sisa Stok (Rupiah)');
				$objPHPExcel->getActiveSheet()->getStyle('O8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(15, 8, 15, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('P8', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('P8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(16, 8, 16, 10);
				$objPHPExcel->getActiveSheet()->setCellValue('Q8', 'ACC');
				$objPHPExcel->getActiveSheet()->getStyle('Q8')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$i 			= 11;
				$j 			= 11;
				$xxx 		= 11;
				$xxxx 		= 0;
				$xarea 		= '';
				$saldo 		= 0;
				$no 		= 0;
				// $nsisastock = 0;
				#####
				/* $wew = $this->db->query("select a.n_quantity_stock as jml, a.i_product, b.v_product_retail as harga
										from tm_ic a
										left join tr_product_price b on (a.i_product=b.i_product and i_price_group='00')
										where a.i_store='$istore' and a.i_store_location='$istoreloc'");
				foreach ($wew->result() as $wow) {
					#          $zz=$this->db->query(" select * from f_stock_onhand('$istore','00','$wow->i_product')");
					#          foreach($zz->result() as $ww){
					#            $nsisastock=$nsisastock+($ww->qty*$wow->harga);            
					#          }
					$nsisastock = $nsisastock + ($wow->jml * $wow->harga);
				} */
				#####
				$totorder 		= 0;
				$totstock 		= 0;
				$subtotrata2 	= 0;
				foreach ($query->result() as $row) {
					$no++;

					########
					/* $fpaw = 'FP-' . $peraw;
					$fpak = 'FP-' . $perak;
					$query = $this->db->query(" select trunc(sum(a.n_deliver*a.v_unit_price)/3) as vrata, trunc(sum(a.n_deliver)/3) as nrata, 
												a.i_product, a.i_product_grade
												from tm_nota_item a, tm_nota b
												where b.i_nota>'$fpaw' and b.i_nota<'$fpak' 
												and a.i_product='$row->i_product' and a.i_product_motif='$row->i_product_motif' 
												and b.i_area in (select i_area from tr_area where i_area_parent='$row->i_area')
												and a.i_sj=b.i_sj and a.i_area=b.i_area and b.f_nota_cancel='f'
												group by a.i_product,a.i_product_grade ");
					#='$row->i_area'
					if ($query->num_rows() > 0) {
						$vrata = 0;
						foreach ($query->result() as $raw) {
							$vrata = $raw->vrata;
							$nrata = $raw->nrata;
						}
					} else {
						$vrata = 0;
						$nrata = 0;
					} */

					/* $perkinibk = $perkini . 'BK';
					$query = $this->db->query(" select sum(a.n_deliver) as sumjual
                                      from tm_nota_item a, tm_nota b
                                      where b.i_sj like '%-$perkini%' and not b.i_sj like '%-$perkinibk%' and a.i_product='$row->i_product'
                                      and a.i_sj=b.i_sj and a.i_area=b.i_area and b.f_nota_cancel='f'
                                      and b.i_area in (select i_area as i_area from tr_area where i_store='$row->i_area')
                                      group by a.i_product ");
					#                                     where b.i_sj like '%-$perkini%' and a.i_product='$row->i_product'
					#and i_area='$row->i_area'
					if ($query->num_rows() > 0) {
						foreach ($query->result() as $raw) {
							$nkini = $raw->sumjual;
						}
					} else {
						$nkini = 0;
					} */

					/*$query = $this->db->query("select i_store_location
					from tm_ic 
                                     where i_store='$istore'");
									 $st=$query->row();
          			$storeloc=$st->i_store_location;*/
					if ($row->f_spmb_consigment == 'f') {
						if ($istore == 'AA') {
							$storeloc = '01';
						} else {
							$storeloc = '00';
						}
					} else {
						$storeloc = 'PB';
					}

					/** =====| Nilai Sisa Stok Cabang |===== */
					/* $rpsaldoakhir = $this->db->query(" 	SELECT
															-- ctg.i_sales_category,
															-- CASE WHEN ctg.e_sales_categoryname ISNULL THEN '-'
															-- ELSE ctg.e_sales_categoryname END AS e_sales_categoryname,
															-- sum(n_saldo_akhir) AS saldoakhir,
															-- sum(n_saldo_stockopname + n_mutasi_git + n_git_penjualan) AS saldostockopname,
															-- sum((n_saldo_stockopname + n_mutasi_git + n_git_penjualan) * a.v_product_retail) AS rpstockopname,
															-- sum((n_saldo_stockopname + n_mutasi_git + n_git_penjualan) - n_saldo_akhir) AS selisih,
															-- sum(((n_saldo_stockopname + n_mutasi_git + n_git_penjualan) - n_saldo_akhir) * a.v_product_retail) AS rpselisih
															sum(n_saldo_akhir * a.v_product_retail) AS rpsaldoakhir,n_mutasi_git
														FROM
															f_mutasi_stock_daerah_saldoakhir2('$iperiode','$istore','$istoreloc') a
															LEFT JOIN tr_product b ON a.i_product = b.i_product
															LEFT JOIN tr_product_price ppr ON a.i_product = ppr.i_product AND ppr.i_price_group = '00'
															LEFT JOIN tr_product_sales_category ctg ON (b.i_sales_category = ctg.i_sales_category)
														-- GROUP BY ctg.i_sales_category, ctg.e_sales_categoryname
														GROUP BY n_mutasi_git
														ORDER BY 1 "); */
					/* *********************************************************************** */

					/* if ($rpsaldoakhir->num_rows() > 0) {
						$saldoakhir2 = $rpsaldoakhir->row();
						$gitasal 	 = $saldoakhir2->n_mutasi_git;
					} else {
						$gitasal = 0;
					} */

					/** =====| Data Qty Stok Cabang (Pcs) Ngambilnya dari IC (Qty Stok) |===== */
					// $query = $this->db->query(" select n_quantity_stock from tm_ic where i_store='$istore' and i_product='$row->i_product' 
					// 							and i_product_motif='00' and i_store_location='$storeloc'");
					// if ($query->num_rows() > 0) {
					// 	$ic = $query->row();
					// 	$jmlstock = $ic->n_quantity_stock;
					// } else {
					// 	$jmlstock = 0;
					// }


					/** =====| Data Qty Stok Pusat (Pcs) Ngambilnya dari Mutasi Stok (Saldo Akhir) |===== */
					/* $queryy = $this->db->query("SELECT i_store, i_store_location, 
												i_store_locationbin,n_saldo_akhir,i_product, 
												i_product_grade, i_product_motif, n_mutasi_git ,n_git_penjualan
												FROM f_mutasi_stock_pusat_saldoakhir('$iperiode') 
												WHERE i_product ='$row->i_product' AND i_product_grade ='A' "); */ /* $row->i_product_grade */
					/* if ($queryy->num_rows() > 0) {
						$ic = $queryy->row();
						$jmlstockk 		= $ic->n_saldo_akhir - $ic->n_mutasi_git - $ic->n_git_penjualan;
						$gitcabang 		= $ic->n_mutasi_git;
						$gitpenjualan 	= $ic->n_git_penjualan;
					} else {
						$jmlstockk 		= 0;
						$gitcabang 		= 0;
						$gitpenjualan 	= 0;
					} */

					/*
          $query=$this->db->query(" select * from f_stock_onhand('$istore','$storeloc','$row->i_product')");
          if($query->num_rows()>0){
            $ic=$query->row();
            $jmlstock=$ic->qty;
          }else{
            $jmlstock=0;
          }
		*/
					/*
          $query = $this->db->query(" select n_quantity_stock from tm_ic where i_store='$istore' and i_product='$row->i_product' 
                                      and i_product_motif='00' and i_store_location='$storeloc'");
          if($query->num_rows()>0){
            $ic=$query->row();
            $jmlstock=$ic->n_quantity_stock;
          }else{
            $jmlstock=0;
          }
		*/
					/*
          $query = $this->db->query(" select a.n_mutasi_git, b.i_spmb, b.i_product
                                      from tm_mutasi a, tm_spmb_item b
                                      where a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                                      and a.i_store='$istore' and a.i_store_location='$storeloc'
                                      and substr(a.e_mutasi_periode,3,4)=substr(b.i_spmb,6,4)
                                      and b.i_spmb='$ispmb' and b.i_product='$row->i_product'");
          if($query->num_rows()>0){
            $ig=$query->row();
            $git=$ig->n_mutasi_git;
          }else{
            $git=0;
          }
		*/
					/* $query = $this->db->query(" select sum(b.n_quantity_deliver) as git
												from tm_sjp a, tm_sjp_item b
												where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.f_sjp_cancel='f'
												and b.i_store='$istore' and b.i_store_location='$storeloc'
												and a.d_sjp_receive isnull and b.i_product='$row->i_product'");
					if ($query->num_rows() > 0) {
						$ig = $query->row();
						$git = $ig->git;
					} else {
						$git = 0;
					}
					if ($git == '') $git = 0; */

					/** =====| Atur Ukuran Font Teks |===== */
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':Q' . $i
					);

					/** =====| Isi Datanya |===== */
					$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $no);
					$objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
							'alignment' => array(
								'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
								'vertical'  => Style_Alignment::VERTICAL_CENTER,
								'wrap'      => false
							)
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->i_product);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->n_order);
					$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $row->n_saldo_cabang);
					$objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $row->n_mutasi_git);
					$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row->n_mutasi_penjualan);
					$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->nrata);
					$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->n_saldo_pusat);
					$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, '');
					$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, '');
					$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, '');
					$objPHPExcel->getActiveSheet()->getStyle('L' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $row->v_unit_price);
					$objPHPExcel->getActiveSheet()->getStyle('M' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
							'alignment' => array(
								'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
								'vertical'  => Style_Alignment::VERTICAL_CENTER,
								'wrap'      => true
							)
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $row->v_unit_price * $row->n_order);
					$objPHPExcel->getActiveSheet()->getStyle('N' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->getStyle('N' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
							'alignment' => array(
								'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
								'vertical'  => Style_Alignment::VERTICAL_CENTER,
								'wrap'      => true
							)
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $row->v_unit_price * $row->n_saldo_cabang);
					$objPHPExcel->getActiveSheet()->getStyle('O' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->getStyle('O' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
							'alignment' => array(
								'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
								'vertical'  => Style_Alignment::VERTICAL_CENTER,
								'wrap'      => true
							)
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('P' . $i, $row->e_remark);
					$objPHPExcel->getActiveSheet()->getStyle('P' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('Q' . $i, $row->n_acc);
					$objPHPExcel->getActiveSheet()->getStyle('Q' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN), 'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					//END ISI DATA
					/**
					 *Ket:
					 *1. gitcabang 		= GIT PERJALANAN KE CABANG
					 *2. gitpenjualan 	= GIT PERJALANAN KE TOKO
					 */
					// $subtotrata2 += $row->v_unit_price * $nrata;

					$totorder 	= $totorder + ($row->v_unit_price * $row->n_order);
					$totstock 	= $totstock + ($row->v_unit_price * $row->n_saldo_cabang);
					$totprice 	= $row->v_unit_price + $row->v_unit_price;
					// $nsisastock += ($jmlstockk - $gitcabang - $gitpenjualan);

					$i++;
					$j++;
				}
				// $x = $i - 1;
			}
			// $i = $i + 2;
			$ii = $i + 2;
			$xxxx = $i - 1;
			/**Keterangan */
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						// 'bold'  => false,
						'italic' => false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_LEFT, 'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A' . $i . ':C' . $ii
			);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				),
				'A' . $i . ':Q' . $i
			);
			/** =====| Kolom Total  |===== */
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, 'TOTAL');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(2, $i, 11, $i);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER, 'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'C' . $i . ':O' . $ii
			);


			/** =====| Untuk Menampilan Data Total Order Dan Total Stok |===== */
			$objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $totorder);
			$objPHPExcel->getActiveSheet()->getStyle('N' . ($i) . ':N' . ($i))->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_LEFT, 'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					),
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10,
					)
				)
			);

			/** =====| Untuk Menampilan Data Total Harga |===== */
			/*$objPHPExcel->getActiveSheet()->setCellValue('M' . ($i), "=SUM(M$xxx:M$xxxx)");
			$objPHPExcel->getActiveSheet()->getStyle('M' . ($i))->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$objPHPExcel->getActiveSheet()->getStyle('M' . ($i). ':M' . ($i))->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					),
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10,
					)
				)
			);*/

			$objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $totstock);
			$objPHPExcel->getActiveSheet()->getStyle('O' . ($i) . ':O' . ($i))->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					),
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10,
					)
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('N' . $i . ':O' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			// $objPHPExcel->getActiveSheet()->getStyle('D7:O' . $x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);

			/*Untuk Menentukan Jarak 2 Kolom Antara Total Dan Nilai Flafond*/
			$i = $i + 2;
			$j = $j + 2;
			$ii = $i + 2;
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'D' . $i . ':Q' . $ii
			);

			/**Inventory Control G1 */
			$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, 'Inventory Control');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6, $i, 7, $i);
			$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
					'font' => array(
						'bold'  => true,
						'italic' => true,
						'autosize' => true
					),
				)
			);

			/*merge Inventory Control G2*/
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6, $i, 7, $i + 1);
			$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
				array(
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
					'font' => array(
						'bold'  => true,
						'italic' => true,
						'autosize' => true
					),
				)
			);

			/*Kolom Supervisor Warehouse I1 */
			$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, 'Supervisor Warehouse');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, $i, 9, $i);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				)
			);

			/*merge Supervisor Warehouse I2*/
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, $i, 9, $i + 1);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
					'font' => array(
						'bold'  => true,
						'italic' => true,
						'autosize' => true
					),
				)
			);

			/*Kolom SDH/RSM K1 */
			$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, 'SDH/RSM');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(10, $i, 11, $i + 1);
			$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				)
			);

			/*Kolom M1 */
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(12, $i, 12, $i + 1);
			$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, 'FM');
			$objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER, 'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				)
			);

			$j++;
			$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, 'Keterangan :');
			$objPHPExcel->getActiveSheet()->getStyle('B' . $j)->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_LEFT, 'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				)
			);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $j, '* Awal bulan sampai tanggal penarikan data');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(2, $j, 2, $j);
			$objPHPExcel->getActiveSheet()->getStyle('C' . $j)->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_LEFT, 'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					),
					'font' => array(
						'color' => array('rgb' => 'FF0000'),
						'bold' 	=> false,
					),
				)
			);
			// $i++;
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, '** 3 bulan');
			$objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_LEFT, 'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					),
					'font' => array(
						'color' => array('rgb' => 'FF0000'),
						'bold' => false,
					),
				)
			);
			$i++;
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, 'NILAI SISA STOK :');
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial', 'bold'  => true,
						'italic' => false, 'size'  => 10
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_LEFT, 'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)

				),

				'A' . $i . ':A' . $ii
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, $i, 1, $i);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $vt_rp_sisastok);
			$objPHPExcel->getActiveSheet()->getStyle('C' . $i . ':C' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$objPHPExcel->getActiveSheet()->getStyle('C' . $i . ':C' . $i)->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_RIGHT, 'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					),
					'borders' => array(
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
					'font' => array(
						'name'	=> 'Arial', 'bold'  => true, 'italic' => false, 'size'  => 10,
					)
				)
			);
			$objPHPExcel->getActiveSheet()->setCellValue('A' . ($i + 2), 'RATA2 JUAL :');
			$objPHPExcel->getActiveSheet()->getStyle('A' . ($i + 2) . ':A' . ($i + 2))->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					),
					'font' => array(
						'name'	=> 'Arial', 'bold'  => true,
						'italic' => false, 'size'  => 10,
					)
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, ($i + 2), 1, ($i + 2));
			$objPHPExcel->getActiveSheet()->setCellValue('C' . ($i + 2), $vt_rp_rata2);
			$objPHPExcel->getActiveSheet()->getStyle('C' . ($i + 2))->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$objPHPExcel->getActiveSheet()->getStyle('C' . ($i + 2) . ':C' . ($i + 2))->applyFromArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_RIGHT, 'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					),
					'borders' => array(
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
					'font' => array(
						'name'	=> 'Arial', 'bold'  => true,
						'italic' => false, 'size'  => 10,
					)
				)
			);

			/**G3 */
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6, $i, 7, $i);
			$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
				array(
					'borders' => array(
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			/**I3 */
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, $i, 9, $i);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN) // BORDER BAWAH WAREHOUSE
					),
				)
			);
			/**K3 */
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(10, $i, 11, $i);
			$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN) // BORDER BAWAH WAREHOUSE
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' => array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN),
						'top' => array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN) // BORDER BAWAH WAREHOUSE
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6, $i, 7, $i);
			$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, $i, 9, $i);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(10, $i, 11, $i);
			$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			// garis kanan kiri kolom pertama
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(12, $i, 12, $i);
			$objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			/**I5 */
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6, $i, 7, $i);
			$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(10, $i, 11, $i);
			$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);


			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, $i, 9, $i);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			//kolom ke 2 right
			$objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6, $i, 7, $i);
			$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, $i, 9, $i);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(10, $i, 11, $i);
			$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6, $i, 7, $i);
			$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, $i, 9, $i);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			// garis kanan kiri kolom ke 4
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(10, $i, 11, $i);
			$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN), 'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

			$nama = $ispmb . '.xls';
			$istore = $istore == "AA" ? "00" : $istore;

			#$objWriter->save("excel/".$iarea.'/'.$nama); 
			if (file_exists("excel/" . $istore . "/" . $nama)) {
				@chmod("excel/" . $istore . "/" . $nama, 0777);
				@unlink("excel/" . $istore . "/" . $nama);
			}
			$objWriter->save("excel/" . $istore . "/" . $nama);
			@chmod("excel/" . $istore . "/" . $nama, 0777);

			$this->logger->writenew('Export SPMB No' . $ispmb . ' Area:' . $iarea);

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$data['folder']	= "excel/" . $istore;

			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
