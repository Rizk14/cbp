<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('spmb');
			$data['iperiode']	= '';
			$data['istore']	  = '';
			$data['ispmb']	  = '';
			$this->load->view('exp-spmb/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-spmb/cform/area/index/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('exp-spmb/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-spmb/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('exp-spmb/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function spmb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$istore = $this->input->post("istore");
			#$area=$this->input->post("area");
			$periode = $this->input->post("periode");
			$cari = $this->input->post("cari");
			if ($istore == '') $istore = $this->uri->segment(4);
			#if($area=='')$area=$this->uri->segment(4);
			if ($periode == '') $periode = $this->uri->segment(5);
			$config['base_url'] = base_url() . 'index.php/exp-spmb/cform/spmb/' . $istore . '/' . $periode . '/';
			#$config['base_url'] = base_url().'index.php/exp-spmb/cform/spmb/'.$area.'/'.$periode.'/';
			$query = $this->db->query("select a.i_spmb from tm_spmb a, tr_area b
                                 where a.i_area=b.i_area and b.i_store = '$istore' and substring(a.i_spmb,6,4)='$periode'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('exp-spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_spmb');
			$data['isi'] = $this->mmaster->bacaspmb($config['per_page'], $this->uri->segment(6), $istore, $periode, $cari);
			#$data['isi']=$this->mmaster->bacaspmb($config['per_page'],$this->uri->segment(6),$area,$periode,$cari);
			$data['istore'] = $istore;
			#$data['area']=$area;
			$data['periode'] = $periode;
			$data['cari'] = $cari;
			$this->load->view('exp-spmb/vlistspmb', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-spmb/cform/store/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store");
			} else {
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store
                                  and (c.i_area = '$area1' or c.i_area = '$area2' or
                                  c.i_area = '$area3' or c.i_area = '$area4' or
                                  c.i_area = '$area5')");
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi'] = $this->mmaster->bacastore($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('exp-spmb/vliststore', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-spmb/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
				$query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')", false);
			} else {
				$query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')
                                    and (c.i_area = '$area1' or c.i_area = '$area2' or
                                    c.i_area = '$area3' or c.i_area = '$area4' or
                                    c.i_area = '$area5')", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi'] = $this->mmaster->caristore($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('exp-spmb/vliststore', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu233') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-spmb/mmaster');
			$istore    = $this->input->post('istore');
			$istoreloc = $this->input->post('istorelocation');
			#$iarea    = $this->input->post('iarea');
			$iperiode = $this->input->post('iperiode');
			$ispmb    = $this->input->post('ispmb');
			$a = substr($iperiode, 0, 4);
			$b = substr($iperiode, 4, 2);
			$peri = mbulan($b) . " - " . $a;

			if ($istore == 'AA') {
				$istoreloc = '01';
			}
			#####      
			$thak = substr($a, 2, 2);
			$blak = $b;
			$blaw = intval($b);
			$thaw = intval($a);
			for ($z = 1; $z <= 3; $z++) {
				$blaw = $blaw - 1;
				if ($blaw == 0) {
					$blaw = 12;
					$thaw = $thaw - 1;
				}
			}
			$thaw = strval($thaw);
			$thaw = substr($thaw, 2, 2);
			$blaw = substr($blaw, 0, 2);
			if (strlen($blaw) == 1) {
				$blaw = '0' . $blaw;
			}
			$peraw = $thaw . $blaw;
			$perak = $thak . $blak;
			#      $perkini=substr($iperiode,2,4).'-'.$istore;
			$perkini = substr($iperiode, 2, 4) . '-';
			#####
			/* $query = $this->db->query("select i_area,e_area_name from tr_area where i_store='$istore'"); */
			$query = $this->db->query("select i_area,e_area_name from tr_area where i_area='$istore'");
			$st = $query->row();
			$namaarea = $st->e_area_name;
			$iarea = $st->i_area;
			$this->db->select("	d_spmb from tm_spmb where i_spmb = '$ispmb'", false);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $txt) {
					$dspmb = $txt->d_spmb;
				}
				if ($dspmb != '') {
					$tmp = explode("-", $dspmb);
					$th = $tmp[0];
					$bl = $tmp[1];
					$hr = $tmp[2];
					$dspmb = $hr . " " . mbulan($bl) . " " . $th;
				}
			}

			$this->db->select("	a.*, b.*, c.e_product_motifname from tm_spmb a, tm_spmb_item b, tr_product_motif c
						              where a.i_spmb = '$ispmb' and a.i_spmb=b.i_spmb and b.i_product=c.i_product
                          and b.i_product_motif=c.i_product_motif
						              order by b.n_item_no ", false);
			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("SPMB ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			$i = 10;
			$x = 0;
			$totorder = 0;
			$totstock = 0;
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'FORMULIR PERMINTAAN BARANG UNTUK STOCK CABANG');
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A5'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(16);

				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Area : ' . $namaarea . '     No : ' . $ispmb);
				$objPHPExcel->getActiveSheet()->setCellValue('A4', 'Tanggal : ' . $dspmb);
				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Stock');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 1, 10, 1);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 10, 3);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 4, 10, 4);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 5, 10, 5);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A7:L9'
				);

				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 7, 0, 9);
				$objPHPExcel->getActiveSheet()->setCellValue('A7', 'No.');
				$objPHPExcel->getActiveSheet()->getStyle('A7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(1, 7, 1, 9);
				$objPHPExcel->getActiveSheet()->setCellValue('B7', 'Kode Produk');
				$objPHPExcel->getActiveSheet()->getStyle('B7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(2, 7, 2, 9);
				$objPHPExcel->getActiveSheet()->setCellValue('C7', 'Nama Barang');
				$objPHPExcel->getActiveSheet()->getStyle('C7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('D7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D8', 'Order');
				$objPHPExcel->getActiveSheet()->getStyle('D8')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D9', '(pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('D9')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E7', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('E7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E8', 'Stock');
				$objPHPExcel->getActiveSheet()->getStyle('E8')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E9', '(pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('E9')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F7', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('F7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F8', 'GIT');
				$objPHPExcel->getActiveSheet()->getStyle('F8')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F9', '(pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('F9')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G7', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('G7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G8', 'Jual');
				$objPHPExcel->getActiveSheet()->getStyle('G8')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G9', '(pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('G9')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H7', 'Rata2');
				$objPHPExcel->getActiveSheet()->getStyle('H7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H8', 'Jual');
				$objPHPExcel->getActiveSheet()->getStyle('H8')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H9', '(pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('H9')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, 7, 8, 9);
				$objPHPExcel->getActiveSheet()->setCellValue('I7', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('I7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J7', 'Total Nilai');
				$objPHPExcel->getActiveSheet()->getStyle('J7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J8', 'Order');
				$objPHPExcel->getActiveSheet()->getStyle('J8')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J9', '(Rupiah)');
				$objPHPExcel->getActiveSheet()->getStyle('J9')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K7', 'Total Nilai');
				$objPHPExcel->getActiveSheet()->getStyle('K7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K8', 'Sisa Stok');
				$objPHPExcel->getActiveSheet()->getStyle('K8')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K9', '(Rupiah)');
				$objPHPExcel->getActiveSheet()->getStyle('K9')->applyFromArray(
					array(
						'borders' => array(
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(11, 7, 11, 9);
				$objPHPExcel->getActiveSheet()->setCellValue('L7', 'Keterangan (motif)');
				$objPHPExcel->getActiveSheet()->getStyle('L7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(12, 7, 12, 9);
				$objPHPExcel->getActiveSheet()->setCellValue('M7', 'Acc');
				$objPHPExcel->getActiveSheet()->getStyle('M7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => style_Border::BORDER_THIN)
						),
					)
				);
				$i = 10;
				$j = 10;
				$xarea = '';
				$saldo = 0;
				$no = 0;
				$nsisastock = 0;
				#####
				$wew = $this->db->query("select a.n_quantity_stock as jml, a.i_product, b.v_product_retail as harga
                                from tm_ic a
                                left join tr_product_price b on (a.i_product=b.i_product and i_price_group='00')
                                where a.i_store='$istore' and a.i_store_location='$istoreloc'");
				foreach ($wew->result() as $wow) {
					#          $zz=$this->db->query(" select * from f_stock_onhand('$istore','00','$wow->i_product')");
					#          foreach($zz->result() as $ww){
					#            $nsisastock=$nsisastock+($ww->qty*$wow->harga);            
					#          }
					$nsisastock = $nsisastock + ($wow->jml * $wow->harga);
				}
				#####
				$totorder = 0;
				$totstock = 0;
				foreach ($query->result() as $row) {
					$no++;
					########
					$fpaw = 'FP-' . $peraw;
					$fpak = 'FP-' . $perak;
					$query = $this->db->query(" select trunc(sum(a.n_deliver*a.v_unit_price)/3) as vrata, trunc(sum(a.n_deliver)/3) as nrata, 
                                      a.i_product 
                                      from tm_nota_item a, tm_nota b
                                      where b.i_nota>'$fpaw' and b.i_nota<'$fpak' and a.i_product='$row->i_product'
                                      and a.i_product_motif='$row->i_product_motif' 
                                      and b.i_area in (select i_area from tr_area where i_area_parent='$row->i_area')
                                      and a.i_sj=b.i_sj and a.i_area=b.i_area and b.f_nota_cancel='f'
                                      group by a.i_product ");
					#='$row->i_area'
					if ($query->num_rows() > 0) {
						foreach ($query->result() as $raw) {
							$vrata = $raw->vrata;
							$nrata = $raw->nrata;
						}
					} else {
						$vrata = 0;
						$nrata = 0;
					}
					$perkinibk = $perkini . 'BK';
					$query = $this->db->query(" select sum(a.n_deliver) as sumjual
                                      from tm_nota_item a, tm_nota b
                                      where b.i_sj like '%-$perkini%' and not b.i_sj like '%-$perkinibk%' and a.i_product='$row->i_product'
                                      and a.i_sj=b.i_sj and a.i_area=b.i_area and b.f_nota_cancel='f'
                                      and b.i_area in (select i_area as i_area from tr_area where i_store='$row->i_area')
                                      group by a.i_product ");
					#                                     where b.i_sj like '%-$perkini%' and a.i_product='$row->i_product'
					#and i_area='$row->i_area'
					if ($query->num_rows() > 0) {
						foreach ($query->result() as $raw) {
							$nkini = $raw->sumjual;
						}
					} else {
						$nkini = 0;
					}

					/*$query = $this->db->query("select i_store_location
                                     from tm_ic 
                                     where i_store='$istore'");
          $st=$query->row();
          $storeloc=$st->i_store_location;*/
					if ($row->f_spmb_consigment == 'f') {
						if ($istore == 'AA') {
							$storeloc = '01';
						} else {
							$storeloc = '00';
						}
					} else {
						$storeloc = 'PB';
					}
					$query = $this->db->query(" select n_quantity_stock from tm_ic where i_store='$istore' and i_product='$row->i_product' 
                                      and i_product_motif='00' and i_store_location='$storeloc'");
					if ($query->num_rows() > 0) {
						$ic = $query->row();
						$jmlstock = $ic->n_quantity_stock;
					} else {
						$jmlstock = 0;
					}

					/*
          $query=$this->db->query(" select * from f_stock_onhand('$istore','$storeloc','$row->i_product')");
          if($query->num_rows()>0){
            $ic=$query->row();
            $jmlstock=$ic->qty;
          }else{
            $jmlstock=0;
          }
*/
					/*
          $query = $this->db->query(" select n_quantity_stock from tm_ic where i_store='$istore' and i_product='$row->i_product' 
                                      and i_product_motif='00' and i_store_location='$storeloc'");
          if($query->num_rows()>0){
            $ic=$query->row();
            $jmlstock=$ic->n_quantity_stock;
          }else{
            $jmlstock=0;
          }
*/
					$totorder = $totorder + ($row->v_unit_price * $row->n_order);
					$totstock = $totstock + ($row->v_unit_price * $jmlstock);
					/*
          $query = $this->db->query(" select a.n_mutasi_git, b.i_spmb, b.i_product
                                      from tm_mutasi a, tm_spmb_item b
                                      where a.i_product=b.i_product and a.i_product_motif=b.i_product_motif
                                      and a.i_store='$istore' and a.i_store_location='$storeloc'
                                      and substr(a.e_mutasi_periode,3,4)=substr(b.i_spmb,6,4)
                                      and b.i_spmb='$ispmb' and b.i_product='$row->i_product'");
          if($query->num_rows()>0){
            $ig=$query->row();
            $git=$ig->n_mutasi_git;
          }else{
            $git=0;
          }
*/
					$query = $this->db->query(" select sum(b.n_quantity_deliver) as git
                                      from tm_sjp a, tm_sjp_item b
                                      where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.f_sjp_cancel='f'
                                      and b.i_store='$istore' and b.i_store_location='$storeloc'
                                      and a.d_sjp_receive isnull and b.i_product='$row->i_product'");
					if ($query->num_rows() > 0) {
						$ig = $query->row();
						$git = $ig->git;
					} else {
						$git = 0;
					}
					if ($git == '') $git = 0;
					########
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':K' . $i
					);

					$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $no);
					$objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->i_product);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->n_order);
					$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $jmlstock);
					$objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $git);
					$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $nkini);
					$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $nrata);
					$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->v_unit_price);
					$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->v_unit_price * $row->n_order);
					$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $row->v_unit_price * $jmlstock);
					$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row->e_remark);
					$objPHPExcel->getActiveSheet()->getStyle('L' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $row->n_acc);
					$objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$i++;
					$j++;
				}
				$x = $i - 1;
			}
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, 'TOTAL');
			$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $totorder);
			$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $totstock);
			$objPHPExcel->getActiveSheet()->getStyle('J' . $i . ':K' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER);
			$objPHPExcel->getActiveSheet()->getStyle('D7:K' . $x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A10:A' . $x
			);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'D10:H' . $x
			);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'alignment' => array(
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'C' . $i . ':C' . $i
			);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'alignment' => array(
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'I10:K' . $i
			);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				),
				'A' . $i . ':L' . $i
			);
			$objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$i = $i + 2;
			$ii = $i + 2;
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'D' . $i . ':L' . $ii
			);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A' . $i . ':A' . $ii
			);
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, 'NILAI PLAFON');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, $i, 1, $i);
			$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, 'Admin Gudang');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, 'Spv Adm Gudang');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, 'SDH');
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, 'FADH');
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, 'MD');
			$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$i++;
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, 'NILAI Sisa Stok');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, $i, 1, $i);
			$objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
				array(
					'borders' => array(
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $nsisastock);
			$objPHPExcel->getActiveSheet()->getStyle('C' . $i . ':C' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$i++;
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, 'SISA PLAFON');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, $i, 1, $i);
			$objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
				array(
					'borders' => array(
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);


			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			//}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama = $ispmb . '.xls';
			#			$objWriter->save("excel/".$iarea.'/'.$nama); 
			if (file_exists("excel/" . $istore . "/" . $nama)) {
				@chmod("excel/" . $istore . "/" . $nama, 0777);
				@unlink("excel/" . $istore . "/" . $nama);
			}
			$objWriter->save("excel/" . $istore . "/" . $nama);
			@chmod("excel/" . $istore . "/" . $nama, 0777);

			// $sess = $this->session->userdata('session_id');
			// $id = $this->session->userdata('user_id');
			// $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			// $rs		= pg_query($sql);
			// if (pg_num_rows($rs) > 0) {
			// 	while ($row = pg_fetch_assoc($rs)) {
			// 		$ip_address	  = $row['ip_address'];
			// 		break;
			// 	}
			// } else {
			// 	$ip_address = 'kosong';
			// }
			// $query 	= pg_query("SELECT current_timestamp as c");
			// while ($row = pg_fetch_assoc($query)) {
			// 	$now	  = $row['c'];
			// }
			// $pesan = 'Export SPMB No' . $ispmb . ' Area:' . $iarea;
			// $this->load->model('logger');
			$this->logger->writenew('Export SPMB No' . $ispmb . ' Area:' . $iarea);

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$data['folder']	= "excel/" . $istore;

			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
