<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu260')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/hargabeli/cform/index/';
			$query = $this->db->query("select * from tr_harga_beli",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('hargabeli');
			$data['iproduct']='';
			$this->load->model('hargabeli/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('hargabeli/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu260')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	= $this->input->post('iproduct', TRUE);
			$ipricegroup= $this->input->post('ipricegroup', TRUE);
			$eproductname 	= $this->input->post('eproductname', TRUE);
			$iproductgrade 	= $this->input->post('iproductgrade', TRUE);
			$vproductmill	= $this->input->post('vproductmill', TRUE);
			$vproductmill	= str_replace(",","",$this->input->post('vproductmill', TRUE));
			if($vproductmill=='') $vproductmill=0;
			if ((isset($iproduct) && $iproduct != '') && (isset($eproductname) && $eproductname != '') && (isset($iproductgrade) && $iproductgrade != ''))
			{
        $ipricegroup=strtoupper($ipricegroup);
				$this->load->model('hargabeli/mmaster');
				$query = $this->db->query(" select distinct i_product, i_product_grade, v_product_mill 
                                    from tr_harga_beli where i_product='$iproduct' and i_product_grade='$iproductgrade' 
                                    and i_price_group='ipricegroup'",false);
				if($query->num_rows()>0){ 
					$this->mmaster->update($iproduct,$ipricegroup,$eproductname,$iproductgrade,$vproductmill);
				}else{
					$this->mmaster->insert($iproduct,$ipricegroup,$eproductname,$iproductgrade,$vproductmill);
				}

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
	      	$now	  = $row['c'];
			  }
			  $pesan='Input Harga Beli Kode:'.$iproduct;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );  

			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu260')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('hargabeli');
			$this->load->view('hargabeli/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu260')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('hargabeli')." update";
			if( ($this->uri->segment(4)) && ($this->uri->segment(5)) ){
				$iproduct = $this->uri->segment(4);
				$ipricegroup = $this->uri->segment(5);
				$data['iproduct'] = $iproduct;
				$data['ipricegroup'] = $ipricegroup;
				$this->load->model('hargabeli/mmaster');
				$data['isi']=$this->mmaster->baca($iproduct,$ipricegroup);
		 		$this->load->view('hargabeli/vmainform',$data);
			}else{
				$this->load->view('hargabeli/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu260')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	= $this->input->post('iproduct', TRUE);
			$eproductname 	= $this->input->post('eproductname', TRUE);
			$iproductgrade 	= $this->input->post('iproductgrade', TRUE);
			$ipricegroup	= $this->input->post('ipricegroup', TRUE);
			$vproductmill	= $this->input->post('vproductmill', TRUE);
			$vproductmill	= str_replace(",","",$this->input->post('vproductmill', TRUE));
			if($vproductmill=='')
				$vproductmill=0;			
      $ipricegroup=strtoupper($ipricegroup);
			$this->load->model('hargabeli/mmaster');
			$this->mmaster->update($iproduct,$ipricegroup,$eproductname,$iproductgrade,$vproductmill);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Update Harga Beli Kode Barang:'.$iproduct;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu260')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	= $this->uri->segment(4);
			$ipricegroup= $this->uri->segment(5);
			$this->load->model('hargabeli/mmaster');
			$this->mmaster->delete($iproduct,$ipricegroup);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Hapus Harga Beli Kode Barang:'.$iproduct;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$config['base_url'] = base_url().'index.php/hargabeli/cform/index/';
			$query = $this->db->query("select * from tr_harga_beli",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('hargabeli');
			$data['iproduct']='';
			$data['iproductgrade']='';
			$this->load->model('hargabeli/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('hargabeli/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu260')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/hargabeli/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$cari=($this->input->post("cari",false));
			if($cari=='')$cari=$this->uri->segment(4);
			if($cari=='zxvf'){
				$cari='';
				$config['base_url'] = base_url().'index.php/hargabeli/cform/cari/zxvf/';
			}else{
				$config['base_url'] = base_url().'index.php/hargabeli/cform/cari/'.$cari.'/';
			}

			$query = $this->db->query(" select * from tr_harga_beli 
					                where (upper(tr_harga_beli.i_product) ilike '%$cari%' 
					                or upper(tr_harga_beli.e_product_name) ilike '%$cari%')
                          order by i_product, i_price_group",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('hargabeli/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('hargabeli');
			$data['iproduct']='';
			$data['iproductgrade']='';
	 		$this->load->view('hargabeli/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productgrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu260')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/hargabeli/cform/productgrade/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_product_grade from tr_product_grade 
										where upper(i_product_grade) like '%$cari%' or upper(e_product_gradename) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('hargabeli/mmaster');
			$data['page_title'] = $this->lang->line('list_productgrade');
			$data['isi']=$this->mmaster->bacaproductgrade($config['per_page'],$this->uri->segment(5));
			$this->load->view('hargabeli/vlistproductgrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductgrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu260')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/hargabeli/cform/productgrade/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_product_grade from tr_product_grade 
										where upper(i_product_grade) like '%$cari%' or upper(e_product_gradename) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('hargabeli/mmaster');
			$data['page_title'] = $this->lang->line('list_productgrade');
			$data['isi']=$this->mmaster->cariproductgrade($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('hargabeli/vlistproductgrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu260')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris=$this->uri->segment(4);
			/*$cari =$this->uri->segment(5);*/
			$config['base_url'] = base_url().'index.php/hargabeli/cform/product/'.$baris.'/sikasep/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query("select i_product from tr_product",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('hargabeli/mmaster');
			$data['baris']=$baris;
			$data['cari']='';
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6));
			$this->load->view('hargabeli/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu260')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/hargabeli/cform/product/'.$cari.'/'.'index/';*/

			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/hargabeli/cform/cariproduct/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/hargabeli/cform/cariproduct/'.$baris.'/sikasep/';

			$query = $this->db->query("select i_product from tr_product
						   where upper(i_product) ilike '%$cari%' or upper(e_product_name) ilike '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('hargabeli/mmaster');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('hargabeli/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
