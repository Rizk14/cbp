<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('cttbretur');
         $this->load->model('ttbretur/mmaster');
         $data['ittb']  = '';
         $data['inota'] = '';
         $data['isi']   = '';
      $data['tgl']=date('d-m-Y');
         $this->load->view('ttbretur/vmainform', $data);
      }elseif($this->session->userdata('logged_in')){
         $this->load->view('errorauthority');
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('listspb');
         $this->load->view('ttbretur/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function deletedetail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){

         $ittb       = str_replace('%20',' ',$this->uri->segment(4));
         $iarea         = $this->uri->segment(5);
         $tahun         = $this->uri->segment(6);
         $iproduct      = $this->uri->segment(7);
         $iproductgrade = $this->uri->segment(8);
         $iproductmotif = $this->uri->segment(9);
#        $inota         = $this->uri->segment(10);
         $icustomer        = $this->uri->segment(10);
         $nttbdiscount1 = $this->uri->segment(11);
         $nttbdiscount2 = $this->uri->segment(12);
         $nttbdiscount3 = $this->uri->segment(13);
         $vttbdiscount1 = $this->uri->segment(14);
         $vttbdiscount1 = str_replace(',','',$vttbdiscount1);
         $vttbdiscount2 = $this->uri->segment(15);
         $vttbdiscount2 = str_replace(',','',$vttbdiscount2);
         $vttbdiscount3 = $this->uri->segment(16);
         $vttbdiscount3 = str_replace(',','',$vttbdiscount3);
         $vttbdiscounttotal   = $this->uri->segment(17);
         $vttbdiscounttotal   = str_replace(',','',$vttbdiscounttotal);
         $vttbnetto     = $this->uri->segment(18);
         $vttbnetto     = str_replace(',','',$vttbnetto);
         $vttbgross     = $this->uri->segment(19);
         $vttbgross     = str_replace(',','',$vttbgross);
         $jml        = $this->uri->segment(20);
         $dttb       = $this->uri->segment(21);
         if($dttb!=''){
            $tmp=explode("-",$dttb);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dttb=$th."-".$bl."-".$hr;
            $tahun   = $th;
         }
         $dreceive1     = $this->uri->segment(22);
         if($dreceive1!='' && $dreceive1!='dreceive'){
            $tmp=explode("-",$dreceive1);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dreceive1=$th."-".$bl."-".$hr;
         }else{
        $dreceive1=null;
      }
         $ettbremark    = $this->uri->segment(23);
         if($ettbremark=='')
            $ettbremark=null;
         $ecustomerpkpnpwp    = $this->uri->segment(24);
         if($ecustomerpkpnpwp=='')
            $fttbpkp = 'f';
         else
            $fttbpkp = 't';
         $fttbcancel='f';
         $dfrom   = $this->uri->segment(25);
         $dto     = $this->uri->segment(26);
         $ibbm    = $this->uri->segment(27);
         $this->load->model('ttbretur/mmaster');
         $this->db->trans_begin();
         $this->mmaster->updateheader( $ittb,$iarea,$tahun,$dttb,$dreceive1,$ettbremark,
                                               $nttbdiscount1,$nttbdiscount2,$nttbdiscount3,$vttbdiscount1,
                                               $vttbdiscount2,$vttbdiscount3,$vttbdiscounttotal,$vttbnetto,
                                               $vttbgross,$icustomer,$ibbm);
         $this->mmaster->deletedetail($iarea, $ittb, $iproduct, $iproductgrade, $iproductmotif, $tahun);
         if ($this->db->trans_status()===false)
         {
            $this->db->trans_rollback();
         }else{
          $this->db->trans_commit();
#           $this->db->trans_rollback();
         }

               $sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
                  $now    = $row['c'];
               }
               $pesan='Hapus Detail TTB Retur Area '.$iarea.' No:'.$ittb;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );

         $data['page_title'] = $this->lang->line('ttbretur')." update";
         if(($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')){
            $ittb    = $this->uri->segment(4);
            $iarea      = $this->uri->segment(5);
            $tahun      = $this->uri->segment(6);
            $query      = $this->db->query("select i_area, i_ttb, d_ttb, i_nota, d_nota, i_product1, i_product1_grade, i_product1_motif,
                    i_product2, i_product2_grade, i_product2_motif, n_quantity, n_quantity_receive, v_unit_price,
                    n_ttb_year from tm_ttbretur_item where i_ttb = '$ittb' and i_area = '$iarea' and n_ttb_year=$tahun");
            $data['jmlitem']     = $query->num_rows();
            $data['ittb']        = $ittb;
            $data['iarea']       = $iarea;
            $data['tahun']       = $tahun;
            $data['dfrom']       = $dfrom;
            $data['dto']         = $dto;
            $this->load->model('ttbretur/mmaster');
            $data['isi']=$this->mmaster->bacattb($iarea,$ittb,$tahun);
            $data['detail']=$this->mmaster->bacattbdetail($iarea,$ittb,$tahun);
            $this->load->view('ttbretur/vmainform',$data);
         }else{
            $this->load->view('ttbretur/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cari()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $config['base_url'] = base_url().'index.php/ttbretur/cform/index/';
         $config['per_page'] = '10';
         $limo=$config['per_page'];
         $ofso=$this->uri->segment(4);
         if($ofso=='')
            $ofso=0;
         $query = $this->db->query(" select a.i_spb, a.i_salesman, a.i_customer, a.i_price_group, a.i_nota, a.i_store, a.i_store_location,
            a.d_spb, a.d_nota, a.d_spbentry, a.d_sj, a.d_spb_receive, a.f_spb_op, a.f_spb_pkp, a.f_spb_plusdiscount, a.f_spb_stockdaerah,
            a.f_spb_program, a.f_spb_consigment, a.f_spb_valid, a.f_spb_siapnotagudang, a.f_spb_cancel, a.n_spb_discount1, a.n_spb_discount2,
            a.n_spb_discount3, a.v_spb, a.v_spb_after, a.i_approve1, a.i_approve2, a.d_approve1, a.d_approve2, a.i_area, a.f_siapnotasales,
            a.i_spb_old, a.i_product_group, a.f_spb_opclose, a.f_spb_pemenuhan, a.i_cek, a.d_cek, a.e_remark1, b.i_customer, b.e_customer_name
            from tm_spb a, tr_customer b
                 where a.i_customer=b.i_customer and not a.i_approve1 isnull
                  and not a.i_approve2 isnull and f_spb_siapnota = 't'
                  and f_spb_cancel = 'f' and not i_store isnull and a.i_nota isnull
                  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                      or upper(a.i_spb) like '%$cari%') limit $limo offset $ofso ",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(4);
         $this->pagination->initialize($config);
         $this->load->model('ttbretur/mmaster');
         $data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
         $data['page_title'] = $this->lang->line('cttbretur');
         $data['ispb']='';
         $this->load->view('ttbretur/vmainform',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function approve()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('ttbretur');
         if($this->input->post('inotaedit')){
            $inota = $this->input->post('inotaedit');
            $query = $this->db->query("select * from tm_nota_item where i_nota = '$inota'");
            $data['jmlitem'] = $query->num_rows();
            $data['inota'] = $inota;
            $data['ittb']='';
            $this->load->model('ttbretur/mmaster');
            $data['isi']=$this->mmaster->baca($inota);
            $data['detail']=$this->mmaster->bacadetail($inota);
            $this->load->view('ttbretur/vmainform',$data);
         }else{
            $this->load->view('ttbretur/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function update()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('ttbretur/mmaster');
         $icustomer       = $this->input->post('icustomer', TRUE);
         $ecustomername = $this->input->post('ecustomername', TRUE);
         $iarea             = $this->input->post('iarea', TRUE);
         $eareaname       = $this->input->post('eareaname', TRUE);
         $isalesman       = $this->input->post('isalesman',TRUE);
         $ipricegroup    = $this->input->post('ipricegroup',TRUE);
         $esalesmanname = $this->input->post('esalesmanname',TRUE);
         $nttbdiscount1 = $this->input->post('nttbdiscount1',TRUE);
         $nttbdiscount2 = $this->input->post('nttbdiscount2',TRUE);
         $nttbdiscount3 = $this->input->post('nttbdiscount3',TRUE);
         $vttbdiscount1 = $this->input->post('vttbdiscount1',TRUE);
         $vttbdiscount1 = str_replace(',','',$vttbdiscount1);
         $vttbdiscount2 = $this->input->post('vttbdiscount2',TRUE);
         $vttbdiscount2 = str_replace(',','',$vttbdiscount2);
         $vttbdiscount3 = $this->input->post('vttbdiscount3',TRUE);
         $vttbdiscount3 = str_replace(',','',$vttbdiscount3);
         $vttbdiscounttotal   = $this->input->post('vttbdiscounttotal',TRUE);
         $vttbdiscounttotal   = str_replace(',','',$vttbdiscounttotal);
         $vttbnetto        = $this->input->post('vttbnetto',TRUE);
         $vttbnetto        = str_replace(',','',$vttbnetto);
         $vttbgross        = $this->input->post('vttbgross',TRUE);
         $vttbgross        = str_replace(',','',$vttbgross);
         $fttbplusppn      = $this->input->post('fttbplusppn',TRUE);
         $fttbplusdiscount = $this->input->post('fttbplusdiscount',TRUE);
         $jml             = $this->input->post('jml', TRUE);
         $ialasanretur  = $this->input->post('ialasanretur', TRUE);
         $ittb             = $this->input->post('ittb', TRUE);
         $dttb             = $this->input->post('dttb', TRUE);
         if($dttb!=''){
            $tmp=explode("-",$dttb);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dttb=$th."-".$bl."-".$hr;
            $tahun   = $th;
         }
         $dreceive1  = $this->input->post('dreceive1', TRUE);
         if($dreceive1!=''){
            $tmp=explode("-",$dreceive1);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dreceive1=$th."-".$bl."-".$hr;
         }else{
        $dreceive1=null;
      }
         $ettbremark    = $this->input->post('eremark', TRUE);
         if($ettbremark=='')
            $ettbremark=null;
         $ecustomerpkpnpwp    = $this->input->post('ecustomerpkpnpwp', TRUE);
         if($ecustomerpkpnpwp=='')
            $fttbpkp = 'f';
         else
            $fttbpkp = 't';
         $fttbcancel='f';
         if(($dttb!='') && ($ittb!='') && ($iarea!='') && ($icustomer!='') && ($ialasanretur!='') && ($jml!='') && ($jml!='0')){
            $this->db->trans_begin();
            $dbbm                = $dttb;
            $istore               = 'AA';
            $istorelocation     = '01';
            $istorelocationbin= '00';
            $eremark       = 'TTB Retur';
            $ibbktype         = '01';
            $ibbmtype         = '05';
            $this->mmaster->insertheader( $iarea,$ittb,$dttb,$icustomer,$isalesman,$nttbdiscount1,$nttbdiscount2,
                                    $nttbdiscount3,$vttbdiscount1,$vttbdiscount2,$vttbdiscount3,$fttbpkp,$fttbplusppn,
                                    $fttbplusdiscount,$vttbgross,$vttbdiscounttotal,$vttbnetto,$ettbremark,$fttbcancel,
                                    $dreceive1,$tahun,$ialasanretur,$ipricegroup);
            for($i=1;$i<=$jml;$i++){
              $iproduct             = $this->input->post('iproduct'.$i, TRUE);
              $iproductgrade        = 'A';
              $iproductmotif        = $this->input->post('motif'.$i, TRUE);
              $eproductname            = $this->input->post('eproductname'.$i, TRUE);
              $vunitprice           = $this->input->post('vunitprice'.$i, TRUE);
              $vunitprice           = str_replace(',','',$vunitprice);
              $ndeliver             = $this->input->post('ndeliver'.$i, TRUE);
              $nquantity            = $this->input->post('nquantity'.$i, TRUE);
              $ettbremark           = $this->input->post('eremark'.$i, TRUE);
#             $inota             = $this->input->post('inota'.$i, TRUE);
#             $dnota             = $this->input->post('dnota'.$i, TRUE);
              if($ettbremark=='')
               $ettbremark=null;
              if($nquantity>0){
                 $this->mmaster->insertdetail(  $iarea,$ittb,$dttb,$iproduct,$iproductgrade,$iproductmotif,
                                       $nquantity,$vunitprice,$ettbremark,$tahun,$ndeliver,$i);
#                $this->mmaster->insertdetail(  $iarea,$ittb,$dttb,$inota,$dnota,$iproduct,$iproductgrade,$iproductmotif,
#                                      $nquantity,$vunitprice,$ettbremark,$tahun,$ndeliver);
//               $this->mmaster->insertbbmdetail(  $iproduct,$iproductgrade,$eproductname,$iproductmotif,$nquantity,
//                                        $vunitprice,$ittb,$ibbm,$eremark,$dttb);
              }
            }
            if ($this->db->trans_status() === FALSE)
            {
               $this->db->trans_rollback();
            }else{
               $this->db->trans_commit();

               $sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
                  $now    = $row['c'];
               }
               $pesan='Update TTB Retur Area '.$iarea.' No:'.$ittb;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );

#              $this->db->trans_rollback();
               $data['sukses']=true;
               $data['inomor']   = $iarea.$ittb;
               $this->load->view('nomor',$data);
            }
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function edit()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('ttbretur')." update";
         if(($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')){
            $ittb    = str_replace('%20','',$this->uri->segment(4));
            $iarea      = $this->uri->segment(5);
            $tahun      = $this->uri->segment(6);
            $dfrom      = $this->uri->segment(7);
            $dto        = $this->uri->segment(8);
            $query      = $this->db->query("select i_area, i_ttb, d_ttb, i_nota, d_nota, i_product1, i_product1_grade, i_product1_motif, i_product2,
                                      i_product2_grade, i_product2_motif, n_quantity, n_quantity_receive, v_unit_price, n_ttb_year from
                                      tm_ttbretur_item where i_ttb = '$ittb' and i_area = '$iarea' and n_ttb_year=$tahun");
            $data['jmlitem']     = $query->num_rows();
            $data['ittb']        = str_replace('%20','',$this->uri->segment(4));
            $data['iarea']       = $iarea;
            $data['tahun']       = $tahun;
            $data['dfrom']       = $dfrom;
            $data['dto']            = $dto;
            $this->load->model('ttbretur/mmaster');
            $data['isi']=$this->mmaster->bacattb($iarea,$ittb,$tahun);
            $data['detail']=$this->mmaster->bacattbdetail($iarea,$ittb,$tahun);
            $this->load->view('ttbretur/vmainform',$data);
         }else{
            $this->load->view('ttbretur/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function editna()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('ttbretur')." update";
         if(($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')){
            $ittb    = str_replace('%20','',$this->uri->segment(4));
            $iarea      = $this->uri->segment(5);
            $tahun      = $this->uri->segment(6);
            $dfrom      = $this->uri->segment(7);
            $dto        = $this->uri->segment(8);
            $query      = $this->db->query("select i_area, i_ttb, d_ttb, i_nota, d_nota, i_product1, i_product1_grade, i_product1_motif, i_product2,
                                      i_product2_grade, i_product2_motif, n_quantity, n_quantity_receive, v_unit_price, n_ttb_year from

                                      tm_ttbretur_item where i_ttb = '$ittb' and i_area = '$iarea' and n_ttb_year=$tahun");
            $data['jmlitem']     = $query->num_rows();
            $data['ittb']        = str_replace('%20','',$this->uri->segment(4));
            $data['iarea']       = $iarea;
            $data['tahun']       = $tahun;
            $data['dfrom']       = $dfrom;
            $data['dto']            = $dto;
            $this->load->model('ttbretur/mmaster');
            $data['isi']=$this->mmaster->bacattb($iarea,$ittb,$tahun);
            $data['detail']=$this->mmaster->bacattbdetail($iarea,$ittb,$tahun);
            $this->load->view('ttbretur/vformupdatena',$data);
         }else{
            $this->load->view('ttbretur/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function updatettbretur()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('ttbretur/mmaster');
         $iarea         = $this->input->post('iarea', TRUE);
         $ipricegroup   = $this->input->post('ipricegroup',TRUE);
         $nttbdiscount1 = $this->input->post('nttbdiscount1',TRUE);
         $nttbdiscount2 = $this->input->post('nttbdiscount2',TRUE);
         $nttbdiscount3 = $this->input->post('nttbdiscount3',TRUE);
         $vttbdiscount1 = $this->input->post('vttbdiscount1',TRUE);
         $vttbdiscount2 = $this->input->post('vttbdiscount2',TRUE);
         $vttbdiscount3 = $this->input->post('vttbdiscount3',TRUE);
         $vttbdiscounttotal   = $this->input->post('vttbdiscounttotal',TRUE);
         $vttbdiscount1 = str_replace(',','',$vttbdiscount1);
         $vttbdiscount2 = str_replace(',','',$vttbdiscount2);
         $vttbdiscount3 = str_replace(',','',$vttbdiscount3);
         $vttbdiscounttotal   = str_replace(',','',$vttbdiscounttotal);
         $vttbnetto        = $this->input->post('vttbnetto',TRUE);
         $vttbnetto        = str_replace(',','',$vttbnetto);
         $vttbgross        = $this->input->post('vttbgross',TRUE);
         $vttbgross        = str_replace(',','',$vttbgross);
         $jml           = $this->input->post('jml', TRUE);
         $ittb             = $this->input->post('ittb', TRUE);
         $dttb             = $this->input->post('dttb', TRUE);
         $isalesman  = $this->input->post('isalesman',TRUE);
         $ialasanretur  = $this->input->post('ialasanretur', TRUE);
         if($dttb!=''){
            $tmp=explode("-",$dttb);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dttb=$th."-".$bl."-".$hr;
         }
         $tahun   = $this->input->post('nttbyear', TRUE);
         $dreceive1  = $this->input->post('dreceive1', TRUE);
         if($dreceive1!=''){
            $tmp=explode("-",$dreceive1);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dreceive1=$th."-".$bl."-".$hr;
         }else{
        $dreceive1=null;
      }
         $ettbremark    = $this->input->post('eremark', TRUE);
         if($ettbremark=='') $ettbremark=null;
         $ibbm            = $this->input->post('ibbm', TRUE);
         $dbbm            = $dttb;
         $icustomer    = $this->input->post('icustomer', TRUE);
         $istore           = 'AA';
         $istorelocation      = '01';
         $istorelocationbin   = '00';
         $eremark       = 'TTB Retur';
         $ibbktype         = '01';
         $ibbmtype         = '05';
         if(($dttb!='') && ($ittb!='') && ($iarea!='') && ($icustomer!='') && ($ialasanretur!='') && ($jml!='') && ($jml!='0')){
            $this->db->trans_begin();
            $this->mmaster->updateheader( $ittb,$iarea,$tahun,$dttb,$dreceive1,$ettbremark,
                                    $nttbdiscount1,$nttbdiscount2,$nttbdiscount3,$vttbdiscount1,
                                    $vttbdiscount2,$vttbdiscount3,$vttbdiscounttotal,$vttbnetto,
                                    $vttbgross,$icustomer,$ibbm,$isalesman,$ialasanretur,$ipricegroup);
            for($i=1;$i<=$jml;$i++){
              $iproduct             = $this->input->post('iproduct'.$i, TRUE);
              $iproductgrade        = 'A';
              $iproductmotif        = $this->input->post('motif'.$i, TRUE);
              $eproductname            = $this->input->post('eproductname'.$i, TRUE);
              $vunitprice           = $this->input->post('vunitprice'.$i, TRUE);
              $vunitprice           = str_replace(',','',$vunitprice);
              $ndeliver             = $this->input->post('ndeliver'.$i, TRUE);
              $nquantity            = $this->input->post('nquantity'.$i, TRUE);
              $ettbremark           = $this->input->post('eremark'.$i, TRUE);
              if($ettbremark=='')
               $ettbremark=null;
#             $this->mmaster->deletedetail(  $iarea, $ittb, $iproduct, $iproductgrade, $iproductmotif, $tahun);
              $this->mmaster->updatedetail(  $iarea,$ittb,$dttb,$iproduct,$iproductgrade,$iproductmotif,$nquantity,
                                                   $vunitprice,$ettbremark,$tahun,$ndeliver,$i);
              $this->mmaster->updatebbm(  $ittb,$dttb,$iproduct,$iproductgrade,$iproductmotif,$vunitprice);

            }
            if ($this->db->trans_status() === FALSE)
            {
               $this->db->trans_rollback();
            }else{
               $this->db->trans_commit();

               $sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
                  $now    = $row['c'];
               }
               $pesan='Update TTB Retur Area '.$iarea.' No:'.$ittb;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );

#              $this->db->trans_rollback();
               $data['sukses']=true;
               $data['inomor']=$ittb;
               $this->load->view('nomor',$data);
            }
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function product()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['baris'] = $this->uri->segment(4);
         $baris         = $this->uri->segment(4);
         $kdharga    = $this->uri->segment(5);
         $customer      = $this->uri->segment(6);
         $config['base_url'] = base_url().'index.php/ttbretur/cform/product/'.$baris.'/'.$kdharga.'/'.$customer.'/index/';
         $query = $this->db->query(" select a.i_product as kode
                                            from tr_product_motif a,tr_product_price b,tr_product c
                                            where b.i_product=a.i_product
                                           and a.i_product=c.i_product
                                           and b.i_price_group='$kdharga'",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $this->load->model('ttbretur/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
         $data['isi']      = $this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(8),$kdharga,$customer);
         $data['baris']    = $baris;
         $data['kdharga']  = $kdharga;
         $data['customer'] = $customer;
         $this->load->view('ttbretur/vlistproduct', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cariproduct()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $baris   = $this->input->post('baris',TRUE);
         $kdharga = $this->input->post('iproductprice',TRUE);
         $customer   = $this->input->post('icustomer',TRUE);
         $data['baris']=$baris;
         $data['kdharga']=$kdharga;
         $data['customer']=$customer;
         $config['base_url'] = base_url().'index.php/ttbretur/cform/product/'.$baris.'/'.$kdharga.'/'.$customer.'/index/';
         $cari = strtoupper($this->input->post('cari', FALSE));
         $query = $this->db->query("   select a.i_product as kode
                                                   from tr_product_motif a,tr_product_price b,tr_product c
                                                   where b.i_product=a.i_product
                                                      and a.i_product=c.i_product
                                                      and b.i_price_group='$kdharga'
                                                      and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $this->load->model('ttbretur/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
         $data['isi']      = $this->mmaster->cariproduct($cari,$kdharga,$customer,$config['per_page'],$this->uri->segment(8));
         $data['baris']    = $baris;
         $data['kdharga']  = $kdharga;
         $data['customer'] = $customer;
         $this->load->view('ttbretur/vlistproduct', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/ttbretur/cform/area/index/';
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') {
            $query = $this->db->query("select i_area from tr_area ",false);
         } else {
            $query = $this->db->query("select i_area from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' ",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('ttbretur/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
         $this->load->view('ttbretur/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/ttbretur/cform/area/index/';
         $cari = strtoupper($this->input->post('cari', FALSE));
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') {
            $query = $this->db->query("select * from tr_area
                                    where upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%' ",false);
         }else{
            $query = $this->db->query("select * from tr_area
                                    where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5') and
                                     upper(e_area_name) like '%$cari%'",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('ttbretur/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
         $this->load->view('ttbretur/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function alasan()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari = strtoupper($this->input->post('cari',false));
         $config['base_url'] = base_url().'index.php/ttbretur/cform/alasan/';
         $config['per_page'] = '10';
         $query = $this->db->query("select i_alasan_retur from tr_alasan_retur
                                 where upper(i_alasan_retur) like '%$cari%' or upper(e_alasan_returname) like '%$cari%'",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(4);
         $this->pagination->initialize($config);
         $this->load->model('ttbretur/mmaster');
         $data['page_title'] = $this->lang->line('listalasanretur');
         $data['isi']=$this->mmaster->bacaalasan($cari,$config['per_page'],$this->uri->segment(4));
         $data['cari']=$cari;
         $this->load->view('ttbretur/vlistalasan', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function customer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea = $this->uri->segment(4);
         $config['base_url'] = base_url().'index.php/ttbretur/cform/customer/'.$iarea.'/index/';
         $config['per_page'] = '10';
         $query = $this->db->query("select * from tr_customer
                              where i_area = '$iarea'",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);

         $this->load->model('ttbretur/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->bacacustomer($iarea,$config['per_page'],$this->uri->segment(6));
         $data['iarea']=$iarea;
         $this->load->view('ttbretur/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function caricustomer()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/ttbretur/cform/customer/index/';
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $iarea = $this->uri->segment(4);
         $query = $this->db->query("select * from tr_customer
                     where i_area='$iarea'
                      and (upper(i_customer) like '%$cari%'
                             or upper(e_customer_name) like '%$cari%') ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('ttbretur/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
         $data['iarea']=$iarea;
         $this->load->view('ttbretur/vlistcustomer', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function productupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['baris'] = $this->uri->segment(4);
         $baris         = $this->uri->segment(4);
         $kdharga    = $this->uri->segment(5);
         $customer      = $this->uri->segment(6);
         $config['base_url'] = base_url().'index.php/ttbretur/cform/productupdate/'.$baris.'/'.$kdharga.'/'.$customer.'/index/';
         $query = $this->db->query(" select a.i_product as kode
                              from tr_product_motif a,tr_product_price b,tr_product c, tm_nota_item d, tm_nota e
                              where b.i_product=a.i_product
                                and a.i_product=c.i_product
                                and b.i_price_group='$kdharga'
                                and d.i_product=c.i_product
                                and d.i_product=a.i_product
                                and d.i_product_motif=a.i_product_motif
                                and d.i_nota=e.i_nota
                                and d.n_deliver>0
                                and e.i_customer='$customer'"
                             ,false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $this->load->model('ttbretur/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
         $data['isi']      = $this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(8),$kdharga,$customer);
         $data['baris']    = $baris;
         $data['kdharga']  = $kdharga;
         $data['customer'] = $customer;
         $this->load->view('ttbretur/vlistproductupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cariproductupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu84')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $baris   = $this->input->post('baris',TRUE);
         $kdharga = $this->input->post('iproductprice',TRUE);
         $customer   = $this->input->post('icustomer',TRUE);
         $data['baris']=$baris;
         $data['kdharga']=$kdharga;
         $data['customer']=$customer;
         $config['base_url'] = base_url().'index.php/ttbretur/cform/productupdate/'.$baris.'/'.$kdharga.'/'.$customer.'/index/';
         $cari = strtoupper($this->input->post('cari', FALSE));
         $query = $this->db->query("   select a.i_product as kode
                                                   from tr_product_motif a,tr_product_price b,tr_product c, tm_nota_item d, tm_nota e
                                                   where b.i_product=a.i_product
                                                      and a.i_product=c.i_product
                                                      and b.i_price_group='$kdharga'
                                                      and d.i_product=c.i_product
                                                      and d.i_product=a.i_product
                                                      and d.i_product_motif=a.i_product_motif
                                                      and d.i_nota=e.i_nota
                                                      and d.n_deliver>0
                                                      and e.i_customer='$customer'
                                                      and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);
         $this->load->model('ttbretur/mmaster');
         $data['page_title'] = $this->lang->line('list_product');
         $data['isi']      = $this->mmaster->cariproduct($cari,$kdharga,$customer,$config['per_page'],$this->uri->segment(8));
         $data['baris']    = $baris;
         $data['kdharga']  = $kdharga;
         $data['customer'] = $customer;
         $this->load->view('ttbretur/vlistproductupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
