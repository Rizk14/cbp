<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if 
    (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('penjualankonsinyasi');
      $data['ispg']='';
      $data['iarea']='';
			$data['icustomer'] = '';
			$data['eareaname'] = '';
			$data['espgname'] = '';
			$data['ecustomername'] = '';
      $data['inotapb']='';
		  $data['isi']='';
		  $data['detail']="";
		  $data['jmlitem']="";
		  $data['tgl']=date('d-m-Y');
		  $this->load->view('bonkonsinyasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$inotapb 		= $this->input->post('inotapb', TRUE);
			$dnotapb 		= $this->input->post('dnotapb', TRUE);
			if($dnotapb!=''){
				$tmp=explode("-",$dnotapb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dnotapb=$th."-".$bl."-".$hr;
				$thbl=substr($th,2,2).$bl;
			}
			$iarea	= $this->input->post('iarea', TRUE);
			$ispg   = $this->input->post('ispg', TRUE);
			$icustomer= $this->input->post('icustomer', TRUE);
			$nnotapbdiscount= $this->input->post('nnotapbdiscount', TRUE);
		  $nnotapbdiscount= str_replace(',','',$nnotapbdiscount);
			$vnotapbdiscount= $this->input->post('vnotapbdiscount', TRUE);
		  $vnotapbdiscount= str_replace(',','',$vnotapbdiscount);
			$vnotapbgross= $this->input->post('vnotapbgross', TRUE);
		  $vnotapbgross	  	= str_replace(',','',$vnotapbgross);
			$jml		= $this->input->post('jml', TRUE);
			if($dnotapb!='' && $inotapb!='' && $jml>0)
			{
				$this->db->trans_begin();
				$this->load->model('bonkonsinyasi/mmaster');
				settype($inotapb,"string");
        $a=strlen($inotapb);
        while($a<7){
         $inotapb="0".$inotapb;
         $a=strlen($inotapb);
        }
				$inotapb = 'FB-'.$thbl.'-'.$inotapb;
				$this->mmaster->insertheader($inotapb, $dnotapb, $iarea, $ispg, $icustomer, $nnotapbdiscount, $vnotapbdiscount, $vnotapbgross);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= 'A';
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		  = $this->input->post('vunitprice'.$i, TRUE);
				  $vunitprice	  	= str_replace(',','',$vunitprice);
				  $nquantity   		= $this->input->post('nquantity'.$i, TRUE);
				  $nquantity	  	= str_replace(',','',$nquantity);
          $ipricegroupco  = $this->input->post('ipricegroupco'.$i, TRUE);
          $eremark        = $this->input->post('eremark'.$i, TRUE);
				  if($nquantity>0){
				    $this->mmaster->insertdetail( $inotapb,$iarea,$icustomer,$dnotapb,$iproduct,$iproductmotif,$iproductgrade,$nquantity,$vunitprice,$i,$eproductname,$ipricegroupco,$eremark);
            $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$icustomer);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_stock;
                $q_ak =$itrans->n_quantity_stock;
                $q_in =0;
                $q_out=0;
                break;
              }
            }else{
              $q_aw=0;
              $q_ak=0;
              $q_in=0;
              $q_out=0;
            }
            $th=substr($dnotapb,0,4);
            $bl=substr($dnotapb,5,2);
            $emutasiperiode=$th.$bl;
            $ada=$this->mmaster->cekmutasi2($iproduct,$iproductgrade,$iproductmotif,$icustomer,$emutasiperiode);
            if($ada=='ada')
            {
              $this->mmaster->updatemutasi1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$nquantity,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasi1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$nquantity,$emutasiperiode,$q_aw,$q_ak);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$icustomer))
            {
              $this->mmaster->updateic1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$nquantity,$q_ak);
            }else{
              $this->mmaster->insertic1($iproduct,$iproductgrade,$iproductmotif,$icustomer,$eproductname,$nquantity);
            }
				  }
				}
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
#			    $this->db->trans_rollback();
			    $this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		=  $this->db->query($sql);
			    if($rs->num_rows>0){
				    foreach($rs->result() as $tes){
					    $ip_address	  = $tes->ip_address;
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }

			    $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
			    $data['host']	= $ip_address;
			    $data['uri']	= $this->session->userdata('printeruri');
				  $query 	= pg_query("SELECT current_timestamp as c");
				  while($row=pg_fetch_assoc($query)){
					  $now	  = $row['c'];
				  }
				  $pesan='Input Penjualan Konsinyasi dari  Pusat Area : '.$iarea.' No : '.$inotapb;
				  $this->load->model('logger');
				  $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $inotapb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('penjualankonsinyasi');
			$this->load->view('bonkonsinyasi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('penjualankonsinyasi')." update";
			if($this->uri->segment(4)!=''){
				$inotapb  = $this->uri->segment(4);
        $inotapb=str_replace("%20","",$inotapb);
				$icustomer= $this->uri->segment(5);
				$dfrom    = $this->uri->segment(6);
				$dto 	    = $this->uri->segment(7);
        $iarea    = $this->uri->segment(8);
				$data['inotapb']  = $inotapb;
				$data['icustomer']= $icustomer;
				$data['dfrom'] = $dfrom;
				$data['dto']	 = $dto;
				$data['iarea'] = $iarea;
				$query = $this->db->query("select i_product from tm_notapb_item where i_notapb='$inotapb' and i_customer='$icustomer'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('bonkonsinyasi/mmaster');
				$data['isi']=$this->mmaster->baca($inotapb,$icustomer);
				$data['detail']=$this->mmaster->bacadetail($inotapb,$icustomer);
		 		$this->load->view('bonkonsinyasi/vmainform',$data);
			}else{
				$this->load->view('bonkonsinyasi/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$xinotapb		= $this->input->post('xinotapb', TRUE);
			$inotapb 		= $this->input->post('inotapb', TRUE);
			$dnotapb 		= $this->input->post('dnotapb', TRUE);
			if($dnotapb!=''){
				$tmp=explode("-",$dnotapb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dnotapb=$th."-".$bl."-".$hr;
				$thbl=substr($th,2,2).$bl;
			}
			$iarea	= $this->input->post('iarea', TRUE);

			$ispg   = $this->input->post('ispg', TRUE);
			$icustomer= $this->input->post('icustomer', TRUE);
			$nnotapbdiscount= $this->input->post('nnotapbdiscount', TRUE);
		  $nnotapbdiscount= str_replace(',','',$nnotapbdiscount);
			$vnotapbdiscount= $this->input->post('vnotapbdiscount', TRUE);
		  $vnotapbdiscount= str_replace(',','',$vnotapbdiscount);
			$vnotapbgross= $this->input->post('vnotapbgross', TRUE);
		  $vnotapbgross	  	= str_replace(',','',$vnotapbgross);
			$jml		= $this->input->post('jml', TRUE);
			if($dnotapb!='' && $inotapb!='' && $jml>0)
			{
				$this->db->trans_begin();
				$this->load->model('bonkonsinyasi/mmaster');
				settype($inotapb,"string");
        $a=strlen($inotapb);
        while($a<7){
         $inotapb="0".$inotapb;
         $a=strlen($inotapb);
        }
				$inotapb = 'FB-'.$thbl.'-'.$inotapb;
				$this->mmaster->deleteheader($xinotapb, $iarea, $icustomer);
				$this->mmaster->insertheader($inotapb, $dnotapb, $iarea, $ispg, $icustomer, $nnotapbdiscount, $vnotapbdiscount, $vnotapbgross);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= 'A';
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		  = $this->input->post('vunitprice'.$i, TRUE);
				  $vunitprice	  	= str_replace(',','',$vunitprice);
				  $nquantity   		= $this->input->post('nquantity'.$i, TRUE);
				  $nquantity	  	= str_replace(',','',$nquantity);
          $ipricegroupco  = $this->input->post('ipricegroupco'.$i, TRUE);
          $eremark        = $this->input->post('eremark'.$i, TRUE);
          $this->mmaster->deletedetail($iproduct, $iproductgrade, $xinotapb, $iarea, $icustomer, $iproductmotif);
				  if($nquantity>0){
				    $this->mmaster->insertdetail( $inotapb,$iarea,$icustomer,$dnotapb,$iproduct,$iproductmotif,$iproductgrade,$nquantity,$vunitprice,$i,$eproductname,$ipricegroupco,$eremark);
				  }
				}
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
#			    $this->db->trans_rollback();
			    $this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		=  $this->db->query($sql);
			    if($rs->num_rows>0){
				    foreach($rs->result() as $tes){
					    $ip_address	  = $tes->ip_address;
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }

			    $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
			    $data['host']	= $ip_address;
			    $data['uri']	= $this->session->userdata('printeruri');
				  $query 	= pg_query("SELECT current_timestamp as c");
				  while($row=pg_fetch_assoc($query)){
					  $now	  = $row['c'];
				  }
				  $pesan='Update Penjualan Konsinyasi dari Pusat Area : '.$iarea.' No : '.$inotapb;
				  $this->load->model('logger');
				  $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $inotapb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari=strtoupper($this->input->post("cari"));
      $cust=strtoupper($this->input->post("cust"));
      if($cust=='') $cust=$this->uri->segment(5);
      if($cari=='' && $this->uri->segment(6)!='zxasqw' )$cari=$this->uri->segment(6);
      $baris=$this->input->post("baris");
			if($baris=='')$baris=$this->uri->segment(4);
			if($this->uri->segment(6)=='zxasqw'){
  			$config['base_url'] = base_url().'index.php/bonkonsinyasi/cform/product/'.$baris.'/'.$cust.'/zxasqw/';
      }else{
  			$config['base_url'] = base_url().'index.php/bonkonsinyasi/cform/product/'.$baris.'/'.$cust.'/'.$cari.'/';      
      }
			$query = $this->db->query(" select a.i_product
										              from tr_product_motif a,tr_product c, tr_product_priceco b, tr_customer_consigment d
										              where a.i_product=c.i_product 
										              and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') 
										              and a.i_product=b.i_product
										              and d.i_customer='$cust' and d.i_price_groupco=b.i_price_groupco and a.i_product_motif='00'
										              ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya'; 
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('bonkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$baris;
			$data['cust']=$cust;
			$data['cari']=$cari;
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(7),$cari,$cust);
			$this->load->view('bonkonsinyasi/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bonkonsinyasi/cform/product/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
								and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc ", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('bonkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('bonkonsinyasi/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bonkonsinyasi/cform/area/index/';
			$allarea	= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($allarea=='t') {
				$query = $this->db->query("select * from tr_area ",false);
			}else {
				$query = $this->db->query("select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3'
						or i_area='$area4' or i_area='$area5' ",false);				
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bonkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5,$allarea);
			$this->load->view('bonkonsinyasi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bonkonsinyasi/cform/area/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$allarea	= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($allarea=='t') {
				$query = $this->db->query("select * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%'",false);
			}else {
				$query = $this->db->query("select * from tr_area where (i_area='$area1' or i_area='$area2' or i_area='$area3'
						or i_area='$area4' or i_area='$area5') and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);				
			}				
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bonkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5,$allarea);
			$this->load->view('bonkonsinyasi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function spg()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bonkonsinyasi/cform/spg/index/';
			$allarea	= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($allarea=='t') {
				$query = $this->db->query(" select a.*, b.e_customer_name
                                    from tr_spg a, tr_customer b
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area
                                    order by a.i_spg ",false);
			}else {
				$query = $this->db->query(" select a.*, b.e_customer_name
                                    from tr_spg a, tr_customer b
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area
                                    and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3'
                                    or a.i_area='$area4' or a.i_area='$area5')
                                    order by a.i_spg ",false);				
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bonkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_spg');
			$data['isi']=$this->mmaster->bacaspg($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5,$allarea);
			$this->load->view('bonkonsinyasi/vlistspg', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carispg()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bonkonsinyasi/cform/spg/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$allarea	= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($allarea=='t') {
				$query = $this->db->query(" select a.*, b.e_customer_name
                                    from tr_spg a, tr_customer b
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area
                                    and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or
                                    upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
                                    order by a.i_spg ",false);
			}else {
				$query = $this->db->query(" select a.*, b.e_customer_name
                                    from tr_spg a, tr_customer b
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area
                                    and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3'
                                    or a.i_area='$area4' or a.i_area='$area5')
                                    and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or
                                    upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
                                    order by a.i_spg ",false);				
			}				
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bonkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_spg');
			$data['isi']=$this->mmaster->carispg($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5,$allarea);
			$this->load->view('bonkonsinyasi/vlistspg', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
