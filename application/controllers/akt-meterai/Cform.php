<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
				(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu536')=='t')) ||
				(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
			){
			
				$data['dfrom']		= '';
				$data['dto']  		= '';
				$data['iarea']		= '';
				$data['isi']  		= '';
				$data['page_title'] = $this->lang->line('akt-meterai');

				$this->load->view('akt-meterai/vform', $data);

			}elseif($this->session->userdata('logged_in')){
				$this->load->view('errorauthority');
			}else{
				$this->load->view('awal/index.php');
			}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu536')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
			){
				$data['page_title'] = $this->lang->line('akt-meterai');
				$this->load->view('akt-meterai/vinsert_fail',$data);
			}else{
				$this->load->view('awal/index.php');
			}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu536')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
			){
				$config['base_url'] = base_url().'index.php/akt-meterai/cform/area/index/';
				$iuser   			= $this->session->userdata('user_id');
				
				$query 				= $this->db->query(" select * 
														from tr_area 
														where 
														i_area in ( select i_area from tm_user_area where i_user='$iuser') 
														order by i_area", false);
				
				$config['total_rows'] 	= $query->num_rows();
				$config['per_page'] 	= '10';
				$config['first_link'] 	= 'Awal';
				$config['last_link'] 	= 'Akhir';
				$config['next_link'] 	= 'Selanjutnya';
				$config['prev_link'] 	= 'Sebelumnya';
				$config['cur_page'] 	= $this->uri->segment(5);
				$this->pagination->initialize($config);

				$this->load->model('akt-meterai/mmaster');

				$data['page_title'] = $this->lang->line('list_area');
				$data['isi']		= $this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
				
				$this->load->view('akt-meterai/vlistarea', $data);
			}else{
				$this->load->view('awal/index.php');
			}
	}
   function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu536')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
			){
				$config['base_url'] = base_url().'index.php/akt-meterai/cform/area/index/';
				$iuser   			= $this->session->userdata('user_id');
				$cari    			= $this->input->post('cari', FALSE);
				$cari 		  		= strtoupper($cari);
				
				$query   		= $this->db->query("select * from tr_area
													where 
													(upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
													and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
				$config['total_rows'] 	= $query->num_rows();
				$config['per_page'] 	= '10';
				$config['first_link'] 	= 'Awal';
				$config['last_link'] 	= 'Akhir';
				$config['next_link'] 	= 'Selanjutnya';
				$config['prev_link'] 	= 'Sebelumnya';
				$config['cur_page'] 	= $this->uri->segment(5);
				$this->pagination->initialize($config);
				
				$this->load->model('akt-meterai/mmaster');

				$data['page_title'] = $this->lang->line('list_area');
				$data['isi']		= $this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
				
				$this->load->view('akt-meterai/vlistarea', $data);
			}else{
				$this->load->view('awal/index.php');
			}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu536')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom 	= $this->input->post('dfrom');
			$dto 	= $this->input->post('dto');
			$iarea 	= $this->input->post('iarea');
			$cari   = strtoupper($this->input->post('cari'));
			if($cari=='') $cari = $this->uri->segment(4);

			if($dfrom != '' && $dto != '' && $iarea != ''){
				if($iarea == "NA"){
					$query 	= $this->db->query("SELECT
													a.i_area,
													c.e_area_name,
													a.i_customer,
													b.e_customer_name,
													a.i_nota,
													a.d_nota,
													a.v_nota_netto,
													a.v_sisa,
													a.v_materai,
													a.v_materai_sisa
												FROM
													tm_nota a
												INNER JOIN tr_customer b ON
													(a.i_customer = b.i_customer
														AND a.i_area = b.i_area)
												INNER JOIN tr_area c ON
													(a.i_area = c.i_area)
												WHERE
													a.f_nota_cancel = 'f'
													AND a.v_sisa = 0
													AND a.v_materai_sisa > 0
													AND a.d_nota >= to_date('$dfrom','dd-mm-yyyy')
													AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
												ORDER BY
													a.i_nota ");
				}else{
					$query 	= $this->db->query("SELECT
													a.i_area,
													c.e_area_name,
													a.i_customer,
													b.e_customer_name,
													a.i_nota,
													a.d_nota,
													a.v_nota_netto,
													a.v_sisa,
													a.v_materai,
													a.v_materai_sisa
												FROM
													tm_nota a
												INNER JOIN tr_customer b ON
													(a.i_customer = b.i_customer
														AND a.i_area = b.i_area)
												INNER JOIN tr_area c ON
													(a.i_area = c.i_area)
												WHERE
													a.f_nota_cancel = 'f'
													AND a.v_sisa = 0
													AND a.v_materai_sisa > 0
													AND a.d_nota >= to_date('$dfrom','dd-mm-yyyy')
													AND a.d_nota <= to_date('$dto','dd-mm-yyyy')
													AND a.i_area = '$iarea'
												ORDER BY
													a.i_nota ");
				}
				
				$config['total_rows']   = $query->num_rows();
				$config['per_page']  	= 'all';
				$config['first_link']   = 'Awal';
				$config['last_link']    = 'Akhir';
				$config['next_link']    = 'Selanjutnya';
				$config['prev_link']    = 'Sebelumnya';
				$config['base_url'] 	= base_url().'index.php/akt-meterai/cform/view/'.$cari.'/index/';
				$config['cur_page']  	= $this->uri->segment(6);
				$this->paginationxx->initialize($config);

				$this->load->model('akt-meterai/mmaster');
				
				$data['cari']  		= $cari;
				$data['page_title'] = $this->lang->line('akt-meterai');
				$data['isi']   		= $this->mmaster->bacaperiode($dfrom, $dto, $iarea, $config['per_page'],$this->uri->segment(6),$cari);

				$query = $this->db->query(" select i_periode from tm_periode ",false);
				if ($query->num_rows() > 0){
					foreach($query->result() as $row){
						$data['periode'] = $row->i_periode;
					}
				}
				$this->load->view('akt-meterai/vformview', $data);
			}else{
				echo "<script>alert ('Data masih ada yang salah!!!')</script>";
				$data['page_title'] = $this->lang->line('akt-meterai');
				$this->load->view('akt-meterai/vform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu536')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$jml  	  = $this->input->post('jml', TRUE);
			$thbl	  = date('Ym');
			$dalokasi = date('Y-m-d');
			$icoabank = "000-00000";

			$this->load->model('akt-meterai/mmaster');
			
			$this->db->trans_begin();
				for ($i=1; $i < $jml ; $i++) {
					$chk			= $this->input->post('chk'.$i, TRUE);

					if($chk == 1){
						$inota 			= $this->input->post('inota' . $i, TRUE);
						$dnota 			= $this->input->post('dnota' . $i, TRUE);
						$iarea 			= $this->input->post('iarea' . $i, TRUE);
						$icustomer		= $this->input->post('icustomer' . $i, TRUE);
						$vjumlah    	= $this->input->post('vmeterai' . $i, TRUE);
						$vmeteraisisa	= $this->input->post('vmeteraisisa' . $i, TRUE);

						$ialokasi 	= $this->mmaster->runningnumber($iarea, $thbl);

					//* START POSTING
						$egirodescription 	= "Alokasi Meterai (Penyesuaian)";
						$fclose     		= 'f';
						$ireff 				= $ialokasi . '|' . $inota;

						//* DEBET
						$accdebet   = cons_beameterai;
						$namadebet  = $this->mmaster->namaacc($accdebet);

						$tmp        = $this->mmaster->carisaldo($accdebet, $thbl);
						if ($tmp)
							$vsaldoaw1    = $tmp->v_saldo_awal;
						else
							$vsaldoaw1    = 0;
						if ($tmp)
							$vmutasidebet1  = $tmp->v_mutasi_debet;
						else
							$vmutasidebet1  = 0;
						if ($tmp)
							$vmutasikredit1 = $tmp->v_mutasi_kredit;
						else
							$vmutasikredit1 = 0;
						if ($tmp)
							$vsaldoak1    = $tmp->v_saldo_akhir;
						else
							$vsaldoak1    = 0;
		
						//* KREDIT
						$acckredit    = cons_meterai;
						$namakredit   = $this->mmaster->namaacc($acckredit);

						$saldoawkredit  = $this->mmaster->carisaldo($acckredit, $thbl);
						if ($tmp)
							$vsaldoaw2    = $tmp->v_saldo_awal;
						else
							$vsaldoaw2    = 0;
						if ($tmp)
							$vmutasidebet2  = $tmp->v_mutasi_debet;
						else
							$vmutasidebet2  = 0;
						if ($tmp)
							$vmutasikredit2 = $tmp->v_mutasi_kredit;
						else
							$vmutasikredit2 = 0;
						if ($tmp)
							$vsaldoak2    = $tmp->v_saldo_akhir;
						else
							$vsaldoak2    = 0;

						//* INSERT tm_jurnal_transharian
						$this->mmaster->inserttransheader($ireff, $iarea, $egirodescription, $fclose, $dalokasi);
						
						//* INSERT tm_jurnal_transharian_item
						$this->mmaster->inserttransitemdebet($accdebet, $ireff, $namadebet, 't', 't', $iarea, $egirodescription, $vjumlah, $dalokasi, $icoabank);
						$this->mmaster->inserttransitemkredit($acckredit, $ireff, $namakredit, 'f', 't', $iarea, $egirodescription, $vjumlah, $dalokasi, $icoabank);
						
						//* INSERT tm_general_ledger
						$this->mmaster->insertgldebet($accdebet, $ireff, $namadebet, 't', $iarea, $vjumlah, $dalokasi, $egirodescription, $icoabank);
						$this->mmaster->insertglkredit($acckredit, $ireff, $namakredit, 'f', $iarea, $vjumlah, $dalokasi, $egirodescription, $icoabank);
						
						//* UPDATE tm_coa_saldo
						$this->mmaster->updatesaldodebet($accdebet, $thbl, $vjumlah);
						$this->mmaster->updatesaldokredit($acckredit, $thbl, $vjumlah);
					//* END POSTING

					//* INSERT HEADER
						$this->mmaster->insertheader($ialokasi, $iarea, $icustomer, $dalokasi, $vjumlah, $icoabank);

					//* INSERT DETAIL
						$this->mmaster->insertdetail($ialokasi, $iarea, $inota, $dnota, $vjumlah, $vmeteraisisa, $icoabank);

					//* UPDATE v_materai_sisa
						$this->mmaster->updatenota($inota, $iarea, $vjumlah);

						$nomor[]=$ialokasi;
					}
				}
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

					for($i=0;$i<count($nomor);$i++){
						$sess	= $this->session->userdata('session_id');
						$id		= $this->session->userdata('user_id');
						$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
						$rs		= pg_query($sql);
						if(pg_num_rows($rs)>0){
							while($row=pg_fetch_assoc($rs)){
								$ip_address	  = $row['ip_address'];
								break;
							}
						}else{
							$ip_address='kosong';
						}
						$query 	= pg_query("SELECT current_timestamp as c");
						while($row=pg_fetch_assoc($query)){
							$now	  = $row['c'];
						}

						$pesan='Input Alokasi Meterai (Penyesuaian) No : '.$nomor[$i];
						$this->load->model('logger');
						$this->logger->write($id, $ip_address, $now , $pesan );
					}

					$data['sukses']			= true;
					$data['inomor']			= $nomor;
					$this->load->view('nomorarray',$data);
				}	
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu536')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1   = $this->session->userdata('i_area');
			$data['page_title'] = $this->lang->line('akt-meterai') . " Update";

			if (
				($this->uri->segment(4)) && ($this->uri->segment(5))
			) {
				$ialokasi = $this->uri->segment(4);
				$iarea    = $this->uri->segment(5);
				$dfrom    = $this->uri->segment(6);
				$dto      = $this->uri->segment(7);
				$iareax   = $this->uri->segment(8);

				$this->load->model('akt-meterai/mmaster');

				$query   = $this->db->query(" 	SELECT b.i_nota, to_char(a.d_alokasi,'yyyymm') AS thbl 
												FROM tm_meterai a, tm_meterai_item b
												WHERE a.i_alokasi = b.i_alokasi AND a.i_area=b.i_area
												AND a.i_alokasi = '$ialokasi' AND a.i_area = '$iarea' ");

				if ($query->num_rows() > 0) {
					$data['jmlitem'] = $query->num_rows();
					$data['isi']     = $this->mmaster->bacaheader($iarea, $ialokasi);
					$data['detail']  = $this->mmaster->bacadetail($iarea, $ialokasi);

					$hasilrow = $query->row();
					$thbl     = $hasilrow->thbl;
					$query3   = $this->db->query(" select i_periode from tm_periode ");
					if ($query3->num_rows() > 0) {
						$hasilrow = $query3->row();
						$i_periode   = $hasilrow->i_periode;
					}

					if ($i_periode <= $thbl){
						$data['bisaedit'] = true;
					}else{
						$data['bisaedit'] = false;
					}
				}
			}

			$data['ialokasi'] = $ialokasi;
			$data['iarea']    = $iarea;
			$data['iareax']   = $iareax;
			$data['dfrom']    = $dfrom;
			$data['dto']      = $dto;

			$this->load->view('akt-meterai/vformupdate', $data);

		} elseif ($this->session->userdata('logged_in')) {
			$this->load->view('errorauthority');
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
?>
