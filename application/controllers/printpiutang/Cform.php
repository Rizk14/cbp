<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu509')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/printpiutang/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('printpiutang');
			$data['cari'] = '';
			//$data['dfrom']= '';
			$data['dto']  = '';
			$data['no']   = '';
			$data['isi']  = '';
			$this->load->view('printpiutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu509')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printpiutang/cform/customer/index/';
      		$iuser  = $this->session->userdata('user_id');
      		
      		$query 	= $this->db->query(" select distinct(a.*) from tr_customer a, tm_nota b
										where 
										a.i_area in(select i_area from tm_user_area where i_user='$iuser') 
										and a.i_area=b.i_area
										and a.i_customer=b.i_customer
										and b.v_sisa<>0
										and b.f_nota_cancel='f'
										order by a.i_area asc, a.i_customer asc", false);

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('printpiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printpiutang/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu509')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printpiutang/cform/customer/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$iuser  = $this->session->userdata('user_id');
			$query 	= $this->db->query(" select i_customer, e_customer_name from tr_customer 
                          				 where 
                          				 (upper(e_customer_name) like '%$cari%' or upper(i_customer) like '%$cari%') and
                          				 i_area in(select i_area from tm_user_area where i_user='$iuser') order by i_customer asc",false);
			
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printpiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printpiutang/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu509')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			//$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$icust	= $this->input->post('icust');
			$iuser	= $this->session->userdata('user_id');
			$no 	= $this->input->post('no');

			//if($dfrom=='')$dfrom=$this->uri->segment(4);
			if($dto=='')$dto=$this->uri->segment(4);
			if($icust=='')$icust=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/printpiutang/cform/view/'.$dto.'/'.$icust.'/index/';
			
			$query = $this->db->query(" select a.i_nota from tm_nota a left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
										where 
										a.i_customer=b.i_customer 
										and (a.n_print=0 or a.n_print isnull)
										and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										or upper(a.i_nota) like '%$cari%')
										and a.d_nota <= to_date('$dto','dd-mm-yyyy')
										and a.i_customer='$icust' and a.f_lunas='f' and a.v_sisa<>0
										and f_nota_cancel='f'
										",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printpiutang');
			$this->load->model('printpiutang/mmaster');
			$data['dfrom']= $dfrom;
			$data['dto']  = $dto;
			$data['no']   = $no;
			$data['icust']= $icust;
			$data['isi']=$this->mmaster->bacasemua($icust,$cari,$config['per_page'],$this->uri->segment(6),$dto,$no);
			$this->load->view('printpiutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu509')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			//$inota = $this->uri->segment(4);
			
			$dto	  				= $this->input->post('dto');
			$no	  					= $this->input->post('no');
			$icust					= $this->input->post('icust');

      		$icust 					= $this->uri->segment(4);
      		if($icust=='')$icust 	= $this->uri->segment(4);
      		if($dto=='')$dto 		= $this->uri->segment(5);
      		if($no=='')$no 		= $this->uri->segment(6);
			$this->load->model('printpiutang/mmaster');
      		
      		$data['no']		= $no;
			$data['dto']  	= $dto;
			$data['icust']	= $icust;
			$data['page_title'] 	= $this->lang->line('printpiutang');
			$data['isi'] 			= $this->mmaster->baca($icust,$no,$dto);
			$data['hitung'] 	    = $this->mmaster->hitung($icust);

      		$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak Konfirmasi Piutang Customer:'.$icust;//.' No:'.$inota;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printpiutang/vformrpt', $data);
#      $this->mmaster->close($icust,$inota);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu509')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printpiutang');
			$this->load->view('printpiutang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu509')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printpiutang/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$query = $this->db->query(" 	select a.*, b.e_customer_name from tm_spb a, tr_customer b
							where a.i_customer=b.i_customer
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%')
							and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('printpiutang/mmaster');
			$data['isi']=$this->mmaster->cari($area1,$area2,$area3,$area4,$area5,$cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('printpiutang');
			$data['cari']=$cari;
			$data['inota']='';
			$data['detail']='';
	 		$this->load->view('printpiutang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
