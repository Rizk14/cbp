<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu154')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transkk');
			$data['isi']= directory_map('./kas kecil/');
			$data['file']='';
			$this->load->view('transkk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu154')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$file= $this->input->post('namafile', TRUE);
			$data['file']='./kas kecil/'.$file;
			$this->load->view('transkk/vfile', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu154')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml= $this->input->post('jml', TRUE);
			$this->load->model('transkk/mmaster');
			$this->db->trans_begin();
			for($i=0;$i<$jml;$i++){
				$cek					=$this->input->post('chk'.$i, TRUE);
				if($cek!=''){
					$iarea			=$this->input->post('iarea'.$i, TRUE);
					$ikk			=$this->input->post('ikk'.$i, TRUE);
					$iperiode		=$this->input->post('iperiode'.$i, TRUE);
					$icoa			=$this->input->post('icoa'.$i, TRUE);
					$ikendaraan		=$this->input->post('ikendaraan'.$i, TRUE);
					$vkk			=$this->input->post('vkk'.$i, TRUE);
					$vkk			=str_replace(',','',$vkk);
					$dkk			=$this->input->post('dkk'.$i, TRUE);
					if($dkk!=''){
						$tmp=explode("-",$dkk);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dkk=$th."-".$bl."-".$hr;
					}
					$ecoaname		=$this->input->post('ecoaname'.$i, TRUE);
					$edescription	=$this->input->post('edescription'.$i, TRUE);
					if($edescription=='') $edescription=null;
					$ejamin			=$this->input->post('ejamin'.$i, TRUE);
					if($ejamin=='') $ejamin=null;
					$ejamout		=$this->input->post('ejamout'.$i, TRUE);
					if($ejamout=='') $ejamout=null;
					$nkm			=$this->input->post('nkm'.$i, TRUE);
					if($nkm=='') $nkm=0;
					$dentry			=$this->input->post('dentry'.$i, TRUE);
/*					if($dentry!=''){
						$tmp=explode("-",$dentry);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dentry=$th."-".$bl."-".$hr;
					}
*/					$dupdate		=$this->input->post('dupdate'.$i, TRUE);
					if($dupdate=='') $dupdate=null;
/*					if($dupdate!=''){
						$tmp=explode("-",$dupdate);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dupdate=$th."-".$bl."-".$hr;
					}
*/					$fposting		=$this->input->post('fposting'.$i, TRUE);
					$fclose			=$this->input->post('fclose'.$i, TRUE);
					$etempat		=$this->input->post('etempat'.$i, TRUE);
					if($etempat=='') $etempat=null;
					$fdebet			=$this->input->post('fdebet'.$i, TRUE);
					$dbukti			=$this->input->post('dbukti'.$i, TRUE);
					if($dbukti!=''){
						$tmp=explode("-",$dbukti);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dbukti=$th."-".$bl."-".$hr;
					}
					$enamatoko		=$this->input->post('enamatoko'.$i, TRUE);
					if($enamatoko=='') $enamatoko=null;
					$this->mmaster->insertkk($iarea,$ikk,$iperiode,$icoa,$ikendaraan,$vkk,$dkk,$ecoaname,$edescription,
											 $ejamin,$ejamout,$nkm,$dentry,$dupdate,$fposting,$fclose,$etempat,$fdebet,
											 $dbukti,$enamatoko);
				}
			}
			if ( ($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
				$this->load->view('transkk/vformgagal',$data);
			}else{
				$this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Transfer KK Area '.$iarea.' Periode'.$iperiode;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

				$this->load->view('transkk/vformsukses');
			}			

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
