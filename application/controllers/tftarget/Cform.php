<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
    $this->load->library('excel');
    require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu224')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('tftarget');
			$store	= $this->session->userdata('store');
      if($store=='AA') $store='00';
			$data['isi']= directory_map('./target');
			$data['file']='';
			$this->load->view('tftarget/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu224')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$file= $this->input->post('namafile', TRUE);
			$tgl = $this->input->post('tgl', TRUE);
			$store	= $this->session->userdata('store');
      $fileName = $file;
                  $ext4='.xls';
                  $inputFileName = 'target/'.$file;
                  $data['filename'] = $fileName.$ext4;
                  $objPHPExcel = IOFactory::load($inputFileName);

                  //$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                  $sheet = $objPHPExcel->getSheet(0);
                  $hrow = $sheet->getHighestRow();
                  $aray = array();
                  for ($n=2; $n<=$hrow; $n++){
                    // var_dump($objPHPExcel->getActiveSheet()->getCell('D'.$n)->getCalculatedValue());
                    // die('HHi!!!!');
                    $aray[] = array(
                    'PERIODE'      => $objPHPExcel->getActiveSheet()->getCell('A'.$n)->getCalculatedValue(),
                    'KODELANG'     => $objPHPExcel->getActiveSheet()->getCell('B'.$n)->getCalculatedValue(),
                    'TARGET(RP)'   => $objPHPExcel->getActiveSheet()->getCell('C'.$n)->getCalculatedValue(),
                    'TARGET(QTY)'  => $objPHPExcel->getActiveSheet()->getCell('D'.$n)->getCalculatedValue(),
                    );
                  }
                  $data['items'] = $aray;
                  $data['file']=$file;

			//$data['file']='so/'.$store.'/'.$file;
			$data['tgl'] =$tgl;
			$this->load->view('tftarget/vfile', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu224')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $filetf = $this->input->post('filetf', TRUE);
      $this->load->model('tftarget/mmaster');
      $this->db->trans_begin();

                  $fileName = $filetf;
                  $ext4='.xls';
                  $inputFileName = 'target/'.$filetf;
                  $data['filename'] = $fileName.$ext4;
                  $objPHPExcel = IOFactory::load($inputFileName);

                  $sheet = $objPHPExcel->getSheet(0);
                  $hrow = $sheet->getHighestRow();
                  $hcol = $sheet->getHighestColumn();
                  $aray = array();

                  for ($n=2; $n<=$hrow; $n++){
                          $periode          = $objPHPExcel->getActiveSheet()->getCell('A'.$n)->getCalculatedValue();
                          $icustomer        = $objPHPExcel->getActiveSheet()->getCell('B'.$n)->getCalculatedValue();
                          $vtarget          = $objPHPExcel->getActiveSheet()->getCell('C'.$n)->getCalculatedValue();
                          $ntarget          = $objPHPExcel->getActiveSheet()->getCell('D'.$n)->getCalculatedValue();
                          $que=$this->db->query(" SELECT distinct a.i_customer, a.i_salesman, a.i_area, b.i_city 
                                                  FROM tr_customer_salesman a 
                                                  INNER JOIN tr_customer b ON (a.i_customer = b.i_customer)
                                                  WHERE e_periode = '$periode' AND a.i_customer = '$icustomer'", false);
                          if($que->num_rows()>0){
                            $tes=$que->row();
                            $isalesman=$tes->i_salesman;
                            $iarea=$tes->i_area;
                            $icity=$tes->i_city;
                          }        

                          $this->mmaster->inseritemcustomer($periode,$icustomer,$vtarget,$ntarget,$isalesman,$iarea,$icity);
                }
                /* $this->mmaster->inseritemcustomercity($periode);
                $this->mmaster->inseritemcustomersales($periode);
                $this->mmaster->inseritemtarget($periode); */
  
                  $q = $this->db->query("select i_periode, i_area, i_salesman,
                                          sum(v_target) as v_target, sum(n_target) as n_target
                                          from tm_target_itemcustomer
                                          where i_periode = '$periode'
                                          group by 1, 2, 3");
              
                  foreach($q->result() as $row){
                    $iarea      = $row->i_area;
                    $isalesman  = $row->i_salesman;
                    $vtarget    = $row->v_target;
                    $nqty       = $row->n_target;
              
                    $this->mmaster->insert_target_sales($periode,$iarea,$isalesman,$vtarget,$nqty);
                  }

				  $r = $this->db->query("select i_periode, i_area, i_city, i_salesman,
                                          sum(v_target) as v_target, sum(n_target) as n_target
                                          from tm_target_itemcustomer
                                          where i_periode = '$periode'
                                          group by 1, 2, 3, 4");
              
                  foreach($r->result() as $row){
                    $iarea      = $row->i_area;
                    $isalesman  = $row->i_salesman;
					$icity  	= $row->i_city;
                    $vtarget    = $row->v_target;
                    $nqty       = $row->n_target;
              
                    $this->mmaster->insert_target_kota($periode,$iarea,$isalesman,$vtarget,$nqty,$icity);
                  }

				  $s = $this->db->query("select i_periode, i_area,
                                          sum(v_target) as v_target, sum(n_target) as n_target
                                          from tm_target_itemcustomer
                                          where i_periode = '$periode'
                                          group by 1, 2");
              
                  foreach($s->result() as $row){
                    $iarea      = $row->i_area;
                    $vtarget    = $row->v_target;
                    $nqty       = $row->n_target;
              
                    $this->mmaster->insert_targetheader($periode,$iarea, $vtarget, $nqty);
                  }

              
			if ( ($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
				$this->load->view('tftarget/vformgagal',$data);
			}else{
				$this->db->trans_commit();
#				$this->db->trans_rollback();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
          $data['inomor'] = $periode;
					$pesan='Input/Update Target Customer'.$periode;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );
				$this->load->view('tftarget/vformsukses',$data);
			}			
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
