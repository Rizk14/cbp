<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu505') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('listsjpreceive');
			$data['dfrom'] = '';
			$data['dto'] = '';
			$data['isjp'] = '';
			$this->load->view('listsjpreceive/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu505') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari 			= strtoupper($this->input->post('cari', FALSE));
			$dfrom			= $this->input->post('dfrom');
			$dto			= $this->input->post('dto');
			$iarea			= $this->input->post('iarea');
			$sql1			= "";
			$sql2			= "";
			$fapprovefa		= "";
			$fconsigment	= "";
			$iuser			= $this->session->userdata('user_id');
			// $iarea1			= $this->session->userdata('i_area');

			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea	= $this->uri->segment(6);

			$config['base_url'] = base_url() . 'index.php/listsjpreceive/cform/view/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';

			if ($iarea == "NA") {
				$query = $this->db->query(" select a.*, b.e_area_name, c.f_spmb_consigment from tm_sjp a, tr_area b, tm_spmb c
											where a.i_area=b.i_area and a.i_spmb=c.i_spmb
											and (upper(a.i_sjp) like '%$cari%' or upper(a.i_sjp_old) like '%$cari%')
											and a.d_sjp_receive >= to_date('$dfrom','dd-mm-yyyy') AND
											a.d_sjp_receive <= to_date('$dto','dd-mm-yyyy') and not a.d_sjp_receive is null $sql1 $sql2
											ORDER BY a.i_area, a.i_sjp desc", false);
			} else {
				$query = $this->db->query(" select a.*, b.e_area_name, c.f_spmb_consigment from tm_sjp a, tr_area b, tm_spmb c
											where a.i_area=b.i_area and a.i_spmb=c.i_spmb
											and (upper(a.i_sjp) like '%$cari%' or upper(a.i_sjp_old) like '%$cari%')
											and a.i_area='$iarea' and
											a.d_sjp_receive >= to_date('$dfrom','dd-mm-yyyy') AND
											a.d_sjp_receive <= to_date('$dto','dd-mm-yyyy') and not a.d_sjp_receive is null $sql1 $sql2
											ORDER BY a.i_area, a.i_sjp desc", false);
			}

			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '9999';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpreceive');

			$this->load->model('listsjpreceive/mmaster');

			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
			// $data['iarea1']	= $iarea1;
			$data['isjp']   = '';
			$data['fapprovefa']   = '';
			$data['fconsigment']   = '';
			$data['total_rows'] = $query->num_rows();
			$data['isi']	= $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari, $sql1, $sql2);
			$data['list_area'] = $this->db->query(" SELECT i_area, e_area_name FROM tr_area WHERE i_area IN (SELECT i_area FROM tm_user_area WHERE i_user = '$iuser') ORDER BY i_area ");

			$this->logger->writenew('Membuka Data SJP Receive Area ' . $iarea . ' Periode:' . $dfrom . ' s/d ' . $dto);

			$this->load->view('listsjpreceive/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu505') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			// $freceive	= $this->input->post('freceive');
			$fapprovefa	= $this->input->post('fapprovefa');
			$fconsigment	= $this->input->post('fconsigment');
			$iuser			= $this->session->userdata('user_id');
			// $iarea1			= $this->session->userdata('iarea1');

			if ($fapprovefa == "0") {
				$sql1	= "AND d_approve IS NULL ";
			} else if ($fapprovefa == "1") {
				$sql1	= "AND NOT d_approve IS NULL";
			} else {
				$sql1	= "";
			}

			if ($fconsigment == "0") {
				$sql2	= "AND f_spmb_consigment = 'f' ";
			} else if ($fconsigment == "1") {
				$sql2	= "AND f_spmb_consigment = 't' ";
			} else {
				$sql2	= "";
			}

			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea	= $this->uri->segment(6);

			$config['base_url'] = base_url() . 'index.php/listsjpreceive/cform/view/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';

			if ($iarea == "NA") {
				$query = $this->db->query(" select a.*, b.e_area_name, c.f_spmb_consigment from tm_sjp a, tr_area b, tm_spmb c
											where a.i_area=b.i_area and a.i_spmb=c.i_spmb
											and (upper(a.i_sjp) like '%$cari%' or upper(a.i_sjp_old) like '%$cari%')
											and a.d_sjp_receive >= to_date('$dfrom','dd-mm-yyyy') AND
											a.d_sjp_receive <= to_date('$dto','dd-mm-yyyy') and not a.d_sjp_receive is null $sql1 $sql2
											ORDER BY a.i_area, a.i_sjp desc ", false);
			} else {
				$query = $this->db->query(" select a.*, b.e_area_name, c.f_spmb_consigment from tm_sjp a, tr_area b, tm_spmb c
											where a.i_area=b.i_area and a.i_spmb=c.i_spmb
											and (upper(a.i_sjp) like '%$cari%' or upper(a.i_sjp_old) like '%$cari%')
											and a.i_area='$iarea' and
											a.d_sjp_receive >= to_date('$dfrom','dd-mm-yyyy') AND
											a.d_sjp_receive <= to_date('$dto','dd-mm-yyyy') and not a.d_sjp_receive is null $sql1 $sql2
											ORDER BY a.i_area, a.i_sjp desc", false);
			}

			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '9999';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpreceive');

			$this->load->model('listsjpreceive/mmaster');

			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			// $data['iarea1']		= $iarea1;
			$data['fapprovefa']	= $fapprovefa;
			$data['fconsigment'] = $fconsigment;
			$data['total_rows'] = $query->num_rows();
			$data['isjp']   	= '';
			$data['isi']		= $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari, $sql1, $sql2);
			$data['list_area'] = $this->db->query(" SELECT i_area, e_area_name FROM tr_area WHERE i_area IN (SELECT i_area FROM tm_user_area WHERE i_user = '$iuser') ORDER BY i_area ");

			$this->load->view('listsjpreceive/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu505') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('listsjpreceive');
			$this->load->view('listsjpreceive/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu505') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari	 	= strtoupper($this->input->post('cari', FALSE));
			$isjp		= $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$dfrom	= $this->uri->segment(6);
			$dto		= $this->uri->segment(7);
			$this->load->model('listsjpreceive/mmaster');
			$this->mmaster->delete($isjp, $iarea);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Menghapus SJP Receive No:' . $isjp . ' Area:' . $iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$config['base_url'] = base_url() . 'index.php/listsjpreceive/cform/view/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';
			$query = $this->db->query(" select a.i_sjp from tm_sjp a, tr_area b
																	where a.i_area=b.i_area
																	and (upper(a.i_sjp) like '%$cari%')
																	and a.i_area='$iarea' and not a.d_sjp_receive is null and
																	a.d_sjp_receive >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_sjp_receive <= to_date('$dto','dd-mm-yyyy')", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpreceive');
			$this->load->model('listsjpreceive/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari);
			$this->load->view('listsjpreceive/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu505') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/listsjpreceive/cform/area/index/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listsjpreceive/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('listsjpreceive/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu505') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/listsjpreceive/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listsjpreceive/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('listsjpreceive/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu505') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('listsjpreceive') . " Update";
			if ($this->uri->segment(4) != '') {
				$isjp	= $this->uri->segment(4);
				$iarea = $this->uri->segment(5);
				$dfrom = $this->uri->segment(6);
				$dto 	= $this->uri->segment(7);
				$areah = $this->session->userdata('i_area');
				$data['areah'] = $areah;
				$data['isjp'] = $isjp;
				$data['iarea'] = $iarea;
				$data['dfrom'] = $dfrom;
				$data['dto']	= $dto;
				$query 	= $this->db->query("select * from tm_sjp_item where i_sjp = '$isjp' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows();
				$this->load->model('listsjpreceive/mmaster');
				$data['isi'] = $this->mmaster->baca($isjp, $iarea);
				$data['detail'] = $this->mmaster->bacadetail($isjp, $iarea);
				$this->load->view('listsjpreceive/vmainform', $data);
			} else {
				$this->load->view('listsjpreceive/vinsert_fail', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$isj	      = $this->input->post('isj', TRUE);
			$dsjreceive = $this->input->post('dreceive', TRUE);
			if ($dsjreceive != '') {
				$tmp = explode("-", $dsjreceive);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dsjreceive = $th . "-" . $bl . "-" . $hr;
				$thbl	  = substr($th, 2, 2) . $bl;
				$tmpsj	= explode("-", $isj);
				$firstsj = $tmpsj[0];
				$lastsj	= $tmpsj[2];
				$newsj	= $firstsj . "-" . $thbl . "-" . $lastsj;
			}
			$dsj = $this->input->post('dsj', TRUE);
			if ($dsj != '') {
				$tmp = explode("-", $dsj);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dsj = $th . "-" . $bl . "-" . $hr;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$isjold		= $this->input->post('isjold', TRUE);
			$vspbnetto = $this->input->post('vsj', TRUE);
			$vspbnetto = str_replace(',', '', $vspbnetto);
			$vsjrec   = $this->input->post('vsjrec', TRUE);
			$vsjrec   = str_replace(',', '', $vsjrec);

			$jml	    = $this->input->post('jml', TRUE);
			$gaono = true;
			for ($i = 1; $i <= $jml; $i++) {
				$cek = $this->input->post('chk' . $i, TRUE);
				if ($cek == 'on') {
					$gaono = false;
				}
				if (!$gaono) break;
			}
			if (!$gaono) {
				$this->db->trans_begin();
				$this->load->model('listsjpreceive/mmaster');
				$istore	  			= $this->input->post('istore', TRUE);
				if ($istore == 'AA') {
					$istorelocation		= '01';
				} else {
					$istorelocation		= '00';
				}
				$istorelocationbin	= '00';
				$this->mmaster->updatesjheader($isj, $iarea, $isjold, $dsjreceive, $vspbnetto, $vsjrec);
				for ($i = 1; $i <= $jml; $i++) {
					$cek = $this->input->post('chk' . $i, TRUE);
					$iproduct		= $this->input->post('iproduct' . $i, TRUE);
					$eproductname	= $this->input->post('eproductname' . $i, TRUE);
					$iproductgrade	= 'A';
					$iproductmotif	= $this->input->post('motif' . $i, TRUE);
					$ndeliver		= $this->input->post('ndeliver' . $i, TRUE);
					$ndeliver 		= str_replace(',', '', $ndeliver);
					$nreceive		= $this->input->post('nreceive' . $i, TRUE);
					$nreceive		= str_replace(',', '', $nreceive);
					$nasal  	= $this->input->post('nasal' . $i, TRUE);
					$nasal  	= str_replace(',', '', $nasal);
					if ($ndeliver == '') $ndeliver = $nreceive;

					$this->mmaster->deletesjdetail($isj, $iarea, $iproduct, $iproductgrade, $iproductmotif);

					$th = substr($dsjreceive, 0, 4);
					$bl = substr($dsjreceive, 5, 2);
					$emutasiperiode = $th . $bl;
					$tra = $this->mmaster->deletetrans($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $isj, $nasal, $eproductname);
					$this->mmaster->updatemutasi01($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $nasal, $emutasiperiode);
					$this->mmaster->updateic01($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $nasal);
					if ($cek == 'on') {
						$eproductname	= $this->input->post('eproductname' . $i, TRUE);
						$vunitprice		= $this->input->post('vproductmill' . $i, TRUE);
						$vunitprice		= str_replace(',', '', $vunitprice);
						$eremark  		= $this->input->post('eremark' . $i, TRUE);
						if ($eremark == '') $eremark = null;
						#						  if($nreceive>0){
						$this->mmaster->insertsjdetail(
							$iproduct,
							$iproductgrade,
							$iproductmotif,
							$eproductname,
							$nreceive,
							$ndeliver,
							$vunitprice,
							$isj,
							$dsj,
							$iarea,
							$istore,
							$istorelocation,
							$istorelocationbin,
							$eremark,
							$i
						);
						$trans = $this->mmaster->lasttrans($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin);
						if (isset($trans)) {
							foreach ($trans as $itrans) {
								$q_aw = $itrans->n_quantity_awal;
								$q_ak = $itrans->n_quantity_akhir;
								$q_in = $itrans->n_quantity_in;
								$q_out = $itrans->n_quantity_out;
								break;
							}
						} else {
							$trans = $this->mmaster->qic($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin);
							if (isset($trans)) {
								foreach ($trans as $itrans) {
									$q_aw = $itrans->n_quantity_stock;
									$q_ak = $itrans->n_quantity_stock;
									$q_in = 0;
									$q_out = 0;
									break;
								}
							} else {
								$q_aw = 0;
								$q_ak = 0;
								$q_in = 0;
								$q_out = 0;
							}
						}
						$this->mmaster->inserttrans1($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $eproductname, $isj, $q_in, $q_out, $nreceive, $q_aw, $q_ak, $tra);
						$ada = $this->mmaster->cekmutasi2($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $emutasiperiode);
						if ($ada == 'ada') {
							$this->mmaster->updatemutasi1($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $nreceive, $emutasiperiode);
						} else {
							$this->mmaster->insertmutasi1($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $nreceive, $emutasiperiode);
						}
						if ($this->mmaster->cekic($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin)) {
							$this->mmaster->updateic1($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $nreceive, $q_ak);
						} else {
							$this->mmaster->insertic1($iproduct, $iproductgrade, $iproductmotif, $istore, $istorelocation, $istorelocationbin, $eproductname, $nreceive);
						}
						#						  }
					}
				}
				$sjnew = 0;
				if (($this->db->trans_status() === FALSE)) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();

					$sess = $this->session->userdata('session_id');
					$id = $this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if (pg_num_rows($rs) > 0) {
						while ($row = pg_fetch_assoc($rs)) {
							$ip_address	  = $row['ip_address'];
							break;
						}
					} else {
						$ip_address = 'kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while ($row = pg_fetch_assoc($query)) {
						$now	  = $row['c'];
					}
					$pesan = 'Update SJP Receive No ' . $isj . ' Area:' . $iarea;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now, $pesan);

					$data['sukses']			= true;
					$data['inomor']			= $newsj;
					$this->load->view('nomor', $data);
				}
			}
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu207') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari = strtoupper($this->input->post('cari', FALSE));
			$baris = strtoupper($this->input->post('baris', FALSE));
			if ($baris == '') $baris = $this->uri->segment(4);
			if ($this->uri->segment(5) != 'x01') {
				if ($cari == '') $cari = $this->uri->segment(5);
				$config['base_url'] = base_url() . 'index.php/listsjpreceive/cform/product/' . $baris . '/' . $cari . '/';
			} else {
				$config['base_url'] = base_url() . 'index.php/listsjpreceive/cform/product/' . $baris . '/x01/';
			}
			$query = $this->db->query(" select a.i_product, a.e_product_name, b.v_product_retail, c.i_product_motif, c.e_product_motifname 
                                  from tr_product a, tr_product_price b, tr_product_motif c
                                  where a.i_product=b.i_product and b.i_price_group='00'
                                  and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                                  and a.i_product=c.i_product", false); # and a.i_product_status<>'4'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('listsjpreceive/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris'] = $this->uri->segment(4);
			$data['isi'] = $this->mmaster->bacaproduct($config['per_page'], $this->uri->segment(6), $cari);
			$this->load->view('listsjpreceive/vlistproduct', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
