<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu473')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title1'] = $this->lang->line('exp-bonkeluar');
			$data['page_title2'] = $this->lang->line('exp-bonmasuk');
			$data['dfrom']	= '';
			$data['dto']	= '';
			//$data['iperiode']	= '';
			//$data['iarea']	  = '';
			$this->load->view('exp-bonkm/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function exportkeluar()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu409')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-bonkm/mmaster');
			$datefrom = $this->input->post('datefrom');
			$dateto	  = $this->input->post('dateto');

			$dfrom=date('d-m-Y',strtotime($datefrom));
			$dto=date('d-m-Y',strtotime($dateto));

			$this->db->select(" * from tm_bk
                where d_bk >= to_date('$dfrom','dd-mm-yyyy') and d_bk <= to_date('$dto','dd-mm-yyyy')
	                    order by d_bk, i_bk",false);

			$query=$this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Bon Keluar")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:A4'
				);
        
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
			
#        if($iarea=='00'){
#  				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);
#        }	

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'BON KELUAR');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,12,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$dfrom.' - '.$dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,12,3);
#        		$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No : '.$no);
#				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,4,12,4);
				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Bon Keluar');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tanggal Bon Keluar');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Total');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				
        $objPHPExcel->getActiveSheet()->getStyle('A6:E6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');

				$i=7;
				$j=7;
				$xarea='';
				$saldo=0;

				foreach($query->result() as $row){
					
					if($row->i_area!=$xarea)
					{
    				$j=7;
            if($i!=7){
              $i++;
				      $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'No');
				      $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      )

					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, 'Bon Keluar');
				      $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Tanggal Bon Keluar');
				      $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Keterangan');
				      $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
				      $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, 'Total');
				      $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
					      array(
						      'borders' => array(
							      'top' 	=> array('style' => Style_Border::BORDER_THIN),
							      'bottom'=> array('style' => Style_Border::BORDER_THIN),
							      'left'  => array('style' => Style_Border::BORDER_THIN),
							      'right' => array('style' => Style_Border::BORDER_THIN)
						      ),
					      )
				      );
            }
            if($i!=7){
              $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':E'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
              $i++;
            }


						$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
					
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->e_remark, Cell_DataType::TYPE_STRING);
						$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						
						$i++;
					}
					$xarea=$row->i_area;
					if($row->f_debet=='f')
					{
						$debet =$row->v_kk;
						$kredit=0;
					}else{
						$kredit=$row->v_kk;
						$debet =0;
					}
					$saldo=$saldo+$debet-$kredit;
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j-6);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_bk, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					if($row->d_bk!=''){
						$dbk=date('d-m-Y',strtotime($row->d_bk));
					}
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $dbk);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->e_remark);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->v_bk);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$i++;
					$j++;
				}
				$x=$i-1;
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => true
					    )
				    ),
				    'A7:E'.$x
			    );
			    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						    'wrap'      => true
					    )
				    ),
				    'K7:E'.$x
			    );
#        }
			}
#      if($iarea=='00'){
#	  		$objPHPExcel->getActiveSheet()->getStyle('A6:M6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB#('FFFFFF00');
#      }else{
  			$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
#      }
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='Bon Keluar - '.$dfrom.'-'.$dto.'.xls';
      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        @unlink('excel/00/'.$nama);
      }
			$objWriter->save('excel/00/'.$nama); 

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
      }
      $pesan='Export Bon Keluar :'.$dfrom.' sampai:'.$dto;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses'] = true;
			$data['inomor']	= "Export Bon Keluar ".$dfrom.' - '.$dto;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu473')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-bonkm/mmaster');
			$datefrom = $this->input->post('datefrom');
			$dateto	  = $this->input->post('dateto');
			$dfrom=date('d-m-Y',strtotime($datefrom));
			$dto=date('d-m-Y',strtotime($dateto));
			}
      
      		$this->db->select(" * from tm_bk
                where d_bk >= to_date('$dfrom','dd-mm-yyyy') and d_bk <= to_date('$dto','dd-mm-yyyy')
	                    order by d_bk, i_bk",false);
		  
			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Bon Keluar")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(28);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'BON KELUAR');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,11,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$$dfrom.' - '.$dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,11,3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:E5'
				);
				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);				
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Bon Keluar');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tanggal Bon Keluar');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			    $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Total');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
								
				$i=6;
				$j=6;
        		$no=0;
				foreach($query->result() as $row)
				{
          		$no++;
          		$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  	array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':E'.$i
				  );         
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $no, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_bk, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->d_bk, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->e_remark, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, number_format($row->v_bk), Cell_DataType::TYPE_STRING);
					$i++;
					$j++;
				}
				$x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      	$nama='exp-bonkeluar '.$dfrom.' - '.$dto.'.xls';
			$objWriter->save('excel/00/'.$nama); 

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
      }
      $pesan='Export Bon Keluar :'.$dfrom.' - '.$dto;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses'] = true;
			$data['inomor']	= "Export Bon Keluar ".$dfrom.' - '.$dto;
			$this->load->view('nomor',$data);
		}
}
?>
