<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu473')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title1'] = $this->lang->line('exp-bonkeluar');
			$data['page_title2'] = $this->lang->line('exp-bonmasuk');
			$data['dfrom']	= '';
			$data['dto']	= '';
			//$data['iperiode']	= '';
			//$data['iarea']	  = '';
			$this->load->view('exp-bonkm/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function exportkeluar()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu473')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-bonkm/mmaster');
			$datefrom = $this->input->post('datefrom');
			$dateto	  = $this->input->post('dateto');

			$dfrom=date('d-m-Y',strtotime($datefrom));
			$dto=date('d-m-Y',strtotime($dateto));

      $this->db->select("* from tm_bk
                		where d_bk >= to_date('$dfrom','dd-mm-yyyy') and d_bk <= to_date('$dto','dd-mm-yyyy')
                		and f_bk_cancel!='t'
	                    order by d_bk, i_bk",false);
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Bon Keluar ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:A3'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(65);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'BON KELUAR');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,5,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', ''.NmPerusahaan.'');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,5,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$dfrom.' - '.$dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,5,3);
				


				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Bon Keluar');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl Bon Keluar');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Total');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Sub Dist');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->getStyle('A6:F6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#DCDCDC');

				$i=7;
				$na=1;
				foreach($query->result() as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':F'.$i
				  );
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $na);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_bk);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_bk);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->e_remark);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->v_bk);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->e_subdist);
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$i++;
					$na++;
				}
        $x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='Bon Keluar '.$dfrom.'-'.$dto.'.xls';
      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        unlink('excel/00/'.$nama);
      }
			$objWriter->save('excel/00/'.$nama); 
      @chmod('excel/00/'.$nama, 0777);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
		  }
		  $pesan='Export Bon Keluar'.$dfrom.'-'.$dto;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );  

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function exportmasuk()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu473')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-bonkm/mmaster');
			$datefrom = $this->input->post('datefrom2');
			$dateto	  = $this->input->post('dateto2');

			$dfrom=date('d-m-Y',strtotime($datefrom));
			$dto=date('d-m-Y',strtotime($dateto));

      $this->db->select("* from tm_bm
                        where d_bm >= to_date('$dfrom','dd-mm-yyyy') and d_bm <= to_date('$dto','dd-mm-yyyy')
                        and f_bm_cancel!='t'
				        order by d_bm, i_bm",false);
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Bon Masuk ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:A3'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(65);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'BON MASUK');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,5,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', ''.NmPerusahaan.'');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,5,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$dfrom.' - '.$dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,5,3);
				


				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Bon Masuk');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl Bon Masuk');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Total');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Sub Dist');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->getStyle('A6:F6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('#DCDCDC');

				$i=7;
				$na=1;
				foreach($query->result() as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':E'.$i
				  );
          $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $na);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_bm);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_bm);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->e_remark);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
         	$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->v_bm);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->e_subdist);
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$i++;
					$na++;
				}
        $x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='Bon Masuk '.$dfrom.'-'.$dto.'.xls';
      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        unlink('excel/00/'.$nama);
      }
			$objWriter->save('excel/00/'.$nama); 
      @chmod('excel/00/'.$nama, 0777);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
		  }
		  $pesan='Export Bon Masuk'.$dfrom.'-'.$dto;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );  

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>