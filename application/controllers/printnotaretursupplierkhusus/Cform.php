<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu579')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/printnotaretursupplierkhusus/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('printnotaretursupplierkhusus');
			$data['cari'] = '';
      		$data['dfrom']= '';
			$data['dto']  = '';
			$data['isi']  = '';
			$this->load->view('printnotaretursupplierkhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu579')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printnotaretursupplierkhusus/cform/supplier/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_supplier",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('printnotaretursupplierkhusus/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('printnotaretursupplierkhusus/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu579')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/printnotaretursupplierkhusus/cform/supplier/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_supplier
									   	where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('printnotaretursupplierkhusus/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printnotaretursupplierkhusus/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		        ($this->session->userdata('menu579')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	  = strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
			$isupplier	= $this->input->post('isupplier');
      if($dfrom=='')$dfrom=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
      if($isupplier=='')$isupplier=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printnotaretursupplierkhusus/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/';
			$query = $this->db->query(" select a.i_bbkretur, a.d_bbkretur from tm_bbkretur a, tr_supplier b
                                  		where a.i_supplier=b.i_supplier 
				  						and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
				  						or upper(a.i_bbkretur) like '%$cari%')
                                  		and a.d_bbkretur >= to_date('$dfrom','dd-mm-yyyy') and a.d_bbkretur <= to_date('$dto','dd-mm-yyyy')
                                  		and a.i_supplier='$isupplier' and a.i_kn is not null",false);
#and (a.d_cetak isnull)
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printnotaretursupplierkhusus');
			$this->load->model('printnotaretursupplierkhusus/mmaster');
      		$data['dfrom']= $dfrom;
			$data['dto']  = $dto;
			$data['isupplier']= $isupplier;
			$data['isi']=$this->mmaster->bacasemua($isupplier,$cari,$config['per_page'],$this->uri->segment(7),$dfrom,$dto);
			$this->load->view('printnotaretursupplierkhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu579')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ibbkretur = $this->uri->segment(4);
			$isupplier = $this->uri->segment(5);
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
			$isupplier	= $this->input->post('isupplier');
      if($isupplier=='')$isupplier=$this->uri->segment(5);
      if($dfrom=='')$dfrom=$this->uri->segment(6);
      if($dto=='')$dto=$this->uri->segment(7);
			$this->load->model('printnotaretursupplierkhusus/mmaster');
	        $data['dfrom']= $dfrom;
			$data['dto']  = $dto;
			$data['isupplier']= $isupplier;
			$data['ibbkretur']=$ibbkretur;
			$data['page_title'] = $this->lang->line('printnotaretursupplierkhusus');
			$data['isi']=$this->mmaster->baca($ibbkretur,$isupplier);
      		$data['detail']=$this->mmaster->bacadetail($ibbkretur,$isupplier);
#      $data['detail']='';

      		$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak Nota Retur Supplier :'.$isupplier.' No:'.$ibbkretur;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printnotaretursupplierkhusus/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu579')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printnotaretursupplierkhusus');
			$this->load->view('printnotaretursupplierkhusus/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu579')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printnotaretursupplierkhusus/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select a.i_bbkretur, a.d_bbkretur from tm_bbkretur a, tr_supplier b
                                  		where a.i_supplier=b.i_supplier 
				  						and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
				  						or upper(a.i_bbkretur) like '%$cari%')
                                  		and a.d_bbkretur >= to_date('$dfrom','dd-mm-yyyy') and a.d_bbkretur <= to_date('$dto','dd-mm-yyyy')
                                  		and a.i_supplier='$isupplier'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('printnotaretursupplierkhusus/mmaster');
			$data['isi']=$this->mmaster->cari($isupplier,$cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('printnotaretursupplierkhusus');
			$data['cari']=$cari;
			$data['ibbkretur']='';
			$data['detail']='';
	 		$this->load->view('printnotaretursupplierkhusus/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
