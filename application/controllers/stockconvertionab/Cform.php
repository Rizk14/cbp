<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu75')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iicconvertion 	= $this->input->post('iicconvertion', TRUE);
			$dicconvertion 	= $this->input->post('dicconvertion', TRUE);
			if($dicconvertion!=''){
				$tmp=explode("-",$dicconvertion);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dicconvertion=$th."-".$bl."-".$hr;
        $thbl=substr($th,2,2).$bl;
        $tehbl=$th.$bl;
			}
			$jml1		= $this->input->post('jml1', TRUE);
			$jml2		= $this->input->post('jml1', TRUE);
			if (
          (isset($dicconvertion) && $dicconvertion != '') && ($iicconvertion == '') &&
          ($jml1>0) && ($jml2>0)
         )
			{
				$this->db->trans_begin();
				$this->load->model('stockconvertionab/mmaster');
				$iicconvertion		= $this->mmaster->runningnumber($thbl);
				$ibbk				= $this->mmaster->runningnumberbbk($tehbl);
				$ibbm				= $this->mmaster->runningnumberbbm($tehbl);
				$dbbk				= $dicconvertion;
				$dbbm				= $dicconvertion;
				$periode    = substr($dicconvertion,6,4).substr($dicconvertion,3,2);
				$istore				    = 'AA';
				$istorelocation		= '01';
				$istorelocationbin= '00';
				$eremark			= 'Konversi Stock';
				$ibbktype			= '04';
				$ibbmtype			= '03';
				$iarea				= '00';
				if($jml1==1){
//=======================================FOR(1)==========================================//          
					for($i=1;$i<=$jml1;$i++){
					  $iproduct			=$this->input->post('iproduct'.$i, TRUE);
					  $iproductgrade	=$this->input->post('iproductgrade'.$i, TRUE);
					  $iproductmotif 	=$this->input->post('iproductmotif'.$i, TRUE);
					  $ficconvertion	='t';
					  $nicconvertion	=$this->input->post('nicconvertion'.$i, TRUE);
					  $eproductname		=$this->input->post('eproductname'.$i, TRUE);
					  $vunitprice		=$this->input->post('vproductretail'.$i, TRUE);
					  $this->mmaster->insertheader($iicconvertion,$dicconvertion,$iproduct,$iproductgrade,
												   $eproductname,$iproductmotif,$ficconvertion,$nicconvertion);
					  $this->mmaster->insertbbkdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nicconvertion,
													  $vunitprice,$iicconvertion,$ibbk,$eremark,$ibbktype,$periode);
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttransbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$iicconvertion,$q_in,$q_out,$nicconvertion,$q_aw,$q_ak);
            $th=substr($dicconvertion,0,4);
            $bl=substr($dicconvertion,5,2);
            $emutasiperiode=$th.$bl;
            if($ibbktype=='05'){            
              if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
              {
                $this->mmaster->updatemutasibbk5($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
              }else{
                $this->mmaster->insertmutasibbk5($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
              }
            }else{
              if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
              {
                $this->mmaster->updatemutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
              }else{
                $this->mmaster->insertmutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
              }
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateicbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$q_ak);
            }else{
              $this->mmaster->inserticbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nicconvertion);
            }
          }
//=======================================FOR(1)==========================================//
					$this->mmaster->insertbbkheader($iicconvertion,$dicconvertion,$ibbk,$dbbk,$ibbktype,$eremark,$iarea);
					$this->mmaster->insertbbmheader($iicconvertion,$dicconvertion,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea);
//=======================================FOR(2)==========================================//
					for($i=1;$i<=$jml2;$i++){
					  $iproduct      =$this->input->post('iproduct'.$i, TRUE);
            $iproductgrade  ='B';
            $iproductmotif  =$this->input->post('iproductmotif'.$i, TRUE);
            $ficconvertion  ='t';
            $nicconvertion  =$this->input->post('nicconvertion'.$i, TRUE);
            $eproductname   =$this->input->post('eproductname'.$i, TRUE);
            $vunitprice   =$this->input->post('vproductretail'.$i, TRUE);
					  $this->mmaster->insertdetail($iicconvertion,$dicconvertion,$iproduct,$iproductgrade,
												   $eproductname,$iproductmotif,$nicconvertion);
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$iicconvertion,$q_in,$q_out,$nicconvertion,$q_aw,$q_ak);
            $th=substr($dicconvertion,0,4);
            $bl=substr($dicconvertion,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
#              $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$q_ak);
              $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$nicconvertion);
            }else{
              $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nicconvertion);
            }
            $this->mmaster->insertbbmdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nicconvertion,
                            $vunitprice,$iicconvertion,$ibbm,$eremark,$ibbmtype,$periode);
//=======================================FOR(2)==========================================//
          }
        }else{
//=============================FOR(1)ELSE======================================================//
					for($i=1;$i<=$jml2;$i++){
					  $iproduct      =$this->input->post('iproduct'.$i, TRUE);
            $iproductgrade  =$this->input->post('iproductgrade'.$i, TRUE);
            $iproductmotif  =$this->input->post('iproductmotif'.$i, TRUE);
            $ficconvertion  ='t';
            $nicconvertion  =$this->input->post('nicconvertion'.$i, TRUE);
            $eproductname   =$this->input->post('eproductname'.$i, TRUE);
            $vunitprice   =$this->input->post('vproductretail'.$i, TRUE);
					  $this->mmaster->insertheader($iicconvertion,$dicconvertion,$iproduct,$iproductgrade,
												   $eproductname,$iproductmotif,$ficconvertion,$nicconvertion);
					  $this->mmaster->insertbbmdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nicconvertion,
													  $vunitprice,$iicconvertion,$ibbm,$eremark,$ibbmtype,$periode);

            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttransbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$iicconvertion,$q_in,$q_out,$nicconvertion,$q_aw,$q_ak);
            $th=substr($dicconvertion,0,4);
            $bl=substr($dicconvertion,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasibbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasibbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateicbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$q_ak);
            }else{
              $this->mmaster->inserticbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nicconvertion);
            }
//=============================FOR(1)ELSE======================================================//
					}
					$this->mmaster->insertbbkheader($iicconvertion,$dicconvertion,$ibbk,$dbbk,$ibbktype,$eremark,$iarea);
					$this->mmaster->insertbbmheader($iicconvertion,$dicconvertion,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea);
          
//=============================FOR(2)ELSE======================================================//					
          for($i=1;$i<=$jml1;$i++){
					  $iproduct      =$this->input->post('iproduct'.$i, TRUE);
            $iproductgrade  ='B';
            $iproductmotif  =$this->input->post('iproductmotif'.$i, TRUE);
            $nicconvertion  =$this->input->post('nicconvertion'.$i, TRUE);
            $eproductname   =$this->input->post('eproductname'.$i, TRUE);
            $vunitprice   =$this->input->post('vproductretail'.$i, TRUE);
					  $this->mmaster->insertbbkdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nicconvertion,
													  $vunitprice,$iicconvertion,$ibbk,$eremark,$ibbktype,$periode);
					  $this->mmaster->insertdetail($iicconvertion,$dicconvertion,$iproduct,$iproductgrade,
												   $eproductname,$iproductmotif,$nicconvertion);
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){              
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttrans4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$iicconvertion,$q_in,$q_out,$nicconvertion,$q_aw,$q_ak);
            $th=substr($dicconvertion,0,4);
            $bl=substr($dicconvertion,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateic4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$q_ak);
            }else{
              $this->mmaster->insertic4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nicconvertion);
            }
					}
//=============================FOR(2)ELSE======================================================//
				}
				if ($this->db->trans_status()===false)
				{
					$this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();

          $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		=  $this->db->query($sql);
			    if($rs->num_rows>0){
				    foreach($rs->result() as $tes){
					    $ip_address	  = $tes->ip_address;
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }

			    $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
			    $data['host']	= $ip_address;
			    $data['uri']	= $this->session->userdata('printeruri');
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Input Konversi Stok No:'.$iicconvertion;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']=true;
					$data['inomor']=$iicconvertion;
					$this->load->view('nomor',$data);
				}
			}
			else
			{

				$data['page_title'] = $this->lang->line('stockconvertionab');
				$data['iicconvertion']='';
				$this->load->model('stockconvertionab/mmaster');
				$data['isi']="";
				$data['jmlitem']="";
				$data['status']='';
				$this->load->view('stockconvertionab/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu75')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('stockconvertionab');
			$this->load->view('stockconvertionab/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu75')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('stockconvertionab')." update";
			$status 	= $this->uri->segment(5);
			$iproduct 	= $this->uri->segment(6);
			if($this->uri->segment(4)!=''){
				$iicconvertion = $this->uri->segment(4);
				$query = $this->db->query("select * from tm_ic_convertionitem where i_ic_convertion = '$iicconvertion'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['iicconvertion'] = $iicconvertion;
				$this->load->model('stockconvertionab/mmaster');
				$data['status']=$status;
				$data['isi']=$this->mmaster->baca($iicconvertion,$iproduct);
				$data['detail']=$this->mmaster->bacadetail($iicconvertion,$iproduct);
		 		$this->load->view('stockconvertionab/vmainform',$data);
			}else{
				$this->load->view('stockconvertionab/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu75')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iicconvertion 	= $this->input->post('iicconvertion', TRUE);
			$dicconvertion 	= $this->input->post('dicconvertion', TRUE);
			$periode    = substr($dicconvertion,6,4).substr($dicconvertion,3,2);
			if($dicconvertion!=''){
				$tmp=explode("-",$dicconvertion);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dicconvertion=$th."-".$bl."-".$hr;
			}
			$jml1		= $this->input->post('jml1', TRUE);
			$jml2		= $this->input->post('jml1', TRUE);
			if ((isset($dicconvertion) && $dicconvertion != '') && ($iicconvertion != ''))
			{
				$this->db->trans_begin();
				$this->load->model('stockconvertionab/mmaster');
				$istore				= 'AA';
				$istorelocation		= '01';
				$istorelocationbin	= '00';
				$eremark			= 'Konversi Stock';
				$ibbktype			= '04';
				$ibbmtype			= '03';
				$iarea				= '00';
				$ibbk				= $this->input->post('ibbk', TRUE);
				$ibbm				= $this->input->post('ibbm', TRUE);
				if($jml1==1){
					for($i=1;$i<=$jml1;$i++){
					  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
					  $iproductgrade	= $this->input->post('iproductgrade'.$i, TRUE);
					  $iproductmotif 	= $this->input->post('iproductmotif'.$i, TRUE);
					  $ficconvertion	= 't';
					  $nicconvertion	= $this->input->post('nicconvertion'.$i, TRUE);
					  $nicconvertionx	= $this->input->post('nicconvertion'.$i.'x', TRUE);
					  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
					  $vunitprice		  = $this->input->post('vproductretail'.$i, TRUE);
					  $this->mmaster->updateheader($iicconvertion,$dicconvertion,$iproduct,$iproductgrade,
												   $eproductname,$iproductmotif,$ficconvertion,$nicconvertion);
#            if($nicconvertionx!='' || $nicconvertionx==NULL){
					  $this->mmaster->updatebbkdetail($iproduct, $iproductgrade, $iicconvertion, $iproductmotif,$nicconvertion,$ficconvertion,$istore,
												   $istorelocation,$istorelocationbin,$nicconvertionx);
					  $this->mmaster->insertbbkdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nicconvertion,
													  $vunitprice,$iicconvertion,$ibbk,$eremark,$ibbktype,$periode);
############
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttransbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$iicconvertion,$q_in,$q_out,$nicconvertion,$q_aw,$q_ak);
            $th=substr($dicconvertion,0,4);
            $bl=substr($dicconvertion,5,2);
            $emutasiperiode=$th.$bl;
            if($ibbktype=='05'){            
              if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
              {
                $this->mmaster->updatemutasibbk5($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
              }else{
                $this->mmaster->insertmutasibbk5($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
              }
            }else{
              if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
              {
                $this->mmaster->updatemutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);

              }else{
                $this->mmaster->insertmutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
              }
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateicbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$q_ak);
            }else{
              $this->mmaster->inserticbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nicconvertion);
            }
############
					}
          
					for($i=1;$i<=$jml2;$i++){
					  $iproduct     =$this->input->post('iproduct'.$i, TRUE);
            $iproductgrade  ='B';
            $iproductmotif  =$this->input->post('iproductmotif'.$i, TRUE);
            $nicconvertion  =$this->input->post('nicconvertion'.$i, TRUE);
            $eproductname   =$this->input->post('eproductname'.$i, TRUE);
            $vunitprice   =$this->input->post('vproductretail'.$i, TRUE);
            $ficconvertion  = 'f';
            $nicconvertionx = $this->input->post('nicconvertion'.$i.'x', TRUE);
					  $this->mmaster->updatebbmdetail($iproduct, $iproductgrade, $iicconvertion, $iproductmotif,$nicconvertion,$ficconvertion,$istore,
													  $istorelocation,$istorelocationbin,$nicconvertionx);
					  $this->mmaster->insertbbmdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nicconvertion,
													  $vunitprice,$iicconvertion,$ibbm,$eremark,$ibbmtype,$periode);
#					  $this->mmaster->updatedetail($iproduct,$iproductgrade,$iproductmotif,$nicconvertion,$istore,$istorelocation,$istorelocationbin);
					  $this->mmaster->insertdetail($iicconvertion,$dicconvertion,$iproduct,$iproductgrade,
												   $eproductname,$iproductmotif,$nicconvertion);
############
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$iicconvertion,$q_in,$q_out,$nicconvertion,$q_aw,$q_ak);
            $th=substr($dicconvertion,0,4);

            $bl=substr($dicconvertion,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$q_ak);
            }else{
              $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname);
            }
############
					}
				}else{
					for($i=1;$i<=$jml2;$i++){
            $iproduct     =$this->input->post('iproduct'.$i, TRUE);
            $iproductgrade  ='B';
            $iproductmotif  =$this->input->post('iproductmotif'.$i, TRUE);
            $nicconvertion  =$this->input->post('nicconvertion'.$i, TRUE);
            $eproductname   =$this->input->post('eproductname'.$i, TRUE);
            $vunitprice   =$this->input->post('vproductretail'.$i, TRUE);
            $ficconvertion	= 'f';
            $nicconvertionx	= $this->input->post('nicconvertion'.$i.'x', TRUE);
					  $this->mmaster->updateheader($iicconvertion,$dicconvertion,$iproduct,$iproductgrade,
												   $eproductname,$iproductmotif,$ficconvertion,$nicconvertion);
					  $this->mmaster->updatebbmdetail($iproduct, $iproductgrade, $iicconvertion, $iproductmotif,$nicconvertion,$ficconvertion,$istore,
													  $istorelocation,$istorelocationbin,$nicconvertionx);
					  $this->mmaster->insertbbmdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nicconvertion,
													  $vunitprice,$iicconvertion,$ibbm,$eremark,$ibbmtype,$periode);
############
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttransbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$iicconvertion,$q_in,$q_out,$nicconvertion,$q_aw,$q_ak);
            $th=substr($dicconvertion,0,4);
            $bl=substr($dicconvertion,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasibbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasibbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateicbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$q_ak);
            }else{
              $this->mmaster->inserticbbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nicconvertion);
            }
############
					}
					for($i=1;$i<=$jml1;$i++){
					  $iproduct     =$this->input->post('iproduct'.$i, TRUE);
            $iproductgrade  ='B';
            $iproductmotif  =$this->input->post('iproductmotif'.$i, TRUE);
            $nicconvertion  =$this->input->post('nicconvertion'.$i, TRUE);
            $eproductname   =$this->input->post('eproductname'.$i, TRUE);
            $vunitprice   =$this->input->post('vproductretail'.$i, TRUE);
            $ficconvertion  = 'f';
            $nicconvertionx = $this->input->post('nicconvertion'.$i.'x', TRUE);
					  $this->mmaster->updatebbkdetail($iproduct, $iproductgrade, $iicconvertion, $iproductmotif,$nicconvertion,$ficconvertion,$istore,
												      $istorelocation,$istorelocationbin,$nicconvertionx);
					  $this->mmaster->insertbbkdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nicconvertion,
													  $vunitprice,$iicconvertion,$ibbk,$eremark,$ibbktype,$periode);
############
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){              
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttrans4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$iicconvertion,$q_in,$q_out,$nicconvertion,$q_aw,$q_ak);
            $th=substr($dicconvertion,0,4);
            $bl=substr($dicconvertion,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateic4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nicconvertion,$q_ak);
            }else{
              $this->mmaster->insertic4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nicconvertion);

            }
############
					  $this->mmaster->insertdetail($iicconvertion,$dicconvertion,$iproduct,$iproductgrade,
												   $eproductname,$iproductmotif,$nicconvertion);
					}
				}
				if ($this->db->trans_status()===false)
				{
					$this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();

          $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		=  $this->db->query($sql);
			    if($rs->num_rows>0){
				    foreach($rs->result() as $tes){
					    $ip_address	  = $tes->ip_address;
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }

			    $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
			    $data['host']	= $ip_address;
			    $data['uri']	= $this->session->userdata('printeruri');
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Update Konversi Stok No:'.$iicconvertion;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']=true;					
					$data['inomor']=$iicconvertion;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu75')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iicconvertion	= $this->input->post('iicconvertiondelete', TRUE);
			$this->load->model('stockconvertionab/mmaster');
			$this->mmaster->delete($iicconvertion);

      $sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus Konversi Stok No:'.$iicconvertion;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('stockconvertion');
			$data['iicconvertion']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['status']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('stockconvertionab/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu75')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iicconvertion		= $this->uri->segment(4);
			$iproduct			= $this->uri->segment(5);
  			$iproductgrade		= $this->uri->segment(6);
			$iproductmotif		= $this->uri->segment(7);
			$nicconvertion		= $this->uri->segment(8);
			$ficconvertion		= $this->uri->segment(9);
			$istore				= 'AA';
			$istorelocation		= '01';
			$istorelocationbin	= '00';
			$this->load->model('stockconvertionab/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $iicconvertion, $iproductmotif,$nicconvertion,$ficconvertion,$istore,
										 $istorelocation,$istorelocationbin);
			$data['page_title'] = $this->lang->line('stockconvertionab');
			$query = $this->db->query("select * from tm_ic_convertionitem
									   where i_ic_convertion = '$iicconvertion'");
			$data['jmlitem'] = $query->num_rows(); 				
			$data['iicconvertion'] = $iicconvertion;
			$this->load->model('stockconvertionab/mmaster');
			$data['isi']=$this->mmaster->baca($iicconvertion);
			$data['detail']=$this->mmaster->bacadetail($iicconvertion);
			$data['status']='';
	 		$this->load->view('stockconvertionab/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu75')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iuser   = $this->session->userdata('user_id');
			$cari = $this->input->post('cari', FALSE);
			if($cari=='') $cari=$this->uri->segment(6);
			if($cari=='index') $cari='';
			$cari = strtoupper($cari);		
			$data['cari']=$cari;
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['baris2']=$this->uri->segment(5);
			$baris2=$this->uri->segment(5);
			if($cari==''){
				$config['base_url'] = base_url().'index.php/stockconvertionab/cform/product/'.$baris.'/'.$baris2.'/index/';
			}else{
				$config['base_url'] = base_url().'index.php/stockconvertionab/cform/product/'.$baris.'/'.$baris2.'/'.$cari.'/index/';
			}
			$query = $this->db->query("select * from f_listproduct_a('$iuser','%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if($cari=='')
				$config['cur_page'] = $this->uri->segment(7);
			else
				$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('stockconvertionab/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($iuser,$cari,$config['per_page'],$config['cur_page']);
			$this->load->view('stockconvertionab/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu75')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iuser   = $this->session->userdata('user_id');
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['baris2']=$this->uri->segment(5);
			$baris2=$this->uri->segment(5);
			$cari = $this->input->post('cari', FALSE);
			if($cari=='') $cari=$this->uri->segment(6);
			if($cari=='index') $cari='';
			$cari = strtoupper($cari);
			$data['cari']=$cari;
			$config['base_url'] = base_url().'index.php/stockconvertionab/cform/product/'.$baris.'/'.$baris2.'/'.$cari.'/index/';
			$query = $this->db->query("select * from f_listproduct_a('$iuser','%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('stockconvertionab/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($iuser,$cari,$config['per_page'],$this->uri->segment(8));
			$data['baris']=$baris;
			$this->load->view('stockconvertionab/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu75')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iuser   = $this->session->userdata('user_id');
			$cari = $this->input->post('cari', FALSE);
			if($cari=='') $cari=$this->uri->segment(6);
			if($cari=='index') $cari='';			
			$cari = strtoupper($cari);
			$data['cari']=$cari;
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['baris2']=$this->uri->segment(5);
			$baris2=$this->uri->segment(5);
			if($cari==''){
				$config['base_url'] = base_url().'index.php/stockconvertionab/cform/product2/'.$baris.'/'.$baris2.'/index/';
			}else{
				$config['base_url'] = base_url().'index.php/stockconvertionab/cform/product2/'.$baris.'/'.$baris2.'/'.$cari.'/index/';
			}
			$query = $this->db->query("select * from f_listproduct_ab('$iuser','%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if($cari=='')
				$config['cur_page'] = $this->uri->segment(7);
			else
				$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('stockconvertionab/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($iuser,$cari,$config['per_page'],$config['cur_page']);
			$this->load->view('stockconvertionab/vlistproduct2', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu75')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iuser   = $this->session->userdata('user_id');
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['baris2']=$this->uri->segment(5);
			$baris2=$this->uri->segment(5);
			$cari = $this->input->post('cari', FALSE);
			if($cari=='') $cari=$this->uri->segment(6);
			$cari = strtoupper($cari);
			$data['cari']=$cari;
			$config['base_url'] = base_url().'index.php/stockconvertionab/cform/product2/'.$baris.'/'.$baris2.'/'.$cari.'/index/';
			$query = $this->db->query("select * from f_listproduct_ab('$iuser','%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('stockconvertionab/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($iuser,$cari,$config['per_page'],$this->uri->segment(8));
			$data['baris']=$baris;
			$this->load->view('stockconvertionab/vlistproduct2', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu75')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iuser   = $this->session->userdata('user_id');
			$cari = $this->input->post('cari', FALSE);
			if($cari=='') $cari=$this->uri->segment(6);
			if($cari=='index') $cari='';			
			$cari = strtoupper($cari);
			$data['cari']=$cari;
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['baris2']=$this->uri->segment(5);
			$baris2=$this->uri->segment(5);
			if($cari==''){
				$config['base_url'] = base_url().'index.php/stockconvertionab/cform/productupdate/'.$baris.'/'.$baris2.'/index/';
			}else{
				$config['base_url'] = base_url().'index.php/stockconvertionab/cform/productupdate/'.$baris.'/'.$baris2.'/'.$cari.'/index/';
			}
			$query = $this->db->query("select * from f_listproduct_a('$iuser','%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if($cari=='')
				$config['cur_page'] = $this->uri->segment(7);
			else
				$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('stockconvertionab/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($iuser,$cari,$config['per_page'],$config['cur_page']);
			$this->load->view('stockconvertionab/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productupdate2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu75')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			if($cari=='') $cari=$this->uri->segment(6);
			if($cari=='index') $cari='';			
			$cari = strtoupper($cari);
			$data['cari']=$cari;
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['baris2']=$this->uri->segment(5);
			$baris2=$this->uri->segment(5);
			$iuser   = $this->session->userdata('user_id');
			if($cari==''){
				$config['base_url'] = base_url().'index.php/stockconvertionab/cform/productupdate2/'.$baris.'/'.$baris2.'/index/';
			}else{
				$config['base_url'] = base_url().'index.php/stockconvertionab/cform/productupdate2/'.$baris.'/'.$baris2.'/'.$cari.'/index/';
			}
			$query = $this->db->query("select * from f_listproduct_ab('$iuser','%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if($cari=='')
				$config['cur_page'] = $this->uri->segment(7);
			else
				$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('stockconvertionab/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($iuser,$cari,$config['per_page'],$config['cur_page']);
			$this->load->view('stockconvertionab/vlistproductupdate2', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
