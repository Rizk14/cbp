<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu169')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listrrkh');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listrrkh/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu169')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listrrkh/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_salesman_name, c.e_area_name from tm_rrkh a, tr_salesman b, tr_area c
										where a.i_area='$iarea' and
										a.i_salesman=b.i_salesman and
										a.i_area=c.i_area and
										a.d_rrkh >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_rrkh <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('listrrkh/mmaster');
			$data['page_title'] = $this->lang->line('listrrkh');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data RRKH Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listrrkh/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu169')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listrrkh');
			$this->load->view('listrrkh/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu169')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dto			= $this->uri->segment(8);
			$dfrom		= $this->uri->segment(7);
			$iarea		= $this->uri->segment(6);
			$isalesman= $this->uri->segment(5);
			$drrkh		= $this->uri->segment(4);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$this->load->model('listrrkh/mmaster');
			$this->mmaster->delete($drrkh,$isalesman,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus RRKH Area '.$iarea.' tanggal:'.$drrkh.' Salesman '.$isalesman;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$config['base_url'] = base_url().'index.php/listrrkh/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_salesman_name, c.e_area_name from tm_rrkh a, tr_salesman b, tr_area c
										where a.i_area='$iarea' and
										a.i_salesman=b.i_salesman and
										a.i_area=c.i_area and
										a.d_rrkh >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_rrkh <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('listrrkh/mmaster');
			$data['page_title'] = $this->lang->line('listrrkh');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listrrkh/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu169')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($cari=='') $cari	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/listrrkh/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_salesman_name, c.e_area_name from tm_rrkh a, tr_salesman b, tr_area c
										where a.i_area='$iarea' and
										a.i_salesman=b.i_salesman and
										a.i_area=c.i_area and
										a.d_rrkh >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_rrkh <= to_date('$dto','dd-mm-yyyy')
										and (upper(a.i_area) like '%$cari%' or upper(c.e_area_name) like '%$cari%' or upper(a.i_salesman) like '%$cari%' or upper(b.e_salesman_name)  
                    like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('listrrkh/mmaster');
			$data['page_title'] = $this->lang->line('listrrkh');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->cariperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listrrkh/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu263')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$keyword = strtoupper($this->uri->segment(7));	
			$config['base_url'] = base_url().'index.php/listrrkh/cform/cariperpages/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$keyword.'/';
			$query = $this->db->query(" select a.*, b.e_salesman_name, c.e_area_name from tm_rrkh a, tr_salesman b, tr_area c
										where a.i_area='$iarea' and
										a.i_salesman=b.i_salesman and
										a.i_area=c.i_area and
										a.d_rrkh >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_rrkh <= to_date('$dto','dd-mm-yyyy')
										and (upper(a.i_area) like '%$keyword%' or upper(c.e_area_name) like '%$keyword%' or upper(a.i_salesman) like '%$keyword%' or upper
                    (b.e_salesman_name) like '%$keyword%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('listrrkh/mmaster');
			$data['page_title'] = $this->lang->line('listrrkh');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$keyword);
			$this->load->view('listrrkh/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu169')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listrrkh/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
        else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }
												   
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listrrkh/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listrrkh/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu169')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listrrkh/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
									   	
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listrrkh/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listrrkh/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
