<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu137')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_girodgu');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listgirodgu/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu137')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier	= $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listgirodgu/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select * from tm_giro_dgu a 
										inner join tr_supplier on(a.i_supplier=tr_supplier.i_supplier)
										where (upper(a.i_giro) like '%$cari%' or upper(a.i_pv) like '%$cari%' 
										or upper(a.i_supplier) like '%$cari%')
										and a.i_supplier='$isupplier' and
										a.d_giro >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_giro <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_girodgu');
			$this->load->model('listgirodgu/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Giro DGU Supplier '.$isupplier.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listgirodgu/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu137')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listgirodgu');
			$this->load->view('listgirodgu/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu137')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$igiro		= $this->uri->segment(4);
			$ipv			= $this->uri->segment(5);
			$isupplier= $this->uri->segment(6);
			$dfrom		= $this->uri->segment(7);
			$dto			= $this->uri->segment(8);
			$this->load->model('listgirodgu/mmaster');
			$this->mmaster->delete($igiro,$ipv);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus Giro DGU No:'.$igiro.' Supplier:'.$isupplier;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$cari		= strtoupper($this->input->post('cari'));
			$config['base_url'] = base_url().'index.php/listgirodgu/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select * from tm_giro_dgu a 
										inner join tr_supplier on(a.i_supplier=tr_supplier.i_supplier)
										where (upper(a.i_giro) like '%$cari%' or upper(a.i_pv) like '%$cari%' 
										or upper(a.i_supplier) like '%$cari%')
										and a.i_supplier='$isupplier' and
										a.d_giro >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_giro <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_girodgu');
			$this->load->model('listgirodgu/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listgirodgu/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu137')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listgirodgu/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select * from tm_giro_dgu a 
										inner join tr_supplier on(a.i_supplier=tr_supplier.i_supplier)
										where (upper(a.i_giro) like '%$cari%' or upper(a.i_pv) like '%$cari%' 
										or upper(a.i_supplier) like '%$cari%')
										and a.i_supplier='$isupplier' and
										a.d_giro >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_giro <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_girodgu');
			$this->load->model('listgirodgu/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']		= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listgirodgu/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu137')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listgirodgu/cform/supplier/index/';
			$query = $this->db->query("select * from tr_supplier ",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10'; 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listgirodgu/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('listgirodgu/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu137')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listgirodgu/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%' 
						      	  or upper(e_supplier_name) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listgirodgu/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('listgirodgu/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
