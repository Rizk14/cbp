<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu512')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('closingper');
			$data['iperiode']   = '';
			$this->load->view('closingperiode/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu512')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		) {
			  $data['page_title'] = $this->lang->line('closing');
			  $this->load->view('closingperiode/vinsert_fail',$data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	}
	function pindah()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu512')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode1	= $this->input->post('iperiode1', TRUE);
			$this->load->model('closingperiode/mmaster');
			list($amanopname, $store) = $this->mmaster->cekopname($iperiode1);
      if($amanopname){
        list($amanap, $do) = $this->mmaster->cekap($iperiode1);
        if($amanap){
          list($amanar, $sj) = $this->mmaster->cekar($iperiode1);
          if($amanar){
            $this->db->trans_begin();
            $this->mmaster->pindah($iperiode1);
            if ( ($this->db->trans_status() === FALSE) )
				    {
			        $this->db->trans_rollback();
				    }else{
					    $sess=$this->session->userdata('session_id');
					    $id=$this->session->userdata('user_id');
					    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					    $rs		= pg_query($sql);
					    if(pg_num_rows($rs)>0){
						    while($row=pg_fetch_assoc($rs)){
							    $ip_address	  = $row['ip_address'];
							    break;
						    }
					    }else{
						    $ip_address='kosong';
					    }
					    $query 	= pg_query("SELECT current_timestamp as c");
					    while($row=pg_fetch_assoc($query)){
						    $now	  = $row['c'];
					    }
					    $pesan='Closing Transaksi Periode : '.$iperiode1;
					    $this->load->model('logger');
					    $this->logger->write($id, $ip_address, $now , $pesan );
#    			    $this->db->trans_rollback();
			        $this->db->trans_commit();
				      $data['sukses']			= true;
				      $data['inomor']			= $iperiode1;
				      $this->load->view('nomor',$data);
				    }
#				  }else{
#				    echo 'xxxxxxxx'; die;
				  }else{
				    $data['sj']			= $sj;
    			  $this->load->view('errorclosingar',$data);
				  }
				}else{
				  $data['do']			= $do;
  			  $this->load->view('errorclosingap',$data);
				}
			}else{
			  $data['store']			= $store;
			  $this->load->view('errorclosingopname',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
