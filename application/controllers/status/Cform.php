<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu10')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_status');
			$data['istatus']='';
			$this->load->model('status/mmaster');
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('status/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu10')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$istatus 	= $this->input->post('istatus', TRUE);
			$estatusname 	= $this->input->post('estatusname', TRUE);

			if ($istatus != '' && $estatusname != '')
			{
				$this->load->model('status/mmaster');
				$this->mmaster->insert($istatus,$estatusname);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu10')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_status');
			$this->load->view('status/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu10')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_status')." update";
			if($this->uri->segment(4)){
				$istatus = $this->uri->segment(4);
				$data['istatus'] = $istatus;
				$this->load->model('status/mmaster');
				$data['isi']=$this->mmaster->baca($istatus);
		 		$this->load->view('status/vmainform',$data);
			}else{
				$this->load->view('status/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu10')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$istatus		= $this->input->post('istatus', TRUE);
			$estatusname 	= $this->input->post('estatusname', TRUE);
			$this->load->model('status/mmaster');
			$this->mmaster->update($istatus,$estatusname);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu10')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$istatus	= $this->uri->segment(4);
			$this->load->model('status/mmaster');
			$this->mmaster->delete($istatus);
			$data['page_title'] = $this->lang->line('master_status');
			$data['istatus']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('status/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
