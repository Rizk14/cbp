<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu9')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/seri/cform/index/';
			$cari=$this->input->post('cari');
			$cari=strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_seri
										where upper(i_product_seri) like '%$cari%' 
										or upper(e_product_seriname) like '%$cari%'",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] 	= "Master Seri Barang";
			$data['iseri']			= '';
			$this->load->model('seri/mmaster');
			$data['isi']			= $this->mmaster->bacasemua($config['per_page'],$this->uri->segment(4));
			$this->load->view('seri/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu9')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iseri 	= $this->input->post('iseri', TRUE);
			$e_product_seriname = $this->input->post('e_product_seriname', TRUE);

			if ($iseri != '' && $e_product_seriname != '')
			{
				$this->load->model('seri/mmaster');
				$this->mmaster->insert($iseri,$e_product_seriname);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu9')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('master_seri');
			$this->load->view('seri/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu9')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('master_seri')." update";
			if($this->uri->segment(4)){
				$iseri = $this->uri->segment(4);
				$data['iseri'] = $iseri;
				$this->load->model('seri/mmaster');
				$data['isi']=$this->mmaster->baca($iseri);
		 		$this->load->view('seri/vmainform',$data);
			}else{
				$this->load->view('seri/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu9')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iseri		= $this->input->post('iseri', TRUE);
			$e_product_seriname 	= $this->input->post('e_product_seriname', TRUE);
			$this->load->model('seri/mmaster');
			$this->mmaster->update($iseri,$e_product_seriname);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu9')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iseri		= $this->uri->segment(4);
			$this->load->model('seri/mmaster');
			$this->mmaster->delete($iseri);
			$data['page_title'] = $this->lang->line('master_seri');
			$data['iseri']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('seri/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu11')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$config['base_url'] 	= base_url().'index.php/seri/cform/index/';
			$cari=$this->input->post('cari');
			$cari=strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_seri
										where upper(i_product_seri) like '%$cari%' 
										or upper(e_product_seriname) like '%$cari%'",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('seri/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] 	= "Master Seri Barang";
			$data['iseri']			= '';
	 		$this->load->view('seri/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
