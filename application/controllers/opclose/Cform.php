<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu54')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opclose/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query("	select distinct(z.i_op) from (
                                  select a.i_op from tm_op a 
                                  left join tm_do o on (a.i_op=o.i_op), tr_supplier b, tr_area c
							                    where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.i_area=c.i_area and a.f_op_close='f'
							                    and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
							                    or upper(a.i_op) like '%$cari%' or upper(a.i_reff) like '%$cari%')
                                  union all
                                  select a.i_op from tm_op a 
                                  left join tm_ap x on (a.i_op=x.i_op), tr_supplier b, tr_area c
							                    where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.i_area=c.i_area and a.f_op_close='f'
							                    and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
							                    or upper(a.i_op) like '%$cari%' or upper(a.i_reff) like '%$cari%')
                                  ) as z",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('opclose');
			$this->load->model('opclose/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('opclose/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function closing()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu54')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml = $this->input->post('jml', TRUE);
			$this->db->trans_begin();
			$this->load->model('opclose/mmaster');
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$iop = $this->input->post('op'.$i, TRUE);
					$ireff = $this->input->post('ireff'.$i, TRUE);
					$iarea = $this->input->post('iarea'.$i, TRUE);
					$this->mmaster->updateop($iop);
          $this->mmaster->updatespb($ireff,$iop,$iarea);
				}
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
			}

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Closing OP No:'.$iop;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$config['base_url'] = base_url().'index.php/opclose/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" 	select a.*, o.i_do, o.d_do, b.e_supplier_name from tm_op a left join tm_do o on (a.i_op=o.i_op), tr_supplier b
							where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.f_op_close='f'
							and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
							or upper(a.i_op) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('opclose');
			$this->load->model('opclose/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('opclose/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu54')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('opclose');
			$this->load->view('opclose/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu54')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/opclose/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));*/

			$cari=str_replace("%20", " ", $this->input->post("cari",false));
			if($cari=='')$cari=str_replace("%20", " ", $this->uri->segment(4));
			if($cari=='zxvf'){
				$cari='';
				$config['base_url'] = base_url().'index.php/opclose/cform/cari/zxvf/';
			}else{
				$config['base_url'] = base_url().'index.php/opclose/cform/cari/'.$cari.'/';
			}

			$query = $this->db->query(" select a.*, o.i_do, o.d_do, b.e_supplier_name 
										from tm_op a left join tm_do o on (a.i_op=o.i_op), tr_supplier b, tr_area c
										where a.i_supplier=b.i_supplier and a.f_op_cancel='f' and a.i_area=c.i_area and a.f_op_close='f'
										and (upper(c.e_area_name) ilike '%$cari%' or upper(b.e_supplier_name) ilike '%$cari%'
										or upper(a.i_op) ilike '%$cari%' or upper(a.i_reff) ilike '%$cari%' or upper(o.i_do) ilike '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('opclose');
			$this->load->model('opclose/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('opclose/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
