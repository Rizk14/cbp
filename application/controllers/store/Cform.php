<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu3')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/store/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_store');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('master_store');
			$data['istore']='';
			$this->load->model('store/mmaster');
			$data['isi']=$this->mmaster->bacasemua($config['per_page'],$this->uri->segment(4));
			$this->load->view('store/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu3')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$istore 	= $this->input->post('istore', TRUE);
			$estorename 	= $this->input->post('estorename', TRUE);
			$dstoreregister = $this->input->post('dstoreregister', TRUE);

			if ((isset($istore) && $istore != '') && (isset($estorename) && $estorename != ''))
			{
				$this->load->model('store/mmaster');
				$this->mmaster->insert($istore,$estorename,$dstoreregister);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu3')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_store');
			$this->load->view('store/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu3')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_store')." update";
			if($this->uri->segment(4)){
				$istore = $this->uri->segment(4);
				$data['istore'] = $istore;
				$this->load->model('store/mmaster');
				$data['isi']=$this->mmaster->baca($istore);
		 		$this->load->view('store/vmainform',$data);
			}else{
				$this->load->view('store/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu3')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$istore		= $this->input->post('istore', TRUE);
			$estorename 	= $this->input->post('estorename', TRUE);
			$dstoreregister = $this->input->post('dstoreregister', TRUE);
			$this->load->model('store/mmaster');
			$this->mmaster->update($istore,$estorename,$dstoreregister);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu3')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$istore	= $this->uri->segment(4);
			$this->load->model('store/mmaster');
			$this->mmaster->delete($istore);
			$config['base_url'] = base_url().'index.php/store/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_store');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('master_store');
			$data['istore']='';
			$data['isi']=$this->mmaster->bacasemua($config['per_page'],$this->uri->segment(4));
			$this->load->view('store/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu3')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/store/cform/index/';
			$query=$this->db->query("select * from tr_store where upper(i_store) like '%$cari%' or upper(e_store_name) like '%$cari%'",false);			
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('store/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_store');
			$data['istore']='';
	 		$this->load->view('store/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
