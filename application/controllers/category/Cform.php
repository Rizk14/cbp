<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu12')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/category/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_category
										where upper(i_product_category) like '%$cari%' 
										or upper(e_product_categoryname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_sub_category');
			$data['icategory']='';
			$this->load->model('category/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('category/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu12')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icategory 	= $this->input->post('icategory', TRUE);
			$ecategoryname 	= $this->input->post('ecategoryname', TRUE);
			$iclass 	= $this->input->post('iclass', TRUE);

			if ($icategory != '' && $ecategoryname != '' && $iclass != '')
			{
				$this->load->model('category/mmaster');
				$this->mmaster->insert($icategory,$ecategoryname,$iclass);

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Input Kategori:('.$icategory.')-'.$ecategoryname;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu12')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_sub_category');
			$this->load->view('category/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu12')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_sub_category')." update";
			if($this->uri->segment(4)){
				$icategory = $this->uri->segment(4);
				$data['icategory'] = $icategory;
				$this->load->model('category/mmaster');
				$data['isi']=$this->mmaster->baca($icategory);
		 		$this->load->view('category/vmainform',$data);
			}else{
				$this->load->view('category/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu12')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icategory = $this->input->post('icategory', TRUE);
			$ecategoryname 	= $this->input->post('ecategoryname', TRUE);
			$iclass = $this->input->post('iclass', TRUE);
			$eclassname = $this->input->post('eclassname', TRUE);
			$this->load->model('category/mmaster');
			$this->mmaster->update($icategory,$ecategoryname,$iclass);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
		  while($row=pg_fetch_assoc($query)){
			  $now	  = $row['c'];
		  }
		  $pesan='Update Kategori:('.$icategory.')-'.$ecategoryname;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );

			$config['base_url'] = base_url().'index.php/category/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_category
										where upper(i_product_category) like '%$cari%' 
										or upper(e_product_categoryname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_sub_category');
			$data['icategory']='';
			$this->load->model('category/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('category/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu12')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icategory = $this->uri->segment(4);
			$this->load->model('category/mmaster');
			$this->mmaster->delete($icategory);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
		  while($row=pg_fetch_assoc($query)){
			  $now	  = $row['c'];
		  }
		  $pesan='Delete Kategori:('.$icategory.')-'.$ecategoryname;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );

			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/category/cform/index/';
			$query = $this->db->query(" select * from tr_product_category
										where upper(i_product_category) like '%$cari%' 
										or upper(e_product_categoryname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_sub_category');
			$data['icategory']='';
			$this->load->model('category/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('category/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kelas()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu12')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/category/cform/kelas/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_class
										where upper(i_product_class) like '%$cari%' 
										or upper(e_product_classname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('category/mmaster');
			$data['page_title'] = $this->lang->line('list_class');
			$data['isi']=$this->mmaster->bacakelas($config['per_page'],$this->uri->segment(5));
			$this->load->view('category/vlistclass', $data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu12')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/category/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_category
										where upper(i_product_category) like '%$cari%' 
										or upper(e_product_categoryname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('category/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_sub_category');
			$data['icategory']='';
	 		$this->load->view('category/vmainform',$data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function carikelas()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu12')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/category/cform/kelas/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_class
										where upper(i_product_class) like '%$cari%' 
										or upper(e_product_classname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('category/mmaster');
			$data['page_title'] = $this->lang->line('list_class');
			$data['isi']=$this->mmaster->carikelas($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('category/vlistclass', $data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
}
?>
