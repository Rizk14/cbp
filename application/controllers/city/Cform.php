<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			/*	$cari = ($this->input->post("cari"))? $this->input->post("cari") : "";
       		$cari = ($this->uri->segment(4)) ? $this->uri->segment(4) : $cari;*/

			$config['base_url'] = base_url() . 'index.php/city/cform/index/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query("	select a.*, b.e_area_name, c.e_city_typename, 
							d.e_city_typeperareaname, e.e_city_groupname, 
							f.e_city_statusname 
							from tr_city a, tr_area b, tr_city_type c, tr_city_typeperarea d, 
							tr_city_group e, tr_city_status f 
							where a.i_area=b.i_area and a.i_city_type=c.i_city_type 
							  and a.i_area=d.i_area 
							  and a.i_city_typeperarea=d.i_city_typeperarea 
							  and a.i_city_group=e.i_city_group 
							  and a.i_city_status= f.i_city_status and b.i_area=e.i_area ", false);
			/*$data['cari'] = $cari;*/
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_city');
			$data['icity'] = '';
			$data['$icity'] = '';
			$data['$iarea'] = '';
			$data['$icitytype'] = '';
			$data['$icitytypeperarea'] = '';
			$data['$icitygroup'] = '';
			$data['$icitystatus'] = '';
			$cari = $this->input->post('cari', FALSE);
			$this->load->model('city/mmaster');
			$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = "Membuka Menu Master Kota";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('city/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$icity 			= $this->input->post('icity');
			$ecityname		= $this->input->post('ecityname');
			$iarea 			= $this->input->post('iarea');
			$eareaname		= $this->input->post('eareaname');
			$icitytype		= $this->input->post('icitytype');
			$ecitytypename		= $this->input->post('ecitytypename');
			$icitytypeperarea	= $this->input->post('icitytypeperarea');
			$ecitytypeperareaname	= $this->input->post('ecitytypeperareaname');
			$icitygroup		= $this->input->post('icitygroup');
			$ecitygroupname		= $this->input->post('ecitygroupname');
			$icitystatus 		= $this->input->post('icitystatus');
			$ecitystatusname	= $this->input->post('ecitystatusname');
			$ntoleransipusat	= $this->input->post('ntoleransipusat');
			$ntoleransicabang	= $this->input->post('ntoleransicabang');
			if ((isset($icity) && $icity != '') && (isset($ecityname) && $ecityname != '') && (isset($iarea) && $iarea != '')) {
				$this->load->model('city/mmaster');
				$this->mmaster->insert(
					$icity,
					$ecityname,
					$iarea,
					$icitytype,
					$icitytypeperarea,
					$icitygroup,
					$icitystatus,
					$ntoleransipusat,
					$ntoleransicabang
				);

				$sess = $this->session->userdata('session_id');
				$id = $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {
						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {
					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {
					$now	  = $row['c'];
				}
				$pesan = 'Input Kota:(' . $icity . ')-' . $ecityname;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$config['base_url'] = base_url() . 'index.php/city/cform/index/';
				/*$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);*/
				$query = $this->db->query("	select a.*, b.e_area_name, c.e_city_typename, 
								d.e_city_typeperareaname, e.e_city_groupname, 
								f.e_city_statusname 
								from tr_city a, tr_area b, tr_city_type c, tr_city_typeperarea d, 
								tr_city_group e, tr_city_status f 
								where a.i_area=b.i_area and a.i_city_type=c.i_city_type 
								  and a.i_area=d.i_area 
								  and a.i_city_typeperarea=d.i_city_typeperarea 
								  and a.i_city_group=e.i_city_group 
								  and a.i_city_status= f.i_city_status and b.i_area=e.i_area ", false);
				/*$data['cari'] = $cari;*/
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);

				$data['page_title'] = $this->lang->line('master_city');
				$data['icity'] = '';
				$data['$icity'] = '';
				$data['$iarea'] = '';
				$data['$icitytype'] = '';
				$data['$icitytypeperarea'] = '';
				$data['$icitygroup'] = '';
				$data['$icitystatus'] = '';
				$cari = $this->input->post('cari', FALSE);
				$this->load->model('city/mmaster');
				$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4));
				$this->load->view('city/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('master_city');
			$this->load->view('city/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('master_city') . " update";
			if ($this->uri->segment(4)) {
				$icity = $this->uri->segment(4);
				$iarea = $this->uri->segment(5);
				$data['icity'] = $icity;
				$data['iarea'] = $iarea;
				$this->load->model('city/mmaster');
				$data['isi'] = $this->mmaster->baca($icity, $iarea);

				$sess = $this->session->userdata('session_id');
				$id = $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if (pg_num_rows($rs) > 0) {
					while ($row = pg_fetch_assoc($rs)) {
						$ip_address	  = $row['ip_address'];
						break;
					}
				} else {
					$ip_address = 'kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while ($row = pg_fetch_assoc($query)) {
					$now	  = $row['c'];
				}
				$pesan = 'Membuka Edit Master Kota:(' . $icity . ')';
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$this->load->view('city/vmainform', $data);
			} else {
				$this->load->view('city/vinsert_fail', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$icity 			= $this->input->post('icity');
			$ecityname		= $this->input->post('ecityname');
			$iarea 			= $this->input->post('iarea');
			$eareaname		= $this->input->post('eareaname');
			$icitytype		= $this->input->post('icitytype');
			$ecitytypename		= $this->input->post('ecitytypename');
			$icitytypeperarea	= $this->input->post('icitytypeperarea');
			$ecitytypeperareaname	= $this->input->post('ecitytypeperareaname');
			$icitygroup		= $this->input->post('icitygroup');
			$ecitygroupname		= $this->input->post('ecitygroupname');
			$icitystatus 		= $this->input->post('icitystatus');
			$ecitystatusname	= $this->input->post('ecitystatusname');
			$ntoleransipusat	= $this->input->post('ntoleransipusat');
			$ntoleransicabang	= $this->input->post('ntoleransicabang');
			$this->load->model('city/mmaster');
			$this->mmaster->update(
				$icity,
				$ecityname,
				$iarea,
				$icitytype,
				$icitytypeperarea,
				$icitygroup,
				$icitystatus,
				$ntoleransipusat,
				$ntoleransicabang
			);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Update Kota:(' . $icity . ')-' . $ecityname;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$config['base_url'] = base_url() . 'index.php/city/cform/index/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query("	select a.*, b.e_area_name, c.e_city_typename, 
							d.e_city_typeperareaname, e.e_city_groupname, 
							f.e_city_statusname 
							from tr_city a, tr_area b, tr_city_type c, tr_city_typeperarea d, 
							tr_city_group e, tr_city_status f 
							where a.i_area=b.i_area and a.i_city_type=c.i_city_type 
							  and a.i_area=d.i_area 
							  and a.i_city_typeperarea=d.i_city_typeperarea 
							  and a.i_city_group=e.i_city_group 
							  and a.i_city_status= f.i_city_status and b.i_area=e.i_area ", false);
			/*$data['cari'] = $cari;*/
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_city');
			$data['icity'] = '';
			$data['$icity'] = '';
			$data['$iarea'] = '';
			$data['$icitytype'] = '';
			$data['$icitytypeperarea'] = '';
			$data['$icitygroup'] = '';
			$data['$icitystatus'] = '';
			$cari = $this->input->post('cari', FALSE);
			$this->load->model('city/mmaster');
			$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4));
			$this->load->view('city/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$icity = $this->uri->segment(4);
			$iarea = $this->uri->segment(5);
			$ecityname = $this->uri->segment(6);
			$this->load->model('city/mmaster');
			$this->mmaster->delete($icity, $iarea);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Delete Kota:(' . $icity . ')-' . $ecityname;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$config['base_url'] = base_url() . 'index.php/city/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_city
									   	where upper(e_city_name) like '%$cari%' or upper(i_city) like '%$cari%' ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_city');
			$data['icity'] = '';
			$data['iarea'] = '';
			$this->load->model('city/mmaster');
			$data['isi'] = $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4));
			$this->load->view('city/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$baris = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/city/cform/area/' . $baris . '/sikasep/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query("	select a.e_area_name, b.e_city_typename, c.* from tr_area a, tr_city_type b, tr_city_typeperarea c order by a.i_area asc ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris'] = $baris;
			$data['cari'] = '';
			$this->load->model('city/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(6));
			$this->load->view('city/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function citystatus()
	{

		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/city/cform/citystatus/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_city_status
							where upper(e_city_statusname) like '%$cari%' or upper(i_city_status) like '%$cari%' ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('city/mmaster');
			$data['page_title'] = $this->lang->line('list_citystatus');
			$data['isi'] = $this->mmaster->bacacitystatus($config['per_page'], $this->uri->segment(5));
			$this->load->view('city/vlistcitystatus', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function citygroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/city/cform/citygroup/' . $iarea . '/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_city_group
							where i_area='$iarea' 
							and (upper(e_city_groupname) like '%$cari%' or upper(i_city_group) like '%$cari%') ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('city/mmaster');
			$data['page_title'] = $this->lang->line('list_citygroup');
			$data['iarea'] = $iarea;
			$data['isi'] = $this->mmaster->bacacitygroup($iarea, $config['per_page'], $this->uri->segment(5));
			$this->load->view('city/vlistcitygroup', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			/*	$cari = ($this->input->post("cari"))? $this->input->post("cari") : "";
       		$cari = ($this->uri->segment(4)) ? $this->uri->segment(4) : $cari;

			$config['base_url'] = base_url().'index.php/city/cform/index/'.$cari;*/
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$cari = ($this->input->post("cari", false));
			if ($cari == '') $cari = $this->uri->segment(4);
			if ($cari == 'zxvf') {
				$cari = '';
				$config['base_url'] = base_url() . 'index.php/city/cform/cari/zxvf/';
			} else {
				$config['base_url'] = base_url() . 'index.php/city/cform/cari/' . $cari . '/';
			}

			$query = $this->db->query("	select a.*, b.e_area_name, c.e_city_typename, d.e_city_typeperareaname, e.e_city_groupname, f.e_city_statusname from tr_city a, tr_area b, tr_city_type c, tr_city_typeperarea d, tr_city_group e, tr_city_status f where a.i_area=b.i_area and a.i_city_type=c.i_city_type and a.i_area=d.i_area and a.i_city_typeperarea=d.i_city_typeperarea and a.i_city_group=e.i_city_group and a.i_city_status= f.i_city_status and b.i_area=e.i_area and (upper(a.e_city_name) ilike '%$cari%' or upper(a.i_city) ilike '%$cari%') ", false);
			/*$data['cari'] = $cari;*/
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('city/mmaster');
			$data['isi'] = $this->mmaster->cari($cari, $config['per_page'], $this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_city');
			$data['icity'] = '';
			$this->load->view('city/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			/*$config['base_url'] = base_url().'index.php/city/cform/area/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$baris 	= $this->input->post('baris', FALSE);
			if ($baris == '') $baris = $this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if ($cari == '' && $this->uri->segment(5) != 'sikasep') $cari = $this->uri->segment(5);
			if ($cari != 'sikasep')
				$config['base_url'] = base_url() . 'index.php/city/cform/cariarea/' . $baris . '/' . $cari . '/';
			else
				$config['base_url'] = base_url() . 'index.php/city/cform/cariarea/' . $baris . '/sikasep/';

			$query = $this->db->query("	select a.e_area_name, b.e_city_typename, c.* from tr_area a, tr_city_type b, 
										tr_city_typeperarea c 
										where a.i_area=c.i_area and b.i_city_type=c.i_city_type and ( upper(a.e_area_name) ilike 
										'%$cari%' or upper(a.i_area) ilike '%$cari%' or upper(c.e_city_typeperareaname) ilike 
										'%$cari%') ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris'] = $baris;
			$data['cari'] = $cari;
			$this->load->model('city/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(6));
			$this->load->view('city/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricitystatus()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/city/cform/citystatus/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_city_status
							where upper(e_city_statusname) like '%$cari%' or upper(i_city_status) like '%$cari%' ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('city/mmaster');
			$data['page_title'] = $this->lang->line('list_citystatus');
			$data['isi'] = $this->mmaster->caricitystatus($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('city/vlistcitystatus', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricitygroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu27') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iarea = $this->input->post('iarea', FALSE);
			if ($iarea == '') $iarea = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/city/cform/citygroup/' . $iarea . '/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_city_group
							where i_area='$iarea' 
							and (upper(e_city_groupname) like '%$cari%' or upper(i_city_group) like '%$cari%') ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('city/mmaster');
			$data['page_title'] = $this->lang->line('list_citygroup');
			$data['iarea'] = $iarea;
			$data['isi'] = $this->mmaster->caricitygroup($cari, $iarea, $config['per_page'], $this->uri->segment(5));
			$this->load->view('city/vlistcitygroup', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function getnumber()
	{
		header("Content-Type: application/json", true);
		$iarea     	= $this->input->post('iarea');

		$this->load->model('city/mmaster');

		$query  = array(
			'number' => $this->mmaster->runningnumber($iarea)
		);
		echo json_encode($query);
	}
}
