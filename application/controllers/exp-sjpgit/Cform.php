<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu274') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('exp-sjpgit');
			$data['iperiode']	= '';
			$this->load->view('exp-sjpgit/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu274') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-sjpgit/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}

			$a = substr($iperiode, 0, 4);
			$b = substr($iperiode, 4, 2);
			$peri = mbulan($b) . " - " . $a;

			$this->db->select("	a.i_sjp, c.e_area_name , a.d_sjp, a.d_sjp_receive, b.i_product, 
                          b.e_product_name, b.n_quantity_deliver, b.n_quantity_receive
                          from tm_sjp a, tm_sjp_item b, tr_area c
                          where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.i_area=c.i_area and a.f_sjp_cancel='f'
                          and to_char(a.d_sjp,'yyyymm')='$iperiode' and (b.n_quantity_receive is null
                          or (to_char(a.d_sjp::timestamp with time zone, 'yyyymm'::text))<(to_char(a.d_sjp_receive::timestamp with time zone, 'yyyymm'::text)))
                          order by a.i_sjp", false);

			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar SJP GIT ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(16);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar SJP GIT');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 8, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Bulan : ' . $peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 8, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:I5'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'No  SJP');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tgl Kirim');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Tgl Terima');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Jml Kirim');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Jml Terima');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Selisih');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

				$i = 6;
				$j = 6;
				$no = 0;
				foreach ($query->result() as $row) {
					$no++;
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':I' . $i
					);
					$selisih = $row->n_quantity_receive - $row->n_quantity_deliver;

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->e_area_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->i_sjp, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->d_sjp, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->d_sjp_receive, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->i_product, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->e_product_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->n_quantity_deliver, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->n_quantity_receive, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $selisih, Cell_DataType::TYPE_NUMERIC);
					$i++;
					$j++;
				}
				$x = $i - 1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama = 'SJPGIT' . $iperiode . '.xls';
			$objWriter->save('excel/00/' . $nama);

			// $sess = $this->session->userdata('session_id');
			// $id = $this->session->userdata('user_id');
			// $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			// $rs		= pg_query($sql);
			// if (pg_num_rows($rs) > 0) {
			// 	while ($row = pg_fetch_assoc($rs)) {
			// 		$ip_address	  = $row['ip_address'];
			// 		break;
			// 	}
			// } else {
			// 	$ip_address = 'kosong';
			// }
			// $query 	= pg_query("SELECT current_timestamp as c");
			// while ($row = pg_fetch_assoc($query)) {
			// 	$now	  = $row['c'];
			// }
			// $pesan = 'Export SJP GIT Periode' . $peri;
			// $this->load->model('logger');
			$this->logger->writenew('Export SJP GIT Periode' . $peri);

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$data['folder']	= 'excel/00';

			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
