<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu149')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-kk-un/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tm_kk
										where (upper(i_area) like '%$cari%' or upper(i_kk) like '%$cari%') and f_close='t' and f_posting='f'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listkk');
			$this->load->model('akt-kk-un/mmaster');
			$data['ikk']	= '';
			$data['iarea']	= '';
			$data['iperiode']= '';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('akt-kk-un/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu149')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('list_nota');
			$this->load->view('akt-kk-un/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu149')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/akt-kk-un/cform/index/';
			$config['per_page'] = '10';
			$limo=$config['per_page'];
			$ofso=$this->uri->segment(4);
			if($ofso=='')
				$ofso=0;
			$query = $this->db->query(" select * from tm_kk
										where (upper(i_area) like '%$cari%' or upper(i_kk) like '%$cari%') and f_close='t' and f_posting='f'",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('akt-kk-un/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('listkk');
			$data['ikk']	= '';
			$data['iarea']	= '';
			$data['iperiode']= '';
	 		$this->load->view('akt-kk-un/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu149')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('kk');
			if(
				$this->uri->segment(4) && $this->uri->segment(5) && $this->uri->segment(6)
			  ){
				$this->load->model('akt-kk-un/mmaster');
				$data['ikk'] 			= $this->uri->segment(4);
				$data['iperiode']		= $this->uri->segment(5);
				$data['iarea'] 			= $this->uri->segment(6);
				$ikk 					= $this->uri->segment(4);
				$iperiode				= $this->uri->segment(5);
				$iarea 					= $this->uri->segment(6);
				$data['isi']			= $this->mmaster->baca($ikk,$iperiode,$iarea);
		 		$this->load->view('akt-kk-un/vformapprove',$data);
			}else{
				$this->load->view('akt-kk-un/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function unposting()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu149')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('akt-kk-un/mmaster');
			$iperiode		= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$ikk			= $this->input->post('ikk', TRUE);
			$icoa			= $this->input->post('icoa', TRUE);
			$ecoaname		= $this->input->post('ecoaname', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$edescription	= $this->input->post('edescription', TRUE);
			$eremark		= $ikk.":".$iarea.":".$iperiode.":".$edescription;
			$vkk			= $this->input->post('vkk', TRUE);
			$vkk			= str_replace(',','',$vkk);
			$dkk			= $this->input->post('dkk', TRUE);
			if($dkk!=''){
				$tmp=explode("-",$dkk);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkk=$th."-".$bl."-".$hr;

			}
			$fdebet			= $this->input->post('fdebet', TRUE);
			$fclose			= 'f';
			$this->db->trans_begin();
			$this->mmaster->deletetransheader($ikk,$iarea,$dkk);
			$this->mmaster->updatekk($ikk,$iarea,$iperiode);
			if($fdebet=='t'){
				$accdebet		= $icoa;
				$namadebet		= $ecoaname;
				$acckredit		= '111.2'.$iarea;
				$namakredit		= $this->mmaster->namaacc($acckredit);
			}else{
				$accdebet		= '111.2'.$iarea;
				$namadebet		= $this->mmaster->namaacc($acckredit);
				$acckredit		= $icoa;
				$namakredit		= $ecoaname;
			}			
			$this->mmaster->deletetransitemdebet($accdebet,$ikk,$dkk,$iarea);
			$this->mmaster->updatesaldodebet($accdebet,$iperiode,$vkk);
			$this->mmaster->deletetransitemkredit($acckredit,$ikk,$dkk,$iarea);
			$this->mmaster->updatesaldokredit($acckredit,$iperiode,$vkk);
			$this->mmaster->deletegldebet($accdebet,$ikk,$iarea,$dkk);
			$this->mmaster->deleteglkredit($acckredit,$ikk,$iarea,$dkk);
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='UnPosting Kas Kecil No:'.$ikk.' Area:'.$iarea;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses'] = true;
				$data['inomor']	= $ikk;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
