<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu466')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	 /* $cari = ($this->input->post("cari"))? $this->input->post("cari") : "";
       		  $cari = ($this->uri->segment(4)) ? $this->uri->segment(4) : $cari;*/

			  $config['base_url'] = base_url().'index.php/userarea/cform/index/';
			  //$cari = strtoupper($this->input->post('cari', FALSE));
			  $query = $this->db->query(" select * from tm_user_area order by i_area asc ",false);
         	  
         	 /* $data['cari'] 		= $cari;*/
			  $config['total_rows'] = $query->num_rows();				
			  $config['per_page'] 	= '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] 	= 'Akhir';
			  $config['next_link'] 	= 'Selanjutnya';
			  $config['prev_link'] 	= 'Sebelumnya';
			  $config['cur_page'] 	= $this->uri->segment(4);
			  $this->paginationxx->initialize($config);

			  $data['page_title'] 	= $this->lang->line('master_userarea');
			  $data['iuser'] 		='';
			  $data['iarea'] 		='';
			  $this->load->model('userarea/mmaster');
			  $cari = $this->input->post('cari', FALSE);
			  $data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			  $sess  = $this->session->userdata('session_id');
				$id   = $this->session->userdata('user_id');
				$sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs   = pg_query($sql);
				if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
				 $ip_address   = $row['ip_address'];
				 break;
				}
				}else{
				$ip_address='kosong';
				}
				$query  = pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
				$now    = $row['c'];
				}
				$pesan="Membuka Menu User Area";
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

			  $this->load->view('userarea/vmainform', $data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu466')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iuser 		= $this->input->post('iuser', TRUE);
			$iarea 		= $this->input->post('iarea', TRUE);

			if ((isset($iuser) && $iuser != '') 
			    && (isset($iarea) && $iarea != ''))
			{
				$this->load->model('userarea/mmaster');
				$this->mmaster->insert($iuser,$iarea);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input User Area:('.$iarea.') -'.$iuser;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		        $config['base_url'] = base_url().'index.php/userarea/cform/index/';

				$query = $this->db->query(" select * from tm_user_area order by i_area asc ",false);

				/* $data['cari'] 		= $cari;*/
				$config['total_rows'] = $query->num_rows();				
				$config['per_page'] 	= '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] 	= 'Akhir';
				$config['next_link'] 	= 'Selanjutnya';
				$config['prev_link'] 	= 'Sebelumnya';
				$config['cur_page'] 	= $this->uri->segment(4);
				$this->paginationxx->initialize($config);

				$data['page_title'] 	= $this->lang->line('master_userarea');
				$data['iuser']	  	= '';
				$data['iarea']	  	= '';
				$this->load->model('userarea/mmaster');
				$cari = $this->input->post('cari', FALSE);
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('userarea/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu466')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_userarea');
			$this->load->view('userarea/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu466')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iuser	= $this->uri->segment(4);
			$iarea 	= $this->uri->segment(5);
			$this->load->model('userarea/mmaster');
			$this->mmaster->delete($iuser,$iarea);
			
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
			  $ip_address	  = $row['ip_address'];
			  break;
			}
			}else{
			$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
			}
			$pesan='Delete User Area:'.$iuser;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			
			$config['base_url'] = base_url().'index.php/userarea/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tm_user_area 
										where upper(tm_user_area.i_user) like '%$cari%'
										or upper(tm_user_area.i_area) like '%$cari%'
									    order by tm_user_area.i_user, tm_user_area.i_area ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_userarea');
			$data['iuser']		='';
			$data['iarea']		='';
			$this->load->model('userarea/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('userarea/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu466')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/userarea/cform/area/'.$baris.'/sikasep/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select * from tr_area order by i_area ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->model('userarea/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6));
			$this->load->view('userarea/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu466')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	  /*$cari = ($this->input->post("cari"))? $this->input->post("cari") : "";
       		  $cari = ($this->uri->segment(4)) ? $this->uri->segment(4) : $cari;

			  $config['base_url'] = base_url().'index.php/userarea/cform/index/'.$cari;
			  //$cari = strtoupper($this->input->post('cari', FALSE));*/

			$cari=($this->input->post("cari",false));
			if($cari=='')$cari=$this->uri->segment(4);
			if($cari=='zxvf'){
				$cari='';
				$config['base_url'] = base_url().'index.php/userarea/cform/cari/zxvf/';
			}else{
				$config['base_url'] = base_url().'index.php/userarea/cform/cari/'.$cari.'/';
			}

			  $query = $this->db->query("	select * from tm_user_area where upper(tm_user_area.i_user) ilike '%$cari%' 
											or upper(tm_user_area.i_area) ilike '%$cari%'
											order by tm_user_area.i_user, tm_user_area.i_area ",false);
        
			  $config['total_rows'] = $query->num_rows();				
			  $config['per_page'] 	= '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] 	= 'Akhir';
			  $config['next_link'] 	= 'Selanjutnya';
			  $config['prev_link'] 	= 'Sebelumnya';
			  $config['cur_page'] 	= $this->uri->segment(5);
			  $this->paginationxx->initialize($config);
			  $this->load->model('userarea/mmaster');
			  $data['cari'] 		= $cari;
			  $data['isi']			= $this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			  $data['page_title'] 	= $this->lang->line('master_userarea');
			  $data['iuser']		= '';
			  $data['iarea']		= '';
	   		$this->load->view('userarea/vmainform',$data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu466')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/userarea/cform/area/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/userarea/cform/cariarea/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/userarea/cform/cariarea/'.$baris.'/sikasep/';

			$query = $this->db->query("select i_area from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('userarea/mmaster');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('userarea/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
