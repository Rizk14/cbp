<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu416')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('ttdpajak');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('ttdpajak/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu416')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-daftarnota/mmaster');
			$iarea  	= $this->input->post('iarea');
/*
			$iperiode	= $this->input->post('iperiode');
      $a=substr($iperiode,2,2);
	    $b=substr($iperiode,4,2);
		  $peri=mbulan($b)." - ".$a;
      if($iarea=='NA'){
        $no='FP-'.$a.$b.'-%';
      }else{
        $no='FP-'.$a.$b.'-'.$iarea.'%';
      }
*/
      $dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
      if($iarea=='NA'){
        $this->db->select(" a.i_nota, b.e_customer_name, a.i_seri_pajak, a.d_pajak, a.v_nota_netto, d.e_customer_pkpname, b.f_customer_pkp,
                            a.v_nota_discount, a.v_nota_discounttotal, a.v_nota_discount1, a.v_nota_discount2, a.v_nota_discount3, 
                            a.v_nota_discount4
                            from tm_nota a, tr_customer b
                            left join tr_customer_pkp d on(b.i_customer=d.i_customer) 
                            where (a.d_pajak >= to_date('$dfrom', 'dd-mm-yyyy') 
                            and a.d_pajak <= to_date('$dto', 'dd-mm-yyyy')) and a.i_customer=b.i_customer
                            and not a.i_faktur_komersial isnull 
                            and not a.i_seri_pajak isnull and a.f_nota_cancel='f'
                            order by a.i_area, a.d_nota, a.i_nota",false);
                            #and a.n_faktur_komersialprint>0 
      }else{
        $this->db->select(" a.i_nota, b.e_customer_name, a.i_seri_pajak, a.d_pajak, a.v_nota_netto, d.e_customer_pkpname, b.f_customer_pkp,
                            a.v_nota_discount, a.v_nota_discounttotal, a.v_nota_discount1, a.v_nota_discount2, a.v_nota_discount3, 
                            a.v_nota_discount4
                            from tm_nota a, tr_customer b
                            left join tr_customer_pkp d on(b.i_customer=d.i_customer) 
                            where (a.d_pajak >= to_date('$dfrom', 'dd-mm-yyyy') 
                            and a.d_pajak <= to_date('$dto', 'dd-mm-yyyy')) and a.i_customer=b.i_customer
                            and not a.i_faktur_komersial isnull 
                            and not a.i_seri_pajak isnull and a.f_nota_cancel='f' and a.i_area='$iarea'
                            order by a.i_area, a.d_nota, a.i_nota",false);
                            #and a.n_faktur_komersialprint>0 
      }
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("TTD Pajak")->setDescription("PT. Dialogue Garmindo Utama");
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(22);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A4:H4'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A4', 'No.');
				$objPHPExcel->getActiveSheet()->getStyle('A4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
              'bottom'=> array('style' => Style_Border::BORDER_THIN),
              'left' 	=> array('style' => Style_Border::BORDER_THIN),
              'right'	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B4', 'NAMALANG');
				$objPHPExcel->getActiveSheet()->getStyle('B4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
              'bottom'=> array('style' => Style_Border::BORDER_THIN),
              'left' 	=> array('style' => Style_Border::BORDER_THIN),
              'right'	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C4', 'NAMAPKP');
				$objPHPExcel->getActiveSheet()->getStyle('C4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
              'bottom'=> array('style' => Style_Border::BORDER_THIN),
              'left' 	=> array('style' => Style_Border::BORDER_THIN),
              'right'	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D4', 'NODOK');
				$objPHPExcel->getActiveSheet()->getStyle('D4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
              'bottom'=> array('style' => Style_Border::BORDER_THIN),
              'left' 	=> array('style' => Style_Border::BORDER_THIN),
              'right'	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E4', 'NOSERI');
				$objPHPExcel->getActiveSheet()->getStyle('E4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
              'bottom'=> array('style' => Style_Border::BORDER_THIN),
              'left' 	=> array('style' => Style_Border::BORDER_THIN),
              'right'	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F4', 'PPN');
				$objPHPExcel->getActiveSheet()->getStyle('F4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
              'bottom'=> array('style' => Style_Border::BORDER_THIN),
              'left' 	=> array('style' => Style_Border::BORDER_THIN),
              'right'	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G4', 'TGLPJK');
				$objPHPExcel->getActiveSheet()->getStyle('G4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
              'bottom'=> array('style' => Style_Border::BORDER_THIN),
              'left' 	=> array('style' => Style_Border::BORDER_THIN),
              'right'	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H4', 'CHECK');
				$objPHPExcel->getActiveSheet()->getStyle('H4')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
              'bottom'=> array('style' => Style_Border::BORDER_THIN),
              'left' 	=> array('style' => Style_Border::BORDER_THIN),
              'right'	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i=5;
				foreach($query->result() as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  ),
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
              'bottom'=> array('style' => Style_Border::BORDER_THIN),
              'left' 	=> array('style' => Style_Border::BORDER_THIN),
              'right'	=> array('style' => Style_Border::BORDER_THIN)
						)
				  ),
				  'A'.$i.':H'.$i
				  );
          
          if($row->d_pajak!=''){
            $tmp=explode('-',$row->d_pajak);
            $hr=$tmp[2];
            $bl=$tmp[1];
            $th=$tmp[0];
            $row->d_pajak=$hr.'-'.$bl.'-'.$th;
          }
          $row->v_nota_dpp=round($row->v_nota_netto/1.1);
          $row->v_nota_ppn=round($row->v_nota_netto/1.1*0.1);
          if($row->f_customer_pkp=='t'){
            $kodetran='04';
          }else{
            $kodetran='07';
          }

          if($row->v_nota_discount!=$row->v_nota_discounttotal){
            $row->v_nota_discount=$row->v_nota_discount1+$row->v_nota_discount2+$row->v_nota_discount3+$row->v_nota_discount4;
          }

          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $i-4, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->e_customer_pkpname, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, substr($row->i_nota,8,7), Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->i_seri_pajak, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->v_nota_ppn, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->d_pajak, Cell_DataType::TYPE_STRING);
					$i++;
				}
        $x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='ttdpajak'.$iarea.$dfrom.$dto.'.xls';
      if(file_exists('pajak/'.$nama)){
        @chmod('pajak/'.$nama, 0777);
        @unlink('pajak/'.$nama);
      }
			$objWriter->save('pajak/'.$nama); 
      @chmod('pajak/'.$nama, 0777);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
		  $data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='TTD Pajak Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses'] = true;
			$data['inomor']	= "Tanda Terima Dokumen Pajak";
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu416')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/ttdpajak/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('ttdpajak/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('ttdpajak/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu416')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/ttdpajak/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('ttdpajak/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('ttdpajak/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
