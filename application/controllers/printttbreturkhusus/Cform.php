<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu247')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/printttbreturkhusus/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$user=$this->session->userdata('user_id');
			/* $query = $this->db->query(" select a.i_bbm
                                  from tm_bbm a, tr_area c, tr_salesman d
										              where a.i_bbm_type='05' and a.f_bbm_cancel='f' and a.i_area=c.i_area 
                                  and a.i_salesman=d.i_salesman and a.i_area=d.i_area
										              and (upper(a.i_bbm) like '%$cari%' or upper(a.i_salesman) like '%$cari%' 
										              or upper(c.e_area_name) like '%$cari%' or upper(d.e_salesman_name) like '%$cari%') 
                                  and (a.i_area='$area1' or a.i_area like '%$area2%'
													  or a.i_area like '%$area3%' or a.i_area like '%$area4%' or a.i_area like '%$area5%')",false); */
													  
			$query = $this->db->query(" SELECT a.i_area , a.i_ttb, d_ttb, b.e_salesman_name 
										from tm_ttbretur a 
										inner join tr_salesman b on a.i_salesman = b.i_salesman 
										where a.i_area in(select i_area from tm_user_area  where i_user = '$user')",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printbbmretur');
			$this->load->model('printttbreturkhusus/mmaster');
			$data['ibbm']='';
			$data['cari']=$cari;
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4),$area1,$area2,$area3,$area4,$area5,$user);
			$this->load->view('printttbreturkhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu247')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ittb  = $this->uri->segment(4);
			$this->load->model('printttbreturkhusus/mmaster');
			$data['ittb']=$ittb;
			$data['page_title'] = $this->lang->line('printbbmretur');
			$data['isi']=$this->mmaster->baca($ittb);
			$data['detail']=$this->mmaster->bacadetail($ittb);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak TTB-Retur No:'.$ittb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printttbreturkhusus/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu247')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printbbmretur');
			$this->load->view('printttbreturkhusus/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu247')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printttbreturkhusus/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			/*$query = $this->db->query(" select a.*, b.e_supplier_name, c.e_area_name from tm_ap a, tr_supplier b, tr_area c
										where a.i_supplier=b.i_supplier and a.i_area=c.i_area
										and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
										or upper(a.i_ap) like '%$cari%' or upper(a.i_area) like '%$cari%' or upper(c.e_area_name) like '%$cari%')",false);
			*/

			/*$query = $this->db->query(" select a.*, b.e_supplier_name, c.e_area_name from tm_ap a, tr_supplier b, tr_area c
										where a.i_supplier=b.i_supplier and a.i_area=c.i_area
										and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
										or upper(a.i_ap) like '%$cari%' or upper(a.i_area) like '%$cari%' 
										or upper(c.e_area_name) like '%$cari%') and (a.i_area='$area1' or a.i_area like '%$area2%'
										or a.i_area like '%$area3%' or a.i_area like '%$area4%' or a.i_area like '%$area5%')",false);
			*/
			$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name from tm_ttbretur a, tr_customer b, tr_area c, tr_salesman d
										where a.i_customer=b.i_customer and a.i_area=c.i_area and a.i_salesman=d.i_salesman
										and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										or upper(a.i_ttb) like '%$cari%' or upper(a.i_area) like '%$cari%' or upper(a.i_salesman) like '%$cari%' 
										or upper(c.e_area_name) like '%$cari%') or upper(c.e_salesman_name) like '%$cari%')",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('printttbreturkhusus/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('printbbmretur');
			$data['cari']=$cari;
			$data['iap']='';
			$data['detail']='';
	 		$this->load->view('printttbreturkhusus/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
