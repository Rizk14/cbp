<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu399')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_pelunasanap');
			$data['dfrom']='';
			$data['dto']  ='';
			$data['ipl']  ='';
			$data['isupplier']='';
			$data['isi']  ='';
			$this->load->view('cekpelunasanhutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu399')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cekpelunasanhutang/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select a.*, b.e_supplier_name, b.e_supplier_address, b.e_supplier_city, c.e_jenis_bayarname
                                  from tm_pelunasanap a, tr_supplier b, tr_jenis_bayar c
                                  where 
                                  a.i_supplier=b.i_supplier
                                  and a.i_jenis_bayar=c.i_jenis_bayar
                                  and (upper(a.i_pelunasanap) like '%$cari%' or upper(a.i_giro) like '%$cari%'
                                  or upper(a.i_supplier) like '%$cari%')
                                  and a.i_supplier='$isupplier' and a.f_pelunasanap_cancel='f' and
                                  a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
                                  a.d_bukti <= to_date('$dto','dd-mm-yyyy')
                                  order by a.d_bukti,a.i_supplier,a.i_pelunasanap",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_pelunasanap');
			$this->load->model('cekpelunasanhutang/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isupplier']	= $isupplier;
			$data['ipl']	= '';
			$data['isi']	= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('cekpelunasanhutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu399')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_pelunasanap');
			$this->load->view('cekpelunasanhutang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu399')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('pelunasanap')." update";
			if(
				($this->uri->segment(4)) && ($this->uri->segment(5))			
			  )
			{
				$ipl 	= $this->uri->segment(4);
				$isupplier= $this->uri->segment(5);
				$dfrom= $this->uri->segment(6);
				$dto 	= $this->uri->segment(7);
				$query 			= $this->db->query("select * from tm_pelunasanap_item 
													where i_pelunasanap = '$ipl' ");
				$data['jmlitem'] 		= $query->num_rows(); 				
				$data['ipl'] 			  = $ipl;
				$data['isupplier']	= $isupplier;
				$data['dfrom']   = $dfrom;
				$data['dto']	   = $dto;
				$this->load->model('cekpelunasanhutang/mmaster');
				$data['vsisa']=$this->mmaster->sisa($isupplier,$ipl);
				$data['isi']=$this->mmaster->bacapl($isupplier,$ipl);
				$data['detail']=$this->mmaster->bacadetailpl($isupplier,$ipl);
		 		$this->load->view('cekpelunasanhutang/vmainform',$data);
			}else{
				$this->load->view('cekpelunasanhutang/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu399')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cekpelunasanhutang/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select a.*, b.e_supplier_name, b.e_supplier_address, b.e_supplier_city, c.e_jenis_bayarname
                                  from tm_pelunasanap a, tr_supplier b, tr_jenis_bayar c
                                  where 
                                  a.i_supplier=b.i_supplier
                                  and a.i_jenis_bayar=c.i_jenis_bayar
                                  and (upper(a.i_pelunasanap) like '%$cari%' or upper(a.i_giro) like '%$cari%'
                                  or upper(a.i_supplier) like '%$cari%')
                                  and a.i_supplier='$isupplier' and a.f_pelunasanap_cancel='f' and
                                  a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
                                  a.d_bukti <= to_date('$dto','dd-mm-yyyy')
                                  order by a.d_bukti,a.i_supplier,a.i_pelunasanap",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_pelunasan');

			$this->load->model('cekpelunasanhutang/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isupplier']	= $isupplier;
			$data['ipl']	= '';
			$data['isi']	= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('cekpelunasanhutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu399')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/cekpelunasanhutang/cform/supplier/index/';
			$query = $this->db->query("select * from tr_supplier ",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10'; 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('cekpelunasanhutang/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('cekpelunasanhutang/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu399')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/cekpelunasanhutang/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%' 
						      	  or upper(e_supplier_name) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('cekpelunasanhutang/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('cekpelunasanhutang/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function updatepelunasan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu105')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('cekpelunasanhutang/mmaster');
			$ipl 	= $this->input->post('ipelunasanap', TRUE);
			$isupplier	= $this->input->post('isupplier', TRUE);
			$ecek1	= $this->input->post('ecek1',TRUE);
			if($ecek1=='')
				$ecek1=null;
			$user		=$this->session->userdata('user_id');

			$this->mmaster->updatecek($ecek1,$user,$ipl,$isupplier);
########## Posting ##########
      $iarea 			= '00';
      $dbukti			= $this->input->post('dpelunasanap', TRUE);
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
				$iperiode=$th.$bl;
			}
			$suppname 		= $this->input->post('esuppliername', TRUE);
			$esuppname=str_replace("'","''",$esuppname);
			$egirodescription="Pelunasan hutang kepada:".$suppname;
			$fclose			= 'f';
			$jml			= $this->input->post('jml', TRUE);
			for($i=1;$i<=$jml;$i++)
			{
			  $inota=$this->input->post('inota'.$i, TRUE);
			  $ireff=$ipl.'|'.$inota; 
        if($i==1){
			    $this->mmaster->inserttransheader($ireff,$iarea,$egirodescription,$fclose,$dbukti);
			    $this->mmaster->updatepelunasan($ipl,$iarea,$dbukti);
        }
				$vjumlah		= $this->input->post('vjumlah'.$i, TRUE);
				$vjumlah		= str_replace(',','',$vjumlah);
				$accdebet		= HutangDagang;
				$namadebet		= $this->mmaster->namaacc($accdebet);
				$tmp			= $this->mmaster->carisaldo($accdebet,$iperiode);
				if($tmp) 
					$vsaldoaw1		  = $tmp->v_saldo_awal;
				else 
					$vsaldoaw1		  = 0;
				if($tmp) 
					$vmutasidebet1	= $tmp->v_mutasi_debet;
				else
					$vmutasidebet1	= 0;
				if($tmp) 
					$vmutasikredit1	= $tmp->v_mutasi_kredit;
				else
					$vmutasikredit1	= 0;
				if($tmp) 
					$vsaldoak1		  = $tmp->v_saldo_akhir;
				else
					$vsaldoak1		  = 0;
				
				$acckredit		    = KasBesar;
				$namakredit		    = $this->mmaster->namaacc($acckredit);
				$saldoawkredit	  = $this->mmaster->carisaldo($acckredit,$iperiode);
				if($tmp) 
					$vsaldoaw2		  = $tmp->v_saldo_awal;
				else
					$vsaldoaw2		  = 0;
				if($tmp) 
					$vmutasidebet2	= $tmp->v_mutasi_debet;
				else
					$vmutasidebet2	= 0;
				if($tmp) 
					$vmutasikredit2	= $tmp->v_mutasi_kredit;
				else
					$vmutasikredit2	= 0;
				if($tmp) 
					$vsaldoak2		= $tmp->v_saldo_akhir;
				else
					$vsaldoak2		= 0;
				$this->mmaster->inserttransitemdebet($accdebet,$ireff,$namadebet,'t','t',$iarea,$egirodescription,$vjumlah,$dbukti);
				$this->mmaster->updatesaldodebet($accdebet,$iperiode,$vjumlah);
				$this->mmaster->inserttransitemkredit($acckredit,$ireff,$namakredit,'f','t',$iarea,$egirodescription,$vjumlah,$dbukti);
				$this->mmaster->updatesaldokredit($acckredit,$iperiode,$vjumlah);
				$this->mmaster->insertgldebet($accdebet,$ireff,$namadebet,'t',$iarea,$vjumlah,$dbukti,$egirodescription);
				$this->mmaster->insertglkredit($acckredit,$ireff,$namakredit,'f',$iarea,$vjumlah,$dbukti,$egirodescription);
		  }
########## End Of Posting ##########
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
	        while($row=pg_fetch_assoc($rs)){
		        $ip_address	  = $row['ip_address'];
		        break;
	        }
        }else{
	        $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
	        $now	  = $row['c'];
        }
        $pesan='Cek Pelunasan Hutang No:'.$ipl.' Supplier:'.$isupplier;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses'] = true;
				$data['inomor']	= $ipl;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
