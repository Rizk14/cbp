<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listks');
			$data['iperiode']	= '';
			$this->load->view('listks/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listks/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select   * from tm_ic_convertion where to_char(d_ic_convertion::timestamp with time zone, 'yyyymm'::text)='$iperiode' 
			                            and (upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%' or upper(i_ic_convertion) like '%$cari%'
			                            or upper(i_refference) like '%$cari%') order by d_ic_convertion asc",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('listks/mmaster');
			$data['page_title'] = $this->lang->line('listks');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->baca($iperiode,$cari,$config['per_page'],$this->uri->segment(6));

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Konversi Stock Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listks/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listks/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select * from tm_ic_convertion
                                  where to_char(d_ic_convertion::timestamp with time zone, 'yyyymm'::text)='$iperiode' and
                                  upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%' or upper(i_ic_convertion) like '%$cari%'
                                  order by i_ic_convertion desc",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('listks/mmaster');
			$data['page_title'] = $this->lang->line('listks');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode,$cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('listks/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	/*function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listks/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tm_ic_convertion
										where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'
										or upper(i_ic_convertion) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listks');
			$this->load->model('listks/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$data['cari'] = $cari;

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Konversi Stock';
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listks/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}*/
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listks');
			$this->load->view('listks/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iicconvertion	= $this->uri->segment(4);
			$this->load->model('listks/mmaster');
			$istore				= 'AA';
			$istorelocation		= '01';
			$istorelocationbin	= '00';
      $this->db->trans_begin();
			$this->mmaster->delete($iicconvertion,$istore,$istorelocation,$istorelocationbin);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus Konversi Stock No:'.$iicconvertion.' Gudang:'.$istore;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
      if ($this->db->trans_status()===false)
			{
				$this->db->trans_rollback();
			}else{
#        $this->db->trans_rollback();
		    $this->db->trans_commit();
      }    
			$data['page_title'] = $this->lang->line('listks');
			$config['base_url'] = base_url().'index.php/listks/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tm_ic_convertion
										where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'
										or upper(i_ic_convertion) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listks');
			$this->load->model('listks/mmaster');
			$data['cari'] = $cari;
      $iperiode=$this->uri->segment(5);
      $data['iperiode']=$iperiode;
#			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
      $data['isi']	= $this->mmaster->baca($iperiode,$cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('listks/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	/*function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu76')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listks/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tm_ic_convertion
										where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'
										or upper(i_ic_convertion) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listks');
			$this->load->model('listks/mmaster');
			$data['cari'] = $cari;
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('listks/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}*/
}
?>
