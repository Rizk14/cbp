<?php 
class Main extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if ($this->session->userdata('logged_in')){
			$this->load->view('main/index.php');
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
