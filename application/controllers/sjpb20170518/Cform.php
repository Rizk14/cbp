<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		)
		{
			$dsjpb  = $this->input->post('dsj', TRUE);
			if($dsjpb!='')
			{
				$tmp=explode("-",$dsjpb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsjpb=$th."-".$bl."-".$hr;
   			$thbl=$th.$bl;
			}
			$iarea	= $this->input->post('iarea', TRUE);
			$isjp	= $this->input->post('isjp', TRUE);
			$dsjp	= $this->input->post('dsjp', TRUE);
			if($dsjp!='')
			{
				$tmp=explode("-",$dsjp);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsjp=$th."-".$bl."-".$hr;
			}
			$eareaname	= $this->input->post('eareaname', TRUE);
      	$icustomer	= $this->input->post('icustomer',TRUE);
      	$ispg	  = $this->input->post('ispg',TRUE);
			$vsjpb	= $this->input->post('vsjpb',TRUE);
			$vsjpb	= str_replace(',','',$vsjpb);
			$jml	  = $this->input->post('jml', TRUE);

			if($dsjpb!='' && $eareaname!='' && $isjp!='' && $icustomer!='')
			{
				$gaono=true;
				for($i=1;$i<=$jml;$i++)
				{
					$cek=$this->input->post('chk'.$i, TRUE);
					if($cek=='on'){
						$gaono=false;
					}
					if(!$gaono) break;
				}
				if(!$gaono)
				{
					$this->db->trans_begin();
					$this->load->model('sjpb/mmaster');
					$istore	= $this->input->post('istore', TRUE);
          		if($istore=='PB')
          		{
            		$istorelocation		= '00';
					}
					else
					{
            		$istorelocation		= 'PB';		
					}
					#$istorelocationbin= substr($ispg,2,2);
          		$istorelocationbin	= '00';
					$areasj=$iarea;
					$isjpb	 		= $this->mmaster->runningnumbersj($areasj,$thbl);
					$this->mmaster->insertsjpb($isjpb, $isjp, $icustomer, $iarea, $ispg, $dsjpb, $dsjp, $vsjpb);
					$this->mmaster->updatesjp($isjp,$iarea,$isjpb,$dsjpb);
					for($i=1;$i<=$jml;$i++)
					{
						$cek=$this->input->post('chk'.$i, TRUE);
						if($cek=='on')
						{
						  	$iproduct		  = substr($this->input->post('iproduct'.$i, TRUE),0,7);
              			$ipricegroup  = substr($this->input->post('iproduct'.$i, TRUE),7,2);
						  	$iproductgrade= 'A';
						  	$iproductmotif= $this->input->post('motif'.$i, TRUE);
						  	$eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  	$vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
						  	$vunitprice		= str_replace(',','',$vunitprice);
						  	$ndeliver	  	= $this->input->post('ndeliver'.$i, TRUE);
						  	$ndeliver		  = str_replace(',','',$ndeliver);
						  	$norder   		= $this->input->post('norder'.$i, TRUE);
						  	$norder		    = str_replace(',','',$norder);
						  	if($ndeliver>0)
						  	{
							  	$this->mmaster->insertsjpbdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                                    			     			$vunitprice,$isjpb,$iarea,$i,$dsjpb,$ipricegroup);
					      	$this->mmaster->updatesjpdetail($isjp,$iarea,$iproduct,$iproductgrade,$iproductmotif,$ndeliver);
	###
                			$trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                			if(isset($trans))
                			{
                  			foreach($trans as $itrans)
                  			{
			                    $q_aw =$itrans->n_quantity_awal;
			                    $q_ak =$itrans->n_quantity_akhir;
			                    $q_in =$itrans->n_quantity_in;
			                    $q_out=$itrans->n_quantity_out;
			                    break;
			                  }
                			}
                			else
                			{
                  			$trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                  			if(isset($trans))
                  			{
	                    			foreach($trans as $itrans)
	                    			{
			                      	$q_aw =$itrans->n_quantity_stock;
			                      	$q_ak =$itrans->n_quantity_stock;
			                      	$q_in =0;
			                      	$q_out=0;
			                      	break;
			                    	}
                  			}
                  			else
                  			{
                    $q_aw=0;
                    $q_ak=0;
                    $q_in=0;
                    $q_out=0;
                  }
                }
                $this->mmaster->inserttrans04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isjpb,$q_in,$q_out,$ndeliver,$q_aw,$q_ak);
                $th=substr($dsjpb,0,4);
                $bl=substr($dsjpb,5,2);
                $emutasiperiode=$th.$bl;
		
                if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
                {
                  $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
                }else{
                  $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode,$q_aw);
                }
                if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
                {
                  $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$q_ak);
                }else{
                  $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ndeliver,$q_aw);
                }
  ###              
						  }else{
#                $this->mmaster->updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$iarea);
              }
						 #}else{
						 # $iproduct		= $this->input->post('iproduct'.$i, TRUE);
						 # $iproductgrade	= 'A';
						 # $iproductmotif	= $this->input->post('motif'.$i, TRUE);
             # $this->mmaster->updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,0,$iarea);
            }
					}
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
#						$this->db->trans_rollback();
						$this->db->trans_commit();
						$sess=$this->session->userdata('session_id');
						$id=$this->session->userdata('user_id');
						$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
						$rs		= pg_query($sql);
						if(pg_num_rows($rs)>0){
							while($row=pg_fetch_assoc($rs)){
								$ip_address	  = $row['ip_address'];
								break;
							}
						}else{
							$ip_address='kosong';
						}
						$query 	= pg_query("SELECT current_timestamp as c");
						while($row=pg_fetch_assoc($query)){
							$now	  = $row['c'];
						}
						$pesan='Input SJPB No:'.$isjpb;
						$this->load->model('logger');
						$this->logger->write($id, $ip_address, $now , $pesan );
						$data['sukses']			= true;
						$data['inomor']			= $isjpb;
						$this->load->view('nomor',$data);
					}
				}
			}else{
				$data['page_title'] = $this->lang->line('sjpb');
				$data['isj']='';
				$data['isi']='';
				$data['detail']="";
				$data['jmlitem']="";
				$data['dsj']='';
				$data['isjp']='';
				$data['dsjp']='';
        if($this->uri->segment(5)!='')
        {
				  $data['iarea']=$this->uri->segment(5);
				  $data['istore']=$this->uri->segment(6);
				  $data['eareaname'] =str_replace('%20',' ' ,$this->uri->segment(4));
        }else{
				  $data['iarea']     ='';
				  $data['istore']    ='';
          $data['istorelocation']='';
				  $data['eareaname'] ='';
        }
				$data['ispg']    = '';
				$data['vsjp']    = '';
        $data['dsj']     = date('Y-m-d');
  			$data['ecustomername']='';
  			$data['icustomer']='';
				$this->load->view('sjpb/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjpb');
			$this->load->view('sjpb/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjpb')." update";
			if($this->uri->segment(4)!=''){
				$isj 	  = $this->uri->segment(4);
				$icustomer  = $this->uri->segment(5);
				$dfrom	= $this->uri->segment(6);
				$dto 	  = $this->uri->segment(7);
#				$ispb	  = $this->uri->segment(8);
				$data['isj'] 	  = $isj;
				$data['icustomer']	= $icustomer;
				$data['dfrom']	= $dfrom;
				$data['dto']	  = $dto;

				$this->load->model('sjpb/mmaster');
				$data['isi']=$this->mmaster->baca($isj,$icustomer);
				$data['detail']=$this->mmaster->bacadetail($isj,$icustomer);
		    $query        = $this->db->query(" select a.i_product as kode
						           from tr_product_motif a,tr_product c, tm_sjpb_item b
						           where a.i_product=c.i_product 
						           and b.i_product_motif=a.i_product_motif
						           and c.i_product=b.i_product
						           and b.i_sjpb='$isj'",false);
  			$data['jmlitem'] = $query->num_rows();
				$this->db->select("	a.*, b.e_area_name, c.e_customer_name from tm_sjpb a, tr_area b, tr_customer c
									          where a.i_sjpb ='$isj' and a.i_customer=c.i_customer
									          and a.i_area=b.i_area", false);
				$query = $this->db->get();
				if ($query->num_rows() > 0){
					foreach($query->result() as $row){
						$data['dsj']=$row->d_sjpb;
#						$data['ispb']=$row->i_spb;
#						$data['dspb']=$row->d_spb;
#           $que = $this->db->query("select i_store from tr_area where i_area='$iareasj'");
#           $st=$que->row();
#           $data['istore']=$st->i_store;
            $data['isjp']=$row->i_sjp;
						$data['eareaname']=$row->e_area_name;
						$data['vsjpb']=$row->v_sjpb;
						$data['icustomer']=$row->i_customer;
						$data['ecustomername']=$row->e_customer_name;
						$data['ispg']=$row->i_spg;
					}
				}
		 		$this->load->view('sjpb/vformupdate',$data);
			}else{
				$this->load->view('sjpb/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj  = $this->input->post('isj', TRUE);
			$isjp  	= $this->input->post('isjp', TRUE);
			$dsj 	= $this->input->post('dsj', TRUE);
 			$iarea	= $this->input->post('iarea', TRUE);
			$dsjp  	= $this->input->post('dsjp', TRUE);
			if($dsj!=''){
				$tmp	  = explode("-",$dsj);
				$th	    = $tmp[2];
				$bl	    = $tmp[1];
				$hr	    = $tmp[0];
				$dsj	= $th."-".$bl."-".$hr;
				$thbl	  = substr($th,2,2).$bl;
				$tmpsj	= explode("-",$isj);
				$firstsj= $tmpsj[0];
				$lastsj	= $tmpsj[2];
				$newsj	= $firstsj."-".$thbl."-".$lastsj;
			}
			if($dsjp!=''){
				$tmp  =explode("-",$dsjp);
				$th   =$tmp[2];
				$bl   =$tmp[1];
				$hr   =$tmp[0];
				$dsjp =$th."-".$bl."-".$hr;
			}
			$eareaname  	= $this->input->post('eareaname', TRUE);
			$ispg     		= $this->input->post('ispg',TRUE);
			$icustomer		= $this->input->post('icustomer',TRUE);
			$vsjpb      	= $this->input->post('vsjpb',TRUE);
			$vsjpb      	= str_replace(',','',$vsjpb);

			$jml	  = $this->input->post('jml', TRUE);
			
			if($isj!='' && $dsj!='' && $icustomer!='')
			{
				$gaono=true;
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					if($cek=='on'){
						$gaono=false;
					}
					if(!$gaono) break;
				}
				if(!$gaono){
					$this->db->trans_begin();
					$this->load->model('sjpb/mmaster');
					$istore	= $this->input->post('istore', TRUE);
					if($istore=='PB'){
            $istorelocation		= '00';
					}else{
            $istorelocation		= 'PB';		
					}
          $istorelocationbin	= '00';
				  $this->mmaster->updatesjheader($isj,$dsj,$isjp,$dsjp,$iarea,$icustomer,$ispg,$vsjpb);
					
#					$this->mmaster->updatesjp($isjpb,$iarea,$isjp,$dsjpb);
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
						$iproduct	= $this->input->post('iproduct'.$i, TRUE);
						$iproductgrade= $this->input->post('iproductgrade'.$i, TRUE);
						$iproductmotif= $this->input->post('iproductmotif'.$i, TRUE);
						$ndeliver		= $this->input->post('ndeliver'.$i, TRUE);
						$ndeliver		= str_replace(',','',$ndeliver);
						$ipricegroup	= $this->input->post('ipricegroup'.$i, TRUE);
#						$this->mmaster->deletesjpbdetail($iproduct,$iproductgrade,$iproductmotif,$isj,$iarea,$ipricegroup);

						$th=substr($dsj,0,4);
						$bl=substr($dsj,5,2);
						$emutasiperiode=$th.$bl;
						$tra=$this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj);
            			if( ($ndeliver!='') && ($ndeliver!=0) ){
					    $this->mmaster->updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
					    $this->mmaster->updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver);
            			}
						if($cek=='on'){
						  $iproduct		  = substr($this->input->post('iproduct'.$i, TRUE),0,7);
						  if(substr($this->input->post('iproduct'.$i, TRUE),7,2)=='' or substr($this->input->post('iproduct'.$i, TRUE),7,2)=='00'){
						    $ipricegroup  = $this->input->post('ipricegroup'.$i, TRUE);
						  }else{
						    $ipricegroup  = substr($this->input->post('iproduct'.$i, TRUE),7,2);
						  }
						  $ndeliver		= $this->input->post('ndeliver'.$i, TRUE);
						  $ndeliver		= str_replace(',','',$ndeliver);
						  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice	= $this->input->post('vproductmill'.$i, TRUE);
						  $vunitprice	= str_replace(',','',$vunitprice);
						  $iproductgrade= $this->input->post('iproductgrade'.$i, TRUE);
						  $iproductmotif= $this->input->post('iproductmotif'.$i, TRUE);
						  
						  if($ndeliver>0){
							  $this->mmaster->updatesjpbdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                                    			     $vunitprice,$isj,$iarea,$i,$dsj,$ipricegroup);

						    $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
					      if(isset($trans)){
						      foreach($trans as $itrans)
						      {
						        $q_aw =$itrans->n_quantity_stock;
						        $q_ak =$itrans->n_quantity_stock;
						        $q_in =0;
						        $q_out=0;
						        break;
						      }
						    }else{
							    $q_aw=0;
							    $q_ak=0;
							    $q_in=0;
							    $q_out=0;
						    }	
							  $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$ndeliver,$q_aw,$q_ak,$tra);
							  if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
							  {
							    $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
							  }else{
							    $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode,$q_aw);
							  }
							  if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
							  {
							    $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$q_ak);
							  }else{
							    $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ndeliver,$q_aw);
							  }
							  #$this->mmaster->updatesjpitem($isjp,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$iarea);
						  }
						}else{
              #$this->mmaster->updatesjpitem($isjp,$iproduct,$iproductgrade,$iproductmotif,0,$iarea);
            }
					}
					if (($this->db->trans_status() === FALSE))
					{
						$this->db->trans_rollback();
					}else{
#						$this->db->trans_rollback();
						$this->db->trans_commit();
						$sess=$this->session->userdata('session_id');
						$id=$this->session->userdata('user_id');
						$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
						$rs		= pg_query($sql);
						if(pg_num_rows($rs)>0){
							while($row=pg_fetch_assoc($rs)){
								$ip_address	  = $row['ip_address'];
								break;
							}
						}else{
							$ip_address='kosong';
						}
						$query 	= pg_query("SELECT current_timestamp as c");
						while($row=pg_fetch_assoc($query)){
							$now	  = $row['c'];
						}
						$pesan='Edit SJPB No:'.$isj;
						$this->load->model('logger');
						$this->logger->write($id, $ip_address, $now , $pesan );
						$data['sukses']			= true;
						if($sjnew=1){
							$data['inomor']			= $newsj;
						}else{
							$data['inomor']			= $isjpb;
						}	
						$this->load->view('nomor',$data);
					}
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj	= $this->input->post('isjdelete', TRUE);
			$this->load->model('sjpb/mmaster');
			$this->mmaster->delete($isj);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus SJPB No:'.$isj;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('sjpb');
			$data['isj']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('sjpb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj			= $this->input->post('isjdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
  		$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$this->db->trans_begin();
			$this->load->model('sjpb/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $isj, $iproductmotif);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
				$data['page_title'] = $this->lang->line('sjpb')." Update";
				$query = $this->db->query("select * from tm_sjpb_item where i_sjpb = '$isj'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['isj'] = $isj;
				$data['isi']=$this->mmaster->baca($isj);
				$data['detail']=$this->mmaster->bacadetail($isj);
				$this->load->view('sjpb/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['spb']=$this->uri->segment(5);
			$spb=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/sjpb/cform/product/'.$baris.'/'.$spb.'/';
			$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
							                    a.e_product_motifname as namamotif, 
							                    c.e_product_name as nama,b.v_unit_price as harga
							                    from tr_product_motif a,tr_product c, tm_spb_item b
							                    where a.i_product=c.i_product 
							                    and b.i_product_motif=a.i_product_motif
							                    and c.i_product=b.i_product
							                    and b.i_spb='$spb' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('sjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($spb,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$data['spb']=$spb;
			$this->load->view('sjpb/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/sjpb/cform/product/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										              c.e_product_name as nama,c.v_product_mill as harga
										              from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product
										              and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
									                ,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('sjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('sjpb/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
      $iuser   = $this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/sjpb/cform/area/index/';
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('sjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($area1,$iuser,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sjpb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$config['base_url'] = base_url().'index.php/sjpb/cform/area/index/';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                              where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                              and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($area1,$iuser,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sjpb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function sjp()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area=$this->uri->segment(4);
			$tgl =$this->uri->segment(5);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/sjpb/cform/sjp/'.$area.'/'.$tgl.'/';
#			if($area1=='00'||$area2=='00'||$area3=='00'||$area4=='00'||$area5=='00'){
				$query = $this->db->query(" select distinct(a.*) from tm_sjp a, tm_sjp_item b, tm_spmb c
                                    where a.i_area='$area' and a.i_spmb=c.i_spmb 
                                    and c.f_spmb_consigment='t' and a.f_sjp_cancel='f'
                                    and c.f_spmb_cancel='f' and b.n_saldo>0
                                    and not a.d_sjp_receive is null
                                    and a.i_sjp=b.i_sjp and a.i_area=b.i_area
                                    and a.f_sjpb_close='0'
                                    order by a.i_sjp",false);
/*				$query = $this->db->query(" select a.* from tm_sjp a, tm_spmb c
                                    where a.i_area='$area' and a.i_spmb=c.i_spmb and c.f_spmb_consigment='t' 
                                    and a.f_sjp_cancel='f' and c.f_spmb_cancel='f' and a.i_sjpb isnull
                                    and not a.d_sjp_receive is null
                                    and a.i_sjp not in (select i_sjp from tm_sjpb b where a.i_area=b.i_area)",false);*/
#			}else{
#				$query = $this->db->query(" select a.* from tm_sjp a where a.i_area='$area'
#                                    and a.i_sjp not in (select i_sjp from tm_sjpb b where a.i_area=b.i_area)",false);
#			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('sjpb/mmaster');
			$data['page_title'] = $this->lang->line('listsjp');
			$data['area']=$area;
			$data['isi']=$this->mmaster->bacasjp($area1,$area2,$area3,$area4,$area5,$area,$config['per_page'],$this->uri->segment(6));
			$this->load->view('sjpb/vlistsjp', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisjp()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$area = $this->input->post('iarea', FALSE);
			$config['base_url'] = base_url().'index.php/sjpb/cform/sjp/'.$area.'/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select distinct(a.*) from tm_sjp a, tm_sjp_item b, tm_spmb c
                                    where a.i_area='$area' and a.i_spmb=c.i_spmb 
                                    and c.f_spmb_consigment='t' and a.f_sjp_cancel='f'
                                    and c.f_spmb_cancel='f' and b.n_saldo>0
                                    and not a.d_sjp_receive is null
                                    and a.i_sjp=b.i_sjp and a.i_area=b.i_area
                                    and a.f_sjpb_close='0'
                                    and a.i_sjp like '%$cari%'",false);
/*			$query = $this->db->query("	select a.* from tm_sjp a, tm_spmb c
                                    where a.i_area='$area' and a.i_spmb=c.i_spmb and c.f_spmb_consigment='t' 
                                    and a.f_sjp_cancel='f' and c.f_spmb_cancel='f' and a.i_sjpb isnull
                                    and not a.d_sjp_receive is null
                                    and a.i_sjp not in (select i_sjp from tm_sjpb b where a.i_area=b.i_area)
                                    and a.i_sjp like '%$cari%'",false);*/
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sjpb/mmaster');
			$data['area']=$area;
			$data['page_title'] = $this->lang->line('listsjp');
			$data['isi']=$this->mmaster->carisjp($cari,$area1,$area2,$area3,$area4,$area5,$area,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sjpb/vlistsjp', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/sjpb/cform/index/';
			$query = $this->db->query("select * from tm_spb
						   where upper(i_spb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('sjpb/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spb');
			$data['isj']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('sjpb/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function hitung()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dsj 	= $this->uri->segment(8);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
			}
			$isjp		      = $this->uri->segment(4);
			$dsjp		      = $this->uri->segment(5);
			$iarea		    = $this->uri->segment(6);
			$eareaname	  = str_replace('%20',' ',$this->uri->segment(7));
			$istore		    = $this->uri->segment(9);
			$ecustomername= $this->uri->segment(10);
			$icustomer		= $this->uri->segment(11);
			$ispg		      = $this->uri->segment(12);
			$query        = $this->db->query(" select a.i_product as kode
							       from tr_product_motif a,tr_product c, tm_sjp_item b
							       where a.i_product=c.i_product 
							       and b.i_product_motif=a.i_product_motif
							       and c.i_product=b.i_product
							       and b.i_sjp='$isjp' and b.i_area='$iarea' ",false);
			$data['jmlitem'] = $query->num_rows(); 
			$data['page_title'] = $this->lang->line('sjpb');
			$data['isj']='';
			$this->load->model('sjpb/mmaster');
			$data['isi']="xxxxx";
			$data['dsj']=$dsj;
			$data['isjp']=$isjp;
			$data['dsjp']=$dsjp;
			$data['iarea']=$iarea;
			$data['istore']=$istore;
			$data['ispg']=$ispg;
			$data['eareaname']=$eareaname;
      $data['ecustomername']=$ecustomername;
      $data['icustomer']=$icustomer;
			$query=$this->db->query("	select v_sjp from tm_sjp where i_sjp='$isjp' and i_area='$iarea' ",false);
			if ($query->num_rows() > 0){
				foreach($query->result() as $row){
					$vsjp	    = $row->v_sjp;
				}
			}
			$data['vsjp']	=$vsjp;
			$data['detail']=$this->mmaster->product($isjp,$iarea);
			$this->load->view('sjpb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari  = strtoupper($this->input->post('cari', FALSE));
      $iarea = strtoupper($this->input->post('iarea', FALSE));
 			if($iarea=='') $iarea=$this->uri->segment(4);
      if($this->uri->segment(5)!='x01' && $this->uri->segment(5)!=''){
        if($cari=='') $cari=$this->uri->segment(5);
        $config['base_url'] = base_url().'index.php/sjpb/cform/customer/'.$iarea.'/'.$cari.'/';
      }else{
        $config['base_url'] = base_url().'index.php/sjpb/cform/customer/'.$iarea.'/x01/';
      }
			$query 	= $this->db->query("select a.i_customer from tr_customer a, tr_customer_consigment b, tr_spg c
                                  where a.i_area='$iarea' and a.i_customer=b.i_customer and a.i_customer=c.i_customer
										              and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('sjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('sjpb/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function harga()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu289')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(5);
			$baris=$this->uri->segment(5);
			$data['product']=substr($this->uri->segment(4),0,7);
			$product=substr($this->uri->segment(4),0,7);
			$config['base_url'] = base_url().'index.php/sjpb/cform/harga/'.$product.'/'.$baris.'/';
			$query = $this->db->query(" select b.i_product as kode, b.i_price_group, a.i_product_motif as motif,
						                      a.e_product_motifname as namamotif, b.v_product_retail as harga,
						                      c.e_product_name as nama
						                      from tr_product_motif a,tr_product c, tr_product_priceco b
						                      where a.i_product=c.i_product and a.i_product=b.i_product and a.i_product='$product'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('sjpb/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($product,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$data['product']=$product;
			$this->load->view('sjpb/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
