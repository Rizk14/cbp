<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		        ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawancuti/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query=$this->db->query(" select * from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department)
			   left join tr_karyawan_cuti e on(a.i_nik=e.i_nik)
			   where e.i_periode='2016' and (upper(a.i_nik) like '%$cari%' or upper(b.e_nama_karyawan_lengkap) like '%$cari%')",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_karyawancuti');
			$data['i_nik']='';
			$this->load->model('karyawancuti/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Karyawan Cuti";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('karyawancuti/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_karyawancuti')." update";
			if($this->uri->segment(4)){
				$i_nik = $this->uri->segment(4);
				$query = $this->db->query("select * from tr_karyawan_cuti_item where i_nik = '$i_nik'");
				$data['jmlcuti'] = $query->num_rows(); 	
				$data['i_nik'] = $i_nik;
				$th_skrng=date('Y');
				$this->load->model('karyawancuti/mmaster');
				$data['isi']=$this->mmaster->baca($i_nik,$th_skrng);
				$data['detail']=$this->mmaster->bacadetail($i_nik,$th_skrng);
			//	var_dump($data['detail']);
			//	die();	 	

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Karyawan Cuti ('.$i_nik.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('karyawancuti/vmainform',$data);
			}else{
				$this->load->view('karyawancuti/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			 $i_nik			    		= $this->input->post('i_nik', TRUE);
			 $v_cuti_lebaran			= $this->input->post('v_cuti_lebaran', TRUE);
			// "<br>";
			 $v_cuti_lebaranx			= $this->input->post('v_cuti_lebaranx', TRUE);
			//echo "<br>";
			 $v_saldo_cuti				= $this->input->post('v_saldo_cuti', TRUE);
			//echo "<br>";
			 $baris						= $this->input->post('baris', TRUE);
			// $d_cuti_awal				= $this->input->post('d_cuti_awal', TRUE);
			 $th_skrng					= date('Y');


			$this->load->model('karyawancuti/mmaster');
			$x=$this->mmaster->jumlahcutiambil($i_nik,$th_skrng);
			$jumlah_cuti=$x->jumlah_cuti;
			$jumlah_cuti;
			//var_dump($jumlahcutiambil);
			//$tot=$jumlahcutiambil['jumlah_cuti'];
			$query=$this->db->query("select * from tr_karyawan_cuti_item where i_nik='$i_nik' and i_periode='$th_skrng'");
            $jml=$query->num_rows();
			for($i=1;$i<=$jml;$i++){
            
               	  $i_nik               = $this->input->post('i_nik'.$i, TRUE);
              //echo "<br>";
                  $d_cuti_awal         = $this->input->post('d_cuti_awal'.$i, TRUE);
              //echo "<br>";
                  $d_cuti_akhir		   = $this->input->post('d_cuti_akhir'.$i, TRUE);
              //echo "<br>";
                  $v_jumlah_cuti 	   = $this->input->post('v_jumlah_cuti'.$i, TRUE);
              //echo "<br>";
                  $e_alasan       	   = $this->input->post('e_alasan'.$i, TRUE);
			

			}
			//echo "<br>";
			//echo $totalubah=$v_cuti_lebaran+$v_saldo_cuti+$jumlah_cuti;
			//echo "<br>";
			 $totalawal=$v_cuti_lebaranx+$v_saldo_cuti+$jumlah_cuti;
			//echo "<br>";
			 $totsementara=$v_cuti_lebaran+$jumlah_cuti;
			//echo "<br>";
			 $saldo_cuti=$totalawal-$totsementara;
			//echo $totalseluruh=$totalawal+$jumlah_cuti;

			$this->mmaster->updatedetail(
											$i_nik,$d_cuti_awal,$d_cuti_akhir,$e_alasan,$v_jumlah_cuti,$th_skrng		 
					    			  	);
			$this->mmaster->updateheader(
											$i_nik,$v_cuti_lebaran,$saldo_cuti,$th_skrng
					      				);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Karyawan Cuti ('.$i_nik.')';
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);
	        
      		$config['base_url'] = base_url().'index.php/karyawancuti/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query=$this->db->query("select * from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department)
			   left join tr_karyawan_cuti e on(a.i_nik=e.i_nik)
			   where e.i_periode='$th_skrng' and (upper(a.i_nik) like '%$cari%' or upper(b.e_nama_karyawan_lengkap) like '%$cari%')",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_karyawancuti');
			$data['i_nik']='';
			$this->load->model('karyawancuti/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('karyawancuti/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct	= $this->uri->segment(4);
			$this->load->model('productbase/mmaster');
			$this->mmaster->delete($iproduct);

			$config['base_url'] = base_url().'index.php/productbase/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_product');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['page_title'] = $this->lang->line('master_product');
			$data['iproduct']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('productbase/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawancuti/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query=$this->db->query("select * from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department)
			   left join tr_karyawan_cuti e on(a.i_nik=e.i_nik)
			   where e.i_periode='2016' and (upper(a.i_nik) like '%$cari%' or upper(b.e_nama_karyawan_lengkap) like '%$cari%')",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('karyawancuti/mmaster');
			$data['isi']		  =$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title']   = $this->lang->line('master_karyawancuti');
			$data['i_nik']		  ='';
	 		$this->load->view('karyawancuti/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caridepartment()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/department/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("	select * from tr_department where upper(i_department) like '%$cari%' or upper(e_department_name) like '%$cari%'",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('karyawan/mmaster');
			$data['page_title'] 	= $this->lang->line('list_department');
			$data['isi']			=$this->mmaster->caridepartment($config['per_page'],$this->uri->segment(5));
			$this->load->view('karyawan/vlistdepartment', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function department()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] 	= base_url().'index.php/karyawan/cform/department/index/';
			$query 					= $this->db->query("select * from tr_department",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page']   	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('karyawan/mmaster');
			$data['page_title'] 	= $this->lang->line('list_area');
			$data['isi']			=$this->mmaster->bacadepartment($config['per_page'],$this->uri->segment(5));
			$this->load->view('karyawan/vlistdepartment', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}	

	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query = $this->db->query("	select * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('karyawan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('karyawan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/area/index/';
			$query = $this->db->query("select * from tr_area",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('karyawan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('karyawan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carikaryawan_status()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/karyawan_status/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query = $this->db->query("	select * from tr_karyawan_status where upper(i_karyawan) like '%$cari%' or upper(e_karyawan_status) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('karyawan/mmaster');
			$data['page_title'] = $this->lang->line('list_karyawan_status');
			$data['isi']=$this->mmaster->carikaryawanstatus($config['per_page'],$this->uri->segment(5));
			$this->load->view('karyawan/vlistkaryawanstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function karyawan_status()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/karyawan/cform/karyawan_status/index/';
			$query = $this->db->query("select * from tr_karyawan_status",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('karyawan/mmaster');
			$data['page_title'] = $this->lang->line('list_karyawan_status');
			$data['isi']=$this->mmaster->bacakaryawanstatus($config['per_page'],$this->uri->segment(5));
			$this->load->view('karyawan/vlistkaryawanstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function update_input()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$i_nik						= $this->input->post('i_nik', TRUE);
			$d_cuti_awal				= $this->input->post('d_cuti_awal', TRUE);
			$d_cuti_akhir				= $this->input->post('d_cuti_akhir', TRUE);
			$v_jumlah_cuti				= $this->input->post('v_jumlah_cuti', TRUE);
			$e_alasan					= $this->input->post('e_alasan', TRUE);
			$e_nama_karyawan_lengkap	= $this->input->post('e_nama_karyawan_lengkap', TRUE);
			$d_masaberlaku_cutiawal		= $this->input->post('d_masaberlaku_cutiawal', TRUE);
			
			if($d_cuti_awal!=''){
				$tmp=explode("-",$d_cuti_awal);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$d_cuti_awal=$th."-".$bl."-".$hr;
			}else{
       		$d_cuti_awal=null;
      }

      		if($d_cuti_akhir!=''){
					$tmp=explode("-",$d_cuti_akhir);
					$th=$tmp[2];
					$bl=$tmp[1];
					$hr=$tmp[0];
					$d_cuti_akhir=$th."-".$bl."-".$hr;
				}else{
	        $d_cuti_akhir=null;
	      }

	      	if($d_masaberlaku_cutiawal!=''){
					$tmp=explode("-",$d_masaberlaku_cutiawal);
					$th=$tmp[0];
					$bl=$tmp[1];
					$hr=$tmp[2];
					$d_masaberlaku_cutiawal=$th."-".$bl."-".$hr;
					$periode=$th;
				}else{
	        $d_masaberlaku_cutiawal=null;
	      }
				$this->load->model('karyawancuti/mmaster');
				$this->db->trans_begin();
				$data=$this->mmaster->ceksaldocuti($i_nik);
				$v_saldo=$data['v_saldo_cuti'];
				$v_saldo_akhir=$v_saldo-$v_jumlah_cuti;
				if($data!=NULL){							
					$this->mmaster->simpancutidetail($i_nik,$d_cuti_awal,$d_cuti_akhir,$v_jumlah_cuti,$e_alasan,$periode);
					$this->mmaster->updatekaryawancuti($i_nik,$v_saldo_akhir);
					
					$nomor=$i_nik." - ".$e_nama_karyawan_lengkap;
					if ($this->db->trans_status() === FALSE)
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
	          while($row=pg_fetch_assoc($query)){
	          	$now	  = $row['c'];
			      }
			      $pesan='Cuti Karyawan Dengan Identitas:'.$i_nik.' Nama:'.$e_nama_karyawan_lengkap;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan ); 

						$data['sukses']			= true;
						$data['inomor']			= $nomor;
						$this->load->view('nomor',$data);
					}
				}else{
					echo "ERROR DATA";
				}
			

		}else{
			$this->load->view('awal/index.php');

		}
	}
}
?>
