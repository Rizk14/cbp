<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu451')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listpenjualankonsinyasiperhari');
			$data['dfrom']='';
			$data['dto']='';
			$data['icustomer']='';
			$this->load->view('listpenjualankonsinyasiperhari/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu451')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			if($dfrom=='') $dfrom	= $this->uri->segment(4);
			$dto		= $this->input->post('dto');
			if($dto=='') $dto	= $this->uri->segment(5);
			$icustomer		= $this->input->post('icustomer');
			if($icustomer=='') $icustomer	= $this->uri->segment(6);
			$this->load->model('listpenjualankonsinyasiperhari/mmaster');
			$data['page_title'] = $this->lang->line('listpenjualankonsinyasiperhari');
			$data['dfrom']	  	= $dfrom;
			$data['dto']    		= $dto;
			$data['icustomer']	= $icustomer;
      $data['diskon']     = $this->mmaster->bacadiskon($dfrom,$dto,$icustomer);
			$data['isi']		    = $this->mmaster->bacaperiode($dfrom,$dto,$icustomer);
			$data['total']		    = $this->mmaster->bacatotal($dfrom,$dto,$icustomer);
/*			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Daftar Rekap Penjualan Konsinyasi Per Hari dari : '.$dfrom.' sampai : '.$dto.' Customer : '.$icustomer;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
*/
			$this->load->view('listpenjualankonsinyasiperhari/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu451')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listpenjualankonsinyasiperhari');
			$this->load->view('listpenjualankonsinyasiperhari/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu451')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listpenjualankonsinyasiperhari/cform/customer/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name
                                    from tr_spg a, tr_customer b, tr_area c
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area",false);
			}else{
				$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name
                                    from tr_spg a, tr_customer b, tr_area c
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area
                                    and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
										                or a.i_area = '$area4' or a.i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('listpenjualankonsinyasiperhari/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listpenjualankonsinyasiperhari/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu451')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listpenjualankonsinyasiperhari/cform/customer/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name
                                    from tr_spg a, tr_customer b, tr_area c
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area
                                    and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name
                                    from tr_spg a, tr_customer b, tr_area c
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area and
                                    (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                                    and (a.i_area = '$area1' or a.i_area = '$area2' 
              										  or a.i_area = '$area3' or a.i_area = '$area4' or a.i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('listpenjualankonsinyasiperhari/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listpenjualankonsinyasiperhari/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
