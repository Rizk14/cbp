<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transspbkons');
			$data['iperiode']='';
			$this->load->view('transspbkons/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode		= $this->input->post('iperiode');
			if($iperiode=='') $iperiode	= $this->uri->segment(4);
			$this->load->model('transspbkons/mmaster');
			$data['page_title'] = $this->lang->line('transspbkons');
			$data['iperiode']		= $iperiode;
			$data['isi']		    = $this->mmaster->bacaperiode($iperiode);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Buka Transfer SPB Konsinyasi Periode: '.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
      $data['ispb']='';
			$this->load->view('transspbkons/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('transspbkons');
			$this->load->view('transspbkons/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml  = $this->input->post('jml', TRUE);
			$iperiode= $this->input->post('iperiode', TRUE);
			$this->db->trans_begin();
			$this->load->model('transspbkons/mmaster');
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$iarea= $this->input->post('iarea'.$i, TRUE);
					$ispb = $this->input->post('ispb'.$i, TRUE);
          $qdis=$this->db->query("select i_spb from tm_spb where i_spb='$ispb' and i_area='$iarea' and i_sj isnull");
          if($qdis->num_rows()>0){
            $this->mmaster->update($ispb,$iarea);
          }else{
            $this->mmaster->insert($ispb,$iarea);
          }
				}
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
        $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		=  $this->db->query($sql);
		    if($rs->num_rows>0){
			    foreach($rs->result() as $tes){
				    $ip_address	  = $tes->ip_address;
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }

		    $data['user']	= $this->session->userdata('user_id');
		    $data['host']	= $ip_address;
		    $query 	= pg_query("SELECT current_timestamp as c");
		    while($row=pg_fetch_assoc($query)){
			    $now	  = $row['c'];
		    }
		    $pesan='Transfer SPB Konsinyasi Periode:'.$iperiode;
		    $this->load->model('logger');
		    $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $iperiode;
				$this->load->view('nomor',$data);
			  $this->db->trans_commit();
#				$this->db->trans_rollback();
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function edit()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu442')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('transspbkons')." update";
         if(
            ($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
           )
         {
            $ispb        = $this->uri->segment(4);
            $iarea       = $this->uri->segment(5);
            $dfrom       = $this->uri->segment(6);
            $dto         = $this->uri->segment(7);
            $ipricegroup = $this->uri->segment(8);
            $query = $this->db->query("select * from tm_transspbkons_item where i_spb = '$ispb' and i_area='$iarea'");
            $data['jmlitem']= $query->num_rows();
            $data['ispb']  = $ispb;
            $data['departement']=$this->session->userdata('departement');
            $this->load->model('transspbkons/mmaster');
            $data['isi']   = $this->mmaster->baca($ispb,$iarea);
            $data['detail']   = $this->mmaster->bacadetail($ispb,$iarea,$ipricegroup);

            $qnilaispb  = $this->mmaster->bacadetailnilaispb($ispb,$iarea,$ipricegroup);
            if($qnilaispb->num_rows()>0){
               $row_nilaispb  = $qnilaispb->row();
               $data['nilaispb'] = $row_nilaispb->nilaispb;
            }else{
               $data['nilaispb'] = 0;
            }
            $qnilaiorderspb   = $this->mmaster->bacadetailnilaiorderspb($ispb,$iarea,$ipricegroup);
            if($qnilaiorderspb->num_rows()>0){
               $row_nilaiorderspb   = $qnilaiorderspb->row();
               $data['nilaiorderspb']  = $row_nilaiorderspb->nilaiorderspb;
            }else{
               $data['nilaiorderspb']  = 0;
            }
            $qeket   = $this->db->query(" SELECT e_remark1 as keterangan from tm_transspbkons where i_spb ='$ispb' and i_area='$iarea' ");
            if($qeket->num_rows()>0){
               $row_eket   = $qeket->row();
               $data['keterangan']  = $row_eket->keterangan;
            }

            $data['iperiode'] = 'edit';
            $data['dfrom'] = $dfrom;
            $data['dto'] = $dto;
#            $data['iarea_awal'] = $iarea_awal;

            $this->load->view('transspbkons/vmainform',$data);
         }else{
            $this->load->view('transspbkons/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');

      }
   }
   function update()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu442')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb    = $this->input->post('ispb', TRUE);
         $iarea              = $this->input->post('iarea', TRUE);
         $nspbdiscount1 = $this->input->post('ncustomerdiscount1',TRUE);
         $vspbdiscount1 = $this->input->post('vcustomerdiscount1',TRUE);
         $vspbdiscounttotal   = $this->input->post('vspbdiscounttotal',TRUE);
         $vspb             = $this->input->post('vspb',TRUE);
         $nspbdiscount1 = str_replace(',','',$nspbdiscount1);
         $vspbdiscount1 = str_replace(',','',$vspbdiscount1);
         $vspbdiscounttotal   = str_replace(',','',$vspbdiscounttotal);
         $vspb      = str_replace(',','',$vspb);
         if(($iarea!='') && ($ispb!=''))
         {
            $benar="false";
            $this->db->trans_begin();
            $this->load->model('transspbkons/mmaster');
            $this->mmaster->updateheader($ispb, $iarea, $nspbdiscount1, $vspbdiscount1, $vspbdiscounttotal, $vspb);
            if ( ($this->db->trans_status() === FALSE) )
            {
                $this->db->trans_rollback();
            }else{
               $sess=$this->session->userdata('session_id');
               $id=$this->session->userdata('user_id');
               $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
                  $now    = $row['c'];
               }
               $pesan='Update SPB Konsinyasi Area '.$iarea.' No:'.$ispb;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );
               $this->db->trans_commit();
               $data['sukses']         = true;
               $data['inomor']         = $ispb;
               $this->load->view('nomor',$data);
            }
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
