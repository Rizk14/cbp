<?php
class Cform extends CI_Controller
{
	public $title 	= "Kas Kecil (NASIONAL)";
	public $folder 	= "exp-kk-lkhall";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model($this->folder . '/mmaster');
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu409') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data = [
				'page_title' 	=> $this->title,
				'folder' 		=> $this->folder,
				'datefrom' 		=> '',
				'dateto' 		=> '',
			];

			$this->load->view($this->folder . '/vform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu409') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$datefrom = $this->uri->segment('4');
			$dateto	  = $this->uri->segment('5');

			$iarea1   = $this->session->userdata('i_area');
			$idept 	  = $this->session->userdata('departement');

			if ($datefrom != '') {
				$tmp = explode("-", $datefrom);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				// $datefrom = $th . "-" . $bl . "-" . $hr;
				$periodeawal	= $hr . "/" . $bl . "/" . $th;
			}

			if ($dateto != '') {
				$tmp = explode("-", $dateto);
				$thto = $tmp[2];
				$blto = $tmp[1];
				$hrto = $tmp[0];
				$toto = $thto . "-" . $blto . "-" . $hrto;
			}

			$tmp  			= explode("-", $dateto);
			$det			= $tmp[0];
			$mon			= $tmp[1];
			$yir 			= $tmp[2];
			$dtos			= $yir . "/" . $mon . "/" . $det;
			$periodeakhir	= $det . "/" . $mon . "/" . $yir;

			$dtos			= $this->mmaster->dateAdd("d", 1, $dtos);
			$tmp 			= explode("-", $dtos);
			$det1			= $tmp[2];
			$mon1			= $tmp[1];
			$yir1 			= $tmp[0];
			$dtos			= $yir1 . "-" . $mon1 . "-" . $det1;

			$periode 		= date('Ym', strtotime($datefrom));
			$coaku			= KasKecil;
			$kasbesar		= KasBesar;
			$bank			= Bank;

			$this->db->select(" 	* FROM (
										SELECT a.*,
											b.e_area_name,
											CASE WHEN NOT c.i_pv ISNULL THEN c.i_pv
											ELSE d.i_rv END AS i_reff
										FROM
											(
												SELECT
													i_kb AS i_kk,
													d_kb AS d_kk,
													f_debet,
													e_description,
													'' AS i_kendaraan,
													0 AS n_km,
													v_kb AS v_kk,
													a.i_area,
													d_bukti,
													i_coa,
													a.e_coa_name,
													a.d_entry 
												FROM
													tm_kb a
												INNER JOIN tr_area ON (a.i_area = tr_area.i_area)
												WHERE
													a.i_periode = '$periode'
													AND a.d_kb >= to_date('$datefrom', 'dd-mm-yyyy')
													AND a.d_kb <= to_date('$dateto', 'dd-mm-yyyy')
													AND a.f_debet = 't'
													AND a.f_kb_cancel = 'f'
													AND a.i_coa IN(SELECT i_coa FROM tr_coa WHERE i_area <> '' AND e_coa_name LIKE '%Kas Kecil%')
													AND a.v_kb NOT IN (SELECT b.v_kk AS v_kb FROM tm_kk b WHERE b.d_kk = a.d_kb AND b.i_area = a.i_area AND b.f_kk_cancel = 'f' AND b.f_debet = 'f' AND b.i_coa LIKE '900-000%' AND b.i_periode = '$periode')
											UNION ALL
												SELECT
													i_kbank AS i_kk,
													d_bank AS d_kk,
													f_debet,
													e_description,
													'' AS i_kendaraan,
													0 AS n_km,
													v_bank AS v_kk,
													a.i_area,
													d_bank AS d_bukti,
													i_coa,
													a.e_coa_name,
													a.d_entry
												FROM
													tm_kbank a
												INNER JOIN tr_area ON (a.i_area = tr_area.i_area)
												WHERE
													a.i_periode = '$periode'
													AND a.d_bank >= to_date('$datefrom', 'dd-mm-yyyy')
													AND a.d_bank <= to_date('$dateto', 'dd-mm-yyyy')
													AND a.f_debet = 't'
													AND a.f_kbank_cancel = 'f'
													AND a.i_coa IN(
														SELECT
															i_coa
														FROM
															tr_coa
														WHERE
															i_area <> ''
															AND e_coa_name LIKE '%Kas Kecil%'
													)
													AND a.v_bank NOT IN (
														SELECT
															b.v_kk AS v_kb
														FROM
															tm_kk b
														WHERE
															b.d_kk = a.d_bank
															AND b.i_area = a.i_area
															AND b.f_kk_cancel = 'f'
															AND b.f_debet = 'f'
															AND b.i_coa LIKE '$bank%'
															AND b.i_periode = '$periode'
													)
											UNION ALL
												SELECT
													a.i_kk AS i_kk,
													a.d_kk,
													a.f_debet,
													a.e_description,
													a.i_kendaraan,
													a.n_km,
													a.v_kk,
													a.i_area,
													d_bukti,
													i_coa,
													a.e_coa_name,
													a.d_entry
												FROM
													tm_kk a,
													tr_area b
												WHERE
													a.d_kk >= to_date('$datefrom', 'dd-mm-yyyy')
													AND a.d_kk <= to_date('$dateto', 'dd-mm-yyyy')
													AND a.i_area = b.i_area
													AND a.f_kk_cancel = 'f'
											) AS a
										LEFT JOIN tr_area b ON (a.i_area = b.i_area)
										LEFT JOIN tm_pv_item c ON (a.i_kk = c.i_kk AND a.i_area = c.i_area AND a.i_coa = c.i_coa)
										LEFT JOIN tm_rv_item d ON (a.i_kk = d.i_kk AND a.i_area = d.i_area AND a.i_coa = d.i_coa)
									) AS b
									ORDER BY
										b.i_area,
										b.d_kk,
										b.d_entry,
										b.i_reff,
										b.i_kk ", FALSE);

			/* $this->db->select("	a.*, b.e_area_name, c.i_pv from(
		                      select i_kb as i_kk, d_kb as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_kb as v_kk, 
		                      a.i_area, d_bukti, i_coa
		                      from tm_kb a
		                      inner join tr_area on (a.i_area=tr_area.i_area)
		                      where a.i_periode='$periode' and a.d_kb >= to_date('$datefrom','dd-mm-yyyy') 
		                      and a.d_kb <= to_date('$toto','dd-mm-yyyy')
		                      and a.f_debet='t' and a.f_kb_cancel='f' and a.i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kecil%')
		                      and a.v_kb not in (select b.v_kk as v_kb from tm_kk b where 
		                      b.d_kk = a.d_kb and b.i_area=a.i_area and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '900-000%' 
		                      and b.i_periode='$periode')
		                      union all
		                      select i_kbank as i_kk, d_bank as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, 
		                      v_bank as v_kk, a.i_area, d_bank as d_bukti, i_coa
		                      from tm_kbank a
		                      inner join tr_area on (a.i_area=tr_area.i_area)
		                      where a.i_periode='$periode'
		                      and a.d_bank >= to_date('$datefrom','dd-mm-yyyy') and a.d_bank <= to_date('$toto','dd-mm-yyyy')
		                      and a.f_debet='t' and a.f_kbank_cancel='f' and a.i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kecil%')
		                      and a.v_bank not in (select b.v_kk as v_kb from tm_kk b where 
		                      b.d_kk = a.d_bank and b.i_area=a.i_area and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '$bank%' 
		                      and b.i_periode='$periode')
		                      union all
                          select a.i_kk as i_kk, a.d_kk, a.f_debet, a.e_description, a.i_kendaraan, a.n_km, a.v_kk, a.i_area, d_bukti, i_coa
                          from tm_kk a, tr_area b
							            where a.d_kk >= to_date('$datefrom','dd-mm-yyyy') and a.d_kk <= to_date('$toto','dd-mm-yyyy')
							            and a.i_area=b.i_area and a.f_kk_cancel='f'
							            ) as a
							            left join tr_area b on (a.i_area = b.i_area)
							            lEFT JOIN tm_pv_item c ON(a.i_kk=c.i_kk AND a.i_area=c.i_area AND a.i_coa=c.i_coa)
							            order by a.i_area,a.d_kk,a.i_kk",false); */
			#			$this->db->select("	* from tm_kk 
			#								          inner join tr_area on (tm_kk.i_area=tr_area.i_area)
			#								          where tm_kk.d_kk >= '$datefrom' and tm_kk.d_kk < '$dtos' 
			#                          and tm_kk.f_kk_cancel='f'
			#								          order by tm_kk.i_area,tm_kk.i_kk",false);
			$query = $this->db->get();

			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Kas Kecil Harian")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);

			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 12
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A1:A4'
				);

				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(22);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(28);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(32);
				// $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);
				// $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
				#        if($iarea=='00'){
				#  				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(13);
				#        }	

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'LAPORAN KAS HARIAN');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 1, 11, 1);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', NmPerusahaan);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 11, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'AREA NASIONAL');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 11, 3);
				$objPHPExcel->getActiveSheet()->setCellValue('A4', 'PERIODE : ' . $periodeawal . ' - ' . $periodeakhir);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 4, 11, 4);
				#        $objPHPExcel->getActiveSheet()->setCellValue('A4', 'No : '.$no);
				#				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,4,12,4);
				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Tgl Trans');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl Bukti');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'No Reff');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'NO Perk');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Nama COA');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				// $objPHPExcel->getActiveSheet()->setCellValue('I6', 'NO Perk Asal');
				// $objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
				// 	array(
				// 		'borders' => array(
				// 			'top' 	=> array('style' => Style_Border::BORDER_THIN),
				// 			'bottom'=> array('style' => Style_Border::BORDER_THIN),
				// 			'left'  => array('style' => Style_Border::BORDER_THIN),
				// 			'right' => array('style' => Style_Border::BORDER_THIN)
				// 		),
				// 	)
				// );
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'No Kendaraan');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'KM');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Debet');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L6', 'Kredit');
				$objPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M6', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('M6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				if (($iarea1 == '00' && ($idept == '4' || $idept == '0'))) {
					$objPHPExcel->getActiveSheet()->setCellValue('N6', 'No. Voucher');
					$objPHPExcel->getActiveSheet()->getStyle('N6')->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
				}

				$objPHPExcel->getActiveSheet()->getStyle('A6:N6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
				#        if($iarea=='00'){
				#				  $objPHPExcel->getActiveSheet()->setCellValue('N6', 'No. Voucher');
				#				  $objPHPExcel->getActiveSheet()->getStyle('N6')->applyFromArray(
				#					  array(
				#						  'borders' => array(
				#							  'top' 	=> array('style' => Style_Border::BORDER_THIN),
				#							  'bottom'=> array('style' => Style_Border::BORDER_THIN),
				#							  'left'  => array('style' => Style_Border::BORDER_THIN),
				#							  'right' => array('style' => Style_Border::BORDER_THIN)
				#						  ),
				#					  )
				#				  );
				#        }
				$i 		= 7;
				$j 		= 7;
				$xarea 	= '';
				$saldo 	= 0;

				foreach ($query->result() as $row) {
					// $periode = substr($datefrom, 0, 4) . substr($datefrom, 5, 2);
					$periode = date('Ym', strtotime($datefrom));

					if ($row->i_area != $xarea) {
						$j		= 7;
						$saldo	= $this->mmaster->bacasaldo($row->i_area, $periode, $datefrom);
						if ($i != 7) {
							$i++;
							$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, 'No');
							$objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									)

								)
							);

							$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, 'Tgl Trans');
							$objPHPExcel->getActiveSheet()->getStyle('B' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									),
								)
							);
							$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, 'Tgl Bukti');
							$objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									),
								)
							);
							$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, 'No Reff');
							$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									),
								)
							);
							$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, 'Keterangan');
							$objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									),
								)
							);

							$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, 'Area');
							$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									),
								)
							);

							$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, 'NO Perk');
							$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									),
								)
							);

							$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, 'Nama COA');
							$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									),
								)
							);

							// $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, 'NO Perk Asal');
							// $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
							// 	array(
							// 		'borders' => array(
							// 			'top' 	=> array('style' => Style_Border::BORDER_THIN),
							// 			'bottom'=> array('style' => Style_Border::BORDER_THIN),
							// 			'left'  => array('style' => Style_Border::BORDER_THIN),
							// 			'right' => array('style' => Style_Border::BORDER_THIN)
							// 		),
							// 	)
							// );
							$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, 'No Kendaraan');
							$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									),
								)
							);
							$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, 'KM');
							$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									),
								)
							);

							$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, 'Debet');
							$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									),
								)
							);
							$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, 'Kredit');
							$objPHPExcel->getActiveSheet()->getStyle('L' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									),
								)
							);
							$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, 'Saldo');
							$objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
								array(
									'borders' => array(
										'top' 	=> array('style' => Style_Border::BORDER_THIN),
										'bottom' => array('style' => Style_Border::BORDER_THIN),
										'left'  => array('style' => Style_Border::BORDER_THIN),
										'right' => array('style' => Style_Border::BORDER_THIN)
									),
								)
							);

							if (($iarea1 == '00' && ($idept == '4' || $idept == '0'))) {
								$objPHPExcel->getActiveSheet()->setCellValue('N' . $i, 'No Voucher');
								$objPHPExcel->getActiveSheet()->getStyle('N' . $i)->applyFromArray(
									array(
										'borders' => array(
											'top' 	=> array('style' => Style_Border::BORDER_THIN),
											'bottom' => array('style' => Style_Border::BORDER_THIN),
											'left'  => array('style' => Style_Border::BORDER_THIN),
											'right' => array('style' => Style_Border::BORDER_THIN)
										),
									)
								);
							}
						}

						if ($i != 7) {
							$objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':N' . $i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
							$i++;
						}

						$objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom' => array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

						#						$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, "Saldo Awal Area ".$row->i_area);
						$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->i_area, Cell_DataType::TYPE_STRING);
						$objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom' => array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom' => array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom' => array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom' => array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom' => array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom' => array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

						$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom' => array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

						$objPHPExcel->getActiveSheet()->getStyle('L' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom' => array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);

						// $objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
						// 	array(
						// 		'borders' => array(
						// 			'top' 	=> array('style' => Style_Border::BORDER_THIN),
						// 			'bottom' => array('style' => Style_Border::BORDER_THIN),
						// 			'left'  => array('style' => Style_Border::BORDER_THIN),
						// 			'right' => array('style' => Style_Border::BORDER_THIN)
						// 		)
						// 	)
						// );

						$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $saldo);
						$objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom' => array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								),
							)
						);

						$objPHPExcel->getActiveSheet()->getStyle('N' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom' => array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						#            if($iarea=='00'){
						#						  $objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray(
						#							  array(
						#								  'borders' => array(
						#									  'top' 	=> array('style' => Style_Border::BORDER_THIN),
						#									  'bottom'=> array('style' => Style_Border::BORDER_THIN),
						#									  'left'  => array('style' => Style_Border::BORDER_THIN),
						#									  'right' => array('style' => Style_Border::BORDER_THIN)
						#								  ),
						#							  )
						#						  );
						#            }
						$i++;
					}
					$xarea = $row->i_area;
					if ($row->f_debet == 'f' || substr($row->i_kk, 0, 2) == 'KB') {
						$debet = $row->v_kk;
						$kredit = 0;
					} elseif ($row->f_debet == 'f' || substr($row->i_kk, 0, 2) == 'BK') {
						$debet = $row->v_kk;
						$kredit = 0;
					} else {
						$kredit = $row->v_kk;
						$debet = 0;
					}
					#					if($row->f_debet=='f')
					#					{
					#						$debet =$row->v_kk;
					#						$kredit=0;
					#					}else{
					#						$kredit=$row->v_kk;
					#						$debet =0;
					#					}
					$saldo = $saldo + $debet - $kredit;
					$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $j - 6);
					$objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					if ($row->d_kk != '') {
						$tmp = explode("-", $row->d_kk);
						$th = $tmp[0];
						$bl = $tmp[1];
						$hr = $tmp[2];
						$row->d_kk = $hr . "-" . $bl . "-" . $th;
					}
					#					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_area);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->i_area . " - " . $row->e_area_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->d_kk);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					if ($row->d_bukti != '') {
						$tmp = explode("-", $row->d_bukti);
						$th = $tmp[0];
						$bl = $tmp[1];
						$hr = $tmp[2];
						$row->d_bukti = $hr . "-" . $bl . "-" . $th;
					}
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->d_bukti);
					$objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->i_kk);
					$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $row->e_description);
					$objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					#					if(substr($row->i_coa,0,5)=='800.0')$row->i_coa='800.000';
					if (substr($row->i_coa, 0, 7) == '900-000') $row->i_coa = '900-00000';
					#					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->i_coa);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->i_coa, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);

					$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->e_coa_name);
					$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);

					# if(strlen($row->i_coa)==5){
					// if (strlen($row->i_coa) == 7) {
					// 	$coa  = $row->i_coa . $row->i_area;
					// } else {
					// 	# $coa  = substr($row->i_coa,0,5).$row->i_area;
					// 	$coa  = substr($row->i_coa, 0, 7) . $row->i_area;
					// 	# if(substr($coa,0,6)=='900-00')$coa='900-0000';
					// 	// $coa = $this->db->query("select i_coa from tr_coa where i_area = '$row->i_area' and e_coa_name like '%Kas Kecil%'")->row()->i_coa;

					// 	# $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $coa);
					// 	$objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $coa, Cell_DataType::TYPE_STRING);
					// 	$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
					// 		array(
					// 			'borders' => array(
					// 				'top' 	=> array('style' => Style_Border::BORDER_THIN),
					// 				'bottom' => array('style' => Style_Border::BORDER_THIN),
					// 				'left'  => array('style' => Style_Border::BORDER_THIN),
					// 				'right' => array('style' => Style_Border::BORDER_THIN)
					// 			),
					// 		)
					// 	);
					// }
					$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->i_kendaraan);
					$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->n_km);
					$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $debet);
					$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $kredit);
					$objPHPExcel->getActiveSheet()->getStyle('L' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $saldo);
					$objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);

					if (($iarea1 == '00' && ($idept == '4' || $idept == '0'))) {
						$objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $row->i_reff);
						$objPHPExcel->getActiveSheet()->getStyle('N' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom' => array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								),
							)
						);
					}
					#          if($iarea=='00'){
					#					  $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $row->i_bukti_pengeluaran);
					#					  $objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray(
					#						  array(
					#							  'borders' => array(
					#								  'top' 	=> array('style' => Style_Border::BORDER_THIN),
					#								  'bottom'=> array('style' => Style_Border::BORDER_THIN),
					#								  'left'  => array('style' => Style_Border::BORDER_THIN),
					#								  'right' => array('style' => Style_Border::BORDER_THIN)
					#							  ),
					#						  )
					#					  );
					#          }
					$i++;
					$j++;
				}

				$x = $i - 1;

				$objPHPExcel->getActiveSheet()->getStyle('G7:H7' . $x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
				$objPHPExcel->getActiveSheet()->getStyle('L7:M' . $x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
				/*        if($row->i_area=='00'){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => true
					    )
				    ),
				    'A7:M'.$x
			    );
			    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						    'wrap'      => true
					    )
				    ),
				    'J7:M'.$x
			    );
        }else{
*/
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' 	=> array(
							'name' 	=> 'Arial',
							'bold'  => false,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A7:N' . $x
				);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' 	=> array(
							'name' 	=> 'Arial',
							'bold'  => false,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'horizontal' => Style_Alignment::HORIZONTAL_RIGHT,
							'wrap'      => true
						)
					),
					'L7:M' . $x
				);
				#        }
			}
			#      if($iarea=='00'){
			#	  		$objPHPExcel->getActiveSheet()->getStyle('A6:M6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB#('FFFFFF00');
			#      }else{
			$objPHPExcel->getActiveSheet()->getStyle('A6:N6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
			#      }
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

			$nama = 'LKH_NASIONAL(' . $datefrom . '_sd_' . $dateto . ').xls';

			if (file_exists('excel/00/' . $nama)) {
				@chmod('excel/00/' . $nama, 0777);
				@unlink('excel/00/' . $nama);
			}
			$objWriter->save('excel/00/' . $nama);

			$this->logger->writenew('Export LKH NASIONAL tanggal:' . $datefrom . ' sampai:' . $dateto);

			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output', 'w');
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu409') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-kk-lkhall/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($area1 == '00') {
				$query = $this->db->query("select * from tr_area", false);
			} else {
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									or i_area = '$area4' or i_area = '$area5'", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);


			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view($this->folder . '/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu409') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$area1	= $this->session->userdata('i_area1');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url() . 'index.php/exp-kk-lkhall/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if ($area1 == '00') {
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')", false);
			} else {
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view($this->folder . '/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
