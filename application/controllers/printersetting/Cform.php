<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu93')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
				$data['page_title'] = $this->lang->line('printersetting');
				$data['host']='';
				$this->load->model('printersetting/mmaster');
				$data['isi']=$this->mmaster->baca($this->session->userdata('user_id'));
				
				$this->load->view('printersetting/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu93')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printersetting');
			$this->load->view('printersetting/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu93')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printersetting')." update";
			if($this->input->post('istatusedit')){
				$istatus = $this->input->post('istatusedit');
				$data['istatus'] = $istatus;
				$this->load->model('printersetting/mmaster');
				$data['isi']=$this->mmaster->baca($istatus);
		 		$this->load->view('printersetting/vmainform',$data);
			}else{
				$this->load->view('printersetting/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu93')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$eprinterhost	= $this->input->post('eprinterhost', TRUE);
			$eprinteruri 	= $this->input->post('eprinteruri', TRUE);
			$ihost 			  = $this->input->post('ihost', TRUE);
			$user			    = $this->session->userdata("user_id");
      if($eprinterhost!='' && $eprinteruri!=''){
			  $this->db->trans_begin();
			  $this->load->model('printersetting/mmaster');
			  $this->mmaster->update($ihost,$eprinterhost,$eprinteruri,$user);
			  if ($this->db->trans_status() === FALSE)
			  {
				  $this->db->trans_rollback();
			  }else{
				  $this->db->trans_commit();

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		=  $this->db->query($sql);
			  if($rs->num_rows>0){
				  foreach($rs->result() as $tes){
					  $ip_address	  = $tes->ip_address;
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }

			  $data['user']	= $this->session->userdata('user_id');
  #			$data['host']	= $this->session->userdata('printerhost');
			  $data['host']	= $ip_address;
			  $data['uri']	= $this->session->userdata('printeruri');
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Setting Printer user:'.$user;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );

				  $data['sukses'] = true;
				  $data['inomor']	= $eprinteruri;
				  $this->load->view('nomor',$data);
			  }
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function host()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu93')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = site_url().'/printersetting/cform/host/index/';
			$cari	= strtoupper($this->input->post('cari'));
			$query = $this->db->query(" select * from tr_host",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printersetting/mmaster');
			$data['page_title'] = $this->lang->line('printersetting');
			$data['isi']=$this->mmaster->bacahost($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('printersetting/vlisthost', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carihost()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu93')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari	= strtoupper($this->input->post('cari'));
			$config['base_url'] = site_url().'/printersetting/cform/host/index/';
			$query = $this->db->query(" select * from tr_host where e_host_name like '%$cari%' or e_ip_address like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printersetting/mmaster');
			$data['page_title'] = $this->lang->line('printersetting');
			$data['isi']=$this->mmaster->bacahost($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('printersetting/vlisthost', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function uri()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu93')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$host=$this->uri->segment(4);
			$data['host']=$host;
			$data['page_title'] = $this->lang->line('printersetting');
			$this->load->view('printersetting/vlisturi', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
