<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');

			$icustomer			= $this->input->post('icustomer', TRUE);
			$icustomerplugroup		= $this->input->post('icustomerplugroup', TRUE);
			$icity				= $this->input->post('icity', TRUE);
			$icustomergroup			= $this->input->post('icustomergroup', TRUE);
//			$ipricegroup			= $this->input->post('ipricegroup', TRUE);
			$ipricegroup			= $this->input->post('nline', TRUE);
			$iarea				= $this->input->post('iarea', TRUE);
			$icustomerstatus		= $this->input->post('icustomerstatus', TRUE);
			$icustomerproducttype		= $this->input->post('icustomerproducttype', TRUE);
			$icustomerspecialproduct	= $this->input->post('icustomerspecialproduct', TRUE);
			$icustomergrade			= $this->input->post('icustomergrade', TRUE);
			$icustomerservice		= $this->input->post('icustomerservice', TRUE);
			$icustomersalestype		= $this->input->post('icustomersalestype', TRUE);
			$icustomerclass			= $this->input->post('icustomerclass', TRUE);
			$icustomerstatus		= $this->input->post('icustomerstatus', TRUE);
			$ecustomername			= $this->input->post('ecustomername', TRUE);
			$ecustomeraddress		= $this->input->post('ecustomeraddress', TRUE);
			$ecityname			= $this->input->post('ecityname', TRUE);
			$ecustomerpostal		= $this->input->post('ecustomerpostal', TRUE);
			$ecustomerphone			= $this->input->post('ecustomerphone', TRUE);
			$ecustomerfax			= $this->input->post('ecustomerfax', TRUE);
			$ecustomermail			= $this->input->post('ecustomermail', TRUE);
			$ecustomersendaddress		= $this->input->post('ecustomersendaddress', TRUE);
			$ecustomerreceiptaddress	= $this->input->post('ecustomerreceiptaddress', TRUE);
			$ecustomerremark		= $this->input->post('ecustomerremark', TRUE);
			$ecustomerpayment		= $this->input->post('ecustomerpayment', TRUE);
			$ecustomerpriority		= $this->input->post('ecustomerpriority', TRUE);
			$ecustomercontact		= $this->input->post('ecustomercontact', TRUE);
			$ecustomercontactgrade		= $this->input->post('ecustomercontactgrade', TRUE);
			$ecustomerrefference		= $this->input->post('ecustomerrefference', TRUE);
			$ecustomerothersupplier		= $this->input->post('ecustomerothersupplier', TRUE);
			$fcustomerplusppn		= $this->input->post('fcustomerplusppn', TRUE);
			$fcustomerplusdiscount		= $this->input->post('fcustomerplusdiscount', TRUE);
			$fcustomerpkp			= $this->input->post('fcustomerpkp', TRUE);
			$fcustomeraktif			= $this->input->post('fcustomeraktif', TRUE);
			$fcustomertax			= $this->input->post('fcustomertax', TRUE);
			$fcustomercicil			= $this->input->post('fcustomercicil', TRUE);
			$ecustomerretensi		= $this->input->post('ecustomerretensi', TRUE);
			$ncustomertoplength		= $this->input->post('ncustomertoplength', TRUE);
			if($ncustomertoplength=='')
				$ncustomertoplength=0;
			if ((isset($icustomer) && $icustomer != '') && (isset($ecustomername) && $ecustomername != ''))
			{
				$this->load->model('customerpareto/mmaster');
				$this->mmaster->insert
					(
					$icustomer, $icustomerplugroup, $icity, $icustomergroup, $ipricegroup,
					$iarea, $icustomerstatus, $icustomerproducttype, $icustomerspecialproduct, 
					$icustomergrade, $icustomerservice, $icustomersalestype, $icustomerclass, 
					$icustomerstatus, $ecustomername, $ecustomeraddress,$ecityname, 
					$ecustomerpostal, $ecustomerphone, $ecustomerfax, $ecustomermail, 
					$ecustomersendaddress, $ecustomerreceiptaddress, $ecustomerremark, 
					$ecustomerpayment, $ecustomerpriority, $ecustomercontact, 
					$ecustomercontactgrade, $ecustomerrefference, $ecustomerothersupplier, 
					$fcustomerplusppn, $fcustomerplusdiscount, $fcustomerpkp,$fcustomeraktif, 
					$fcustomertax, $ecustomerretensi, $ncustomertoplength,$fcustomercicil
					);

				$sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Input Pelanggan Pareto:'.$icustomer;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now, $pesan);

			    $area1	= $this->session->userdata('i_area');
				$area2	= $this->session->userdata('i_area2');
				$area3	= $this->session->userdata('i_area3');
				$area4	= $this->session->userdata('i_area4');
				$area5	= $this->session->userdata('i_area5');
				$cari=strtoupper($this->input->post("cari",false));
				$config['base_url'] = base_url().'index.php/customerpareto/cform/index/';
				if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				      $query=$this->db->query("select * from tr_customer 
				                           where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%')", false);
				}else{
				  $query=$this->db->query("select * from tr_customer 
				                           where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') 
				                           and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
				                           or i_area = '$area4' or i_area = '$area5')", false);
				}
				$config['total_rows'] = $query->num_rows();
#				$config['total_rows'] = $this->db->count_all('tr_customer');
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('customerpareto');
				$data['icustomer']='';
				$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);
				$this->load->model('customerpareto/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4),$area1,$area2,$area3,$area4,$area5);
				$this->load->view('customerpareto/vmainform', $data);
			}
			else
			{
        $area1	= $this->session->userdata('i_area');
			  $area2	= $this->session->userdata('i_area2');
			  $area3	= $this->session->userdata('i_area3');
			  $area4	= $this->session->userdata('i_area4');
			  $area5	= $this->session->userdata('i_area5');
			  $cari=strtoupper($this->input->post("cari",false));
			  $config['base_url'] = base_url().'index.php/customerpareto/cform/index/';
        if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		      $query=$this->db->query("select * from tr_customer 
                                   where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%')", false);
        }else{
          $query=$this->db->query("select * from tr_customer 
                                   where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') 
                                   and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                                   or i_area = '$area4' or i_area = '$area5')", false);
        }
				$config['total_rows'] = $query->num_rows();
#				$config['total_rows'] = $this->db->count_all('tr_customer');
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('customerpareto');
				$data['icustomer']='';
				$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);
				$this->load->model('customerpareto/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4),$area1,$area2,$area3,$area4,$area5);

			   	$sess	= $this->session->userdata('session_id');
				$id 	= $this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan="Membuka Menu Pelanggan Pareto";
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now, $pesan);

				$this->load->view('customerpareto/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('customerpareto');
			$this->load->view('customerpareto/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('customerpareto')." update";
			if($this->uri->segment(4)){
				$icustomer = $this->uri->segment(4);
				$data['icustomer'] = $icustomer;
				$this->load->model('customerpareto/mmaster');
				$data['isi']=$this->mmaster->baca($icustomer);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Pelanggan Pareto:('.$icustomer.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);
		        
		 		$this->load->view('customerpareto/vmainform',$data);
			}else{
				$this->load->view('customerpareto/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $icustomer	  = $this->input->post('icustomer', TRUE);
			$fpareto			= $this->input->post('fpareto', TRUE);
			if($fpareto!='')
				$fpareto		= 't';
			else
				$fpareto			= 'f';
			$this->load->model('customerpareto/mmaster');
      $this->db->trans_begin();
			$this->mmaster->update($icustomer,$fpareto);
      if ( ($this->db->trans_status() === FALSE) )
				{
			    $this->db->trans_rollback();
				}else{
			    $this->db->trans_commit();
			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Update Pelanggan Pareto:'.$icustomer;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $icustomer;
					$this->load->view('nomor',$data);
				}

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer	= $this->uri->segment(4);
			$this->load->model('customerpareto/mmaster');
			$this->mmaster->delete($icustomer);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus Pelanggan Pareto:'.$icustomer;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$config['base_url'] = base_url().'index.php/customerpareto/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('customerpareto');
			$data['icustomer']='';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customerpareto/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari=strtoupper($this->input->post("cari",false));
      if($cari=='')$cari=$this->uri->segment(4);
      if($cari=='zxvf'){
        $cari='';
    		$config['base_url'] = base_url().'index.php/customerpareto/cform/cari/zxvf/';
      }else{
    		$config['base_url'] = base_url().'index.php/customerpareto/cform/cari/'.$cari.'/';
      }
#			$config['base_url'] = base_url().'index.php/customerpareto/cform/index/';
      if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
		    $query=$this->db->query("select a.*, b.i_customer_groupar from tr_customer a, tr_customer_groupar b 
                                 where (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
                                 and a.i_customer=b.i_customer", false);
      }else{
        $query=$this->db->query("select a.*, b.i_customer_groupar from tr_customer a, tr_customer_groupar b 
                                 where (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%') 
                                 and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                 or a.i_area = '$area4' or a.i_area = '$area5')
                                 and a.i_customer=b.i_customer", false);
      }
#			$query=$this->db->query("select * from tr_customer where i_customer like '%$cari%' or e_customer_name like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 	
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$data['page_title'] = $this->lang->line('customerpareto');
			$data['icustomer']='';
	 		$this->load->view('customerpareto/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function plugroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/plugroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_plugroup');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_plugroup');
			$data['isi']=$this->mmaster->bacaplugroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistplugroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariplugroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/plugroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_plugroup where upper(i_customer_plugroup) like '%$cari%' 
										or upper(e_customer_plugroupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_plugroup');
			$data['isi']=$this->mmaster->cariplugroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistplugroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function city()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/customerpareto/cform/city/'.$iarea.'/index/';
//			$config['total_rows'] = $this->db->count_all('tr_city');
			$query = $this->db->query("	select * from tr_city where (upper(i_city) like '%$cari%' 
							or upper(e_city_name) like '%$cari%') and i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->bacacity($iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('customerpareto/vlistcity', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricity()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea= $this->input->post('iarea', FALSE);
			$config['base_url'] = base_url().'index.php/customerpareto/cform/city/'.$iarea.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
//			$query = $this->db->query("select * from tr_city where upper(i_city) like '%cari%' or upper(e_city_name) like '%cari%'",false);
			$query = $this->db->query("	select * from tr_city where (upper(i_city) like '%$cari%' 
							or upper(e_city_name) like '%$cari%') and i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['isi']=$this->mmaster->caricity($cari,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('customerpareto/vlistcity', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customergroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_group');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customergroup');
			$data['isi']=$this->mmaster->bacacustomergroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomergroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customergroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_group where upper(i_customer_group) like '%cari%' 
										or upper(e_customer_groupname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customergroup');
			$data['isi']=$this->mmaster->caricustomergroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomergroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function pricegroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/pricegroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_price_group');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_pricegroup');
			$data['isi']=$this->mmaster->bacapricegroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistpricegroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caripricegroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/pricegroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_price_group where upper(i_price_group) like '%cari%' 
										or upper(e_price_groupname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_pricegroup');
			$data['isi']=$this->mmaster->caripricegroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistpricegroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/area/index/';
			$config['total_rows'] = $this->db->count_all('tr_area');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/area/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select * from tr_area where upper(i_area) like '%cari%' or upper(e_area_name) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerstatus()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customerstatus/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_status');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customerstatus');
			$data['isi']=$this->mmaster->bacacustomerstatus($config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomerstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerstatus()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customerstatus/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_status where upper(i_customer_status) like '%cari%' 
										or upper(e_customer_statusname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customerstatus');
			$data['isi']=$this->mmaster->caricustomerstatus($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomerstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerproducttype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customerproducttype/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_producttype');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customerproducttype');
			$data['isi']=$this->mmaster->bacacustomerproducttype($config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomerproducttype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerproducttype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customerproducttype/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_producttype where upper(i_customer_producttype) like '%cari%' 
										or upper(e_customer_producttypename) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customerproducttype');
			$data['isi']=$this->mmaster->caricustomerproducttype($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomerproducttype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerspecialproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerproducttype = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customerspecialproduct/index/';
			$this->db->where("i_customer_producttype='$icustomerproducttype'");
			$config['total_rows'] = $this->db->count_all('tr_customer_specialproduct');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customerspecialproduct');
			$data['isi']=$this->mmaster->bacacustomerspecialproduct($icustomerproducttype,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomerspecialproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerspecialproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customerspecialproduct/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_specialproduct 
										where upper(i_customer_specialproduct) like '%cari%' 
										or upper(e_customer_specialproductname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customerspecialproduct');
			$data['isi']=$this->mmaster->caricustomerspecialproduct($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomerspecialproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customergrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customergrade/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_grade');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customergrade');
			$data['isi']=$this->mmaster->bacacustomergrade($config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomergrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomergrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customergrade/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_grade 
										where upper(i_customer_grade) like '%cari%' 
										or upper(e_customer_gradename) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customergrade');
			$data['isi']=$this->mmaster->caricustomergrade($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomergrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerservice()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customerservice/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_service');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customerservice');
			$data['isi']=$this->mmaster->bacacustomerservice($config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomerservice', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerservice()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customerservice/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_customer_service where upper(i_customer_service) like '%cari%' 
										or upper(e_customer_servicename) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customerservice');
			$data['isi']=$this->mmaster->caricustomerservice($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomerservice', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customersalestype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customersalestype/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_salestype');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customersalestype');
			$data['isi']=$this->mmaster->bacacustomersalestype($config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomersalestype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomersalestype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customersalestype/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_salestype where upper(i_customer_salestype) like '%cari%' 
										or upper(e_customer_salestypename) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customersalestype');
			$data['isi']=$this->mmaster->caricustomersalestype($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomersalestype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customerclass()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customerclass/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_class');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customerclass');
			$data['isi']=$this->mmaster->bacacustomerclass($config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomerclass', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomerclass()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu413')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpareto/cform/customerclass/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_class where upper(i_customer_class) like '%cari%' 
										or upper(e_customer_classname) like '%cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customerpareto/mmaster');
			$data['page_title'] = $this->lang->line('list_customerclass');
			$data['isi']=$this->mmaster->caricustomerclass($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('customerpareto/vlistcustomerclass', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
