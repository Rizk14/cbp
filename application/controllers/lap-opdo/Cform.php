<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu333')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('lap-opdo');
			$data['iperiode']	= '';
			$data['isupplier']	= '';
			$this->load->view('lap-opdo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu333')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);      
			$isupplier	= $this->input->post('isupplier');
			if($isupplier=='') $isupplier=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/lap-opdo/cform/view/'.$iperiode.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select a.i_op, a.i_area, a.i_reff, a.d_reff, a.e_op_remark, a.d_op, b.i_product, b.e_product_name, b.v_product_mill, b.n_order, 
			(b.n_order * b.v_product_mill) as rpop, c.e_supplier_name,
				case when b.n_delivery isnull then 0 else b.n_delivery end as n_delivery,  (n_delivery * b.v_product_mill) as rpdo,
				case when b.n_delivery isnull then b.n_order else b.n_order-b.n_delivery end as pending from tm_op a 
				left join tm_op_item b on (a.i_op=b.i_op)
				inner join tr_supplier c on (a.i_supplier=c.i_supplier)
			  where to_char(a.d_op::timestamp with time zone, 'yyyymm'::text)='$iperiode'
			  and a.i_supplier='$isupplier' and a.f_op_cancel='f' order by a.i_op",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(7);
			$this->paginationxx->initialize($config);

			$this->load->model('lap-opdo/mmaster');
			$data['page_title'] = $this->lang->line('lap-opdo');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isupplier']	= $isupplier;
			$data['isi']	= $this->mmaster->baca($iperiode,$isupplier,$config['per_page'],$this->uri->segment(7),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Laporan OP vs DO:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('lap-opdo/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu333')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$isupplier	= $this->input->post('isupplier');
			if($isupplier=='') $isupplier=$this->uri->segment(5);
			$cari	= strtoupper($this->input->post('cari'));
			$config['base_url'] = base_url().'index.php/lap-opdo/cform/view/'.$iperiode.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select a.i_op, a.i_area, a.i_reff, a.d_reff, a.e_op_remark, a.d_op, b.i_product, b.e_product_name, b.v_product_mill, b.n_order, 
			(b.n_order * b.v_product_mill) as rpop, c.e_supplier_name,
				case when b.n_delivery isnull then 0 else b.n_delivery end as n_delivery,  (n_delivery * b.v_product_mill) as rpdo,
				case when b.n_delivery isnull then b.n_order else b.n_order-b.n_delivery end as pending from tm_op a 
				left join tm_op_item b on (a.i_op=b.i_op)
				inner join tr_supplier c on (a.i_supplier=c.i_supplier)
			  where to_char(a.d_op::timestamp with time zone, 'yyyymm'::text)='$iperiode'
			  and(upper(b.i_product) like '%$cari%' or upper(b.e_product_name) like '%$cari%')
			  and a.i_supplier='$isupplier' and a.f_op_cancel='f' order by a.i_op",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);

			$this->load->model('lap-opdo/mmaster');
			$data['page_title'] = $this->lang->line('lap-opdo');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isupplier']	= $isupplier;
			$data['isi']	= $this->mmaster->cari($iperiode,$isupplier,$cari,$config['per_page'],$this->uri->segment(7));
			$this->load->view('lap-opdo/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu333')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/lap-opdo/cform/supplier/index/';
			$config['total_rows'] = $this->db->count_all('tr_supplier');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('lap-opdo/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('lap-opdo/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu333')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/lap-opdo/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_supplier 
										where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('lap-opdo/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('lap-opdo/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
