<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnamenota');
			$data['iarea']='';
			$data['dto']='';
      $data['nt']='';
      $data['jt']='';
			$this->load->view('exp-opnnota/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnamenota');
			$this->load->view('exp-opnnota/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-opnnota/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-opnnota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('exp-opnnota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-opnnota/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-opnnota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('exp-opnnota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu238')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
         {
            $this->load->model('exp-opnnota/mmaster');
            $dto	= $this->input->post('dto');
            $iarea= $this->input->post('iarea');
            $eareaname= $this->input->post('eareaname');
            $nt   = $this->input->post('chkntx');
            $jt   = $this->input->post('chkjtx');
            if($iarea=='')$iarea=$this->uri->segment(4);
            if($dto=='')$dto=$this->uri->segment(5);
            if($nt=='') $nt=$this->uri->segment(6);
            if($jt=='') $jt=$this->uri->segment(7);
            $this->load->model('exp-opnnota/mmaster');
            if($dto!='')
            {
               $tmp=explode("-",$dto);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $dto=$th."-".$bl."-".$hr;
               $irep=$hr.'-'.$bl.'-'.$th;
               $peri=$hr.' '.mbulan($bl).' '.$th;
               $jd1 = GregorianToJD($bl, $hr, $th);
            }

            if($nt=='qqq')
            {
               $this->db->select(" a.* from (
                                  select distinct
                                  h.i_product_group, i.e_product_groupname, a.*, b.e_area_name, f.e_salesman_name, c.e_customer_name, 
                                  c.f_customer_pkp, c.e_customer_address, c.e_customer_phone, c.n_customer_toplength, 
                                  x.e_pelunasan_remark, 
                                  case when not d.e_remark isnull then d.e_remark when not j.e_remark isnull then j.e_remark 
                                  when not l.e_remark isnull then l.e_remark when not n.e_remark isnull then n.e_remark 
                                  else '' end as remlunas, 
                                  case when substring(a.i_sj,9,2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                              	  else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang end as d_jatuh_tempo_plustoleransi,
                                 	case when substring(a.i_sj,9,2)='00' then g.n_toleransi_pusat else g.n_toleransi_cabang end as n_toleransi
  												from tm_spb h, tr_product_group i, tm_nota a
													left join tr_area b on b.i_area = a.i_area
													left join tr_customer c on c.i_customer = a.i_customer
													left join tr_salesman f on f.i_salesman=a.i_salesman
													left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
													left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
													left join tm_pelunasan e on (d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area 
													and e.f_pelunasan_cancel='f')
													left join tr_city g on c.i_city = g.i_city and c.i_area = g.i_area

													left join tm_alokasi_item j on(a.i_nota=j.i_nota)
                          left join tm_alokasi k on(j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area 
                          and k.f_alokasi_cancel='f' and k.d_alokasi<='$dto')
                          left join tm_alokasikn_item l on(a.i_nota=l.i_nota)
                          left join tm_alokasikn m on(l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area 
                          and m.f_alokasi_cancel='f' and m.d_alokasi<='$dto')
                          left join tm_alokasiknr_item n on(a.i_nota=n.i_nota)
                          left join tm_alokasiknr o on(n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area 
                          and o.f_alokasi_cancel='f' and o.d_alokasi<='$dto')
                          left join tm_alokasihl_item p on(a.i_nota=p.i_nota)
                          left join tm_alokasihl q on(p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f' 
                          and q.d_alokasi<='$dto')
 
            	  			  where a.i_area = '$iarea' and a.d_nota<='$dto' and a.v_sisa>0 and a.f_nota_cancel='f'
                        and a.i_spb = h.i_spb and a.i_area = h.i_area and h.i_product_group = i.i_product_group
                        
                                  union all
                                  
                                  select distinct
                                  h.i_product_group, i.e_product_groupname, a.*, b.e_area_name, f.e_salesman_name, c.e_customer_name, 
                                  c.f_customer_pkp, c.e_customer_address, c.e_customer_phone, c.n_customer_toplength, x.e_pelunasan_remark, 
                                  case when not d.e_remark isnull then d.e_remark when not j.e_remark isnull then j.e_remark 
                                  when not l.e_remark isnull then l.e_remark when not n.e_remark isnull then n.e_remark 
                                  else '' end as remlunas,
                                  case when substring(a.i_sj,9,2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                              	  else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang end as d_jatuh_tempo_plustoleransi,
                                 	case when substring(a.i_sj,9,2)='00' then g.n_toleransi_pusat else g.n_toleransi_cabang end as n_toleransi
  												from tm_spb h, tr_product_group i, tm_nota a
  												inner join tr_customer_consigment z on (a.i_customer=z.i_customer and z.i_area_real='$iarea')
													left join tr_area b on b.i_area = a.i_area
													left join tr_customer c on c.i_customer = a.i_customer
													left join tr_salesman f on f.i_salesman=a.i_salesman
													left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
													left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
													left join tm_pelunasan e on (d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area 
													and e.f_pelunasan_cancel='f')
													left join tr_city g on c.i_city = g.i_city and c.i_area = g.i_area

													left join tm_alokasi_item j on(a.i_nota=j.i_nota)
                          left join tm_alokasi k on(j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area 
                          and k.f_alokasi_cancel='f' and k.d_alokasi<='$dto')
                          left join tm_alokasikn_item l on(a.i_nota=l.i_nota)
                          left join tm_alokasikn m on(l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area 
                          and m.f_alokasi_cancel='f' and m.d_alokasi<='$dto')
                          left join tm_alokasiknr_item n on(a.i_nota=n.i_nota)
                          left join tm_alokasiknr o on(n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area 
                          and o.f_alokasi_cancel='f' and o.d_alokasi<='$dto')
                          left join tm_alokasihl_item p on(a.i_nota=p.i_nota)
                          left join tm_alokasihl q on(p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f' 
                          and q.d_alokasi<='$dto')
 
            	  			  where a.d_nota<='$dto' and a.v_sisa>0 and a.f_nota_cancel='f'
                        and a.i_spb = h.i_spb and a.i_area = h.i_area and h.i_product_group = i.i_product_group
                        ) as a
            	  			  order by a.e_customer_name, a.i_nota desc",false);
                      	  			              }
            else
            {
               $this->db->select("a.* from (
                                  select distinct 
                                  h.i_product_group, i.e_product_groupname, a.*, b.e_area_name, f.e_salesman_name, c.e_customer_name, c.f_customer_pkp, 
                                  c.e_customer_address, c.e_customer_phone, c.n_customer_toplength,
                                  x.e_pelunasan_remark, d.e_remark as remlunas, g.n_toleransi_cabang, g.n_toleransi_pusat,
                                  	case when substring(a.i_sj,9,2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                                  		else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
                              			end as d_jatuh_tempo_plustoleransi,
                                  	case when substring(a.i_sj,9,2)='00' then g.n_toleransi_pusat
                              	  		else g.n_toleransi_cabang
                          	  			end as n_toleransi
												from tm_spb h, tr_product_group i, tm_nota a
													left join tr_area b on b.i_area = a.i_area
													left join tr_customer c on c.i_customer = a.i_customer
													left join tr_salesman f on f.i_salesman=a.i_salesman
													left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
													left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
													left join tm_pelunasan e on (d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area 
													and e.f_pelunasan_cancel='f')
													left join tr_city g on c.i_city = g.i_city and c.i_area = g.i_area
													left join tm_alokasi_item j on(a.i_nota=j.i_nota)
                          left join tm_alokasi k on(j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area 
                          and k.f_alokasi_cancel='f' and k.d_alokasi<='$dto')
                          left join tm_alokasikn_item l on(a.i_nota=l.i_nota)
                          left join tm_alokasikn m on(l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area 
                          and m.f_alokasi_cancel='f' and m.d_alokasi<='$dto')
                          left join tm_alokasiknr_item n on(a.i_nota=n.i_nota)
                          left join tm_alokasiknr o on(n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area 
                          and o.f_alokasi_cancel='f' and o.d_alokasi<='$dto')
                          left join tm_alokasihl_item p on(a.i_nota=p.i_nota)
                          left join tm_alokasihl q on(p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f' 
                          and q.d_alokasi<='$dto')

            	  			  where a.i_area = '$iarea' and a.d_jatuh_tempo<='$dto' and a.v_sisa>0 and a.f_nota_cancel='f'
            	  			  and h.i_spb = a.i_spb and a.i_area = h.i_area and h.i_product_group = i.i_product_group
            	  			  
            	  			  union all
                                  
                                  select distinct
                                  h.i_product_group, i.e_product_groupname, a.*, b.e_area_name, f.e_salesman_name, c.e_customer_name, 
                                  c.f_customer_pkp, c.e_customer_address, c.e_customer_phone, c.n_customer_toplength, x.e_pelunasan_remark, 
                                  case when not d.e_remark isnull then d.e_remark when not j.e_remark isnull then j.e_remark 
                                  when not l.e_remark isnull then l.e_remark when not n.e_remark isnull then n.e_remark 
                                  else '' end as remlunas, g.n_toleransi_cabang, g.n_toleransi_pusat, 
                                  case when substring(a.i_sj,9,2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                              	  else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang end as d_jatuh_tempo_plustoleransi,
                                 	case when substring(a.i_sj,9,2)='00' then g.n_toleransi_pusat else g.n_toleransi_cabang end as n_toleransi
  												from tm_spb h, tr_product_group i, tm_nota a
  												inner join tr_customer_consigment z on (a.i_customer=z.i_customer and z.i_area_real='$iarea')
													left join tr_area b on b.i_area = a.i_area
													left join tr_customer c on c.i_customer = a.i_customer
													left join tr_salesman f on f.i_salesman=a.i_salesman
													left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
													left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
													left join tm_pelunasan e on (d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area 
													and e.f_pelunasan_cancel='f')
													left join tr_city g on c.i_city = g.i_city and c.i_area = g.i_area

													left join tm_alokasi_item j on(a.i_nota=j.i_nota)
                          left join tm_alokasi k on(j.i_alokasi=k.i_alokasi and j.i_kbank=k.i_kbank and j.i_area=k.i_area 
                          and k.f_alokasi_cancel='f' and k.d_alokasi<='$dto')
                          left join tm_alokasikn_item l on(a.i_nota=l.i_nota)
                          left join tm_alokasikn m on(l.i_alokasi=m.i_alokasi and l.i_kn=m.i_kn and l.i_area=m.i_area 
                          and m.f_alokasi_cancel='f' and m.d_alokasi<='$dto')
                          left join tm_alokasiknr_item n on(a.i_nota=n.i_nota)
                          left join tm_alokasiknr o on(n.i_alokasi=o.i_alokasi and n.i_kn=o.i_kn and n.i_area=o.i_area 
                          and o.f_alokasi_cancel='f' and o.d_alokasi<='$dto')
                          left join tm_alokasihl_item p on(a.i_nota=p.i_nota)
                          left join tm_alokasihl q on(p.i_alokasi=q.i_alokasi and p.i_area=q.i_area and q.f_alokasi_cancel='f' 
                          and q.d_alokasi<='$dto')
 
            	  			  where a.d_jatuh_tempo<='$dto' and a.v_sisa>0 and a.f_nota_cancel='f'
                        and a.i_spb = h.i_spb and a.i_area = h.i_area and h.i_product_group = i.i_product_group
                        ) as a
            	  			  order by a.e_customer_name, a.i_nota desc",false);
                			            }
            $query = $this->db->get();
            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("Opname Nota")->setDescription(NmPerusahaan);
            $objPHPExcel->setActiveSheetIndex(0);
            if ($query->num_rows() > 0)
            {
               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'	=> 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A2:A4'
               );
               $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
               $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
               $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
               $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(20);

               $objPHPExcel->getActiveSheet()->setCellValue('A2', 'DAFTAR NOTA YANG BELUM LUNAS PER '.$peri);
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,16,2);
               $objPHPExcel->getActiveSheet()->setCellValue('A3', "Area:".$iarea."-".$eareaname);
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,16,3);

               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'	=> 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A5:U5'
               );


               $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
               $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('B5', 'KdLang');
               $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Area');
               $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Nama');
               $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Alamat');
               $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Telepon');
               $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('G5', 'TOP');
               $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('H5', 'PKP');
               $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Salesman');
               $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('J5', 'No. SJ');
               $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('K5', 'No. Nota');
               $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Tgl. Nota');
               $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('M5', 'Tgl. JT');
               $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('N5', 'Tgl. Opname');
               $objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('O5', 'Jumlah');
               $objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Sisa Tagihan');
               $objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Toleransi');
               $objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('R5', 'Lama');
               $objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('S5', 'Keterangan');
               $objPHPExcel->getActiveSheet()->getStyle('S5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('T5', 'Status');
               $objPHPExcel->getActiveSheet()->getStyle('T5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('U5', 'Divisi');
               $objPHPExcel->getActiveSheet()->getStyle('U5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $i=6;
               $j=6;
               $xarea='';
               $saldo=0;
               $no=1;
               $nonota='';
               $epelunasan_remark='';
               #bag1
               $saldonyajtp	= 0;
               $totalsaldonya = 0;
               $saldonya90 = 0;
               $saldonya60 = 0;
               $saldonya30 = 0;
               $saldonya0  = 0;
               $saldojalan = 0;
               $footer = array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  
                  ),
                  'font' => array(
                     'name'	=> 'Arial',
                     'bold'  => true,
                     'italic'=> false,
                     'size'  => 10
                  ),
                  'alignment' => array(
                     // 'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
                     'vertical'  => Style_Alignment::VERTICAL_CENTER
                  )
               );
/*                #bag1
               $saldonyajtp	= 0;
               $totalsaldonya = 0;
               $saldonya90 = 0;
               $saldonya60 = 0;
               $saldonya30 = 0;
               $saldonya0  = 0;
               $saldojalan = 0;
               $footer = array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  
                  ),
                  'font' => array(
                     'name'	=> 'Arial',
                     'bold'  => true,
                     'italic'=> false,
                     'size'  => 10
                  ),
                  'alignment' => array(
                     // 'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
                     'vertical'  => Style_Alignment::VERTICAL_CENTER
                  )
               ); */



               foreach($query->result() as $row)
               {
                  $objPHPExcel->getActiveSheet()->duplicateStyleArray
                  (
                     array(
                        'font' => array
                        (
                           'name'   => 'Arial',
                           'bold'   => false,
                           'italic' => false,
                           'size'   => 10
                        )
                     ),
                     'A'.$i.':U'.$i
                  );
                  if($row->f_customer_pkp=='t'){
                    $pkp="Ya";
                  }else{
                    $pkp="Tdk";
                  }
                  $status=$row->v_nota_netto-$row->v_sisa;
                  if($status==0){
                    $ketstatus="Utuh";
                  }else{
                    $ketstatus="Sisa";
                  }
                  if( $nonota <> $row->i_nota)
                  {
                     $lama=datediff("d",$row->d_jatuh_tempo,date("Y-m-d"),false);

                     $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no);
                     $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, "'".$row->i_customer);
                     $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->e_area_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->e_customer_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->e_customer_address);
                     if($lama>0 && $lama<=7){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':U'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF00FF00');
                     }elseif($lama>8 && $lama<=15){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':U'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
                     }elseif($lama>16){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':U'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF0000');
                     }
                     $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->e_customer_phone);
                     $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->n_customer_toplength);
                     $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $pkp);
                     $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, "(".$row->i_salesman.") ".$row->e_salesman_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->i_sj);
                     $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->i_nota);
                     $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row->d_nota);
                     $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $row->d_jatuh_tempo);
                     $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $dto);
                     $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $row->v_nota_netto);
                     $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $row->v_sisa);
                     $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $row->n_toleransi);
                     
                     $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $lama);
                     if(trim($row->e_pelunasan_remark)!='' && $row->e_pelunasan_remark!=null)
                     {
                        $epelunasan_remark = $row->e_pelunasan_remark.",".$row->remlunas;
                        $objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $epelunasan_remark);
                     }else{
                        $row->e_remark=$row->e_remark.",".$row->remlunas;
                        $objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $row->e_remark);
                     }
                     $objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $ketstatus);
                     $objPHPExcel->getActiveSheet()->setCellValue('U'.$i, $row->e_product_groupname);
                     $no++;
                     $i++;
                     $j++;
                  }
                  else
                  {
                     if($row->e_pelunasan_remark!='')
                     {
                        $k = $i-1;
                        $epelunasan_remark = $epelunasan_remark.",".$row->e_pelunasan_remark.",".$row->remlunas;
                        $objPHPExcel->getActiveSheet()->setCellValue('S'.$k, $epelunasan_remark);
                     }else{
                        $k = $i-1;
                        $epelunasan_remark = $epelunasan_remark.",".$row->e_remark;
                        $objPHPExcel->getActiveSheet()->setCellValue('S'.$k, $epelunasan_remark);
                     }
                  }
                  $totalsaldonya += $row->v_sisa;
                  $tgl2 = $row->d_jatuh_tempo;
                  if ($tgl2!='' || $tgl2!=null) {
                     $pecah2 = explode('-', $tgl2);
                     $date2 = $pecah2[2];
                     $month2 = $pecah2[1];
                     $year2 =  $pecah2[0];
                     $jd2 = GregorianToJD($month2, $date2, $year2);
                     $lamaa = $jd1 - $jd2;
                  }else{
                     $lamaa = '0';
                  }

                  if ($lamaa>90) {
                     $saldonya90 += $row->v_sisa;
                  }elseif($lamaa>=61 && $lamaa<=90){
                     $saldonya60 += $row->v_sisa;
                  }elseif ($lamaa>=31 && $lamaa<=60) {
                     $saldonya30 += $row->v_sisa;
                  }elseif ($lamaa>=0 && $lamaa<=30) {
                     $saldonya0 += $row->v_sisa;
                  }elseif ($lamaa<0) {
                     $saldojalan += $row->v_sisa;
                  }

                  $saldonyajtp   = $totalsaldonya-$saldojalan;
                  $p90           = $saldonya90/$totalsaldonya;
                  $p60  		   = $saldonya60/$totalsaldonya;
                  $p30 		      = $saldonya30/$totalsaldonya;
                  $p0 		      = $saldonya0/$totalsaldonya;
                  $pjtp  	  	   = $saldonyajtp/$totalsaldonya;
                  $pj            = $saldojalan/$totalsaldonya;
                  $ptot 		   = $totalsaldonya/$totalsaldonya;



                  $nonota=$row->i_nota;
               }
               #bag4
$i=$i+2;


$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Kategori Keterlambatan');
$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
   array(
      'borders' => array(
         'top'    => array('style' => Style_Border::BORDER_THIN)
      ),
   )
);
/* $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($style_col); */
$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Nilai Piutang');
/* $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($style_col); */
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
   array(
      'borders' => array(
         'top'    => array('style' => Style_Border::BORDER_THIN)
      ),
   )
);

$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, 'Persentase');
/* $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($style_col); */
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
   array(
      'borders' => array(
         'top'    => array('style' => Style_Border::BORDER_THIN)
      ),
   )
);



$i=$i+1;
$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '>90 hari');
$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $saldonya90);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $p90);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_PERCENTAGE_00);

$i=$i+1;
$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '61-90 hari');
$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $saldonya60);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $p60);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_PERCENTAGE_00);

$i=$i+1;
$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '31-60 hari');
$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $saldonya30);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $p30);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_PERCENTAGE_00);

$i=$i+1;
$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, '0-30 hari');
$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $saldonya0);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $p0);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_PERCENTAGE_00);

$i=$i+1;
$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Total Piutang Jatuh Tempo');
$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $saldonyajtp);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $pjtp);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_PERCENTAGE_00);

$i=$i+1;
$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Piutang Berjalan');
$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $saldojalan);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $pj);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($footer);
$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_PERCENTAGE_00);

               $x=$i-1;
            }
            $objPHPExcel->getActiveSheet()->getStyle('N6:O'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $nama='OPN'.$iarea.$dto.'.xls';
            $area=$iarea;
            switch($iarea){
              case '06':
                $area='23';
                break;
              case '08':
                $area='01';
                break;
              case '13':
                $area='11';
                break;
              case '14':
                $area='12';
                break;
              case '15':
                $area='12';
                break;
              case '18':
                $area='11';
                break;
              case '19':
                $area='11';
                break;
              case '20':
                $area='07';
                break;
              case '21':
                $area='11';
                break;
              case '22':
                $area='09';
                break;
              case '24':
                $area='05';
                break;
              case '25':
                $area='07';
                break;
              case '26':
                $area='07';
                break;
              case '27':
                $area='11';
                break;
              case '28':
                $area='11';
                break;
              case '29':
                $area='11';
                break;
              case '30':
                $area='11';
                break;
              case '32':
                $area='11';
                break;
              case '33':
                $area='11';
                break;
              case '16':
                $area='23';
                break;
            }
            if(file_exists('excel/'.$area.'/'.$nama))
            {
               @chmod('excel/'.$area.'/'.$nama, 0777);
               @unlink('excel/'.$area.'/'.$nama);
            }
            $objWriter->save("excel/".$area.'/'.$nama); 

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
	          while($row=pg_fetch_assoc($query)){
	          	$now	  = $row['c'];
			      }
			      $pesan='Export Opname Nota sampai tanggal:'.$dto.' area:'.$iarea;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan ); 

            $data['sukses']   = true;
            $data['inomor']   = "Ekspor Opname Nota Area : ".$iarea."-".$eareaname." berhasil ke file ".$nama;
            $this->load->view('nomor',$data);
         }
         else
         {
            $this->load->view('awal/index.php');
      }
   }
}
?>
