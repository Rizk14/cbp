<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnamenota');
			$data['iarea']='';
			$data['dto']='';
      $data['nt']='';
      $data['jt']='';
			$this->load->view('exp-opnnota/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnamenota');
			$this->load->view('exp-opnnota/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-opnnota/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
											or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-opnnota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('exp-opnnota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/exp-opnnota/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-opnnota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('exp-opnnota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu238')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
         {
            $this->load->model('exp-opnnota/mmaster');
            $dto	= $this->input->post('dto');
            $iarea= $this->input->post('iarea');
            $eareaname= $this->input->post('eareaname');
            $nt   = $this->input->post('chkntx');
            $jt   = $this->input->post('chkjtx');
            if($iarea=='')$iarea=$this->uri->segment(4);
            if($dto=='')$dto=$this->uri->segment(5);
            if($nt=='') $nt=$this->uri->segment(6);
            if($jt=='') $jt=$this->uri->segment(7);
            $this->load->model('exp-opnnota/mmaster');
            if($dto!='')
            {
               $tmp=explode("-",$dto);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $dto=$th."-".$bl."-".$hr;
               $irep=$hr.'-'.$bl.'-'.$th;
               $peri=$hr.' '.mbulan($bl).' '.$th;
            }

            if($nt=='qqq')
            {
               $this->db->select("distinct 
                                  a.*, b.e_area_name, f.e_salesman_name, c.e_customer_name, c.f_customer_pkp, 
                                  c.e_customer_address, c.e_customer_phone, c.n_customer_toplength, x.e_pelunasan_remark, 
                                  d.e_remark as remlunas,
                                  	case when substring(a.i_sj,9,2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                              	  		else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
                          	  			end as d_jatuh_tempo_plustoleransi,
                                  	case when substring(a.i_sj,9,2)='00' then g.n_toleransi_pusat
                              	  		else g.n_toleransi_cabang
                          	  			end as n_toleransi
                      	  			  from tr_area b, tr_city g, tr_customer c, tr_salesman f, tm_nota a
                      	  			  left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                      	  			  left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
                      	  			  left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan
                      	  			  and d.i_dt=e.i_dt and d.i_area=e.i_area
                      	  			  and e.f_pelunasan_cancel='f')
                      	  			  where a.i_area = '$iarea' and a.d_nota<='$dto' and a.v_sisa>0
                      	  			  and a.i_area=b.i_area and a.i_customer=c.i_customer and a.f_nota_cancel='f'
                      	  			  and a.i_salesman=f.i_salesman
                      	  			  and c.i_city = g.i_city and a.i_area = g.i_area
                      	  			  order by c.e_customer_name, a.i_nota desc",false);
                      	  			              }
            else
            {
               $this->db->select("distinct 
                                  a.*, b.e_area_name, f.e_salesman_name, c.e_customer_name, c.f_customer_pkp, 
                                  c.e_customer_address, c.e_customer_phone, c.n_customer_toplength,
                                  x.e_pelunasan_remark, d.e_remark as remlunas, g.n_toleransi_cabang, g.n_toleransi_pusat,
                                  	case when substring(a.i_sj,9,2)='00' then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                                  		else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
                              			end as d_jatuh_tempo_plustoleransi,
                                  	case when substring(a.i_sj,9,2)='00' then g.n_toleransi_pusat
                              	  		else g.n_toleransi_cabang
                          	  			end as n_toleransi
                            			from tr_area b, tr_city g, tr_customer c, tr_salesman f, tm_nota a
                            			left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                            			left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
                            			left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan
                            			and d.i_dt=e.i_dt and d.i_area=e.i_area and e.f_pelunasan_cancel='f')
                            			where a.i_area = '$iarea' 
                            			and a.d_jatuh_tempo<='$dto' and a.v_sisa>0 and a.i_area=b.i_area and a.i_customer=c.i_customer and a.f_nota_cancel='f' and
                            			a.i_salesman=f.i_salesman
                            			and c.i_city = g.i_city and g.i_area=c.i_area order by c.e_customer_name, a.i_nota desc",false);
                			            }
            $query = $this->db->get();
            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("Opname Nota")->setDescription(NmPerusahaan);
            $objPHPExcel->setActiveSheetIndex(0);
            if ($query->num_rows() > 0)
            {
               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'	=> 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A2:A4'
               );
               $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
               $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
               $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
               $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50);
               $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(5);
               $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
               $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
               $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(6);

               $objPHPExcel->getActiveSheet()->setCellValue('A2', 'DAFTAR NOTA YANG BELUM LUNAS PER '.$peri);
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,16,2);
               $objPHPExcel->getActiveSheet()->setCellValue('A3', "Area:".$iarea."-".$eareaname);
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,16,3);

               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'	=> 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A5:R5'
               );


               $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
               $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Area');
               $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('C5', 'KdLang');
               $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Nama');
               $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('E5', 'TOP');
               $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Alamat');
               $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Telepon');
               $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('H5', 'PKP');
               $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Salesman');
               $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('J5', 'No. Nota');
               $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Tgl. Nota');
               $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Tgl. JT');
               $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('M5', 'Tgl. Opname');
               $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('N5', 'Jumlah');
               $objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('O5', 'Sisa Tagihan');
               $objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Lama');
               $objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Keterangan');
               $objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('R5', 'No. SJ');
               $objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('S5', 'Status');
               $objPHPExcel->getActiveSheet()->getStyle('S5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('T5', 'Toleransi');
               $objPHPExcel->getActiveSheet()->getStyle('T5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $i=6;
               $j=6;
               $xarea='';
               $saldo=0;
               $no=1;
               $nonota='';
               $epelunasan_remark='';
               foreach($query->result() as $row)
               {
                  $objPHPExcel->getActiveSheet()->duplicateStyleArray
                  (
                     array(
                        'font' => array
                        (
                           'name'   => 'Arial',
                           'bold'   => false,
                           'italic' => false,
                           'size'   => 10
                        )
                     ),
                     'A'.$i.':T'.$i
                  );
                  if($row->f_customer_pkp=='t'){
                    $pkp="Ya";
                  }else{
                    $pkp="Tdk";
                  }
                  $status=$row->v_nota_netto-$row->v_sisa;
                  if($status==0){
                    $ketstatus="Utuh";
                  }else{
                    $ketstatus="Sisa";
                  }
                  if( $nonota <> $row->i_nota)
                  {
                     $lama=datediff("d",$row->d_jatuh_tempo,date("Y-m-d"),false);

                     $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no);
                     $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, "(".$row->i_area.") ".$row->e_area_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, "'".$row->i_customer);
                     $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->e_customer_name);
                     if($lama>0 && $lama<=7){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':S'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF00FF00');
                     }elseif($lama>8 && $lama<=15){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':S'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
                     }elseif($lama>16){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':S'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF0000');
                     }
                     $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->n_customer_toplength);
                     $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->e_customer_address);
                     $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->e_customer_phone);
                     $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $pkp);
                     $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, "(".$row->i_salesman.") ".$row->e_salesman_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->i_nota);
                     $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->d_nota);
                     $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row->d_jatuh_tempo);
                     $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $dto);
                     $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $row->v_nota_netto);
                     $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $row->v_sisa);
                     $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $lama);
                     if(trim($row->e_pelunasan_remark)!='' && $row->e_pelunasan_remark!=null)
                     {
                        $epelunasan_remark = $row->e_pelunasan_remark.",".$row->remlunas;
                        $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $epelunasan_remark);
                     }else{
                        $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $row->e_remark);
                     }
                     $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $row->i_sj);
                     $objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $ketstatus);
                     $objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $row->n_toleransi);
                     $no++;
                     $i++;
                     $j++;
                  }
                  else
                  {
                     if($row->e_pelunasan_remark!='')
                     {
                        $k = $i-1;
                        $epelunasan_remark = $epelunasan_remark.",".$row->e_pelunasan_remark.",".$row->remlunas;
                        $objPHPExcel->getActiveSheet()->setCellValue('Q'.$k, $epelunasan_remark);
                     }else{
                        $k = $i-1;
                        $epelunasan_remark = $epelunasan_remark.",".$row->e_remark;
                        $objPHPExcel->getActiveSheet()->setCellValue('Q'.$k, $epelunasan_remark);
                     }
                  }
                  $nonota=$row->i_nota;
               }
               $x=$i-1;
            }
            $objPHPExcel->getActiveSheet()->getStyle('N6:O'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $nama='OPN'.$iarea.$dto.'.xls';
            $area=$iarea;
            switch($iarea){
              case '06':
                $area='23';
                break;
              case '08':
                $area='01';
                break;
              case '14':
                $area='12';
                break;
              case '15':
                $area='12';
                break;
              case '18':
                $area='11';
                break;
              case '19':
                $area='11';
                break;
              case '20':
                $area='07';
                break;
              case '21':
                $area='13';
                break;
              case '22':
                $area='09';
                break;
              case '24':
                $area='05';
                break;
              case '25':
                $area='07';
                break;
              case '26':
                $area='07';
                break;
              case '27':
                $area='11';
                break;
              case '28':
                $area='11';
                break;
              case '29':
                $area='11';
                break;
              case '30':
                $area='11';
                break;
              case '32':
                $area='11';
                break;
              case '33':
                $area='11';
                break;
            }
            if(file_exists('excel/'.$area.'/'.$nama))
            {
               @chmod('excel/'.$area.'/'.$nama, 0777);
               @unlink('excel/'.$area.'/'.$nama);
            }
            $objWriter->save("excel/".$area.'/'.$nama); 

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
	          while($row=pg_fetch_assoc($query)){
	          	$now	  = $row['c'];
			      }
			      $pesan='Export Opname Nota sampai tanggal:'.$dto.' area:'.$iarea;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan ); 

            $data['sukses']   = true;
            $data['inomor']   = "Ekspor Opname Nota Area : ".$iarea."-".$eareaname." berhasil ke file ".$nama;
            $this->load->view('nomor',$data);
         }
         else
         {
            $this->load->view('awal/index.php');
      }
   }
}
?>
