<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('printnota');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('printnotakhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
	    	($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printnotakhusus/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
							                    where a.i_customer=b.i_customer and f_ttb_tolak='f'
							                    and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							                    or upper(a.i_nota) like '%$cari%') AND a.f_nota_cancel='f' 
                                  and a.i_area='$iarea' and
																	a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_nota <= to_date('$dto','dd-mm-yyyy') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
      $config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('printnota');
			$this->load->model('printnotakhusus/mmaster');
			$data['inota']='';
			$data['cari']=$cari;
			$data['detail']='';
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['iarea']=$iarea;
			$data['isi']=$this->mmaster->bacasemua($iarea,$dfrom,$dto,$cari,$config['per_page'],$this->uri->segment(7));
			$this->load->view('printnotakhusus/vformview', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
		    (($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu64')=='t')) ||
		    (($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('allmenu')=='t'))
		   ){
			$inota = $this->uri->segment(4);
			$this->load->model('printnotakhusus/mmaster');
			$data['inota']=$inota;
			$data['page_title'] = $this->lang->line('printnota');
			$data['isi']	= $this->mmaster->baca($inota);
			$data['detail']	= $this->mmaster->bacadetail($inota);
      $this->mmaster->updatenota($inota);
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak Nota Khusus No:'.$inota;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printnotakhusus/vformrpt',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printnota');
			$this->load->view('printnotakhusus/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = strtoupper($this->input->post('cari', FALSE));
			if($cari==''){
			  if($this->uri->segment(5)!='sikasep'){
			    $cari=$this->uri->segment(5);
			  }
			}
			$config['base_url'] = base_url().'index.php/printnotakhusus/cform/index/'.$cari.'/sikasep/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										              where a.i_customer=b.i_customer and f_ttb_tolak='f'
										              and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
										              or upper(a.i_nota) like '%$cari%') AND a.f_nota_cancel='f'  ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if($this->uri->segment(6)==''){
				$config['cur_page'] = $this->uri->segment(5);
				$off = $this->uri->segment(5);
			}else{
				$config['cur_page'] = $this->uri->segment(6);
				$off = $this->uri->segment(6);
			}
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printnota');
			$this->load->model('printnotakhusus/mmaster');
			$data['inota']='';
			$data['cari']=$cari;
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$config['cur_page']);
//			$this->load->view('printnotakhusus/vmainform', $data);
			$this->load->view('printnotakhusus/vform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printnotakhusus/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printnotakhusus/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printnotakhusus/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu64')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printnotakhusus/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printnotakhusus/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printnotakhusus/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
