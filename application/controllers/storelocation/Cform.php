<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu4')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/storelocation/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_store_location');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_storelocation');
			$data['istorelocation']='';
			$data['istore']='';
			$data['istorelocationbin']='';
			$this->load->model('storelocation/mmaster');
			
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('storelocation/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu4')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$istorelocation 	= $this->input->post('istorelocation', TRUE);
			$estorelocationname 	= $this->input->post('estorelocationname', TRUE);
			$istore 		= $this->input->post('istore', TRUE);
			$istorelocationbin 	= $this->input->post('istorelocationbin', TRUE);

			if ((isset($istorelocation) && $istorelocation != '') && (isset($estorelocationname) 
			    && $estorelocationname != '') && (isset($istore) && $istore != '') 
			    && (isset($istorelocationbin) && $istorelocationbin != ''))
			{
				$this->load->model('storelocation/mmaster');
				$this->mmaster->insert($istorelocation,$estorelocationname,$istore,$istorelocationbin);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu4')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_storelocation');
			$this->load->view('storelocation/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu4')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_storelocation')." update";
			if( ($this->uri->segment(4)) && ($this->uri->segment(5)) ){
				$istorelocation 	= $this->uri->segment(4);
				$istore	 		= $this->uri->segment(5);
				$istorelocationbin 	= $this->uri->segment(6);
				$data['istorelocation'] = $istorelocation;
				$data['istore'] 	= $istore;
				$data['istorelocationbin'] = $istorelocationbin;
				$this->load->model('storelocation/mmaster');
				$data['isi']=$this->mmaster->baca($istorelocation, $istore, $istorelocationbin);
		 		$this->load->view('storelocation/vmainform',$data);
			}else{
				$this->load->view('storelocation/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu4')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$istorelocation 	= $this->input->post('istorelocation', TRUE);
			$istorelocationbin 	= $this->input->post('istorelocationbin', TRUE);	
			$estorelocationname 	= $this->input->post('estorelocationname', TRUE);
			$istore 		= $this->input->post('istore', TRUE);
			$estorename 		= $this->input->post('estorename', TRUE);
			$this->load->model('storelocation/mmaster');
			$this->mmaster->update($istorelocation,$estorelocationname,$istore,$istorelocationbin);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu4')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$istorelocation 	= $this->uri->segment(4);
			$istore	 		= $this->uri->segment(5);
			$istorelocationbin 	= $this->uri->segment(6);
			$this->load->model('storelocation/mmaster');
			$this->mmaster->delete($istorelocation, $istore, $istorelocationbin);
			
			$config['base_url'] = base_url().'index.php/storelocation/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_store_location');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = '1';
//			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_storelocation');
			$data['istorelocation']='';
			$data['istore']='';
			$data['istorelocationbin']='';
			$this->load->model('storelocation/mmaster');
			
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],0);
//			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('storelocation/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu4')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/storelocation/cform/store/index/';
			$config['total_rows'] = $this->db->count_all('tr_store');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('storelocation/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5));
			$this->load->view('storelocation/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu4')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/storelocation/cform/index/';
			$query = $this->db->query("	select i_store_location from tr_store_location 
										where upper(i_store) like '%$cari%' or upper(e_store_locationname) like '%$cari%'
										or upper(i_store_location) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('storelocation/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_storelocation');
			$data['istorelocation']	='';
			$data['istore']		='';
	 		$this->load->view('storelocation/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu4')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/storelocation/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_store from tr_store where upper(i_store) like '%$cari%' or upper(e_store_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('storelocation/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('storelocation/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
