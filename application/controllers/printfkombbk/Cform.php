<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
    $this->load->library('paginationxx');
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu186')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $config['base_url'] = base_url().'index.php/printfkombbk/cform/index/';
         $cari = strtoupper($this->input->post('cari', FALSE));
         $data['page_title'] = $this->lang->line('printfkombbk');
         $data['cari'] = '';
         $data['dfrom']= '';
         $data['dto']  = '';
         $data['isi']  = '';
         $this->load->view('printfkombbk/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu186')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/printfkombbk/cform/area/index/';
         $iarea1  = $this->session->userdata('i_area');
         $iarea2  = $this->session->userdata('i_area2');
         $iarea3  = $this->session->userdata('i_area3');
         $iarea4  = $this->session->userdata('i_area4');
         $iarea5  = $this->session->userdata('i_area5');

         if($iarea1=='00' or $iarea2=='00' or $iarea3=='00' or $iarea4=='00' or $iarea5=='00'){
            $query = $this->db->query("select * from tr_area",false);
         }else{
            $query = $this->db->query("select * from tr_area where i_area = '$iarea1' or i_area = '$iarea2' or i_area = '$iarea3'
                                 or i_area = '$iarea4' or i_area = '$iarea5'",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->paginationxx->initialize($config);

         $this->load->model('printfkombbk/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iarea1,$iarea2,$iarea3,$iarea4,$iarea5);
         $this->load->view('printfkombbk/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu186')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea1  = $this->session->userdata('i_area');
         $iarea2  = $this->session->userdata('i_area2');
         $iarea3  = $this->session->userdata('i_area3');
         $iarea4  = $this->session->userdata('i_area4');
         $iarea5  = $this->session->userdata('i_area5');
         $config['base_url'] = base_url().'index.php/printfkombbk/cform/area/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         if($iarea1=='00' or $iarea2=='00' or $iarea3=='00' or $iarea4=='00' or $iarea5=='00'){
            $query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
         }else{
            $query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$iarea1' or i_area = '$iarea2'
                                 or i_area = '$iarea3' or i_area = '$iarea4' or i_area = '$iarea5')",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->paginationxx->initialize($config);
         $this->load->model('printfkombbk/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iarea1,$iarea2,$iarea3,$iarea4,$iarea5);
         $this->load->view('printfkombbk/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function view()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu186')=='t')) ||
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('allmenu')=='t'))
         ){

         $cari   = strtoupper($this->input->post('cari'));
         $dfrom   = $this->input->post('dfrom');
         $dto    = $this->input->post('dto');
         $iarea   = $this->input->post('iarea');
         $nama    = $this->input->post('nama');
      if($dfrom=='')$dfrom=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
      if($iarea=='')$iarea=$this->uri->segment(6);
      if($nama=='')$nama=$this->uri->segment(7);
         $config['base_url'] = base_url().'index.php/printfkombbk/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$nama.'/';
         $iarea1  = $this->session->userdata('i_area');
         $iarea2  = $this->session->userdata('i_area2');
         $iarea3  = $this->session->userdata('i_area3');
         $iarea4  = $this->session->userdata('i_area4');
         $iarea5  = $this->session->userdata('i_area5');
      if($iarea1=='00'){
         $query = $this->db->query("select a.* from tm_bbk_pajak a
                                    where upper(a.i_faktur_komersial) like '%$cari%'
                                    and a.d_pajak >= to_date('$dfrom','dd-mm-yyyy') and a.d_pajak <= to_date('$dto','dd-mm-yyyy')
                                    and a.i_area='$iarea' and not a.i_faktur_komersial isnull",false);
      }else{
         $query = $this->db->query("select a.* from tm_bbk_pajak a
                                    where upper(a.i_faktur_komersial) like '%$cari%'
                                    and (a.i_area='$iarea1' or a.i_area='$iarea2' or a.i_area='$iarea3' or a.i_area='$iarea4'
                                    or a.i_area='$iarea5')
                                    and a.d_pajak >= to_date('$dfrom','dd-mm-yyyy') and a.d_pajak <= to_date('$dto','dd-mm-yyyy')
                                    and a.i_area='$iarea' and not a.i_faktur_komersial isnull",false);
      }
#and (a.n_print=0 or a.n_print isnull)
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(8);
         $this->pagination->initialize($config);

         $data['page_title'] = $this->lang->line('printfkombbk');
         $this->load->model('printfkombbk/mmaster');
      $data['dfrom']= $dfrom;
         $data['dto']  = $dto;
         $data['iarea']= $iarea;
         $data['nama']= $nama;
         $data['isi']=$this->mmaster->bacasemua($iarea,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$cari,$config['per_page'],$this->uri->segment(8),$dfrom,$dto);
         $this->load->view('printfkombbk/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cetak()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu186')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $inota  = $this->uri->segment(4);
         $iarea  = $this->uri->segment(5);
         //$nama   = $this->uri->segment(6);
         $nama   = $this->input->post('nama');
         $this->load->model('printfkombbk/mmaster');
         $data['nama']=$nama;
         $data['page_title'] = $this->lang->line('printfkombbk');
         $inota=str_replace('%20','',$inota);
         $this->mmaster->updatebbk($inota,$iarea);
#         $inota=str_replace('%20','',$inota);
         $data['isi']=$this->mmaster->baca($inota,$iarea);
         $data['detail']=$this->mmaster->bacadetail($inota,$iarea);
         $sess=$this->session->userdata('session_id');
         $id=$this->session->userdata('user_id');
         $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
         $rs      =  $this->db->query($sql);
         if($rs->num_rows>0){
            foreach($rs->result() as $tes){
               $ip_address   = $tes->ip_address;
               break;
            }
         }else{
            $ip_address='kosong';
         }
         $data['user']  = $this->session->userdata('user_id');
#        $data['host']  = $this->session->userdata('printerhost');
         $data['host']  = $ip_address;
         $data['uri']   = $this->session->userdata('printeruri');
         $query   = pg_query("SELECT current_timestamp as c");
         while($row=pg_fetch_assoc($query)){
            $now    = $row['c'];
         }
         $pesan='Cetak Faktur Komersial Area '.$iarea.' Nota:'.$inota;
         $this->load->model('logger');
         $this->logger->write($id, $ip_address, $now , $pesan );
         $this->load->view('printfkombbk/vformrpt', $data);
#         $this->mmaster->close($iarea,$inota);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu186')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('printfkombbk');
         $this->load->view('printfkombbk/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cari()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu186')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/printfkombbk/cform/index/';
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $iarea1  = $this->session->userdata('i_area');
         $iarea2  = $this->session->userdata('i_area2');
         $iarea3  = $this->session->userdata('i_area3');
         $iarea4  = $this->session->userdata('i_area4');
         $iarea5  = $this->session->userdata('i_area5');
         $query = $this->db->query("   select a.*, b.e_customer_name from tm_spb a, tr_customer b
                     where a.i_customer=b.i_customer
                     and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                     or upper(a.i_spb) like '%$cari%')
                     and (a.i_area='$iarea1' or a.i_area='$iarea2' or a.i_area='$iarea3' or a.i_area='$iarea4' or a.i_area='$iarea5')",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(4);
         $this->pagination->initialize($config);
         $this->load->model('printfkombbk/mmaster');
         $data['isi']=$this->mmaster->cari($iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$cari,$config['per_page'],$this->uri->segment(5));
         $data['page_title'] = $this->lang->line('printfkombbk');
         $data['cari']=$cari;
         $data['inota']='';
         $data['detail']='';
         $this->load->view('printfkombbk/vmainform',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
