<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu306')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('penjualankonsinyasi');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listbonkonsi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu306')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$icustomer		= $this->input->post('icustomer');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($icustomer=='') $icustomer	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listbonkonsi/cform/view/'.$dfrom.'/'.$dto.'/'.$icustomer.'/index/';
			$query = $this->db->query(" select a.i_notapb, a.d_notapb, a.i_spg, a.i_customer, b.e_spg_name,
                                  a.v_notapb_gross, a.v_notapb_discount, c.e_customer_name, a.f_notapb_cancel
                                  from tm_notapb a, tr_spg b, tr_customer c
                                  where a.i_spg=b.i_spg and a.i_customer=c.i_customer
                                  and (upper(a.i_notapb) like '%$cari%')
                                  and b.i_customer='$icustomer' and
                                  a.d_notapb >= to_date('$dfrom','dd-mm-yyyy') AND
                                  a.d_notapb <= to_date('$dto','dd-mm-yyyy')
						                      ORDER BY a.i_notapb",false);
			$config['total_rows']	= $query->num_rows(); 
			$config['per_page']	= '10';
			$config['first_link']	= 'Awal';
			$config['last_link']	= 'Akhir';
			$config['next_link']	= 'Selanjutnya';
			$config['prev_link']	= 'Sebelumnya';
			$config['cur_page']	= $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('listbonkonsi/mmaster');
			$data['page_title'] = $this->lang->line('penjualankonsinyasi');
			$data['cari']	  = $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	  = $dto;
			$data['icustomer']	= $icustomer;
			$data['isi']	  = $this->mmaster->bacaperiode($icustomer,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data BON KONSINYASI Pelanggan '.$icustomer.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listbonkonsi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu306')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('penjualankonsinyasi');
			$this->load->view('listbonkonsi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu306')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$icustomer		= $this->input->post('icustomer');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($icustomer=='') $icustomer	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listbonkonsi/cform/view/'.$dfrom.'/'.$dto.'/'.$icustomer.'/index/';
		  $query = $this->db->query(" select a.i_notapb, a.d_notapb, a.i_spg, a.i_customer, b.e_spg_name,
                        a.v_notapb_gross, a.v_notapb_discount, c.e_customer_name, a.f_notapb_cancel
                        from tm_notapb a, tr_spg b, tr_customer c
                        where a.i_spg=b.i_spg and a.i_customer=c.i_customer
                        and (upper(a.i_notapb) like '%$cari%')
                        and b.i_customer='$icustomer' and
                        a.d_notapb >= to_date('$dfrom','dd-mm-yyyy') AND
                        a.d_notapb <= to_date('$dto','dd-mm-yyyy')
						            ORDER BY a.i_notapb",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('listbonkonsi/mmaster');
			$data['page_title'] = $this->lang->line('penjualankonsinyasi');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['icustomer']		= $icustomer;
			$data['isi']		= $this->mmaster->bacaperiode($icustomer,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listbonkonsi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu306')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('penjualankonsinyasi')." update";
			if($this->uri->segment(4)!=''){
				$inotapb  = $this->uri->segment(4);
        $inotapb=str_replace("%20","",$inotapb);
				$icustomer= $this->uri->segment(5);
				$dfrom    = $this->uri->segment(6);
				$dto 	    = $this->uri->segment(7);
        $iarea=$this->session->userdata("i_area");
				$data['inotapb']  = $inotapb;
				$data['icustomer']= $icustomer;
				$data['dfrom'] = $dfrom;
				$data['dto']	 = $dto;
				$data['iarea'] = $iarea;
				$query = $this->db->query("select i_product from tm_notapb_item where i_notapb='$inotapb' and i_customer='$icustomer'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('listbonkonsi/mmaster');
				$data['isi']=$this->mmaster->baca($inotapb,$icustomer);
				$data['detail']=$this->mmaster->bacadetail($inotapb,$icustomer);
		 		$this->load->view('listbonkonsi/vformupdate',$data);
			}else{
				$this->load->view('listbonkonsi/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu306')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$xinotapb		= $this->input->post('xinotapb', TRUE);
			$inotapb 		= $this->input->post('inotapb', TRUE);
			$dnotapb 		= $this->input->post('dnotapb', TRUE);
			if($dnotapb!=''){
				$tmp=explode("-",$dnotapb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dnotapb=$th."-".$bl."-".$hr;
				$thbl=substr($th,2,2).$bl;
			}
			$iarea	= $this->input->post('iarea', TRUE);

			$ispg   = $this->input->post('ispg', TRUE);
			$icustomer= $this->input->post('icustomer', TRUE);
			$nnotapbdiscount= $this->input->post('nnotapbdiscount', TRUE);
		  $nnotapbdiscount= str_replace(',','',$nnotapbdiscount);
			$vnotapbdiscount= $this->input->post('vnotapbdiscount', TRUE);
		  $vnotapbdiscount= str_replace(',','',$vnotapbdiscount);
			$vnotapbgross= $this->input->post('vnotapbgross', TRUE);
		  $vnotapbgross	  	= str_replace(',','',$vnotapbgross);
			$jml		= $this->input->post('jml', TRUE);
			if($dnotapb!='' && $inotapb!='' && $jml>0)
			{
				$this->db->trans_begin();
				$this->load->model('listbonkonsi/mmaster');
				$inotapb = 'FB-'.$thbl.'-'.$inotapb;
				$this->mmaster->deleteheader($xinotapb, $iarea, $icustomer);
				$this->mmaster->insertheader($inotapb, $dnotapb, $iarea, $ispg, $icustomer, $nnotapbdiscount, $vnotapbdiscount, $vnotapbgross);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= 'A';
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		  = $this->input->post('vunitprice'.$i, TRUE);
				  $vunitprice	  	= str_replace(',','',$vunitprice);
				  $nquantity   		= $this->input->post('nquantity'.$i, TRUE);
				  $nquantity	  	= str_replace(',','',$nquantity);
          $ipricegroupco  = $this->input->post('ipricegroupco'.$i, TRUE);
          $eremark        = $this->input->post('eremark'.$i, TRUE);
          $this->mmaster->deletedetail($iproduct, $iproductgrade, $xinotapb, $iarea, $icustomer, $iproductmotif, $vunitprice);
				  if($nquantity>0){
				    $this->mmaster->insertdetail( $inotapb,$iarea,$icustomer,$dnotapb,$iproduct,$iproductmotif,$iproductgrade,$nquantity,$vunitprice,$i,$eproductname,$ipricegroupco,$eremark);
				  }
				}
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
#			    $this->db->trans_rollback();
			    $this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	        	$now	  = $row['c'];
			    }
			    $pesan='Update Penjualan Konsinyasi No:'.$inotapb;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan ); 

					$data['sukses']			= true;
					$data['inomor']			= $inotapb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu306')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
     	$area1	= $this->session->userdata('i_area');
     	if($cari=='' && $this->uri->segment(4)!='zxasqw') $cari=$this->uri->segment(4);
     	if($cari==''){
  			$config['base_url'] = base_url().'index.php/listbonkonsi/cform/customer/zxasqw/';
  	  }else{
  			$config['base_url'] = base_url().'index.php/listbonkonsi/cform/customer/'.$cari.'/';
  	  }
			if($area1=='PB' || $area1=='00')
			{
				$query = $this->db->query(" select b.i_customer from tr_spg a, tr_customer b
                                    where a.i_customer=b.i_customer
                                    and (upper(a.i_customer)like'%$cari%' or upper(b.e_customer_name)like'%$cari%')",false);
     	}else{
				$query = $this->db->query(" select a.*, b.e_customer_name from tr_spg a, tr_customer b
                                    where a.i_customer=b.i_customer and a.i_area='$area1'
                                    and (upper(a.i_customer)like'%$cari%' or upper(b.e_customer_name)like'%$cari%')",FALSE);
     	}
     	$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listbonkonsi/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('listbonkonsi/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu306')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   $area1	= $this->session->userdata('i_area');
			$config['base_url'] = base_url().'index.php/listbonkonsi/cform/customer/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$query 	= $this->db->query("select a.*, b.e_customer_name from tr_spg a, tr_customer b
                          where a.i_customer=b.i_customer and a.i_area='$area1'and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                          or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
						              order by a.i_customer",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listbonkonsi/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listbonkonsi/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu306')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      	$cari=strtoupper($this->input->post("cari"));
      	$cust=strtoupper($this->input->post("cust"));
      	if($cust=='') $cust=$this->uri->segment(5);
      	if($cari=='' && $this->uri->segment(6)!='zxasqw' )$cari=$this->uri->segment(6);
      	$baris=$this->input->post("baris");
			if($baris=='')$baris=$this->uri->segment(4);
			if($this->uri->segment(6)=='zxasqw'){
  				$config['base_url'] = base_url().'index.php/listbonkonsi/cform/product/'.$baris.'/'.$cust.'/zxasqw/';
      	}
      	else{
  				$config['base_url'] = base_url().'index.php/listbonkonsi/cform/product/'.$baris.'/'.$cust.'/'.$cari.'/';      
      	}
			$query = $this->db->query(" select a.i_product
										              from tr_product_motif a,tr_product c, tr_product_priceco b, tr_customer_consigment d
										              where a.i_product=c.i_product 
										              and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') 
										              and a.i_product=b.i_product
										              and d.i_customer='$cust' and d.i_price_groupco=b.i_price_groupco and a.i_product_motif='00'
										              ",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya'; 
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('listbonkonsi/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['cust']=$cust;
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(7),$cari,$cust);
			$this->load->view('listbonkonsi/vlistproduct', $data);
		}
		else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu306')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listbonkonsi/cform/product/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
								and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc ", false);
#c.v_product_mill as harga
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('listbonkonsi/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('listbonkonsi/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
