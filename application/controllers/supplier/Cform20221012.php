<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu2')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/supplier/cform/index/';
			$cari				= $this->input->post('cari', TRUE);
			$cari 				= strtoupper($cari);
			$query				= $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%' 
													or upper(e_supplier_name) like '%$cari%' order by i_supplier",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_supplier');
			$data['isupplier']='';
			$this->load->model('supplier/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$data['isupplier']='';

			/*Start Logger*/
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    	while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Menu Supplier';
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 
			/*End Logger*/

			$this->load->view('supplier/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu2')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isupplier				= $this->input->post('isupplier', TRUE);
			$isuppliergroup			= $this->input->post('isuppliergroup', TRUE);
			$esuppliername			= $this->input->post('esuppliername', TRUE);
			$esupplieraddress		= $this->input->post('esupplieraddress', TRUE);
			$esuppliercity			= $this->input->post('esuppliercity', TRUE);
			$esupplierpostalcode	= $this->input->post('esupplierpostalcode', TRUE);
			$esupplierphone			= $this->input->post('esupplierphone', TRUE);
			$esupplierfax			= $this->input->post('esupplierfax', TRUE);
			$esupplierownername		= $this->input->post('esupplierownername', TRUE);
			$esupplierowneraddress	= $this->input->post('esupplierowneraddress', TRUE);
			$esuppliernpwp			= $this->input->post('esuppliernpwp', TRUE);
			$esupplierphone2		= $this->input->post('esupplierphone2', TRUE);
			$esuppliercontact		= $this->input->post('esuppliercontact', TRUE);
			$esupplieremail			= $this->input->post('esupplieremail', TRUE);
			$nsupplierdiscount		= $this->input->post('nsupplierdiscount', TRUE);
			$nsupplierdiscount2		= $this->input->post('nsupplierdiscount2', TRUE);
			$nsuppliertoplength		= $this->input->post('nsuppliertoplength', TRUE);
			$fsupplierpkp			= $this->input->post('fsupplierpkp', TRUE);
			$fsupplierppn			= $this->input->post('fsupplierppn', TRUE);
			if($nsuppliertoplength=='')
				$nsuppliertoplength=0;
			if($nsupplierdiscount=='')
				$nsupplierdiscount=0;
			if($nsupplierdiscount2=='')
				$nsupplierdiscount2=0;
			if ((isset($isupplier) && $isupplier != '') && (isset($esuppliername) && $esuppliername != ''))
			{
//				$this->db->trans_begin();
				$this->load->model('supplier/mmaster');
				$this->mmaster->insert
					(
					$isupplier, $isuppliergroup, $esuppliername, $esupplieraddress, 
					$esuppliercity, $esupplierpostalcode, $esupplierphone, $esupplierfax, 
					$esupplierownername, $esupplierowneraddress, $esuppliernpwp, 
					$esupplierphone2, $esuppliercontact, $esupplieremail, 
					$nsupplierdiscount, $nsupplierdiscount2, $nsuppliertoplength, 
					$fsupplierpkp, $fsupplierppn
					);
				if ($this->db->trans_status() === FALSE)
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();
//					$data['sukses'] = true;
//					$data['inomor']	= $isupplier;
//					$this->load->view('nomor',$data);
					$config['base_url'] = base_url().'index.php/supplier/cform/index/';
					$cari				= $this->input->post('cari', TRUE);
					$cari 				= strtoupper($cari);
					$query				= $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%' 
															or upper(e_supplier_name) like '%$cari%' order by i_supplier",FALSE);
					$config['total_rows'] = $query->num_rows();
					$config['per_page'] = '10';
					$config['first_link'] = 'Awal';
					$config['last_link'] = 'Akhir';
					$config['next_link'] = 'Selanjutnya';
					$config['prev_link'] = 'Sebelumnya';
					$config['cur_page'] = $this->uri->segment(4);
					$this->paginationxx->initialize($config);
					$data['page_title'] = $this->lang->line('master_supplier');
					$data['isupplier']='';
					$this->load->model('supplier/mmaster');
					$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
					$data['isupplier']='';

					/*Start Logger*/
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
					  	$now	  = $row['c'];
					}
					$pesan='Simpan Kode Supplier:'.$supplier;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan ); 
					/*End Logger*/

					$this->load->view('supplier/vmainform', $data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu2')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_supplier');
			$this->load->view('supplier/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu2')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_supplier')." update";
			if($this->uri->segment(4)){
				$isupplier = $this->uri->segment(4);
				$data['isupplier'] = $isupplier;
				$this->load->model('supplier/mmaster');
				$data['isi']=$this->mmaster->baca($isupplier);
		 		$this->load->view('supplier/vformupdate',$data);

		 		/*Start Logger*/
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    	while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Edit Supplier:'.$isupplier;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 
			/*End Logger*/

			}else{
				$this->load->view('supplier/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu2')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isupplier				= $this->input->post('isupplier', TRUE);
			$isuppliergroup			= $this->input->post('isuppliergroup', TRUE);
			$esuppliername			= $this->input->post('esuppliername', TRUE);
			$esupplieraddress		= $this->input->post('esupplieraddress', TRUE);
			$esuppliercity			= $this->input->post('esuppliercity', TRUE);
			$esupplierpostalcode	= $this->input->post('esupplierpostalcode', TRUE);
			$esupplierphone			= $this->input->post('esupplierphone', TRUE);
			$esupplierfax			= $this->input->post('esupplierfax', TRUE);
			$esupplierownername		= $this->input->post('esupplierownername', TRUE);
			$esupplierowneraddress	= $this->input->post('esupplierowneraddress', TRUE);
			$esuppliernpwp			= $this->input->post('esuppliernpwp', TRUE);
			$esupplierphone2		= $this->input->post('esupplierphone2', TRUE);
			$esuppliercontact		= $this->input->post('esuppliercontact', TRUE);
			$esupplieremail			= $this->input->post('esupplieremail', TRUE);
			$nsupplierdiscount		= $this->input->post('nsupplierdiscount', TRUE);
			$nsupplierdiscount2		= $this->input->post('nsupplierdiscount2', TRUE);
			$nsuppliertoplength		= $this->input->post('nsuppliertoplength', TRUE);
			$fsupplierpkp			= $this->input->post('fsupplierpkp', TRUE);
			$fsupplierppn			= $this->input->post('fsupplierppn', TRUE);
			if($nsuppliertoplength=='')
				$nsuppliertoplength=0;
			if($nsupplierdiscount=='')
				$nsupplierdiscount=0;
			if($nsupplierdiscount2=='')
				$nsupplierdiscount2=0;
			$this->db->trans_begin();
			$this->load->model('supplier/mmaster');
			$this->mmaster->update(
						$isupplier, $isuppliergroup, $esuppliername, $esupplieraddress, 
						$esuppliercity, $esupplierpostalcode, $esupplierphone, $esupplierfax, 
						$esupplierownername, $esupplierowneraddress, $esuppliernpwp, 
						$esupplierphone2, $esuppliercontact, $esupplieremail, 
						$nsupplierdiscount, $nsupplierdiscount2, $nsuppliertoplength, 
						$fsupplierpkp, $fsupplierppn
					      );
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
//				$data['sukses'] = true;
//				$data['inomor']	= $isupplier;
//				$this->load->view('nomor',$data);
				$config['base_url'] = base_url().'index.php/supplier/cform/index/';
				$cari				= $this->input->post('cari', TRUE);
				$cari 				= strtoupper($cari);
				$query				= $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%' 
														or upper(e_supplier_name) like '%$cari%' order by i_supplier",FALSE);
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('master_supplier');
				$data['isupplier']='';
				$this->load->model('supplier/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$data['isupplier']='';

				/*Start Logger*/
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
	    		while($row=pg_fetch_assoc($query)){
	    		$now	  = $row['c'];
				}
				$pesan='Updete Kode Supplier:'.$isuppliergroup;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan ); 
				/*End Logger*/

				$this->load->view('supplier/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu2')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isupplier	= $this->uri->segment(4);
			$this->load->model('supplier/mmaster');
			$this->mmaster->delete($isupplier);

			$config['base_url'] = base_url().'index.php/supplier/cform/index/';
			$cari				= $this->input->post('cari', TRUE);
			$cari = strtoupper($cari);
			$query				= $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%' 
													or upper(e_supplier_name) like '%$cari%' order by i_supplier",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_supplier');
			$data['isupplier']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('supplier/vmainform', $data);

			/*Start Logger*/
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    	while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Delete Kode Supplier No:'.$isuppliergroup;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 
			/*End Logger*/

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu2')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/supplier/cform/index/';
			$cari				= $this->input->post('cari', TRUE);
			$cari 				= strtoupper($cari);
			$query				= $this->db->query("select * from tr_supplier where upper(i_supplier) like '%$cari%' 
													or upper(e_supplier_name) like '%$cari%' order by i_supplier",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('supplier/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('master_supplier');
			$data['isupplier']='';

			/*Start Logger*/
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    	while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Cari Kode Supplier:'.$supplier;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 
			/*End Logger*/
			
	 		$this->load->view('supplier/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function suppliergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu2')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$config['base_url'] = base_url().'index.php/supplier/cform/suppliergroup/index/';
			$cari				= $this->input->post('cari', TRUE);
			$cari 				= strtoupper($cari);
			$query				= $this->db->query("select * from tr_supplier_group where upper(i_supplier_group) like '%$cari%' 
													or upper(e_supplier_groupname) like '%$cari%' order by i_supplier_group",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('master_suppliergroup');
			$data['isupplier']='';
			$this->load->model('supplier/mmaster');
			$data['isi']=$this->mmaster->bacagroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('supplier/vlistsuppliergroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisuppliergroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu2')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/supplier/cform/suppliergroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_supplier_group 
										where upper(i_supplier_group) like '%$cari%' 
										or upper(e_supplier_groupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('supplier/mmaster');
			$data['page_title'] = $this->lang->line('list_suppliergroup');
			$data['isi']=$this->mmaster->carigroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('supplier/vlistsuppliergroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
