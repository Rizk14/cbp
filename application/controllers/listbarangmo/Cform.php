<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu499')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbarangmo');
			$data['igroup']='';
			$this->load->view('listbarangmo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu499')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $igroup	  = $this->uri->segment(4); 
      $egroupname	  = $this->uri->segment(5);
			$this->load->model('listbarangmo/mmaster');
			$data['page_title']   = $this->lang->line('listbarangmo');
			$data['igroup']		    = $igroup;
			$data['egroupname']		= $egroupname;
			$data['isi']		= $this->mmaster->bacabarang($igroup);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Master Barang MO Group Harga:'.$igroup;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('listbarangmo/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu499')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbarangmo');
			$this->load->view('listbarangmo/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function group()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu499')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea	= $this->session->userdata('i_area');
			$iuser	= $this->session->userdata('user_id');
			$user  =  strtoupper($iuser);
      $query = $this->db->query("select b.*, c.e_price_groupconame from tr_spg a, tr_customer_consigment b, tr_price_groupco c
                                 where a.i_spg ='$user' and b.i_customer = a.i_customer and b.i_price_groupco = c.i_price_groupco");
          if ($query->num_rows() > 0){
            
            $raw	= $query->row();
            $icust    = $raw->i_customer;
            $igroup   = $raw->i_price_groupco;
            $egroupname   = $raw->e_price_groupconame;
            $status   = 'group';
          }else{
              $status = 'all';
              $igroup = '';
              $icust  = '';
              $egroupname = '';
            }
            
			    $config['base_url'] = base_url().'index.php/listbarangmo/cform/group/index/';
			    if($status=='all'){
				    $query = $this->db->query("select * from tr_price_groupco",false);
			    }else{
				    $query = $this->db->query("select * from tr_price_groupco where i_price_groupco = '$igroup'",false);
			    }
			    
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listbarangmo/mmaster');
			$data['page_title'] = $this->lang->line('list_groupco');
			$data['isi']=$this->mmaster->bacagroup($config['per_page'],$this->uri->segment(5),$igroup,$status);
			$this->load->view('listbarangmo/vlistgroup', $data);
			
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carigroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu499')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listsj/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listsj/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listsj/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu499')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   
			$igroup		  = $this->input->post('igroup');
			$egroupname	= $this->input->post('egroupname');

			$this->load->model('listbarangmo/mmaster');
			$data['page_title']   = $this->lang->line('listbarangmo');
			$data['igroup']		    = $igroup;
			$data['egroupname']		= $egroupname;
			$isi		              = $this->mmaster->bacabarang($igroup);
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();

			$objPHPExcel->getProperties()->setTitle("Daftar Master Barang MO Group ".$egroupname)->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      if(count($isi)>0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(

						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,

						'wrap'      => true
					)
				),
				'A1:F1'
				);

        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'KODE BARANG');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(

							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'NAMA BARANG');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)

						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'KODE HARGA');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),

					)
				);			
			  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'GROUP');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),

					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('E1', 'GRADE');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'HARGA');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)

				);
        $i=2;

        foreach($isi as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
		      array(
			      'font' => array(
				      'name'	=> 'Arial',

				      'bold'  => false,
				      'italic'=> false,
				      'size'  => 10
			      )
		      ),

		      'A'.$i.':F'.$i
		      );
		      
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_product, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->e_product_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_price_group, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->e_price_groupconame, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, 'A', Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->v_product_retail, Cell_DataType::TYPE_NUMERIC);
          $i++;
          }

			  $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $nama='Export Daftar Master Barang MO Group '.$egroupname.'.xls';
        if(file_exists('excel/'.$nama)){
          @chmod('excel/'.$nama, 0777);
          @unlink('excel/'.$nama);
        }
			  $objWriter->save('excel/00/'.$nama);
			  $sess=$this->session->userdata('session_id');

			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Export Daftar Master Barang MO Group '.$egroupname;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['sukses']			= true;
			  $data['inomor']			= $pesan;
			  $this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
  		}
    }
  }
}
?>
