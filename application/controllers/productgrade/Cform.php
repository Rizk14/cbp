<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu16')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_productgrade');
			$data['iproductgrade']='';
			$this->load->model('productgrade/mmaster');
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('productgrade/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu16')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproductgrade 	= $this->input->post('iproductgrade', TRUE);
			$eproductgradename 	= $this->input->post('eproductgradename', TRUE);

			if ($iproductgrade != '' && $eproductgradename != '')
			{
				$this->load->model('productgrade/mmaster');
				$this->mmaster->insert($iproductgrade,$eproductgradename);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu16')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_productgrade');
			$this->load->view('productgrade/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu16')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_productgrade')." update";
			if($this->uri->segment(4)){
				$iproductgrade = $this->uri->segment(4);
				$data['iproductgrade'] = $iproductgrade;
				$this->load->model('productgrade/mmaster');
				$data['isi']=$this->mmaster->baca($iproductgrade);
		 		$this->load->view('productgrade/vmainform',$data);
			}else{
				$this->load->view('productgrade/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu16')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproductgrade		= $this->input->post('iproductgrade', TRUE);
			$eproductgradename 	= $this->input->post('eproductgradename', TRUE);
			$this->load->model('productgrade/mmaster');
			$this->mmaster->update($iproductgrade,$eproductgradename);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu16')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproductgrade		= $this->uri->segment(4);
			$this->load->model('productgrade/mmaster');
			$this->mmaster->delete($iproductgrade);
			$data['page_title'] = $this->lang->line('master_productgrade');
			$data['iproductgrade']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('productgrade/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
