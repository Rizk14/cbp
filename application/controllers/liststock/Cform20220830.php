<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu61')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('liststock');
			$data['iarea']='';
			$this->load->view('liststock/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu61')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$iarea		= $this->input->post('iarea');
			if($iarea=='') $iarea	= $this->uri->segment(4);
      	$istore = $this->input->post('istore');
      	$istorelocation = $this->input->post('istorelocation');
			if($istore=='') $istore=$this->uri->segment(5);
			if($istorelocation=='') $istorelocation=$this->uri->segment(6);

			$config['base_url'] = base_url().'index.php/liststock/cform/view/'.$iarea.'/'.$istore.'/'.$istorelocation.'/index/';
			$sql="	select a.i_product, a.i_product_motif, a.i_product_grade, a.i_store, a.i_store_location, a.i_store_locationbin, a.e_product_name, 
							a.n_quantity_stock, a.f_product_active, c.e_product_motifname 
							from tm_ic a, tr_store b, tr_product_motif c 
							where a.i_store=b.i_store and a.i_product_motif = c.i_product_motif and a.i_product = c.i_product
							and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
							and a.i_store='$istore' and a.i_store_location='$istorelocation'
							order by a.i_product";
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('liststock');
			$this->load->model('liststock/mmaster');
			$data['cari']	= $cari;
			$data['iarea']= $iarea;
			$data['istore']		= $istore;
			$data['istorelocation']		= $istorelocation;
      	$data['cur']  = $this->uri->segment(8);
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$istore,$istorelocation,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Stock Area '.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('liststock/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu61')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$iarea		= $this->input->post('iarea');
      $istore = $this->input->post('istore');
      $istorelocation = $this->input->post('istorelocation');
			if($iarea=='') $iarea	= $this->uri->segment(4);
			if($istore=='') $istore=$this->uri->segment(5);
			if($istorelocation=='') $istorelocation=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/liststock/cform/view/'.$iarea.'/'.$istore.'/'.$istorelocation.'/index/';
			$sql="	select a.i_product, a.i_product_motif, a.i_product_grade, a.i_store, a.i_store_location, a.i_store_locationbin, a.e_product_name, 
							a.n_quantity_stock, a.f_product_active, c.e_product_motifname 
							from tm_ic a, tr_store b, tr_product_motif c 
							where a.i_store=b.i_store and a.i_product_motif = c.i_product_motif and a.i_product = c.i_product
							and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
							and a.i_store='$istore' and a.i_store_location='$istorelocation'
							order by a.i_product";
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('liststock');
			$this->load->model('liststock/mmaster');
			$data['cari']		= $cari;
			$data['iarea']		= $iarea;
			$data['istore']		= $istore;
			$data['istorelocation']		= $istorelocation;
      $data['cur']  = $this->uri->segment(6);
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$istore,$istorelocation,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('liststock/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu61')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/liststock/cform/area/index/';
			$iuser   = $this->session->userdata('user_id');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
			                        from tr_area a, tr_store b 
			                        where a.i_store=b.i_store and (a.i_area in (select i_area from tm_user_area where i_user='$iuser') )
			                        group by a.i_store, b.e_store_name",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('liststock/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('liststock/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu61')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   $iuser   = $this->session->userdata('user_id');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/liststock/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query = $this->db->query("select distinct on (a.i_store) a.i_store, b.e_store_name 
                                 from tr_area a, tr_store b 
                                 where a.i_store=b.i_store 
                                 and (a.i_area in (select i_area from tm_user_area where i_user='$iuser') )
                                 and (upper(a.i_area) like '%$cari%' or upper(a.e_area_name) like '%$cari%')
                                 group by a.i_store, b.e_store_name
                                 order by a.i_store, b.e_store_name",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('liststock/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('liststock/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
