<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu339') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = "Lap Pembelian Per Supplier Per Item";
			$data['iperiode']	= '';
			$data['isupplier']	  = '';

			$this->load->view('exp-beliperitem/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu339') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-beliperitem/cform/supplier/index/';
			$config['total_rows'] = $this->db->count_all('tr_supplier');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-beliperitem/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi'] = $this->mmaster->bacasupplier($config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-beliperitem/vlistsupplier', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu339') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-beliperitem/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_supplier 
										where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-beliperitem/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi'] = $this->mmaster->carisupplier($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-beliperitem/vlistsupplier', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu339') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-beliperitem/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$isupplier = $this->input->post('isupplier');
			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}

			$a = substr($iperiode, 0, 4);
			$b = substr($iperiode, 4, 2);
			$peri = mbulan($b) . " - " . $a;

			if ($isupplier == '') $isupplier = $this->uri->segment(5);
			$qsupname	= $this->mmaster->esupname($isupplier);
			if ($qsupname->num_rows() > 0) {
				$row_supname	= $qsupname->row();
				$sname   = $row_supname->e_supplier_name;
			} else {
				$sname   = '';
			}
			$this->db->select("	distinct on (a.i_product) a.i_product, a.i_supplier, b.e_supplier_name
                          from tm_dtap_item a
                          inner join tr_supplier b on (a.i_supplier=b.i_supplier)
                          where to_char(a.d_dtap::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                          and a.i_supplier='$isupplier'
                          order by a.i_product ", false);

			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan Pembelian PerSupplier PerItem")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan Pembelian PerSupplier PerItem');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 5, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Bulan : ' . $peri . ' - Supplier : ' . $sname);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 5, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:F5'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Harga/Pcs');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Total Rp');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);


				$i = 6;
				$j = 6;
				$no = 0;
				foreach ($query->result() as $row) {
					$no++;
					$product = $row->i_product;
					$query = $this->db->query(" select a.i_product, a.e_product_name, a.v_pabrik, sum(a.n_jumlah) as qtyy
                                      from tm_dtap_item a, tm_dtap c
                                      where to_char(a.d_dtap::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                                      and a.i_supplier='$isupplier' and a.i_dtap=c.i_dtap and c.f_dtap_cancel='f'
                                      and a.i_product='$product' and a.i_area=c.i_area and a.i_supplier=c.i_supplier
                                      group by a.i_product, a.e_product_name, a.v_pabrik
                                      order by i_product");
					if ($query->num_rows() > 0) {
						foreach ($query->result() as $raw) {
							$objPHPExcel->getActiveSheet()->duplicateStyleArray(
								array(
									'font' => array(
										'name'	=> 'Arial',
										'bold'  => false,
										'italic' => false,
										'size'  => 10
									)
								),
								'A' . $i . ':F' . $i
							);
							$qty = $raw->qtyy;
							$harga = $raw->v_pabrik;
							$nmbrg = $raw->e_product_name;

							if ($harga != 0 || $qty != 0) {
								$total = $harga * $qty;
							} else {
								$total = 0;
							}
							$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $j - 5, Cell_DataType::TYPE_NUMERIC);
							$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->i_product, Cell_DataType::TYPE_STRING);
							$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $nmbrg, Cell_DataType::TYPE_STRING);
							$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $qty, Cell_DataType::TYPE_NUMERIC);
							$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $harga, Cell_DataType::TYPE_NUMERIC);
							$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $total, Cell_DataType::TYPE_NUMERIC);
							$i++;
							$j++;
						}
					}
				}
				$x = $i - 1;
				# $objPHPExcel->getActiveSheet()->getStyle('E6:F'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama = 'LapBeliPerItem-' . $isupplier . '-' . $iperiode . '.xls';
			$objWriter->save('beli/' . $nama);

			$this->logger->writenew("LapPembelian_persupp_peritem_" . $isupplier . '-' . $iperiode);

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$data['folder']	= 'beli';

			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
