<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu323') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('exp-orderkons');
			$data['datefrom'] = '';
			$data['dateto']	= '';
			$this->load->view('exp-orderkons/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu323') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-orderkons/mmaster');
			$icustomer    = $this->input->post('icustomer');
			$datefrom = $this->input->post('datefrom');
			$dateto	  = $this->input->post('dateto');
			if ($datefrom != '') {
				$tmp = explode("-", $datefrom);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$datefrom = $th . "-" . $bl . "-" . $hr;
				$periodeawal	= $hr . "/" . $bl . "/" . $th;
			}
			if ($dateto != '') {
				$tmp = explode("-", $dateto);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dateto = $th . "-" . $bl . "-" . $hr;
				$periodeakhir   = $hr . "/" . $bl . "/" . $th;
			}
			$qcustname	= $this->mmaster->ecustname($icustomer);
			if ($qcustname->num_rows() > 0) {
				$row_custname	= $qcustname->row();
				$cname   = $row_custname->e_customer_name;
			} else {
				$cname   = '';
			}

			$satu = substr($datefrom, 2, 2);
			$dua = substr($datefrom, 5, 2);

			$boocir = $satu . $dua;
			if ($dua == '01') $dua = '13';
			$blaw = $dua - 1;
			if ($dua == '02') $dua = '14';
			$blak = $dua - 2;
			$dua = substr($datefrom, 5, 2);

			if ($blaw == '01') {
				$bulanawal = 'Januari';
			} elseif ($blaw == '02') {
				$bulanawal = 'Februari';
			} elseif ($blaw == '03') {
				$bulanawal = 'Maret';
			} elseif ($blaw == '04') {
				$bulanawal = 'April';
			} elseif ($blaw == '05') {
				$bulanawal = 'Mei';
			} elseif ($blaw == '06') {
				$bulanawal = 'Juni';
			} elseif ($blaw == '07') {
				$bulanawal = 'Juli';
			} elseif ($blaw == '08') {
				$bulanawal = 'Agustus';
			} elseif ($blaw == '09') {
				$bulanawal = 'September';
			} elseif ($blaw == '10') {
				$bulanawal = 'Oktober';
			} elseif ($blaw == '11') {
				$bulanawal = 'November';
			} elseif ($blaw == '12') {
				$bulanawal = 'Desember';
			}

			if ($blak == '01') {
				$bulanakhir = 'Januari';
			} elseif ($blak == '02') {
				$bulan = 'Februari';
			} elseif ($blak == '03') {
				$bulanakhir = 'Maret';
			} elseif ($blak == '04') {
				$bulanakhir = 'April';
			} elseif ($blak == '05') {
				$bulanakhir = 'Mei';
			} elseif ($blak == '06') {
				$bulanakhir = 'Juni';
			} elseif ($blak == '07') {
				$bulanakhir = 'Juli';
			} elseif ($blak == '08') {
				$bulanakhir = 'Agustus';
			} elseif ($blak == '09') {
				$bulanakhir = 'September';
			} elseif ($blak == '10') {
				$bulanakhir = 'Oktober';
			} elseif ($blak == '11') {
				$bulanakhir = 'November';
			} elseif ($blak == '12') {
				$bulanakhir = 'Desember';
			}


			$data['page_title'] = $this->lang->line('exp-orderkons');
			$this->db->select("	distinct(b.i_product), b.e_product_name, b.n_quantity_order, b.n_quantity_stock, d.v_product_retail
                            from tm_orderpb_item b, tr_customer c, tm_orderpb a, tr_product d
                            where b.i_customer=c.i_customer and b.i_customer=a.i_customer
                            and b.i_customer='$icustomer' and b.i_product=d.i_product
                            and(b.d_orderpb >= to_date('$datefrom','yyyy-mm-dd') and
                            b.d_orderpb <= to_date('$dateto','yyyy-mm-dd'))
                            and a.f_orderpb_cancel='f'
                            order by b.i_product", false);
			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar Order Konsinyasi")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			$i = 0;
			$totorder = 0;
			$x = 0;
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A3'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar Order Konsinyasi - ' . $cname);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 11, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Tanggal Order : ' . $periodeawal . ' - ' . $periodeakhir);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 11, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:L6'
				);

				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 5, 0, 6);
				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A5:A6')->applyFromArray(
					array(
						'borders'   => array(
							'top'       => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(1, 5, 1, 6);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('B5:B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(2, 5, 2, 6);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('C5:C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, 5, 3, 6);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Jml Order');
				$objPHPExcel->getActiveSheet()->getStyle('D5:D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4, 5, 4, 6);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Jml Stock');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, 5, 6, 5);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Jual');
				$objPHPExcel->getActiveSheet()->getStyle('F5:G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7, 5, 7, 6);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Total');
				$objPHPExcel->getActiveSheet()->getStyle('H5:H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	 => array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, 5, 8, 6);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Rata2');
				$objPHPExcel->getActiveSheet()->getStyle('I5:I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	 => array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(9, 5, 9, 6);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('J5:J6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	 => array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(10, 5, 10, 6);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Total');
				$objPHPExcel->getActiveSheet()->getStyle('K5:K6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	 => array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(11, 5, 11, 6);
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('L5:L6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	 => array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', $bulanakhir);
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	 => array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', $bulanawal);
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	 => array('style' => Style_Border::BORDER_THIN),
							'bottom' => array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i = 7;
				$j = 7;
				$no = 0;
				$totorder = 0;

				foreach ($query->result() as $row) {
					$no++;
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':L' . $i
					);

					$boo = $boocir - 1;
					if (substr($boo, 2, 2) == '00') {
						$xx = substr($boocir, 0, 2) - 1;
						$boo = $xx . '12';
					}
					$cir = $boocir - 2;
					if (substr($cir, 2, 2) == '00') {
						$xx = substr($boocir, 0, 2) - 1;
						$cir = $xx . '12';
					} elseif (substr($cir, 2, 2) == '99') {
						$xx = substr($boocir, 0, 2) - 1;
						$cir = $xx . '11';
					}


					$fbaw = 'FB-' . $boo;
					$fbak = 'FB-' . $cir;

					$product = $row->i_product;
					$total = $row->n_quantity_order * $row->v_product_retail;
					$totorder = $totorder + ($row->v_product_retail * $row->n_quantity_order);

					$query = $this->db->query(" select trunc(sum(n_quantity)) as njuala, trunc(sum(n_quantity)/2) as nrata, i_product
                                        from tm_notapb_item
                                        where i_notapb like '$fbaw%' and i_customer='$icustomer' and i_product='$product'
                                        group by i_product  ");

					if ($query->num_rows() > 0) {
						foreach ($query->result() as $raw) {
							$njualaw = $raw->njuala;
							$nrataw = $raw->nrata;
						}
					} else {
						$njualaw = 0;
						$nrataw = 0;
					}

					$que = $this->db->query(" select trunc(sum(n_quantity)) as njualo, trunc(sum(n_quantity)/2) as nrata, i_product
                                        from tm_notapb_item
                                        where i_notapb like '$fbak%' and i_customer='$icustomer' and i_product='$product'
                                        group by i_product  ");

					if ($que->num_rows() > 0) {
						foreach ($que->result() as $rew) {
							$njualak = $rew->njualo;
							$nratak = $rew->nrata;
						}
					} else {
						$njualak = 0;
						$nratak = 0;
					}

					$jualtotal = $njualaw + $njualak;
					$ratatotal = $nrataw + $nratak;

					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':K' . $i
					);

					$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $j - 6, $no);
					$objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->i_product);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->n_quantity_order);
					$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $row->n_quantity_stock);
					$objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $njualak);
					$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $njualaw);
					$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $jualtotal);
					$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $ratatotal);
					$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->v_product_retail);
					$objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $total);
					$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, '');
					$objPHPExcel->getActiveSheet()->getStyle('L' . $i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN),
								'bottom' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$i++;
					$j++;

					$x = $i - 1;
				}
			}
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, 'TOTAL');
			$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $totorder);
			$objPHPExcel->getActiveSheet()->getStyle('K' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER);
			$objPHPExcel->getActiveSheet()->getStyle('D7:K' . $x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_NUMBER);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A6:A' . $x
			);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'D6:H' . $x
			);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'alignment' => array(
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'C' . $i . ':C' . $i
			);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'alignment' => array(
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'I6:K' . $i
			);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				),
				'A' . $i . ':L' . $i
			);
			$objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$i = $i + 2;
			$ii = $i + 2;
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'D' . $i . ':L' . $ii
			);
			$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A' . $i . ':A' . $ii
			);
			$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, 'TL');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, 'Admin');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, 'Gudang');
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7, $i, 8, $i);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7, $i, 8, $i);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7, $i, 8, $i);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7, $i, 8, $i);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7, $i, 8, $i);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);

			$i++;
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3, $i, 4, $i);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, $i, 6, $i);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);
			$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7, $i, 8, $i);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
				array(
					'borders' => array(
						'left' 	=> array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN)
					),
				)
			);


			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama = 'orderkonsinyasi-' . substr($datefrom, 5, 2) . '-' . substr($datefrom, 0, 4) . '.xls';
			$objWriter->save('excel/PB/' . $nama);
			// $sess = $this->session->userdata('session_id');
			// $id = $this->session->userdata('user_id');
			// $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			// $rs		= pg_query($sql);
			// if (pg_num_rows($rs) > 0) {
			// 	while ($row = pg_fetch_assoc($rs)) {
			// 		$ip_address	  = $row['ip_address'];
			// 		break;
			// 	}
			// } else {
			// 	$ip_address = 'kosong';
			// }
			// $query 	= pg_query("SELECT current_timestamp as c");
			// while ($row = pg_fetch_assoc($query)) {
			// 	$now	  = $row['c'];
			// }
			// $pesan = 'Export Order Konsinyasi tanggal:' . $datefrom . ' Supplier:' . $dateto;
			// $this->load->model('logger');
			// $this->logger->write($id, $ip_address, $now, $pesan);

			$this->logger->writenew('Export Order Konsinyasi tanggal:' . $datefrom . ' Supplier:' . $dateto);

			$data['sukses'] = true;
			$data['inomor']	= $nama;/* "Order Konsinyasi" */
			$data['folder']	= 'excel/PB';/* "Order Konsinyasi" */

			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu306') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-orderkons/cform/customer/index/';
			$icustomer	= $this->input->post('i_customer');
			$query = $this->db->query(" select a.*, b.e_customer_name from tr_spg a, tr_customer b
                    where a.i_customer=b.i_customer order by a.i_customer", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-orderkons/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi'] = $this->mmaster->bacacustomer($config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-orderkons/vlistcustomer', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu306') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-orderkons/cform/customer/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$query 	= $this->db->query("select a.*, b.e_customer_name from tr_spg a, tr_customer b
                          where a.i_customer=b.i_customer and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                          or upper(a.i_spg) like '%$cari%' or upper(a.e_spg_name) like '%$cari%')
						              order by a.i_customer", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-orderkons/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi'] = $this->mmaster->caricustomer($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-orderkons/vlistcustomer', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu323') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-orderkons/cform/area/index/';
			$area1   = $this->session->userdata('i_area');
			$area2   = $this->session->userdata('i_area2');
			$area3   = $this->session->userdata('i_area3');
			$area4   = $this->session->userdata('i_area4');
			$area5   = $this->session->userdata('i_area5');
			if ($area1 == '00') {
				$query = $this->db->query("select * from tr_area", false);
			} else {
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                                 or i_area = '$area4' or i_area = '$area5'", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-orderkons/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('exp-orderkons/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu323') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$area1	= $this->session->userdata('i_area1');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url() . 'index.php/exp-orderkons/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if ($area1 == '00') {
				$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')", false);
			} else {
				$query 	= $this->db->query("select * from tr_area
                                          where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
                                          and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                                          or i_area = '$area4' or i_area = '$area5')", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-orderkons/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('exp-orderkons/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
