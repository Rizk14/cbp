<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('cekbon');
			$data['dfrom']='';
			$data['dto']='';	
			$data['inotapb']='';			
			$this->load->view('cekbonkons/vmainform', $data);			
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari		    = strtoupper($this->input->post('cari'));
			$cari		    = strtoupper($cari);
			$dfrom		  = $this->input->post('dfrom');
			$dto		    = $this->input->post('dto');
			$icustomer	= $this->input->post('icustomer');

			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($icustomer=='') $icustomer	= $this->uri->segment(6);

			$config['base_url'] = base_url().'index.php/cekbonkons/cform/view/'.$dfrom.'/'.$dto.'/'.$icustomer.'/index/';

			$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name, d.e_spg_name
                                  from tm_notapb a, tr_customer b, tr_area c, tr_spg d
                                  where a.i_customer=b.i_customer
                                  and a.i_area=c.i_area and b.i_area=c.i_area
                                  and a.i_spg=d.i_spg and b.i_customer=d.i_customer
                                  and (upper(a.i_notapb) like '%$cari%' or upper(a.i_spg) like '%$cari%'
                                      or upper(d.e_spg_name) like '%$cari%')
                                  and a.i_customer='$icustomer'
                                  and (a.d_notapb >= to_date('$dfrom','dd-mm-yyyy')
                                  and a.d_notapb <= to_date('$dto','dd-mm-yyyy'))
                                  order by a.d_notapb, a.i_notapb",false);

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8,0);
			$this->pagination->initialize($config);
			$data['create_links']	= $this->pagination->create_links();
			$data['page_title'] 	= $this->lang->line('cekbon');
			$this->load->model('cekbonkons/mmaster');
			$data['inotapb']		= '';
			$data['cari']		    = $cari;
			$data['dfrom']		  = $dfrom;
			$data['dto']		    = $dto;
			$data['icustomer']	= $icustomer;
			$data['isi']=$this->mmaster->bacasemua($icustomer,$dfrom,$dto,$cari,$config['per_page'],$config['cur_page']);
			$this->load->view('cekbonkons/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('cekbon');
			$this->load->view('cekbonkons/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			
			$dfrom	    = $this->input->post('dfrom')?$this->input->post('dfrom'):$this->uri->segment(4);
			$dto	      = $this->input->post('dto')?$this->input->post('dto'):$this->uri->segment(5);
			$icustomer	= $this->input->post('icustomer')?$this->input->post('icustomer'):$this->uri->segment(6);
			$cari       = $this->input->post('cari', FALSE);
			$cari       = strtoupper($cari);

			$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name, d.e_spg_name
                                  from tm_notapb a, tr_customer b, tr_area c, tr_spg d
                                  where a.i_customer=b.i_customer
                                  and a.i_area=c.i_area and b.i_area=c.i_area
                                  and a.i_spg=d.i_spg and b.i_customer=d.i_customer
                                  and a.i_customer='$icustomer'
                                  and (a.d_notapb >= to_date('$dfrom','dd-mm-yyyy')
                                  and a.d_notapb <= to_date('$dto','dd-mm-yyyy'))
                                  and (upper(a.i_notapb) like '%$cari%' or upper(a.i_spg) like '%$cari%'
                                      or upper(d.e_spg_name) like '%$cari%')
                                  order by a.d_notapb, a.i_notapb ",false);

			$config['base_url'] = base_url().'index.php/cekbonkons/cform/view/'.$dfrom.'/'.$dto.'/'.$icustomer.'/index/';
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8,0);
			$this->pagination->initialize($config);
			$data['create_links']	= $this->pagination->create_links();
			$this->load->model('cekbonkons/mmaster');
			$data['cari']	      = '';
			$data['dfrom']	    = $dfrom;
			$data['dto']	      = $dto;
			$data['icustomer']	= $icustomer;
			$data['isi']=$this->mmaster->cari($icustomer,$dfrom,$dto,$cari,$config['per_page'],$config['cur_page']);
			$data['page_title'] = $this->lang->line('cekbon');
			$data['inotapb']='';
	 		$this->load->view('cekbonkons/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('cekbon');
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
			  )
			{
#				$inotapb = $this->uri->segment(4);
				$inotapb= str_replace('%20',' ',$this->uri->segment(4));
				$icustomer= $this->uri->segment(5);
				$dfrom= $this->uri->segment(6);
				$dto= $this->uri->segment(7);
				$icustomer= $this->uri->segment(8);
				$query = $this->db->query("select * from tm_notapb_item where i_notapb = '$inotapb' and i_customer='$icustomer'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['inotapb'] = $inotapb;
				$data['dfrom'] = $dfrom;
				$data['dto'] = $dto;
				$data['icustomer'] = $icustomer;
				$this->load->model('cekbonkons/mmaster');
				$data['isi']=$this->mmaster->baca($inotapb,$icustomer);
				$data['detail']=$this->mmaster->bacadetail($inotapb,$icustomer);
		 		$this->load->view('cekbonkons/vmainform',$data);
			}else{
				$this->load->view('cekbonkons/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$inotapb 	  = $this->input->post('inotapb', TRUE);
			$icustomer	= $this->input->post('icustomer',TRUE);
			$ecek	      = $this->input->post('ecek',TRUE);
			if($ecek=='')
				$ecek=null;
			$user		=$this->session->userdata('user_id');
			$this->db->trans_begin();
			$this->load->model('cekbonkons/mmaster');
######
			$dnotapb 	= $this->input->post('dnotapb', TRUE);
			if($dnotapb!=''){
				$tmp=explode("-",$dnotapb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dnotapb=$th."-".$bl."-".$hr;
			}
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomername	= $this->input->post('ecustomername', TRUE);
			$eareaname		= $this->input->post('eareaname', TRUE);
			$ispg		= $this->input->post('ispg',TRUE);
			$espgname	= $this->input->post('espgname',TRUE);
			$fnotapbcancel		= 'f';
			$nnotapbdiscount	= $this->input->post('nnotapbdiscount',TRUE);
			$vnotapbdiscount	= $this->input->post('vnotapbdiscount',TRUE);
			$vnotapbgross	= $this->input->post('vnotapbgross',TRUE);
			$nnotapbdiscount	= str_replace(',','',$nnotapbdiscount);
			$vnotapbdiscount	= str_replace(',','',$vnotapbdiscount);
			$vnotapbgross	= str_replace(',','',$vnotapbgross);		
			$jml		= $this->input->post('jml', TRUE);
			if(($icustomer!='') && ($inotapb!=''))
			{
				$benar="false";
				$this->mmaster->updateheader($inotapb, $icustomer,$ecek,$user);
				if ( ($this->db->trans_status() === FALSE) )
				{
			    $this->db->trans_rollback();
				}else{
			    $this->db->trans_commit();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Cek Penjualan Konsinyasi Customer '.$icustomer.' No:'.$inotapb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );
					$data['sukses']			= true;
					$data['inomor']			= $inotapb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
######
	}

	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/cekbonkons/cform/customer/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name
                                    from tr_spg a, tr_customer b, tr_area c
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area",false);
			}else{
				$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name
                                    from tr_spg a, tr_customer b, tr_area c
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area
                                    and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
										                or a.i_area = '$area4' or a.i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('cekbonkons/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('cekbonkons/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu449')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/cekbonkons/cform/customer/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name
                                    from tr_spg a, tr_customer b, tr_area c
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area
                                    and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name
                                    from tr_spg a, tr_customer b, tr_area c
                                    where a.i_customer=b.i_customer and a.i_area=b.i_area and a.i_area=c.i_area and
                                    (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                                    and (a.i_area = '$area1' or a.i_area = '$area2' 
              										  or a.i_area = '$area3' or a.i_area = '$area4' or a.i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('cekbonkons/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('cekbonkons/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
