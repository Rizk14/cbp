<?php 
class Cform extends CI_Controller
{
	public $title  = "Biaya VS Omset";
	public $folder = "biomset";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->helper(array('file', 'directory', 'fusioncharts'));
		require_once("php/fungsi.php");
	}

	function index()
	{
		$data['page_title'] = $this->title;
		$data['periode1']	= date('Y-m');

		$this->load->view('biomset/vform', $data);
	}

	function view()
	{
		function noDash($param)
		{
			return str_replace('-', '', $param);
		}

		$iperiode1 = noDash($this->input->post('iperiode1'));
		$iperiode2 = ($this->input->post('iperiode2') != '') ? noDash($this->input->post('iperiode2')) : $iperiode1;

		$this->load->model('biomset/mmaster');

		$data = [
			'page_title' => $this->title,
			'iperiode1' => $iperiode1,
			'iperiode2' => $iperiode2,
			'isi'       => $this->mmaster->bacaperiode($iperiode1, $iperiode2)
		];

		$sess = $this->session->userdata('session_id');
		$id = $this->session->userdata('user_id');
		$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		$rs		= pg_query($sql);
		if (pg_num_rows($rs) > 0) {
			while ($row = pg_fetch_assoc($rs)) {
				$ip_address	  = $row['ip_address'];
				break;
			}
		} else {
			$ip_address = 'kosong';
		}
		$query 	= pg_query("SELECT current_timestamp as c");
		while ($row = pg_fetch_assoc($query)) {
			$now	  = $row['c'];
		}
		$pesan = 'Membuka Biaya VS Omset:' . $iperiode1 . "-" . $iperiode2;

		$this->load->model('logger');
		$this->logger->write($id, $ip_address, $now, $pesan);

		$this->load->view('biomset/vformview', $data);
	}

	function export()
	{
		$iperiode1 = $this->uri->segment(4);
		$iperiode2 = $this->uri->segment(5);

		$this->load->model('biomset/mmaster');

		$isi = $this->mmaster->bacaperiode($iperiode1, $iperiode2);

		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');

		$phpexcel = new PHPExcel();
		$phpexcel->setActiveSheetIndex(0);

		$activesheet = $phpexcel->getActiveSheet();

		$styleArray = array(
			'font' => array(
				'bold' => true,
				'size' => 11,
			)
		);

		$activesheet->getStyle('A1:G1')->applyFromArray($styleArray);

		$activesheet->getColumnDimension('A')->setWidth(14);
		$activesheet->getColumnDimension('B')->setWidth(30);
		$activesheet->getColumnDimension('C')->setWidth(18);
		$activesheet->getColumnDimension('D')->setWidth(18);
		$activesheet->getColumnDimension('E')->setWidth(18);
		$activesheet->getColumnDimension('F')->setWidth(18);
		$activesheet->getColumnDimension('G')->setWidth(18);

		$activesheet->setCellValue('A1', 'PERIODE');
		$activesheet->setCellValue('B1', 'AREA');
		$activesheet->setCellValue('C1', 'BIAYA KAS KECIL');
		$activesheet->setCellValue('D1', 'BIAYA KAS BESAR');
		$activesheet->setCellValue('E1', 'JUMLAH BIAYA');
		$activesheet->setCellValue('F1', 'OMSET');
		$activesheet->setCellValue('G1', 'PERSENTASE');

		$n = 1;

		$totalkk    = 0;
		$totalkb    = 0;
		$totalbiaya = 0;
		$totalnota  = 0;

		if ($isi != 'zero') {
			foreach ($isi as $row) {
				$n++;
				$activesheet->setCellValueExplicit('A' . $n, $row->i_periode);
				$activesheet->setCellValue('B' . $n, $row->i_area . ' - ' . $row->e_area_name);
				$activesheet->setCellValue('C' . $n, $row->v_biaya_kk);
				$activesheet->setCellValue('D' . $n, $row->v_biaya_kb);
				$activesheet->setCellValue('E' . $n, $row->v_biaya_total);
				$activesheet->setCellValue('F' . $n, $row->v_omset);
				$activesheet->setCellValue('G' . $n, $row->persen . ' %');

				$totalkk    += $row->v_biaya_kk;
				$totalkb    += $row->v_biaya_kb;
				$totalbiaya += $row->v_biaya_total;
				$totalnota  += $row->v_omset;
			}

			$totalpersen = $totalbiaya / (($totalnota == 0) ? 1 : $totalnota) * 100;

			/*********Style1*********/

			$styleArray_item = array(
				'font' => array(
					'size' => 10
				)
			);
			$activesheet->getStyle('A2:G' . $n)->applyFromArray($styleArray_item);

			/*********Style1*********/

			/*********Style2*********/

			$styleArray_item2 = array(
				'alignment' => array(
					'horizontal' => Style_Alignment::HORIZONTAL_RIGHT
				)

			);
			$activesheet->getStyle('C2:G' . $n)
				->applyFromArray($styleArray_item2)
				->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);

			/*********Style2*********/

			/*********Total*********/

			$m = $n + 1;
			$activesheet->mergeCells("A$m:B$m")->setCellValue("A$m", 'TOTAL');
			$activesheet->getStyle("A$m:B$m")->getAlignment()->setHorizontal(Style_Alignment::HORIZONTAL_RIGHT);

			$activesheet->setCellValue('C' . $m, $totalkk);
			$activesheet->setCellValue('D' . $m, $totalkb);
			$activesheet->setCellValue('E' . $m, $totalbiaya);
			$activesheet->setCellValue('F' . $m, $totalnota);
			$activesheet->setCellValue('G' . $m, number_format($totalpersen, 3) . ' %');

			$styleArray_total = array(
				'alignment' => array(
					'horizontal' => Style_Alignment::HORIZONTAL_RIGHT
				)

			);
			$activesheet->getStyle("C$m:G$m")
				->applyFromArray($styleArray_total)
				->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);

			/*********Total*********/
		}

		$name = 'Biaya VS Omset Periode ' . $iperiode1 . ' S-d ' . $iperiode2 . '.xlsx';

		// Proses file excel    
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename=' . $name . ''); // Set nama file excel nya    
		header('Cache-Control: max-age=0');

		// $ObjWriter = IOFactory::createWriter($phpexcel, 'Excel2007');
		$ObjWriter = IOFactory::createWriter($phpexcel, 'Excel5');

		// ob_start();

		$ObjWriter->save('php://output');

		// $xlsData = ob_get_contents();
		// ob_end_clean();

		// $response =  array(
		// 	'op'   => 'ok',
		// 	'name' => $name,
		// 	'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64," . base64_encode($xlsData)
		// );

		// die(json_encode($response));
	}

	function chartx()
	{
		$iperiode = $this->uri->segment(4);
		$data['charts'] = $this->getChart($iperiode);
		$this->load->view('biomset/charts', $data);
	}

	function getChart($iperiode)
	{
		$this->load->library('highcharts');
		$th = substr($iperiode, 0, 4);
		$bl = substr($iperiode, 4, 2);
		$bl = mbulan($bl);
		$this->highcharts->set_title('Target Penjualan Per area', 'Periode : ' . $bl . ' ' . $th);
		$this->highcharts->set_dimensions(1340, 500);
		$this->highcharts->set_type('column');
		$this->highcharts->set_axis_titles('Area', 'Nilai');
		$credits->href = base_url();
		$credits->text = NmPerusahaan;
		$this->highcharts->set_credits($credits);
		#    $this->highcharts->render_to("my_div");
		$this->load->model('biomset/mmaster');
		$result = $this->mmaster->bacatarget($iperiode);
		foreach ($result as $row) {
			$target[] = intval($row->v_target);
		}
		$result = $this->mmaster->bacaspb($iperiode);
		foreach ($result as $row) {
			$spb[] = intval($row->v_spb_gross);
		}
		$result = $this->mmaster->bacasj($iperiode);
		foreach ($result as $row) {
			$sj[] = intval($row->v_sj_gross);
		}
		$result = $this->mmaster->bacanota($iperiode);
		foreach ($result as $row) {
			$nota[] = intval($row->v_nota_gross);
		}
		$result = $this->mmaster->bacaarea($iperiode);
		foreach ($result as $row) {
			$area[] = $row->i_area;
		}
		$data['axis']['categories'] = $area;
		$data['targets']['data'] = $target;
		$data['targets']['name'] = 'Target';
		$data['spbs']['data'] = $spb;
		$data['spbs']['name'] = 'SPB';
		$data['sjs']['data'] = $sj;
		$data['sjs']['name'] = 'SJ';
		$data['notas']['data'] = $nota;
		$data['notas']['name'] = 'Nota';

		$this->highcharts->set_xAxis($data['axis']);
		$this->highcharts->set_serie($data['targets']);
		$this->highcharts->set_serie($data['spbs']);
		$this->highcharts->set_serie($data['sjs']);
		$this->highcharts->set_serie($data['notas']);
		return $this->highcharts->render();
	}
	function fcf()
	{
		$iperiode = $this->uri->segment(4);
		$tipe = $this->uri->segment(5);
		if ($tipe == '') {
			$graph_swfFile      = base_url() . 'flash/FCF_MSColumn3D.swf';
		} else {
			$tipe = str_replace("tandatitik", ".", $tipe);
			$graph_swfFile      = base_url() . 'flash/' . $tipe;
		}
		$th = substr($iperiode, 0, 4);
		$bl = substr($iperiode, 4, 2);
		$bl = mbulan($bl);
		$graph_caption      = 'Target Penjualan Per Area Periode : ' . $bl . ' ' . $th;
		$graph_numberPrefix = 'Rp.';
		$graph_title        = 'Penjualan Produk';
		$graph_width        = 954;
		$graph_height       = 500;
		$this->load->model('biomset/mmaster');

		// Area
		$i = 0;
		$result = $this->mmaster->bacaarea($iperiode);
		foreach ($result as $row) {
			$category[$i] = $row->i_area;
			$i++;
		}

		// data set
		$dataset[0] = 'Target';
		$dataset[1] = 'SPB';
		$dataset[2] = 'SJ';
		$dataset[3] = 'Nota';

		//data 1
		$i = 0;
		$result = $this->mmaster->bacatarget($iperiode);
		foreach ($result as $row) {
			$arrData['Target'][$i] = intval($row->v_target);
			$i++;
		}

		//data 2
		$i = 0;
		$result = $this->mmaster->bacaspb($iperiode);
		foreach ($result as $row) {
			$arrData['SPB'][$i] = intval($row->v_spb_gross);
			$i++;
		}

		//data 3
		$i = 0;
		$result = $this->mmaster->bacasj($iperiode);
		foreach ($result as $row) {
			$arrData['SJ'][$i] = intval($row->v_sj_gross);
			$i++;
		}


		//data 4
		$i = 0;
		$result = $this->mmaster->bacanota($iperiode);
		foreach ($result as $row) {
			$arrData['Nota'][$i] = intval($row->v_nota_gross);
			$i++;
		}

		$strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='100' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='0' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='" . $graph_caption . "' numberPrefix='" . $graph_numberPrefix . "' showValues='0'>";

		//Convert category to XML and append
		$strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>";
		foreach ($category as $c) {
			$strXML .= "<category name='" . $c . "'/>";
		}
		$strXML .= "</categories>";

		//Convert dataset and data to XML and append
		foreach ($dataset as $set) {
			$strXML .= "<dataset seriesname='" . $set . "' color='" .  getFCColor() . "'>";
			foreach ($arrData[$set] as $d) {
				$strXML .= "<set value='" . $d . "'/>";
			}
			$strXML .= "</dataset>";
		}

		//Close <chart> element
		$strXML .= "</graph>";

		$data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div", $graph_width, $graph_height);
		$data['iperiode'] = $iperiode;
		$data['modul'] = 'salesperformance_new';
		$data['isi'] = directory_map('./flash/');
		$data['file'] = '';

		$this->load->view('biomset/chart_view', $data);
	}
}
