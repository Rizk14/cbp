<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title']	= $this->lang->line('bbmap');
			$data['iap']		= '';
			$data['isi']		= "";
			$data['detail']		= "";
			$data['jmlitem']	= "";
			$this->load->view('bbmap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan(){
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iap 	= $this->input->post('iap', TRUE);
			$iapold	= $this->input->post('iapold', TRUE);
			$dap 	= $this->input->post('dap', TRUE);
			if($dap!=''){
				$tmp=explode("-",$dap);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dap=$th."-".$bl."-".$hr;
        $thbl=substr($th,2,2).$bl;
			}
			$isupplier		= $this->input->post('isupplier', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$iop			= $this->input->post('iop', TRUE);
			$vapgross		= $this->input->post('vapgross',TRUE);
			$vapgross		= str_replace(',','',$vapgross);
			$jml			= $this->input->post('jml', TRUE);
			if(
				($iap=='')
				&& ($isupplier!='')
				&& ($vapgross!='') && ($vapgross!='0')
				&& ($iop!='')
				&& ($dap!='')
			  )
			{
				$this->db->trans_begin();
				$this->load->model('bbmap/mmaster');
				$iap=$this->mmaster->runningnumber($thbl);
				$query=$this->db->query("select * from tm_ap where i_ap='$iap' and i_supplier='$isupplier'");
				if($query->num_rows()==0){
/*===================================================================================================================================================*/
					$ibbm				  = $this->mmaster->runningnumberbbm($thbl);
					$dbbm				  = $dap;
					$istore				= 'AA';
					$istorelocation		= '01';
					$istorelocationbin= '00';
					$eremark			= 'BBM AP';
					$ibbktype			= '01';
					$ibbmtype			= '04';
/*===================================================================================================================================================*/
					$this->mmaster->insertheader($iap,$isupplier,$iop,$iarea,$dap,$vapgross,$iapold);
/*===================================================================================================================================================*/
					$this->mmaster->insertbbmheader($iap,$dap,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea);
/*===================================================================================================================================================*/
					for($i=1;$i<=$jml;$i++){
					  $iproduct					= $this->input->post('iproduct'.$i, TRUE);
						$iproductgrade			= 'A';
						$iproductmotif			= $this->input->post('motif'.$i, TRUE);
						$eproductname				= $this->input->post('eproductname'.$i, TRUE);
						$vproductmill				= $this->input->post('vproductmill'.$i, TRUE);
						$vproductmill				= str_replace(',','',$vproductmill);
						$nreceive				  	= $this->input->post('nreceive'.$i, TRUE);
						$nitemno				  	= $this->input->post('nitemno'.$i, TRUE);
						$this->mmaster->insertdetail($iap,$isupplier,$iproduct,$iproductgrade,$iproductmotif,$eproductname,$nreceive,$vproductmill,$dap,$iop,$nitemno);
############
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$iap,$q_in,$q_out,$nreceive,$q_aw,$q_ak);
            $th=substr($dap,0,4);
            $bl=substr($dap,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nreceive,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nreceive,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nreceive,$q_ak);
            }else{
              $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nreceive);
            }
############
				    $this->mmaster->insertbbmdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nreceive,
														$vproductmill,$iap,$ibbm,$eremark,$dap);
					}
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();
#$this->db->trans_rollback();
		        $sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
			        while($row=pg_fetch_assoc($rs)){
				        $ip_address	  = $row['ip_address'];
				        break;
			        }
		        }else{
			        $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
			        $now	  = $row['c'];
		        }
		        $pesan='Input BBMAP No:'.$iap.' Area:'.$iarea;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now , $pesan );

						$data['sukses']			= true;
						$data['inomor']			= $iap;
						$this->load->view('nomor',$data);
					}
				}else{
					
				}
			}else{
				$this->load->view('awal/index.php');
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('bbmap');
			$this->load->view('bbmap/vinsert_fail',$data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('bbmap')." update";
			if(
				($this->uri->segment(5)) && ($this->uri->segment(6))
			  ){
				$iap				= $this->uri->segment(5);
				$isupplier	= $this->uri->segment(6);
				$dfrom 			= $this->uri->segment(7);
				$dto 	 			= $this->uri->segment(8);
				$query 				= $this->db->query("select * from tm_ap_item where i_ap = '$iap' ");//and i_supplier='$isupplier'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['iap'] 			= $iap;
				$data['isupplier']= $isupplier;
				$data['dfrom']   	= $dfrom;
				$data['dto']	   	= $dto;
				$this->load->model('bbmap/mmaster');
				$data['isi']		= $this->mmaster->baca($iap,$isupplier);
				$data['detail']		= $this->mmaster->bacadetail($iap, $isupplier);
		 		$this->load->view('bbmap/vmainform',$data);
			}else{
				$this->load->view('bbmap/vinsert_fail',$data);
			}
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iap 	= $this->input->post('iap', TRUE);
			$iapold	= $this->input->post('iapold', TRUE);
			$dap 	= $this->input->post('dap', TRUE);
			if($dap!=''){
				$tmp=explode("-",$dap);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dap=$th."-".$bl."-".$hr;
				$tahun=$th;
			}
			$isupplier		= $this->input->post('isupplier', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$iop			= $this->input->post('iop', TRUE);
			$vapgross		= $this->input->post('vapgross',TRUE);
			$vapgross		= str_replace(',','',$vapgross);
			$jml			= $this->input->post('jml', TRUE);
			if(
				($iap!='')
				&& ($isupplier!='')
				&& (($vapgross!='') || ($vapgross!='0'))
				&& ($iop!='')
				&& ($dap!='')
			  )
			{
				$this->db->trans_begin();
				$this->load->model('bbmap/mmaster');
				$istore				= 'AA';
				$istorelocation		= '01';
				$istorelocationbin	= '00';
				$eremark			= 'BBM AP';
				$ibbmtype			= '04';
				$query 				= $this->db->query("select i_bbm from tm_bbm where i_refference_document = '$iap' and i_bbm_type='04' ");
				foreach($query->result() as $t){
					$ibbm			= $t->i_bbm;
				}
				$dbbm				= $dap;
				$this->mmaster->updateheader($iap,$isupplier,$iop,$iarea,$dap,$vapgross,$iapold);
/*===================================================================================================================================================*/
				$this->mmaster->updatebbmheader($iap,$dap,$ibbm,$dbbm,$ibbmtype,$eremark,$iarea);
/*===================================================================================================================================================*/
				for($i=1;$i<=$jml;$i++){
					$iproduct					= $this->input->post('iproduct'.$i, TRUE);
					$iproductgrade		= 'A';
					$iproductmotif		= $this->input->post('motif'.$i, TRUE);
					$eproductname			= $this->input->post('eproductname'.$i, TRUE);
					$vproductmill			= $this->input->post('vproductmill'.$i, TRUE);
					$vproductmill			= str_replace(',','',$vproductmill);
					$nreceive					= $this->input->post('nreceive'.$i, TRUE);
					$nreceive         = str_replace(',','',$nreceive);
          $ntmp		          = $this->input->post('ntmp'.$i, TRUE);
					$ntmp		          = str_replace(',','',$ntmp);
					$this->mmaster->deletedetail($iproduct, $iproductgrade, $iap, $isupplier, $iproductmotif, $tahun);

					$query=$this->db->query(" select i_area, i_reff from tm_op where i_area='$iarea' and i_op='$iop' ",false);
					if ($query->num_rows() > 0){
						foreach($query->result() as $row){
							if(substr($row->i_reff,0,3)=='SPB'){
								$ispb=$row->i_reff;
								$this->mmaster->updatespbx($iproduct,$iproductgrade,$iproductmotif,$ntmp,$ispb,$iarea);
							}else if(substr($row->i_reff,0,4)=='SPMB'){
								$ispmb=$row->i_reff;
								$this->mmaster->updatespmbx($iproduct,$iproductgrade,$iproductmotif,$ntmp,$ispmb,$iarea);
							}
						}
					}		
          $th=substr($dap,0,4);
				  $bl=substr($dap,5,2);
				  $emutasiperiode=$th.$bl;
				  $tra=$this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$iap,$ntmp,$eproductname);
          if( ($ntmp!='') && ($ntmp!=0) ){
			      $this->mmaster->updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ntmp,$emutasiperiode);
			      $this->mmaster->updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ntmp);
          }

					$this->mmaster->insertdetail($iap,$isupplier,$iproduct,$iproductgrade,$iproductmotif,$eproductname,
												 $nreceive,$vproductmill,$dap,$iop,$i);
##########
          $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
          if(isset($trans)){
            foreach($trans as $itrans)
            {
              $q_aw =$itrans->n_quantity_awal;
              $q_ak =$itrans->n_quantity_akhir;
              $q_in =$itrans->n_quantity_in;
              $q_out=$itrans->n_quantity_out;
              break;
            }
          }else{
            $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){

              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_stock;
                $q_ak =$itrans->n_quantity_stock;
                $q_in =0;
                $q_out=0;
                break;
              }
            }else{
              $q_aw=0;
              $q_ak=0;
              $q_in=0;
              $q_out=0;
            }
          }
          $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$iap,$q_in,$q_out,$nreceive,$q_aw,$q_ak);
          $th=substr($dap,0,4);
          $bl=substr($dap,5,2);
          $emutasiperiode=$th.$bl;
          if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
          {
            $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nreceive,$emutasiperiode);
          }else{
            $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nreceive,$emutasiperiode);
          }
          if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
          {
            $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nreceive,$q_ak);
          }else{
            $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nreceive);
          }
##########

					$this->mmaster->insertbbmdetail($iproduct,$iproductgrade,$eproductname,$iproductmotif,$nreceive,
													$vproductmill,$iap,$ibbm,$eremark,$dap);
					$query=$this->db->query(" 	select i_area, i_reff from tm_op
												where i_area='$iarea' and i_op='$iop' ",false);
					if ($query->num_rows() > 0){
						foreach($query->result() as $row){
							if(substr($row->i_reff,0,3)=='SPB'){
								$ispb=$row->i_reff;
								$this->mmaster->updatespb($iproduct,$iproductgrade,$iproductmotif,$nreceive,$ispb,$iarea);
							}else if(substr($row->i_reff,0,4)=='SPMB'){
								$ispmb=$row->i_reff;
								$this->mmaster->updatespmb($iproduct,$iproductgrade,$iproductmotif,$nreceive,$ispmb,$iarea);
							}
						}
					}		
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
			    $this->db->trans_rollback();
				}else{
			    $this->db->trans_commit();

	        $sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
		        while($row=pg_fetch_assoc($rs)){
			        $ip_address	  = $row['ip_address'];
			        break;
		        }
	        }else{
		        $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
		        $now	  = $row['c'];
	        }
	        $pesan='Update BBMAP No:'.$iap.' Area:'.$iarea;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $iap;
					$this->load->view('nomor',$data);
				}
			}
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iap		= $this->input->post('iapdelete', TRUE);
			$isupplier	= $this->input->post('isupplierdelete', TRUE);
			$iop		= $this->input->post('iopdelete', TRUE);
			$this->load->model('bbmap/mmaster');
			$this->mmaster->delete($iap,$isupplier,$iop);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
        while($row=pg_fetch_assoc($rs)){
	        $ip_address	  = $row['ip_address'];
	        break;
        }
      }else{
        $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
        $now	  = $row['c'];
      }
      $pesan='Delete BBMAP No:'.$iap;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title']	= $this->lang->line('bbmap');
			$data['iap']		= '';
			$data['isupplier']	= '';
			$data['jmlitem']	= '';
			$data['detail']		= '';
			$data['isi']		= '';//$this->mmaster->bacasemua();
			$this->load->view('bbmap/vmainform', $data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iap			= $this->uri->segment(4);
			$isupplier		= $this->uri->segment(5);
			$iproduct		= $this->uri->segment(6);
 			$iproductgrade	= $this->uri->segment(7);
			$iproductmotif	= $this->uri->segment(8);
			$vapgross		= $this->uri->segment(9);
			$iop			= $this->uri->segment(10);
			$iarea			= $this->uri->segment(11);
			$th				= $this->uri->segment(12);
			$dap			= $this->uri->segment(13);
			$this->db->trans_begin();
			$this->load->model('bbmap/mmaster');
			$this->mmaster->uphead($iap,$isupplier,$iop,$iarea,$dap,$vapgross);
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $iap, $isupplier, $iproductmotif, $th);
			if ($this->db->trans_status() === FALSE)
			{
		    $this->db->trans_rollback();
			}else{
		    $this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
	        while($row=pg_fetch_assoc($rs)){
		        $ip_address	  = $row['ip_address'];
		        break;
	        }
        }else{
	        $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
	        $now	  = $row['c'];
        }
        $pesan='Delete Item BBMAP No:'.$iap.' Area:'.$iarea;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

				$data['page_title'] = $this->lang->line('bbmap')." Update";
				$query 				= $this->db->query("select * from tm_ap_item where i_ap = '$iap' ");//and i_supplier='$isupplier'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['iap'] 		= $iap;
				$data['isupplier']	= $isupplier;
				$this->load->model('bbmap/mmaster');
				$data['isi']		= $this->mmaster->baca($iap,$isupplier);
				$data['detail']		= $this->mmaster->bacadetail($iap, $isupplier);
		 		$this->load->view('bbmap/vmainform',$data);
			}
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']		= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$op					= $this->uri->segment(5);
			$data['op']			= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/bbmap/cform/product/'.$baris.'/'.$op.'/index/';
			$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif, a.e_product_motifname as namamotif, 
									c.e_product_name as nama, b.v_product_mill as harga 
									from tr_product_motif a, tm_op_item b, tr_product c 
									where a.i_product=c.i_product 
									and b.i_op='$op' and b.i_product=a.i_product 
									and b.i_product_motif=a.i_product_motif" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('bbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($op,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$this->load->view('bbmap/vlistproduct', $data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']		= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bbmap/cform/product/'.$baris.'/index/';
			$cari 				= $this->input->post('cari', FALSE);
			$cari				= strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_mill as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
									   	and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
									  ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('bbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('bbmap/vlistproduct', $data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']		= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$op					= $this->uri->segment(5);
			$data['op']			= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/bbmap/cform/product/'.$baris.'/'.$op.'/index/';
			$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif, a.e_product_motifname as namamotif, 
									c.e_product_name as nama, c.v_product_mill as harga 
									from tr_product_motif a, tm_op_item b, tr_product c 
									where a.i_product=c.i_product 
									and b.i_op='$op' and b.i_product=a.i_product 
									and b.i_product_motif=a.i_product_motif" ,false);			
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('bbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']		=$this->mmaster->bacaproduct($op,$config['per_page'],$this->uri->segment(7));
			$this->load->view('bbmap/vlistproductupdate', $data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function cariproductupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris				= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bbmap/cform/productupdate/index/';
			$cari 				= $this->input->post('cari', FALSE);
			$cari				= strtoupper($cari);
			$query 	= $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_mill as harga
										from tr_product_motif a, tr_product c
										where a.i_product=c.i_product
										and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')"
										,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']		=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(5));
			$data['baris']		=$baris;
			$this->load->view('bbmap/vlistproductupdate', $data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/bbmap/cform/supplier/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_supplier where i_supplier_group='G0000' ",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']		=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('bbmap/vlistsupplier', $data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}

	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/bbmap/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_supplier where (upper(i_supplier) like '%$cari%' 
						      	  		or upper(e_supplier_name) like '%$cari%') and i_supplier_group='G0000' ",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']		=$this->mmaster->carisupplier($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('bbmap/vlistsupplier', $data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/bbmap/cform/index/';
			$query=$this->db->query("select * from tm_ap",false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(4);
			$this->pagination->initialize($config);
			$cari 				= $this->input->post('cari', FALSE);
			$cari				= strtoupper($cari);
			$this->load->model('bbmap/mmaster');
			$data['isi']		= $this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title']	= $this->lang->line('bbmap');
			$data['iap']		= '';
			$data['isupplier']	= '';
			$data['jmlitem']	= '';
			$data['detail']		= '';
	 		$this->load->view('bbmap/vmainform',$data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function op()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/bbmap/cform/op/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select a.*, 'Lain-lain' as e_supplier_name, c.e_area_name 
							from tm_op a, tr_area c, tr_supplier b 
							where a.i_area=c.i_area and b.i_supplier_group='G0000'
							and a.f_op_cancel='f' and a.i_supplier=b.i_supplier
							and a.f_op_close='f'",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbmap/mmaster');
			$data['page_title'] = $this->lang->line('list_op');
			$data['isi']		=$this->mmaster->bacaop($config['per_page'],$this->uri->segment(5));
			$this->load->view('bbmap/vlistop', $data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
	function cariop()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu77')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/bbmap/cform/op/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari =strtoupper($cari);
			$query = $this->db->query("	select a.*, 'Lain-lain' as e_supplier_name, c.e_area_name 
							from tm_op a, tr_area c, tr_supplier b 
							where a.i_area=c.i_area and b.i_supplier_group='G0000' 
							and (upper(a.i_op) like '%$cari%' 
							or upper(a.i_supplier) like '%$cari%'
							or upper(b.e_supplier_name) like '%$cari%'
							or upper(a.i_reff) like '%$cari%') 
							and a.f_op_cancel='f' and a.i_supplier=b.i_supplier
							and a.f_op_close='f'",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbmap/mmaster');
			$data['page_title']	= $this->lang->line('list_op');
			$data['isi']		= $this->mmaster->cariop($cari, $config['per_page'],$this->uri->segment(5));
//			$data['iarea']		= $iarea;
			$this->load->view('bbmap/vlistop', $data);
		}else{
//			$this->load->view('awal/index.php');
			$this->load->view('awal/index.php');
		}
	}
}
?>
