<?php
class Cform extends CI_Controller
{
   public $title  = "Opname KN/DN";
   public $folder = "exp-opnkn";

   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->model($this->folder . '/mmaster');
      require_once("php/fungsi.php");
   }

   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu343') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data['page_title']  = $this->title;
         $data['folder']      = $this->folder;
         $data['dateto']      = '';

         $this->logger->writenew("Membuka Menu " . $this->title);

         $this->load->view($this->folder . '/vform', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu343') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {

         $iarea      = $this->uri->segment('4') == "NA" ? "00" : $this->uri->segment('4');
         $dateto     = $this->uri->segment('5');
         $datetox    = $dateto;

         if ($iarea != "00") {
            $que        = $this->db->query(" select a.*, c.e_customer_name, d.e_salesman_name
                                             from tm_kn a
                                             inner join tr_customer c on (a.i_customer=c.i_customer and a.i_area=c.i_area)
                                             inner join tr_salesman d on (a.i_salesman=d.i_salesman)
                                             where a.i_area='$iarea' and a.f_kn_cancel='f' and
                                             a.d_kn <= to_date('$datetox','dd-mm-yyyy')
                                             and a.v_sisa <> 0
                                             order by a.d_kn, a.i_kn");
         } else {
            $que        = $this->db->query(" select a.*, c.e_customer_name, d.e_salesman_name
                                             from tm_kn a
                                             inner join tr_customer c on (a.i_customer=c.i_customer and a.i_area=c.i_area)
                                             inner join tr_salesman d on (a.i_salesman=d.i_salesman)
                                             where a.f_kn_cancel='f' and
                                             a.d_kn <= to_date('$datetox','dd-mm-yyyy')
                                             and a.v_sisa <> 0
                                             order by a.d_kn, a.i_kn");
         }

         $fromname = '';

         if ($dateto != '') {
            $tmp = explode("-", $dateto);
            $th = $tmp[2];
            $bl = $tmp[1];
            $hr = $tmp[0];
            $fromname = $fromname . $hr;
            $dateto = $th . "-" . $bl . "-" . $hr;
            $periodeawal   = $hr . " " . mbulan($bl) . " " . $th;
         }

         $tmp           = explode("-", $dateto);
         $det           = $tmp[0];
         $mon           = $tmp[1];
         $yir           = $tmp[2];
         $dtos          = $yir . "/" . $mon . "/" . $det;
         $fromname      = $fromname . $det;
         $periodeakhir  = $det . " " . mbulan($mon) . " " . $yir;

         $dtos          = $this->mmaster->dateAdd("d", 1, $dtos);
         $tmp           = explode("-", $dtos);
         $det1          = $tmp[2];
         $mon1          = $tmp[1];
         $yir1          = $tmp[0];
         $dtos          = $yir1 . "-" . $mon1 . "-" . $det1;

         $qareaname   = $this->mmaster->eareaname($iarea);
         if ($qareaname->num_rows() > 0) {
            $row_areaname   = $qareaname->row();
            $aname   = $row_areaname->e_area_name;
         } else {
            $aname   = '';
         }

         $this->load->library('PHPExcel');
         $this->load->library('PHPExcel/IOFactory');

         $objPHPExcel = new PHPExcel();
         $objPHPExcel->getProperties()->setTitle("Daftar Opname Kredit Nota/Debet Nota")
            ->setDescription(NmPerusahaan);
         $objPHPExcel->setActiveSheetIndex(0);

         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            array(
               'font'   => array(
                  'name'   => 'Arial',
                  'bold'   => true,
                  'italic' => false,
                  'size'  => 12
               ),
               'alignment' => array(
                  'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
                  'vertical'  => Style_Alignment::VERTICAL_CENTER,
                  'wrap'      => true
               )
            ),
            'A1:A4'
         );

         $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
         $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
         $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(17);
         $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
         $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
         $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
         $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);
         $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);

         $objPHPExcel->getActiveSheet()->setCellValue('A1', NmPerusahaan);
         $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 1, 12, 1);
         $objPHPExcel->getActiveSheet()->setCellValue('A2', 'DAFTAR OPNAME KN/DN - ' . $aname);
         $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 12, 2);
         $objPHPExcel->getActiveSheet()->setCellValue('A3', 's/d Tanggal : ' . $periodeawal);
         $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 12, 3);
         $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');

         $objPHPExcel->getActiveSheet()->getStyle('A5:A5')->applyFromArray(
            array(
               'borders'   => array(
                  'top'       => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('B5', 'No KN/DN');
         $objPHPExcel->getActiveSheet()->getStyle('B5:B5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tgl KN/DN');
         $objPHPExcel->getActiveSheet()->getStyle('C5:C5')->applyFromArray(


            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Jenis KN');
         $objPHPExcel->getActiveSheet()->getStyle('D5:D5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)



               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Batal');
         $objPHPExcel->getActiveSheet()->getStyle('E5:E5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('F5', 'No Referensi');
         $objPHPExcel->getActiveSheet()->getStyle('F5:F5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Kode Lang');
         $objPHPExcel->getActiveSheet()->getStyle('G5:G5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Nama Lang');
         $objPHPExcel->getActiveSheet()->getStyle('H5:H5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Salesman');
         $objPHPExcel->getActiveSheet()->getStyle('I5:I5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Nilai Bersih');
         $objPHPExcel->getActiveSheet()->getStyle('J5:J5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Discount');
         $objPHPExcel->getActiveSheet()->getStyle('K5:K5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Nilai Kotor');
         $objPHPExcel->getActiveSheet()->getStyle('L5:L5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );
         $objPHPExcel->getActiveSheet()->setCellValue('M5', 'Sisa');
         $objPHPExcel->getActiveSheet()->getStyle('M5:M5')->applyFromArray(
            array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN),
                  'bottom' => array('style' => Style_Border::BORDER_THIN),
                  'left'  => array('style' => Style_Border::BORDER_THIN),
                  'right' => array('style' => Style_Border::BORDER_THIN)
               ),
            )
         );

         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
            array(
               'font' => array(
                  'name'   => 'Arial',
                  'bold'  => false,
                  'italic' => false,
                  'size'  => 12
               ),
               'alignment' => array(
                  'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
                  'vertical'  => Style_Alignment::VERTICAL_CENTER,
                  'wrap'      => true
               )
            ),
            'A5:M5'
         );

         if ($iarea != '00') {
            $this->db->select("  a.*, c.e_customer_name, d.e_salesman_name
                              from tm_kn a
                              inner join tr_customer c on (a.i_customer=c.i_customer and a.i_area=c.i_area)
                              inner join tr_salesman d on (a.i_salesman=d.i_salesman)
                              where a.i_area='$iarea' and a.f_kn_cancel='f' and
                              a.d_kn <= to_date('$datetox','dd-mm-yyyy')
                              and a.v_sisa<>0
                              order by a.d_kn, a.i_kn", false);
         } else {
            $this->db->select("  a.*, c.e_customer_name, d.e_salesman_name
                              from tm_kn a
                              inner join tr_customer c on (a.i_customer=c.i_customer and a.i_area=c.i_area)
                              inner join tr_salesman d on (a.i_salesman=d.i_salesman)
                              where a.f_kn_cancel='f' and
                              a.d_kn <= to_date('$datetox','dd-mm-yyyy')
                              and a.v_sisa<>0
                              order by a.d_kn, a.i_kn", false);
         }

         $query = $this->db->get();

         if ($query->num_rows() > 0) {
            $i = 5;
            $j = 5;

            $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'   => array('style' => Style_Border::BORDER_THIN),
                     'right'  => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'   => array('style' => Style_Border::BORDER_THIN),
                     'right'  => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );
            $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom' => array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  )
               )
            );

            foreach ($query->result() as $row) {
               if ($row->i_kn_type != '') {
                  if ($row->i_kn_type == '01') {
                     $kn = 'Retur';
                  } elseif ($row->i_kn_type == '02') {
                     $kn = 'Non Retur';
                  } elseif ($row->i_kn_type == '03') {
                     $kn = 'Debet';
                  }
               } else {
                  $kn = '';
               }

               $fcancel = $row->f_kn_cancel == "f" ? "Tidak" : "Ya";

               $i++;
               $j++;

               $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $j - 5);
               $objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     ),
                     'alignment' => array(
                        'horizontal' => Style_Alignment::HORIZONTAL_RIGHT
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->i_kn);
               $objPHPExcel->getActiveSheet()->getStyle('B' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               if ($row->d_kn) {
                  $tmp = explode('-', $row->d_kn);
                  $tgl = $tmp[2];
                  $bln = $tmp[1];
                  $thn = $tmp[0];
                  $row->d_kn = $tgl . '-' . $bln . '-' . $thn;
               }
               $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->d_kn);
               $objPHPExcel->getActiveSheet()->getStyle('C' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $kn);
               $objPHPExcel->getActiveSheet()->getStyle('D' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $fcancel);
               $objPHPExcel->getActiveSheet()->getStyle('E' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->i_refference, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->getStyle('F' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->i_customer, Cell_DataType::TYPE_STRING);
               $objPHPExcel->getActiveSheet()->getStyle('G' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->e_customer_name);
               $objPHPExcel->getActiveSheet()->getStyle('H' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),

                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->e_salesman_name);
               $objPHPExcel->getActiveSheet()->getStyle('I' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->v_netto);
               $objPHPExcel->getActiveSheet()->getStyle('J' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $row->v_discount);
               $objPHPExcel->getActiveSheet()->getStyle('K' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row->v_gross);
               $objPHPExcel->getActiveSheet()->getStyle('L' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $row->v_sisa);
               $objPHPExcel->getActiveSheet()->getStyle('M' . $i)->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
            }
            $x = $i - 1;
            $objPHPExcel->getActiveSheet()->getStyle('J6:M' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
         }

         $objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
         $nama       = 'opnkndn-' . $iarea . '-' . $fromname . '.xls';
         $area       = $iarea;
         if (file_exists('excel/' . $area . '/' . $nama)) {
            @chmod('excel/' . $area . '/' . $nama, 0777);
            @unlink('excel/' . $area . '/' . $nama);
         }
         $objWriter->save('excel/' . $area . '/' . $nama);
         @chmod('excel/' . $area . '/' . $nama, 0777);

         $this->logger->writenew('Export Opname KN/DN sampai tanggal : ' . $dateto . ' Area:' . $iarea);

         // Proses file excel    
         header('Content-Type: application/vnd.ms-excel');
         header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
         header('Cache-Control: max-age=0');

         $objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
         $objWriter->save('php://output', 'w');
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu343') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/exp-opnkn/cform/area/index/';
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         if ($area1 == '00') {
            $query = $this->db->query("select * from tr_area", false);
         } else {
            $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                                 or i_area = '$area4' or i_area = '$area5'", false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);


         $data['page_title'] = $this->lang->line('list_area');
         $data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
         $this->load->view($this->folder . '/vlistarea', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu343') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $area1   = $this->session->userdata('i_area1');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         $config['base_url'] = base_url() . 'index.php/exp-opnkn/cform/area/index/';
         $cari    = strtoupper($this->input->post('cari', FALSE));
         if ($area1 == '00') {
            $query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')", false);
         } else {
            $query    = $this->db->query("select * from tr_area
                                          where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
                                          and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                                          or i_area = '$area4' or i_area = '$area5')", false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $data['page_title'] = $this->lang->line('list_area');
         $data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
         $this->load->view($this->folder . '/vlistarea', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }
}
