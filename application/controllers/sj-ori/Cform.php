<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu86')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea1	= $this->session->userdata('i_area');
			$isjold	= $this->input->post('isjold', TRUE);
			$dsj 	= $this->input->post('dsj', TRUE);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
#   			$thbl=substr($th,2,2).$bl;
   			$thbl=$th.$bl;
			}
			$iarea	= $this->input->post('iarea', TRUE);
			$ispb	= $this->input->post('ispb', TRUE);
			$dspb	= $this->input->post('dspb', TRUE);
			if($dspb!=''){
				$tmp=explode("-",$dspb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspb=$th."-".$bl."-".$hr;
			}
			$eareaname	= $this->input->post('eareaname', TRUE);
			##########################################
			$isalesman	= $this->input->post('isalesman',TRUE);
			$icustomer	= $this->input->post('icustomer',TRUE);
			$nsjdiscount1	= $this->input->post('nsjdiscount1',TRUE);
			$nsjdiscount1	= str_replace(',','',$nsjdiscount1);
			$nsjdiscount2	= $this->input->post('nsjdiscount2',TRUE);
			$nsjdiscount2	= str_replace(',','',$nsjdiscount2);
			$nsjdiscount3	= $this->input->post('nsjdiscount3',TRUE);
			$nsjdiscount3	= str_replace(',','',$nsjdiscount3);
			$vsjdiscount1	= $this->input->post('vsjdiscount1',TRUE);
			$vsjdiscount1	= str_replace(',','',$vsjdiscount1);
			$vsjdiscount2	= $this->input->post('vsjdiscount2',TRUE);
			$vsjdiscount2	= str_replace(',','',$vsjdiscount2);
			$vsjdiscount3	= $this->input->post('vsjdiscount3',TRUE);
			$vsjdiscount3	= str_replace(',','',$vsjdiscount3);
			$vsjdiscounttotal	= $this->input->post('vsjdiscounttotal',TRUE);
			$vsjdiscounttotal	= str_replace(',','',$vsjdiscounttotal);
			$vsjgross	= $this->input->post('vsjgross',TRUE);
			$vsjgross	= str_replace(',','',$vsjgross);
			$vsjnetto	= $this->input->post('vsjnetto',TRUE);
			$vsjnetto	= str_replace(',','',$vsjnetto);
			$ntop	= $this->input->post('ntop',TRUE);
			##########################################
			$jml	  = $this->input->post('jml', TRUE);
			if($dsj!='' && $eareaname!='')
			{
				$gaono=true;
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					if($cek=='on'){
						$gaono=false;
					}
					if(!$gaono) break;
				}
				if(!$gaono){
					$this->db->trans_begin();
					$this->load->model('sj/mmaster');
					$istore	= $this->input->post('istore', TRUE);
          $kons		= $this->mmaster->cekkons($ispb,$iarea);
					if($istore=='AA'){
						$istorelocation		= '01';
					}else{
            if($kons=='t')
              $istorelocation		= 'PB';
            else
      				$istorelocation		= '00';
					}
					$istorelocationbin	= '00';
					$eremark		= 'SPB';
					$isjtype		= '04';
					$typearea		= $this->mmaster->cekdaerah($ispb,$iarea);
					
					if($typearea=='t'){
						$areasj=$iarea;
					}else{
						$areasj='00';
					}

					if($iarea1=='00' && $iarea1!=$iarea){
						$fentpusat = 't';
						$iareareff = $iarea;
						$areanumsj = $iarea;
					}elseif($iarea1!='00' && $iarea1==$iarea){
						$fentpusat = 'f';
						$iareareff = $iarea1;
						$areanumsj = $iarea1;
					}else{
						$fentpusat = 'f';
						$iareareff = $iarea1;
						$areanumsj = $iarea1;
					}

					$isj	 		= $this->mmaster->runningnumbersj($areasj,$thbl,$kons);

					$this->mmaster->insertsjheader($ispb,$dspb,$isj,$dsj,$iarea,$isalesman,$icustomer,
									$nsjdiscount1,$nsjdiscount2,$nsjdiscount3,$vsjdiscount1, 
									$vsjdiscount2,$vsjdiscount3,$vsjdiscounttotal,$vsjgross,$vsjnetto,$isjold,$fentpusat,$iareareff,$ntop);
					
					$this->mmaster->updatespb($ispb,$iarea,$isj,$dsj);
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
						if($cek=='on'){
						  $iproduct		  = $this->input->post('iproduct'.$i, TRUE);
						  $iproductgrade= 'A';
						  $iproductmotif= $this->input->post('motif'.$i, TRUE);
						  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
						  $vunitprice		= str_replace(',','',$vunitprice);
						  $ndeliver	  	= $this->input->post('ndeliver'.$i, TRUE);
						  $ndeliver		  = str_replace(',','',$ndeliver);
						  $norder   		= $this->input->post('norder'.$i, TRUE);
						  $norder		    = str_replace(',','',$norder);
						  if($ndeliver>0){
							  $this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                                    			     $vunitprice,$isj,$iarea,$i);
							  $this->mmaster->updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$iarea);
	###
                $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                if(isset($trans)){
                  foreach($trans as $itrans)
                  {
                    $q_aw =$itrans->n_quantity_awal;
                    $q_ak =$itrans->n_quantity_akhir;
                    $q_in =$itrans->n_quantity_in;
                    $q_out=$itrans->n_quantity_out;
                    break;
                  }
                }else{
                  $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                  if(isset($trans)){
                    foreach($trans as $itrans)
                    {
                      $q_aw =$itrans->n_quantity_stock;
                      $q_ak =$itrans->n_quantity_stock;
                      $q_in =0;
                      $q_out=0;
                      break;
                    }
                  }else{
                    $q_aw=0;
                    $q_ak=0;
                    $q_in=0;
                    $q_out=0;
                  }
                }
                $this->mmaster->inserttrans04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$ndeliver,$q_aw,$q_ak);
                $th=substr($dsj,0,4);
                $bl=substr($dsj,5,2);
                $emutasiperiode=$th.$bl;
		
                if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
                {
                  $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
                }else{
                  $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode,$q_aw);
                }
                if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
                {
                  $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$q_ak);
                }else{
                  $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ndeliver,$q_aw);
                }
  ###              
						  }else{
                $this->mmaster->updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$iarea);
              }
						}else{
						  $iproduct		= $this->input->post('iproduct'.$i, TRUE);
						  $iproductgrade	= 'A';
						  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
              $this->mmaster->updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,0,$iarea);
            }
					}
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
#						$this->db->trans_rollback();
						$this->db->trans_commit();
						$sess=$this->session->userdata('session_id');
						$id=$this->session->userdata('user_id');
						$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
						$rs		= pg_query($sql);
						if(pg_num_rows($rs)>0){
							while($row=pg_fetch_assoc($rs)){
								$ip_address	  = $row['ip_address'];
								break;
							}
						}else{
							$ip_address='kosong';
						}
						$query 	= pg_query("SELECT current_timestamp as c");
						while($row=pg_fetch_assoc($query)){
							$now	  = $row['c'];
						}
						$pesan='Input SJ No:'.$isj;
						$this->load->model('logger');
						$this->logger->write($id, $ip_address, $now , $pesan );
						$data['sukses']			= true;
						$data['inomor']			= $isj;
						$this->load->view('nomor',$data);
					}
				}
			}else{
				$data['page_title'] = $this->lang->line('sj');
				$data['isj']='';
				$data['isi']='';
				$data['detail']="";
				$data['jmlitem']="";
				$data['dsj']='';
				$data['ispb']='';
				$data['dspb']='';
        if($this->uri->segment(5)!='')
        {
				  $data['iarea']=$this->uri->segment(5);
          $iarea=$this->uri->segment(5);
				  $data['istore']=$this->uri->segment(6);
				  $data['eareaname'] =str_replace('%20',' ' ,$this->uri->segment(4));
        }else{
				  $data['iarea']     ='';
          $iarea='';
				  $data['istore']    ='';
				  $data['eareaname'] ='';
        }
				$data['isjold']      ='';
				$data['vsjgross']    ='';
				$data['nsjdiscount1']='';
				$data['nsjdiscount2']='';
				$data['nsjdiscount3']='';
				$data['vsjdiscount1']='';
				$data['vsjdiscount2']='';
				$data['vsjdiscount3']='';
				$data['vsjdiscounttotal']='';
				$data['vsjnetto']    ='';
				$data['icustomer']   ='';
				$data['ecustomername']   ='';
				$data['isalesman']   ='';
        $data['ntop']        ='';
        $data['fspbconsigment']='';
        $data['fspbplusppn']='';
        $data['fplusppn']   ='';
        $data['dsj']        =date('Y-m-d');
        $data['tglakhir']='';
#        $data['tglakhirx']='';
				$this->load->view('sj/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu86')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sj');
			$this->load->view('sj/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$data['page_title'] = $this->lang->line('sj')." update";
			if($this->uri->segment(4)!=''){
				$isj 	  = $this->uri->segment(4);
				$iarea  = $this->uri->segment(5);
				$dfrom	= $this->uri->segment(6);
				$dto 	  = $this->uri->segment(7);
				$ispb	  = $this->uri->segment(8);
        $iareasj= substr($isj,8,2);
				$data['isj'] 	  = $isj;
				$data['iarea']	= $iarea;
 		 		$data['iareasj']= substr($isj,8,2);
				$data['dfrom']	= $dfrom;
				$data['dto']	  = $dto;
				$query 	= $this->db->query("select i_sj from tm_nota_item where i_sj = '$isj'");
				$data['jmlitem'] = $query->num_rows(); 				
			  $query=$this->db->query("	select f_spb_consigment
                                  from tm_spb where i_spb='$ispb' and i_area='$iarea' ",false);
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $row){
            $fspbconsigment=$row->f_spb_consigment;
				  }
			  }
				$this->load->model('sj/mmaster');
				$data['isi']=$this->mmaster->baca($isj,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isj,$iarea);
				$this->db->select("	a.*, b.e_area_name, c.e_customer_name from tm_nota a, tr_area b, tr_customer c
									          where a.i_sj ='$isj' and a.i_customer=c.i_customer
									          and a.i_area=b.i_area", false);
				$query = $this->db->get();
				if ($query->num_rows() > 0){
					foreach($query->result() as $row){
						$data['dsj']=$row->d_sj;
						$data['ispb']=$row->i_spb;
						$data['dspb']=$row->d_spb;
#						if($iareasj=='00'){
#							$data['istore']='AA';
#						}else{
#							$data['istore']=$iareasj;
#						}
            if($iareasj=='BK')$iareasj=$iarea;
            $que = $this->db->query("select i_store from tr_area where i_area='$iareasj'");
            $st=$que->row();
            $data['istore']=$st->i_store;
						$data['isjold']=$row->i_sj_old;
						$data['eareaname']=$row->e_area_name;
						$data['vsjgross']=$row->v_nota_gross;
						$data['nsjdiscount1']=$row->n_nota_discount1;
						$data['nsjdiscount2']=$row->n_nota_discount2;
						$data['nsjdiscount3']=$row->n_nota_discount3;
						$data['vsjdiscount1']=$row->v_nota_discount1;
						$data['vsjdiscount2']=$row->v_nota_discount2;
						$data['vsjdiscount3']=$row->v_nota_discount3;
						$data['vsjdiscounttotal']=$row->v_nota_discounttotal;
						$data['vsjnetto']=$row->v_nota_netto;
						$data['icustomer']=$row->i_customer;
						$data['ecustomername']=$row->e_customer_name;
						$data['isalesman']=$row->i_salesman;
            $data['fplusppn']=$row->f_plus_ppn;
            $data['ntop']=$row->n_nota_toplength;
            $data['fspbconsigment']=$fspbconsigment;
        #$data['dsj']        =date('Y-m-d');
        #$data['tglakhir']='';
					}
				}
		 		$this->load->view('sj/vmainform',$data);
			}else{
				$this->load->view('sj/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$isj	  = $this->input->post('isj', TRUE);
			$isjold	= $this->input->post('isjold', TRUE);
			$dsj   	= $this->input->post('dsj', TRUE);
 			$iarea	= $this->input->post('iarea', TRUE);
			$ispb 	= $this->input->post('ispb', TRUE);
			$dspb	  = $this->input->post('dspb', TRUE);
			if($dsj!=''){
				$tmp	= explode("-",$dsj);
				$th	= $tmp[2];
				$bl	= $tmp[1];
				$hr	= $tmp[0];
				$dsj	= $th."-".$bl."-".$hr;
				$thbl	= substr($th,2,2).$bl;
				$tmpsj	= explode("-",$isj);
				$firstsj= $tmpsj[0];
				$lastsj	= $tmpsj[2];
				$newsj	= $firstsj."-".$thbl."-".$lastsj;
			}
			if($dspb!=''){
				$tmp=explode("-",$dspb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspb=$th."-".$bl."-".$hr;
			}
			$eareaname	= $this->input->post('eareaname', TRUE);
			$isalesman		= $this->input->post('isalesman',TRUE);
			$icustomer		= $this->input->post('icustomer',TRUE);
			$nsjdiscount1	= $this->input->post('nsjdiscount1',TRUE);
			$nsjdiscount1	= str_replace(',','',$nsjdiscount1);
			$nsjdiscount2	= $this->input->post('nsjdiscount2',TRUE);
			$nsjdiscount2	= str_replace(',','',$nsjdiscount2);
			$nsjdiscount3	= $this->input->post('nsjdiscount3',TRUE);
			$nsjdiscount3	= str_replace(',','',$nsjdiscount3);
			$vsjdiscount1	= $this->input->post('vsjdiscount1',TRUE);
			$vsjdiscount1	= str_replace(',','',$vsjdiscount1);
			$vsjdiscount2	= $this->input->post('vsjdiscount2',TRUE);
			$vsjdiscount2	= str_replace(',','',$vsjdiscount2);
			$vsjdiscount3	= $this->input->post('vsjdiscount3',TRUE);
			$vsjdiscount3	= str_replace(',','',$vsjdiscount3);
			$vsjdiscounttotal	= $this->input->post('vsjdiscounttotal',TRUE);
			$vsjdiscounttotal	= str_replace(',','',$vsjdiscounttotal);
			$vsjgross	= $this->input->post('vsjgross',TRUE);
			$vsjgross	= str_replace(',','',$vsjgross);
			$vsjnetto	= $this->input->post('vsjnetto',TRUE);
			$vsjnetto	= str_replace(',','',$vsjnetto);

			$jml	  = $this->input->post('jml', TRUE);
			
			if($isj!='' && $dsj!='' && $eareaname!='')
			{
				$gaono=true;
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					if($cek=='on'){
						$gaono=false;
					}
					if(!$gaono) break;
				}
				if(!$gaono){
					$this->db->trans_begin();
					$this->load->model('sj/mmaster');
					$istore	= $this->input->post('istore', TRUE);
					$kons		= $this->mmaster->cekkons($ispb,$iarea);
					if($istore=='AA'){
						$istorelocation		= '01';
					}else{
            if($kons=='t')
              $istorelocation		= 'PB';
            else
      				$istorelocation		= '00';
					}
					$istorelocationbin	= '00';
					$eremark		= 'SPB';
				  $this->mmaster->updatesjheader($ispb,$dspb,$isj,$dsj,$iarea,$isalesman,$icustomer,
								$nsjdiscount1,$nsjdiscount2,$nsjdiscount3,$vsjdiscount1, 
								$vsjdiscount2,$vsjdiscount3,$vsjdiscounttotal,$vsjgross,$vsjnetto,$isjold);
					
					$this->mmaster->updatespb($ispb,$iarea,$isj,$dsj);
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
						$iproduct	= $this->input->post('iproduct'.$i, TRUE);
						$eproductname	= $this->input->post('eproductname'.$i, TRUE);
						$iproductgrade= 'A';
						$iproductmotif= $this->input->post('motif'.$i, TRUE);
						$ntmp		= $this->input->post('ntmp'.$i, TRUE);
						$ntmp		= str_replace(',','',$ntmp);
            $ndeliver		= $this->input->post('ndeliver'.$i, TRUE);
					  $ndeliver		= str_replace(',','',$ndeliver);
            if($ntmp!=$ndeliver){
						  $this->mmaster->deletesjdetail($iproduct,$iproductgrade,$iproductmotif,$isj,$iarea);

						  $th=substr($dsj,0,4);
						  $bl=substr($dsj,5,2);
						  $emutasiperiode=$th.$bl;
						  $tra=$this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj,$ntmp,$eproductname);
              if( ($ntmp!='') && ($ntmp!=0) ){
					      $this->mmaster->updatemutasi04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ntmp,$emutasiperiode);
					      $this->mmaster->updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ntmp);
              }
						  if($cek=='on'){
						    $ndeliver		= $this->input->post('ndeliver'.$i, TRUE);
						    $ndeliver		= str_replace(',','',$ndeliver);
						    $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						    $vunitprice	= $this->input->post('vproductmill'.$i, TRUE);
						    $vunitprice	= str_replace(',','',$vunitprice);
						    $norder		= $this->input->post('norder'.$i, TRUE);
						    $norderx		= str_replace(',','',$norder);
						    
						    if($ndeliver>0){	
							    $this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                                      			     $vunitprice,$isj,$iarea,$i);
                  $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                  if(isset($trans)){
                    foreach($trans as $itrans)
                    {
                      $q_aw =$itrans->n_quantity_awal;
                      $q_ak =$itrans->n_quantity_akhir;
                      $q_in =$itrans->n_quantity_in;
                      $q_out=$itrans->n_quantity_out;
                      break;
                    }
                  }else{
						        $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
					          if(isset($trans)){
						          foreach($trans as $itrans)
						          {
						            $q_aw =$itrans->n_quantity_stock;
						            $q_ak =$itrans->n_quantity_stock;
						            $q_in =0;
						            $q_out=0;
						            break;
						          }
						        }else{
							        $q_aw=0;
							        $q_ak=0;
							        $q_in=0;
							        $q_out=0;
						        }	
                  }
							    $this->mmaster->inserttrans4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$ndeliver,$q_aw,$q_ak,$tra);
							    if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
							    {
							      $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode);
							    }else{
							      $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$emutasiperiode,$q_aw);
							    }
							    if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
							    {
							      $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ndeliver,$q_ak);
							    }else{
							      $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ndeliver,$q_aw);
							    }
							    $this->mmaster->updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$iarea);
						    }
						  }else{
                $this->mmaster->updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,0,$iarea);
              }
            }
					}
					if (($this->db->trans_status() === FALSE))
					{
						$this->db->trans_rollback();
					}else{
#						$this->db->trans_rollback();
						$this->db->trans_commit();

						$sess=$this->session->userdata('session_id');
						$id=$this->session->userdata('user_id');
						$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
						$rs		= pg_query($sql);
						if(pg_num_rows($rs)>0){
							while($row=pg_fetch_assoc($rs)){
								$ip_address	  = $row['ip_address'];
								break;
							}
						}else{
							$ip_address='kosong';
						}
						$query 	= pg_query("SELECT current_timestamp as c");
						while($row=pg_fetch_assoc($query)){
							$now	  = $row['c'];
						}
						$pesan='Edit SJ No:'.$isj;
						$this->load->model('logger');
						$this->logger->write($id, $ip_address, $now , $pesan );

						$data['sukses']			= true;
						if($sjnew=1){
							$data['inomor']			= $newsj;
						}else{
							$data['inomor']			= $isj;
						}	
						$this->load->view('nomor',$data);
					}
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu86')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj	= $this->input->post('isjdelete', TRUE);
			$this->load->model('sj/mmaster');
			$this->mmaster->delete($isj);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus SJ No:'.$isj;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('sj');
			$data['isj']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('sj/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu86')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj			= $this->input->post('isjdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
  			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$this->db->trans_begin();
			$this->load->model('sj/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $isj, $iproductmotif);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus Detail SJ No:'.$isj.' Kode Barang :'.$iproduct;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

				$data['page_title'] = $this->lang->line('sj')." Update";
				$query = $this->db->query("select * from tm_spb_item where i_spb = '$isj'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['isj'] = $isj;
				$data['isi']=$this->mmaster->baca($isj);
				$data['detail']=$this->mmaster->bacadetail($isj);
				$this->load->view('sj/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu86')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['spb']=$this->uri->segment(5);
			$spb=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/sj/cform/product/'.$baris.'/'.$spb.'/';
			$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
							                    a.e_product_motifname as namamotif, 
							                    c.e_product_name as nama,b.v_unit_price as harga
							                    from tr_product_motif a,tr_product c, tm_spb_item b
							                    where a.i_product=c.i_product 
							                    and b.i_product_motif=a.i_product_motif
							                    and c.i_product=b.i_product
							                    and b.i_spb='$spb' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('sj/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($spb,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$data['spb']=$spb;
			$this->load->view('sj/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu86')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/sj/cform/product/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										              c.e_product_name as nama,c.v_product_mill as harga
										              from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product
										              and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
									                ,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('sj/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('sj/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu86')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/sj/cform/area/index/';
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area
										   where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('sj/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sj/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu86')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/sj/cform/area/index/';
			$cari = $this->input->post('cari', FALSE)?$this->input->post('cari', FALSE):$this->input->get_post('cari', FALSE);
			$cari=strtoupper($cari);
			$stquery = "select * from tr_area
						   			   where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5')
									   and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')";

			$query = $this->db->query($stquery,false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sj/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($area1,$area2,$area3,$area4,$area5,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sj/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function spb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu86')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area=$this->uri->segment(4);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/sj/cform/spb/'.$area.'/';
			if($area1=='00'||$area2=='00'||$area3=='00'||$area4=='00'||$area5=='00'){
				$query = $this->db->query(" select a.i_spb from tm_spb a, tr_customer b, tr_area c
											where a.i_customer=b.i_customer and a.i_area=c.i_area
											and a.i_nota isnull
											and not a.i_store isnull 
											and a.f_spb_cancel = 'f' 
											and a.i_area='$area'
											and  (
										        (	not a.i_approve1 isnull 
											        and not a.i_approve2 isnull 
											        and a.f_spb_siapnotagudang = 't'
											        and a.f_spb_siapnotasales = 't')
                              and 
                                ( (a.f_spb_stockdaerah = 'f' and f_spb_consigment='f')
                                   or

                                  (a.f_spb_stockdaerah = 't' and f_spb_consigment='t') )
										        )
											and a.i_sj isnull",false);
# and a.f_spb_valid = 't'
			}else{
				$query = $this->db->query("	select a.i_spb from tm_spb a, tr_customer b, tr_area c
											where a.i_customer=b.i_customer and a.i_area=c.i_area and
											a.i_area='$area'
											and a.i_nota isnull
											and not a.i_store isnull 
											and a.f_spb_cancel = 'f' 
											and (
													a.f_spb_stockdaerah = 't'
												)
											and a.i_sj isnull",false);
# and a.f_spb_valid = 't'
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sj/mmaster');
			$data['page_title'] = $this->lang->line('listspb');
			$data['area']=$area;
			$data['isi']=$this->mmaster->bacaspb($area1,$area2,$area3,$area4,$area5,$area,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sj/vlistspb', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carispb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu86')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$area = $this->input->post('iarea', FALSE);
			$config['base_url'] = base_url().'index.php/sj/cform/spb/'.$area.'/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			if($area1=='00'||$area2=='00'||$area3=='00'||$area4=='00'||$area5=='00'){
				$query = $this->db->query("	select a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
											where a.i_customer=b.i_customer and a.i_area=c.i_area
											and a.i_nota isnull
											and not a.i_store isnull 
											and a.f_spb_cancel = 'f' 
											and a.i_area='$area'
											and  (
										        (	not a.i_approve1 isnull 
											        and not a.i_approve2 isnull 
											        and a.f_spb_siapnotagudang = 't'
											        and a.f_spb_siapnotasales = 't')
                              and 
                                ( (a.f_spb_stockdaerah = 'f' and f_spb_consigment='f')
                                   or
                                  (a.f_spb_stockdaerah = 't' and f_spb_consigment='t') )
										        )
											and (upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')
											and a.i_sj isnull",false);
# and a.f_spb_valid = 't'
			}else{
				$query = $this->db->query("	select a.*, b.e_customer_name, c.e_area_name from tm_spb a, tr_customer b, tr_area c
											where a.i_customer=b.i_customer and a.i_area=c.i_area and
											a.i_area='$area'
											and a.i_nota isnull
											and not a.i_store isnull 
											and a.f_spb_cancel = 'f' 
											and (
													a.f_spb_stockdaerah = 't'
												)
											and (upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')
											and a.i_sj isnull",false);
# and a.f_spb_valid = 't'
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sj/mmaster');
			$data['area']=$area;
			$data['page_title'] = $this->lang->line('listspb');
			$data['isi']=$this->mmaster->carispb($cari,$area1,$area2,$area3,$area4,$area5,$area,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sj/vlistspb', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu86')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/sj/cform/index/';
			$query = $this->db->query("select * from tm_spb
						   where upper(i_spb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('sj/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spb');
			$data['isj']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('sj/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function hitung()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu86')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('sj/mmaster');
			$dsj 	= $this->uri->segment(8);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
        $tglsjbaru=$th.$bl.$hr;
				$dsj=$th."-".$bl."-".$hr;
			}
			$ispb		      = $this->uri->segment(4);
			$dspb		      = $this->uri->segment(5);
			$iarea		    = $this->uri->segment(6);
      $eareaname	  = str_replace('%20',' ',$this->uri->segment(7));

      $typearea		= $this->mmaster->cekdaerah($ispb,$iarea);
      if($typearea=='t'){
	      $areasj=$iarea;
      }else{
	      $areasj='00';
      }
      $tglakhir='';
      $sjpot='SJ-'.substr(date('Y'),2,2).'%-'.$areasj;
      $query  = $this->db->query("SELECT d_sj from tm_nota where i_sj like '$sjpot%' order by d_sj desc");
			if($query->num_rows()>0){
        foreach($query->result() as $tmp){
          $tglakhir=$tmp->d_sj;
          break;
        }
      }
      $data['txttglakhir']='';
      $data['tglakhirx']=$tglakhir;
      if($tglakhir!=''){
			  $tmp=explode("-",$tglakhir);
			  $hr=$tmp[2];
			  $bl=$tmp[1];
			  $th=$tmp[0];
			  $data['txttglakhir']=$hr."-".$bl."-".$th;
//			  $data['tglakhirx']=$hr."-".$bl."-".$th;
        $tglakhir=$th.$bl.$hr;
      }

			$istore		    = $this->uri->segment(9);
			$isjold		    = $this->uri->segment(12);
			$ecustomername= $this->uri->segment(10);
      $ecustomername= str_replace('%20',' ',$ecustomername);
#      $ecustomername= str_replace(" n "," & ",$ecustomername);
      $ecustomername= str_replace("tandakoma",",",$ecustomername);
      $ecustomername= str_replace("tandapetiksatukebalik","`",$ecustomername);
      $ecustomername= str_replace("tandapetiksatu","'",$ecustomername);
      $ecustomername= str_replace("tandakurungbuka","(",$ecustomername);
      $ecustomername= str_replace("tandakurungtutup",")",$ecustomername);
      $ecustomername= str_replace("tandadan","&",$ecustomername);
			$icustomer		= $this->uri->segment(11);
			$query        = $this->db->query(" select a.i_product as kode
							       from tr_product_motif a,tr_product c, tm_spb_item b
							       where a.i_product=c.i_product 
							       and b.i_product_motif=a.i_product_motif
							       and c.i_product=b.i_product
							       and b.i_spb='$ispb' and b.i_area='$iarea' ",false);
			$data['jmlitem'] = $query->num_rows(); 
			$data['page_title'] = $this->lang->line('sj');
			$data['isj']='';
			$data['isi']="xxxxx";
			$data['dsj']=$dsj;
			$data['ispb']=$ispb;
			$data['dspb']=$dspb;
			$data['iarea']=$iarea;
			$data['istore']=$istore;
			$data['isjold']=$isjold;
			$data['eareaname']=$eareaname;
			$data['ecustomername']=$ecustomername;
			$data['icustomer']=$icustomer;
			$query=$this->db->query("	select v_spb, n_spb_discount1, n_spb_discount2, n_spb_discount3,
                                v_spb_discount1, v_spb_discount2, v_spb_discount3, v_spb_discounttotal,
                                i_customer, i_salesman, n_spb_toplength, f_spb_consigment, f_spb_plusppn
                                from tm_spb where i_spb='$ispb' and i_area='$iarea' ",false);
			if ($query->num_rows() > 0){
				foreach($query->result() as $row){
					$vsjgross	    = $row->v_spb;
					$nsjdiscount1 = $row->n_spb_discount1;
					$nsjdiscount2 = $row->n_spb_discount2;
					$nsjdiscount3 = $row->n_spb_discount3;
					$vsjdiscount1 = $row->v_spb_discount1;
					$vsjdiscount2 = $row->v_spb_discount2;
					$vsjdiscount3 = $row->v_spb_discount3;
					$vsjdiscounttotal=$row->v_spb_discounttotal;
					$vsjnetto 	  = $row->v_spb-$row->v_spb_discounttotal;
					$icustomer	  = $row->i_customer;
					$isalesman	  = $row->i_salesman;
					$ntop     	  = $row->n_spb_toplength;
          $fspbconsigment=$row->f_spb_consigment;
          $fspbplusppn  = $row->f_spb_plusppn;
//          echo 'disc = '.$vsjdiscounttotal;
				}
			}
			$data['vsjgross']	=$vsjgross;
			$data['nsjdiscount1']=$nsjdiscount1;
			$data['nsjdiscount2']=$nsjdiscount2;
			$data['nsjdiscount3']=$nsjdiscount3;
			$data['vsjdiscount1']=$vsjdiscount1;
			$data['vsjdiscount2']=$vsjdiscount2;
			$data['vsjdiscount3']=$vsjdiscount3;
			$data['vsjdiscounttotal']=$vsjdiscounttotal;
			$data['vsjnetto']=$vsjnetto;
			$data['icustomer']=$icustomer;
			$data['isalesman']=$isalesman;
			$data['ntop']=$ntop;
			$data['fspbplusppn']=$fspbplusppn;
			$data['fspbconsigment']=$fspbconsigment;
			$data['detail']=$this->mmaster->product($ispb,$iarea);
      $data['tglsjbaru']=$tglsjbaru;
      $data['tglakhir']=$tglakhir;
      $data['areasj']=$areasj;
			$this->load->view('sj/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
