<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu150')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exp-kb');
			$data['datefrom']='';
			$data['dateto']	='';
			$this->load->view('exp-kb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu150')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-kb/mmaster');
			$datefrom = $this->input->post('datefrom');
			$dateto	  = $this->input->post('dateto');
      $no   	  = $this->input->post('no');
			if($datefrom!=''){
				$tmp=explode("-",$datefrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$datefrom=$th."-".$bl."-".$hr;
				$periodeawal	= $hr."/".$bl."/".$th;
			}
			$iarea= $this->input->post('iarea');
			$tmp  = explode("-", $dateto);
			$det	= $tmp[0];
			$mon	= $tmp[1];
			$yir 	= $tmp[2];
			$dtos	= $yir."/".$mon."/".$det;
			$periodeakhir	= $det."/".$mon."/".$yir;
			$dtos	= $this->mmaster->dateAdd("d",1,$dtos);
			$tmp 	= explode("-", $dtos);
			$det1	= $tmp[2];
			$mon1	= $tmp[1];
			$yir1 	= $tmp[0];
			$dtos	= $yir1."-".$mon1."-".$det1;
			$data['page_title'] = $this->lang->line('exp-kb');
			$qareaname	= $this->mmaster->eareaname($iarea);
			if($qareaname->num_rows()>0) {
				$row_areaname	= $qareaname->row();
				$aname	= $row_areaname->e_area_name;
			} else {
				$aname	= '';
			}
			$this->db->select("	vc, d_bukti, i_kb, e_description, i_coa, i_area, v_kb, f_debet, d_kb
								from(
								select  a.d_bukti, a.i_kb, a.e_description, a.i_coa, b.i_rv as vc, a.i_area, a.v_kb, a.f_debet, a.d_kb
								from tr_area e, tm_kb a
								left join tm_rv_item b on (a.i_kb=b.i_kk and a.i_area=b.i_area_kb and b.i_rv_type='01')
								inner join tm_rv c on(b.i_rv_type=c.i_rv_type and b.i_area=c.i_area and b.i_rv=c.i_rv) 
								where a.i_area=e.i_area and a.d_kb >= '$datefrom' and a.d_kb <= '$dtos'  and a.f_kb_cancel='f' and a.i_area='$iarea'
								union all
								select  a.d_bukti, a.i_kb, a.e_description, a.i_coa, b.i_pv as vc, a.i_area, a.v_kb, a.f_debet, a.d_kb
								from tr_area e, tm_kb a
								left join tm_pv_item b on (a.i_kb=b.i_kk and a.i_area=b.i_area_kb and b.i_pv_type='01')
								inner join tm_pv c on(b.i_pv_type=c.i_pv_type and b.i_area=c.i_area and b.i_pv=c.i_pv) 
								where  a.i_area=e.i_area and a.d_kb >= '$datefrom' and a.d_kb <= '$dtos'  and a.f_kb_cancel='f' and a.i_area='$iarea'
								) as a
								order by a.i_area, a.d_kb, a.i_kb",false);
			$query=$this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Kas Kecil Harian")
						->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:A4'
				);
        
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(22);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
        if($iarea=='00'){
  				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(13);
        }	

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'LAPORAN KAS BESAR');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,11,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'AREA '.$aname);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,11,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$periodeawal.' - '.$periodeakhir);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,11,3);
        $objPHPExcel->getActiveSheet()->setCellValue('A4', 'No : '.$no);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,4,11,4);
				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Tgl Trans');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tgl Bukti');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'No Reff');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Keterangan');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'NO Perk');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'NO Perk Asal');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Debet');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Kredit');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Voucher');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$i=7;
				$j=7;
				$xarea='';
				$saldo=0;

				foreach($query->result() as $row){
					$periode=substr($datefrom,0,4).substr($datefrom,5,2);
					
					if($row->i_area!=$xarea)
					{
						$saldo	=$this->mmaster->bacasaldo($row->i_area,$periode,$datefrom);

						$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
					
						$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, "Saldo Awal");
						$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
            $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
            $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
            $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								)
							)
						);
						$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $saldo);
						$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN),
									'bottom'=> array('style' => Style_Border::BORDER_THIN),
									'left'  => array('style' => Style_Border::BORDER_THIN),
									'right' => array('style' => Style_Border::BORDER_THIN)
								),
							)
						);
            if($iarea=='00'){
						  $objPHPExcel->getActiveSheet()->getStyle('M'.$i)->applyFromArray(
							  array(
								  'borders' => array(
									  'top' 	=> array('style' => Style_Border::BORDER_THIN),
									  'bottom'=> array('style' => Style_Border::BORDER_THIN),
									  'left'  => array('style' => Style_Border::BORDER_THIN),
									  'right' => array('style' => Style_Border::BORDER_THIN)
								  ),
							  )
						  );
            }
						$i++;
					}
					$xarea=$row->i_area;
					if($row->f_debet=='f')
					{
						$debet =$row->v_kb;
						$kredit=0;
					}else{
						$kredit=$row->v_kb;
						$debet =0;
					}
					$saldo=$saldo+$debet-$kredit;
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j-6);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					if($row->d_kb!=''){
						$tmp=explode("-",$row->d_kb);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$row->d_kb=$hr."-".$bl."-".$th;
					}
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->d_kb);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					if($row->d_bukti!=''){
						$tmp=explode("-",$row->d_bukti);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$row->d_bukti=$hr."-".$bl."-".$th;
					}
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_bukti);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->i_kb);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->e_description);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->i_coa);
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
           if(strlen($row->i_coa)==5){
			        $coa  = $row->i_coa.$iarea;
		        }else{
              $coa  = substr($row->i_coa,0,5).$iarea;
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $coa);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
          }

					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $debet);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $kredit);
					$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $saldo);
					$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->vc);
					$objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);

					$i++;
					$j++;
				}
				$x=$i-1;
				$objPHPExcel->getActiveSheet()->getStyle('F7:G7'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
				$objPHPExcel->getActiveSheet()->getStyle('J7:L'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
        if($iarea=='00'){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => true
					    )
				    ),
				    'A7:M'.$x
			    );
			    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						    'wrap'      => true
					    )
				    ),
				    'J7:M'.$x
			    );
        }else{
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'wrap'      => true
					    )
				    ),
				    'A7:L'.$x
			    );
			    $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' 	=> array(
						    'name' 	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    ),
					    'alignment' => array(
						    'vertical'  => Style_Alignment::VERTICAL_CENTER,
						    'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT,
						    'wrap'      => true
					    )
				    ),
				    'J7:L'.$x
			    );
        }
			}
      if($iarea=='00'){
	  		$objPHPExcel->getActiveSheet()->getStyle('A6:K6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
      }else{
  			$objPHPExcel->getActiveSheet()->getStyle('A6:L6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
      }
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='kb-'.$no.'-'.substr($datefrom,5,2).'-'.substr($datefrom,0,4).'.xls';
      if(file_exists('excel/'.$iarea.'/'.$nama)){
        @chmod('excel/'.$iarea.'/'.$nama, 0777);
        @unlink('excel/'.$iarea.'/'.$nama);
      }
			$objWriter->save('excel/'.$iarea.'/'.$nama); 

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
      }
      $pesan='Export LKH tanggal:'.$datefrom.' sampai:'.$dateto.' Area:'.$iarea;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses'] = true;
			$data['inomor']	= "Kas Besar";
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu150')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-kb/cform/area/index/';
			$iuser	= $this->session->userdata('user_id');
			$allarea= $this->session->userdata('allarea');

			if($allarea=='t'){
				$query = $this->db->query(" select * from tr_area order by i_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-kb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('exp-kb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu150')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
            $allarea = $this->session->userdata('allarea');
            $iuser   = $this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/exp-kb/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($allarea=='t'){
				$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area",false);
			}else{
				$query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-kb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('exp-kb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
