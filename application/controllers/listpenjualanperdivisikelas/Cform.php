<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu387')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listpenjualanperdivisikelas');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listpenjualanperdivisikelas/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu387')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
#			$dfrom		= $this->input->post('dfrom');
#			if($dfrom=='') $dfrom	= $this->uri->segment(4);
#			$dto		= $this->input->post('dto');
#			if($dto=='') $dto	= $this->uri->segment(5);
      $dfrom		= $this->input->post('dfrom');
			$dto  		= $this->input->post('dto');
			if($dfrom=='') $dfrom	= $this->uri->segment(4);
			if($dto=='') $dto	= $this->uri->segment(5);
			$this->load->model('listpenjualanperdivisikelas/mmaster');
			$data['page_title'] = $this->lang->line('listpenjualanperdivisikelas');
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$data['dfrom']= $dfrom;
			$data['dto']= $dto;
      $data['prodnya']= $this->mmaster->bacaproductnya($dfrom,$dto);
      $data['areanya']= $this->mmaster->bacaareanya($dfrom,$dto);
      $data['kelasnya']= $this->mmaster->bacakelasnya();
			$data['isi']		= $this->mmaster->bacaperiode($dfrom,$dto);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Penjualan Per Divisi per Area Tanggal :'.$dfrom.' Sampai : '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listpenjualanperdivisikelas/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu387')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listpenjualanperdivisikelas');
			$this->load->view('listpenjualanperdivisikelas/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
