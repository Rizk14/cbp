<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu494')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('cek_rtunai');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('cekrtunai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu494')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari	  = $this->uri->segment(7);
      if($cari=='' || $cari=='index')	$cari	= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
      if($cari=='' || $cari=='index'){
	  		$config['base_url'] = base_url().'index.php/cekrtunai/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
        $cari='';
      }else{
  			$config['base_url'] = base_url().'index.php/cekrtunai/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
      }
			$teksquery=" select distinct a.i_rtunai AS irtunai
					from tm_rtunai a
          left join tr_area d on(a.i_area=d.i_area)
					where (upper(a.i_rtunai) like '%$cari%' ";
					if(!is_numeric($cari)){
						$teksquery=$teksquery.")";
					}else{
						$teksquery=$teksquery."or a.v_jumlah=$cari)";
					}
					$teksquery=$teksquery." and
					a.i_area='$iarea' and
					(a.d_rtunai >= to_date('$dfrom','dd-mm-yyyy') and
					a.d_rtunai <= to_date('$dto','dd-mm-yyyy')) and a.f_rtunai_cancel='f'";
			$query = $this->db->query($teksquery,false);
						
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('cek_rtunai');
			$this->load->model('cekrtunai/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Transfer uang masuk Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('cekrtunai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu494')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('cek_rtunai');
			$this->load->view('cekrtunai/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu494')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cekrtunai/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
						
			$teksquery=" select distinct a.i_customer AS icustomer
					from tm_tunai a
					left join tr_customer c on(a.i_customer=c.i_customer)
					left join tr_area d on(a.i_area=d.i_area)
					left join tr_customer_owner e on(a.i_customer=e.i_customer)
					left join tm_pelunasan on(a.i_tunai=tm_pelunasan.i_giro and a.d_tunai=tm_pelunasan.d_giro)
					left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
					where (upper(a.i_tunai) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(c.e_customer_name) like '%$cari%' ";
					if(!is_numeric($cari)){
						$teksquery=$teksquery.")";
					}else{
						$teksquery=$teksquery."or a.v_jumlah=$cari)";
					}
					$teksquery=$teksquery." and
					((tm_pelunasan.i_jenis_bayar!='02' and 
					tm_pelunasan.i_jenis_bayar!='01' and 
					tm_pelunasan.i_jenis_bayar!='04' and 
					tm_pelunasan.i_jenis_bayar='03') or ((tm_pelunasan.i_jenis_bayar='03') is null)) and 
					a.i_area='$iarea' and
					(a.d_tunai >= to_date('$dfrom','dd-mm-yyyy') and
					a.d_tunai <= to_date('$dto','dd-mm-yyyy')) and a.f_tunai_cancel='f'";
			$query = $this->db->query($teksquery,false);
									
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('cek_rtunai');
			$this->load->model('cekrtunai/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('cekrtunai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu494')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/cekrtunai/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('cekrtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('cekrtunai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu494')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/cekrtunai/cform/area/index/';
      $iuser   	= $this->session->userdata('user_id');
      $cari    	= $this->input->post('cari', FALSE);
      $cari 		  = strtoupper($cari);
      $query   	= $this->db->query("select * from tr_area
                                    where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                    and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('cekrtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('cekrtunai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (($this->session->userdata('logged_in'))
		){
			$data['page_title'] = $this->lang->line('cek_rtunai');
			if(
				($this->uri->segment(4)!='')
			  ){
        $irtunai          = $this->uri->segment(4);
				$iarea		      = $this->uri->segment(5);
				$dfrom		      = $this->uri->segment(6);
				$dto			      = $this->uri->segment(7);
				$data['irtunai']  = $irtunai;
				$data['iarea']		= $iarea;
				$data['dfrom']		= $dfrom;
				$data['dto']		= $dto;
        $data['pst']	= $this->session->userdata('i_area');
				$this->load->model("cekrtunai/mmaster");
				$data['isi']=$this->mmaster->baca($iarea,$irtunai);
				$data['detail']=$this->mmaster->bacadetail($iarea,$irtunai);
		 		$this->load->view('cekrtunai/vformupdate',$data);
			}else{
				$this->load->view('cekrtunai/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cek()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu494')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		  $irtunai= $this->input->post('irtunai', TRUE);
			$iarea	= $this->input->post('iarea', TRUE);
			$ecek1	= $this->input->post('eremarkcek',TRUE);
			if($ecek1=='')
				$ecek1=null;
			$user		=$this->session->userdata('user_id');
			$this->load->model('cekrtunai/mmaster');
			$this->db->trans_begin();
			$this->mmaster->cek($ecek1,$user,$irtunai,$iarea);
			if( ($this->db->trans_status() === FALSE) && ($cek) )
			{
				$this->db->trans_rollback();
			}else{
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Cek Setor Tunai Area '.$iarea.' No:'.$irtunai;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $irtunai;
				$this->db->trans_commit();
#  			$this->db->trans_rollback();
			  $this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
}
?>
