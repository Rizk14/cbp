<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_knalokasi');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listknalokasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	= $this->session->userdata('i_area');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listknalokasi/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.i_alokasi
							                    from tm_alokasikn a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_kn e
							                    where
							                    a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_jenis_bayar=d.i_jenis_bayar
							                    and a.i_area=e.i_area and a.i_kn=e.i_kn and e.i_kn_type='02'
							                    and (upper(a.i_kn) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                                  or upper(a.i_alokasi) like '%$cari%') and a.i_area='$iarea' and
							                    a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
							                    a.d_alokasi <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_knalokasi');
			$this->load->model('listknalokasi/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['area1']  = $area1;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Alokasi KN Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listknalokasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listknalokasi');
			$this->load->view('listknalokasi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	  = $this->session->userdata('i_area');
			$ialokasi	= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$ikn  		= $this->uri->segment(6);
			$dfrom		= $this->input->post('dfrom');
			$dto  		= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(7);
			if($dto=='') $dto=$this->uri->segment(8);
			$this->load->model('listknalokasi/mmaster');
      $this->db->trans_begin();
			$this->mmaster->delete($ialokasi,$iarea,$ikn);
      if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
			}else{
		    $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		= pg_query($sql);
		    if(pg_num_rows($rs)>0){
			    while($row=pg_fetch_assoc($rs)){
				    $ip_address	  = $row['ip_address'];
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }
		    $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
        	$now	  = $row['c'];
		    }
		    $pesan='Menghapus Alokasi Kredit Nota Area:'.$iarea.' No:'.$ialokasi.' KN no:'.$ikn;
		    $this->load->model('logger');
		    $this->logger->write($id, $ip_address, $now , $pesan );  

#				$this->db->trans_rollback();
				$this->db->trans_commit();

		    $cari	= strtoupper($this->input->post('cari'));
			  $config['base_url'] = base_url().'index.php/listknalokasi/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			  $query = $this->db->query(" select a.i_alokasi
							                      from tm_alokasikn a, tr_area b, tr_customer c, tr_jenis_bayar d, tm_kn e
							                      where
							                      a.i_area=b.i_area and a.i_customer=c.i_customer and a.i_jenis_bayar=d.i_jenis_bayar
							                      and a.i_area=e.i_area and a.i_kn=e.i_kn
							                      and (upper(a.i_kn) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                                    or upper(a.i_alokasi) like '%$cari%') and a.i_area='$iarea' and
							                      a.d_alokasi >= to_date('$dfrom','dd-mm-yyyy') AND
							                      a.d_alokasi <= to_date('$dto','dd-mm-yyyy')",false);
			  $config['total_rows'] = $query->num_rows(); 
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(9);
			  $this->pagination->initialize($config);

			  $data['page_title'] = $this->lang->line('list_knalokasi');
			  $this->load->model('listknalokasi/mmaster');
			  $data['cari']		= $cari;
			  $data['dfrom']	= $dfrom;
			  $data['dto']		= $dto;
			  $data['iarea']	= $iarea;
        $data['area1']  = $area1;
			  $data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
	      	$now	  = $row['c'];
			  }
			  $pesan='Membuka Data Alokasi KN Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );  
			  $this->load->view('listknalokasi/vmainform', $data);
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	= $this->session->userdata('i_area');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listknalokasi/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_pelunasan a 
										              inner join tr_area on(a.i_area=tr_area.i_area)
										              inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										              where (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%')
										              and a.i_area='$iarea' and
										              a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
										              a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_knalokasi');
			$this->load->model('listknalokasi/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['area1']  = $area1;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listknalokasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listknalokasi/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listknalokasi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listknalokasi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listknalokasi/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listknalokasi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listknalokasi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
