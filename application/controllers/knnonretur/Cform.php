<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('knnonretur');
		//	$this->load->model('knnonretur/mmaster');
			$data['ikn']='';
			$data['isi']='';#'$this->mmaster->bacasemua();
			$this->load->view('knnonretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('knnonretur');
			$this->load->view('knnonretur/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$data['page_title'] = $this->lang->line('knnonretur')." update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5) && $this->uri->segment(6)
			  ){
				$this->load->model('knnonretur/mmaster');
				$data['ikn'] 		= str_replace("%20"," ",$this->uri->segment(4));
				$data['nknyear']= $this->uri->segment(5);
				$data['iarea'] 	= $this->uri->segment(6);
				$dfrom					= $this->uri->segment(7);
				$dto 						= $this->uri->segment(8);
				$data['dfrom'] 	= $this->uri->segment(7);
				$data['dto']	 	= $this->uri->segment(8);
				$ikn 						= str_replace("%20"," ",$this->uri->segment(4));
				$nknyear				= $this->uri->segment(5);
				$iarea 					= $this->uri->segment(6);
				$data['isi']		= $this->mmaster->bacakn($ikn,$nknyear,$iarea);
########
				$query  = $this->mmaster->bacakn($ikn,$nknyear,$iarea);
				foreach($query as $r){
  			  $dkn=substr($r->d_kn,0,4).substr($r->d_kn,5,2);
  			}
				$data['bisaedit']=false;
				$query=$this->db->query("	select i_periode from tm_periode ",false);
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $rw){
            $periode=$rw->i_periode;
				  }
				  if($periode<=$dkn)$data['bisaedit']=true;
			  }
########
		 		$this->load->view('knnonretur/vformupdate',$data);
			}else{
				$this->load->view('knnonretur/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea 			  = $this->input->post('iarea', TRUE);
			$ikn 			    = $this->input->post('ikn', TRUE);
			$ikn          = str_replace("%20"," ",$ikn);
			$icustomer		= $this->input->post('icustomer', TRUE);
			$irefference	= $this->input->post('irefference', TRUE);
			$icustomergroupar= $this->input->post('icustomergroupar', TRUE);
			$isalesman		= $this->input->post('isalesman', TRUE);
			$ikntype		  = '02';
			$drefference	= $this->input->post('drefference', TRUE);
			if($drefference!=''){
				$tmp=explode("-",$drefference);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$drefference=$th."-".$bl."-".$hr;
			}
			$dkn			= $this->input->post('dkn', TRUE);
			if($dkn!=''){
				$tmp=explode("-",$dkn);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkn=$th."-".$bl."-".$hr;
				$nknyear=$th;
			}
			$fcetak		= 'f';
			$fmasalah	= $this->input->post('fmasalah', TRUE);
			if($fmasalah=='')
				$fmasalah='f';
			else
				$fmasalah='t';
			$finsentif	= $this->input->post('finsentif', TRUE);
			if($finsentif=='')
				$finsentif='f';
			else
				$finsentif='t';
			$vnetto		= $this->input->post('vnetto', TRUE);
			$vnetto		= str_replace(',','',$vnetto);
			$vsisa		= $vnetto;
			$vgross		= $this->input->post('vgross', TRUE);
			$vgross		= str_replace(',','',$vgross);
			$vdiscount	= $this->input->post('vdiscount', TRUE);
			$vdiscount	= str_replace(',','',$vdiscount);
			$eremark	= $this->input->post('eremark', TRUE);
			if (
				(isset($irefference) && $irefference != '') && 
				(isset($dkn) && $dkn != '') &&
				(isset($iarea) && $iarea != '') &&
				(isset($icustomer) && $icustomer != '')
			   )
			{
				$this->load->model('knnonretur/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update(	$iarea,$ikn,$icustomer,$irefference,$icustomergroupar,$isalesman,$ikntype,$dkn,$nknyear,$fcetak,$fmasalah,
										$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	        	$now	  = $row['c'];
			    }
			    $pesan='Update KN Non Retur No:'.$ikn.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan ); 

					$data['sukses']			= true;
					$data['inomor']			= $ikn;
					$this->load->view('nomor',$data);
				}

			}
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikn		= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$this->load->model('knnonretur/mmaster');
			$this->mmaster->delete($ikn,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Hapus KN Non Retur No:'.$ikn.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['page_title'] = $this->lang->line('knnonretur');
			$data['isi']=$this->mmaster->bacasemua();
			$data['ikn']='';
			$this->load->view('knnonretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/knnonretur/cform/area/index/';
			$query = $this->db->query("select * from tr_area ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('knnonretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('knnonretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/knnonretur/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_area
									   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('knnonretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('knnonretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bbm()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/knnonretur/cform/bbm/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select a.i_bbm
										from tm_bbm a, tr_salesman d, tm_ttbretur b, tr_area c
										where a.i_bbm_type='05' and a.i_area='$iarea' 
										and a.i_salesman=d.i_salesman 
										and a.i_bbm=b.i_bbm and a.i_area=b.i_area
										and a.i_area=c.i_area
										and a.i_bbm not in(select i_refference from tm_kn)",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('knnonretur/mmaster');
			$data['page_title'] = $this->lang->line('list_bbm');
			$data['isi']=$this->mmaster->bacabbm($iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('knnonretur/vlistbbm', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caribbm()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/knnonretur/cform/customer/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$iarea = $this->uri->segment(4);
			$query = $this->db->query("select * from tr_customer
						   where i_area='$iarea' 
							 and (upper(i_customer) like '%$cari%' 
						      	  or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('knnonretur/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
			$this->load->view('knnonretur/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea 			= $this->input->post('iarea', TRUE);
			$ikn 			= $this->input->post('ikn', TRUE);
			$icustomer		= $this->input->post('icustomer', TRUE);
			$irefference	= $this->input->post('irefference', TRUE);
			$icustomergroupar= $this->input->post('icustomergroupar', TRUE);
			$isalesman		= $this->input->post('isalesman', TRUE);
			$ikntype		= '02';
			$drefference	= $this->input->post('drefference', TRUE);
			if($drefference!=''){
				$tmp=explode("-",$drefference);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$drefference=$th."-".$bl."-".$hr;
			}
			$dkn			= $this->input->post('dkn', TRUE);
			if($dkn!=''){
				$tmp=explode("-",$dkn);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkn=$th."-".$bl."-".$hr;
				$nknyear=$th;
			}
			$fcetak		= 'f';
			$fmasalah	= $this->input->post('fmasalah', TRUE);
			if($fmasalah=='')
				$fmasalah='f';
			else
				$fmasalah='t';
			$finsentif	= $this->input->post('finsentif', TRUE);
			if($finsentif=='')
				$finsentif='f';
			else
				$finsentif='t';
			$vnetto		= $this->input->post('vnetto', TRUE);
			$vnetto		= str_replace(',','',$vnetto);
			$vsisa		= $vnetto;
			$vgross		= $this->input->post('vgross', TRUE);
			$vgross		= str_replace(',','',$vgross);
			$vdiscount	= $this->input->post('vdiscount', TRUE);
			$vdiscount	= str_replace(',','',$vdiscount);
			$eremark	= $this->input->post('eremark', TRUE);
			if (
				(isset($irefference) && $irefference != '') && 
				(isset($iarea) && $iarea != '') &&
				(isset($icustomer) && $icustomer != '')
			   )
			{
				$this->load->model('knnonretur/mmaster');
				$ikn = $this->mmaster->runningnumberkn($nknyear,$iarea,$ikn);
				$this->db->trans_begin();
				$this->mmaster->insert(	$iarea,$ikn,$icustomer,$irefference,$icustomergroupar,$isalesman,$ikntype,$dkn,$nknyear,$fcetak,$fmasalah,
										$finsentif,$vnetto,$vsisa,$vgross,$vdiscount,$eremark,$drefference);
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	        	$now	  = $row['c'];
			    }
			    $pesan='Input KN Non Retur No:'.$ikn.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan ); 

					$data['sukses']			= true;
					$data['inomor']			= $ikn;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/knnonretur/cform/customer/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_customer where i_area = '$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('knnonretur/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('knnonretur/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/knnonretur/cform/customer/'.$iarea.'/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_customer
									   	where i_area='$iarea' 
										and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('knnonretur/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('knnonretur/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
/*
	function salesman()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea		= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/knnonretur/cform/salesman/'.$iarea.'/index/';
			$query = $this->db->query("	select * from tr_salesman
										where tr_salesman.i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['iarea']		=$iarea;
			$this->load->model('knnonretur/mmaster');
			$data['page_title'] = $this->lang->line('list_salesman');
			$data['isi']=$this->mmaster->bacasalesman($iarea,$config['per_page'],$this->uri->segment(6));
			$this->load->view('knnonretur/vlistsalesman', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisalesman()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu113')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari		= strtoupper($this->input->post('cari',false));
			$iarea		= $this->input->post('iarea',false);
			$data['iarea']		=$iarea;
			$config['base_url'] = base_url().'index.php/knnonretur/cform/salesman/'.$iarea.'/index/';
			$query = $this->db->query("	select * from tr_salesman
										where tr_salesman.i_area='$iarea' 
										and (tr_salesman.i_salesman like '%$cari%' or tr_salesman.e_salesman_name like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 			
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('knnonretur/mmaster');
			$data['page_title'] = $this->lang->line('list_salesman');
			$data['isi']=$this->mmaster->carisalesman($iarea,$cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('knnonretur/vlistsalesman', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
*/
}
?>
