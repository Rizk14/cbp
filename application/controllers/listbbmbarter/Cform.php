<?php 
class Cform extends CI_Controller {
function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbbmretur').'(Barter)';
			$data['dfrom'] 		= '';
			$data['dto'] 		= '';
			$this->load->view('listbbmbarter/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari					= strtoupper($this->input->post('cari'));
			$dfrom					= $this->input->post('dfrom');
			$dto					= $this->input->post('dto');
			$iarea					= $this->input->post('iarea');
			if($dfrom=='') $dfrom 	= $this->uri->segment(4);
			if($dto=='') $dto 		= $this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listbbmbarter/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      	
      	if($iarea=='NA'){
			$query = $this->db->query(" select a.*, b.e_customer_name
										from tm_bbm a, tr_customer b
										where 
										a.i_area=b.i_area
										and a.i_supplier=b.i_customer
										--and a.f_bbm_cancel='f'
										and a.i_bbm_type='02'
										and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
										and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy')
										and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
										order by i_bbm desc",false);
      	}else{
			$query = $this->db->query(" select a.*, b.e_customer_name
										from tm_bbm a, tr_customer b
										where 
										a.i_area=b.i_area
										and a.i_supplier=b.i_customer
										--and a.f_bbm_cancel='f'
										and a.i_bbm_type='02'
										and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
										and a.i_area='$iarea'
										and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy')
										and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
										order by i_bbm desc",false);
      	}

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->pagination->initialize($config);
			
			$data['page_title'] 	= $this->lang->line('listbbmretur').'(Barter)';
			$this->load->model('listbbmbarter/mmaster');
			$data['cari']			= $cari;
			$data['dfrom']			= $dfrom;
			$data['dto']			= $dto;
			$data['iarea']			= $iarea;
			$data['isi']			= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess 					= $this->session->userdata('session_id');
			$id 					= $this->session->userdata('user_id');
			$sql					= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs						= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			
			$query 	= pg_query("SELECT current_timestamp as c");
	    	while($row=pg_fetch_assoc($query)){
	    		$now	  = $row['c'];
			}
				$pesan='Membuka Data BBM Barter Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listbbmbarter/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbbmretur').'(Barter)';
			$this->load->view('listbbmbarter/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari					= strtoupper($this->input->post('cari'));
			$dfrom					= $this->input->post('dfrom');
			$dto					= $this->input->post('dto');
			$iarea					= $this->input->post('iarea');
			if($dfrom=='') $dfrom 	= $this->uri->segment(4);
			if($dto=='') $dto 		= $this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] 	= base_url().'index.php/listbbmbarter/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      	
      	if($iarea=='NA'){
		  	$query = $this->db->query(" select a.*, b.e_customer_name
										from tm_bbm a, tr_customer b
										where 
										a.i_area=b.i_area
										and a.i_supplier=b.i_customer
										--and a.f_bbm_cancel='f'
										and a.i_bbm_type='02'
										and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
										and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy')
										and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
										order by i_bbm desc",false);
      	}else{
			$query = $this->db->query(" select a.*, b.e_customer_name
										from tm_bbm a, tr_customer b
										where 
										a.i_area=b.i_area
										and a.i_supplier=b.i_customer
										--and a.f_bbm_cancel='f'
										and a.i_bbm_type='02'
										and (b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
										and a.i_area='$iarea'
										and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy')
										and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
										order by i_bbm desc",false);
      	}

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] 	= $this->lang->line('listbbmretur').'(Barter)';
			$this->load->model('listbbmbarter/mmaster');
			$data['cari']			= $cari;
			$data['dfrom']			= $dfrom;
			$data['dto']			= $dto;
			$data['iarea']			= $iarea;
			$data['isi']			= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listbbmbarter/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu109')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbbmretur').'(Barter Update)';
			if(

				($this->uri->segment(4)!=='')
			  ){
				$ibbm		= $this->uri->segment(4);
				$iarea		= $this->uri->segment(5);
				$dfrom		= $this->uri->segment(6);
				$dto		= $this->uri->segment(7);
				$query 		= $this->db->query("select a.*, b.e_product_motifname from tm_bbm_item a, tr_product_motif b 
												where a.i_bbm = '$ibbm' 
												and a.i_product=b.i_product 
												and a.i_product_motif=b.i_product_motif");
				
			$data['jmlitem']	= $query->num_rows(); 				
			$data['ibbm'] 		= $ibbm;
			$data['iarea']		= $iarea;
			$data['dfrom']		= $dfrom;
			$data['dto'] 		= $dto;
			$this->load->model('listbbmbarter/mmaster');
			$data['isi']		= $this->mmaster->baca($ibbm);
			$data['detail']		= $this->mmaster->bacadetail($ibbm);
				
				$this->load->view('listbbmbarter/vformupdate',$data);
			}else{
				$this->load->view('listbbmbarter/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbm		= $this->uri->segment(4);
			//$ittb		= $this->uri->segment(5);
			$iarea	= $this->uri->segment(5);
			//$tahun	= $this->uri->segment(6);
			$this->load->model('listbbmbarter/mmaster');
			$this->mmaster->delete($ibbm,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Hapus BBM Barter No:'.$ibbm.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->uri->segment(6);
			$dto 		= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/listbbmbarter/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			
			$query = $this->db->query(" select a.*, b.e_customer_name, a.i_area
										from tm_bbm a, tr_customer b
										where 
										(b.i_customer like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_bbm) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_bbm >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_bbm <= to_date('$dto','dd-mm-yyyy')",false);

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] 	= $this->lang->line('listbbmretur').'(Barter)';
			$data['cari']			= $cari;
			$data['dfrom']			= $dfrom;
			$data['dto']			= $dto;
			$data['iarea']			= $iarea;
			$data['isi']			= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listbbmbarter/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listbbmbarter/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      		$iuser  = $this->session->userdata('user_id');
        	
        	$query 	= $this->db->query("select * from tr_area where i_area in (select i_area from tm_user_area where i_user='$iuser') 
        								and (i_area like '%$cari%' or e_area_name like '%$cari%') 
        								order by i_area", false);
			
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listbbmbarter/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listbbmbarter/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu110')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari					= strtoupper($this->input->post('xcari'));
			$dfrom					= $this->input->post('xdfrom');
			$dto					= $this->input->post('xdto');
			$iarea					= $this->input->post('xiarea');
			$is_cari 				= $this->input->post('xis_cari'); 
			if($dfrom=='') $dfrom 	= $this->uri->segment(4);
			if($dto=='') $dto 		= $this->uri->segment(5);
			if($iarea=='') $iarea 	= $this->uri->segment(6);
			
			if ($is_cari == '')
				$is_cari= $this->uri->segment(8);
			
			if ($cari == '' && $is_cari == "1") 
				$cari=$this->uri->segment(7);
			
			$this->load->model('listbbmbarter/mmaster');
			$data['page_title'] = $this->lang->line('listspb');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea'] 		= $iarea;

      		if($iarea!='NA'){
      			$sql = "a.*, c.e_customer_name, d.e_area_name, e.e_salesman_name, b.i_product,b.e_product_name, b.n_quantity 
      					from tm_bbm a, tm_bbm_item b, tr_customer c,tr_area d, tr_salesman e
						where
						a.i_bbm=b.i_bbm
						and a.i_salesman=e.i_salesman
						and a.i_area=d.i_area
						and a.i_supplier=c.i_customer
						and a.i_bbm_type=b.i_bbm_type
						and a.i_bbm_type='02'
						and a.i_area='$iarea'
						and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy')
						and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
						order by i_bbm";
			}else{
				$sql = "a.*, c.e_customer_name, d.e_area_name, e.e_salesman_name, b.i_product,b.e_product_name, b.n_quantity 
      					from tm_bbm a, tm_bbm_item b, tr_customer c,tr_area d, tr_salesman e
						where
						a.i_bbm=b.i_bbm
						and a.i_salesman=e.i_salesman
						and a.i_area=d.i_area
						and a.i_supplier=c.i_customer
						and a.i_bbm_type=b.i_bbm_type
						and a.i_bbm_type='02'
						and a.d_bbm >= to_date('$dfrom','dd-mm-yyyy')
						and a.d_bbm <= to_date('$dto','dd-mm-yyyy')
						order by i_bbm";
			}

			$this->db->select($sql,false);
			$query = $this->db->get();

			$sess 	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$queryy 	= pg_query("SELECT current_timestamp as c");
	    	while($row=pg_fetch_assoc($queryy)){
	    	$now	  = $row['c'];
			}
			$pesan='Export Daftar BBM Barter Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

#####
      		$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar BBM Barter ")->setDescription("PT. Bahtera Laju Nusantara");
			$objPHPExcel->setActiveSheetIndex(0);
      if ($query->num_rows() > 0){
     		$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => FALSE
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(3);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar BBM Barter');
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Tanggal : '.$dfrom.' s/d '.$dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,11,2);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:H5'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'No BBM');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tanggal BBM');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
							'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Salesman');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Customer');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Produk');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Qty');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        $i=6;
		$j=6;
        $no=0;
				foreach($query->result() as $row){
         $no++;
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ), 
				  'A'.$i.':H'.$i
				  );

         		$tmp 		= explode("-",$row->d_bbm);
         		$hr 		= $tmp[2];
         		$bl 		= $tmp[1];
         		$th 		= $tmp[0];
         		$row->d_bbm = $hr.'-'.$bl.'-'.$th;
    
					if($row->f_bbm_cancel=='t'){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':H'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF0000');
                    }

					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_bbm, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_bbm, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->i_area.'-'.$row->e_area_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->i_salesman.'-'.$row->e_salesman_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, '('.$row->i_supplier.')-'.$row->e_customer_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->i_product.'-'.$row->e_product_name);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->n_quantity);
					$i++;
				}
			}
			
		$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      	$nama='List BMB - '.$th.$bl.'.xls';
			if($iarea!='NA'){
              $iarea=$iarea;
            }else{
              $iarea='00';
            }

      if(file_exists('excel/'.$iarea.'/'.$nama)){
        @chmod('excel/'.$iarea.'/'.$nama, 0777);
        @unlink('excel/'.$iarea.'/'.$nama);
      }
		
		$objWriter->save("excel/".$iarea.'/'.$nama); 
      	@chmod('excel/'.$iarea.'/'.$nama, 0777);
      	//echo $nama;
#####	
      	$data['sukses']   = true;
        $data['inomor']   = "Berhasil Export ".$nama;
            $this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
