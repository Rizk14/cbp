<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
    $this->load->helper(array('file','directory','fusioncharts'));
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu403')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspbperdivisi');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listspbperdivisirekap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu403')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			if($dfrom=='') $dfrom	= $this->uri->segment(4);
			$dto		= $this->input->post('dto');
			if($dto=='') $dto	= $this->uri->segment(5);
			$this->load->model('listspbperdivisirekap/mmaster');
			$data['page_title'] = $this->lang->line('listspbperdivisi');
      if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom=$th."-".$bl."-".$hr;
			}
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$data['dfrom']= $dfrom;
			$data['dto']= $dto;
      $data['prodnya']= $this->mmaster->bacaproductnya($dfrom,$dto);
      $data['areanya']= $this->mmaster->bacaareanya($dfrom,$dto);
			$data['isi']		= $this->mmaster->bacaperiode($dfrom,$dto);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Penjualan Per Divisi Tanggal :'.$dfrom.' Sampai : '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listspbperdivisirekap/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

  function fcfx(){
    $dfrom=$this->uri->segment(4);
    $dto=$this->uri->segment(5);
    $tipe=$this->uri->segment(6);
    if($tipe==''){
      $graph_swfFile      = base_url().'flash/FCF_MSColumn3D.swf';
    }else{
      $tipe=str_replace("tandatitik",".",$tipe);
      $graph_swfFile      = base_url().'flash/'.$tipe;
    }
    
    $graph_caption      = 'SPB per Divisi Periode : '.$dfrom.' s/d '.$dto;

    if($dfrom!=''){
			$tmp=explode("-",$dfrom);
			$th=$tmp[2];
			$bl=$tmp[1];
			$hr=$tmp[0];
			$dfromx=$th."-".$bl."-".$hr;
		}
    if($dto!=''){
			$tmp=explode("-",$dto);
			$th=$tmp[2];
			$bl=$tmp[1];
			$hr=$tmp[0];
			$dtox=$th."-".$bl."-".$hr;
		}

    $graph_numberPrefix = 'Rp.' ;
    $graph_title        = 'SPB Per Divisi';
    $graph_width        = 954;
    $graph_height       = 500;
    $this->load->model('listspbperdivisirekap/mmaster');
    $i=0;
    $result = $this->mmaster->bacaareanya($dfromx,$dtox);
    foreach($result as $row){
      $category[$i] = $row->i_area;
      $i++;
    }
    $dataset[0] = 'Bedding' ;
    $dataset[1] = 'NonBedding' ;
    $dataset[2] = 'DialogueHome' ;
    $dataset[3] = 'Konsinyasi' ;

    $i=0;
    $result = $this->mmaster->bacabedding($dfromx,$dtox);
    foreach($result as $row){
      $arrData['Bedding'][$i] = intval($row->jumlah);
      $i++;
    }
    $i=0;
    $result = $this->mmaster->bacanonbedding($dfromx,$dtox);
    foreach($result as $row){
      $arrData['NonBedding'][$i] = intval($row->jumlah);
      $i++;
    }
    $i=0;
    $result = $this->mmaster->bacahome($dfromx,$dtox);
    foreach($result as $row){
      $arrData['DialogueHome'][$i] = intval($row->jumlah);
      $i++;
    }
    $i=0;
    $result = $this->mmaster->bacakonsinyasi($dfromx,$dtox);
    foreach($result as $row){
      $arrData['Konsinyasi'][$i] = intval($row->jumlah);
      $i++;
    }

    $strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='100' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='0' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' showValues='0'>" ;
    $strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
    foreach ($category as $c) {
        $strXML .= "<category name='".$c."'/>" ;
    }
    $strXML .= "</categories>" ;
    foreach ($dataset as $set) {
        $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
        foreach ($arrData[$set] as $d) {
            $strXML .= "<set value='".$d."'/>" ;
        }
        $strXML .= "</dataset>" ;
    }
$strXML .= "</graph>";

    $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);
    $data['dfrom']=$dfrom;
    $data['dto']=$dto;
    $data['modul']='listspbperdivisirekap';
    $data['isi']= directory_map('./flash/');
    $data['file']='';
    $this->load->view('listspbperdivisirekap/chart_view',$data) ;
  }

	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu403')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspbperdivisi');
			$this->load->view('listspbperdivisirekap/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
