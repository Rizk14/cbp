<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationxx');
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
        $area1 = $this->session->userdata('i_area');
           $area2 = $this->session->userdata('i_area2');
           $area3 = $this->session->userdata('i_area3');
           $area4 = $this->session->userdata('i_area4');
           $area5 = $this->session->userdata('i_area5');
           $config['base_url'] = base_url().'index.php/customersalesman/cform/index/';
           $cari   = ($this->input->post('cari', FALSE));
        $per    = date('Ym');
        if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
             $query = $this->db->query("  select a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
                                             from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d
                                             where a.i_customer = b.i_customer
                                                and a.i_area = c.i_area
                                                and a.i_salesman = d.i_salesman
                                                and a.i_area=b.i_area
                                      and a.e_periode='$per'",false);
        }else{
             $query = $this->db->query("  select a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
                                             from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d
                                             where a.i_customer = b.i_customer and a.i_area = c.i_area
                                                and a.i_salesman = d.i_salesman
                                                and a.i_area=b.i_area
                                                and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                                or a.i_area = '$area4' or a.i_area = '$area5') and a.e_periode='$per'",false);
        }

           $config['total_rows'] = $query->num_rows();
           $config['per_page'] = '10';
           $config['first_link'] = 'Awal';
           $config['last_link'] = 'Akhir';
           $config['next_link'] = 'Selanjutnya';
           $config['prev_link'] = 'Sebelumnya';
           $config['cur_page'] = $this->uri->segment(4);
           $this->paginationxx->initialize($config);

           $data['page_title'] = $this->lang->line('master_customersalesman');
           $data['iarea']='';
           $data['icustomer']='';
           $data['isalesman']='';
           $this->load->model('customersalesman/mmaster');
           $data['isi']=$this->mmaster->bacasemua($per,$cari,$config['per_page'],$this->uri->segment(4),$area1,$area2,$area3,$area4,$area5);

           $sess=$this->session->userdata('session_id');
            $id=$this->session->userdata('user_id');
            $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs   = pg_query($sql);
            if(pg_num_rows($rs)>0){
                while($row=pg_fetch_assoc($rs)){
                  $ip_address   = $row['ip_address'];
                  break;
                }
            }else{
              $ip_address='kosong';
            }
            $query  = pg_query("SELECT current_timestamp as c");
            while($row=pg_fetch_assoc($query)){
              $now    = $row['c'];
            }
            $pesan='Membuka data Customer Salesman ';
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now , $pesan );

           $this->load->view('customersalesman/vmainform', $data);
        }else{
           $this->load->view('awal/index.php');
        }
   }
   function simpan()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea      = $this->input->post('iarea', TRUE);
         $eareaname  = $this->input->post('eareaname', TRUE);
         $icustomer  = $this->input->post('icustomer', TRUE);
         $ecustomername    = $this->input->post('ecustomername', TRUE);
         $isalesman  = $this->input->post('isalesman', TRUE);
         $esalesmanname    = $this->input->post('esalesmanname', TRUE);
         $iproductgroup    = $this->input->post('iproductgroup', TRUE);
         $eproductgroupname   = $this->input->post('eproductgroupname', TRUE);
      $iperiode = $this->input->post('iperiode', TRUE);

         if
            (
                   (isset($iarea) && $iarea != '')
             && (isset($eareaname) && $eareaname != '')
             && (isset($icustomer) && $icustomer != '')
             && (isset($ecustomername) && $ecustomername != '')
             && (isset($isalesman) && $isalesman != '')
             && (isset($esalesmanname) && $esalesmanname != '')
               && (isset($iproductgroup) && $iproductgroup != '')
             && (isset($eproductgroupname) && $eproductgroupname != '')
          && (isset($iperiode) && $iperiode != '')
            )

         {
            $this->load->model('customersalesman/mmaster');
            //$cek_data = $this->mmaster->cek_data($icustomer,$iarea,$isalesman,$iproductgroup,$esalesmanname,$iperiode);
				//if($cek_data->num_rows() > 0){
				// 	echo "Data Sudah Ada !";
				// 	die();
				// }
          $this->mmaster->insert($icustomer,$iarea,$isalesman,$iproductgroup,$esalesmanname,$iperiode);

          $sess=$this->session->userdata('session_id');
          $id=$this->session->userdata('user_id');
          $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
          $rs   = pg_query($sql);
          if(pg_num_rows($rs)>0){
            while($row=pg_fetch_assoc($rs)){
              $ip_address   = $row['ip_address'];
              break;
            }
          }else{
            $ip_address='kosong';
          }
          $query  = pg_query("SELECT current_timestamp as c");
          while($row=pg_fetch_assoc($query)){
            $now    = $row['c'];
          }
          $pesan='Simpan Customer Salesman '.$iarea.' Salesman:'.$isalesman;
          $this->load->model('logger');
          $this->logger->write($id, $ip_address, $now , $pesan );

          $area1 = $this->session->userdata('i_area');
          $area2 = $this->session->userdata('i_area2');
          $area3 = $this->session->userdata('i_area3');
          $area4 = $this->session->userdata('i_area4');
          $area5 = $this->session->userdata('i_area5');
          $config['base_url'] = base_url().'index.php/customersalesman/cform/index/';
          $cari   = ($this->input->post('cari', FALSE));
          $per    = date('Ym');
          if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
               $query = $this->db->query("  select a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
                                               from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d
                                               where a.i_customer = b.i_customer
                                                  and a.i_area = c.i_area
                                                  and a.i_salesman = d.i_salesman
                                                  and a.i_area=b.i_area
                                        and a.e_periode='$per'",false);
          }else{
               $query = $this->db->query("  select a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
                                               from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d
                                               where a.i_customer = b.i_customer and a.i_area = c.i_area
                                                  and a.i_salesman = d.i_salesman
                                                  and a.i_area=b.i_area
                                                  and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                                  or a.i_area = '$area4' or a.i_area = '$area5') and a.e_periode='$per'",false);
          }

          $config['total_rows'] = $query->num_rows();
          $config['per_page'] = '10';
          $config['first_link'] = 'Awal';
          $config['last_link'] = 'Akhir';
          $config['next_link'] = 'Selanjutnya';
          $config['prev_link'] = 'Sebelumnya';
          $config['cur_page'] = $this->uri->segment(4);
          $this->paginationxx->initialize($config);

          $data['page_title'] = $this->lang->line('master_customersalesman');
          $data['iarea']='';
          $data['icustomer']='';
          $data['isalesman']='';
          $this->load->model('customersalesman/mmaster');
          $data['isi']=$this->mmaster->bacasemua($per,$cari,$config['per_page'],$this->uri->segment(4),$area1,$area2,$area3,$area4,$area5);
          $this->load->view('customersalesman/vmainform', $data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('master_customersalesman');
         $this->load->view('customersalesman/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function edit()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('master_customersalesman')." update";
         if( ($this->uri->segment(4))
          && ($this->uri->segment(5))
          && ($this->uri->segment(6))
          && ($this->uri->segment(7))
          && ($this->uri->segment(8))
            ){
            $iarea            = $this->uri->segment(4);
            $icustomer     = $this->uri->segment(5);
            $isalesman     = $this->uri->segment(6);
            $iproductgroup= $this->uri->segment(7);
            $per          = $this->uri->segment(8);
            $data['iarea']                = $iarea;
            $data['icustomer']         = $icustomer;
            $data['isalesman']         = $isalesman;
            $data['iproductgroup']  = $iproductgroup;
            $data['per']   = $per;
            $this->load->model('customersalesman/mmaster');
            $data['isi']=$this->mmaster->baca($per,$icustomer, $iarea, $isalesman,$iproductgroup);

            $sess=$this->session->userdata('session_id');
            $id=$this->session->userdata('user_id');
            $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs   = pg_query($sql);
            if(pg_num_rows($rs)>0){
              while($row=pg_fetch_assoc($rs)){
                $ip_address   = $row['ip_address'];
                break;
              }
            }else{
              $ip_address='kosong';
            }
            $query  = pg_query("SELECT current_timestamp as c");
            while($row=pg_fetch_assoc($query)){
              $now    = $row['c'];
            }
            $pesan='Membuka Update Customer Salesman Area : '.$iarea.' Customer:'.$icustomer.' Salesman asal : '.$isalesman;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now , $pesan );
            $this->load->view('customersalesman/vmainform',$data);
         }else{
            $this->load->view('customersalesman/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function update()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea      = $this->input->post('iarea', TRUE);
         $eareaname  = $this->input->post('eareaname', TRUE);
         $icustomer  = $this->input->post('icustomer', TRUE);
         $ecustomername    = $this->input->post('ecustomername', TRUE);
         $isalesman  = $this->input->post('isalesman', TRUE);
         $esalesmanname    = $this->input->post('esalesmanname', TRUE);
         $iproductgroup    = $this->input->post('iproductgroup', TRUE);
         $eproductgroupname   = $this->input->post('eproductgroupname', TRUE);
         $iperiode   = $this->input->post('iperiode', TRUE);
         $this->load->model('customersalesman/mmaster');
         $this->mmaster->delete($icustomer,$iarea,$isalesman,$iproductgroup);
         $this->mmaster->insert($icustomer,$iarea,$isalesman,$iproductgroup,$esalesmanname,$iperiode);

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs   = pg_query($sql);
        if(pg_num_rows($rs)>0){
          while($row=pg_fetch_assoc($rs)){
            $ip_address   = $row['ip_address'];
            break;
          }
        }else{
          $ip_address='kosong';
        }
        $query  = pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
          $now    = $row['c'];
        }
        $pesan='Update Customer Salesman ke Kode Salesman :'.$isalesman.' Periode :'.$iperiode;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

        $area1 = $this->session->userdata('i_area');
        $area2 = $this->session->userdata('i_area2');
        $area3 = $this->session->userdata('i_area3');
        $area4 = $this->session->userdata('i_area4');
        $area5 = $this->session->userdata('i_area5');
        $config['base_url'] = base_url().'index.php/customersalesman/cform/index/';
        $cari   = ($this->input->post('cari', FALSE));
        $per    = date('Ym');
        if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
             $query = $this->db->query("  select a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
                                             from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d
                                             where a.i_customer = b.i_customer
                                                and a.i_area = c.i_area
                                                and a.i_salesman = d.i_salesman
                                                and a.i_area=b.i_area
                                      and a.e_periode='$per'",false);
        }else{
             $query = $this->db->query("  select a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
                                             from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d
                                             where a.i_customer = b.i_customer and a.i_area = c.i_area
                                                and a.i_salesman = d.i_salesman
                                                and a.i_area=b.i_area
                                                and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                                or a.i_area = '$area4' or a.i_area = '$area5') and a.e_periode='$per'",false);
        }

        $config['total_rows'] = $query->num_rows();
        $config['per_page'] = '10';
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = 'Selanjutnya';
        $config['prev_link'] = 'Sebelumnya';
        $config['cur_page'] = $this->uri->segment(4);
        $this->paginationxx->initialize($config);

        $data['page_title'] = $this->lang->line('master_customersalesman');
        $data['iarea']='';
        $data['icustomer']='';
        $data['isalesman']='';
        $this->load->model('customersalesman/mmaster');
        $data['isi']=$this->mmaster->bacasemua($per,$cari,$config['per_page'],$this->uri->segment(4),$area1,$area2,$area3,$area4,$area5);
        $this->load->view('customersalesman/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function delete()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $iarea            = $this->uri->segment(4);
         $icustomer     = $this->uri->segment(5);
         $isalesman     = $this->uri->segment(6);
         $iproductgroup= $this->uri->segment(7);
         $this->load->model('customersalesman/mmaster');
         $this->mmaster->delete($icustomer, $iarea, $isalesman, $iproductgroup);
         $sess=$this->session->userdata('session_id');
          $id=$this->session->userdata('user_id');
          $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
          $rs   = pg_query($sql);
          if(pg_num_rows($rs)>0){
            while($row=pg_fetch_assoc($rs)){
              $ip_address   = $row['ip_address'];
              break;
            }
          }else{
            $ip_address='kosong';
          }
          $query  = pg_query("SELECT current_timestamp as c");
          while($row=pg_fetch_assoc($query)){
            $now    = $row['c'];
          }
          $pesan='Delete Data Customer Salesman '.$iarea.' Customer :'.$icustomer;
          $this->load->model('logger');
          $this->logger->write($id, $ip_address, $now, $pesan);

         $config['base_url'] = base_url().'index.php/customersalesman/cform/index/';
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $query = $this->db->query("   select * from tr_customer_salesman
                                 where upper(e_salesman_name) like '%$cari%' or upper(i_customer) like '%$cari%'
                              or upper(i_salesman) like '%$cari%' ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(4);
         $this->paginationxx->initialize($config);

         $data['page_title'] = $this->lang->line('master_customersalesman');
         $data['iarea']    ='';
         $data['icustomer']   ='';
         $data['isalesman']   ='';
         $this->load->model('customersalesman/mmaster');
         $data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
         $this->load->view('customersalesman/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function customerarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $baris=$this->uri->segment(4);
         $config['base_url'] = base_url().'index.php/customersalesman/cform/customerarea/'.$baris.'/sikasep/';
        /* $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);*/
         $query = $this->db->query("   select * from tr_customer_area
                     inner join tr_customer on (tr_customer_area.i_customer = tr_customer.i_customer)
                     inner join tr_area on (tr_customer_area.i_area = tr_area.i_area)  ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('customersalesman/mmaster');
         $data['baris']=$baris;
         $data['cari']='';
         $data['page_title'] = $this->lang->line('list_customerarea');
         $data['isi']=$this->mmaster->bacacustomerarea($config['per_page'],$this->uri->segment(6));
         $this->load->view('customersalesman/vlistcustomerarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function salesman()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $baris=$this->uri->segment(4);
         $config['base_url'] = base_url().'index.php/customersalesman/cform/salesman/'.$baris.'/sikasep/';
        /* $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);*/
         $query = $this->db->query(" select * from tr_salesman ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);

         $this->load->model('customersalesman/mmaster');
         $data['baris']=$baris;
         $data['cari']='';
         $data['page_title'] = $this->lang->line('list_salesman');
         $data['isi']=$this->mmaster->bacasalesman($config['per_page'],$this->uri->segment(6));
         $this->load->view('customersalesman/vlistsalesman', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cari()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
        $area1 = $this->session->userdata('i_area');
           $area2 = $this->session->userdata('i_area2');
           $area3 = $this->session->userdata('i_area3');
           $area4 = $this->session->userdata('i_area4');
           $area5 = $this->session->userdata('i_area5');
           /*$config['base_url'] = base_url().'index.php/customersalesman/cform/index/';
           $cari = strtoupper($this->input->post('cari', FALSE));*/

           $cari=($this->input->post("cari",false));
          if($cari=='')$cari=$this->uri->segment(4);
          if($cari=='zxvf'){
            $cari='';
            $config['base_url'] = base_url().'index.php/customersalesman/cform/cari/zxvf/';
          }else{
            $config['base_url'] = base_url().'index.php/customersalesman/cform/cari/'.$cari.'/';
          }

        $per    = date('Ym');
        if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
             $query = $this->db->query("  select a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
                                             from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d
                                             where a.i_customer = b.i_customer and a.i_area = c.i_area and a.i_salesman = d.i_salesman
                                             and (upper(a.e_salesman_name) ilike '%$cari%' or upper(a.i_customer) ilike '%$cari%'
                                             or upper(a.i_salesman) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%')
                                      and a.e_periode='$per'",false);
        }else{
             $query = $this->db->query("  select a.*, b.e_customer_name, c.e_area_name, d.e_salesman_name
                                             from tr_customer_salesman a, tr_customer b, tr_area c, tr_salesman d
                                             where a.i_customer = b.i_customer and a.i_area = c.i_area and a.i_salesman = d.i_salesman
                                             and (upper(a.e_salesman_name) ilike '%$cari%' or upper(a.i_customer) ilike '%$cari%'
                                             or upper(a.i_salesman) ilike '%$cari%' or upper(b.e_customer_name) ilike '%$cari%')
                                      and (c.i_area = '$area1' or c.i_area = '$area2' or c.i_area = '$area3'
                                      or c.i_area = '$area4' or c.i_area = '$area5') and a.e_periode='$per'",false);
        }
           $config['total_rows'] = $query->num_rows();
           $config['per_page'] = '10';
           $config['first_link'] = 'Awal';
           $config['last_link'] = 'Akhir';
           $config['next_link'] = 'Selanjutnya';
           $config['prev_link'] = 'Sebelumnya';
           $config['cur_page'] = $this->uri->segment(5);
           $this->paginationxx->initialize($config);
           $this->load->model('customersalesman/mmaster');
           $data['isi']    =$this->mmaster->cari($per,$cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
           $data['page_title']   =$this->lang->line('master_customersalesman');
           $data['iarea']     ='';
           $data['icustomer'] ='';
           $data['isalesman'] ='';
            $this->load->view('customersalesman/vmainform',$data);
        }else{
           $this->load->view('awal/index.php');
        }
   }
   function carisalesman()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
        /* $config['base_url'] = base_url().'index.php/customersalesman/cform/salesman/index/';
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);*/

         $baris   = $this->input->post('baris', FALSE);
        if($baris=='')$baris=$this->uri->segment(4);
        $cari   = ($this->input->post('cari', FALSE));
        if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
        if($cari!='sikasep')
          $config['base_url'] = base_url().'index.php/customersalesman/cform/carisalesman/'.$baris.'/'.$cari.'/';
            else
          $config['base_url'] = base_url().'index.php/customersalesman/cform/carisalesman/'.$baris.'/sikasep/';


         $query = $this->db->query("   select * from tr_salesman
                     where upper(e_salesman_name) ilike '%$cari%' or upper(i_salesman) ilike '%$cari%' ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('customersalesman/mmaster');
         $data['baris']=$baris;
         $data['cari']=$cari;
         $data['page_title'] = $this->lang->line('list_salesman');
         $data['isi']=$this->mmaster->carisalesman($cari,$config['per_page'],$this->uri->segment(6));
         $this->load->view('customersalesman/vlistsalesman', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function caricustomerarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
        /* $config['base_url'] = base_url().'index.php/customersalesman/cform/customerarea/index/';
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);*/

          $baris   = $this->input->post('baris', FALSE);
        if($baris=='')$baris=$this->uri->segment(4);
        $cari   = ($this->input->post('cari', FALSE));
        if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
        if($cari!='sikasep')
          $config['base_url'] = base_url().'index.php/customersalesman/cform/caricustomerarea/'.$baris.'/'.$cari.'/';
            else
          $config['base_url'] = base_url().'index.php/customersalesman/cform/caricustomerarea/'.$baris.'/sikasep/';

         $query = $this->db->query("   select * from tr_customer_area
                     inner join tr_customer on (tr_customer_area.i_customer = tr_customer.i_customer)
                     inner join tr_area on (tr_customer_area.i_area = tr_area.i_area)
                     where upper(tr_customer_area.i_customer) ilike '%$cari%' or upper(tr_customer_area.i_area) ilike '%$cari%'
                     or upper(tr_customer.e_customer_name) ilike '%$cari%' or upper(tr_area.e_area_name) ilike '%$cari%' ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('customersalesman/mmaster');
         $data['baris']=$baris;
         $data['cari']=$cari;
         $data['page_title'] = $this->lang->line('list_customerarea');
         $data['isi']=$this->mmaster->caricustomerarea($cari,$config['per_page'],$this->uri->segment(6));
         $this->load->view('customersalesman/vlistcustomerarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function productgroup()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/customersalesman/cform/productgroup/index/';
         $cari = strtoupper($this->input->post('cari', FALSE));
         $query= $this->db->query(" select * from tr_product_group
                                                   where upper(e_product_groupname) like '%$cari%' or upper(i_product_group) like '%$cari%' ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('customersalesman/mmaster');
         $data['page_title'] = $this->lang->line('list_productgroup');
         $data['isi']=$this->mmaster->bacaproductgroup($cari,$config['per_page'],$this->uri->segment(5));
         $this->load->view('customersalesman/vlistproductgroup', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cariproductgroup()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu30')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/customersalesman/cform/productgroup/index/';
         $cari = strtoupper($this->input->post('cari', FALSE));
         $query= $this->db->query(" select * from tr_product_group
                                                   where upper(e_product_groupname) like '%$cari%' or upper(i_product_group) like '%$cari%' ",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('customersalesman/mmaster');
         $data['page_title'] = $this->lang->line('list_productgroup');
         $data['isi']=$this->mmaster->bacaproductgroup($cari,$config['per_page'],$this->uri->segment(5));
         $this->load->view('customersalesman/vlistproductgroup', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
