<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu144')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
			){
			$data['page_title'] = $this->lang->line('kendaraan');
			$this->load->model('kendaraan/mmaster');
			$data['ikendaraan']='';
			$data['iperiode']='';
			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Input Data Kendaraan";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);
			$this->load->view('kendaraan/vmainform', $data);
	}else{
		$this->load->view('awal/index.php');
	}
}
function insert_fail()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$data['page_title'] = $this->lang->line('kendaraan');
	$this->load->view('kendaraan/vinsert_fail',$data);
}else{
	$this->load->view('awal/index.php');
}
}
function edit()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kendaraan')." update";

			$data['iperiode'] 		= $this->uri->segment(4);
			$data['ikendaraan']		= str_replace('%20',' ',$this->uri->segment(5));
			$data['i_kendaraan_asuransi'] 	= $this->uri->segment(6);
			$iperiode 				= $this->uri->segment(4);
			$ikendaraan				= str_replace('%20',' ',$this->uri->segment(5));
			$i_kendaraan_asuransi	= $this->uri->segment(6);

			$this->load->model("kendaraan/mmaster");
			$data['isi']=$this->mmaster->baca($iperiode,$ikendaraan,$i_kendaraan_asuransi);

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Edit Data Kendaraan";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);
			$this->load->view('kendaraan/vformupdate',$data);

}else{
	$this->load->view('awal/index.php');
}
}
function platnomor()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/kendaraan/cform/platnomor/index/';
	$query = $this->db->query("select a.*, b.e_area_name, c.e_kendaraan_jenis, d.e_kendaraan_bbm, a.d_pajak from tr_kendaraan a, tr_area b, tr_kendaraan_jenis c, tr_kendaraan_bbm d
		where
		a.i_area = b.i_area
		and a.i_kendaraan_jenis = c.i_kendaraan_jenis
		and a.i_kendaraan_bbm = d.i_kendaraan_bbm",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);

	$this->load->model('kendaraan/mmaster');
	$data['page_title'] = $this->lang->line('kendaraan');
	$data['isi']=$this->mmaster->bacaplatnomor($config['per_page'],$this->uri->segment(5));
	$this->load->view('kendaraan/listplatnomor', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function cariplatnomor()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/kendaraan/cform/platnomor/index/';
	$cari 	= $this->input->post('cari', FALSE);
	$cari	= strtoupper($cari);
	$query 	= $this->db->query("select a.*, b.e_area_name, c.e_kendaraan_jenis, d.e_kendaraan_bbm, a.d_pajak from tr_kendaraan a, tr_area b, tr_kendaraan_jenis c, tr_kendaraan_bbm d
		where
		a.i_area = b.i_area
		and a.i_kendaraan_jenis = c.i_kendaraan_jenis
		and a.i_kendaraan_bbm = d.i_kendaraan_bbm
		and (upper(a.i_kendaraan) like '%$cari%')",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);
	$this->load->model('kendaraan/mmaster');
	$data['page_title'] = $this->lang->line('kendaraan');
	$data['isi']=$this->mmaster->cariplatnomor($cari,$config['per_page'],$this->uri->segment(5));
	$this->load->view('kendaraan/listplatnomor', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function area()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/kendaraan/cform/area/index/';
	$query = $this->db->query("select * from tr_area",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);

	$this->load->model('kendaraan/mmaster');
	$data['page_title'] = $this->lang->line('list_area');
	$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
	$this->load->view('kendaraan/vlistarea', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function cariarea()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/kendaraan/cform/area/index/';
	$cari 	= $this->input->post('cari', FALSE);
	$cari	= strtoupper($cari);
	$query 	= $this->db->query("select * from tr_area
		where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);
	$this->load->model('kendaraan/mmaster');
	$data['page_title'] = $this->lang->line('list_area');
	$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5));
	$this->load->view('kendaraan/vlistarea', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function jeniskendaraan()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/kendaraan/cform/jeniskendaraan/index/';
	$query = $this->db->query("select * from tr_kendaraan_jenis",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);

	$this->load->model('kendaraan/mmaster');
	$data['page_title'] = $this->lang->line('list_jeniskendaraan');
	$data['isi']=$this->mmaster->bacajeniskendaraan($config['per_page'],$this->uri->segment(5));
	$this->load->view('kendaraan/vlistjeniskendaraan', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function carijeniskendaraan()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/kendaraan/cform/jeniskendaraan/index/';
	$cari 	= $this->input->post('cari', FALSE);
	$cari	= strtoupper($cari);
	$query 	= $this->db->query("select * from tr_kendaraan_jenis
		where (upper(i_kendaraan_jenis) like '%$cari%' or upper(e_kendaraan_jenis) like '%$cari%')",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);
	$this->load->model('kendaraan/mmaster');
	$data['page_title'] = $this->lang->line('list_jeniskendaraan');
	$data['isi']=$this->mmaster->carijeniskendaraan($cari,$config['per_page'],$this->uri->segment(5));
	$this->load->view('kendaraan/vlistjeniskendaraan', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function asuransikendaraan()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/kendaraan/cform/asuransikendaraan/index/';
	$query = $this->db->query("select * from tr_kendaraan_asuransi",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);

	$this->load->model('kendaraan/mmaster');
	$data['page_title'] = $this->lang->line('list_asuransikendaraan');
	$data['isi']=$this->mmaster->bacaasuransikendaraan($config['per_page'],$this->uri->segment(5));
	$this->load->view('kendaraan/vlistasuransikendaraan', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function cariasuransikendaraan()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/kendaraan/cform/asuransikendaraan/index/';
	$cari 	= $this->input->post('cari', FALSE);
	$cari	= strtoupper($cari);
	$query 	= $this->db->query("select * from tr_kendaraan_asuransi
		where (upper(i_kendaraan_asuransi) like '%$cari%' or upper(e_kendaraan_asuransi) like '%$cari%')",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);
	$this->load->model('kendaraan/mmaster');
	$data['page_title'] = $this->lang->line('list_asuransikendaraan');
	$data['isi']=$this->mmaster->cariasuransikendaraan($cari,$config['per_page'],$this->uri->segment(5));
	$this->load->view('kendaraan/vlistasuransikendaraan', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function bbmkendaraan()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/kendaraan/cform/bbmkendaraan/index/';
	$query = $this->db->query("select * from tr_kendaraan_bbm",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);

	$this->load->model('kendaraan/mmaster');
	$data['page_title'] = $this->lang->line('list_bbmkendaraan');
	$data['isi']=$this->mmaster->bacabbmkendaraan($config['per_page'],$this->uri->segment(5));
	$this->load->view('kendaraan/vlistbbmkendaraan', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function caribbmkendaraan()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/kendaraan/cform/bbmkendaraan/index/';
	$cari 	= $this->input->post('cari', FALSE);
	$cari	= strtoupper($cari);
	$query 	= $this->db->query("select * from tr_kendaraan_bbm
		where (upper(i_kendaraan_bbm) like '%$cari%' or upper(e_kendaraan_bbm) like '%$cari%')",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);
	$this->load->model('kendaraan/mmaster');
	$data['page_title'] = $this->lang->line('list_bbmkendaraan');
	$data['isi']=$this->mmaster->caribbmkendaraan($cari,$config['per_page'],$this->uri->segment(5));
	$this->load->view('kendaraan/vlistbbmkendaraan', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function update()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$iperiode		= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
	$ikendaraan		= $this->input->post('ikendaraan', TRUE);
	$ikendaraan		= strtoupper($ikendaraan);
	$iarea			= $this->input->post('iarea', TRUE);
	$ikendaraanjenis= $this->input->post('ikendaraanjenis', TRUE);
	$ikendaraanbbm	= $this->input->post('ikendaraanbbm', TRUE);
	$epengguna		= $this->input->post('epengguna', TRUE);
	$dpajak			= $this->input->post('dpajak', TRUE);
	$e_pemilik_kendaraan		= $this->input->post('e_pemilik_kendaraan', TRUE);
	$e_posisi_kendaraan			= $this->input->post('e_posisi_kendaraan', TRUE);
	$d_serah_terima_kendaraan	= $this->input->post('d_serah_terima_kendaraan', TRUE);
	$e_merek_kendaraan			= $this->input->post('e_merek_kendaraan', TRUE);
	$e_jenis_kendaraan			= $this->input->post('e_jenis_kendaraan', TRUE);
	$e_warna_kendaraan			= $this->input->post('e_warna_kendaraan', TRUE);
	$e_nomor_rangka				= $this->input->post('e_nomor_rangka', TRUE);
	$e_nomor_mesin				= $this->input->post('e_nomor_mesin', TRUE);
	$d_pajak_1_tahun			= $this->input->post('d_pajak_1_tahun', TRUE);
	$d_pajak_5_tahun			= $this->input->post('d_pajak_5_tahun', TRUE);
	$ikendaraanasuransi			= $this->input->post('ikendaraanasuransi', TRUE);
	$d_bayar_asuransi			= $this->input->post('d_bayar_asuransi', TRUE);
	$e_nomor_polisasuransi		= $this->input->post('e_nomor_polisasuransi', TRUE);
	$e_tlo						= $this->input->post('e_tlo', TRUE);
	$d_periode_awalasuransi		= $this->input->post('d_periode_awalasuransi', TRUE);
	$d_periode_akhirasuransi	= $this->input->post('d_periode_akhirasuransi', TRUE);
	$e_tahun					= $this->input->post('e_tahun', TRUE);
	$e_desc						= $this->input->post('e_desc', TRUE);

	if($dpajak!=''){
		$tmp=explode("-",$dpajak);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$dpajak=$th."-".$bl."-".$hr;
	}else{
		$dpajak=null;
	}

	if($d_serah_terima_kendaraan!=''){
		$tmp=explode("-",$d_serah_terima_kendaraan);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$d_serah_terima_kendaraan=$th."-".$bl."-".$hr;
	}else{
		$d_serah_terima_kendaraan=null;
	}
	if($d_pajak_1_tahun!=''){
		$tmp=explode("-",$d_pajak_1_tahun);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$d_pajak_1_tahun=$th."-".$bl."-".$hr;
	}else{
		$d_pajak_1_tahun=null;
	}
	if($d_pajak_5_tahun!=''){
		$tmp=explode("-",$d_pajak_5_tahun);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$d_pajak_5_tahun=$th."-".$bl."-".$hr;
	}else{
		$d_pajak_5_tahun=null;
	}
	if($d_bayar_asuransi!=''){
		$tmp=explode("-",$d_bayar_asuransi);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$d_bayar_asuransi=$th."-".$bl."-".$hr;
	}else{
		$d_bayar_asuransi=null;
	}
	if($d_periode_awalasuransi!=''){
		$tmp=explode("-",$d_periode_awalasuransi);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$d_periode_awalasuransi=$th."-".$bl."-".$hr;
	}else{
		$d_periode_awalasuransi=null;
	}
	if($d_periode_akhirasuransi!=''){
		$tmp=explode("-",$d_periode_akhirasuransi);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$d_periode_akhirasuransi=$th."-".$bl."-".$hr;
	}else{
		$d_periode_akhirasuransi=null;
	}

	if (
		(isset($iperiode) && $iperiode != '') && 
		(isset($ikendaraan) && $ikendaraan != '') 
		)
	{

		$this->load->model('kendaraan/mmaster');
		$this->db->trans_begin();
		$cek=$this->mmaster->cek($iperiode,$ikendaraan);
		if($cek){							
			$this->mmaster->updateheaderkendaraan($ikendaraan,$iarea,$ikendaraanjenis,$ikendaraanbbm,$dpajak);
			$this->mmaster->updateheaderkendaraanpengguna($iperiode,$ikendaraan,$epengguna);
			$this->mmaster->updatedetailkendaraan($iperiode,$ikendaraan,$e_pemilik_kendaraan,$e_posisi_kendaraan,
				$d_serah_terima_kendaraan,$e_merek_kendaraan,$e_jenis_kendaraan,$e_warna_kendaraan,$e_nomor_rangka,$e_nomor_mesin,
				$d_pajak_1_tahun,$d_pajak_5_tahun,$ikendaraanasuransi,$d_bayar_asuransi,$e_nomor_polisasuransi,$e_tlo,$d_periode_awalasuransi,$d_periode_akhirasuransi,
				$e_tahun,$e_desc);
			$nomor=$ikendaraan." - ".$iperiode." - ".$epengguna;
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Update Kendaraan Plat:'.$ikendaraan.' Periode:'.$iperiode.' Area:'.$iarea;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan ); 

				$data['sukses']			= true;
				$data['inomor']			= $nomor;
				$this->load->view('nomor',$data);
			}
		}else{
			$nomor="Periode ".$iperiode." - ".$ikendaraan." sudah ada, untuk mengubah lewat menu update kendaraan";
		}
	}

}else{
	$this->load->view('awal/index.php');

}
}
function simpan()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$iperiode					= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
	$ikendaraan					= $this->input->post('ikendaraan', TRUE);
	$ikendaraan					= strtoupper($ikendaraan);
	$iarea						= $this->input->post('iarea', TRUE);
	$ikendaraanjenis 			= $this->input->post('ikendaraanjenis', TRUE);
	$ikendaraanbbm				= $this->input->post('ikendaraanbbm', TRUE);
	$epengguna					= $this->input->post('epengguna', TRUE);
	$dpajak						= $this->input->post('dpajak', TRUE);
	$e_pemilik_kendaraan		= $this->input->post('e_pemilik_kendaraan', TRUE);
	$e_posisi_kendaraan			= $this->input->post('e_posisi_kendaraan', TRUE);
	$d_serah_terima_kendaraan	= $this->input->post('d_serah_terima_kendaraan', TRUE);
	$e_merek_kendaraan			= $this->input->post('e_merek_kendaraan', TRUE);
	$e_jenis_kendaraan			= $this->input->post('e_jenis_kendaraan', TRUE);
	$e_warna_kendaraan			= $this->input->post('e_warna_kendaraan', TRUE);
	$e_nomor_rangka				= $this->input->post('e_nomor_rangka', TRUE);
	$e_nomor_mesin				= $this->input->post('e_nomor_mesin', TRUE);
	$d_pajak_1_tahun			= $this->input->post('d_pajak_1_tahun', TRUE);
	$d_pajak_5_tahun			= $this->input->post('d_pajak_5_tahun', TRUE);
	$ikendaraanasuransi			= $this->input->post('ikendaraanasuransi', TRUE);
	$d_bayar_asuransi			= $this->input->post('d_bayar_asuransi', TRUE);
	$e_nomor_polisasuransi		= $this->input->post('e_nomor_polisasuransi', TRUE);
	$e_tlo						= $this->input->post('e_tlo', TRUE);
	$d_periode_awalasuransi		= $this->input->post('d_periode_awalasuransi', TRUE);
	$d_periode_akhirasuransi	= $this->input->post('d_periode_akhirasuransi', TRUE);
	$e_tahun					= $this->input->post('e_tahun', TRUE);
	$e_desc						= $this->input->post('e_desc', TRUE);

	if($dpajak!=''){
		$tmp=explode("-",$dpajak);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$dpajak=$th."-".$bl."-".$hr;
	}else{
		$dpajak=null;
	}
	if($d_serah_terima_kendaraan!=''){
		$tmp=explode("-",$d_serah_terima_kendaraan);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$d_serah_terima_kendaraan=$th."-".$bl."-".$hr;
	}else{
		$d_serah_terima_kendaraan=null;
	}
	if($d_pajak_1_tahun!=''){
		$tmp=explode("-",$d_pajak_1_tahun);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$d_pajak_1_tahun=$th."-".$bl."-".$hr;
	}else{
		$d_pajak_1_tahun=null;
	}
	if($d_pajak_5_tahun!=''){
		$tmp=explode("-",$d_pajak_5_tahun);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$d_pajak_5_tahun=$th."-".$bl."-".$hr;
	}else{
		$d_pajak_5_tahun=null;
	}
	if($d_bayar_asuransi!=''){
		$tmp=explode("-",$d_bayar_asuransi);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$d_bayar_asuransi=$th."-".$bl."-".$hr;
	}else{
		$d_bayar_asuransi=null;
	}
	if($d_periode_awalasuransi!=''){
		$tmp=explode("-",$d_periode_awalasuransi);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$d_periode_awalasuransi=$th."-".$bl."-".$hr;
	}else{
		$d_periode_awalasuransi=null;
	}
	if($d_periode_akhirasuransi!=''){
		$tmp=explode("-",$d_periode_akhirasuransi);
		$th=$tmp[2];
		$bl=$tmp[1];
		$hr=$tmp[0];
		$d_periode_akhirasuransi=$th."-".$bl."-".$hr;
	}else{
		$d_periode_akhirasuransi=null;
	}
	if (
		(isset($iperiode) && $iperiode != '') && 
		(isset($ikendaraan) && $ikendaraan != '') 
		)
	{
		$this->load->model('kendaraan/mmaster');
		$this->db->trans_begin();
		$cek=$this->mmaster->cek($iperiode,$ikendaraan);
		if(!$cek){							
			$this->mmaster->insertheaderkendaraan($ikendaraan,$iarea,$ikendaraanjenis,$ikendaraanbbm,$dpajak);
			$this->mmaster->insertheaderkendaraanpengguna($iperiode,$ikendaraan,$epengguna);
			$this->mmaster->insertdetailkendaraan($ikendaraan,$e_pemilik_kendaraan,$e_posisi_kendaraan,
				$d_serah_terima_kendaraan,$e_merek_kendaraan,$e_jenis_kendaraan,$e_warna_kendaraan,$e_nomor_rangka,$e_nomor_mesin,
				$d_pajak_1_tahun,$d_pajak_5_tahun,$ikendaraanasuransi,$d_bayar_asuransi,$e_nomor_polisasuransi,$e_tlo,$d_periode_awalasuransi,$d_periode_akhirasuransi,
				$e_tahun,$e_desc);
			$nomor=$ikendaraan." - ".$iperiode." - ".$epengguna;
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Input Kendaraan Plat:'.$ikendaraan.' Periode:'.$iperiode.' Area:'.$iarea;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan ); 

				$data['sukses']			= true;
				$data['inomor']			= $nomor;
				$this->load->view('nomor',$data);
			}
		}else{
			$nomor="Periode ".$iperiode." - ".$ikendaraan." sudah ada, untuk mengubah lewat menu update kendaraan";
		}
	}

}else{
	$this->load->view('awal/index.php');

}
}
function simpanservice()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){

		$i_kendaraan = $this->input->post('platnomor');
	$jml = $this->input->post('jml');
	$this->load->model('kendaraan/mmaster');
	$this->db->trans_begin();
	$d_service = $this->input->post('xtgl');
	$tmp=explode("-",$d_service);
	$th=$tmp[2];
	$bl=$tmp[1];
	$hr=$tmp[0];
	$d_service=$th."-".$bl."-".$hr;
	for ($i=1; $i <= $jml ; $i++) { 
		$n_olimesin_awal = $this->input->post('xkmawal'.$i);
		$n_olimesin_akhir = $this->input->post('xkmakhir'.$i);
		$n_olimesin_selisih = $this->input->post('xselisih'.$i);
		$n_qty = $this->input->post('xqty'.$i);
		$i_satuan = $this->input->post('xsatuan'.$i);
		$e_uraian = $this->input->post('xuraian'.$i);
		$e_keterangan = $this->input->post('xketerangan'.$i);
		$e_pelaksana = $this->input->post('xnama_bengkel'.$i);
		$v_harga = $this->input->post('xbiaya'.$i);
		$this->mmaster->insertservice($i_kendaraan,$d_service, $n_olimesin_awal, $n_olimesin_akhir, $n_olimesin_selisih, $n_qty, $i_satuan, $e_uraian, $e_keterangan, $e_pelaksana, $v_harga, $i);
	}
	if ($this->db->trans_status() === FALSE)
	{
		$this->db->trans_rollback();
	}else{
		$this->db->trans_commit();

		$sess=$this->session->userdata('session_id');
		$id=$this->session->userdata('user_id');
		$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		$rs		= pg_query($sql);
		if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
				$ip_address	  = $row['ip_address'];
				break;
			}
		}else{
			$ip_address='kosong';
		}
		$query 	= pg_query("SELECT current_timestamp as c");
		while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
		}
		$pesan='Input Kendaraan Service :'.$i_kendaraan.' Tanggal:'.$d_service;
		$this->load->model('logger');
		$this->logger->write($id, $ip_address, $now , $pesan ); 

		$data['sukses']			= true;
		$data['inomor']			= $pesan;
		$this->load->view('nomor',$data);
	}
	}else{
		$this->load->view('awal/index.php');
	}
}
function satuan()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/kendaraan/cform/satuan/index/';
	$query = $this->db->query("select * from tr_kendaraan_satuan",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$data['jml'] = $this->uri->segment(4);
	$this->pagination->initialize($config);

	$this->load->model('kendaraan/mmaster');
	$data['page_title'] = $this->lang->line('kendaraan');
	$data['isi']=$this->mmaster->bacapsatuan($config['per_page'],$this->uri->segment(5));
	$this->load->view('kendaraan/listsatuan', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function carisatuan()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu144')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/kendaraan/cform/satuan/index/';
	$cari 	= $this->input->post('cari', FALSE);
	$cari	= strtoupper($cari);
	$query 	= $this->db->query("select * from tr_kendaraan_satuan where  e_satuan like '%$cari%' or i_satuan like '%$cari%' order by i_satuan asc",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);
	$this->load->model('kendaraan/mmaster');
	$data['page_title'] = $this->lang->line('kendaraan');
	$data['isi']=$this->mmaster->carisatuan($cari,$config['per_page'],$this->uri->segment(5));
	$this->load->view('kendaraan/listsatuan', $data);
}else{
	$this->load->view('awal/index.php');
}
}
}
?>
