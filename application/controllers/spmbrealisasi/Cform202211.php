<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		  ($this->session->userdata('departement')=='7') && 
			($this->session->userdata('menu73')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spmbrealisasi/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$query = $this->db->query("	select i_spmb from tm_spmb 
							                    inner join tr_area on (tm_spmb.i_area=tr_area.i_area
							                    and (upper(tr_area.e_area_name) like '%$cari%' 
							                    or upper(tr_area.i_area) like '%$cari%'))
							                    where not tm_spmb.i_approve2 isnull
							                    and tm_spmb.i_store isnull and tm_spmb.i_store_location isnull
							                    and tm_spmb.f_spmb_cancel='f' and tm_spmb.f_spmb_close='f' and tm_spmb.f_spmb_acc='t'
							                    and upper(tm_spmb.i_spmb) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			
			$data['page_title'] = $this->lang->line('spmbrealisasi');
			$data['ispmb']='';
			$data['jmlitem'] = ''; 				
			$data['detail']='';
			$this->load->model('spmbrealisasi/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('spmbrealisasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('master_spmb');
			$this->load->view('spmbrealisasi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('spmbrealisasi');
			if($this->uri->segment(4)!=''){
				$ispmb = $this->uri->segment(4);
				$area = $this->uri->segment(5);
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ispmb' order by e_product_name asc ");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispmb'] = $ispmb;
				$this->load->model('spmbrealisasi/mmaster');
				$data['isi']=$this->mmaster->baca($ispmb,$area);
				$data['detail']=$this->mmaster->bacadetail($ispmb,$area);
				$data['page_title'] = $this->lang->line('spmbrealisasi');
		 		$this->load->view('spmbrealisasi/vmainform',$data);
			}else{
				$this->load->view('spmbrealisasi/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if ($this->session->userdata('logged_in')){
			$iarea					= $this->input->post('iarea', TRUE);
			$ispmb 					= $this->input->post('ispmb', TRUE);
			$istore					= 'AA';
			$istorelocation	= '01';
			$fspmbclose			= 'f';
			$fspmbcancel		= 'f';
			$jml						= $this->input->post('jml', TRUE);
			$this->db->trans_begin();
			$this->load->model('spmbrealisasi/mmaster');
			$this->mmaster->updateheader($ispmb,$istore,$istorelocation,$fspmbcancel,$fspmbclose);
			$langsung=true;
			for($i=1;$i<=$jml;$i++){
			  $iproduct				= $this->input->post('iproduct'.$i, TRUE);
			  $iproductgrade	= 'A';
			  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
			  $vunitprice			= $this->input->post('vproductmill'.$i, TRUE);
			  $vunitprice			= str_replace(',','',$vunitprice);
        $nacc 					= $this->input->post('nacc'.$i, TRUE);
			  $nacc 					= str_replace(',','',$nacc);
			  $norder					= $this->input->post('norder'.$i, TRUE);
			  $norder					= str_replace(',','',$norder);
			  $nstock					= $this->input->post('nstock'.$i, TRUE);
			  $nstock					= str_replace(',','',$nstock);
			  $nqtystock			= $this->input->post('nqtystock'.$i, TRUE);
			  $nqtystock			= str_replace(',','',$nqtystock);
			  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
        $eremark        = $this->input->post('eremark'.$i, TRUE);
#			  $this->mmaster->deletedetail($iproduct, $iproductgrade, $ispmb, $iproductmotif,$iarea);
#			  $this->mmaster->insertdetail($ispmb,$iproduct,$iproductgrade,$eproductname,$nacc,$vunitprice,$iproductmotif,$nstock,$iarea,$i,$norder,$eremark,$nqtystock);
			  $this->mmaster->updatedetail($ispmb,$iproduct,$iproductgrade,$iproductmotif,$nstock,$iarea);
			  if($nstock<$nacc) $langsung=false;
			}
			if($langsung==true) $this->mmaster->langsungclose($ispmb);
			if ( ($this->db->trans_status() === FALSE) )
			{
		    $this->db->trans_rollback();
			}else{
#		    $this->db->trans_rollback();
		    $this->db->trans_commit();

      $sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Realisasi SPMB Area:'.$iarea.' No:'.$ispmb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']	= true;
				$data['inomor']	= $ispmb;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if ($this->session->userdata('logged_in')){
			/*$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/spmbrealisasi/cform/index/';*/

			$cari=($this->input->post("cari",false));
          	if($cari=='')$cari=$this->uri->segment(4);
          	if($cari=='zxvf'){
            	$cari='';
            	$config['base_url'] = base_url().'index.php/spmbrealisasi/cform/cari/zxvf/';
          	}else{
            	$config['base_url'] = base_url().'index.php/spmbrealisasi/cform/cari/'.$cari.'/';
          	}

			$query = $this->db->query("  select * from tm_spmb 
							inner join tr_area on (tm_spmb.i_area=tr_area.i_area)
							where not tm_spmb.i_approve2 isnull
							and tm_spmb.i_store isnull and tm_spmb.i_store_location isnull
							and tm_spmb.f_spmb_cancel='f' and tm_spmb.f_spmb_close='f' and tm_spmb.f_spmb_acc='t'
							and (upper(tm_spmb.i_spmb) ilike '%$cari%' or upper(tr_area.e_area_name) ilike '%$cari%' or upper(tm_spmb.i_spmb_old) ilike '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spmbrealisasi/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('spmbrealisasi');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('spmbrealisasi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
