<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu433')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = 'Umur Hutang';
			$data['dfrom']	= '';
			$data['dto']	= '';
			$data['isupplier']	= '';
			$this->load->view('umurhutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu433')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier	= $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/umurhutang/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select z.i_supplier, z.e_supplier_name, z.d_dtap, z.i_dtap, z.v_netto-z.v_ppn as dpp, z.v_ppn, 
										z.v_netto, z.d_due_date, z.tanggalbayar,z.bankname, z.umurhutang, z.bukti, z.v_sisa from (
		                                    select a.i_supplier, b.e_supplier_name, a.d_dtap, a.i_dtap, a.v_netto-a.v_ppn as dpp, a.v_ppn, a.v_netto, a.d_due_date, 
		                                    d.d_alokasi as tanggalbayar,d.e_bank_name as bankname, d.d_alokasi-a.d_dtap as umurhutang, g.i_pvb as bukti, a.v_sisa
		                                    from tm_dtap a, tr_supplier b, tm_kbank c, tm_alokasi_bk d, tm_alokasi_bk_item e, tm_pv_item f, tm_pvb g 
		                                    where a.d_dtap between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy') 
		                                    and a.i_supplier = '$isupplier' 
		                                    and d.f_alokasi_cancel = 'false'
		                                    and c.f_kbank_cancel = 'false'
		                                    and a.i_supplier = b.i_supplier
		                                    and a.i_dtap = e.i_nota and a.i_supplier = d.i_supplier
		                                    and e.i_alokasi = d.i_alokasi and e.i_kbank = d.i_kbank and e.i_supplier = d.i_supplier and e.i_coa_bank = d.i_coa_bank
		                                    and d.i_kbank = c.i_kbank
		                                    and f.i_pv = g.i_pv
		                                    and e.i_kbank = f.i_kk

		                                    union all

		                                    select a.i_supplier, b.e_supplier_name, a.d_dtap, a.i_dtap, a.v_netto-a.v_ppn as dpp, a.v_ppn, a.v_netto, a.d_due_date, 
		                                    d.d_alokasi as tanggalbayar, 'KAS' as bankname, d.d_alokasi-a.d_dtap as umurhutang,f.i_pv as bukti, a.v_sisa
		                                    from tm_dtap a, tr_supplier b, tm_kb c, tm_alokasi_kb d, tm_alokasi_kb_item e, tm_pv_item f 
		                                    where a.d_dtap between to_date('$dfrom','dd-mm-yyyy') and to_date('$dto','dd-mm-yyyy')
		                                    and a.i_supplier = '$isupplier' 
		                                    and d.f_alokasi_cancel = 'false'
		                                    and c.f_kb_cancel = 'false'
		                                    and a.i_supplier = b.i_supplier
		                                    and a.i_dtap = e.i_nota and a.i_supplier = e.i_supplier
		                                    and e.i_alokasi = d.i_alokasi and e.i_kb = d.i_kb and e.i_supplier = d.i_supplier
		                                    and c.i_kb = d.i_kb
		                                    and e.i_kb = f.i_kk
			                            ) as z
			                            order by z.i_supplier, z.d_dtap",false);
			$config['total_rows'] = $query->num_rows(); 
			/*$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);*/

			$this->load->model('umurhutang/mmaster');
			$data['page_title'] = 'Umur Hutang';
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$cari);
			$this->load->view('umurhutang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu433')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/umurhutang/cform/supplier/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_supplier",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('umurhutang/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('umurhutang/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu433')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/umurhutang/cform/supplier/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_supplier
									   	where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('umurhutang/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('spb/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
