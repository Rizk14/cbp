<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu72')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spmbapprovefa/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query(" select a.*, b.e_area_name from tm_spmb a, tr_area b
										              where a.i_area=b.i_area and a.i_approve2 isnull and a.f_spmb_cancel='f' and f_spmb_acc='t'
										              and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										              or upper(a.i_spmb) like '%$cari%')",false);
#and not a.i_approve1 isnull 
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listspmb');
			$this->load->model('spmbapprovefa/mmaster');
			$data['ispmb']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('spmbapprovefa/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('listspmb');
			$this->load->view('spmbapprovefa/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function delete()
	{
		if ($this->session->userdata('logged_in')){
			$ispmb	= $this->input->post('ispmbdelete', TRUE);
			$this->load->model('spmbapprovefa/mmaster');
			$this->mmaster->delete($ispmb);
			$data['page_title'] = $this->lang->line('listspmb');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('spmbapprovefa/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if ($this->session->userdata('logged_in')){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/spmbapprovefa/cform/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_spmb a, tr_area b 
							                    where a.i_area=b.i_area and a.i_approve2 isnull and a.f_spmb_cancel='f' and f_spmb_acc='t'
							                    and (upper(a.i_spmb) like '%$cari%' or upper(b.e_area_name) like '%$cari%') 
							                    and a.i_area=b.i_area",false);
# and not a.i_approve1 isnull
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('spmbapprovefa/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('listspmb');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('spmbapprovefa/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('spmbapprovefa');
			if($this->uri->segment(4)){
				$ispmb = $this->uri->segment(4);
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ispmb'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispmb'] = $ispmb;
				$this->load->model('spmbapprovefa/mmaster');
				$data['isi']=$this->mmaster->baca($ispmb);
				$data['detail']=$this->mmaster->bacadetail($ispmb);
		 		$this->load->view('spmbapprovefa/vmainform',$data);
			}else{
				$this->load->view('spmbapprovefa/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if ($this->session->userdata('logged_in')){
			$ispmb 		= $this->input->post('ispmb', TRUE);
			$eapprove2	= $this->input->post('eapprove2',TRUE);
			if($eapprove2=='')
				$eapprove2=null;
			$user		=$this->session->userdata('user_id');
			$this->db->trans_begin();
			$this->load->model('spmbapprovefa/mmaster');
			$this->mmaster->approve($ispmb, $eapprove2,$user);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		=  $this->db->query($sql);
			  if($rs->num_rows>0){
				  foreach($rs->result() as $tes){
					  $ip_address	  = $tes->ip_address;
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }

			  $data['user']	= $this->session->userdata('user_id');
  #			$data['host']	= $this->session->userdata('printerhost');
			  $data['host']	= $ip_address;
			  $data['uri']	= $this->session->userdata('printeruri');
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Approve SPMB No:'.$ispmb;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $ispmb;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
