<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/reminderkontrak/cform/index/';
			$data['page_title'] = $this->lang->line('reminderkontrak');
			$this->load->model('reminderkontrak/mmaster');
    	    $now	  = "2017-02-19";
			$tmp=explode("-",$now);
			$thdatabase=$tmp[0];
			$bldatabase=$tmp[1];
			$hrdatabase=$tmp[2];

							//$d_join_date=$th."-".$bl."-".$hr;\

			$now2	  = date("Y-m-d");
			$tmp2=explode("-",$now2);
			$th2=$tmp2[0];
			$bl2=$tmp2[1];
			$hr2=$tmp2[2];
			 $th2;
			//echo "<br>";
				if($th2==$thdatabase){

					 $tahun=$thdatabase.'-'.$bldatabase.'-'.$hrdatabase;
					//echo "<br>";
					$tahunkurang=$thdatabase;
					//echo "<br>";
				 	$bulankurang=$bldatabase-1;
					//echo "<br>";
					 $tanggalreminder=$thdatabase.'-'.'0'.$bulankurang.'-'.$hrdatabase;

				}else if($th2<$thdatabase){
					
					 $tahun=$thdatabase.'-'.$bldatabase.'-'.$hrdatabase;
					//echo "<br>";
					$tahunkurang=$thdatabase-1;
					//echo "<br>";
				 	$bulankurang=$bldatabase-1;
					//echo "<br>";
					 $tanggalreminder=$thdatabase.'-'.'0'.$bulankurang.'-'.$hrdatabase;
				
				}else if($th2>$thdatabase){

					 $tahun=$thdatabase.'-'.$bldatabase.'-'.$hrdatabase;
					//echo "<br>";
					$tahunkurang=$thdatabase+1;
					//echo "<br>";
				 	$bulankurang=$bldatabase+1;
					//echo "<br>";
					 $tanggalreminder=$thdatabase.'-'.'0'.$bulankurang.'-'.$hrdatabase;
				
				}

		  	$query = $this->db->query("select * from tr_karyawan a 
			   left join tr_karyawan_item b on(a.i_nik=b.i_nik)
			   left join tr_karyawan_status c on(a.i_karyawan_status=c.i_karyawan_status)
			   left join tr_department d on(a.i_department=d.i_department)
			   where a.d_kontrak_ke2_akhir >= to_date('$tanggalreminder','yyyy-mm-dd') AND a.d_kontrak_ke2_akhir <= to_date('$tahun','yyyy-mm-dd')",false);
			$config['base_url'] = base_url().'index.php/reminderkontrak/cform/index/'.$tahun.'/'.$tanggalreminder.'/';
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('reminderkontrak');
			$this->load->model('reminderkontrak/mmaster');
			$data['isi']=$this->mmaster->bacasemua($tahun,$tanggalreminder,$config['per_page'],$this->uri->segment(6));
			$data['tahun']=$tahun;
			$data['tanggalreminder']=$tanggalreminder;
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Reminder Data Kendaraan Area '.$tahun;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('reminderkontrak/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari			= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
			  $query = $this->db->query("select i_kendaraan from tr_kendaraan
										  left join tr_kendaraan_jenis on (tr_kendaraan_jenis.i_kendaraan_jenis=tr_kendaraan.i_kendaraan_jenis)
										  left join tr_kendaraan_bbm on (tr_kendaraan_bbm.i_kendaraan_bbm=tr_kendaraan.i_kendaraan_bbm)
										  left join tr_area on (tr_area.i_area=tr_kendaraan.i_area)
										  left join tr_kendaraan_pengguna on (tr_kendaraan_pengguna.i_kendaraan=tr_kendaraan.i_kendaraan)
										  left join tr_kendaraan_item on (tr_kendaraan_item.i_kendaraan=tr_kendaraan.i_kendaraan)
 										  left join tr_kendaraan_asuransi on (tr_kendaraan_asuransi.i_kendaraan_asuransi=tr_kendaraan_item.i_kendaraan_asuransi)
										  where tr_kendaraan_pengguna.i_periode='$iperiode'",false);
      }else{
			  $query = $this->db->query("select i_kendaraan from tr_kendaraan
										  left join tr_kendaraan_jenis on (tr_kendaraan_jenis.i_kendaraan_jenis=tr_kendaraan.i_kendaraan_jenis)
										  left join tr_kendaraan_bbm on (tr_kendaraan_bbm.i_kendaraan_bbm=tr_kendaraan.i_kendaraan_bbm)
										  left join tr_area on (tr_area.i_area=tr_kendaraan.i_area)
										  left join tr_kendaraan_pengguna on (tr_kendaraan_pengguna.i_kendaraan=tr_kendaraan.i_kendaraan)
										  left join tr_kendaraan_item on (tr_kendaraan_item.i_kendaraan=tr_kendaraan.i_kendaraan)
 							 			  left join tr_kendaraan_asuransi on (tr_kendaraan_asuransi.i_kendaraan_asuransi=tr_kendaraan_item.i_kendaraan_asuransi)
										  where tr_kendaraan_pengguna.i_periode='$iperiode' and (tr_kendaraan.i_area = '$area1' or tr_kendaraan.i_area = '$area2' or
                      tr_kendaraan.i_area = '$area3' or tr_kendaraan.i_area = '$area4' or tr_kendaraan.i_area = '$area5')",false);
      }
			$config['base_url'] = base_url().'index.php/reminderkontrak/cform/view/'.$iperiode.'/';
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('reminderkontrak');
			$this->load->model('reminderkontrak/mmaster');
			$data['isi']=$this->mmaster->bacasemua($iperiode,$cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$data['cari'] = $cari;
			$data['iperiode'] = $iperiode;
			$data['area1']=$area1;

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Kendaraan Area '.$area1.' Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('reminderkontrak/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('reminderkontrak');
			$this->load->view('reminderkontrak/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode	= $this->uri->segment(4);
			$ikendaraan	= $this->uri->segment(5);
			$this->load->model('reminderkontrak/mmaster');
			$this->mmaster->delete($iperiode,$ikendaraan);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Hapus Kendaraan Plat:'.$ikendaraan.' Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$config['base_url'] = base_url().'index.php/reminderkontrak/cform/view/'.$iperiode.'/';
			$cari = $this->input->post('cari', FALSE);
			$cari= strtoupper($cari);
			$query = $this->db->query(" select * from tr_kendaraan
																	inner join tr_kendaraan_jenis on (tr_kendaraan_jenis.i_kendaraan_jenis=tr_kendaraan.i_kendaraan_jenis)
																	inner join tr_kendaraan_bbm on (tr_kendaraan_bbm.i_kendaraan_bbm=tr_kendaraan.i_kendaraan_bbm)
																	inner join tr_area on (tr_area.i_area=tr_kendaraan.i_area)
																	inner join tr_kendaraan_pengguna on (tr_kendaraan_pengguna.i_kendaraan=tr_kendaraan.i_kendaraan)
																	where tr_kendaraan_pengguna.i_periode='$iperiode'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('reminderkontrak');
//			$this->load->model('reminderpajakkendaraan/mmaster');
			$data['isi']			= $this->mmaster->bacasemua($iperiode,$cari,$config['per_page'],0);
			$data['cari'] 		= $cari;
			$data['iperiode'] = $iperiode;
			$this->load->view('reminderkontrak/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu587')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari			= strtoupper($this->input->post('cari'));
      $config['base_url'] = base_url().'index.php/reminderkontrak/cform/cari/';
			$data['page_title'] = $this->lang->line('reminderkontrak');
			$this->load->model('reminderkontrak/mmaster');
    	$now	  = date("Y-m-d H:i:s");
		  $tgl=explode(" ",$now);
		  $jm=$tgl[1];
		  $hr=$tgl[0];
		  $tgl2=explode("-",$hr);
		  $d=$tgl2[2];
		  $m=$tgl2[1];
		  $y=$tgl2[0];
		  $m3 = $m+1;
#      $m3=3;
		  if($m<9)
		  $m3='0'.$m3;
		  $m2=$y.$m3;
      $mm =$y.$m;
		  $data['m2']=$m2;
			  $query = $this->db->query("select distinct (tr_kendaraan.i_kendaraan) i_kendaraan from tr_kendaraan
										  inner join tr_kendaraan_jenis on (tr_kendaraan_jenis.i_kendaraan_jenis=tr_kendaraan.i_kendaraan_jenis)
										  inner join tr_kendaraan_bbm on (tr_kendaraan_bbm.i_kendaraan_bbm=tr_kendaraan.i_kendaraan_bbm)
										  inner join tr_kendaraan_pengguna on (tr_kendaraan_pengguna.i_kendaraan=tr_kendaraan.i_kendaraan)
										  inner join tr_area on (tr_area.i_area=tr_kendaraan.i_area)
										  where to_char(tr_kendaraan.d_pajak,'yyyymm')='$m2' and tr_kendaraan_pengguna.i_periode='$mm'",false);
			$config['base_url'] = base_url().'index.php/reminderkontrak/cform/index/'.$m2.'/';
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('reminderkontrak');
			$this->load->model('reminderkontrak/mmaster');
			$data['isi']=$this->mmaster->bacasemua($m2,$mm,$config['per_page'],$this->uri->segment(5));
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Reminder Data Kendaraan Area '.$m2;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('reminderkontrak/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
