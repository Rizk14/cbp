<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationxx');
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('dnalokasi');
         $data['dfrom']='';
         $data['dto']  ='';
         $data['isupplier']  ='';
         $data['isi']  ='';

         $this->load->view('dnalokasi/vmainform', $data);

      }elseif($this->session->userdata('logged_in')){
         $this->load->view('errorauthority');
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('listspb');
         $this->load->view('dnalokasi/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function delete()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $ispb = $this->input->post('ispbdelete', TRUE);
         $this->load->model('dnalokasi/mmaster');
         $this->mmaster->delete($ispb);
         $data['page_title'] = $this->lang->line('listspb');
         $data['ispb']='';
         $data['isi']=$this->mmaster->bacasemua();
         $this->load->view('dnalokasi/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function cari()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $config['base_url'] = base_url().'index.php/dnalokasi/cform/index/';
         $config['per_page'] = '10';
         $limo=$config['per_page'];
         $ofso=$this->uri->segment(4);
         if($ofso=='')
            $ofso=0;
         $query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
                              where a.i_customer=b.i_customer
                              and a.f_lunas = 'f'
                              and (upper(a.i_customer) like '%$cari%'
                                or upper(b.e_customer_name) like '%$cari%'
                                or upper(a.i_spb) like '%$cari%')",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(4);
         $this->pagination->initialize($config);
         $this->load->model('dnalokasi/mmaster');
         $data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
         $data['page_title'] = $this->lang->line('dnalokasi');
         $data['ispb']='';
         $data['ittb']='';
         $data['idtap']='';
         $this->load->view('dnalokasi/vmainform',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function approve()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['page_title'] = $this->lang->line('dnalokasi');
         if(
            ($this->uri->segment(11)!='') && ($this->uri->segment(5)!='')
           )
         {
#            $idt        = str_replace('tandagaring','/',$this->uri->segment(4));
            $esuppliername  = $this->uri->segment(4);
            $esuppliername  = str_replace('%20', ' ',$esuppliername);
            $idnap          = $this->uri->segment(5);
            $idnap          = str_replace('%20', '',$idnap);
            $vnetto         = $this->uri->segment(6);
            $dfrom          = $this->uri->segment(7);
            $dto            = $this->uri->segment(8);
            $ddnap          = $this->uri->segment(9);
            $vsisa          = $this->uri->segment(10);
            $isupplier      = $this->uri->segment(11);
//          $query = $this->db->query("select * from tm_nota_item where i_nota = '$idt' and i_area = '$iarea'");
//$query->num_rows();
            $data['idnap']        =$idnap;
            $data['vnetto']       =$vnetto;
            $data['esuppliername']=$esuppliername;
            $data['dfrom']        =$dfrom;
            $data['dto']          =$dto;
            $data['ddnap']        =$ddnap;
            $data['vsisa']        =$vsisa;
            $data['isupplier']    =$isupplier;
            $this->load->model('dnalokasi/mmaster');
            $data['isi']='';//$this->mmaster->baca($idt,$iarea);
            $data['detail']='';//$this->mmaster->bacadetail($idt,$iarea);
            $this->load->view('dnalokasi/vmainform',$data);
         }else{
            $this->load->view('dnalokasi/vinsert_fail',$data);
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function update()
    {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('dnalokasi/mmaster');
         $idnap         = $this->input->post('idnap', TRUE);
         $idnap          = str_replace('%20','',$idnap);
         $ddnap         = $this->input->post('ddnap', TRUE);
         $isupplier     = $this->input->post('isupplier', TRUE);
         $ijenisbayar   = $this->input->post('ijenisbayar',TRUE);
         $iarea         = '00';

         if($ddnap!=''){
            $tmp=explode("-",$ddnap);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $ddnap=$th."-".$bl."-".$hr;
            $thbl=$th.$bl;
            $iperiode=$th.$bl;
         }
         $dalokasi  = $this->input->post('dalokasi', TRUE);
         if($dalokasi!=''){
            $tmp=explode("-",$dalokasi);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dalokasi=$th."-".$bl."-".$hr;
 
         }
         $vjumlah    = $this->input->post('vjumlah',TRUE);
         $vjumlah    = str_replace(',','',$vjumlah);
         $vlebih     = $this->input->post('vlebih',TRUE);
         $vlebih        = str_replace(',','',$vlebih);
         $jml          = $this->input->post('jml', TRUE);
         $ada=false;
         if(($idnap!='') && ($ddnap!='') && ($ijenisbayar!='') && ($vjumlah!='') && ($vjumlah!='0') && ($jml!='0')){
        for($i=1;$i<=$jml;$i++){
          $vjumla = $this->input->post('vjumlah'.$i, TRUE);
          $vsisa  = $this->input->post('vsisa'.$i, TRUE);
          $vsisa  = $this->input->post('vsisa'.$i, TRUE);
          $vjumla = str_replace(',','',$vjumla);
          $vsisa  = str_replace(',','',$vsisa);
          $vsisa  = $vsisa-$vjumla;

        }
        if(!$ada){
          $this->db->trans_begin();
          $idtapx = $this->input->post('idtap1', TRUE);
          $ialokasi=$this->mmaster->runningnumberpl($iarea,$thbl);

########## Posting ###########
            $egirodescription="Alokasi Debet Nota no:".$idnap;
            $fclose     = 'f';
            $jml      = $this->input->post('jml', TRUE);
            for($i=1;$i<=$jml;$i++)
            {
              $idtap=$this->input->post('idtap'.$i, TRUE);
              $ireff=$ialokasi.'|'.$idtap;
              if($i==1){
                $this->mmaster->inserttransheader($ireff,$iarea,$egirodescription,$fclose,$ddnap);
#               $this->mmaster->updatepelunasan($ialokasi,$iarea,$ikn);
              }
              $vjumlahi    = $this->input->post('vjumlah'.$i, TRUE);
              $vjumlahi    = str_replace(',','',$vjumlahi);
              $accdebet    = HutangDagang;
              $namadebet  = $this->mmaster->namaacc($accdebet);
              $tmp        = $this->mmaster->carisaldo($accdebet,$iperiode);
              if($tmp) 
                $vsaldoaw1    = $tmp->v_saldo_awal;
              else 
                $vsaldoaw1    = 0;
              if($tmp) 
                $vmutasidebet1  = $tmp->v_mutasi_debet;
              else
                $vmutasidebet1  = 0;
              if($tmp) 
                $vmutasikredit1 = $tmp->v_mutasi_kredit;
              else
                $vmutasikredit1 = 0;
              if($tmp) 
                $vsaldoak1    = $tmp->v_saldo_akhir;
              else
                $vsaldoak1    = 0;
              if($ijenisbayar=='06'){
                $acckredit   = ByPromosi;
              }elseif($ijenisbayar=='07'){
                $acckredit   = ByExpedisi;
              }elseif($ijenisbayar=='08'){
                $acckredit   = ByAdmBank;
              }elseif($ijenisbayar=='09'){
                $acckredit   = ByAdmPenjualan;
              }elseif($ijenisbayar=='10'){
                $acckredit   = ByPembulatan;
              }
              $namakredit   = $this->mmaster->namaacc($acckredit);
              $saldoawkredit  = $this->mmaster->carisaldo($acckredit,$iperiode);
              if($tmp) 
                $vsaldoaw2    = $tmp->v_saldo_awal;
              else
                $vsaldoaw2    = 0;
              if($tmp) 
                $vmutasidebet2  = $tmp->v_mutasi_debet;
              else
                $vmutasidebet2  = 0;
              if($tmp) 
                $vmutasikredit2 = $tmp->v_mutasi_kredit;
              else
                $vmutasikredit2 = 0;
              if($tmp) 
                $vsaldoak2    = $tmp->v_saldo_akhir;
              else
                $vsaldoak2    = 0;
                $this->mmaster->inserttransitemdebet($accdebet,$ireff,$namadebet,'t','t',$isupplier,$egirodescription,$vjumlahi,$ddnap);
                $this->mmaster->updatesaldodebet($accdebet,$iperiode,$vjumlahi);
                $this->mmaster->inserttransitemkredit($acckredit,$ireff,$namakredit,'f','t',$isupplier,$egirodescription,$vjumlah,$ddnap);
                $this->mmaster->updatesaldokredit($acckredit,$iperiode,$vjumlahi);
                $this->mmaster->insertgldebet($accdebet,$ireff,$namadebet,'t',$iarea,$vjumlahi,$ddnap,$egirodescription);
                $this->mmaster->insertglkredit($acckredit,$ireff,$namakredit,'f',$iarea,$vjumlahi,$ddnap,$egirodescription);
            }
########## End of Posting ###########

          $this->mmaster->insertheader($ialokasi,$idnap,$isupplier,$ijenisbayar,$ddnap,$vjumlah,$vlebih,$iarea);
          $asal=0;
          $pengurang=$vjumlah-$vlebih;

          
         #   $this->mmaster->updatesaldo($group,$isu,$pengurang);
            $this->mmaster->updatedn($isupplier,$iarea,$idnap,$pengurang,$asal);
            for($i=1;$i<=$jml;$i++){
              $inota              = $this->input->post('idtap'.$i, TRUE);
              $ddtap              = $this->input->post('ddtap'.$i, TRUE);
              if($ddtap!=''){
                 $tmp=explode("-",$ddtap);
                 $th=$tmp[2];
                 $bl=$tmp[1];
                 $hr=$tmp[0];
                 $ddtap=$th."-".$bl."-".$hr;
              }
              $vjumlahi= $this->input->post('vjumlah'.$i, TRUE);
              $vsisa  = $this->input->post('vsisa'.$i, TRUE);
              $vsiso  = $this->input->post('vsisa'.$i, TRUE);
              $vjumlahi= str_replace(',','',$vjumlahi);
              $vsisa = str_replace(',','',$vsisa);
              $vsiso = str_replace(',','',$vsiso);
              $vsiso  = $vsiso-$vjumlahi;
              $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
              $eremark= $this->input->post('eremark'.$i,TRUE);
                $this->mmaster->insertdetail($ialokasi,$idnap,$iarea,$inota,$ddtap,$vjumlahi,$vsisa,$i,$ipelunasanremark,$eremark);
                $notaok=$this->mmaster->updatenota($inota,$iarea,$isupplier,$vjumlahi);
                if(!$notaok) break;
             }
          if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Input dnalokasi No:'.$ialokasi.' supplier:'.$isupplier;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );
                if($notaok)
                 $this->db->trans_commit();
               else
                 $this->db->trans_rollback();                
                 $data['sukses'] = true;
                 $data['inomor'] = $ialokasi;
                 $this->load->view('nomor',$data);
              }
        }
         }
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function edit()
   {
      if ( (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $isupplier   = $this->session->userdata('i_supplier');
         $data['page_title'] = $this->lang->line('bankmultoutnalloc')." update";
         if(
            ($this->uri->segment(4)) && ($this->uri->segment(5))
           )
         {
            $ialokasi = $this->uri->segment(4);
            $ikbank   = $this->uri->segment(5);
            $isupplier = $this->uri->segment(6);
            $dfrom    = $this->uri->segment(7);
            $dto      = $this->uri->segment(8);

            $this->load->model('dnalokasi/mmaster');
            $query   = $this->db->query("select b.i_nota, to_char(a.d_alokasi,'yyyymm') as thbl 
                                         from tm_alokasi_bk a, tm_alokasi_bk_item b
                                         where a.i_alokasi=b.i_alokasi and a.i_supplier=b.i_supplier and a.i_kbank=b.i_kbank
                                         and a.i_alokasi = '$ialokasi' and a.i_supplier = '$isupplier' and a.i_kbank='$ikbank' ");
            if($query->num_rows()>0){
               $data['jmlitem'] = $query->num_rows();
               $data['vsisa']   = $this->mmaster->sisa($isupplier,$ialokasi,$ikbank);
               $data['vbulat']  = $this->mmaster->bulat($isupplier,$ialokasi,$ikbank);
               $data['isi']     = $this->mmaster->bacapl($isupplier,$ialokasi,$ikbank);
               $data['detail']  = $this->mmaster->bacadetailpl($isupplier,$ialokasi,$ikbank);
               
               $hasilrow = $query->row();
               $thbl  = $hasilrow->thbl;
               $query3  = $this->db->query(" select i_periode from tm_periode ");
               if ($query3->num_rows() > 0){
                 $hasilrow = $query3->row();
                 $i_periode = $hasilrow->i_periode;
               }
               if($i_periode <= $thbl)
                  $data['bisaedit']=true;
                else
                  $data['bisaedit']=false;
            }
          }

          $data['ialokasi'] = $ialokasi;
          $data['isupplier']= $isupplier;
          $data['ikbank']   = $ikbank;
          $data['dfrom']    = $dfrom;
          $data['dto']      = $dto;
          $this->load->view('dnalokasi/vmainform',$data);
         }elseif($this->session->userdata('logged_in')){
         $this->load->view('errorauthority');
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function updatepelunasan()
   {
      if (
         (($this->session->userdata('logged_in')) &&
          ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $this->load->model('dnalokasi/mmaster');
         $ipl  = $this->input->post('ipelunasan', TRUE);
         $idt  = $this->input->post('idt', TRUE);
         $ddt  = $this->input->post('ddt', TRUE);
         if($ddt!=''){
            $tmp=explode("-",$ddt);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $ddt=$th."-".$bl."-".$hr;
         }
      $dbukti  = $this->input->post('dbukti', TRUE);
         if($dbukti!=''){
            $tmp=explode("-",$dbukti);
            $th=$tmp[2];
            $bl=$tmp[1];
            $hr=$tmp[0];
            $dbukti=$th."-".$bl."-".$hr;
         }
 
        
         $ecustomercity = $this->input->post('ecustomercity', TRUE);
         $isupplier         = $this->input->post('isupplier', TRUE);
         $esuppliername     = $this->input->post('esuppliername', TRUE);
         $ijenisbayar   = $this->input->post('ijenisbayar',TRUE);
         $ejenisbayarname= $this->input->post('ejenisbayarname',TRUE);
         $egirobank     = $this->input->post('egirobank',TRUE);
         if(($egirobank==Null) && ($ijenisbayar!='05')){
            $egirobank  = '-';
            $igiro      = $this->input->post('igiro',TRUE);
         }else if(($egirobank==Null) && ($ijenisbayar=='05')){
            $egirobank  = $this->input->post('igiro',TRUE);
            $igiro      ='';
         }else if(($egirobank!=Null) && ($ijenisbayar=='01')){
            $igiro      = $this->input->post('igiro',TRUE);
         }else if(($egirobank!=Null) && ($ijenisbayar=='03')){
            $nkuyear = $this->input->post('nkuyear',TRUE);
            $igiro      = $this->input->post('igiro',TRUE);
         }else if($ijenisbayar=='04'){
            $igiro      = $this->input->post('igiro',TRUE);
         }else{
            $igiro      ='';
         }
         $vjumlah    = $this->input->post('vjumlah',TRUE);
         $vjumlah    = str_replace(',','',$vjumlah);
#        $vlebih        = $this->input->post('vlebih',TRUE);
#      if($ijenisbayar=='04'||$ijenisbayar=='05'){
#           $vlebih        = '0';
#      }else{
         $vlebih        = $this->input->post('vlebih',TRUE);
#      }
         $vlebih        = str_replace(',','',$vlebih);
#      $ipelunasanremark = $this->input->post('ipelunsanremark',TRUE);
#      $eremark    = $this->input->post('eremark',TRUE);
         $jml        = $this->input->post('jml', TRUE);
      $ada=false;
         if(($ddt!='') && ($dbukti!='') && ($idt!='') && ($vjumlah!='') && ($vjumlah!='0') && ($jml!='0') && ($ijenisbayar!='')){
        for($i=1;$i<=$jml;$i++){
          $vjumla = $this->input->post('vjumlah'.$i, TRUE);
             $vsisa  = $this->input->post('vsisa'.$i, TRUE);
             $vjumla = str_replace(',','',$vjumla);
             $vsisa  = str_replace(',','',$vsisa);
             $vsisa  = $vsisa-$vjumla;
          $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
#          if( ($vsisa-$vjumlah>0) && ($ipelunasanremark=='') ){
          if( ($vsisa>0) && ($ipelunasanremark=='') ){
            $ada=true;
          }
          if($ada) break;
        }
        if(!$ada){
              $this->db->trans_begin();
              $asalkn   = $this->mmaster->jmlasalkn($ipl,$idt,$isupplier,$ddt);
              foreach($asalkn as $asl){
                 $jmlpl = $asl->v_jumlah;
                 $lbhpl = $asl->v_lebih;
              }
              $asal  = $jmlpl-$lbhpl;
              $this->mmaster->deleteheader(  $ipl,$idt,$isupplier,$ddt);
              $this->mmaster->insertheader(  $ipl,$idt,$isupplier,$ijenisbayar,$igiro,$icustomer,$dgiro,$ddt,$dbukti,$dcair,$egirobank,
                                      $vjumlah,$vlebih);
              $pengurang=$vjumlah-$vlebih;
          $igiro=trim($igiro);

          $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
            $group='xxx';
            foreach($query->result() as $row){
               $group=$row->i_customer_groupbayar;
            }

              if($ijenisbayar=='01'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updategiro($group,$isupplier,$igiro,$pengurang,$asal);
              }elseif($ijenisbayar=='03'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updateku($group,$isupplier,$igiro,$pengurang,$asal,$nkuyear);
              }elseif($ijenisbayar=='04'){
  #              $pengurang=$vjumlah-$vlebih;
                $this->mmaster->updatekn($group,$isupplier,$igiro,$pengurang,$asal);
              }elseif($ijenisbayar=='05'){
            $this->mmaster->updatelebihbayar($group,$isupplier,$egirobank,$pengurang,$asal);
          }
              for($i=1;$i<=$jml;$i++){
                $idnap              = $this->input->post('idnap'.$i, TRUE);
                $ddnap              = $this->input->post('ddnap'.$i, TRUE);
                if($ddnap!=''){
                   $tmp=explode("-",$ddnap);
                   $th=$tmp[2];
                   $bl=$tmp[1];
                   $hr=$tmp[0];
                   $ddnap=$th."-".$bl."-".$hr;
              }
  /*
  a=giro/cek -- 01
  b=tunai -- 02
  c=transfer/ku -- 03
  d=kn -- 04
  e=lebih -- 05
  */
                $vjumlah   = $this->input->post('vjumlah'.$i, TRUE);
                $vasal  = $this->input->post('vasal'.$i, TRUE);
                $vasol  = $this->input->post('vasal'.$i, TRUE);
                if($vasal==''){
                  $vasal   = $this->input->post('vsisa'.$i, TRUE);
                  $vasol   = $this->input->post('vsisa'.$i, TRUE);
                }
                $vjumlah= str_replace(',','',$vjumlah);
                 $vasal = str_replace(',','',$vasal);
                 $vasol = str_replace(',','',$vasol);
#               $vsisa  = $vasal-$vjumlah;
                $vsisa  = $vasal;
                $vsiso  = $vasal-$vjumlah;
            $ipelunasanremark = $this->input->post('ipelunasanremark'.$i,TRUE);
            $eremark    = $this->input->post('eremark'.$i,TRUE);

                $this->mmaster->deletedetail(   $ipl,$idt,$isupplier,$idnap,$ddt);
                $this->mmaster->insertdetail(   $ipl,$idt,$isupplier,$idnap,$idnap,$ddt,$vjumlah,$vsisa,$i,$ipelunasanremark,$eremark);
#           $this->mmaster->updatedt($idt,$isupplier,$ddt,$idnap,$vsisa);
#               $this->mmaster->updatenota($idnap,$vsisa);
#               $this->mmaster->updatenota($idnap,$vsiso);
                $this->mmaster->updatenota($idtap,$isupplier,$iarea,$vjumlah);
              }
#             $nilai=$this->mmaster->hitungsisadt($idt,$isupplier,$ddt);
#             if($nilai==0){
#                $this->mmaster->updatestatusdt($idt,$isupplier,$ddt);
#             }
              if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                 $this->db->trans_commit();
  #               $this->db->trans_rollback();

                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Update Pelunasan No:'.$ipl.' Area:'.$isupplier;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );

                 $data['sukses'] = true;
                 $data['inomor'] = $ipl;
                 $this->load->view('nomor',$data);
              }
        }
         }

      }else{
         $this->load->view('awal/index.php');
      }
   }
   function supplier()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $isupplier  = $this->uri->segment(4);
         $cari   = $this->uri->segment(5);
         if($cari=='sikasep'){
           $config['base_url'] = base_url().'index.php/dnalokasi/cform/supplier/'.$isupplier.'/sikasep/';
           $query = $this->db->query("select i_supplier from tr_supplier",false);
         }else{
           $config['base_url'] = base_url().'index.php/dnalokasi/cform/supplier/'.$isupplier.'/'.$cari.'/';
           $query   = $this->db->query(" select i_supplier from tr_supplier 
                                          where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') ",false);
         }
         $config['per_page'] = '10';
         
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);

         $this->load->model('dnalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->carisupplier($cari,$isupplier,$config['per_page'],$this->uri->segment(6));
         $data['isupplier']=$isupplier;
         $data['cari']=$cari;
         $this->load->view('dnalokasi/vlistsupplier', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carisupplier()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $isupplier    = strtoupper($this->input->post('area', FALSE));
         $cari    = strtoupper($this->input->post('cari', FALSE));
         if($isupplier=='' || $isupplier==null) $isupplier = $this->uri->segment(4);
         if($cari=='' || $cari==null) $cari = $this->uri->segment(5);
         if($cari=='' || $cari==null || $cari=='sikasep'){
           $config['base_url'] = base_url().'index.php/dnalokasi/cform/supplier/sikasep/';
         }else{
           $config['base_url'] = base_url().'index.php/dnalokasi/cform/supplier/'.$cari.'/';
           $query   = $this->db->query(" select i_supplier from tr_supplier
                                        where  (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') ",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('dnalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_customer');
         $data['isi']=$this->mmaster->carisupplier($cari,$isupplier,$config['per_page'],$this->uri->segment(6));
         $data['isupplier']=$isupplier;
         $data['cari']=$cari;
         $this->load->view('dnalokasi/vlistsupplier', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function girocek()
  {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu105')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/dnalokasi/cform/girocek/index/';
         $config['per_page'] = '10';
         if($this->session->userdata('i_area')=='00' || $this->session->userdata('i_area')=='PB'){
           $query = $this->db->query("select * from tr_jenis_bayar where i_jenis_bayar > '05'",false);
         }else{
           $query = $this->db->query("select * from tr_jenis_bayar where i_jenis_bayar > '05'",false);
         }
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('dnalokasi/mmaster');
         $data['area']=$this->session->userdata('i_area');
         $data['page_title'] = $this->lang->line('list_girocek');
         $data['isi']=$this->mmaster->bacagirocek($config['per_page'],$this->uri->segment(5),$this->session->userdata('i_area'));
         $this->load->view('dnalokasi/vlistgirocek', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function nota()
  {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['baris']    = $this->uri->segment(4);
         $data['isupplier']= $this->uri->segment(5);
         $baris              = $this->uri->segment(4);
         $isupplier          = $this->uri->segment(5);
         $config['base_url'] = base_url().'index.php/dnalokasi/cform/nota/'.$baris.'/'.$isupplier.'/';
         $query = $this->db->query("  select * from tm_dtap where  v_sisa!='0' and i_supplier='$isupplier'" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $this->load->model('dnalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($isupplier,$config['per_page'],$this->uri->segment(6));
         $data['isupplier']=$isupplier;
         $this->load->view('dnalokasi/vlistnota', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }  
   function carinota()
 {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari              = strtoupper($this->input->post('cari', FALSE));
         $baris             = $this->input->post('baris', FALSE)?$this->input->post('baris', FALSE):$this->uri->segment(4);
         
         $isupplier        = $this->input->post('isupplier', FALSE)?$this->input->post('isupplier', FALSE):$this->uri->segment(7);
         $dku              = $this->input->post('dku', FALSE)?$this->input->post('dku', FALSE):$this->uri->segment(8);
         $data['baris']    = $baris;
         $data['isupplier']= $isupplier;
         $data['dku']      = $dku;
         $config['base_url'] = base_url().'index.php/dnalokasi/cform/nota/'.$baris.'/'.$isupplier.'/'.$isupplier.'/index/';
         $query = $this->db->query(" select * from tr_supplier" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(10);
         $this->pagination->initialize($config);
         $this->load->model('dnalokasi/mmaster');

         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->carinota($cari,$isupplier,$config['per_page'],$this->uri->segment(10),$group);
         $data['baris']=$baris;
         $this->load->view('dnalokasi/vlistnota', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
##########
  
   function notaupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $data['baris']    = $this->uri->segment(4);
         $data['iarea']    = $this->uri->segment(5);
         $data['idt']      = $this->uri->segment(6);
         $data['icustomer']   = $this->uri->segment(7);
         //$data['dku']    = $this->uri->segment(8);
         $baris            = $this->uri->segment(4);
         $iarea            = $this->uri->segment(5);
         $idt           = $this->uri->segment(6);
         $icustomer        = $this->uri->segment(7);
         //$dku            = $this->uri->segment(8);
         $config['base_url'] = base_url().'index.php/dnalokasi/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
      }
         $query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer_groupar b
                                            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                            and a.i_dt='$idt'
                                            and a.i_area='$iarea'" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         //$config['cur_page']   = $this->uri->segment(10);
         $config['cur_page']  = $this->uri->segment(9);
         $this->pagination->initialize($config);
         $this->load->model('dnalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
         $this->load->view('dnalokasi/vlistnotaupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function carinotaupdate()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari             = strtoupper($this->input->post('cari', FALSE));
         $data['baris']    = $this->input->post('baris', FALSE);
         $data['iarea']    = $this->input->post('iarea', FALSE);
         $data['idt']      = $this->input->post('idt', FALSE);
         $data['icustomer']   = $this->input->post('icustomer', FALSE);
         //$data['dku']    = $this->uri->segment(8);
         $baris            = $this->input->post('baris', FALSE);
         $iarea            = $this->input->post('iarea', FALSE);
         $idt           = $this->input->post('idt', FALSE);
         $icustomer        = $this->input->post('icustomer', FALSE);
         //$dku            = $this->uri->segment(8);
         $config['base_url'] = base_url().'index.php/dnalokasi/cform/nota/'.$baris.'/'.$iarea.'/'.$idt.'/'.$icustomer.'/index/';
         $query = $this->db->query(" select i_customer_groupar from tr_customer_groupar where i_customer='$icustomer'" ,false);
      $group='xxx';
      foreach($query->result() as $row){
        $group=$row->i_customer_groupar;
      }
         $query = $this->db->query(" select a.i_dt from tm_dt_item a, tr_customer_groupar b
                                            where b.i_customer_groupar='$group' and a.i_customer=b.i_customer
                                            and a.i_dt='$idt'
                                            and a.i_area='$iarea'" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         //$config['cur_page']   = $this->uri->segment(10);
         $config['cur_page']  = $this->uri->segment(9);
         $this->pagination->initialize($config);
         $this->load->model('dnalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->bacanota($iarea,$idt,$icustomer,$config['per_page'],$this->uri->segment(9),$group);
         $this->load->view('dnalokasi/vlistnotaupdate', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function bank()
   {
    if (
      (($this->session->userdata('logged_in')) &&
      ($this->session->userdata('menu135')=='t')) ||
      (($this->session->userdata('logged_in')) &&
      ($this->session->userdata('allmenu')=='t'))
       ){
      $cari   = strtoupper($this->input->post("cari"));
      $config['base_url'] = base_url().'index.php/dnalokasi/cform/bank/index/';
      $config['per_page'] = '10';
      $query = $this->db->query(" select * from tr_bank
                                  where (upper(i_bank) like '%$cari%' or upper(e_bank_name) like '%$cari%')",false);
      $config['total_rows'] = $query->num_rows(); 
      $config['first_link'] = 'Awal';
      $config['last_link'] = 'Akhir';
      $config['next_link'] = 'Selanjutnya';
      $config['prev_link'] = 'Sebelumnya';
      $config['cur_page'] = $this->uri->segment(5);
      $this->pagination->initialize($config);

      $data['cari']=$cari;
      $this->load->model('dnalokasi/mmaster');
      $data['page_title'] = $this->lang->line('list_bank');
      $data['isi']=$this->mmaster->bacabank($cari,$config['per_page'],$this->uri->segment(5));
      $this->load->view('dnalokasi/vlistbank', $data);
    }else{
      $this->load->view('awal/index.php');
    }
  }

   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/dnalokasi/cform/area/index/';
         $iuser     = $this->session->userdata('user_id');
         $cari      = $this->input->post('cari', FALSE);
         $cari      = strtoupper($cari);
         $query     = $this->db->query("select * from tr_area
                                  where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' 
                                  and i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('dnalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
         $this->load->view('dnalokasi/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

   function view()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $cari    = strtoupper($this->input->post('cari'));
         $dfrom      = $this->input->post('dfrom');
         $dto     = $this->input->post('dto');
         $isupplier      = $this->input->post('isupplier');
         $vsisa      = $this->input->post('vsisa');
         if($dfrom=='') $dfrom=$this->uri->segment(4);
         if($dto=='') $dto=$this->uri->segment(5);
         if($isupplier=='') $isupplier   = $this->uri->segment(6);
         $config['base_url'] = base_url().'index.php/dnalokasi/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';

         $query = $this->db->query(" select a.i_supplier, a.i_dn_ap, a.i_refference, a.d_dn_ap, a.v_sisa, b.e_supplier_name, a.v_netto
                                     from tm_dn_ap a
                                     left join tr_supplier b on a.i_supplier=b.i_supplier 
                                      where a.f_dn_ap_cancel='false' and
                                      a.i_supplier='$isupplier'
                                      and a.d_dn_ap >= to_date('$dfrom','dd-mm-yyyy')
                                      and a.d_dn_ap <= to_date('$dto','dd-mm-yyyy')
                                      and a.v_sisa>0
                                      ",false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(8);
         $this->paginationxx->initialize($config);
         $data['page_title'] = $this->lang->line('dnalokasi');
         $this->load->model('dnalokasi/mmaster');
         $data['cari']  = $cari;
         $data['dfrom'] = $dfrom;
         $data['dto']   = $dto;
         $data['isupplier'] = $isupplier;
         $data['vsisa'] = $vsisa;
         $data['isi']   = $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
         $data['ikbank']   = '';
         $data['idnap']   = '';
         $data['c']     = $dfrom.'/'.$dto.'/'.$isupplier.'/'.$this->uri->segment(8);
         $this->load->view('dnalokasi/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function remark()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu502')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $baris = $this->uri->segment(4);
         $config['base_url'] = base_url().'index.php/dnalokasi/cform/remark/'.$baris.'/';
         $config['per_page'] = '10';
         $query = $this->db->query("   select i_pelunasan_remark from tr_pelunasan_remark",false);
         $config['total_rows'] = $query->num_rows();
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('dnalokasi/mmaster');
         $data['page_title'] = $this->lang->line('list_pelunasan_remark');
         $data['isi']=$this->mmaster->bacaremark($config['per_page'],$this->uri->segment(5));
      $data['baris']=$baris;
         $this->load->view('dnalokasi/vlistremark', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
