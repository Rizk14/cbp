<?php
// include("php/fungsi.php");
class Cform extends CI_Controller
{
	public $menu 	= "C001001";
	public $title 	= "Surat Jalan Pengantar (Umum)";
	public $folder 	= "sjpumum";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model($this->folder . '/mmaster');
	}

	function index()
	{
		$cari 			= strtoupper($this->input->post('cari', false));
		$iuser 			= $this->session->userdata('user_id');
		$idepartement 	= $this->session->userdata('departement');

		$data['page_title'] = $this->title;
		$data['folder'] 	= $this->folder;

		if ($this->uri->segment(4) != 'x01') {
			if ($cari == '') {
				$cari = $this->uri->segment(4);
			}

			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/index/' . $cari . '/';
		} else {
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/index/' . 'x01/';
		}

		$query = $this->db->query("	SELECT id, i_sjp, l1.e_area_name, to_char(d_sjp, 'mm-dd-yyyy') as d_sjp,  to_char(d_sjp_receive, 'mm-dd-yyyy') as d_sjp_receive, l2.e_sender, l3.e_recipient, a.e_remark, f_sjp_cancel,
									case when f_sjp_cancel = true then 'Batal' else 'Aktif' end as f_sjp_cancel_name, f_printed, 
									case when n_print = 0 then 'Belum' else 'Sudah ' || n_print::text || 'X' end as n_print,
									case 	
										when f_sjp_cancel = true then 'Batal'
										when d_approve is null and d_notapprove is null then 'Menunggu Approval'
										when d_approve is null and d_notapprove is not null then 'Rejected [' || coalesce(e_remark_notapprove, '') || ']'
										when d_approve is not null and d_sjp_receive is null then 'Approved'
										when d_sjp_receive is not null then 'Received'
									end as status_sjp, a.i_approve, a.d_approve, a.d_notapprove, a.e_receive
									from tm_sjp_umum a 
									inner join tr_area b on (a.i_area = b.i_area) /*Sesuaikan Dengan Relasi Program*/
									,lateral (select b.i_area || ' - ' || b.e_area_name as e_area_name) l1
									,lateral (select coalesce(e_sender,'') || case when e_sender is null and e_sender_company is null then '' else ' - ' || coalesce(a.e_sender_company,'') end as e_sender) l2
									,lateral (select coalesce(e_recipient ,'') || case when e_recipient  is null and e_recipient_company  is null then '' else ' - ' || coalesce(a.e_recipient_company ,'') end as e_recipient) l3
									WHERE (a.i_sender = '$iuser' OR a.i_approve = '$iuser')
									and (upper(i_sjp) LIKE '%$cari%' OR upper(l2.e_sender) LIKE '%$cari%') /* AND a.i_area in (SELECT i_area FROM tm_user_area WHERE i_user = '$iuser') */ 
									ORDER BY d_sjp DESC ", FALSE);

		$config['total_rows'] 	= $query->num_rows();
		$config['per_page'] 	= '10';
		$config['first_link'] 	= 'Awal';
		$config['last_link'] 	= 'Akhir';
		$config['next_link'] 	= 'Selanjutnya';
		$config['prev_link'] 	= 'Sebelumnya';
		$config['cur_page'] 	= $this->uri->segment(5);
		$this->pagination->initialize($config);

		$data['isi'] 		= $this->mmaster->bacasemua($cari, $config['per_page'], $this->uri->segment(4), $iuser);
		$data['cari'] 		= $cari;
		$data['detail'] 	= "";
		$data['jmlitem'] 	= "";

		if ($cari != '') {
			$this->logger->writenew("Cari Data Berdasarkan Keyword : " . $cari);
		} else {
			$this->logger->writenew("Membuka Menu : " . $this->menu . " - " . $this->title);
		}

		$this->load->view($this->folder . '/vformview', $data);
	}

	function tambah()
	{
		$data['page_title'] = $this->title;
		$data['folder'] 	= $this->folder;
		$iarea				= $this->session->userdata('i_area');
		$eareaname 			= $this->db->query(" SELECT e_area_name FROM tr_area WHERE i_area = '$iarea' ")->row()->e_area_name;
		$data['iarea'] 		= $iarea;
		$data['eareaname'] 	= $eareaname;
		$data['username'] 	= $this->session->userdata("user_name");

		$this->logger->writenew("Membuka Menu Tambah Data SJP");

		$this->load->view($this->folder . '/vformadd', $data);
	}

	function simpan()
	{
		$esender 			= strtoupper($this->input->post('esender', TRUE));
		$esendercompany		= $this->input->post('esendercompany', TRUE);
		$erecipient 		= $this->input->post('erecipient', TRUE);
		$erecipientcompany	= $this->input->post('erecipientcompany', TRUE);
		$dsjp				= date('Y-m-d', strtotime($this->input->post('dsjp', TRUE)));
		$thbl				= date('Ym', strtotime($this->input->post('dsjp', TRUE)));
		$iarea				= $this->input->post('iarea', TRUE);
		$eareaname			= $this->input->post('eareaname', TRUE);
		$eremarkhead			= $this->input->post('eremarkhead', TRUE);
		$jml				= $this->input->post('jml', TRUE);
		$isender			= $this->session->userdata("user_id");
		$iapprove			= $this->db->query(" SELECT i_atasan1 FROM tm_user WHERE i_user = '$isender' ")->row()->i_atasan1;

		if ($esender != '' & $esendercompany != '' & $erecipient != '' & $erecipientcompany != '' & $dsjp != '' & $iarea != '' & $jml != 0) {
			$isj		 		= $this->mmaster->runningnumbersj($iarea, $thbl);

			$this->db->trans_begin();

			$id = $this->mmaster->insertheader($isj, $isender, $esender, $esendercompany, $erecipient, $erecipientcompany, $dsjp, $iarea, $eremarkhead, $iapprove)->row()->id;

			for ($i = 1; $i <= $jml; $i++) {
				$iproduct		= $this->input->post('iproduct' . $i, TRUE);
				$eproductname	= $this->input->post('eproductname' . $i, TRUE);
				$qty			= $this->input->post('qty' . $i, TRUE);
				$satuan			= $this->input->post('satuan' . $i, TRUE);
				$eremark		= $this->input->post('eremark' . $i, TRUE);

				if ($qty > 0) {
					$this->mmaster->insertdetail($id, $isj, $iarea, $dsjp, $iproduct, $eproductname, $qty, $satuan, $eremark, $i);
				}
			}

			if (($this->db->trans_status() === FALSE)) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();

				$this->logger->writenew("Input SJP Umum No : " . $isj);

				$data['sukses']	= true;
				$data['inomor']	= $isj;

				$this->load->view('nomor', $data);
			}
		}
	}

	function edit()
	{
		$data['page_title'] = $this->title . " Update";
		$data['folder'] 	= $this->folder;

		if ($this->uri->segment(4) != '') {
			$id 		= $this->uri->segment(4);
			$data['id'] 	= $id;

			$query 			 = $this->db->query(" select * from tm_sjp_umum_item where id_sjp='$id' ");
			$data['jmlitem'] = $query->num_rows();
			$data['isi']	 = $this->mmaster->baca($id);
			$data['detail']	 = $this->mmaster->bacadetail($id);

			$this->load->view($this->folder . '/vformupdate', $data);
		} else {
			$this->load->view($this->folder . '/vinsert_fail', $data);
		}
	}

	function update()
	{
		$isjp 				= $this->input->post('isjp', TRUE);
		$id 				= $this->input->post('id', TRUE);
		$esender 			= $this->input->post('esender', TRUE);
		$esendercompany		= $this->input->post('esendercompany', TRUE);
		$erecipient 		= $this->input->post('erecipient', TRUE);
		$erecipientcompany	= $this->input->post('erecipientcompany', TRUE);
		$dsjp				= date('Y-m-d', strtotime($this->input->post('dsjp', TRUE)));
		$thbl				= date('Ym', strtotime($this->input->post('dsjp', TRUE)));
		$iarea				= $this->input->post('iarea', TRUE);
		$eareaname			= $this->input->post('eareaname', TRUE);
		$eremarkhead		= $this->input->post('eremarkhead', TRUE);
		$jml				= $this->input->post('jml', TRUE);

		if ($isjp != "" && $id != "" && $esender != '' & $esendercompany != '' & $erecipient != '' & $erecipientcompany != '' & $dsjp != '' & $iarea != '' & $jml != 0) {

			$this->db->trans_begin();

			$this->mmaster->updateheader($id, $isjp, $esender, $esendercompany, $erecipient, $erecipientcompany, $dsjp, $iarea, $eremarkhead); /* DISINI ADA QUERY DELETE SJP UMUM ITEM */

			/* INSERT ULANG ITEM DISINI */
			for ($i = 1; $i <= $jml; $i++) {
				$iproduct		= $this->input->post('iproduct' . $i, TRUE);
				$eproductname	= $this->input->post('eproductname' . $i, TRUE);
				$qty			= $this->input->post('qty' . $i, TRUE);
				$satuan			= $this->input->post('satuan' . $i, TRUE);
				$eremark		= $this->input->post('eremark' . $i, TRUE);

				if ($qty > 0) {
					$this->mmaster->insertdetail($id, $isjp, $iarea, $dsjp, $iproduct, $eproductname, $qty, $satuan, $eremark, $i);
				}
			}

			if (($this->db->trans_status() === FALSE)) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();

				$this->logger->writenew("Update SJP Umum No : " . $isjp);

				$data['sukses']	= true;
				$data['inomor']	= $isjp;

				$this->load->view('nomor', $data);
			}
		}
	}

	function approvedetail()
	{
		$data['page_title'] = $this->title . " Approve";
		$data['folder'] 	= $this->folder;

		if ($this->uri->segment(4) != '') {
			$id 		= $this->uri->segment(4);
			$isjp 		= $this->uri->segment(5);
			$data['id'] 	= $id;

			$query 			 = $this->db->query(" select * from tm_sjp_umum_item where id_sjp='$id' ");
			$data['jmlitem'] = $query->num_rows();
			$data['isi']	 = $this->mmaster->baca($id);
			$data['detail']	 = $this->mmaster->bacadetail($id);

			$this->logger->writenew("Membuka Halaman Approve SJP Umum No : " . $isjp);

			$this->load->view($this->folder . '/vformapprove', $data);
		} else {
			$this->load->view($this->folder . '/vinsert_fail', $data);
		}
	}

	function approve()
	{
		$id 				= $this->input->post('id', TRUE);
		$isjp 				= $this->input->post('isjp', TRUE);

		if ($isjp != "" && $id != "") {
			$this->db->trans_begin();

			$this->mmaster->approve($id);

			if (($this->db->trans_status() === FALSE)) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();

				$this->logger->writenew("Approve SJP Umum No : " . $isjp);

				$data['sukses']	= true;
				$data['inomor']	= $isjp;

				$this->load->view('nomor', $data);
			}
		}
	}

	function detailreject()
	{
		$id  		= $this->uri->segment(4);
		$isjp  		= $this->uri->segment(5);

		$data['page_title'] = $this->title . " Reject";
		$data['folder'] 	= $this->folder;

		$data['isjp'] 		= $isjp;
		$data['id'] 		= $id;

		$this->logger->writenew("Membuka Halaman Reject SJP Umum No : " . $isjp);

		$this->load->view($this->folder . '/vreject', $data);
	}

	function reject()
	{
		$id 		= $this->input->post('id');
		$isjp		= $this->input->post('isjp');
		$eremark 	= $this->input->post('eremark') != "" ? $this->input->post('eremark') : "";

		$this->db->trans_begin();

		$this->mmaster->reject($id, $eremark);

		if (($this->db->trans_status() === FALSE)) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();

			$this->logger->writenew('Reject SJP Umum No : ' . $isjp);

			$this->load->view($this->folder . '/vformview', $data);
		}
	}

	function receivedetail()
	{
		$data['page_title'] = $this->title . " Receive";
		$data['folder'] 	= $this->folder;

		if ($this->uri->segment(4) != '') {
			$id 		= $this->uri->segment(4);
			$isjp 		= $this->uri->segment(5);
			$data['id'] 	= $id;

			$query 			 = $this->db->query(" select * from tm_sjp_umum_item where id_sjp='$id' ");
			$data['jmlitem'] = $query->num_rows();
			$data['isi']	 = $this->mmaster->baca($id);
			$data['detail']	 = $this->mmaster->bacadetail($id);

			$this->logger->writenew("Membuka Halaman Receive SJP Umum No : " . $isjp);


			$this->load->view($this->folder . '/vreceive', $data);
		} else {
			$this->load->view($this->folder . '/vinsert_fail', $data);
		}
	}

	function receive()
	{
		$id 				= $this->input->post('id', TRUE);
		$isjp 				= $this->input->post('isjp', TRUE);
		$ereceive 			= $this->input->post('ereceive', TRUE);
		$jml				= $this->input->post('jml', TRUE);

		if ($isjp != "" && $id != "") {
			$this->db->trans_begin();

			$this->mmaster->receive($id, $ereceive);

			/* UPDATE ITEM DISINI */
			for ($i = 1; $i <= $jml; $i++) {
				$idproduct		= $this->input->post('idproduct' . $i, TRUE);
				$iproduct		= $this->input->post('iproduct' . $i, TRUE);
				$eproductname	= $this->input->post('eproductname' . $i, TRUE);
				$qty			= $this->input->post('qtyreceive' . $i, TRUE);
				$satuan			= $this->input->post('satuan' . $i, TRUE);
				$eremark		= $this->input->post('eremark' . $i, TRUE);

				if ($qty > 0) {
					$this->mmaster->updatedetail($id, $idproduct, $isjp, $iproduct, $eproductname, $qty, $satuan, $eremark, $i);
				}
			}

			if (($this->db->trans_status() === FALSE)) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();

				$this->logger->writenew("Receive SJP Umum No : " . $isjp);

				$data['sukses']	= true;
				$data['inomor']	= $isjp;

				$this->load->view('nomor', $data);
			}
		}
	}

	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu168') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$irrkh	= $this->input->post('irrkhdelete', TRUE);

			$this->mmaster->delete($irrkh);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if (pg_num_rows($rs) > 0) {
				while ($row = pg_fetch_assoc($rs)) {
					$ip_address	  = $row['ip_address'];
					break;
				}
			} else {
				$ip_address = 'kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Hapus RRKH Salesman:' . $isalesman . ' Area:' . $iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$data['page_title'] = $this->title;
			$data['irrkh'] = '';
			$data['jmlitem'] = '';
			$data['detail'] = '';
			$data['isi'] = $this->mmaster->bacasemua();
			$this->load->view($this->folder . '/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu168') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$drrkh			= $this->uri->segment(4);
			$isalesman	= $this->uri->segment(5);
			$iarea			= $this->uri->segment(6);
			$icustomer	= $this->uri->segment(7);
			$dfrom			= $this->uri->segment(8);
			$dto				= $this->uri->segment(9);
			$this->db->trans_begin();

			$this->mmaster->deletedetail($icustomer, $isalesman, $drrkh, $iarea);
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
				$data['page_title'] = $this->title . " Update";
				$data['drrkh'] 		= $drrkh;
				$data['isalesman'] = $isalesman;
				$data['iarea'] 		= $iarea;
				$data['dfrom'] 		= $dfrom;
				$data['dto']	 		= $dto;
				$tmp = explode("-", $drrkh);
				$th = $tmp[0];
				$bl = $tmp[1];
				$hr = $tmp[2];
				$data['hari']			= dinten($hr, $bl, $th);
				$query = $this->db->query("select * from tm_rrkh_item where d_rrkh = '$drrkh' and i_salesman='$isalesman' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows();

				$data['isi'] = $this->mmaster->baca($drrkh, $isalesman, $iarea);
				$data['detail'] = $this->mmaster->bacadetail($drrkh, $isalesman, $iarea);
				$this->load->view($this->folder . '/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/area/index/';
		$allarea = $this->session->userdata('allarea');
		$iuser   = $this->session->userdata('user_id');
		if ($allarea == 't') {
			$query = $this->db->query(" select * from tr_area order by i_area", false);
		} else {
			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
		}

		$config['total_rows'] = $query->num_rows();
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(5);
		$this->pagination->initialize($config);


		$data['page_title'] = $this->lang->line('list_area');
		$data['folder'] 	= $this->folder;
		$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $allarea, $iuser);
		$this->load->view($this->folder . '/vlistarea', $data);
	}

	function cariarea()
	{

		$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/area/index/';
		$allarea = $this->session->userdata('allarea');
		$iuser   = $this->session->userdata('user_id');
		$cari    = $this->input->post('cari', FALSE);
		$cari = strtoupper($cari);

		if ($allarea == 't') {
			$query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
		} else {
			$query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
		}
		$config['total_rows'] = $query->num_rows();
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(5);
		$this->pagination->initialize($config);

		$data['page_title'] = $this->lang->line('list_area');
		$data['folder'] 	= $this->folder;
		$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $allarea, $iuser);
		$this->load->view($this->folder . '/vlistarea', $data);
	}

	function cari()
	{

		$cari = $this->input->post('cari', FALSE);
		$cari = strtoupper($cari);
		$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/index/';
		$query = $this->db->query("select * from tm_rrkh
						   where upper(i_rrkh) like '%$cari%' ", false);
		$config['total_rows'] = $query->num_rows();
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(4);
		$this->pagination->initialize($config);

		$data['isi'] = $this->mmaster->cari($cari, $config['per_page'], $this->uri->segment(5));
		$data['page_title'] = $this->lang->line('trans_rrkh');
		$data['irrkh'] = '';
		$data['jmlitem'] = '';
		$data['detail'] = '';
		$this->load->view($this->folder . '/vmainform', $data);
	}

	function cetak()
	{
		$id  	= $this->uri->segment(4);
		$isjp  	= $this->uri->segment(5);

		$data['page_title'] = "Cetak " . $this->title;
		$data['folder'] 	= $this->folder;

		$data['isi'] 	= $this->mmaster->baca($id);
		$data['detail'] = $this->mmaster->bacadetail($id);

		$this->logger->writenew('Page Preview Cetak SJP Umum No : ' . $isjp);

		$this->load->view($this->folder . '/vformprint', $data);
	}

	function updatecetak()
	{
		$id  	= $this->input->post('id');
		$isjp  	= $this->input->post('isjp');

		$this->db->trans_begin();

		$this->mmaster->fprinted($id);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();

			$this->logger->writenew("Cetak SJP Umum No : " . $isjp);

			echo $nodo;
		}
	}

	public function undo()
	{
		$id 		= $this->uri->segment(4);
		$isjp 		= $this->uri->segment(5);

		$this->db->trans_begin();

		$this->mmaster->reprint($id, $isjp, $this->folder);

		if ($this->db->trans_status() === FALSE || $this->db->trans_status() === false) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
			$this->logger->writenew('Buka Akses Cetak Ulang SJP No : ' . $isjp);
		}
	}
}
