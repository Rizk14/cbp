<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listpenjualankonsinyasi');
			$data['iperiode']='';
			$this->load->view('listpenjualankonsinyasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $data['cust']='';
			$iperiode		= $this->input->post('iperiode');
			if($iperiode=='') $iperiode	= $this->uri->segment(4);
			$this->load->model('listpenjualankonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('listpenjualankonsinyasi');
			$data['iperiode']		= $iperiode;
      $data['diskon']     = $this->mmaster->bacadiskon($iperiode);
			$data['isi']		    = $this->mmaster->bacaperiode($iperiode);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Buka Rekap SPB Konsinyasi Periode: '.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
      $data['ispb']='';
			$this->load->view('listpenjualankonsinyasi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listpenjualankonsinyasi');
			$this->load->view('listpenjualankonsinyasi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function rinci()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode	= $this->uri->segment(4);
			$cust	= $this->uri->segment(5);
			$this->load->model('listpenjualankonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('listpenjualankonsinyasi');
			$data['iperiode']		= $iperiode;
			$data['type']		    = 'rnc';
			$data['cust']		    = $cust;
      $data['diskon']     = $this->mmaster->bacadiskon($iperiode);
			$data['isi']		    = $this->mmaster->bacaperioderinci($iperiode, $cust);
			$this->load->view('listpenjualankonsinyasi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function rincibrg()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode	= $this->uri->segment(4);
			$cust	= $this->uri->segment(5);
			$this->load->model('listpenjualankonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('listpenjualankonsinyasi');
			$data['iperiode']		= $iperiode;
			$data['cust']		    = $cust;
			$data['type']		    = 'rncbrg';
			$data['isi']		    = $this->mmaster->bacaperioderincibrg($iperiode, $cust);
			$this->load->view('listpenjualankonsinyasi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function rincitglbrg()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode	= $this->uri->segment(4);
			$cust	= $this->uri->segment(5);
			$this->load->model('listpenjualankonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('listpenjualankonsinyasi');
			$data['iperiode']		= $iperiode;
			$data['cust']		    = $cust;
			$data['type']		    = 'rnctglbrg';
			$data['isi']		    = $this->mmaster->bacaperioderincitglbrg($iperiode, $cust);
			$this->load->view('listpenjualankonsinyasi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
