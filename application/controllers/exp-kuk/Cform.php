<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu429')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exp-kuk');
			$data['datefrom']='';
			$data['dateto']	='';
			$this->load->view('exp-kuk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu429')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-kuk/mmaster');
			$datefrom = $this->input->post('datefrom');
			$dateto	  = $this->input->post('dateto');
			if($datefrom!=''){
				$tmp=explode("-",$datefrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$datefrom=$hr."-".$bl."-".$th;
				$periodeawal	= $hr."/".$bl."/".$th;
			}
            if($dateto!='')
            {
               $tmp=explode("-",$dateto);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $dateto=$hr."-".$bl."-".$th;
               $periodeakhir   = $hr."/".$bl."/".$th;
            }
			$data['page_title'] = $this->lang->line('exp-kuk');
			$this->db->select("	distinct a.i_supplier, a.d_kuk, a.f_kuk_cancel, a.i_kuk, a.d_kuk, a.e_bank_name, b.e_supplier_name,
                          a.e_remark, a.v_jumlah, a.v_sisa, a.f_close, a.n_kuk_year, c.i_pelunasanap, c.d_bukti, c.i_giro
                           from tm_kuk a
                           left join tr_supplier b on(a.i_supplier=b.i_supplier)
                           left join tm_pelunasanap c on(a.i_kuk=c.i_giro and a.d_kuk=c.d_giro and c.f_pelunasanap_cancel='f' and
                           ((c.i_jenis_bayar!='02' and 
                           c.i_jenis_bayar!='01' and 
                           c.i_jenis_bayar!='04' and 
                           c.i_jenis_bayar='03') or ((c.i_jenis_bayar='03') is null)))
                           where (a.d_kuk >= to_date('$datefrom', 'dd-mm-yyyy') and
                           a.d_kuk <= to_date('$dateto', 'dd-mm-yyyy')) and a.f_kuk_cancel ='f'
                           ORDER BY a.d_kuk, a.i_supplier, a.i_kuk",false);
		    $query = $this->db->get();
			  $this->load->library('PHPExcel');
			  $this->load->library('PHPExcel/IOFactory');
			  $objPHPExcel = new PHPExcel();
			  $objPHPExcel->getProperties()->setTitle("Daftar Transfer Uang Keluar")->setDescription("PT. Dialogue Garmindo Utama");
			  $objPHPExcel->setActiveSheetIndex(0);
			  if ($query->num_rows() > 0){
				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A2:A4'
				  );
				  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				  $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);

				  $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar Transfer Uang Keluar');
				  $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,10,2);
				  $objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$periodeawal.' - '.$periodeakhir);
				  $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,10,3);

				  $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => true,
						  'italic'=> false,
						  'size'  => 10
					  ),
					  'alignment' => array(
						  'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						  'vertical'  => Style_Alignment::VERTICAL_CENTER,
						  'wrap'      => true
					  )
				  ),
				  'A5:I5'
				  );


				  $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Supplier');
				  $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  )

					  )
				  );

				  $objPHPExcel->getActiveSheet()->setCellValue('B5', 'No Bukti');
				  $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('C5', 'No Pelunasan');
				  $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );

				  $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Tgl Pelunasan');
				  $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );

				  $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Tgl Transfer');
				  $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Nama Bank');
				  $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  )
					  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Keterangan');
				  $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  )
					  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Jumlah');
				  $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  )
					  )
				  );
				  $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Sisa');
				  $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  )

					  )
				  );

				  $i=6;
				  $j=6;
          $no=0;
         
				  foreach($query->result() as $row){
            $no++;
            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				    array(
					    'font' => array(
						    'name'	=> 'Arial',
						    'bold'  => false,
						    'italic'=> false,
						    'size'  => 10
					    )
				    ),
				    'A'.$i.':I'.$i
				    );
					if($row->d_bukti!=''){
						$tmp=explode("-",$row->d_bukti);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$row->d_bukti=$hr."-".$bl."-".$th;
					}
					if($row->d_kuk!=''){
						$tmp=explode("-",$row->d_kuk);
						$th=$tmp[0];
						$bl=$tmp[1];
						$hr=$tmp[2];
						$row->d_kuk=$hr."-".$bl."-".$th;
					}
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_kuk, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_pelunasanap, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->d_bukti, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->d_kuk, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->e_bank_name, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->e_remark, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->v_jumlah, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->v_sisa, Cell_DataType::TYPE_NUMERIC);
					  $i++;
					  $j++;
				  }
				  $x=$i-1;
			  }
			  $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $nama='kuk-'.substr($datefrom,6,5).'-'.substr($datefrom,0,10).'.xls';
			  $objWriter->save('excel/00/'.$nama); 

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
        	$now	  = $row['c'];
	      }
	      $pesan='Export Transfer Uang Keluar tanggal:'.$datefrom.' sampai:'.$dateto;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan ); 

			  $data['sukses'] = true;
			  $data['inomor']	= "Transfer Uang Keluar";
			  $this->load->view('nomor',$data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	  }
}
?>
