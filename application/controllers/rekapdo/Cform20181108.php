<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu379')=='t')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu')=='t'))
			){
			$data['page_title'] = $this->lang->line('rekapdo');
		$data['iperiode']	= '';
		$data['isupplier']	  = '';
		$this->load->view('rekapdo/vmainform', $data);
	}else{
		$this->load->view('awal/index.php');
	}
}
function supplier()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu334')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/rekapdo/cform/supplier/index/';
	$config['total_rows'] = $this->db->count_all('tr_supplier');
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);

	$this->load->model('rekapdo/mmaster');
	$data['page_title'] = $this->lang->line('list_supplier');
	$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
	$this->load->view('rekapdo/vlistsupplier', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function carisupplier()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu334')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$config['base_url'] = base_url().'index.php/rekapdo/cform/supplier/index/';
	$cari = $this->input->post('cari', FALSE);
	$cari = strtoupper($cari);
	$query = $this->db->query("	select * from tr_supplier 
		where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'",false);
	$config['total_rows'] = $query->num_rows(); 
	$config['per_page'] = '10';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);
	$this->load->model('rekapdo/mmaster');
	$data['page_title'] = $this->lang->line('list_supplier');
	$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
	$this->load->view('rekapdo/vlistsupplier', $data);
}else{
	$this->load->view('awal/index.php');
}
}
function view()
{
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu379')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$dfrom = $this->input->post('dfrom');
	$dto = $this->input->post('dto');
	$isupplier = $this->input->post('isupplier'); 
	if($dfrom=='') $dfrom=$this->uri->segment(4);
	if($dto=='') $dto=$this->uri->segment(5);
	if($isupplier=='') $isupplier=$this->uri->segment(6);
	$config['base_url'] = base_url().'index.php/rekapdo/cform/view/index/';
	if($isupplier == 'AS'){
		$query = "select distinct f.i_dtap, f.d_dtap, a.f_do_cancel, a.i_do, a.d_do, a.i_op, a.i_area, c.i_spb, c.i_spb_old, d.i_spmb, d.i_spmb_old, 
		e.e_supplier_name, g.f_dtap_cancel, a.i_supplier from tr_supplier e,
		tm_op b left join tm_spb c on(b.i_reff=c.i_spb and b.i_area=c.i_area) 
		left join tm_spmb d on(b.i_reff=d.i_spmb and b.i_area=d.i_area),
		tm_do a left join tm_dtap_item f on(a.i_do=f.i_do)
		left join tm_dtap g on (f.i_dtap=g.i_dtap and f.i_area=g.i_area and f.i_supplier=g.i_supplier 
		and g.f_dtap_cancel='f')
		where a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy')
		and a.i_op=b.i_op and a.i_area=b.i_area and a.i_supplier=e.i_supplier 
		and a.i_area <> '00'
		and not d.i_spmb isnull
		order by a.i_supplier ";	
	}else{
		$query = "select distinct f.i_dtap, f.d_dtap, a.f_do_cancel, a.i_do, a.d_do, a.i_op, a.i_area, c.i_spb, c.i_spb_old, d.i_spmb, d.i_spmb_old, 
		e.e_supplier_name, g.f_dtap_cancel, a.i_supplier from tr_supplier e,
		tm_op b left join tm_spb c on(b.i_reff=c.i_spb and b.i_area=c.i_area) 
		left join tm_spmb d on(b.i_reff=d.i_spmb and b.i_area=d.i_area),
		tm_do a left join tm_dtap_item f on(a.i_do=f.i_do)
		left join tm_dtap g on (f.i_dtap=g.i_dtap and f.i_area=g.i_area and f.i_supplier=g.i_supplier 
		and g.f_dtap_cancel='f')
		where a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy')
		and a.i_op=b.i_op and a.i_area=b.i_area and a.i_supplier=e.i_supplier and a.i_supplier='$isupplier'
		and a.i_area <> '00'
		and not d.i_spmb isnull
		order by a.i_supplier ";		
	}
	$config['total_rows'] = $this->db->query($query)->num_rows();;
	$config['per_page'] = '50';
	$config['first_link'] = 'Awal';
	$config['last_link'] = 'Akhir';
	$config['next_link'] = 'Selanjutnya';
	$config['prev_link'] = 'Sebelumnya';
	$config['cur_page'] = $this->uri->segment(5);
	$this->pagination->initialize($config);

	$this->load->model('rekapdo/mmaster');
	$data['isi']=$this->mmaster->bacado($config['per_page'],$this->uri->segment(5), $dfrom, $dto, $isupplier);
	$data['page_title'] = $this->lang->line('rekapdo');

	$this->load->view('rekapdo/vformview',$data);


}else{
	$this->load->view('awal/index.php');
}
}
function edit(){
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu379')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$this->load->model('rekapdo/mmaster');
	$i_do = $this->uri->segment(4);
	$i_supplier  = $this->uri->segment(5);
	$data['page_title'] = $this->lang->line('rekapdo');
	$data['isi'] 	= $this->mmaster->bacaheader($i_do, $i_supplier);
	$data['detail']  = $this->mmaster->bacadetail($i_do, $i_supplier);
	$this->load->view('rekapdo/vformupdate',$data);

}else{
	$this->load->view('awal/index.php');
}	
}
function rekap(){
	if (
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu379')=='t')) ||
		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$this->load->model('sjp/mmaster');
		$jml  = $this->input->post('jml', TRUE);

	for($i=1;$i<=$jml;$i++){		
		$cek=$this->input->post('chk'.$i, TRUE);
		if($cek=='on'){
				$this->db->trans_begin();
			$i_supplier = $this->input->post('i_supplier'.$i, TRUE);
			$i_area = $this->input->post('i_area'.$i, TRUE);
			$i_spb = $this->input->post('i_spb'.$i, TRUE);
			$i_spmb = $this->input->post('i_spmb'.$i, TRUE);
			$d_do = $this->input->post('d_do'.$i, TRUE);
			$i_do = $this->input->post('i_do'.$i, TRUE);
			$i_op = $this->input->post('i_op'.$i, TRUE);
			$i_store = $this->input->post('i_store'.$i, TRUE);
			$i_store_location = $this->input->post('i_store_location'.$i, TRUE);
			$d_spmb = $this->input->post('d_spmb'.$i, TRUE);
			$i_spmb_old = $this->input->post('i_spmb_old'.$i, TRUE);
			$detail = $this->mmaster->bacadetail2($i_do, $i_supplier);
			$d_sj = date('Y-m-d');
			$thbl = date('Ym');
			$vspbnetto = 0;
			$v_sjp = 0;
			$isjold = '';
			$isj		 		= $this->mmaster->runningnumbersj($i_area,$thbl);

			$this->mmaster->insertsjheader($i_spmb,$d_spmb,$isj,$d_sj,$i_area,$vspbnetto,$isjold);
			$a = 0;

			foreach ($detail as $row) {
				$v_sjp = $v_sjp + ($row->n_deliver * $row->v_product_mill);
				$a++;
				$iproduct			= $row->i_product;
				$iproductgrade	= $row->i_product_grade;
				$iproductmotif = $row->i_product_motif;
				$eproductname	= $row->e_product_name;
				$vunitprice		= $row->v_product_mill;
				$ndeliver			= $row->n_deliver;
				$norder		  	= $row->n_deliver;

				$eremark  		= 'Dari DO '.$i_do;
				if($eremark=='')$eremark=null;
				if($norder>0){
					$this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$norder,$ndeliver,
						$vunitprice,$i_spmb,$d_spmb,$isj,$d_sj,$i_area,$i_store,$i_store_location,
						$i_store_location,$eremark,$a,$a);
					$this->mmaster->updatespmbitem($i_spmb,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$i_area);

#                $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
					$trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
					if(isset($trans)){
						foreach($trans as $itrans)
						{
							$q_aw =$itrans->n_quantity_stock;
							$q_ak =$itrans->n_quantity_stock;
#                    $q_in =$itrans->n_quantity_in;
#                    $q_out=$itrans->n_quantity_out;
							$q_in =0;
							$q_out=0;
							break;
						}
					}else{
						$trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00');
						if(isset($trans)){
							foreach($trans as $itrans)
							{
								$q_aw =$itrans->n_quantity_stock;
								$q_ak =$itrans->n_quantity_stock;
								$q_in =0;
								$q_out=0;
								break;
							}
						}else{
							$q_aw=0;
							$q_ak=0;
							$q_in=0;
							$q_out=0;
						}
					}
					$this->mmaster->inserttrans($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$isj,$q_in,$q_out,$ndeliver,$q_aw,$q_ak);
					$th=substr($d_sj,0,4);
					$bl=substr($d_sj,5,2);
					$emutasiperiode=$thbl;
					$ada=$this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$emutasiperiode);
					if($ada=='ada')
					{
						$this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$emutasiperiode);
					}else{
						$this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$emutasiperiode);
					}
					if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,'AA','01','00'))
					{
						$this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$ndeliver,$q_ak);
					}else{
						$this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,'AA','01','00',$eproductname,$ndeliver);
					}
				}

			}
	
			$this->db->query("update tm_sjp set v_sjp = '$v_sjp' where i_sjp = '$isj'");

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Input SJP No:'.$isj;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$data['sukses']			= true;
			$data['inomor']			= $i_do." => ".$isj;
			$this->load->view('nomor',$data);
			if ( ($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
			}
			// END FOREACH
		}
		// end if ch on

	}
	// END FOR JML
}else{
	$this->load->view('awal/index.php');
}	
// END IF SESSION
} 
// END FUNCTION
}
?>
