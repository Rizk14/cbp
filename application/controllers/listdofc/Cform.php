<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listdofc/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('listdo');
			$data['cari'] = '';
      		$data['dfrom']= '';
			$data['dto']  = '';
			$data['isi']  = '';
			$this->load->view('listdofc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdo');
			$this->load->view('listdofc/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$op			= $this->uri->segment(4);
			$ido		= str_replace('%20','',$this->uri->segment(5));
			$isupplier	= $this->uri->segment(6);
			$dfrom	= $this->uri->segment(7);
			$dto		= $this->uri->segment(8);
			$iarea	= $this->uri->segment(9);
			$this->load->model('listdofc/mmaster');
      $this->db->trans_begin();
			$this->mmaster->delete($ido,$isupplier,$op);
      if (($this->db->trans_status() === FALSE))
			{
				$this->db->trans_rollback();
			}else{
#				$this->db->trans_rollback();
				$this->db->trans_commit();
			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Delete DO No:'.$ido.' Area:'.$iarea;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['page_title'] = $this->lang->line('listdo');
			  $config['base_url'] = base_url().'index.php/listdofc/cform/index/';
        $config['base_url'] = base_url().'index.php/listdofc/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			  $cari = strtoupper($this->input->post('cari', FALSE));
			  $sql="select a.i_do
              from tm_dofc a, tr_supplier e
              where a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy')
              and a.i_supplier=e.i_supplier and a.i_area='$iarea'";
			  $query 	= $this->db->query($sql,false);
			  $config['total_rows'] = $query->num_rows();
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(8);
			  $this->pagination->initialize($config);
			  $data['page_title'] = $this->lang->line('listdo');
			  $data['cari']	= $cari;
			  $data['dfrom']	= $dfrom;
			  $data['dto']	= $dto;
			  $data['iarea'] = $iarea;
			  $data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
	      	$now	  = $row['c'];
			  }
			  $pesan='Delete Data DO Area '.$iarea.' Nomor:'.$ido;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );  
			  $this->load->view('listdofc/vmainform',$data);	
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/listdofc/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$iarea = $this->input->post('iarea', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select DISTINCT b.i_area, b.i_do, b.d_do, b.i_op, b.i_supplier, a.e_supplier_name
                   										FROM tm_dofc b
              											JOIN tr_supplier a ON a.i_supplier = b.i_supplier
        												WHERE b.f_do_cancel = 'false' and b.i_area='$iarea'
										            	and 
										              	upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
										              	or upper(i_do) like '%$cari%' or upper(i_op) like '%$cari%'
        												ORDER BY b.i_do, b.d_do, b.i_op, b.i_supplier, a.e_supplier_name
										              ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('listdofc/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$iarea,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('listdo');
			$data['cari'] = '';
			$data['ido']='';
	 		$this->load->view('listdofc/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listdofc/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
        else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('listdofc/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listdofc/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listdofc/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listdofc/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('listdofc/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
				$cari	  = strtoupper($this->input->post('cari'));
				$dfrom	= $this->input->post('dfrom');
				$dto	  = $this->input->post('dto');
				$iarea		= $this->input->post('iarea');
				$is_cari	= $this->input->post('is_cari'); 
				
				if($dfrom=='') $dfrom=$this->uri->segment(4);
				if($dto=='') $dto=$this->uri->segment(5);
				if($iarea=='') $iarea=$this->uri->segment(6);
				
				if ($is_cari == '')
					$is_cari= $this->uri->segment(8);
				if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(7);
			
			if ($is_cari=="1") { 
				$config['base_url'] = base_url().'index.php/listdofc/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/'.$is_cari.'/index/';
			}
			else { 
				$config['base_url'] = base_url().'index.php/listdofc/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			} 
			if ($is_cari != "1") {
				$sql= " select a.i_do, a.d_do, a.i_op, a.i_area, e.e_supplier_name, a.i_supplier
            from tm_dofc a, tr_supplier e
            where a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy')
            and a.i_supplier=e.i_supplier and a.i_area='$iarea'";
			}
			else {
				$sql= " select a.i_do, a.d_do, a.i_op, a.i_area, e.e_supplier_name, a.i_supplier
            from tm_dofc a, tr_supplier e
            where a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy')
            and a.i_supplier=e.i_supplier and a.i_area='$iarea'
            and (upper(a.i_supplier) like '%$cari%' or upper(e.e_supplier_name) like '%$cari%'
							or upper(a.i_do) like '%$cari%' or upper(a.i_op) like '%$cari%')";
			}
			//echo $sql; die();
			//echo $iarea;
			//die();
			$query 	= $this->db->query($sql,false);
			
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if ($is_cari=="1"){
				$config['cur_page'] = $this->uri->segment(10);
			}else{
				$config['cur_page'] = $this->uri->segment(8);
			}
			$this->pagination->initialize($config);
			
			$this->load->model('listdofc/mmaster');
			$data['page_title'] = $this->lang->line('listdo');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;
		//	var_dump($cari,$dfrom,$dto,$iarea);
		//	die();
			if ($is_cari=="1"){
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(10),$cari);
			}else{
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
				}
				//die();
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data DO Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listdofc/vmainform',$data);			

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
