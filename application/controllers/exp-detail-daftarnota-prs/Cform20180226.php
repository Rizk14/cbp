<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exp-detail-daftarnota-prs');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('exp-detail-daftarnota-prs/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-detail-daftarnota-prs/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
#      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }

      $a=substr($iperiode,0,4);
		$b=substr($iperiode,4,2);
		$peri=mbulan($b)." - ".$a;
#			if($iarea=='') $iarea=$this->uri->segment(5);
      $this->db->select("a.i_nota, a.i_seri_pajak,  a.d_nota,  a.i_customer, b.e_customer_name, c.e_customer_pkpnpwp, a.i_area, d.e_area_name, 
						 a.i_salesman, e.e_salesman_name, f.i_product, g.e_product_name, f.n_deliver, f.v_unit_price, f.v_unit_price*f.n_deliver as kotor,
						 a.n_nota_discount1, (n_deliver*v_unit_price)*(n_nota_discount1/100) as totaldiscount1,
						 a.n_nota_discount2,(n_deliver*v_unit_price)*(100-n_nota_discount1)/100 * n_nota_discount2/100 as totaldiscount2,
						 a.n_nota_discount3, ((n_deliver*v_unit_price)*(100-n_nota_discount1)/100 ) * (100-n_nota_discount2)/100 * n_nota_discount3/100 as totaldiscount3,
						 a.n_nota_discount4, ((n_deliver*v_unit_price)*(100-n_nota_discount1)/100 ) * (100-n_nota_discount2)/100 * (100-n_nota_discount3)/100  * 
						 n_nota_discount4/100 as totaldiscount4, 
						 (n_deliver*v_unit_price)*(n_nota_discount1/100)+
						 (n_deliver*v_unit_price)*(100-n_nota_discount1)/100 * n_nota_discount2/100 +
						 ((n_deliver*v_unit_price)*(100-n_nota_discount1)/100 ) * (100-n_nota_discount2)/100 * n_nota_discount3/100 +
						 ((n_deliver*v_unit_price)*(100-n_nota_discount1)/100 ) * (100-n_nota_discount2)/100 * (100-n_nota_discount3)/100  * n_nota_discount4/100  as 
						 totaldiscountrp,
						 f.v_unit_price*f.n_deliver - 
						((n_deliver*v_unit_price)*(n_nota_discount1/100)+
						(n_deliver*v_unit_price)*(100-n_nota_discount1)/100 * n_nota_discount2/100 +
						((n_deliver*v_unit_price)*(100-n_nota_discount1)/100 ) * (100-n_nota_discount2)/100 * n_nota_discount3/100 +
						((n_deliver*v_unit_price)*(100-n_nota_discount1)/100 ) * (100-n_nota_discount2)/100 * (100-n_nota_discount3)/100  * n_nota_discount4/100) as bersih,
						 h.i_product_category,i.i_customer_class,h.e_product_categoryname, i.e_customer_classname
      					 from tm_nota a, tr_customer b, tr_customer_pkp c, tr_area d, tr_salesman e, tm_nota_item f, tr_product g, tr_product_category h, tr_customer_class i
						 where  a.i_customer=c.i_customer AND
								c.i_customer=b.i_customer AND
								a.i_area=d.i_area AND
								a.i_salesman=e.i_salesman AND
								f.i_nota=a.i_nota AND
								g.i_product=f.i_product AND
								h.i_product_category=g.i_product_category AND
								b.i_customer_class=i.i_customer_class AND
	 							not a.i_nota isnull AND
								to_char(a.d_nota,'yyyymm')='$iperiode'
	  						    ORDER BY i_nota",false);
		    
		    $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar Master Data Pelanggan")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(28);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(28);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(9);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(20);


				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar Detail Nota');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,11,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Bulan : '.$peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,11,3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:AB5'
				);
				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No Nota');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);				
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'i_seri_pajak');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tgl Nota');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Kd Langg');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			    $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Nama Pelanggan');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Pkp Npwp');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			    $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Kd Area');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			    $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Nama Area');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Kd Sls');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Nama Sls');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Kd Produk');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Nama Produk');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Jml Kirim');
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O5', 'kotor');
				$objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P5', 'discount1');
				$objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Q5', 'totaldiscount1');
				$objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('R5', 'n_nota_discount2');
				$objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('S5', 'totaldiscount2');
				$objPHPExcel->getActiveSheet()->getStyle('S5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('T5', 'n_nota_discount3');
				$objPHPExcel->getActiveSheet()->getStyle('T5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('U5', 'totaldiscount3');
				$objPHPExcel->getActiveSheet()->getStyle('U5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('V5', 'totaldiscountrp');
				$objPHPExcel->getActiveSheet()->getStyle('V5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('W5', 'Bersih');
				$objPHPExcel->getActiveSheet()->getStyle('W5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('X5', 'Id Kategory');
				$objPHPExcel->getActiveSheet()->getStyle('X5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Y5', 'Nama Kategory');
				$objPHPExcel->getActiveSheet()->getStyle('Y5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Z5', 'Type');
				$objPHPExcel->getActiveSheet()->getStyle('Z5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AA5', 'Type');
				$objPHPExcel->getActiveSheet()->getStyle('AA5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AB5', 'Diskon 4');
				$objPHPExcel->getActiveSheet()->getStyle('AB5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i=6;
				$j=6;
        		$no=0;
				foreach($query->result() as $row)
				{
          		$no++;
          		$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  	array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':AB'.$i
				  );         
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_seri_pajak, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->d_nota, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_customer, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->e_customer_pkpnpwp, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->e_area_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->i_salesman, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->e_salesman_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->i_product, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->e_product_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $row->n_deliver, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->v_unit_price, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $row->kotor, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $row->n_nota_discount1, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $row->totaldiscount1, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->n_nota_discount2, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $row->totaldiscount2, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $row->n_nota_discount3, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $row->totaldiscount3, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('V'.$i, $row->totaldiscountrp, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('W'.$i, $row->bersih, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('X'.$i, $row->i_product_category, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('Y'.$i, $row->e_product_categoryname, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('Z'.$i, $row->i_customer_class, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('AA'.$i, $row->e_customer_classname, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('AB'.$i, $row->totaldiscount4, Cell_DataType::TYPE_STRING);
					$i++;
					$j++;
				}
				$Y=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      	$nama='MasterDatadetail'.$iperiode.'.xls';
			$objWriter->save('excel/00/'.$nama); 

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
      }
      $pesan='Export Detail Master Data Penjualan:'.$iperiode;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses'] = true;
			$data['inomor']	= "Export Detail Master Data Penjualan ".$peri;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>