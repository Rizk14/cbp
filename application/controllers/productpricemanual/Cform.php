<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_productprice_manual');
			$data['iproduct']='';
			$this->load->model('productpricemanual/mmaster');
      		$data['isi']=$this->mmaster->bacakode();
			$cari = strtoupper($this->input->post('cari', FALSE));
			$this->load->view('productpricemanual/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	= $this->input->post('iproduct', TRUE);
			$eproductname 	= $this->input->post('eproductname', TRUE);
			$iproductgrade 	= $this->input->post('iproductgrade', TRUE);
		  $vproductretail1	= $this->input->post('vproductretail1', TRUE);
		  $jml	= $this->input->post('jmlitem', TRUE);
			if ((isset($iproduct) && $iproduct != '') && (isset($eproductname) && $eproductname != '') && (isset($iproductgrade) && $iproductgrade != '') && (isset($vproductretail1) && $vproductretail1 != '' && $vproductretail1 != '0'))
			{
			  $this->load->model('productpricemanual/mmaster');
        $this->db->trans_begin();
        for($i=1;$i<=$jml;$i++){
    		  $ipricegroup	 = $this->input->post('ipricegroup'.$i, TRUE);
    		  $vproductmill	 = $this->input->post('vproductmill'.$i, TRUE);
    		  $vproductretail= $this->input->post('vproductretail'.$i, TRUE);
			    $vproductmill	 = str_replace(",","",$vproductmill);
			    $vproductretail= str_replace(",","",$vproductretail);
			    if($vproductmill=='')
				    $vproductmill=0;
			    if($vproductretail=='')
				    $vproductretail=0;
          if($vproductretail!=0){
				    $query = $this->db->query(" select distinct i_product, i_product_grade, v_product_mill 
										                   	from tr_product_price where i_product='$iproduct' 
											                  and i_product_grade='$iproductgrade' and i_price_group='$ipricegroup'",false);
				    if($query->num_rows()>0){ 
					    $this->mmaster->update($iproduct,$ipricegroup,$eproductname,$iproductgrade,$vproductmill,$vproductretail);
				    }else{
					    $this->mmaster->insert($iproduct,$ipricegroup,$eproductname,$iproductgrade,$vproductmill,$vproductretail);
				    }
          }
        }
        if ( ($this->db->trans_status() === FALSE) )
        {
          $this->db->trans_rollback();
        }else{
          $this->db->trans_commit();
		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs      = pg_query($sql);
		  if(pg_num_rows($rs)>0){
			 while($row=pg_fetch_assoc($rs)){
				$ip_address   = $row['ip_address'];
				break;
			 }
		  }else{
			 $ip_address='kosong';
		  }
		  $query   = pg_query("SELECT current_timestamp as c");
		while($row=pg_fetch_assoc($query)){
		  $now    = $row['c'];
		  }
		  $pesan='Update Harga Barang Manual Kode Barang '.$iproduct;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );

#             $this->db->trans_rollback();
          $data['sukses']         = true;
          $data['inomor']         = $iproduct;
          $this->load->view('nomor',$data);
        }
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_productprice_manual');
			$this->load->view('productpricemanual/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_productprice_manual')." update";
			if( ($this->uri->segment(4)) && ($this->uri->segment(5)) ){
				$iproduct = $this->uri->segment(4);
				$iproductgrade = $this->uri->segment(5);
				$data['iproduct'] = $iproduct;
				$data['iproductgrade'] = $iproductgrade;
				$this->load->model('productpricemanual/mmaster');
				$data['isi']=$this->mmaster->baca($iproduct,$iproductgrade);
		 		$this->load->view('productpricemanual/vmainform',$data);
			}else{
				$this->load->view('productpricemanual/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	= $this->input->post('iproduct', TRUE);
			$eproductname 	= $this->input->post('eproductname', TRUE);
			$iproductgrade 	= $this->input->post('iproductgrade', TRUE);
			$nproductmargin	= $this->input->post('nproductmargin', TRUE);
			$vproductmill	= $this->input->post('vproductmill', TRUE);
			$nproductmargin	= str_replace(",","",$this->input->post('nproductmargin', TRUE));
			$vproductmill	= str_replace(",","",$this->input->post('vproductmill', TRUE));
			if($vproductmill=='')
				$vproductmill=0;			
			$this->load->model('productpricemanual/mmaster');
			$this->mmaster->update($iproduct,$eproductname,$iproductgrade,$nproductmargin,$vproductmill);

			if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                 $this->db->trans_commit();
  #               $this->db->trans_rollback();

                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Update Harga Dengan Kode:'.$iproduct;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );

                 $data['sukses'] = true;
                 $data['inomor'] = $ipl;
                 $this->load->view('nomor',$data);
              }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	= $this->uri->segment(4);
			$iproductgrade	= $this->uri->segment(5);
			$this->load->model('productpricemanual/mmaster');
			$this->mmaster->delete($iproduct,$iproductgrade);
			$config['base_url'] = base_url().'index.php/productpricemanual/cform/index/';
			$query = $this->db->query("select distinct i_product, i_product_grade, v_product_mill 
						   from tr_product_price order by i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '20';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_productprice_manual');
			$data['iproduct']='';
			$data['iproductgrade']='';
			$this->load->model('productpricemanual/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('productpricemanual/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productpricemanual/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_price 
					                        where (upper(tr_product_price.i_product) like '%$cari%' 
					                        or upper(tr_product_price.e_product_name) = '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '20';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('productpricemanual/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_productprice_manual');
			$data['iproduct']='';
			$data['iproductgrade']='';
	 		$this->load->view('productpricemanual/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productgrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productpricemanual/cform/productgrade/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_product_grade from tr_product_grade 
										where upper(i_product_grade) like '%$cari%' or upper(e_product_gradename) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productpricemanual/mmaster');
			$data['page_title'] = $this->lang->line('list_productgrade');
			$data['isi']=$this->mmaster->bacaproductgrade($config['per_page'],$this->uri->segment(5));
			$this->load->view('productpricemanual/vlistproductgrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductgrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productpricemanual/cform/productgrade/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_product_grade from tr_product_grade 
										where upper(i_product_grade) like '%$cari%' or upper(e_product_gradename) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productpricemanual/mmaster');
			$data['page_title'] = $this->lang->line('list_productgrade');
			$data['isi']=$this->mmaster->cariproductgrade($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productpricemanual/vlistproductgrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris=$this->uri->segment(4);
			$cari =$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/productpricemanual/cform/product/'.$baris.'/'.$cari.'/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_product from tr_product
										where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productpricemanual/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($cari,$config['per_page'],$this->uri->segment(7));
			$this->load->view('productpricemanual/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/productpricemanual/cform/product/'.$cari.'/'.'index/';
			$query = $this->db->query("select i_product from tr_product
						   where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productpricemanual/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productpricemanual/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
