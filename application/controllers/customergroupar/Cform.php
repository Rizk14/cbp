<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu461')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customergroupar/cform/index/';
			//$cari = $this->input->post('cari', FALSE);
			//$cari = strtoupper($cari);
			$cari = "all";
			$query = $this->db->query("	select a.i_customer, a.i_customer_groupar as i_group
                                  from tr_customer_groupar a
                                  order by a.i_customer",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_customergroupar');
			$data['icustomer']='';
			$this->load->model('customergroupar/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			
			if ($cari == "all")
				$data['cari']='';
			else
				$data['cari']=$cari;
			
			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Group AR";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('customergroupar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 		= $this->input->post('icustomer', TRUE);
			$ecustomername 	= $this->input->post('ecustomername', TRUE);
			$igroup 	= $this->input->post('igroup', TRUE);

			if ((isset($icustomer) && $icustomer != '') && (isset($ecustomername) && $igroup != ''))
			{
				$this->load->model('customergroupar/mmaster');
				$this->mmaster->insert($icustomer,$ecustomername,$igroup);
				
			  	$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Group AR Pelanggan:('.$icustomer.') -'.$igroup;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

				$config['base_url'] = base_url().'index.php/customergroupar/cform/index/';
				$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);
				$query = $this->db->query("	select a.i_customer, a.i_customer_groupar as i_group
				                  from tr_customer_groupar a
				                  where upper(a.i_customer_groupar) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
				                  order by a.i_customer",false);
				$config['total_rows'] = $query->num_rows();
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->paginationxx->initialize($config);
				$data['page_title'] = $this->lang->line('master_customergroupar');
				$data['icustomer']='';
				$this->load->model('customergroupar/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('customergroupar/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customergroupar');
			$this->load->view('customergroupar/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customergroupar')." update";
			if($this->uri->segment(4)){
				$icustomer = $this->uri->segment(4);
				$igroup = $this->uri->segment(5);
				$data['icustomer'] = $icustomer;
				$data['igroup'] = $igroup;
				$this->load->model('customergroupar/mmaster');
				$data['isi']=$this->mmaster->baca($icustomer);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Group AR Pelanggan:('.$icustomer.') -'.$igroup;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('customergroupar/vmainform',$data);
			}else{
				$this->load->view('customergroupar/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomername 	= $this->input->post('ecustomername', TRUE);
			$igroup	= $this->input->post('igroup', TRUE);
			
			$this->load->model('customergroupar/mmaster');
			$this->mmaster->update($icustomer,$ecustomername,$igroup);
			
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Edit Group AR '.$igroup;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
		  $data['sukses']			= true;
		  $data['inomor']			= $igroup;
		  $this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer	= $this->uri->segment(4);
			$this->load->model('customergroupar/mmaster');
			$this->mmaster->delete($icustomer);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
			  $ip_address	  = $row['ip_address'];
			  break;
			}
			}else{
			$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
			}
			$pesan='Delete Master Group AR Pelanggan:('.$icustomer.')';
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);
			
			$config['base_url'] = base_url().'index.php/customergroupar/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select a.i_customer, b.e_customer_name, a.i_customer_groupbayar as i_group
                                  from tr_customer_groupbayar a, tr_customer b
                                  where a.i_customer=b.i_customer and
                                  upper(b.e_customer_name) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                                  order by a.i_customer",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('master_customergroupar');
			$data['icustomer']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customergroupar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			
			//$cari = $this->input->post('cari', FALSE);
			//$cari = strtoupper($cari);
			
			$cari = $this->input->post('cari', FALSE);
			if ($cari == '')
				$cari= $this->uri->segment(4);
			
			if ($cari == '')
				$cari = "all";
			
			if ($cari != "all")
				$query = $this->db->query("	select a.i_customer, a.i_customer_groupbayar as i_group
                                  from tr_customer_groupbayar a
                                  where upper(a.i_customer_groupbayar) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                                  order by a.i_customer ",false);
            else
				$query = $this->db->query("	select a.i_customer, a.i_customer_groupbayar as i_group
                                  from tr_customer_groupbayar a
                                  order by a.i_customer ",false);
				
			$config['total_rows'] = $query->num_rows();				
			$config['base_url'] = base_url().'index.php/customergroupar/cform/cari/'.$cari.'/';
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('customergroupar/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_customergroupar');
			$data['icustomer']='';
			
			if ($cari == "all")
				$data['cari'] = '';
			else
				$data['cari'] = $cari;
				
	 		$this->load->view('customergroupar/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function pelanggan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){				
			$this->load->model('customergroupar/mmaster');
			
			$config['base_url'] = base_url().'index.php/customergroupar/cform/pelanggan/';	
			$query = $this->db->query("select * from tr_customer order by e_customer_name asc",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacapelanggan($config['per_page'],$this->uri->segment(4));
			$data['cari'] = '';
			$this->load->view('customergroupar/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caripelanggan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('customergroupar/mmaster');
			
			//$cari = $this->input->post('cari', FALSE)?$this->input->post('cari', FALSE):$this->input->get_post('cari', FALSE);
			//$keywordcari	= $cari==''?'null':$cari;
			
			$cari = $this->input->post('cari', FALSE);
			if ($cari == '')
				$cari= $this->uri->segment(4);
			
			if ($cari == '')
				$cari = "all";
			
			//$cari=strtoupper($cari);
			if ($cari != "all")
				$stquery = " select * from tr_customer
						where (upper(i_customer) ilike '%$cari%' or upper(e_customer_name) ilike '%$cari%') order by e_customer_name asc ";
			else
				$stquery = " select * from tr_customer order by e_customer_name asc ";

			$query = $this->db->query($stquery,false);

			$config['total_rows'] = $query->num_rows(); 
			$config['base_url'] = base_url().'index.php/customergroupar/cform/caripelanggan/'.$cari.'/';
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caripelanggan($cari,$config['per_page'],$this->uri->segment(5));
			
			if ($cari == "all")
				$data['cari'] = '';
			else
				$data['cari'] = $cari;
			
			$this->load->view('customergroupar/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function group()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){				
			$this->load->model('customergroupar/mmaster');
			
			$config['base_url'] = base_url().'index.php/customergroupar/cform/group/index/';	
			$query = $this->db->query("select i_customer as i_group from tr_customer order by i_area, e_customer_name asc",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacagroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('customergroupar/vlistgroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carigroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('customergroupar/mmaster');
			
			$cari = $this->input->post('cari', FALSE)?$this->input->post('cari', FALSE):$this->input->get_post('cari', FALSE);
			$keywordcari	= $cari==''?'null':$cari;
			$config['base_url'] = base_url().'index.php/customergroupar/cform/carigroup/index/'.$keywordcari.'/';
			
			$cari=strtoupper($cari);
			$stquery = " select i_customer as i_group from tr_customer
						where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') order by i_area, e_customer_name asc ";

			$query = $this->db->query($stquery,false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->carigroup($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('customergroupar/vlistgroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}		
}
?>
