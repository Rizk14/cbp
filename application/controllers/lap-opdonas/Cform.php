<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu333')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('lap-opdo')." Nasional";
			$data['iperiode']	= '';
			$this->load->view('lap-opdonas/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu333')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/lap-opdonas/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select sum(a.n_deliver) as pcsdo, sum(a.n_deliver*a.v_product_mill) as rpdo,
                                  sum(b.n_order) as pcsop, sum(b.n_order*b.v_product_mill) as rpop,
                                  a.i_product, a.e_product_name, a.i_supplier, d.e_supplier_name
                                  from tm_op_item b
                                  left join tm_do c on (b.i_op=c.i_op)
                                  left join tm_do_item a on (a.i_product=b.i_product and c.i_do=a.i_do)
                                  inner join tr_supplier d on (a.i_supplier=d.i_supplier)
                                  where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                                  group by a.i_product, a.e_product_name, a.i_supplier, d.e_supplier_name
                                  order by a.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('lap-opdonas/mmaster');
			$data['page_title'] = $this->lang->line('lap-opdo')." Nasional";
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->baca($iperiode,$config['per_page'],$this->uri->segment(6),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Laporan OP vs DO:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('lap-opdonas/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu333')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$cari	= strtoupper($this->input->post('cari'));
			$config['base_url'] = base_url().'index.php/lap-opdonas/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select sum(a.n_deliver) as pcsdo, sum(a.n_deliver*a.v_product_mill) as rpdo,
                                  sum(b.n_delivery) as pcsop, sum(b.n_delivery*b.v_product_mill) as rpop,
                                  a.i_product, a.e_product_name, a.i_supplier, d.e_supplier_name
                                  from tm_op_item b
                                  left join tm_do c on (b.i_op=c.i_op)
                                  left join tm_do_item a on (a.i_product=b.i_product and c.i_do=a.i_do)
                                  inner join tr_supplier d on (a.i_supplier=d.i_supplier)
                                  where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                                  and(upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                                  group by a.i_product, a.e_product_name, a.i_supplier, d.e_supplier_name
                                  order by a.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('lap-opdonas/mmaster');
			$data['page_title'] = $this->lang->line('lap-opdo')." Nasional";
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode,$cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('lap-opdonas/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/lap-opdonas/cform/supplier/index/';
			$config['total_rows'] = $this->db->count_all('tr_supplier');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('lap-opdonas/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('lap-opdonas/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/lap-opdonas/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_supplier 
										where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('lap-opdonas/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('lap-opdonas/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
