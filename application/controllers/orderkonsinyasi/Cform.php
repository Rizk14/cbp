<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if 
    (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('orderkonsinyasi');
      $ispg=strtoupper($this->session->userdata("user_id"));
      $iarea=$this->session->userdata("i_area");
      $query = $this->db->query(" select a.i_customer, a.i_area, a.e_spg_name, b.e_area_name, c.e_customer_name 
                                  from tr_spg a, tr_area b, tr_customer c
                                  where upper(a.i_spg) = '$ispg' and a.i_area='$iarea' and a.i_area=b.i_area 
                                  and a.i_customer=c.i_customer");
      if($query->num_rows>0){
        foreach($query->result() as $xx){
          $data['ispg']=$ispg;
          $data['iarea']=$iarea;
    			$data['icustomer'] = $xx->i_customer;
    			$data['eareaname'] = $xx->e_area_name;
    			$data['espgname'] = $xx->e_spg_name;
    			$data['ecustomername'] = $xx->e_customer_name;
        }
        $data['iorderpb']='';
			  $data['isi']='';
			  $data['detail']="";
			  $data['jmlitem']="";
			  $data['tgl']=date('d-m-Y');
			  $this->load->view('orderkonsinyasi/vmainform', $data);
      }else{
        $this->load->view('orderkonsinyasi/vkhususspg', $data);
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iorderpb 		= $this->input->post('iorderpb', TRUE);
			$dorderpb 		= $this->input->post('dorderpb', TRUE);
			if($dorderpb!=''){
				$tmp=explode("-",$dorderpb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dorderpb=$th."-".$bl."-".$hr;
				$thbl=substr($th,2,2).$bl;
			}
			$iarea	= $this->input->post('iarea', TRUE);
			$ispg   = $this->input->post('ispg', TRUE);
			$icustomer= $this->input->post('icustomer', TRUE);
			$jml		= $this->input->post('jml', TRUE);
			if($dorderpb!='' && $jml>0)
			{
        $bisa=true;
        for($j=1;$j<=$jml;$j++){
				  $nquantityorder = $this->input->post('nquantityorder'.$j, TRUE);
				  $nquantityorder	= str_replace(',','',$nquantityorder);
				  $nquantitystock = $this->input->post('nquantitystock'.$j, TRUE);
				  $nquantitystock	= str_replace(',','',$nquantitystock);
          if($nquantityorder=='' || $nquantitystock==''){
            $bisa=false;
            break;
          }
        }
        if($bisa){
				  $this->db->trans_begin();
				  $this->load->model('orderkonsinyasi/mmaster');
				  $iorderpb	=$this->mmaster->runningnumber($thbl);
				  $this->mmaster->insertheader($iorderpb, $dorderpb, $iarea, $ispg, $icustomer);
				  for($i=1;$i<=$jml;$i++){
				    $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				    $iproductgrade	= 'A';
				    $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				    $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				    $nquantityorder = $this->input->post('nquantityorder'.$i, TRUE);
				    $nquantityorder	= str_replace(',','',$nquantityorder);
				    $nquantitystock = $this->input->post('nquantitystock'.$i, TRUE);
				    $nquantitystock	= str_replace(',','',$nquantitystock);
            $eremark        = $this->input->post('eremark'.$i, TRUE);
				    if($nquantityorder>0){
				      $this->mmaster->insertdetail( $iorderpb,$iarea,$icustomer,$dorderpb,$iproduct,$iproductmotif,$iproductgrade,$nquantityorder,$nquantitystock,$i,$eproductname,$eremark);
				    }
				  }
          if (($this->db->trans_status() === FALSE))
				  {
			      $this->db->trans_rollback();
				  }else{
  #			    $this->db->trans_rollback();
			      $this->db->trans_commit();

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		=  $this->db->query($sql);
			      if($rs->num_rows>0){
				      foreach($rs->result() as $tes){
					      $ip_address	  = $tes->ip_address;
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }

			      $data['user']	= $this->session->userdata('user_id');
      #			$data['host']	= $this->session->userdata('printerhost');
			      $data['host']	= $ip_address;
			      $data['uri']	= $this->session->userdata('printeruri');
				    $query 	= pg_query("SELECT current_timestamp as c");
				    while($row=pg_fetch_assoc($query)){
					    $now	  = $row['c'];
				    }
				    $pesan='Input Order Konsinyasi SPG '.$ispg.' No:'.$iorderpb;
				    $this->load->model('logger');
				    $this->logger->write($id, $ip_address, $now , $pesan );

					  $data['sukses']			= true;
					  $data['inomor']			= $iorderpb;
					  $this->load->view('nomor',$data);
				  }
        }
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('orderkonsinyasi');
			$this->load->view('orderkonsinyasi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('orderkonsinyasi')." update";
			if($this->uri->segment(4)!=''){
				$iorderpb  = $this->uri->segment(4);
				$icustomer= $this->uri->segment(5);
				$dfrom    = $this->uri->segment(6);
				$dto 	    = $this->uri->segment(7);
        $iarea=$this->session->userdata("i_area");
				$data['iorderpb']  = $iorderpb;
				$data['icustomer']= $icustomer;
				$data['dfrom'] = $dfrom;
				$data['dto']	 = $dto;
				$data['iarea'] = $iarea;
				$query = $this->db->query("select i_product from tm_orderpb_item where i_orderpb='$iorderpb' and i_customer='$icustomer'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('orderkonsinyasi/mmaster');
				$data['isi']=$this->mmaster->baca($iorderpb,$icustomer);
				$data['detail']=$this->mmaster->bacadetail($iorderpb,$icustomer);
		 		$this->load->view('orderkonsinyasi/vmainform',$data);
			}else{
				$this->load->view('orderkonsinyasi/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$xiorderpb		= $this->input->post('xiorderpb', TRUE);
			$iorderpb 		= $this->input->post('iorderpb', TRUE);
			$dorderpb 		= $this->input->post('dorderpb', TRUE);
			if($dorderpb!=''){
				$tmp=explode("-",$dorderpb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dorderpb=$th."-".$bl."-".$hr;
				$thbl=substr($th,2,2).$bl;
			}
			$iarea	= $this->input->post('iarea', TRUE);

			$ispg   = $this->input->post('ispg', TRUE);
			$icustomer= $this->input->post('icustomer', TRUE);
			$norderpbdiscount= $this->input->post('norderpbdiscount', TRUE);
		  $norderpbdiscount= str_replace(',','',$norderpbdiscount);
			$vorderpbdiscount= $this->input->post('vorderpbdiscount', TRUE);
		  $vorderpbdiscount= str_replace(',','',$vorderpbdiscount);
			$vorderpbgross= $this->input->post('vorderpbgross', TRUE);
		  $vorderpbgross	  	= str_replace(',','',$vorderpbgross);
			$jml		= $this->input->post('jml', TRUE);
			if($dorderpb!='' && $iorderpb!='' && $jml>0)
			{
				$this->db->trans_begin();
				$this->load->model('orderkonsinyasi/mmaster');
#				$iorderpb = 'FB-'.$thbl.'-'.$iorderpb;
				$this->mmaster->deleteheader($xiorderpb, $iarea, $icustomer);
				$this->mmaster->insertheader($iorderpb, $dorderpb, $iarea, $ispg, $icustomer, $norderpbdiscount, $vorderpbdiscount, $vorderpbgross);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= 'A';
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		  = $this->input->post('vunitprice'.$i, TRUE);
				  $vunitprice	  	= str_replace(',','',$vunitprice);
			    $nquantityorder = $this->input->post('nquantityorder'.$i, TRUE);
			    $nquantityorder	= str_replace(',','',$nquantityorder);
			    $nquantitystock = $this->input->post('nquantitystock'.$i, TRUE);
			    $nquantitystock	= str_replace(',','',$nquantitystock);
          $ipricegroupco  = $this->input->post('ipricegroupco'.$i, TRUE);
          $eremark        = $this->input->post('eremark'.$i, TRUE);
          $this->mmaster->deletedetail($iproduct, $iproductgrade, $iorderpb, $iarea, $icustomer, $iproductmotif,$i);
				  if($nquantityorder>0){
				    $this->mmaster->insertdetail($iorderpb,$iarea,$icustomer,$dorderpb,$iproduct,$iproductmotif,$iproductgrade,$nquantityorder,$nquantitystock,$i,$eproductname,$eremark);
				  }
				}
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
#			    $this->db->trans_rollback();
			    $this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		=  $this->db->query($sql);
			    if($rs->num_rows>0){
				    foreach($rs->result() as $tes){
					    $ip_address	  = $tes->ip_address;
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }

			    $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
			    $data['host']	= $ip_address;
			    $data['uri']	= $this->session->userdata('printeruri');
				  $query 	= pg_query("SELECT current_timestamp as c");
				  while($row=pg_fetch_assoc($query)){
					  $now	  = $row['c'];
				  }
				  $pesan='Update Order Konsinyasi SPG '.$ispg.' No:'.$iorderpb;
				  $this->load->model('logger');
				  $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $iorderpb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispmb	= $this->input->post('ispmbdelete', TRUE);
			$this->load->model('orderkonsinyasi/mmaster');
			$this->mmaster->delete($ispmb);
			$data['page_title'] = $this->lang->line('orderkonsinyasi');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('orderkonsinyasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispmb			= $this->input->post('ispmbdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$iarea = $this->input->post('iareadel', TRUE);
			$dfrom= $this->input->post('dfrom', TRUE);
			$dto 	= $this->input->post('dto', TRUE);

			$this->db->trans_begin();
			$this->load->model('orderkonsinyasi/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $ispmb, $iproductmotif);
			if ($this->db->trans_status() === FALSE)
			{
			  $this->db->trans_rollback();
			}else{
			  $this->db->trans_commit();

			$data['ispmb'] = $ispmb;
			$data['iarea'] = $iarea;
			$data['dfrom'] = $dfrom;
			$data['dto']	 = $dto;
			$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ispmb'");
			$data['jmlitem'] = $query->num_rows(); 				

			$this->load->model('orderkonsinyasi/mmaster');
			$data['isi']=$this->mmaster->baca($ispmb);
			$data['detail']=$this->mmaster->bacadetail($ispmb);




	 		$this->load->view('orderkonsinyasi/vmainform',$data);

			/*
  			$data['page_title'] = $this->lang->line('orderkonsinyasi')." Update";
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ispmb'");
				$data['jmlitem']= $query->num_rows(); 				
				$data['ispmb']  = $ispmb;
				$data['isi']    =$this->mmaster->baca($ispmb);
				$data['detail']=$this->mmaster->bacadetail($ispmb);
				$this->load->view('orderkonsinyasi/vmainform', $data);
			*/
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari=strtoupper($this->input->post("cari"));
      $baris=$this->input->post("baris");
			if($baris=='')$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/product/'.$baris.'/';
			$query = $this->db->query(" select a.i_product
										              from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product and upper(a.i_product) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('orderkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$baris;
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('orderkonsinyasi/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/product/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
								and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc ", false);
#c.v_product_mill as harga
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('orderkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('orderkonsinyasi/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/area/index/';
			$allarea	= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($allarea=='t') {
				$query = $this->db->query("select * from tr_area ",false);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') ){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%'
						or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') ",false);				
			} else {
				$query = $this->db->query("select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3'
						or i_area='$area4' or i_area='$area5' ",false);				
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('orderkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5,$allarea);
			$this->load->view('orderkonsinyasi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){

			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/area/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$allarea	= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($allarea=='t') {
				$query = $this->db->query("select * from tr_area ",false);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') ){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%'
						or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') ",false);				
			} else {
				$query = $this->db->query("select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3'
						or i_area='$area4' or i_area='$area5' ",false);				
			}				
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('orderkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5,$allarea);
			$this->load->view('orderkonsinyasi/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('orderkonsinyasi/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('orderkonsinyasi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
#			$data['baris']=$this->uri->segment(4);
#			$baris=$this->uri->segment(4);
      $cari=strtoupper($this->input->post("cari"));
      $baris=$this->input->post("baris");
      $peraw=$this->input->post("peraw");
      $perak=$this->input->post("perak");
      $area =$this->input->post("area");
			if($baris=='')$baris=$this->uri->segment(4);
      if($peraw=='')$peraw=$this->uri->segment(5);
      if($perak=='')$perak=$this->uri->segment(6);
      if($area=='')$area=$this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/productupdate/'.$baris.'/'.$peraw.'/'.$perak.'/'.$area.'/';

#			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/productupdate/'.$baris.'/index/';
			$query = $this->db->query(" select a.i_product from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('orderkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(8),$peraw,$perak,$cari);
			$data['baris']=$baris;
      $data['peraw']=$peraw;
      $data['perak']=$perak;
      $data['area']=$area;
			$this->load->view('orderkonsinyasi/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/productupdate/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_retail as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
										and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc "
									  ,false);
# c.v_product_mill as harga
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';

			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('orderkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('orderkonsinyasi/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/store/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store");
			} else {
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store
                                  and (c.i_area = '$area1' or c.i_area = '$area2' or
                                  c.i_area = '$area3' or c.i_area = '$area4' or
                                  c.i_area = '$area5')");				
			}
			$config['total_rows'] = $query->num_rows(); 	
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('orderkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('orderkonsinyasi/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu296')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
        $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')",false);
      }else{
			  $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')
                                    and (c.i_area = '$area1' or c.i_area = '$area2' or
                                    c.i_area = '$area3' or c.i_area = '$area4' or
                                    c.i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('orderkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('orderkonsinyasi/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
