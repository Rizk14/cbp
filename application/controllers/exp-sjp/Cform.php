<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
		$this->load->model('exp-sjp/mmaster');
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu274') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('exp-sjp');
			$data['dfrom']	= '';
			$data['dto']	= '';

			$this->load->view('exp-sjp/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu274') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$dfrom		= $this->uri->segment('4');
			$dto		= $this->uri->segment('5');

			$tmp = explode("-", $dfrom);
			$th = $tmp[2];
			$bl = $tmp[1];
			$hr = $tmp[0];
			$datefrom = $th . "-" . $bl . "-" . $hr;

			$this->db->select("	a.i_sjp, c.e_area_name , a.d_sjp, a.d_sjp_receive, b.i_product, 
								b.e_product_name, b.n_quantity_deliver, b.n_quantity_receive ,a.i_spmb, a.d_spmb,
								a.d_approve
								from tm_sjp a, tm_sjp_item b, tr_area c
								where a.i_sjp=b.i_sjp and a.i_area=b.i_area and a.i_area=c.i_area and a.f_sjp_cancel='f'
								and a.d_sjp>=to_date('$dfrom','dd-mm-yyyy')
								and a.d_sjp<= to_date('$dto','dd-mm-yyyy')
								/* and not a.d_sjp_receive is null */
								order by a.i_sjp", false);
			$query = $this->db->get();

			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar SJP ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);

			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);

				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar SJP');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 11, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Tanggal : ' . $dfrom . ' s/d ' . $dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 11, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:L5'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'No SPMB');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tgl SPMB');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'No  SJP');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Tgl Kirim');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Tgl Terima');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Tgl Approve');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Jml Kirim');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Jml Terima');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Selisih');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

				$i 	= 6;
				$j	= 6;
				$no = 0;

				foreach ($query->result() as $row) {
					$selisih = $row->n_quantity_receive - $row->n_quantity_deliver;
					$no++;
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':L' . $i
					);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->e_area_name);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->i_spmb);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->d_spmb, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_sjp);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->d_sjp, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->d_sjp_receive, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->d_approve, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->i_product);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('J' . $i, $row->n_quantity_deliver, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('K' . $i, $row->n_quantity_receive, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('L' . $i, $selisih, Cell_DataType::TYPE_NUMERIC);

					$i++;
					$j++;
				}
				$x = $i - 1;
			}

			$nama 		= 'Exp_SJP_' . $dfrom . '_sd_' . $dto . '.xls';

			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output', 'w');

			$this->logger->writenew('Export SJP Tanggal ' . $dfrom . ' s/d ' . $dto);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
