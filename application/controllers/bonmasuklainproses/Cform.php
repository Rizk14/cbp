<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu454')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibmnota	= $this->input->post('ibmnota', TRUE);
			$dprocess	= $this->input->post('dprocess', TRUE);
			if($dprocess!=''){
				$tmp=explode("-",$dprocess);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dprocess=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
			}
			$eremark= $this->input->post('eremark', TRUE);
 			$user		= $this->session->userdata('user_id');
			if($dprocess!='' && $ibmnota!='')
			{
				$this->db->trans_begin();
				$this->load->model('bonmasuklainproses/mmaster');
        $istore				    = 'AA';
				$istorelocation		= '01';
				$istorelocationbin= '00';
        $inota=$this->mmaster->carinota($ibmnota);
        $ispb=$this->mmaster->spb($ibmnota,$inota,$thbl,$dprocess);
        $tmp=$this->mmaster->sj($ibmnota,$inota,$thbl,$dprocess);
        if(count($tmp)>0){
          foreach($tmp as $xy){
            $isj=$xy->i_sj;
            $xtr=$this->mmaster->bacasjdetail($isj);
            foreach($xtr as $trx){
              $iproduct       = $trx->i_product;
              $iproductgrade  = $trx->i_product_grade;
              $iproductmotif  = $trx->i_product_motif;
              $eproductname   = $trx->e_product_name;
              $nquantity      = $trx->n_deliver;
############
              $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_awal;
                  $q_ak =$itrans->n_quantity_akhir;
                  $q_in =$itrans->n_quantity_in;
                  $q_out=$itrans->n_quantity_out;
                  break;
                }
              }else{
                $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                if(isset($trans)){
                  foreach($trans as $itrans)
                  {
                    $q_aw =$itrans->n_quantity_stock;
                    $q_ak =$itrans->n_quantity_stock;
                    $q_in =0;
                    $q_out=0;
                    break;
                  }
                }else{
                  $q_aw=0;
                  $q_ak=0;
                  $q_in=0;
                  $q_out=0;
                }
              }
              $this->mmaster->inserttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$nquantity,$q_aw,$q_ak);
              $th=substr($dprocess,0,4);
              $bl=substr($dprocess,5,2);
              $emutasiperiode=$th.$bl;
              if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
              {
                $this->mmaster->updatemutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
              }else{
                $this->mmaster->insertmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode,$q_aw);
              }
              if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
              {
                $this->mmaster->updateic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$q_ak);
              }else{
                $this->mmaster->insertic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nquantity,$q_aw);
              }
############
            }
				  }
				}
				$nosj=$this->mmaster->nosj($ibmnota,$inota,$thbl,$dprocess);
				$idkb=$this->mmaster->dkb($ibmnota,$inota,$thbl,$dprocess);
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Input Bon Masuk No:'.$ibmnota.' ('.$ispb.' / '.$nosj.' / '.$idkb.')';
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ibmnota.' ('.$ispb.' / '.$nosj.' / '.$idkb.')';
					$this->load->view('nomor',$data);
			    $this->db->trans_commit();
#			    $this->db->trans_rollback();
				}
			}else{
				$data['page_title'] = $this->lang->line('prosesbonmasuk').' Lain-lain';
				$data['ibmnota']='';
        $data['cari']='';
        $data['eremark']='';
				$data['isi']="";
				$data['detail']="";
				$data['jmlitem']="";
				$data['tgl']=date('d-m-Y');
				$this->load->view('bonmasuklainproses/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu454')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('prosesbonmasuk').' Lain-lain';
			$this->load->view('bonmasuklainproses/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bmnota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu454')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari=strtoupper($this->input->post("cari"));
			if($cari=='')$cari=$this->uri->segment(4);
			if($cari=='' or $cari=='sikasep'){
			  $config['base_url'] = base_url().'index.php/bonmasuklainproses/cform/bmnota/sikasep/';
			  $config['total_rows'] = 0;
			}else{
			  $config['base_url'] = base_url().'index.php/bonmasuklainproses/cform/bmnota/'.$cari.'/';
			  $query = $this->db->query(" select i_bmnota from tm_bmnota where f_bmnota_cancel='f' and i_bmnota like '%$cari%'
			                              and i_spbnew isnull",false);
			  $config['total_rows'] = $query->num_rows(); 
			}
		  $config['per_page'] = '10';
		  $config['first_link'] = 'Awal';
		  $config['last_link'] = 'Akhir';
		  $config['next_link'] = 'Selanjutnya';
		  $config['prev_link'] = 'Sebelumnya';
		  $config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bonmasuklainproses/mmaster');
			$data['page_title'] = $this->lang->line('list_bmnota');
			$data['isi']=$this->mmaster->bacabmnota($config['per_page'],$this->uri->segment(5),$cari);
			$data['cari']=$cari;
			$this->load->view('bonmasuklainproses/vlistbmnota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function copy()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu454')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('bonmasuklainproses/mmaster');
			$cari         = $this->uri->segment(4);
			$inota  	    = $this->uri->segment(5);
			$tgl  	      = $this->uri->segment(6);
			$ibmnota      = $this->uri->segment(7);
			$eremark      = $this->uri->segment(8);
			$data['detail'] = $this->mmaster->bacadetail($inota);
  		$data['cari']   = $cari;
			$data['tgl']    = $tgl;
			$data['eremark']= $eremark;
			$data['ibmnota']= $ibmnota;
			$data['inota']  = $inota;
			$data['page_title'] = $this->lang->line('prosesbonmasuk').' Lain-lain';
			$data['isi']=true;
			$this->load->view('bonmasuklainproses/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
