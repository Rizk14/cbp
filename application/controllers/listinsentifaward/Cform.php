<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu329')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listinsentif').' Sales Award';
			$data['iperiode']	= '';
			$this->load->view('listinsentifaward/vform.php', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu329')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			   	$this->load->model('listinsentifaward/mmaster');
				$periode = $this->uri->segment('4');
				$data = array(
					'iperiode' => $periode,
					'isi' => $this->mmaster->baca($periode)
				);
				$this->load->view('listinsentifaward/vformview', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
