<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu405')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-notdet');
			$data['iarea']='';
			$this->load->view('exp-notdet/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu405')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('exp-notdet');
			$this->load->view('exp-notdet/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu405')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-notdet/cform/area/index/';
         $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

         $config['total_rows'] = $query->num_rows(); 
         $config['per_page']   = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('exp-notdet/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
         $this->load->view('exp-notdet/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu405')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-notdet/cform/area/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
                                    and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
         $config['total_rows'] = $query->num_rows(); 
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('exp-notdet/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
         $this->load->view('exp-notdet/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu405')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
         {
            $this->load->model('exp-notdet/mmaster');
            $iarea   = $this->input->post('iarea');
            $dfrom   = $this->input->post('datefrom');
            $dto     = $this->input->post('dateto');

            if($dfrom!=''){
               $tmp=explode("-",$dfrom);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $datefrom=$th."-".$bl."-".$hr;
            }
            if($dto!=''){
               $tmp=explode("-",$dto);
               $thto=$tmp[2];
               $blto=$tmp[1];
               $hrto=$tmp[0];
               $toto=$thto."-".$blto."-".$hrto;
            }
            $istore     = $this->input->post('istore');
            $eareaname  = $this->input->post('eareaname');
            /* $eareaname= 'NASIONAL'; */
            if($iarea=='')$iarea=$this->uri->segment(4);
            $this->load->model('exp-notdet/mmaster');
            if($iarea=='NA') $eareaname='Nasional';
            // if($iarea!='NA'){
            //   $this->db->select("  a.i_spb,
            //                      a.d_spb,
            //                      a.d_approve2,
            //                      a.i_customer,
            //                      a.e_customer_name,
            //                      a.e_customer_classname,
            //                      a.n_customer_toplength,
            //                      a.i_salesman,
            //                      a.e_salesman_name,
            //                      a.i_product,
            //                      a.e_product_name,
            //                      a.e_product_seriname,
            //                      a.kategory,
            //                      a.subkategori,
            //                      a.v_unit_price,
            //                      a.n_order,
            //                      a.n_deliver,
            //                      a.v_spb_gross as spbkotor,
            //                      a.n_spb_discount1,
            //                      a.n_spb_discount2,
            //                      a.n_spb_discount3,
            //                      a.v_spb_disc1,
            //                      a.v_spb_disc2,
            //                      a.v_spb_disc3,
            //                      (a.v_spb_gross) - (a.v_spb_disc1 + a.v_spb_disc2 + a.v_spb_disc3) as spbbersih,
            //                      a.i_nota,
            //                      a.d_nota,
            //                      a.notakotor,
            //                      a.n_nota_discount1,
            //                      a.n_nota_discount2,
            //                      a.n_nota_discount3,
            //                      a.v_nota_disc1,
            //                      a.v_nota_disc2,
            //                      a.v_nota_disc3,
            //                      (a.notakotor) - (a.v_nota_disc1 + a.v_nota_disc2 + a.v_nota_disc3) as notabersih,
            //                      a.i_supplier,
            //                      a.e_supplier_name,
            //                      a.i_price_group,
            //                      a.i_sj,
            //                      a.d_sj,
            //                      a.e_store_name,
            //                      a.i_city,
            //                      a.d_sj_receive,
            //                      a.e_product_typename,
            //                      a.e_provinsi,
            //                      a.i_store,
            //                      a.i_approve1,
            //                      a.i_approve2,
            //                      a.f_spb_stockdaerah,
            //                      a.i_dkb,
            //                      a.f_spb_siapnotasales,
            //                      a.f_spb_siapnotagudang,
            //                      a.i_notapprove,
            //                      a.f_spb_opclose,
            //                      a.f_spb_cancel
            //                   from
            //                      (
            //                      select
            //                         a.f_spb_cancel,
            //                         a.f_spb_opclose,
            //                         a.i_notapprove,
            //                         a.i_spb,
            //                         a.d_spb,
            //                         a.d_approve2,
            //                         a.i_customer,
            //                         b.e_customer_name,
            //                         c.e_customer_classname,
            //                         b.n_customer_toplength,
            //                         a.i_salesman,
            //                         d.e_salesman_name,
            //                         e.i_product,
            //                         e.e_product_name,
            //                         g.e_product_seriname,
            //                         i.e_product_classname as kategory,
            //                         h.e_product_categoryname as subkategori,
            //                         e.v_unit_price,
            //                         e.n_order,
            //                         e.n_deliver,
            //                         e.v_unit_price * e.n_order as v_spb_gross,
            //                         a.n_spb_discount1,
            //                         a.n_spb_discount2,
            //                         a.n_spb_discount3,
            //                         (e.n_order * e.v_unit_price * (a.n_spb_discount1 / 100::numeric(20,
            //                         2)))::numeric(20,
            //                         2) as v_spb_disc1,
            //                         (e.n_order * e.v_unit_price * (1::numeric(20,
            //                         2) - a.n_spb_discount1 / 100::numeric(20,
            //                         2)) * (a.n_spb_discount2 / 100::numeric(20,
            //                         2)))::numeric(20,
            //                         2) as v_spb_disc2,
            //                         (e.n_order * e.v_unit_price * (1::numeric(20,
            //                         2) - a.n_spb_discount1 / 100::numeric(20,
            //                         2)) * (1::numeric(20,
            //                         2) - a.n_spb_discount2 / 100::numeric(20,
            //                         2)) * (a.n_spb_discount3 / 100::numeric(20,
            //                         2)))::numeric(20,
            //                         2) as v_spb_disc3,
            //                         j.i_nota,
            //                         j.d_nota,
            //                         k.n_deliver * k.v_unit_price as notakotor,
            //                         j.n_nota_discount1,
            //                         j.n_nota_discount2,
            //                         j.n_nota_discount3,
            //                         (k.n_deliver * k.v_unit_price * (j.n_nota_discount1 / 100::numeric(20,
            //                         2)))::numeric(20,
            //                         2) as v_nota_disc1,
            //                         (k.n_deliver * k.v_unit_price * (1::numeric(20,
            //                         2) - j.n_nota_discount1 / 100::numeric(20,
            //                         2)) * (j.n_nota_discount2 / 100::numeric(20,
            //                         2)))::numeric(20,
            //                         2) as v_nota_disc2,
            //                         (k.n_deliver * k.v_unit_price * (1::numeric(20,
            //                         2) - j.n_nota_discount1 / 100::numeric(20,
            //                         2)) * (1::numeric(20,
            //                         2) - j.n_nota_discount2 / 100::numeric(20,
            //                         2)) * (j.n_nota_discount3 / 100::numeric(20,
            //                         2)))::numeric(20,
            //                         2) as v_nota_disc3,
            //                         f.i_supplier,
            //                         l.e_supplier_name,
            //                         b.i_price_group,
            //                         j.i_sj,
            //                         j.d_sj,
            //                         m.e_store_name,
            //                         b.i_city,
            //                         j.d_sj_receive,
            //                         o.e_product_typename,
            //                         q.e_provinsi,
            //                         a.i_store,
            //                         a.i_approve1,
            //                         a.i_approve2,
            //                         a.f_spb_stockdaerah,
            //                         j.i_dkb,
            //                         a.f_spb_siapnotasales,
            //                         a.f_spb_siapnotagudang
            //                      from
            //                         tm_spb a
            //                      inner join tr_customer b on
            //                         (a.i_customer = b.i_customer)
            //                      inner join tr_customer_class c on
            //                         (b.i_customer_class = c.i_customer_class)
            //                      inner join tr_salesman d on
            //                         (a.i_salesman = d.i_salesman)
            //                      inner join tm_spb_item e on
            //                         (a.i_spb = e.i_spb
            //                            and a.i_area = e.i_area)
            //                      inner join tr_product f on
            //                         (e.i_product = f.i_product)
            //                      left join tr_product_seri g on
            //                         (f.i_product_seri = g.i_product_seri)
            //                      left join tr_product_category h on
            //                         (f.i_product_category = h.i_product_category)
            //                      left join tr_product_class i on
            //                         (h.i_product_class = i.i_product_class)
            //                      left join tm_nota j on
            //                         (a.i_nota = j.i_nota
            //                            and a.i_sj = j.i_sj
            //                            and a.i_area = j.i_area)
            //                      left join tm_nota_item k on
            //                         (j.i_nota = k.i_nota
            //                            and e.i_product = k.i_product)
            //                      inner join tr_supplier l on
            //                         (f.i_supplier = l.i_supplier)
            //                      inner join tr_store m on
            //                         (a.i_store = m.i_store)
            //                      inner join tr_store_location n on
            //                         (m.i_store = n.i_store
            //                            and a.i_store_location = n.i_store_location)
            //                      inner join tr_product_type o on
            //                         (f.i_product_type = o.i_product_type)
            //                      inner join tr_city p on
            //                         (b.i_area = p.i_area
            //                            and b.i_city = p.i_city)
            //                      inner join tr_area q on
            //                         (j.i_area = q.i_area)
            //                      where
            //                         j.d_nota >= '$datefrom'
            //                         and j.d_nota <= '$toto' ) as a
            //                   order by
            //                      a.i_nota
            //                   ",false);
            // }else{
            //   $this->db->select("  a.i_spb,
            //                        a.d_spb,
            //                        a.d_approve2,
            //                        a.i_customer,
            //                        a.e_customer_name,
            //                        a.e_customer_classname,
            //                        a.n_customer_toplength,
            //                        a.i_salesman,
            //                        a.e_salesman_name,
            //                        a.i_product,
            //                        a.e_product_name,
            //                        a.e_product_seriname,
            //                        a.kategory,
            //                        a.subkategori,
            //                        a.v_unit_price,
            //                        a.n_order,
            //                        a.n_deliver,
            //                        a.v_spb_gross as spbkotor,
            //                        a.n_spb_discount1,
            //                        a.n_spb_discount2,
            //                        a.n_spb_discount3,
            //                        a.v_spb_disc1,
            //                        a.v_spb_disc2,
            //                        a.v_spb_disc3,
            //                        (a.v_spb_gross) - (a.v_spb_disc1 + a.v_spb_disc2 + a.v_spb_disc3) as spbbersih,
            //                        a.i_nota,
            //                        a.d_nota,
            //                        a.notakotor,
            //                        a.n_nota_discount1,
            //                        a.n_nota_discount2,
            //                        a.n_nota_discount3,
            //                        a.v_nota_disc1,
            //                        a.v_nota_disc2,
            //                        a.v_nota_disc3,
            //                        (a.notakotor) - (a.v_nota_disc1 + a.v_nota_disc2 + a.v_nota_disc3) as notabersih,
            //                        a.i_supplier,
            //                        a.e_supplier_name,
            //                        a.i_price_group,
            //                        a.i_sj,
            //                        a.d_sj,
            //                        a.e_store_name,
            //                        a.i_city,
            //                        a.d_sj_receive,
            //                        a.e_product_typename,
            //                        a.e_provinsi,
            //                        a.i_store,
            //                        a.i_approve1,
            //                        a.i_approve2,
            //                        a.f_spb_stockdaerah,
            //                        a.i_dkb,
            //                        a.f_spb_siapnotasales,
            //                        a.f_spb_siapnotagudang,
            //                        a.i_notapprove,
            //                        a.f_spb_opclose,
            //                        a.f_spb_cancel
            //                     from
            //                        (
            //                        select
            //                           a.f_spb_cancel,
            //                           a.f_spb_opclose,
            //                           a.i_notapprove,
            //                           a.i_spb,
            //                           a.d_spb,
            //                           a.d_approve2,
            //                           a.i_customer,
            //                           b.e_customer_name,
            //                           c.e_customer_classname,
            //                           b.n_customer_toplength,
            //                           a.i_salesman,
            //                           d.e_salesman_name,
            //                           e.i_product,
            //                           e.e_product_name,
            //                           g.e_product_seriname,
            //                           i.e_product_classname as kategory,
            //                           h.e_product_categoryname as subkategori,
            //                           e.v_unit_price,
            //                           e.n_order,
            //                           e.n_deliver,
            //                           e.v_unit_price * e.n_order as v_spb_gross,
            //                           a.n_spb_discount1,
            //                           a.n_spb_discount2,
            //                           a.n_spb_discount3,
            //                           (e.n_order * e.v_unit_price * (a.n_spb_discount1 / 100::numeric(20,
            //                           2)))::numeric(20,
            //                           2) as v_spb_disc1,
            //                           (e.n_order * e.v_unit_price * (1::numeric(20,
            //                           2) - a.n_spb_discount1 / 100::numeric(20,
            //                           2)) * (a.n_spb_discount2 / 100::numeric(20,
            //                           2)))::numeric(20,
            //                           2) as v_spb_disc2,
            //                           (e.n_order * e.v_unit_price * (1::numeric(20,
            //                           2) - a.n_spb_discount1 / 100::numeric(20,
            //                           2)) * (1::numeric(20,
            //                           2) - a.n_spb_discount2 / 100::numeric(20,
            //                           2)) * (a.n_spb_discount3 / 100::numeric(20,
            //                           2)))::numeric(20,
            //                           2) as v_spb_disc3,
            //                           j.i_nota,
            //                           j.d_nota,
            //                           k.n_deliver * k.v_unit_price as notakotor,
            //                           j.n_nota_discount1,
            //                           j.n_nota_discount2,
            //                           j.n_nota_discount3,
            //                           (k.n_deliver * k.v_unit_price * (j.n_nota_discount1 / 100::numeric(20,
            //                           2)))::numeric(20,
            //                           2) as v_nota_disc1,
            //                           (k.n_deliver * k.v_unit_price * (1::numeric(20,
            //                           2) - j.n_nota_discount1 / 100::numeric(20,
            //                           2)) * (j.n_nota_discount2 / 100::numeric(20,
            //                           2)))::numeric(20,
            //                           2) as v_nota_disc2,
            //                           (k.n_deliver * k.v_unit_price * (1::numeric(20,
            //                           2) - j.n_nota_discount1 / 100::numeric(20,
            //                           2)) * (1::numeric(20,
            //                           2) - j.n_nota_discount2 / 100::numeric(20,
            //                           2)) * (j.n_nota_discount3 / 100::numeric(20,
            //                           2)))::numeric(20,
            //                           2) as v_nota_disc3,
            //                           f.i_supplier,
            //                           l.e_supplier_name,
            //                           b.i_price_group,
            //                           j.i_sj,
            //                           j.d_sj,
            //                           m.e_store_name,
            //                           b.i_city,
            //                           j.d_sj_receive,
            //                           o.e_product_typename,
            //                           q.e_provinsi,
            //                           a.i_store,
            //                           a.i_approve1,
            //                           a.i_approve2,
            //                           a.f_spb_stockdaerah,
            //                           j.i_dkb,
            //                           a.f_spb_siapnotasales,
            //                           a.f_spb_siapnotagudang
            //                        from
            //                           tm_spb a
            //                        inner join tr_customer b on
            //                           (a.i_customer = b.i_customer)
            //                        inner join tr_customer_class c on
            //                           (b.i_customer_class = c.i_customer_class)
            //                        inner join tr_salesman d on
            //                           (a.i_salesman = d.i_salesman)
            //                        inner join tm_spb_item e on
            //                           (a.i_spb = e.i_spb
            //                              and a.i_area = e.i_area)
            //                        inner join tr_product f on
            //                           (e.i_product = f.i_product)
            //                        left join tr_product_seri g on
            //                           (f.i_product_seri = g.i_product_seri)
            //                        left join tr_product_category h on
            //                           (f.i_product_category = h.i_product_category)
            //                        left join tr_product_class i on
            //                           (h.i_product_class = i.i_product_class)
            //                        left join tm_nota j on
            //                           (a.i_nota = j.i_nota
            //                              and a.i_sj = j.i_sj
            //                              and a.i_area = j.i_area)
            //                        left join tm_nota_item k on
            //                           (j.i_nota = k.i_nota
            //                              and e.i_product = k.i_product)
            //                        inner join tr_supplier l on
            //                           (f.i_supplier = l.i_supplier)
            //                        inner join tr_store m on
            //                           (a.i_store = m.i_store)
            //                        inner join tr_store_location n on
            //                           (m.i_store = n.i_store
            //                              and a.i_store_location = n.i_store_location)
            //                        inner join tr_product_type o on
            //                           (f.i_product_type = o.i_product_type)
            //                        inner join tr_city p on
            //                           (b.i_area = p.i_area
            //                              and b.i_city = p.i_city)
            //                        inner join tr_area q on
            //                           (j.i_area = q.i_area)
            //                        where
            //                           j.d_nota >= '$datefrom'
            //                           and j.d_nota <= '$toto' and a.i_area = '$iarea' ) as a
            //                     order by
            //                        a.i_nota
            //                     ",false);
            // }
            if($iarea!='NA'){
               $this->db->select("   0 as nol,
                                          a.i_spb,
                                          a.d_spb,
                                          a.d_approve2,
                                          a.i_customer,
                                          a.e_customer_name,
                                          a.e_customer_classname,
                                          a.n_customer_toplength,
                                          a.i_salesman,
                                          a.e_salesman_name,
                                          a.i_product,
                                          a.e_product_name,
                                          a.e_product_seriname,
                                          a.kategory,
                                          a.subkategori,
                                          a.v_unit_price,
                                          a.n_order,
                                          a.n_deliver,
                                          a.v_spb_gross as spbkotor,
                                          a.n_spb_discount1,
                                          a.n_spb_discount2,
                                          a.n_spb_discount3,
                                          a.v_spb_disc1,
                                          a.v_spb_disc2,
                                          a.v_spb_disc3,
                                          (a.v_spb_gross) - (a.v_spb_disc1 + a.v_spb_disc2 + a.v_spb_disc3) as spbbersih,
                                          a.i_nota,
                                          a.d_nota,
                                          a.notakotor,
                                          a.n_nota_discount1,
                                          a.n_nota_discount2,
                                          a.n_nota_discount3,
                                          a.v_nota_disc1,
                                          a.v_nota_disc2,
                                          a.v_nota_disc3,
                                          (a.notakotor) - (a.v_nota_disc1 + a.v_nota_disc2 + a.v_nota_disc3) as notabersih,
                                          a.i_supplier,
                                          a.e_supplier_name,
                                          a.i_price_group,
                                          a.i_sj,
                                          a.d_sj,
                                          a.e_store_name,
                                          a.i_city,
                                          a.d_sj_receive,
                                          a.e_product_typename,
                                          a.e_provinsi,
                                          a.i_store,
                                          a.i_approve1,
                                          a.i_approve2,
                                          a.f_spb_stockdaerah,
                                          a.i_dkb,
                                          a.f_spb_siapnotasales,
                                          a.f_spb_siapnotagudang,
                                          a.i_notapprove,
                                          a.f_spb_opclose,
                                          a.f_spb_cancel,
                                          a.i_spb_refference,
                                          a.i_spb_program,
                                          a.e_promo_name,
                                          a.d_spb_entry,
                                          coalesce(rata, 0) / coalesce(notac1, 0) as sttstelat,
                                          case
                                             when is_normal = 'f' then 'Melebihi Rata-rata Keterlambatan'
                                             else ''
                                          end as statusss,
                                          a.e_alasan_lanjut
                                       from
                                          (
                                          select
                                             a.f_spb_cancel,
                                             a.f_spb_opclose,
                                             a.i_notapprove,
                                             a.i_spb,
                                             a.d_spb,
                                             a.d_approve2,
                                             a.i_customer,
                                             b.e_customer_name,
                                             c.e_customer_classname,
                                             b.n_customer_toplength,
                                             a.i_salesman,
                                             d.e_salesman_name,
                                             e.i_product,
                                             e.e_product_name,
                                             g.e_product_seriname,
                                             i.e_product_classname as kategory,
                                             h.e_product_categoryname as subkategori,
                                             e.v_unit_price,
                                             e.n_order,
                                             e.n_deliver,
                                             e.v_unit_price * e.n_order as v_spb_gross,
                                             a.n_spb_discount1,
                                             a.n_spb_discount2,
                                             a.n_spb_discount3,
                                             (e.n_order * e.v_unit_price * (a.n_spb_discount1 / 100::numeric(20,
                                             2)))::numeric(20,
                                             2) as v_spb_disc1,
                                             (e.n_order * e.v_unit_price * (1::numeric(20,
                                             2) - a.n_spb_discount1 / 100::numeric(20,
                                             2)) * (a.n_spb_discount2 / 100::numeric(20,
                                             2)))::numeric(20,
                                             2) as v_spb_disc2,
                                             (e.n_order * e.v_unit_price * (1::numeric(20,
                                             2) - a.n_spb_discount1 / 100::numeric(20,
                                             2)) * (1::numeric(20,
                                             2) - a.n_spb_discount2 / 100::numeric(20,
                                             2)) * (a.n_spb_discount3 / 100::numeric(20,
                                             2)))::numeric(20,
                                             2) as v_spb_disc3,
                                             j.i_nota,
                                             j.d_nota,
                                             k.n_deliver * k.v_unit_price as notakotor,
                                             j.n_nota_discount1,
                                             j.n_nota_discount2,
                                             j.n_nota_discount3,
                                             (k.n_deliver * k.v_unit_price * (j.n_nota_discount1 / 100::numeric(20,
                                             2)))::numeric(20,
                                             2) as v_nota_disc1,
                                             (k.n_deliver * k.v_unit_price * (1::numeric(20,
                                             2) - j.n_nota_discount1 / 100::numeric(20,
                                             2)) * (j.n_nota_discount2 / 100::numeric(20,
                                             2)))::numeric(20,
                                             2) as v_nota_disc2,
                                             (k.n_deliver * k.v_unit_price * (1::numeric(20,
                                             2) - j.n_nota_discount1 / 100::numeric(20,
                                             2)) * (1::numeric(20,
                                             2) - j.n_nota_discount2 / 100::numeric(20,
                                             2)) * (j.n_nota_discount3 / 100::numeric(20,
                                             2)))::numeric(20,
                                             2) as v_nota_disc3,
                                             f.i_supplier,
                                             l.e_supplier_name,
                                             b.i_price_group,
                                             j.i_sj,
                                             j.d_sj,
                                             m.e_store_name,
                                             b.i_city,
                                             j.d_sj_receive,
                                             o.e_product_typename,
                                             q.e_provinsi,
                                             a.i_store,
                                             a.i_approve1,
                                             a.i_approve2,
                                             a.f_spb_stockdaerah,
                                             j.i_dkb,
                                             a.f_spb_siapnotasales,
                                             a.f_spb_siapnotagudang,
                                             a.i_spb_refference,
                                             a.i_spb_program,
                                             r.e_promo_name,
                                             a.d_spb_entry,
                                             case
                                                when a.v_spb > s.v_saldo then 'OVER PLAFOND'
                                                else ''
                                             end as sttsplafond,
                                             (u.d_alokasi - j.d_jatuh_tempo) as rata,
                                             v.nota_count as notac1,
                                             v.is_avg_normal as is_normal,
                                             j.e_alasan_lanjut
                                          from
                                             tm_spb a
                                          inner join tr_customer b on
                                             (a.i_customer = b.i_customer)
                                          inner join tr_customer_class c on
                                             (b.i_customer_class = c.i_customer_class)
                                          inner join tr_salesman d on
                                             (a.i_salesman = d.i_salesman)
                                          inner join tm_spb_item e on
                                             (a.i_spb = e.i_spb
                                                and a.i_area = e.i_area)
                                          inner join tr_product f on
                                             (e.i_product = f.i_product)
                                          left join tr_product_seri g on
                                             (f.i_product_seri = g.i_product_seri)
                                          left join tr_product_category h on
                                             (f.i_product_category = h.i_product_category)
                                          left join tr_product_class i on
                                             (h.i_product_class = i.i_product_class)
                                          left join tm_nota j on
                                             (a.i_nota = j.i_nota
                                                and a.i_sj = j.i_sj
                                                and a.i_area = j.i_area)
                                          left join tm_nota_item k on
                                             (j.i_nota = k.i_nota
                                                and e.i_product = k.i_product)
                                          inner join tr_supplier l on
                                             (f.i_supplier = l.i_supplier)
                                          inner join tr_store m on
                                             (a.i_store = m.i_store)
                                          inner join tr_store_location n on
                                             (m.i_store = n.i_store
                                                and a.i_store_location = n.i_store_location)
                                          inner join tr_product_type o on
                                             (f.i_product_type = o.i_product_type)
                                          inner join tr_city p on
                                             (b.i_area = p.i_area
                                                and b.i_city = p.i_city)
                                          inner join tr_area q on
                                             (j.i_area = q.i_area)
                                          left join tm_promo r on
                                             (a.i_spb_program = r.i_promo)
                                          left join tr_customer_groupar s on
                                             (a.i_customer = s.i_customer)
                                          left join tm_alokasi_item t on
                                             (j.i_nota = t.i_nota)
                                          left join tm_alokasi u on
                                             (t.i_alokasi = u.i_alokasi
                                                and t.i_area = u.i_area)
                                          left join (
                                             select
                                                a.i_spb,
                                                a.i_customer,
                                                rata.e_customer_name,
                                                a.i_area,
                                                overfl.v_flapond,
                                                overfl.v_spb,
                                                overfl.v_saldo,
                                                coalesce(rata.n_customer_toplength, 9999) n_customer_toplength,
                                                coalesce(rata.sumketerlambatan, 9999) sumketerlambatan,
                                                coalesce(rata.nota_count, 1) nota_count,
                                                coalesce(rata.rata_keterlambatan, 9999) rata_keterlambatan,
                                                overfl.is_normal is_flapond_normal,
                                                coalesce(rata.is_normal, false) is_avg_normal ,
                                                case
                                                   when overfl.is_normal = 't'
                                                      and rata.is_normal = 't' then true
                                                      else false
                                                   end as all_normal
                                                from
                                                   tm_spb a
                                                left join(/*over flapond*/
                                                   select
                                                      i_spb,
                                                      i_customer,
                                                      v_flapond,
                                                      v_spb,
                                                      v_saldo,
                                                      case
                                                         when v_saldo > 0 then true
                                                         else false
                                                      end as is_normal
                                                   from
                                                      (
                                                      select
                                                         *,
                                                         v_flapond-(piutang + sum(v_spb) over(partition by i_customer
                                                      order by
                                                         i_customer,
                                                         id)) as v_saldo
                                                      from
                                                         (
                                                         select
                                                            row_number() over(partition by a.i_customer
                                                         order by
                                                            replace(a.i_spb, substring(i_spb, 10, 2), '00'),
                                                            a.i_customer,
                                                            a.d_spb) id,
                                                            case
                                                               when substring(i_spb, 10, 2) != '' then replace(a.i_spb, substring(i_spb, 10, 2), '00')
                                                            end as id_spb,
                                                            a.i_spb,
                                                            a.i_customer,
                                                            c.v_flapond,
                                                            coalesce(b.piutang, 0) piutang,
                                                            case
                                                               when (a.v_spb_after = '0'
                                                                  or a.v_spb_after is null) then sum(v_spb-v_spb_discounttotal)
                                                               else a.v_spb_after
                                                            end as v_spb,
                                                            a.d_spb
                                                         from
                                                            tm_spb a
                                                         left join (
                                                            select
                                                               i_customer,
                                                               i_area,
                                                               sum(v_sisa) as piutang
                                                            from
                                                               tm_nota
                                                            where
                                                               f_nota_cancel = 'f'
                                                               and i_nota is not null
                                                            group by
                                                               1,
                                                               2) b on
                                                            (a.i_customer = b.i_customer
                                                               and a.i_area = b.i_area)
                                                         left join tr_customer_groupar c on
                                                            (a.i_customer = c.i_customer
                                                               and a.i_area = substring(c.i_customer, 1, 2))
                                                         where
                                                            a.f_spb_cancel = 'f'
                                                            and a.d_spb >= '$datefrom'
                                                            and a.d_spb <= '$toto'
                                                            and a.i_area = '$iarea'
                                                            and a.i_nota is null
                                                            and substring(a.i_customer, 3, 3) != '000'
                                                         group by
                                                            2,
                                                            3,
                                                            4,
                                                            5,
                                                            6,
                                                            a.i_area
                                                         order by
                                                            a.i_customer,
                                                            a.d_spb,
                                                            2 ) x1 ) x2
                                                   order by
                                                      2,
                                                      1 ) overfl on
                                                   (a.i_customer = overfl.i_customer
                                                      and a.i_area = substring(overfl.i_customer, 1, 2)
                                                         and a.i_spb = overfl.i_spb)
                                                left join (
                                                   select
                                                      i_customer,
                                                      e_customer_name,
                                                      n_customer_toplength,
                                                      sumketerlambatan,
                                                      nota_count,
                                                      rata_keterlambatan,
                                                      case
                                                         when n_customer_toplength = 0 then false
                                                         when (n_customer_toplength >= 30
                                                            and n_customer_toplength <= 35)
                                                         and rata_keterlambatan <= 50 then true
                                                         when n_customer_toplength = 45
                                                         and rata_keterlambatan <= 60 then true
                                                         when n_customer_toplength = 60
                                                         and rata_keterlambatan <= 70 then true
                                                         else false
                                                      end as is_normal
                                                   from
                                                      (
                                                      select
                                                         i_customer,
                                                         e_customer_name,
                                                         n_customer_toplength,
                                                         sum(sumketerlambatan) sumketerlambatan,
                                                         sum(nota_count) nota_count,
                                                         round((sum(sumketerlambatan)/ sum(nota_count))) rata_keterlambatan
                                                      from
                                                         (
                                                         select
                                                            row_number() over(partition by i_customer
                                                         order by
                                                            substring(i_nota, 1, 7) desc) as rownumber,
                                                            i_customer,
                                                            e_customer_name,
                                                            n_customer_toplength ,
                                                            substring(i_nota, 1, 7) inota ,
                                                            sum(1) as nota_count ,
                                                            sum(d_alokasi-d_sj_receive-n_customer_toplength) as sumketerlambatan
                                                         from
                                                            (
                                                            select
                                                               a.i_customer,
                                                               c.e_customer_name,
                                                               a.i_nota,
                                                               c.n_customer_toplength,
                                                               a.v_sisa,
                                                               b.d_alokasi,
                                                               case
                                                                  when a.d_sj_receive is null then a.d_nota
                                                                  when a.d_sj_receive is not null then a.d_sj_receive
                                                               end as d_sj_receive
                                                            from
                                                               tm_nota a
                                                            inner join (
                                                               select
                                                                  pli.i_alokasi,
                                                                  pli.i_area,
                                                                  pli.i_nota,
                                                                  pli.v_jumlah,
                                                                  pl.i_giro,
                                                                  pl.d_alokasi
                                                               from
                                                                  tm_alokasi_item pli
                                                               inner join tm_alokasi pl on
                                                                  (pli.i_alokasi = pl.i_alokasi
                                                                     and pli.i_area = pl.i_area) ) b on
                                                               (a.i_nota = b.i_nota
                                                                  and a.i_area = b.i_area)
                                                            left join tr_customer c on
                                                               (a.i_customer = c.i_customer)
                                                            where
                                                               f_nota_cancel = 'f'
                                                               and c.f_customer_aktif = 't'
                                                            order by
                                                               1,
                                                               3 ) x1
                                                         group by
                                                            2,
                                                            3,
                                                            4,
                                                            5 ) x2
                                                      where
                                                         rownumber <= 6
                                                      group by
                                                         1,
                                                         2,
                                                         3 ) x3 ) rata on
                                                   (a.i_customer = rata.i_customer
                                                      and a.i_area = substring(rata.i_customer, 1, 2))
                                                where
                                                   a.f_spb_cancel = 'f'
                                                   and a.d_spb >= '$datefrom'
                                                   and a.d_spb <= '$toto'
                                                   and a.i_area = '$iarea'
                                                   and a.i_customer not like '%000' ) v on
                                             (a.i_spb = v.i_spb
                                                and a.i_area = v.i_area)
                                          where
                                             j.d_nota >= '$datefrom'
                                             and j.d_nota <= '$toto'
                                             and a.i_area = '$iarea' ) as a
                                       order by
                                          a.i_nota
                                     ",false);
             }else{
               $this->db->select("  0 as nol,
                                    a.i_spb,
                                    a.d_spb,
                                    a.d_approve2,
                                    a.i_customer,
                                    a.e_customer_name,
                                    a.e_customer_classname,
                                    a.n_customer_toplength,
                                    a.i_salesman,
                                    a.e_salesman_name,
                                    a.i_product,
                                    a.e_product_name,
                                    a.e_product_seriname,
                                    a.kategory,
                                    a.subkategori,
                                    a.v_unit_price,
                                    a.n_order,
                                    a.n_deliver,
                                    a.v_spb_gross as spbkotor,
                                    a.n_spb_discount1,
                                    a.n_spb_discount2,
                                    a.n_spb_discount3,
                                    a.v_spb_disc1,
                                    a.v_spb_disc2,
                                    a.v_spb_disc3,
                                    (a.v_spb_gross) - (a.v_spb_disc1 + a.v_spb_disc2 + a.v_spb_disc3) as spbbersih,
                                    a.i_nota,
                                    a.d_nota,
                                    a.notakotor,
                                    a.n_nota_discount1,
                                    a.n_nota_discount2,
                                    a.n_nota_discount3,
                                    a.v_nota_disc1,
                                    a.v_nota_disc2,
                                    a.v_nota_disc3,
                                    (a.notakotor) - (a.v_nota_disc1 + a.v_nota_disc2 + a.v_nota_disc3) as notabersih,
                                    a.i_supplier,
                                    a.e_supplier_name,
                                    a.i_price_group,
                                    a.i_sj,
                                    a.d_sj,
                                    a.e_store_name,
                                    a.i_city,
                                    a.d_sj_receive,
                                    a.e_product_typename,
                                    a.e_provinsi,
                                    a.i_store,
                                    a.i_approve1,
                                    a.i_approve2,
                                    a.f_spb_stockdaerah,
                                    a.i_dkb,
                                    a.f_spb_siapnotasales,
                                    a.f_spb_siapnotagudang,
                                    a.i_notapprove,
                                    a.f_spb_opclose,
                                    a.f_spb_cancel,
                                    a.i_spb_refference,
                                    a.i_spb_program,
                                    a.e_promo_name,
                                    a.d_spb_entry,
                                    coalesce(rata, 0) / coalesce(notac1, 0) as sttstelat,
                                    case
                                       when is_normal = 'f' then 'Melebihi Rata-rata Keterlambatan'
                                       else ''
                                    end as statusss,
                                    a.e_alasan_lanjut
                                 from
                                    (
                                    select
                                       a.f_spb_cancel,
                                       a.f_spb_opclose,
                                       a.i_notapprove,
                                       a.i_spb,
                                       a.d_spb,
                                       a.d_approve2,
                                       a.i_customer,
                                       b.e_customer_name,
                                       c.e_customer_classname,
                                       b.n_customer_toplength,
                                       a.i_salesman,
                                       d.e_salesman_name,
                                       e.i_product,
                                       e.e_product_name,
                                       g.e_product_seriname,
                                       i.e_product_classname as kategory,
                                       h.e_product_categoryname as subkategori,
                                       e.v_unit_price,
                                       e.n_order,
                                       e.n_deliver,
                                       e.v_unit_price * e.n_order as v_spb_gross,
                                       a.n_spb_discount1,
                                       a.n_spb_discount2,
                                       a.n_spb_discount3,
                                       (e.n_order * e.v_unit_price * (a.n_spb_discount1 / 100::numeric(20,
                                       2)))::numeric(20,
                                       2) as v_spb_disc1,
                                       (e.n_order * e.v_unit_price * (1::numeric(20,
                                       2) - a.n_spb_discount1 / 100::numeric(20,
                                       2)) * (a.n_spb_discount2 / 100::numeric(20,
                                       2)))::numeric(20,
                                       2) as v_spb_disc2,
                                       (e.n_order * e.v_unit_price * (1::numeric(20,
                                       2) - a.n_spb_discount1 / 100::numeric(20,
                                       2)) * (1::numeric(20,
                                       2) - a.n_spb_discount2 / 100::numeric(20,
                                       2)) * (a.n_spb_discount3 / 100::numeric(20,
                                       2)))::numeric(20,
                                       2) as v_spb_disc3,
                                       j.i_nota,
                                       j.d_nota,
                                       k.n_deliver * k.v_unit_price as notakotor,
                                       j.n_nota_discount1,
                                       j.n_nota_discount2,
                                       j.n_nota_discount3,
                                       (k.n_deliver * k.v_unit_price * (j.n_nota_discount1 / 100::numeric(20,
                                       2)))::numeric(20,
                                       2) as v_nota_disc1,
                                       (k.n_deliver * k.v_unit_price * (1::numeric(20,
                                       2) - j.n_nota_discount1 / 100::numeric(20,
                                       2)) * (j.n_nota_discount2 / 100::numeric(20,
                                       2)))::numeric(20,
                                       2) as v_nota_disc2,
                                       (k.n_deliver * k.v_unit_price * (1::numeric(20,
                                       2) - j.n_nota_discount1 / 100::numeric(20,
                                       2)) * (1::numeric(20,
                                       2) - j.n_nota_discount2 / 100::numeric(20,
                                       2)) * (j.n_nota_discount3 / 100::numeric(20,
                                       2)))::numeric(20,
                                       2) as v_nota_disc3,
                                       f.i_supplier,
                                       l.e_supplier_name,
                                       b.i_price_group,
                                       j.i_sj,
                                       j.d_sj,
                                       m.e_store_name,
                                       b.i_city,
                                       j.d_sj_receive,
                                       o.e_product_typename,
                                       q.e_provinsi,
                                       a.i_store,
                                       a.i_approve1,
                                       a.i_approve2,
                                       a.f_spb_stockdaerah,
                                       j.i_dkb,
                                       a.f_spb_siapnotasales,
                                       a.f_spb_siapnotagudang,
                                       a.i_spb_refference,
                                       a.i_spb_program,
                                       r.e_promo_name,
                                       a.d_spb_entry,
                                       case
                                          when a.v_spb > s.v_saldo then 'OVER PLAFOND'
                                          else ''
                                       end as sttsplafond,
                                       (u.d_alokasi - j.d_jatuh_tempo) as rata,
                                       v.nota_count as notac1,
                                       v.is_avg_normal as is_normal,
                                       j.e_alasan_lanjut
                                    from
                                       tm_spb a
                                    inner join tr_customer b on
                                       (a.i_customer = b.i_customer)
                                    inner join tr_customer_class c on
                                       (b.i_customer_class = c.i_customer_class)
                                    inner join tr_salesman d on
                                       (a.i_salesman = d.i_salesman)
                                    inner join tm_spb_item e on
                                       (a.i_spb = e.i_spb
                                          and a.i_area = e.i_area)
                                    inner join tr_product f on
                                       (e.i_product = f.i_product)
                                    left join tr_product_seri g on
                                       (f.i_product_seri = g.i_product_seri)
                                    left join tr_product_category h on
                                       (f.i_product_category = h.i_product_category)
                                    left join tr_product_class i on
                                       (h.i_product_class = i.i_product_class)
                                    left join tm_nota j on
                                       (a.i_nota = j.i_nota
                                          and a.i_sj = j.i_sj
                                          and a.i_area = j.i_area)
                                    left join tm_nota_item k on
                                       (j.i_nota = k.i_nota
                                          and e.i_product = k.i_product)
                                    inner join tr_supplier l on
                                       (f.i_supplier = l.i_supplier)
                                    inner join tr_store m on
                                       (a.i_store = m.i_store)
                                    inner join tr_store_location n on
                                       (m.i_store = n.i_store
                                          and a.i_store_location = n.i_store_location)
                                    inner join tr_product_type o on
                                       (f.i_product_type = o.i_product_type)
                                    inner join tr_city p on
                                       (b.i_area = p.i_area
                                          and b.i_city = p.i_city)
                                    inner join tr_area q on
                                       (j.i_area = q.i_area)
                                    left join tm_promo r on
                                       (a.i_spb_program = r.i_promo)
                                    left join tr_customer_groupar s on
                                       (a.i_customer = s.i_customer)
                                    left join tm_alokasi_item t on
                                       (j.i_nota = t.i_nota)
                                    left join tm_alokasi u on
                                       (t.i_alokasi = u.i_alokasi
                                          and t.i_area = u.i_area)
                                    left join (
                                       select
                                          a.i_spb,
                                          a.i_customer,
                                          rata.e_customer_name,
                                          a.i_area,
                                          overfl.v_flapond,
                                          overfl.v_spb,
                                          overfl.v_saldo,
                                          coalesce(rata.n_customer_toplength, 9999) n_customer_toplength,
                                          coalesce(rata.sumketerlambatan, 9999) sumketerlambatan,
                                          coalesce(rata.nota_count, 1) nota_count,
                                          coalesce(rata.rata_keterlambatan, 9999) rata_keterlambatan,
                                          overfl.is_normal is_flapond_normal,
                                          coalesce(rata.is_normal, false) is_avg_normal ,
                                          case
                                             when overfl.is_normal = 't'
                                                and rata.is_normal = 't' then true
                                                else false
                                             end as all_normal
                                          from
                                             tm_spb a
                                          left join(/*over flapond*/
                                             select
                                                i_spb,
                                                i_customer,
                                                v_flapond,
                                                v_spb,
                                                v_saldo,
                                                case
                                                   when v_saldo > 0 then true
                                                   else false
                                                end as is_normal
                                             from
                                                (
                                                select
                                                   *,
                                                   v_flapond-(piutang + sum(v_spb) over(partition by i_customer
                                                order by
                                                   i_customer,
                                                   id)) as v_saldo
                                                from
                                                   (
                                                   select
                                                      row_number() over(partition by a.i_customer
                                                   order by
                                                      replace(a.i_spb, substring(i_spb, 10, 2), '00'),
                                                      a.i_customer,
                                                      a.d_spb) id,
                                                      case
                                                         when substring(i_spb, 10, 2) != '' then replace(a.i_spb, substring(i_spb, 10, 2), '00')
                                                      end as id_spb,
                                                      a.i_spb,
                                                      a.i_customer,
                                                      c.v_flapond,
                                                      coalesce(b.piutang, 0) piutang,
                                                      case
                                                         when (a.v_spb_after = '0'
                                                            or a.v_spb_after is null) then sum(v_spb-v_spb_discounttotal)
                                                         else a.v_spb_after
                                                      end as v_spb,
                                                      a.d_spb
                                                   from
                                                      tm_spb a
                                                   left join (
                                                      select
                                                         i_customer,
                                                         i_area,
                                                         sum(v_sisa) as piutang
                                                      from
                                                         tm_nota
                                                      where
                                                         f_nota_cancel = 'f'
                                                         and i_nota is not null
                                                      group by
                                                         1,
                                                         2) b on
                                                      (a.i_customer = b.i_customer
                                                         and a.i_area = b.i_area)
                                                   left join tr_customer_groupar c on
                                                      (a.i_customer = c.i_customer
                                                         and a.i_area = substring(c.i_customer, 1, 2))
                                                   where
                                                      a.f_spb_cancel = 'f'
                                                      and a.d_spb >= '$datefrom'
                                                      and a.d_spb <= '$toto'
                                                      and a.i_nota is null
                                                      and substring(a.i_customer, 3, 3) != '000'
                                                   group by
                                                      2,
                                                      3,
                                                      4,
                                                      5,
                                                      6,
                                                      a.i_area
                                                   order by
                                                      a.i_customer,
                                                      a.d_spb,
                                                      2 ) x1 ) x2
                                             order by
                                                2,
                                                1 ) overfl on
                                             (a.i_customer = overfl.i_customer
                                                and a.i_area = substring(overfl.i_customer, 1, 2)
                                                   and a.i_spb = overfl.i_spb)
                                          left join (
                                             select
                                                i_customer,
                                                e_customer_name,
                                                n_customer_toplength,
                                                sumketerlambatan,
                                                nota_count,
                                                rata_keterlambatan,
                                                case
                                                   when n_customer_toplength = 0 then false
                                                   when (n_customer_toplength >= 30
                                                      and n_customer_toplength <= 35)
                                                   and rata_keterlambatan <= 50 then true
                                                   when n_customer_toplength = 45
                                                   and rata_keterlambatan <= 60 then true
                                                   when n_customer_toplength = 60
                                                   and rata_keterlambatan <= 70 then true
                                                   else false
                                                end as is_normal
                                             from
                                                (
                                                select
                                                   i_customer,
                                                   e_customer_name,
                                                   n_customer_toplength,
                                                   sum(sumketerlambatan) sumketerlambatan,
                                                   sum(nota_count) nota_count,
                                                   round((sum(sumketerlambatan)/ sum(nota_count))) rata_keterlambatan
                                                from
                                                   (
                                                   select
                                                      row_number() over(partition by i_customer
                                                   order by
                                                      substring(i_nota, 1, 7) desc) as rownumber,
                                                      i_customer,
                                                      e_customer_name,
                                                      n_customer_toplength ,
                                                      substring(i_nota, 1, 7) inota ,
                                                      sum(1) as nota_count ,
                                                      sum(d_alokasi-d_sj_receive-n_customer_toplength) as sumketerlambatan
                                                   from
                                                      (
                                                      select
                                                         a.i_customer,
                                                         c.e_customer_name,
                                                         a.i_nota,
                                                         c.n_customer_toplength,
                                                         a.v_sisa,
                                                         b.d_alokasi,
                                                         case
                                                            when a.d_sj_receive is null then a.d_nota
                                                            when a.d_sj_receive is not null then a.d_sj_receive
                                                         end as d_sj_receive
                                                      from
                                                         tm_nota a
                                                      inner join (
                                                         select
                                                            pli.i_alokasi,
                                                            pli.i_area,
                                                            pli.i_nota,
                                                            pli.v_jumlah,
                                                            pl.i_giro,
                                                            pl.d_alokasi
                                                         from
                                                            tm_alokasi_item pli
                                                         inner join tm_alokasi pl on
                                                            (pli.i_alokasi = pl.i_alokasi
                                                               and pli.i_area = pl.i_area) ) b on
                                                         (a.i_nota = b.i_nota
                                                            and a.i_area = b.i_area)
                                                      left join tr_customer c on
                                                         (a.i_customer = c.i_customer)
                                                      where
                                                         f_nota_cancel = 'f'
                                                         and c.f_customer_aktif = 't'
                                                      order by
                                                         1,
                                                         3 ) x1
                                                   group by
                                                      2,
                                                      3,
                                                      4,
                                                      5 ) x2
                                                where
                                                   rownumber <= 6
                                                group by
                                                   1,
                                                   2,
                                                   3 ) x3 ) rata on
                                             (a.i_customer = rata.i_customer
                                                and a.i_area = substring(rata.i_customer, 1, 2))
                                          where
                                             a.f_spb_cancel = 'f'
                                             and a.d_spb >= '$datefrom'
                                             and a.d_spb <= '$toto'
                                             and a.i_customer not like '%000' ) v on
                                       (a.i_spb = v.i_spb
                                          and a.i_area = v.i_area)
                                    where
                                       j.d_nota >= '$datefrom'
                                       and j.d_nota <= '$toto'
                                       and a.i_area = '01' ) as a
                                 order by
                                    a.i_nota
                                 ",false);
             }

            $query = $this->db->get();
            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("Customer List")->setDescription(NmPerusahaan);
            $objPHPExcel->setActiveSheetIndex(0);
            if ($query->num_rows() > 0)
            {
               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'	=> 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A2:A4'
               );
               $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
               $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(17);
               $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
               $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
               $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
               $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
               $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
               $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
               $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
               $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(15);

               $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(17);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setWidth(17);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AY')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('AZ')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('BB')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('BC')->setWidth(12);

               $objPHPExcel->getActiveSheet()->setCellValue('A2', 'EXport SPB VS NOTA ');
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,53,2);
               if($iarea!='NA'){
                 $objPHPExcel->getActiveSheet()->setCellValue('A3', "Periode : ".$datefrom." - ".$toto);
               }else{
                 $objPHPExcel->getActiveSheet()->setCellValue('A3', "Area : ".$eareaname);
               }
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,53,3);

               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'	=> 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A5:BC'
               );


               $objPHPExcel->getActiveSheet()->setCellValue('A5', 'NO SPB');
               $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('B5', 'TGL SPB');
               $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('C5', 'TGL APPR. KEUANGAN');
               $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('D5', 'KODE LANG');
               $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('E5', 'NAMA TOKO');
               $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('F5', 'JENIS TOKO');
               $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('G5', 'TOP');
               $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('H5', 'KODE SALES');
               $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('I5', 'SALES');
               $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('J5', 'KODE BARANG');
               $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('K5', 'NAMA BARANG');
               $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('L5', 'SERI');
               $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('M5', 'KATEGORI');
               $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('N5', 'SUB KATEGORI');
               $objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('O5', 'HARGA');
               $objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('P5', 'JML PESAN');
               $objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'JML KIRIM');
               $objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('R5', 'NILAI KOTOR SPB');
               $objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('S5', 'DISC SPB 1');
               $objPHPExcel->getActiveSheet()->getStyle('S5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('T5', 'DISC SPB 2');
               $objPHPExcel->getActiveSheet()->getStyle('T5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('U5', 'DISC SPB 3');
               $objPHPExcel->getActiveSheet()->getStyle('U5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               //--------------------------------------------------------------//
               $objPHPExcel->getActiveSheet()->setCellValue('V5', 'NILAI DISC SPB 1');
               $objPHPExcel->getActiveSheet()->getStyle('V5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('W5', 'NILAI DISC SPB 2');
               $objPHPExcel->getActiveSheet()->getStyle('W5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('X5', 'NILAI DISC SPB 3');
               $objPHPExcel->getActiveSheet()->getStyle('X5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('Y5', 'NILAI BERSIH SPB');
               $objPHPExcel->getActiveSheet()->getStyle('Y5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('Z5', 'NO NOTA');
               $objPHPExcel->getActiveSheet()->getStyle('Z5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               //-------------------------------------------------------------------//




               $objPHPExcel->getActiveSheet()->setCellValue('AA5', 'TGL NOTA');
               $objPHPExcel->getActiveSheet()->getStyle('AA5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AB5', 'NILAI NOTA');
               $objPHPExcel->getActiveSheet()->getStyle('AB5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AC5', 'DISC NOTA 1');
               $objPHPExcel->getActiveSheet()->getStyle('AC5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AD5', 'DISC NOTA 2');
               $objPHPExcel->getActiveSheet()->getStyle('AD5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AE5', 'DISC NOTA 3');
               $objPHPExcel->getActiveSheet()->getStyle('AE5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AF5', 'NILAI DISC NOTA 1');
               $objPHPExcel->getActiveSheet()->getStyle('AF5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AG5', 'NILAI DISC NOTA 2');
               $objPHPExcel->getActiveSheet()->getStyle('AG5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AH5', 'NILAI DISC NOTA 3');
               $objPHPExcel->getActiveSheet()->getStyle('AH5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AI5', 'NILAI BERSIH NOTA');
               $objPHPExcel->getActiveSheet()->getStyle('AI5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               /* $objPHPExcel->getActiveSheet()->setCellValue('AJ5', 'NILAI BERSIH NOTA');
               $objPHPExcel->getActiveSheet()->getStyle('AJ5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               ); */
               $objPHPExcel->getActiveSheet()->setCellValue('AJ5', 'KODE SUPPLIER');
               $objPHPExcel->getActiveSheet()->getStyle('AJ5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AK5', 'SUPPLIER');
               $objPHPExcel->getActiveSheet()->getStyle('AK5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AL5', 'KODE HARGA');
               $objPHPExcel->getActiveSheet()->getStyle('AL5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AM5', 'NO SJ');
               $objPHPExcel->getActiveSheet()->getStyle('AM5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AN5', 'TGL SJ');
               $objPHPExcel->getActiveSheet()->getStyle('AN5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AO5', 'PEMENUHAN STOK');
               $objPHPExcel->getActiveSheet()->getStyle('AO5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AP5', 'STATUS');
               $objPHPExcel->getActiveSheet()->getStyle('AP5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AQ5', 'KOTA');
               $objPHPExcel->getActiveSheet()->getStyle('AQ5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AR5', 'TGL TERIMA TOKO');
               $objPHPExcel->getActiveSheet()->getStyle('AR5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AS5', 'BRAND');
               $objPHPExcel->getActiveSheet()->getStyle('AS5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AT5', 'PROVINSI');
               $objPHPExcel->getActiveSheet()->getStyle('AT5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               /* ---------------------------------------------------------------- */
               $objPHPExcel->getActiveSheet()->setCellValue('AU5', 'SPB PARENT');
               $objPHPExcel->getActiveSheet()->getStyle('AU5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AV5', 'Alasan Tidak Buat SPB');
               $objPHPExcel->getActiveSheet()->getStyle('AV5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AW5', 'SPB PROMO');
               $objPHPExcel->getActiveSheet()->getStyle('AW5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AX5', 'NAMA PROMO');
               $objPHPExcel->getActiveSheet()->getStyle('AX5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AY5', 'TGL INPUT SPB');
               $objPHPExcel->getActiveSheet()->getStyle('AY5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('AZ5', 'POTONGAN NOMINAL SPB');
               $objPHPExcel->getActiveSheet()->getStyle('AZ5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('BA5', 'POTONGAN NOMINAL NOTA');
               $objPHPExcel->getActiveSheet()->getStyle('BA5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('BB5', 'STATUS FLAPOND');
               $objPHPExcel->getActiveSheet()->getStyle('BB5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('BC5', 'STATUS RATA TELAT');
               $objPHPExcel->getActiveSheet()->getStyle('BC5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );


               $i=6;
               $j=6;
               $xarea='';
               $no=1;
               $lang='';
               $status= '';
               foreach($query->result() as $row)
               {
                  $objPHPExcel->getActiveSheet()->duplicateStyleArray
                  (
                     array(
                        'font' => array

                        (
                           'name'   => 'Arial',
                           'bold'   => false,
                           'italic' => false,
                           'size'   => 10
                        )
                     ),
                     'A'.$i.':BC'.$i
                  );

                  /* if($row->f_customer_aktif=='t'){
                    $aktif="Ya";
                  }else{
                    $aktif="Tdk";
                  } */
/* 
                  //* PENAMBAHAN SHOP STATUS (12 JUN 2021)
                  $cek1 = $this->db->query(" SELECT
                                             a.i_customer,
                                             a.i_shop_status,
                                             CASE
                                                WHEN (a.i_shop_status = ''
                                                OR a.i_shop_status = NULL) THEN '-'
                                                ELSE b.e_shop_status
                                             END AS status
                                          FROM
                                             tr_customer_tmp a
                                          LEFT JOIN tr_shop_status b ON
                                             (a.i_shop_status = b.i_shop_status)
                                          WHERE
                                             a.i_customer = '$row->i_customer'
                                             AND a.i_area = '$row->i_area'
                                          ORDER BY a.i_customer ");
                  if($cek1->num_rows() > 0){
                     foreach($cek1->result() AS $xx){
                        $eshopstatus = $xx->status;
                     }
                  }else{
                     $cek2 = $this->db->query(" SELECT
                                                   a.i_customer,
                                                   a.i_shop_status,
                                                   CASE
                                                      WHEN (a.i_shop_status = ''
                                                      OR a.i_shop_status = NULL) THEN '-'
                                                      ELSE b.e_shop_status
                                                   END AS status
                                                FROM
                                                   tr_customer_tmpnonspb a
                                                LEFT JOIN tr_shop_status b ON
                                                   (a.i_shop_status = b.i_shop_status)
                                                WHERE
                                                   a.i_customer = '$row->i_customer'
                                                   AND a.i_area = '$row->i_area'
                                                ORDER BY a.i_customer ");
                     if($cek2->num_rows() > 0){
                        foreach($cek2->result() AS $zz){
                           $eshopstatus = $zz->status;
                        }
                     }
                  } */
                  /****************************************************************** */
              if(($row->f_spb_cancel == 't')){
                 $status='Batal';
              }elseif(($row->i_approve1 == null) && ($row->i_notapprove == null)){
                 $status='Sales';
              } elseif(($row->i_approve1 == null) && ($row->i_notapprove != null)){
                 $status='Reject (sls)';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 == null) && ($row->i_notapprove == null)){
                 $status='Keuangan';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 == null) && ($row->i_notapprove != null)){
                 $status='Reject (ar)';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_store == null)){
                 $status='Gudang';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 'f')){
                 $status='Pemenuhan SPB';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_op == 't') && ($row->f_spb_opclose == 'f')){
                 $status='Proses OP';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && ($row->f_spb_siapnotagudang == 'f') && ($row->f_spb_siapnotasales == 'f') && ($row->f_spb_opclose == 't')){
                 $status='OP Close';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 'f')){
                 $status='Siap SJ (sales)';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)){
                 $status='Siap SJ';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj == null)){
                 $status='Siap SJ';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb == null) && ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)){
                 $status='Siap DKB';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_dkb != null) && ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 'f') && ($row->f_spb_siapnotagudang == 't') && ($row->f_spb_siapnotasales == 't') && ($row->i_sj != null)){
                 $status='Siap Nota';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_store != null) && ($row->i_nota == null) && ($row->f_spb_stockdaerah == 't') && ($row->i_sj == null)){
                 $status='Siap SJ';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb == null) && ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)){
                 $status='Siap DKB';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_store != null) && ($row->i_nota == null) && ($row->i_dkb != null) && ($row->f_spb_stockdaerah == 't') && ($row->i_sj != null)){
                 $status='Siap Nota';
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_store != null) && ($row->i_nota != null) && ($row->d_sj_receive != null)){
                 $status='Sudah diterima';			  
              }elseif(($row->i_approve1 != null) && ($row->i_approve2 != null) && ($row->i_store != null) && ($row->i_nota != null) && ($row->d_sj_receive != null)){
                  $status='Sudah dinotakan';			  
              }elseif(($row->i_nota != null)){
                 $status='Sudah dinotakan'; 
              }else{
                 $status='Unknown';		
              }
            /****************************************************************** */

                     $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row->i_spb);
                     $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->d_spb);
                     $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_approve2);
                     $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, "'".$row->i_customer);
                     $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->e_customer_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->e_customer_classname);
                     $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->n_customer_toplength);
                     $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, "'".$row->i_salesman);
                     $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->e_salesman_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->i_product);
                     $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->e_product_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row->e_product_seriname);
                     $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $row->kategory);
                     $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $row->subkategori);
                     $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $row->v_unit_price);
                     $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $row->n_order);
                     $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $row->n_deliver);
                     $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $row->spbkotor);
                     $objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $row->n_spb_discount1);
                     $objPHPExcel->getActiveSheet()->setCellValue('T'.$i, $row->n_spb_discount2);
                     $objPHPExcel->getActiveSheet()->setCellValue('U'.$i, $row->n_spb_discount3);
                     $objPHPExcel->getActiveSheet()->setCellValue('V'.$i, $row->v_spb_disc1);
                     $objPHPExcel->getActiveSheet()->setCellValue('W'.$i, $row->v_spb_disc2);
                     $objPHPExcel->getActiveSheet()->setCellValue('X'.$i, $row->v_spb_disc3);
                     $objPHPExcel->getActiveSheet()->setCellValue('Y'.$i, $row->spbbersih);
                     $objPHPExcel->getActiveSheet()->setCellValue('Z'.$i, $row->i_nota);
                     /* -------------------------------------------------------------------------------- */
                     $objPHPExcel->getActiveSheet()->setCellValue('AA'.$i, $row->d_nota);
                     $objPHPExcel->getActiveSheet()->setCellValue('AB'.$i, $row->notakotor);
                     $objPHPExcel->getActiveSheet()->setCellValue('AC'.$i, $row->n_nota_discount1);
                     $objPHPExcel->getActiveSheet()->setCellValue('AD'.$i, $row->n_nota_discount2);
                     $objPHPExcel->getActiveSheet()->setCellValue('AE'.$i, $row->n_nota_discount3);
                     $objPHPExcel->getActiveSheet()->setCellValue('AF'.$i, $row->v_nota_disc1);
                     $objPHPExcel->getActiveSheet()->setCellValue('AG'.$i, $row->v_nota_disc2);
                     $objPHPExcel->getActiveSheet()->setCellValue('AH'.$i, $row->v_nota_disc3);
                     $objPHPExcel->getActiveSheet()->setCellValue('AI'.$i, $row->notabersih);
                     $objPHPExcel->getActiveSheet()->setCellValue('AJ'.$i, $row->i_supplier);
                     $objPHPExcel->getActiveSheet()->setCellValue('AK'.$i, $row->e_supplier_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('AL'.$i, $row->i_price_group);
                     $objPHPExcel->getActiveSheet()->setCellValue('AM'.$i, $row->i_sj);
                     $objPHPExcel->getActiveSheet()->setCellValue('AN'.$i, $row->d_sj);
                     $objPHPExcel->getActiveSheet()->setCellValue('AO'.$i, $row->e_store_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('AP'.$i, $status);
                     $objPHPExcel->getActiveSheet()->setCellValue('AQ'.$i, $row->i_city);
                     $objPHPExcel->getActiveSheet()->setCellValue('AR'.$i, $row->d_sj_receive);
                     $objPHPExcel->getActiveSheet()->setCellValue('AS'.$i, $row->e_product_typename);
                     $objPHPExcel->getActiveSheet()->setCellValue('AT'.$i, $row->e_provinsi);
                     $objPHPExcel->getActiveSheet()->setCellValue('AU'.$i, $row->i_spb_refference);
                     $objPHPExcel->getActiveSheet()->setCellValue('AV'.$i, $row->e_alasan_lanjut);
                     $objPHPExcel->getActiveSheet()->setCellValue('AW'.$i, $row->i_spb_program);
                     $objPHPExcel->getActiveSheet()->setCellValue('AX'.$i, $row->e_promo_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('AY'.$i, $row->d_spb_entry);
                     $objPHPExcel->getActiveSheet()->setCellValue('AZ'.$i, $row->nol);
                     $objPHPExcel->getActiveSheet()->setCellValue('BA'.$i, $row->nol);
                     $objPHPExcel->getActiveSheet()->setCellValue('BB'.$i, $row->statusss);
                     $objPHPExcel->getActiveSheet()->setCellValue('BC'.$i, $row->sttstelat);


                     $no++;
                     $i++;
                     $j++;
               }
               $x=$i-1;
            }
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $nama='Exp SPB VS NOTA'.$iarea.'.xls';
            if($iarea!='NA'){
              $store=$iarea;
            }else{
              $store='00';
            }
            if(file_exists('excel/'.$iarea.'/'.$nama))
            {
               @chmod('excel/'.$iarea.'/'.$nama, 0777);
               @unlink('excel/'.$iarea.'/'.$nama);
            }
            $objWriter->save("excel/".$iarea.'/'.$nama); 

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
	          while($row=pg_fetch_assoc($query)){
	          	$now	  = $row['c'];
			      }
			      $pesan='Export SPB VS NOTA:'.$iarea;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan ); 

            $data['sukses']   = true;
            $data['inomor']   = "Ekspor SPB VS NOTA : ".$iarea."-".$eareaname." berhasil ke file ".$nama;
            $data['folder']   = "exp-notdet";
            $this->load->view('nomorexport',$data);
         }
         else
         {
            $this->load->view('awal/index.php');
      }
   }
}
?>
