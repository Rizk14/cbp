<?php
class Cform extends CI_Controller
{
   public $title  = "Export SPB VS Nota";
   public $folder = "exp-notdet";

   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationx');
      $this->load->model($this->folder . '/mmaster');
      require_once("php/fungsi.php");
   }

   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu405') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data['page_title']  = $this->title;
         $data['folder']      = $this->folder;
         $data['iarea']       = '';

         $this->load->view($this->folder . '/vform', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu405') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $data['page_title'] = $this->title;
         $this->load->view($this->folder . '/vinsert_fail', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu405') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/exp-notdet/cform/area/index/';
         $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

         $config['total_rows'] = $query->num_rows();
         $config['per_page']   = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $data['page_title'] = $this->lang->line('list_area');
         $data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);
         $this->load->view($this->folder . '/vlistarea', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu405') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         $config['base_url'] = base_url() . 'index.php/exp-notdet/cform/area/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
                                    and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $data['page_title'] = $this->lang->line('list_area');
         $data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);
         $this->load->view($this->folder . '/vlistarea', $data);
      } else {
         $this->load->view('awal/index.php');
      }
   }

   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('menu405') == 't')) ||
         (($this->session->userdata('logged_in')) &&
            ($this->session->userdata('allmenu') == 't'))
      ) {
         ini_set('memory_limit', '1024M');

         $iarea   = $this->uri->segment('4');
         $datefrom   = $this->uri->segment('5');
         $toto     = $this->uri->segment('6');

         // if ($dfrom != '') {
         //    $tmp = explode("-", $dfrom);
         //    $th = $tmp[2];
         //    $bl = $tmp[1];
         //    $hr = $tmp[0];
         //    $datefrom = $th . "-" . $bl . "-" . $hr;
         // }

         // if ($dto != '') {
         //    $tmp = explode("-", $dto);
         //    $thto = $tmp[2];
         //    $blto = $tmp[1];
         //    $hrto = $tmp[0];
         //    $toto = $thto . "-" . $blto . "-" . $hrto;
         // }

         $istore     = $this->input->post('istore');
         $eareaname  = $this->input->post('eareaname');

         if ($iarea == 'NA') $eareaname = 'Nasional';

         $query = $this->mmaster->bacasemua($iarea, $datefrom, $toto);

         $this->load->library('PHPExcel');
         $this->load->library('PHPExcel/IOFactory');

         $objPHPExcel = new PHPExcel();
         $objPHPExcel->getProperties()->setTitle("Customer List")->setDescription(NmPerusahaan);
         $objPHPExcel->setActiveSheetIndex(0);
         if ($query->num_rows() > 0) {

            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
               array(
                  'font' => array(
                     'name'   => 'Arial',
                     'bold'  => true,
                     'italic' => false,
                     'size'  => 10
                  ),
                  'alignment' => array(
                     'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                     'vertical'  => Style_Alignment::VERTICAL_CENTER,
                     'wrap'      => true
                  )
               ),
               'A2:A4'
            );

            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(17);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(17);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setWidth(17);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AW')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AX')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AY')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AZ')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('BA')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('BB')->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimension('BC')->setWidth(12);

            $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Export SPB VS NOTA ');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 53, 2);
            if ($iarea != 'NA') {
               $objPHPExcel->getActiveSheet()->setCellValue('A3', "Periode : " . $datefrom . " - " . $toto);
            } else {
               $objPHPExcel->getActiveSheet()->setCellValue('A3', "Area : " . $eareaname);
            }
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 53, 3);

            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
               array(
                  'font' => array(
                     'name'   => 'Arial',
                     'bold'  => true,
                     'italic' => false,
                     'size'  => 10
                  ),
                  'alignment' => array(
                     'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
                     'vertical'  => Style_Alignment::VERTICAL_CENTER,
                     'wrap'      => true
                  )
               ),
               'A5:BC5'
            );

            $style_col = array(
               'borders' => array(
                  'top'    => array('style' => Style_Border::BORDER_THIN)
               )
            );

            /* var_dump(rand());
               die(); */

            $objPHPExcel->getActiveSheet()->setCellValue('A5', 'NO SPB');
            // $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('B5', 'TGL SPB');
            // $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('C5', 'TGL APPR. KEUANGAN');
            // $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('D5', 'KODE LANG');
            // $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('E5', 'NAMA TOKO');
            // $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('F5', 'JENIS TOKO');
            // $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('G5', 'TOP');
            // $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('H5', 'KODE SALES');
            // $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('I5', 'SALES');
            // $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('J5', 'KODE BARANG');
            // $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('K5', 'NAMA BARANG');
            // $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('L5', 'SERI');
            // $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('M5', 'KATEGORI');
            // $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('N5', 'SUB KATEGORI');
            // $objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('O5', 'HARGA');
            // $objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('P5', 'JML PESAN');
            // $objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'JML KIRIM');
            // $objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('R5', 'NILAI KOTOR SPB');
            // $objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('S5', 'DISC SPB 1');
            // $objPHPExcel->getActiveSheet()->getStyle('S5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('T5', 'DISC SPB 2');
            // $objPHPExcel->getActiveSheet()->getStyle('T5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('U5', 'DISC SPB 3');
            // $objPHPExcel->getActiveSheet()->getStyle('U5')->applyFromArray($style_col);
            //--------------------------------------------------------------//
            $objPHPExcel->getActiveSheet()->setCellValue('V5', 'NILAI DISC SPB 1');
            // $objPHPExcel->getActiveSheet()->getStyle('V5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('W5', 'NILAI DISC SPB 2');
            // $objPHPExcel->getActiveSheet()->getStyle('W5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('X5', 'NILAI DISC SPB 3');
            // $objPHPExcel->getActiveSheet()->getStyle('X5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('Y5', 'NILAI BERSIH SPB');
            // $objPHPExcel->getActiveSheet()->getStyle('Y5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('Z5', 'NO NOTA');
            // $objPHPExcel->getActiveSheet()->getStyle('Z5')->applyFromArray($style_col);
            //-------------------------------------------------------------------//
            $objPHPExcel->getActiveSheet()->setCellValue('AA5', 'TGL NOTA');
            // $objPHPExcel->getActiveSheet()->getStyle('AA5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AB5', 'NILAI NOTA');
            // $objPHPExcel->getActiveSheet()->getStyle('AB5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AC5', 'DISC NOTA 1');
            // $objPHPExcel->getActiveSheet()->getStyle('AC5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AD5', 'DISC NOTA 2');
            // $objPHPExcel->getActiveSheet()->getStyle('AD5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AE5', 'DISC NOTA 3');
            // $objPHPExcel->getActiveSheet()->getStyle('AE5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AF5', 'NILAI DISC NOTA 1');
            // $objPHPExcel->getActiveSheet()->getStyle('AF5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AG5', 'NILAI DISC NOTA 2');
            // $objPHPExcel->getActiveSheet()->getStyle('AG5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AH5', 'NILAI DISC NOTA 3');
            // $objPHPExcel->getActiveSheet()->getStyle('AH5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AI5', 'NILAI BERSIH NOTA');
            // $objPHPExcel->getActiveSheet()->getStyle('AI5')->applyFromArray($style_col);
            /* $objPHPExcel->getActiveSheet()->setCellValue('AJ5', 'NILAI BERSIH NOTA');
               // $objPHPExcel->getActiveSheet()->getStyle('AJ5')->applyFromArray($style_col); */
            $objPHPExcel->getActiveSheet()->setCellValue('AJ5', 'KODE SUPPLIER');
            // $objPHPExcel->getActiveSheet()->getStyle('AJ5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AK5', 'SUPPLIER');
            // $objPHPExcel->getActiveSheet()->getStyle('AK5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AL5', 'KODE HARGA');
            // $objPHPExcel->getActiveSheet()->getStyle('AL5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AM5', 'NO SJ');
            // $objPHPExcel->getActiveSheet()->getStyle('AM5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AN5', 'TGL SJ');
            // $objPHPExcel->getActiveSheet()->getStyle('AN5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AO5', 'PEMENUHAN STOK');
            // $objPHPExcel->getActiveSheet()->getStyle('AO5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AP5', 'STATUS');
            // $objPHPExcel->getActiveSheet()->getStyle('AP5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AQ5', 'KOTA');
            // $objPHPExcel->getActiveSheet()->getStyle('AQ5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AR5', 'TGL TERIMA TOKO');
            // $objPHPExcel->getActiveSheet()->getStyle('AR5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AS5', 'BRAND');
            // $objPHPExcel->getActiveSheet()->getStyle('AS5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AT5', 'PROVINSI');
            // $objPHPExcel->getActiveSheet()->getStyle('AT5')->applyFromArray($style_col);
            /* ---------------------------------------------------------------- */
            $objPHPExcel->getActiveSheet()->setCellValue('AU5', 'SPB PARENT');
            // $objPHPExcel->getActiveSheet()->getStyle('AU5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AV5', 'Alasan Tidak Buat SPB');
            // $objPHPExcel->getActiveSheet()->getStyle('AV5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AW5', 'SPB PROMO');
            // $objPHPExcel->getActiveSheet()->getStyle('AW5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AX5', 'NAMA PROMO');
            // $objPHPExcel->getActiveSheet()->getStyle('AX5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AY5', 'TGL INPUT SPB');
            // $objPHPExcel->getActiveSheet()->getStyle('AY5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('AZ5', 'POTONGAN NOMINAL SPB');
            // $objPHPExcel->getActiveSheet()->getStyle('AZ5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('BA5', 'POTONGAN NOMINAL NOTA');
            // $objPHPExcel->getActiveSheet()->getStyle('BA5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('BB5', 'STATUS FLAPOND');
            // $objPHPExcel->getActiveSheet()->getStyle('BB5')->applyFromArray($style_col);
            $objPHPExcel->getActiveSheet()->setCellValue('BC5', 'STATUS RATA TELAT');
            // $objPHPExcel->getActiveSheet()->getStyle('BC5')->applyFromArray($style_col);

            $i       = 6;
            $j       = 6;
            $xarea   = '';
            $no      = 1;
            $lang    = '';
            $status  = '';

            foreach ($query->result() as $row) {
               $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $row->i_spb);
               $objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->d_spb);
               $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->d_approve2);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_customer);
               $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $row->e_customer_name);
               $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $row->e_customer_classname);
               $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row->n_customer_toplength);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->i_salesman);
               $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->e_salesman_name);
               $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->i_product);
               $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $row->e_product_name);
               $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row->e_product_seriname);
               $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $row->kategory);
               $objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $row->subkategori);
               $objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $row->v_unit_price);
               $objPHPExcel->getActiveSheet()->setCellValue('P' . $i, $row->n_order);
               $objPHPExcel->getActiveSheet()->setCellValue('Q' . $i, $row->n_deliver);
               $objPHPExcel->getActiveSheet()->setCellValue('R' . $i, $row->spbkotor);
               $objPHPExcel->getActiveSheet()->setCellValue('S' . $i, $row->n_spb_discount1);
               $objPHPExcel->getActiveSheet()->setCellValue('T' . $i, $row->n_spb_discount2);
               $objPHPExcel->getActiveSheet()->setCellValue('U' . $i, $row->n_spb_discount3);
               $objPHPExcel->getActiveSheet()->setCellValue('V' . $i, $row->v_spb_disc1);
               $objPHPExcel->getActiveSheet()->setCellValue('W' . $i, $row->v_spb_disc2);
               $objPHPExcel->getActiveSheet()->setCellValue('X' . $i, $row->v_spb_disc3);
               $objPHPExcel->getActiveSheet()->setCellValue('Y' . $i, $row->spbbersih);
               $objPHPExcel->getActiveSheet()->setCellValue('Z' . $i, $row->i_nota);
               /* -------------------------------------------------------------------------------- */
               $objPHPExcel->getActiveSheet()->setCellValue('AA' . $i, $row->d_nota);
               $objPHPExcel->getActiveSheet()->setCellValue('AB' . $i, $row->notakotor);
               $objPHPExcel->getActiveSheet()->setCellValue('AC' . $i, $row->n_nota_discount1);
               $objPHPExcel->getActiveSheet()->setCellValue('AD' . $i, $row->n_nota_discount2);
               $objPHPExcel->getActiveSheet()->setCellValue('AE' . $i, $row->n_nota_discount3);
               $objPHPExcel->getActiveSheet()->setCellValue('AF' . $i, $row->v_nota_disc1);
               $objPHPExcel->getActiveSheet()->setCellValue('AG' . $i, $row->v_nota_disc2);
               $objPHPExcel->getActiveSheet()->setCellValue('AH' . $i, $row->v_nota_disc3);
               $objPHPExcel->getActiveSheet()->setCellValue('AI' . $i, $row->notabersih);
               $objPHPExcel->getActiveSheet()->setCellValue('AJ' . $i, $row->i_supplier);
               $objPHPExcel->getActiveSheet()->setCellValue('AK' . $i, $row->e_supplier_name);
               $objPHPExcel->getActiveSheet()->setCellValueExplicit('AL' . $i, $row->i_price_group);
               $objPHPExcel->getActiveSheet()->setCellValue('AM' . $i, $row->i_sj);
               $objPHPExcel->getActiveSheet()->setCellValue('AN' . $i, $row->d_sj);
               $objPHPExcel->getActiveSheet()->setCellValue('AO' . $i, $row->pemenuhan);
               $objPHPExcel->getActiveSheet()->setCellValue('AP' . $i, $row->status);
               $objPHPExcel->getActiveSheet()->setCellValue('AQ' . $i, $row->i_city);
               $objPHPExcel->getActiveSheet()->setCellValue('AR' . $i, $row->d_sj_receive);
               $objPHPExcel->getActiveSheet()->setCellValue('AS' . $i, $row->e_product_typename);
               $objPHPExcel->getActiveSheet()->setCellValue('AT' . $i, $row->e_provinsi);
               $objPHPExcel->getActiveSheet()->setCellValue('AU' . $i, $row->i_spb_refference);
               $objPHPExcel->getActiveSheet()->setCellValue('AV' . $i, $row->e_alasan_lanjut);
               $objPHPExcel->getActiveSheet()->setCellValue('AW' . $i, $row->i_spb_program);
               $objPHPExcel->getActiveSheet()->setCellValue('AX' . $i, $row->e_promo_name);
               $objPHPExcel->getActiveSheet()->setCellValue('AY' . $i, $row->d_spb_entry);
               $objPHPExcel->getActiveSheet()->setCellValue('AZ' . $i, $row->nol);
               $objPHPExcel->getActiveSheet()->setCellValue('BA' . $i, $row->nol);
               $objPHPExcel->getActiveSheet()->setCellValue('BB' . $i, $row->statusss);
               $objPHPExcel->getActiveSheet()->setCellValue('BC' . $i, $row->sttstelat);

               $no++;
               $i++;
               $j++;
            }

            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
               array(
                  'font' => array(
                     'name'   => 'Arial',
                     'bold'   => false,
                     'italic' => false,
                     'size'   => 10
                  )
               ),
               'A6:BC' . $i
            );

            $objPHPExcel->getActiveSheet()->getStyle('A5:BC5')->applyFromArray($style_col);

            $x = $i - 1;
         }

         $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

         $nama = 'Export SPB VS NOTA ' . $iarea . '-' . $eareaname . '.xls';

         // if ($iarea != 'NA') {
         //    $store = $iarea;
         // } else {
         //    $store = '00';
         // }

         // if (file_exists('excel/' . $iarea . '/' . $nama)) {
         //    @chmod('excel/' . $iarea . '/' . $nama, 0777);
         //    @unlink('excel/' . $iarea . '/' . $nama);
         // }
         // $objWriter->save("excel/" . $store . '/' . $nama);

         $this->logger->writenew('Export SPB VS NOTA:' . $iarea);

         // Proses file excel    
         header('Content-Type: application/vnd.ms-excel');
         header('Content-Disposition: attachment; filename=' . $nama . ''); // Set nama file excel nya    
         header('Cache-Control: max-age=0');

         $objWriter  = IOFactory::createWriter($objPHPExcel, 'Excel5');
         $objWriter->save('php://output', 'w');
      } else {
         $this->load->view('awal/index.php');
      }
   }
}
