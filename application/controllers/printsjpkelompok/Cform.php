<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu175')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printsjp');
			$data['sjfrom']='';
			$data['sjto']	='';
			$this->load->view('printsjpkelompok/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function sjfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu175')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $iuser   = $this->session->userdata('user_id');
			$cari	= strtoupper($this->input->post('cari'));
			$config['base_url'] = base_url().'index.php/printsjpkelompok/cform/sjfrom/index/';
			$query = $this->db->query("	select i_sjp from tm_sjp where upper(i_sjp) like '%$cari%'
                                  and (i_area in ( select i_area from tm_user_area where i_user='$iuser'))",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printsjpkelompok/mmaster');
			$data['page_title'] = $this->lang->line('listsjp');
			$data['isi']=$this->mmaster->bacasj($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printsjpkelompok/vlistsjfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisjfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu175')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari	= strtoupper($this->input->post('cari'));
      $iuser   = $this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/printsjpkelompok/cform/sjfrom/index/';
			$query = $this->db->query("	select i_sjp from tm_sjp where upper(i_sjp) like '%$cari%'
                                  and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printsjpkelompok/mmaster');
			$data['page_title'] = $this->lang->line('listsjp');
			$data['isi']=$this->mmaster->carisj($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printsjpkelompok/vlistsjfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function sjto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu175')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari	= strtoupper($this->input->post('cari'));
			$area	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/printsjpkelompok/cform/sjto/'.$area.'/';
			$query = $this->db->query("	select i_sjp from tm_sjp
              										where upper(i_sjp) like '%$cari%' and i_area ='$area'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printsjpkelompok/mmaster');
			$data['page_title'] = $this->lang->line('listsjp');
      $data['area']=$area;
			$data['isi']=$this->mmaster->bacasj2($config['per_page'],$this->uri->segment(5),$area);
			$this->load->view('printsjpkelompok/vlistsjto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisjto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu175')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari	= strtoupper($this->input->post('cari'));
			$area	= strtoupper($this->input->post('iarea'));
			if($area=='') $area	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/printsjpkelompok/cform/sjto/'.$area.'/';
			$query = $this->db->query("	select i_sjp from tm_sjp
              										where upper(i_sjp) like '%$cari%' and i_area ='$area'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printsjpkelompok/mmaster');
      $data['area']=$area;
			$data['page_title'] = $this->lang->line('listsjp');
			$data['isi']=$this->mmaster->carisj2($cari,$config['per_page'],$this->uri->segment(5),$area);
			$this->load->view('printsjpkelompok/vlistsjto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu175')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	  = $this->input->post('areafrom');
			$sjfrom = $this->input->post('sjfrom');
			$sjto	  = $this->input->post('sjto');
			$this->load->model('printsjpkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['master']=$this->mmaster->bacamaster($sjfrom,$sjto,$area);
			$data['area'] = $area;
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
      $data['sjfrom'] = $sjfrom;
      $data['sjto']   = $sjto;
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak SJP Area:'.$area.' No:'.$sjfrom.' s/d No:'.$sjto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printsjpkelompok/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
