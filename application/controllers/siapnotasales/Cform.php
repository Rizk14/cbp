<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu103')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/siapnotasales/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name 
                                  from tm_spb a, tr_customer b, tr_area c
                                  where a.i_customer=b.i_customer 
                                  and a.i_area=c.i_area
                                  and a.f_spb_cancel='f' 
                                  and a.f_spb_valid='f' 
                                  and a.f_spb_siapnotagudang='t'
                                  and a.f_spb_siapnotasales='f'
                                  and a.i_store='AA'
                                  and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
                                  or upper(a.i_spb) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['ada']	= 'xx';
      $data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('siapsj');
			$this->load->model('siapnotasales/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('siapnotasales/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function closing()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu103')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml = $this->input->post('jml', TRUE);
			$this->db->trans_begin();
			$this->load->model('siapnotasales/mmaster');
			$ispbxx='';
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$ispb = $this->input->post('ispb'.$i, TRUE);
					$ispbxx=$ispbxx.' '.$ispb;
					$iarea= $this->input->post('iarea'.$i, TRUE);
					$this->mmaster->updatespb($ispb, $iarea);
				}
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Validasi SPB Area '.$iarea.' No:'.$ispbxx;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );
			}
			$config['base_url'] = base_url().'index.php/siapnotasales/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name 
										from tm_spb a, tr_customer b, tr_area c
										where a.i_customer=b.i_customer 
										and a.i_area=c.i_area
										and a.f_spb_cancel='f' 
										and a.f_spb_valid='f' 
										and a.f_spb_siapnotagudang='t'
										and a.f_spb_siapnotasales='f'
										and a.i_store='AA'
										and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
											 or upper(a.i_spb) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['ada']	= 'xx';
      $data['cari']=$cari;
			$data['page_title'] = $this->lang->line('siapsj');
			$this->load->model('siapnotasales/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('siapnotasales/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu103')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('siapsj');
			$this->load->view('siapnotasales/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu103')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/siapnotasales/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name 
										from tm_spb a, tr_customer b, tr_area c
										where a.i_customer=b.i_customer 
										and a.i_area=c.i_area
										and a.f_spb_cancel='f' 
										and a.f_spb_valid='t' 
										and a.f_spb_siapnotagudang='t'
										and a.f_spb_siapnotasales='f'
										and a.i_store='AA'
										and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' 
											 or upper(a.i_spb) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('siapnotasales/mmaster');
			$data['ada']	= 'xx';
      $data['cari']=$cari;
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('siapsj');
			$data['iop']='';
	 		$this->load->view('siapnotasales/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu103')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
			  )
			{
				$ispb 	= $this->uri->segment(4);
				$iarea	= $this->uri->segment(5);
        $prog   = $this->uri->segment(6);
				$group  = $this->uri->segment(7);
        if( ($prog==null)||($prog=='') ){
          if($group=='00'){
            $data['page_title'] = $this->lang->line('spb')." Reguler";
          }else{
            $data['page_title'] = $this->lang->line('spb')." Baby";
          }
        }else{
          if($group=='00'){
            $data['page_title'] = $this->lang->line('spb')." Promo Reguler";
          }else{
            $data['page_title'] = $this->lang->line('spb')." Promo Baby";
          }
        }
				$query 	= $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem']	= $query->num_rows(); 				
				$data['ispb'] 		= $ispb;
				$data['departement']= $this->session->userdata('departement');
				$this->load->model('siapnotasales/mmaster');
				$data['ada']	= '';
        $data['cari'] = '';
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
        $tes=$this->db->query("select i_price_group from tm_spb where i_spb = '$ispb' and i_area='$iarea'");
        foreach($tes->result() as $hr){
          $ipricegroup=$hr->i_price_group;
        }
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea,$ipricegroup);
		 		$this->load->view('siapnotasales/vmainform',$data);
			}else{
				$this->load->view('siapnotasales/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu103')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb 	= $this->input->post('ispb', TRUE);
			$dspb 	= $this->input->post('dspb', TRUE);
			if($dspb!=''){
				$tmp=explode("-",$dspb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspb=$th."-".$bl."-".$hr;
			}
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomername	= $this->input->post('ecustomername', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$eareaname		= $this->input->post('eareaname', TRUE);
			$ispbpo			= $this->input->post('ispbpo', TRUE);
			$nspbtoplength	= $this->input->post('nspbtoplength', TRUE);
			$isalesman		= $this->input->post('isalesman',TRUE);
			$esalesmanname	= $this->input->post('esalesmanname',TRUE);
			$ipricegroup	= $this->input->post('ipricegroup',TRUE);
			$inota			= $this->input->post('inota',TRUE);
			$dspbreceive	= $this->input->post('dspbreceive',TRUE);
			if($ispbpo!='')
				$fspbop			= 't';
			else
				$fspbop			= 'f';
			$ecustomerpkpnpwp	= $this->input->post('ecustomerpkpnpwp',TRUE);
			if($ecustomerpkpnpwp!='')
				$fspbpkp		= 't';
			else
				$fspbpkp		= 'f';
			$fspbconsigment		= $this->input->post('fspbconsigment',TRUE);
			if($fspbconsigment!='')
				$fspbconsigment="t";
			else
				$fspbconsigment="f";
			$fspbplusppn		= $this->input->post('fspbplusppn',TRUE);
			$fspbplusdiscount	= $this->input->post('fspbplusdiscount',TRUE);
			$fspbstockdaerah	= $this->input->post('fspbstockdaerah',TRUE);
			if($fspbstockdaerah!='')
				$fspbstockdaerah= 't';
			else
				$fspbstockdaerah= 'f';
			$fspbprogram	= 'f';
			$fspbvalid		= 't';
			$fspbsiapnotagudang	= 't';
			$fspbcancel		= 'f';
			$nspbtoplength	= $this->input->post('nspbtoplength',TRUE);
			$nspbdiscount1	= $this->input->post('ncustomerdiscount1',TRUE);
			$nspbdiscount2	= $this->input->post('ncustomerdiscount2',TRUE);
			$nspbdiscount3	= $this->input->post('ncustomerdiscount3',TRUE);
			$vspbdiscount1	= $this->input->post('vcustomerdiscount1',TRUE);
			$vspbdiscount2	= $this->input->post('vcustomerdiscount2',TRUE);
			$vspbdiscount3	= $this->input->post('vcustomerdiscount3',TRUE);
			$vspbdiscounttotal	= $this->input->post('vspbdiscounttotal',TRUE);
			$vspb			      = $this->input->post('vspb',TRUE);
			$nspbdiscount1	= str_replace(',','',$nspbdiscount1);
			$nspbdiscount2	= str_replace(',','',$nspbdiscount2);
			$nspbdiscount3	= str_replace(',','',$nspbdiscount3);
			$vspbdiscount1	= str_replace(',','',$vspbdiscount1);
			$vspbdiscount2	= str_replace(',','',$vspbdiscount2);
			$vspbdiscount3	= str_replace(',','',$vspbdiscount3);
			$vspbdiscounttotal	= str_replace(',','',$vspbdiscounttotal);
			$vspb			= str_replace(',','',$vspb);
			$vspbdiscounttotalafter	= $this->input->post('vspbdiscounttotalafter',TRUE);
			$vspbafter			= $this->input->post('vspbafter',TRUE);
			$vspbdiscounttotalafter	= str_replace(',','',$vspbdiscounttotalafter);
			$vspbafter			= str_replace(',','',$vspbafter);

			$jml			= $this->input->post('jml', TRUE);
			if(($ecustomername!='') && ($ispb!=''))
			{
				$benar="false";
				$this->db->trans_begin();
				$this->load->model('siapnotasales/mmaster');
				$this->mmaster->updateheader($ispb, $iarea, $dspb, $icustomer, $ispbpo, $nspbtoplength, $isalesman, 
						 $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp, 
						 $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
						 $fspbsiapnotagudang, $fspbcancel, $nspbdiscount1, 
						 $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
						 $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment,$vspbdiscounttotalafter,$vspbafter);
				for($i=1;$i<=$jml;$i++){
				  $iproduct					=$this->input->post('iproduct'.$i, TRUE);
          $iproductstatus	= $this->input->post('iproductstatus'.$i, TRUE);
				  $iproductgrade			='A';
				  $iproductmotif			=$this->input->post('motif'.$i, TRUE);
				  $eproductname				=$this->input->post('eproductname'.$i, TRUE);
				  $vunitprice				=$this->input->post('vproductretail'.$i, TRUE);
				  $vunitprice				=str_replace(',','',$vunitprice);
				  $norder					=$this->input->post('norder'.$i, TRUE);
				  $ndeliver					=$this->input->post('ndeliver'.$i, TRUE);
				  $eremark					= $this->input->post('eremark'.$i, TRUE);
				  $iproductstatus		=$this->input->post('iproductstatus'.$i, TRUE);
				  $data['iproduct']			=$iproduct;
				  $data['iproductgrade']	=$iproductgrade;
				  $data['iproductmotif']	=$iproductmotif;
				  $data['eproductname']		=$eproductname;
				  $data['vunitprice']		=$vunitprice;
				  $data['norder']			=$norder;
#				  $this->mmaster->deletedetail($ispb, $iarea, $iproduct, $iproductgrade, $iproductmotif);
#				  if($norder>0){
				    $this->mmaster->updatedetail( 	$ispb,$iarea,$iproduct,$iproductstatus,$iproductgrade,$eproductname,$norder,$ndeliver,
													$vunitprice,$iproductmotif,$eremark,$i);
#				  }
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update SPB Siap Nota Sales No:'.$ispb.' Area:'.$iarea;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ispb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function pricegroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu103')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $ispb=$this->uri->segment(4);
      $area=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/siapnotasales/cform/pricegroup/'.$ispb.'/'.$area.'/';
      $data['ispb']=$this->uri->segment(4);
      $data['area']=$this->uri->segment(5);
      $cari=strtoupper($this->input->post("cari"));
			$query = $this->db->query(" select * from tr_price_group
                                  where upper(i_price_group) like '%$cari%' or upper(e_price_groupname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('siapnotasales/mmaster');
			$data['page_title'] = $this->lang->line('list_pricegroup');
			$data['isi']=$this->mmaster->bacapricegroup($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('siapnotasales/vlistpricegroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
