<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu29')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$cari = ($this->input->post("cari"))? $this->input->post("cari") : "";
       		$cari = ($this->uri->segment(4)) ? $this->uri->segment(4) : $cari;
        	$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/customerarea/cform/index/'.$cari;
			//$cari = strtoupper($this->input->post('cari', FALSE));
	        if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				  $query = $this->db->query("	select * from tr_customer_area 
								                      inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
								                      inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
								                      where upper(tr_customer_area.i_customer) like '%$cari%' or
								                      upper(tr_customer_area.e_area_name) ilike '%$cari%' or
	  											      upper(tr_customer.e_customer_name) ilike '%$cari%' ",false);
	        }else{
				    $query = $this->db->query("	select * from tr_customer_area 
								                      inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
								                      inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
								                      where upper(tr_customer_area.i_customer) like '%$cari%' or
								                      upper(tr_customer_area.e_area_name) ilike '%$cari%' or
	  											      upper(tr_customer.e_customer_name) ilike '%$cari%'
	                                    and (tr_area.i_area = '$area1' or tr_area.i_area = '$area2' or tr_area.i_area = '$area3'
	                                    or tr_area.i_area = '$area4' or tr_area.i_area = '$area5') ",false);
	        }
			$data['cari'] 		= $cari;
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_customerarea');
			$data['iarea']='';
			$data['icustomer']='';
			$this->load->model('customerarea/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
				$ip_address	  = $row['ip_address'];
				break;
			}
			}else{
			$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Area Pelanggan";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('customerarea/vmainform', $data);
		}else{
			  $this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu29')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 		= $this->input->post('iarea', TRUE);
			$eareaname 	= $this->input->post('eareaname', TRUE);
			$icustomer 	= $this->input->post('icustomer', TRUE);
			$ecustomername 	= $this->input->post('ecustomername', TRUE);

			if ((isset($iarea) && $iarea != '') 
			    && (isset($eareaname) && $eareaname != '') 
			    && (isset($icustomer) && $icustomer != '') 
			    && (isset($ecustomername) && $ecustomername != ''))
			{
				$this->load->model('customerarea/mmaster');
				$this->mmaster->insert($iarea,$icustomer,$eareaname);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Area Pelanggan:('.$icustomer.') -'.$iarea;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		        $cari = ($this->input->post("cari"))? $this->input->post("cari") : "";
	       		$cari = ($this->uri->segment(4)) ? $this->uri->segment(4) : $cari;

	        	$area1	= $this->session->userdata('i_area');
				$area2	= $this->session->userdata('i_area2');
				$area3	= $this->session->userdata('i_area3');
				$area4	= $this->session->userdata('i_area4');
				$area5	= $this->session->userdata('i_area5');
				$config['base_url'] = base_url().'index.php/customerarea/cform/index/'.$cari;
				//$cari = strtoupper($this->input->post('cari', FALSE));
		        if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
					  $query = $this->db->query("	select * from tr_customer_area 
									                      inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
									                      inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
									                      where upper(tr_customer_area.i_customer) ilike '%$cari%' or
									                      upper(tr_customer_area.e_area_name) ilike '%$cari%' or
		  											      upper(tr_customer.e_customer_name) ilike '%$cari%'",false);
		        }else{
					    $query = $this->db->query("	select * from tr_customer_area 
									                      inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
									                      inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
									                      where upper(tr_customer_area.i_customer) ilike '%$cari%' or
									                      upper(tr_customer_area.e_area_name) ilike '%$cari%' or
		  											   	  upper(tr_customer.e_customer_name) ilike '%$cari%'
					                                      and (tr_area.i_area = '$area1' or tr_area.i_area = '$area2' or tr_area.i_area = '$area3'
					                                      or tr_area.i_area = '$area4' or tr_area.i_area = '$area5') ",false);
		        }
				$data['cari'] 		= $cari;
				$config['total_rows'] = $query->num_rows();				
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(5);
				$this->paginationxx->initialize($config);

				$data['page_title'] = $this->lang->line('master_customerarea');
				$data['iarea']='';
				$data['icustomer']='';
				$this->load->model('customerarea/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
				$this->load->view('customerarea/vmainform', $data);

			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu29')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerarea');
			$this->load->view('customerarea/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu29')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerarea')." update";
			if( ($this->uri->segment(4)) && ($this->uri->segment(5)) ){
				$iarea 		= $this->uri->segment(4);
				$icustomer	= $this->uri->segment(5);
				$data['iarea'] 	= $iarea;
				$data['icustomer']	= $icustomer;
				$this->load->model('customerarea/mmaster');
				$data['isi']=$this->mmaster->baca($iarea, $icustomer);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Area Pelanggan:('.$icustomer.') -'.$iarea;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('customerarea/vmainform',$data);
			}else{
				$this->load->view('customerarea/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu29')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 		= $this->input->post('iarea', TRUE);
			$eareaname 	= $this->input->post('eareaname', TRUE);
			$icustomer 	= $this->input->post('icustomer', TRUE);
			$ecustomername 	= $this->input->post('ecustomername', TRUE);
			$this->load->model('customerarea/mmaster');
			$this->mmaster->update($iarea,$icustomer,$eareaname);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Area Pelanggan:('.$icustomer.') -'.$iarea;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

	        $cari = ($this->input->post("cari"))? $this->input->post("cari") : "";
       		$cari = ($this->uri->segment(4)) ? $this->uri->segment(4) : $cari;

        	$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/customerarea/cform/index/'.$cari;
			//$cari = strtoupper($this->input->post('cari', FALSE));
	        if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				  $query = $this->db->query("	select * from tr_customer_area 
								                      inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
								                      inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
								                      where upper(tr_customer_area.i_customer) ilike '%$cari%' or
								                      upper(tr_customer_area.e_area_name) ilike '%$cari%' or
	  											      upper(tr_customer.e_customer_name) ilike '%$cari%'",false);
	        }else{
				    $query = $this->db->query("	select * from tr_customer_area 
								                      inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
								                      inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
								                      where upper(tr_customer_area.i_customer) ilike '%$cari%' or
								                      upper(tr_customer_area.e_area_name) ilike '%$cari%' or
	  											   	  upper(tr_customer.e_customer_name) ilike '%$cari%'
				                                      and (tr_area.i_area = '$area1' or tr_area.i_area = '$area2' or tr_area.i_area = '$area3'
				                                      or tr_area.i_area = '$area4' or tr_area.i_area = '$area5') ",false);
	        }
			$data['cari'] 		= $cari;
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_customerarea');
			$data['iarea']='';
			$data['icustomer']='';
			$this->load->model('customerarea/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('customerarea/vmainform', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu29')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 		= $this->uri->segment(4);
			$icustomer 	= $this->uri->segment(5);
			$this->load->model('customerarea/mmaster');
			$this->mmaster->delete($iarea, $icustomer);
			
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
			  $ip_address	  = $row['ip_address'];
			  break;
			}
			}else{
			$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
			}
			$pesan='Delete Master Area Pelanggan:('.$icustomer.') -'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);
			
			$config['base_url'] = base_url().'index.php/customerarea/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_area 
										inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
										inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
										where upper(tr_customer_area.i_area) like '%$cari%'
										or upper(tr_customer_area.i_customer) like '%$cari%'
									    order by tr_customer_area.i_area, tr_customer_area.i_customer ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_customerarea');
			$data['iarea']		='';
			$data['icustomer']	='';
			$this->load->model('customerarea/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customerarea/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu29')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/customerarea/cform/area/'.$baris.'/sikasep/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_area order by i_area ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->model('customerarea/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(6));
			$this->load->view('customerarea/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu29')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/customerarea/cform/customer/'.$baris.'/sikasep/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query("	select * from tr_customer order by i_customer ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->model('customerarea/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(6));
			$this->load->view('customerarea/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu29')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$cari = ($this->input->post("cari"))? $this->input->post("cari") : "";
       		$cari = ($this->uri->segment(4)) ? $this->uri->segment(4) : $cari;
        	$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/customerarea/cform/index/'.$cari;
			//$cari = strtoupper($this->input->post('cari', FALSE));
        if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
			  $query = $this->db->query("	select * from tr_customer_area 
							                      inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
							                      inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
							                      where upper(tr_customer_area.i_customer) like '%$cari%' or
							                      upper(tr_customer_area.e_area_name) ilike '%$cari%' or
  											      upper(tr_customer.e_customer_name) ilike '%$cari%' ",false);
        }else{
			    $query = $this->db->query("	select * from tr_customer_area 
							                      inner join tr_customer on (tr_customer_area.i_customer=tr_customer.i_customer)
							                      inner join tr_area on (tr_customer_area.i_area=tr_area.i_area)
							                      where upper(tr_customer_area.i_customer) like '%$cari%' or
							                      upper(tr_customer_area.e_area_name) ilike '%$cari%' or
  											      upper(tr_customer.e_customer_name) ilike '%$cari%' 
                                    			  and (tr_area.i_area = '$area1' or tr_area.i_area = '$area2' or tr_area.i_area = '$area3'
                                    			  or tr_area.i_area = '$area4' or tr_area.i_area = '$area5') ",false);
        }
        	  $data['cari'] 		= $cari;
			  $config['total_rows'] = $query->num_rows();				
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(5);
			  $this->paginationxx->initialize($config);
			  $this->load->model('customerarea/mmaster');
			  $data['isi']		=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			  $data['page_title'] 	=$this->lang->line('master_customerarea');
			  $data['iarea']		='';
			  $data['icustomer']	='';
	   		$this->load->view('customerarea/vmainform',$data);
		  }else{
			  $this->load->view('awal/index.php');
		  }
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu29')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		   	$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/customerarea/cform/caricustomer/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/customerarea/cform/caricustomer/'.$baris.'/sikasep/';

			/*$config['base_url'] = base_url().'index.php/customerarea/cform/customer/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query("select i_customer from tr_customer where upper(i_customer) ilike '%$cari%' 
						   or upper(e_customer_name) ilike '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('customerarea/mmaster');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('customerarea/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu29')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/customerarea/cform/area/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/customerarea/cform/cariarea/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/customerarea/cform/cariarea/'.$baris.'/sikasep/';

			$query = $this->db->query("select i_area from tr_area where upper(i_area) ilike '%$cari%' or upper(e_area_name) ilike '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('customerarea/mmaster');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('customerarea/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
