<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
    $this->load->helper(array('file','directory','fusioncharts'));
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('activitysalesman');
			$data['iperiode']	= '';
			$this->load->view('activitysalesman/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu599')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iuser		= $this->session->userdata('user_id');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			if($iperiode!=''){
				$th=substr($iperiode,0,4);
				$bl=substr($iperiode,4,2);
				$prevth  =$th-1;
				$prevdate=$prevth.$bl;
			}
			$config['base_url'] = base_url().'index.php/activitysalesman/cform/view/'.$iperiode.'/index/';
			$this->load->model('activitysalesman/mmaster');
			$data['page_title'] = $this->lang->line('activitysalesman');
			$data['cari']		= $cari;
			$data['iperiode']	= $iperiode;
			$data['th']			= $th;
			$data['prevth']		= $prevth;
			$data['bl']			= $bl;
			$data['iuser']		= $iuser;
			$data['isi']		= $this->mmaster->baca($iperiode,$prevdate,$th,$prevth,$bl,$iuser);
			$data['user']		= $this->mmaster->user();
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Activity Salesman:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('activitysalesman/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
