<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title']	= $this->lang->line('daftartagihan');
			$data['idt']		= '';
			$this->load->model('daftartagihan/mmaster');
			$data['isi']		= "";
			$data['detail']		= "";
			$data['jmlitem']	= "";
			$this->load->view('daftartagihan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan(){
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
//			$idt 	= $this->input->post('idt', TRUE);
			$ddt 	= $this->input->post('ddt', TRUE);
			if($ddt!=''){
				$tmp=explode("-",$ddt);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$ddt=$th."-".$bl."-".$hr;
        $thbl=$th.$bl;
			}
			$iarea			= $this->input->post('iarea', TRUE);
			$vjumlahtot		= $this->input->post('vjumlah', TRUE);
			$vjumlahtot		= str_replace(',','',$vjumlahtot);
			$jml			= $this->input->post('jml', TRUE);
			if(
				($iarea!='')
				&& ($ddt!='')
				&& ($vjumlahtot>0)
				&& ($jml!='0')
			  )
			{
				$this->db->trans_begin();
				$this->load->model('daftartagihan/mmaster');
				$idt=$this->mmaster->runningnumberdt($iarea,$thbl);
//				$query=$this->db->query("select * from tm_dt where i_dt='$idt' and i_area='$iarea'");
				$fsisa='f';
//				if($query->num_rows()==0){
					for($i=1;$i<=$jml;$i++){
					  	$inota				= $this->input->post('inota'.$i, TRUE);
					  	$dnota				= $this->input->post('dnota'.$i, TRUE);
						if($dnota!=''){
							$tmp=explode("-",$dnota);
							$th=$tmp[2];
							$bl=$tmp[1];
							$hr=$tmp[0];
							$dnota=$th."-".$bl."-".$hr;
						}
						$icustomer			= $this->input->post('icustomer'.$i, TRUE);
						$vsisa				= $this->input->post('vsisa'.$i, TRUE);
						$vsisa				= str_replace(',','',$vsisa);
						$vjumlah			= $this->input->post('vjumlah'.$i, TRUE);
						$vjumlah			= str_replace(',','',$vjumlah);
						if($vsisa>0){
							$fsisa='t';
						}
						$this->mmaster->insertdetail($idt,$ddt,$inota,$iarea,$dnota,$icustomer,$vsisa,$vjumlah,$i);
					}
					$this->mmaster->insertheader($idt,$iarea,$ddt,$vjumlahtot,$fsisa);
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
						$this->db->trans_commit();
#            $this->db->trans_rollback();

				    $sess=$this->session->userdata('session_id');
				    $id=$this->session->userdata('user_id');
				    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				    $rs		= pg_query($sql);
				    if(pg_num_rows($rs)>0){
					    while($row=pg_fetch_assoc($rs)){
						    $ip_address	  = $row['ip_address'];
						    break;
					    }
				    }else{
					    $ip_address='kosong';
				    }
				    $query 	= pg_query("SELECT current_timestamp as c");
				    while($row=pg_fetch_assoc($query)){
					    $now	  = $row['c'];
				    }
				    $pesan='Input DT No:'.$idt.' Area:'.$iarea;
				    $this->load->model('logger');
				    $this->logger->write($id, $ip_address, $now , $pesan );

						$data['sukses']			= true;
						$data['inomor']			= $idt;
						$this->load->view('nomor',$data);
					}
//				}
			}
			 /*
			else{
				$this->load->view('awal/index.php');
			}
			*/
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('daftartagihan');
			$this->load->view('daftartagihan/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')))
		   ){
      $area1	= $this->session->userdata('i_area');
			$data['page_title'] = $this->lang->line('daftartagihan')." update";
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
			  ){
				$idt	= $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$dfrom= $this->uri->segment(6);
				$dto 	= $this->uri->segment(7);
				$tgl 	= $this->uri->segment(8);
        if($idt!=''){
				  $tmp=explode("-",$idt);
				  $siji=$tmp[0];
				  $loro=$tmp[1];
				  $idtx=$siji."/".$loro;
			  }
        if($tgl!=''){
				  $tmp=explode("-",$tgl);
				  $th=$tmp[2];
				  $bl=$tmp[1];
				  $hr=$tmp[0];
				  $tgl=$th."-".$bl."-".$hr;
			  }

				$query 				= $this->db->query("select i_dt from tm_dt where i_dt = '$idt' and i_area='$iarea' and d_dt='$tgl'");
        if($query->num_rows()>0){
				  $query 				= $this->db->query("select i_dt from tm_dt_item where i_dt = '$idt' and i_area='$iarea' and d_dt='$tgl'");
				  $data['jmlitem']  = $query->num_rows();
				}elseif($query->num_rows()==0){
					$query 				= $this->db->query("select i_dt from tm_dt_item where i_dt = '$idtx' and i_area='$iarea' and d_dt='$tgl'");
					$data['jmlitem']  = $query->num_rows();
					$idt=$idtx;
				}

        $query 				= $this->db->query("select i_pelunasan from tm_pelunasan where i_dt = '$idt' and i_area='$iarea' and d_dt='$tgl'
                                          and f_pelunasan_cancel='f'");
				if($query->num_rows()>0){
          $data['bisaedit']=false;
        }else{
          $data['bisaedit']=true;
        }

				$data['idt'] 		= $idt;
				$data['iarea']	= $iarea;
				$data['dfrom']	= $dfrom;
				$data['dto']		= $dto;
				$this->load->model('daftartagihan/mmaster');
				$data['isi']		= $this->mmaster->baca($idt,$iarea,$tgl);
				$data['detail']	= $this->mmaster->bacadetail($idt, $iarea,$tgl);
        $data['area1']  = $area1;
		 		$this->load->view('daftartagihan/vmainform',$data);
			}else{
				$this->load->view('daftartagihan/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$idt 	= $this->input->post('idt', TRUE);
			$xddt = $this->input->post('xddt', TRUE);
			$ddt 	= $this->input->post('ddt', TRUE);
			if($ddt!=''){
				$tmp=explode("-",$ddt);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$ddt=$th."-".$bl."-".$hr;
				$tahun=$th;
			}
			if($xddt!=''){
				$tmp=explode("-",$xddt);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$xddt=$th."-".$bl."-".$hr;
			}
			$iarea			= $this->input->post('iarea', TRUE);
			$vjumlah		= $this->input->post('vjumlah',TRUE);
			$vjumlah		= str_replace(',','',$vjumlah);
			$jml			= $this->input->post('jml', TRUE);
			if(
				($idt!='')
				&& ($iarea!='')
				&& (($vjumlah!='') || ($vjumlah!='0'))
				&& ($ddt!='')
			  )
			{
				$this->db->trans_begin();
				$this->load->model('daftartagihan/mmaster');
				$fsisa='f';
				for($i=1;$i<=$jml;$i++){
					$baris				= $this->input->post('baris'.$i, TRUE);
					$inota				= $this->input->post('inota'.$i, TRUE);
			  	$dnota				= $this->input->post('dnota'.$i, TRUE);
					if($dnota!=''){
						$tmp=explode("-",$dnota);
						$th=$tmp[2];
						$bl=$tmp[1];
						$hr=$tmp[0];
						$dnota=$th."-".$bl."-".$hr;
					}
					$icustomer			= $this->input->post('icustomer'.$i, TRUE);
					$vsisa				= $this->input->post('vsisa'.$i, TRUE);
					$vsisa				= str_replace(',','',$vsisa);
					$vjumlah			= $this->input->post('vjumlah'.$i, TRUE);
					$vjumlah			= str_replace(',','',$vjumlah);
					if($vsisa>0){
						$fsisa='t';
					}
					$this->mmaster->deletedetail($idt,$ddt,$inota,$iarea,$vjumlah,$xddt);
					$this->mmaster->insertdetail($idt,$ddt,$inota,$iarea,$dnota,$icustomer,$vsisa,$vjumlah,$baris);
				}
				$vjumlah		= $this->input->post('vjumlah',TRUE);
				$vjumlah		= str_replace(',','',$vjumlah);
				$this->mmaster->updateheader($idt,$iarea,$ddt,$vjumlah,$fsisa);
				if ( ($this->db->trans_status() === FALSE) )
				{
				  $this->db->trans_rollback();
				}else{
				  $this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Update DT No:'.$idt.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $idt;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$idt	= $this->uri->segment(4);
			$iarea= $this->uri->segment(5);
			$dfrom= $this->uri->segment(6);
			$dto 	= $this->uri->segment(7);
			$tgl 	= $this->uri->segment(8);
			$this->load->model('daftartagihan/mmaster');
			$this->mmaster->delete($idt,$ddt,$iarea);
	    $sess=$this->session->userdata('session_id');
	    $id=$this->session->userdata('user_id');
	    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	    $rs		= pg_query($sql);
	    if(pg_num_rows($rs)>0){
		    while($row=pg_fetch_assoc($rs)){
			    $ip_address	  = $row['ip_address'];
			    break;
		    }
	    }else{
		    $ip_address='kosong';
	    }
	    $query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
		    $now	  = $row['c'];
	    }
	    $pesan='Delete DT No:'.$idt.' Area:'.$iarea;
	    $this->load->model('logger');
	    $this->logger->write($id, $ip_address, $now , $pesan );
			$data['page_title']	= $this->lang->line('daftartagihan');
			$data['idt']		= '';
			$data['iarea']		= '';
			$data['jmlitem']	= '';
			$data['detail']		= '';
			$data['isi']		= '';
			$this->load->view('daftartagihan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $area1  	= $this->session->userdata('i_area');
			$idt			= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$inota		= $this->uri->segment(6);
			$vjumlah	= $this->uri->segment(7);
			$dfrom		= $this->uri->segment(8);
			$dto			= $this->uri->segment(9);
			$ddt			= $this->uri->segment(10);
      if($ddt!=''){
				$tmp=explode("-",$ddt);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$ddt=$th."-".$bl."-".$hr;
			}
			$this->db->trans_begin();
			$this->load->model('daftartagihan/mmaster');
			$this->mmaster->deletedetail($idt,$ddt,$inota,$iarea,$vjumlah,$ddt);
			if ($this->db->trans_status() === FALSE)
			{
			  $this->db->trans_rollback();
			}else{
			  $this->db->trans_commit();
#			  $this->db->trans_rollback();

		    $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		= pg_query($sql);
		    if(pg_num_rows($rs)>0){
			    while($row=pg_fetch_assoc($rs)){
				    $ip_address	  = $row['ip_address'];
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }
		    $query 	= pg_query("SELECT current_timestamp as c");
		    while($row=pg_fetch_assoc($query)){
			    $now	  = $row['c'];
		    }
		    $pesan='Delete Item DT No:'.$idt.' Area:'.$iarea;
		    $this->load->model('logger');
		    $this->logger->write($id, $ip_address, $now , $pesan );

				$data['page_title'] = $this->lang->line('daftartagihan')." Update";
				$query 					= $this->db->query("select * from tm_dt_item  where i_dt = '$idt' and i_area='$iarea' and d_dt='$ddt'");
				$data['jmlitem']= $query->num_rows();
        $query 				= $this->db->query("select i_pelunasan from tm_pelunasan where i_dt = '$idt' and i_area='$iarea' and d_dt='$ddt'");
				if($query->num_rows()>0){
          $data['bisaedit']=false;
        }else{
          $data['bisaedit']=true;
        }
        $data['area1']  = $area1;
				$data['idt'] 		= $idt;
				$data['iarea']	= $iarea;
				$data['dfrom']	= $dfrom;
				$data['dto']		= $dto;
				$data['isi']		= $this->mmaster->baca($idt,$iarea,$ddt);
				$data['detail']	= $this->mmaster->bacadetail($idt,$iarea,$ddt);
				$this->load->view('daftartagihan/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function nota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']= $this->uri->segment(4);
			$baris		= $this->uri->segment(4);
			$area		= $this->uri->segment(5);
			$data['area']	= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/daftartagihan/cform/nota/'.$baris.'/'.$area.'/index/';

      $query	= $this->db->query("select a.i_nota, a.i_customer
                                  from tm_nota a, tr_customer b, tr_customer_groupbayar c
                                  where 
                                  a.i_customer=c.i_customer and a.i_customer=b.i_customer and 
                                  a.f_ttb_tolak='f' and 
                                  a.f_nota_cancel='f' and
                                  a.v_sisa>0 and
                                  not (a.i_nota isnull or trim(a.i_nota)='') and 
                                  (
                                  (c.i_customer_groupbayar in(select i_customer_groupbayar from tr_customer_groupbayar 
                                  where substring(i_customer,1,2)='$area'))
                                  )
		                              group by a.i_nota, a.i_area, a.d_nota, a.i_customer, b.e_customer_name, a.v_nota_netto, a.v_sisa, a.d_jatuh_tempo");
/*
      $query	= $this->db->query("select a.i_nota
		                              from tm_nota a, tr_customer b
		                              where 
		                              a.i_customer=b.i_customer and 
		                              a.f_ttb_tolak='f' and 
                                  a.f_nota_cancel='f' and
		                              a.i_area='$area' and  
		                              a.v_sisa>0 and
		                              not a.i_nota isnull
		                              group by a.i_nota, a.i_area, a.d_nota, b.i_customer, b.e_customer_name, a.v_nota_netto, a.v_sisa, a.d_jatuh_tempo");
*/

			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->bacanota($area,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
      			$data['cari']='';
			$this->load->view('daftartagihan/vlistnota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carinota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari	= strtoupper($this->input->post('cari', FALSE));
      if($cari==''){
				if($this->uri->segment(6)!='xqzv'){
					$cari=strtoupper($this->uri->segment(6));
				}
			}
			$data['baris']= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$area				  = $this->uri->segment(5);
			$data['area']	= $this->uri->segment(5);
			if($cari==''){
				$config['base_url'] = base_url().'index.php/daftartagihan/cform/carinota/'.$baris.'/'.$area.'/xqzv/';
			}else{
				$config['base_url'] = base_url().'index.php/daftartagihan/cform/carinota/'.$baris.'/'.$area.'/'.$cari.'/';
			}
		$query	= $this->db->query("select a.i_nota from tm_nota a, tr_customer b, tr_customer_groupbayar c where 
                                a.i_customer=c.i_customer and a.i_customer=b.i_customer and 
                                a.f_ttb_tolak='f' and 
                                a.f_nota_cancel='f' and
                                a.v_sisa>0 and
                                not (a.i_nota isnull or trim(a.i_nota)='') and 
                                (
                                (c.i_customer_groupbayar in(select i_customer_groupbayar from tr_customer_groupbayar 
                                where substring(i_customer,1,2)='$area'))
                                )and 
					                      (upper(a.i_nota) like '%$cari%' or 
					                      a.i_nota_old like '%$cari%' or 
					                      upper(b.i_customer) like '%$cari%' or 
					                      upper(b.e_customer_name) like '%$cari%') 
					                      group by a.i_nota, a.i_area, a.d_nota, b.i_customer, b.e_customer_name, a.v_nota_netto, a.v_sisa, a.d_jatuh_tempo,
                                b.e_customer_city",false);

/*
      $query	= $this->db->query("select a.i_nota
			                            from tm_nota a, tr_customer b
			                            where 
			                            a.i_customer=b.i_customer and 
			                            a.f_ttb_tolak='f' and 
                                  a.f_nota_cancel='f' and
			                            a.i_area='$area' and  
			                            a.v_sisa>0 and
			                            not a.i_nota isnull and 
			                            (upper(a.i_nota) like '%$cari%' or a.i_nota_old like '%$cari%' or upper(b.i_customer) like '%$cari%' 
                                  or upper(b.e_customer_name) like '%$cari%') 
			                            group by a.i_nota, a.i_area, a.d_nota, b.i_customer, b.e_customer_name, a.v_nota_netto, a.v_sisa,
                                  a.d_jatuh_tempo",false);
*/
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->carinota($cari,$area,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$data['cari']=$cari;
			$this->load->view('daftartagihan/vlistnota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function notaupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['baris']= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$area				  = $this->uri->segment(5);
			$data['area']	= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/daftartagihan/cform/notaupdate/'.$baris.'/'.$area.'/index/';
			$query = $this->db->query(" select * from
										              (
										              select a.*, b.e_customer_name, c.e_salesman_name 
										              from tm_nota a, tr_customer b, tr_salesman c 
										              where a.i_customer=b.i_customer and a.i_salesman=c.i_salesman and a.f_ttb_tolak='f' 
                                  and a.i_area='$area' and a.v_sisa>0 and a.f_nota_cancel='f'
										              union all
										              select a.*, b.e_customer_name, c.e_salesman_name 
										              from tm_notakoreksi a, tr_customer b, tr_salesman c 
										              where a.i_customer=b.i_customer and a.i_salesman=c.i_salesman and a.f_ttb_tolak='f' 
                                  and a.i_area='$area' and a.v_sisa>0 and a.f_nota_cancel='f'
										              ) as a" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->bacanota($area,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$this->load->view('daftartagihan/vlistnotaupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carinotaupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari 				= strtoupper($this->input->post('cari', FALSE));
      if($cari=='')$cari=strtoupper($this->uri->segment(6));
			$data['baris']		= $this->uri->segment(4);
			$baris				= $this->uri->segment(4);
			$area				= $this->uri->segment(5);
			$data['area']		= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/daftartagihan/cform/notaupdate/'.$baris.'/'.$area.'/'.$cari.'/';
			$query = $this->db->query(" select * from
										              (
										              select a.*, b.e_customer_name, c.e_salesman_name 
										              from tm_nota a, tr_customer b, tr_salesman c 
										              where a.i_customer=b.i_customer and a.i_salesman=c.i_salesman and a.f_ttb_tolak='f' 
                                  and a.i_area='$area' and a.v_sisa>0 and a.f_nota_cancel='f'
										              union all
										              select a.*, b.e_customer_name, c.e_salesman_name 
										              from tm_notakoreksi a, tr_customer b, tr_salesman c 
										              where a.i_customer=b.i_customer and a.i_salesman=c.i_salesman and a.f_ttb_tolak='f' 
                                  and a.i_area='$area' and a.v_sisa>0 and a.f_nota_cancel='f'
										              ) as a
										              where upper(a.i_nota) like '%$cari%'
                                  or upper(a.i_customer) like '%$cari%'
                                  or upper(a.e_customer_name) like '%$cari%'" ,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->carinota($cari,$area,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$this->load->view('daftartagihan/vlistnotaupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/daftartagihan/cform/area/index/';
			$config['per_page'] = '10';
			$allarea	= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari=strtoupper($this->input->post("cari",false));
			/* Disabled 21042011
			$query = $this->db->query("select * from tr_area where (upper(i_area) = '$area1' or upper(i_area) = '$area2' 
                                 or upper(i_area) = '$area3' or upper(i_area) = '$area4' 
                                 or upper(i_area) = '$area5')",false);
            */

			if($allarea=='t'){
				$query = $this->db->query(" select * from tr_area ", false);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$query = $this->db->query(" select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%')", false);
			}else{
				$query = $this->db->query(" select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' ", false);
			}
			            
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']		=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$cari,$area1,$area2,$area3,$area4,$area5,$allarea);
			$this->load->view('daftartagihan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$allarea	= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/daftartagihan/cform/area/index/';
			$config['per_page'] = '10';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			/* Disabled 21042011
			$query  = $this->db->query("select * from tr_area where (upper(i_area) = '$area1' or upper(i_area) = '$area2' 
                                 or upper(i_area) = '$area3' or upper(i_area) = '$area4' 
                                 or upper(i_area) = '$area5') and upper(e_area_name) like '%$cari%'",false);
            */
			if($allarea=='t'){
				$query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') ", false);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00')){
				$query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') and (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%' or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%')", false);
			}else{
				$query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') and i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5' ", false);
			}            
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']		=$this->mmaster->cariarea($config['per_page'],$this->uri->segment(5),$cari,$area1,$area2,$area3,$area4,$area5,$allarea);
			$this->load->view('daftartagihan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function areaupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&


			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/daftartagihan/cform/areaupdate/index/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));
			$query = $this->db->query("select * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%'",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']		=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('daftartagihan/vlistareaupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariareaupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/daftartagihan/cform/areaupdate/index/';
			$config['per_page'] = '10';
			$cari=strtoupper($this->input->post("cari",false));
			$query = $this->db->query("select * from tr_area where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%'",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('daftartagihan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']		=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('daftartagihan/vlistareaupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu101')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/daftartagihan/cform/index/';
			$query=$this->db->query("select * from tm_dt",false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(4);
			$this->pagination->initialize($config);
			$cari 				= $this->input->post('cari', FALSE);
			$cari				= strtoupper($cari);
			$this->load->model('daftartagihan/mmaster');
			$data['isi']		= $this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title']	= $this->lang->line('daftartagihan');
			$data['idt']		= '';
			$data['iarea']	= '';
			$data['jmlitem']	= '';
			$data['detail']		= '';
	 		$this->load->view('daftartagihan/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
