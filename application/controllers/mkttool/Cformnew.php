<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$user 			= $this->session->userdata('user_id');
			$data['page_title'] = $this->lang->line('promomarketing');
			
			$this->load->model('mkttool/mmaster');
      		$data['isi']=$this->mmaster->bacadata($user);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$this->load->view('mkttool/vform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function next1()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isalesman 		= $this->input->post('isalesman', TRUE);
			$esalesman 		= $this->input->post('esalesman', TRUE);
			$icustomer 		= $this->input->post('icustomer', TRUE);
			$ecustomer 		= $this->input->post('ecustomer', TRUE);
		  	$jenispromo	= $this->input->post('jenispromo', TRUE);
			
		  	// $jenispromo2	= $this->input->post('jenispromo2', TRUE);
		  	// $jenispromo3	= $this->input->post('jenispromo3', TRUE);
			//085960155956

			if ((isset($isalesman) && $isalesman != '') && (isset($icustomer) && $icustomer != '') )
			{
			   	$this->load->model('mkttool/mmaster');
        		$this->db->trans_begin();

				if($jenispromo == 1){
					$this->load->view('mkttool/vform1');
				}else if($jenispromo == 2){
					$this->load->view('mkttool/vform2');
				}else if($jenispromo == 3){
					$this->load->view('mkttool/vform3');
				}
				

        if ( ($this->db->trans_status() === FALSE) )
        {
          $this->db->trans_rollback();
        }else{
          $this->db->trans_commit();
#             $this->db->trans_rollback();
          $data['sukses']         = true;
          $data['inomor']         = $iproduct;
          $this->load->view('nomor',$data);
        }
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_productprice_manual');
			$this->load->view('mkttool/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function next()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$isalesman 		= $this->input->post('isalesman', TRUE);
			$esalesman 		= $this->input->post('esalesman', TRUE);
			$icustomer 		= $this->input->post('icustomer', TRUE);
			$ecustomer 		= $this->input->post('ecustomer', TRUE);
		  	$jenispromo		= $this->input->post('jenispromo', TRUE);
			$user 			= $this->session->userdata('user_id');
			$iarea			= $this->input->post('iarea', TRUE);
			$data['page_title'] = $this->lang->line('master_productprice_manual')." update";
			if( $isalesman != '' && $icustomer != '' ){

				$this->load->model('mkttool/mmaster');
				$query = $this->db->query(" select * from tm_marketing_tool_tmp where i_user='$user'",false);
				if($query->num_rows()>0){ 
					$this->mmaster->update_tmp($isalesman,$esalesman,$icustomer,$ecustomer,$jenispromo,$user,$iarea);
				}else{
					$this->mmaster->insert_tmp($isalesman,$esalesman,$icustomer,$ecustomer,$jenispromo,$user,$iarea);
				}
				if($jenispromo == 1){
					$this->load->view('mkttool/vform1');
				}else if($jenispromo == 2){
					$this->load->view('mkttool/vform2');
				}else if($jenispromo == 3){
					$this->load->view('mkttool/vform3');
				}
		 		// $this->load->view('mkttool/vmainform',$data);
			}else{
				$this->load->view('mkttool/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function save()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*view1*/
			$iloyal 	= $this->input->post('iloyal', TRUE);
			$eremark1 	= $this->input->post('eremarkpengajuan', TRUE);
			$vestimasi 	= $this->input->post('vestimasi', TRUE);
			/*view2*/
			$jenispromo2	= $this->input->post('jenispromo2', TRUE);
			$diskon			= $this->input->post('diskon', TRUE);
			$tandabukti		= $this->input->post('tandabukti', TRUE);
			$remark2		= $this->input->post('remark2', TRUE);

			/*view3*/
			$jenismarket	= $this->input->post('jenismarket', TRUE);
			$panjang		= $this->input->post('panjang', TRUE);
			$lebar			= $this->input->post('lebar', TRUE);
			$tinggi			= $this->input->post('tinggi', TRUE);
			$ukuranlain		= $this->input->post('ukuranlain', TRUE);
			$jumqty			= $this->input->post('jumqty', TRUE);
			$remark3		= $this->input->post('remark3', TRUE);
			// if($jenismarket == '14'){
				$toollainnya		= $this->input->post('toollainnya', TRUE);
			// }
			

			/* $nproductmargin	= str_replace(",","",$this->input->post('nproductmargin', TRUE));
			$vproductmill	= str_replace(",","",$this->input->post('vproductmill', TRUE));  */
			$user 			= $this->session->userdata('user_id');
			$this->load->model('mkttool/mmaster');
			$imkt =$this->mmaster->runningnumber();
			// var_dump($imkt);
			// die();
			$query = $this->db->query(" select * from tm_marketing_tool_tmp where i_user='$user'",false);
			if($query->num_rows()>0){ 
				$row=$query->row();
           		$isalesman = $row->i_salesman;
				$icustomer = $row->i_customer;
				$iarea 	   = $row->i_area;
				$ecustomer = $row->e_customer_name;
				$jenis 	   = $row->i_mkt_type;
			}

			$this->mmaster->save($iloyal,$eremark1,$vestimasi,$isalesman,$icustomer,$iarea,$ecustomer,$jenis,$imkt,$jenispromo2,$diskon,$tandabukti,$remark2,$remark3,$jenismarket,$panjang,$lebar,$tinggi,$ukuranlain,$jumqty,$toollainnya);
			$this->mmaster->deletetmp($user);
			if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                 $this->db->trans_commit();
                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Simpan Marketing Tools:'.$imkt;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );

                 $data['sukses'] = true;
                 $data['inomor'] = $imkt;
                 $this->load->view('nomor',$data);
              }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('mkttool/mmaster');
			/* $this->mmaster->delete($iproduct,$iproductgrade); */
			$config['base_url'] = base_url().'index.php/mkttool/cform/index/';

			$query = $this->db->query("SELECT
											a.*,
											b.e_loyal,
											c.nama,
											d.e_namamkt,
											e.e_customer_name,
											f.e_salesman_name,
											CASE
												WHEN a.i_mkt_type = 1 THEN 'Loyalitas'
												WHEN a.i_mkt_type = 2 THEN 'Promo'
												WHEN a.i_mkt_type = 3 THEN 'Marketing Tools'
											END AS marketing_type,
											CASE
												WHEN a.i_promo_type2 = '1' THEN 'SELL IN'
												WHEN a.i_promo_type2 = '2' THEN 'SELL OUT'
												WHEN a.i_promo_type2 = '3' THEN 'LISTING FEE'
											END AS promotype2
										FROM
											tm_marketing_tool a
										LEFT JOIN tr_mkttools_loyal b ON
											(a.i_loyalitas_type = b.i_loyal)
										LEFT JOIN tr_mkttools_promo c ON
											(trim(a.e_tanda_bukti) = trim(c.nama))
										LEFT JOIN tr_jns_mkttools d ON
											(a.e_jenis_market = d.i_jnsmkt)
										INNER JOIN tr_customer e ON
											(a.i_customer = e.i_customer)
										INNER JOIN tr_salesman f ON
											(a.i_salesman = f.i_salesman)
										WHERE
											a.f_cancel = 'f'
										ORDER BY
											a.i_mkt_type",false);
			/* var_dump('yyy');
			die();	 */										
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '20';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_productprice_manual');
			$data['iproduct']='';
			$data['iproductgrade']='';
			$this->load->model('mkttool/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasadaya($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('mkttool/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/mkttool/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_price 
					                        where (upper(tr_product_price.i_product) like '%$cari%' 
					                        or upper(tr_product_price.e_product_name) = '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '20';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_productprice_manual');
			$data['iproduct']='';
			$data['iproductgrade']='';
	 		$this->load->view('mkttool/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function salesman()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/mkttool/cform/salesman/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_salesman, e_salesman_name from tr_salesman
										where upper(i_salesman) like '%$cari%' or upper(e_salesman_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_salesman');
			$data['isi']=$this->mmaster->bacasalesman($config['per_page'],$this->uri->segment(5));

			// @json_decode(file_get_contents(VIEWPATH . module('source/jenisloyalitas.json')))
			$this->load->view('mkttool/vlistsalesman', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function loyal()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/mkttool/cform/loyal/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_loyal, e_loyal from tr_mkttools_loyal
										where upper(i_loyal) like '%$cari%' or upper(e_loyal) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_loyal');
			$data['isi']=$this->mmaster->bacaloyal($config['per_page'],$this->uri->segment(5));

			// @json_decode(file_get_contents(VIEWPATH . module('source/jenisloyalitas.json')))
			$this->load->view('mkttool/vlistloyal', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function mkttool()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/mkttool/cform/mkttool/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_jnsmkt, e_namamkt from tr_jns_mkttools
										where upper(i_jnsmkt) like '%$cari%' or upper(e_namamkt) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_mkttool');
			$data['isi']=$this->mmaster->bacamkttools($config['per_page'],$this->uri->segment(5));

			// @json_decode(file_get_contents(VIEWPATH . module('source/jenisloyalitas.json')))
			$this->load->view('mkttool/vlistmkttool', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function mktpromo()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/mkttool/cform/mktpromo/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select * from tr_mkttools_promo
										where upper(nama) like '%$cari%' or upper(keterangan) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_mktpromo');
			$data['isi']=$this->mmaster->bacamktpromo($config['per_page'],$this->uri->segment(5));

			// @json_decode(file_get_contents(VIEWPATH . module('source/jenisloyalitas.json')))
			$this->load->view('mkttool/vlistmktpromo', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}



	function carisalesman()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/mkttool/cform/carisalesman/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$isalesman 	= $this->uri->segment(4);
			$query = $this->db->query("select i_salesman, e_salesman_name from tr_salesman
										where upper(i_salesman) like '%$cari%' or upper(e_salesman_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_salesman');
			$data['isi']=$this->mmaster->carisalesman($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('mkttool/vlistsalesman', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris=$this->uri->segment(4);
			$cari =$this->uri->segment(5);
			$salesman =$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/mkttool/cform/customer/'.$baris.'/'.$cari.'/'.$salesman.'/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			// $periode=date('Ym');
			$periode='202211';
			$query = $this->db->query("SELECT
											DISTINCT a.i_customer,
											a.e_customer_name,
											a.i_area
										FROM
											tr_customer a
										INNER JOIN tr_customer_salesman b ON
											(a.i_customer = b.i_customer)
										WHERE
											b.e_periode = '$periode' AND i_salesman = '$salesman'
										",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($cari,$config['per_page'],$this->uri->segment(7),$periode,$salesman);
			$data['isalesman']=$salesman;
			$this->load->view('mkttool/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$periode=date('Ym');
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/mkttool/cform/customer/'.$cari.'/'.'index/';
			$query = $this->db->query("SELECT
											DISTINCT a.i_customer,
											a.e_customer_name,
											a.i_area
										FROM
											tr_customer a
										INNER JOIN tr_customer_salesman b ON
											(a.i_customer = b.i_customer)
										WHERE
											b.e_periode = '$periode' AND i_salesman = '$salesman'
										AND upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('mkttool/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
