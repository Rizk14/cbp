<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('PHPExcel');
		$this->load->library('PHPExcel/IOFactory');
		$this->load->model('mkttool/mmaster');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$user 			= $this->session->userdata('user_id');
			$data['page_title'] = $this->lang->line('promomarketing');
			
			$this->load->model('mkttool/mmaster');
      		$data['isi']=$this->mmaster->bacadata($user);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$this->load->view('mkttool/vform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function next1()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isalesman 		= $this->input->post('isalesman', TRUE);
			$esalesman 		= $this->input->post('esalesman', TRUE);
			$icustomer 		= $this->input->post('icustomer', TRUE);
			$ecustomer 		= $this->input->post('ecustomer', TRUE);
		  	$jenispromo	= $this->input->post('jenispromo', TRUE);
			
			if ((isset($isalesman) && $isalesman != '') && (isset($icustomer) && $icustomer != '') )
			{
				$this->db->trans_begin();
			   	$this->load->model('mkttool/mmaster');
        		

				if($jenispromo == 1){
					$this->load->view('mkttool/vform1');
				}else if($jenispromo == 2){
					$this->load->view('mkttool/vform2');
				}else if($jenispromo == 3){
					$this->load->view('mkttool/vform3');
				}
				
        if ( ($this->db->trans_status() === FALSE) )
        {
          $this->db->trans_rollback();
        }else{
          $this->db->trans_commit();
#             $this->db->trans_rollback();
          $data['sukses']         = true;
          $data['inomor']         = $iproduct;
          $this->load->view('nomor',$data);
        }
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_productprice_manual');
			$this->load->view('mkttool/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function next()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isalesman 		= $this->input->post('isalesman', TRUE);
			$esalesman 		= $this->input->post('esalesman', TRUE);
			$icustomer 		= $this->input->post('icustomer', TRUE);
			$ecustomer 		= $this->input->post('ecustomer', TRUE);
		  	$jenispromo		= $this->input->post('jenispromo', TRUE);
			$user 			= $this->session->userdata('user_id');
			$iarea			= $this->input->post('iarea', TRUE);

			$data['page_title'] = $this->lang->line('promomarketing');
			if( $isalesman != '' && $icustomer != '' ){
				$this->load->model('mkttool/mmaster');
				$query = $this->db->query(" select * from tm_marketing_tool_tmp where i_user='$user'",false);
				if($query->num_rows()>0){ 
					$this->mmaster->update_tmp($isalesman,$esalesman,$icustomer,$ecustomer,$jenispromo,$user,$iarea);
				}else{
					$this->mmaster->insert_tmp($isalesman,$esalesman,$icustomer,$ecustomer,$jenispromo,$user,$iarea);
				}
				if($jenispromo == 1){
					$this->load->view('mkttool/vform1',$data);
				}else if($jenispromo == 2){
					$this->load->view('mkttool/vform2',$data);
				}else if($jenispromo == 3){
					$this->load->view('mkttool/vform3',$data);
				}
			}else{
				$this->load->view('mkttool/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function prosesedit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isalesman 		= $this->input->post('isalesman', TRUE);
			$esalesman 		= $this->input->post('esalesman', TRUE);
			$icustomer 		= $this->input->post('icustomer', TRUE);
			$ecustomer 		= $this->input->post('ecustomer', TRUE);
		  	$jenispromo		= $this->input->post('jenispromo', TRUE);
			$imkt			= $this->input->post('imkt', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$user 			= $this->session->userdata('user_id');
			$iapprove			= $this->input->post('iapprove', TRUE);
			$dapprove			= $this->input->post('dapprove', TRUE);
			$ilevel				= $this->input->post('ilevel', TRUE);



			$data['page_title'] = $this->lang->line('promomarketingedit');
			if( $imkt != ''){
				$this->load->model('mkttool/mmaster');
			$query = $this->db->query("SELECT * FROM tm_marketing_tool where i_mkt = '$imkt'");
			 if($query->num_rows()>0){ 
				$row=$query->row();
           		$imkttype 	= $row->i_mkt_type;
			 }
			 if($jenispromo == $imkttype){
				$cocok = 1;
			 }
			 $data['isalesman']		= $isalesman;
			 $data['esalesman']		= $esalesman;
			 $data['icustomer']		= $icustomer;
			 $data['ecustomer']		= $ecustomer;
			 $data['jenispromo']	= $jenispromo;
			 $data['imkt']			= $imkt;
			 $data['iarea']			= $iarea;
			 $data['iapprove']		= $iapprove;
			 $data['dapprove']		= $dapprove;
			 $data['ilevel']		= $ilevel;
			 $data['data']			= $this->mmaster->bacaedit($imkt);

			 /* var_dump($jenispromo);
			 die(); */

				if($jenispromo == 1){
					$this->load->view('mkttool/vformedit1',$data);
				}else if($jenispromo == 2){
					$this->load->view('mkttool/vformedit2',$data);
				}else if($jenispromo == 3){
					$this->load->view('mkttool/vformedit3',$data);
				}
			}else{
				$this->load->view('mkttool/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approveone()
	{
		$imkt  		= $this->uri->segment(4);
		$this->load->model('mkttool/mmaster');

		/* $data['page_title'] = $this->lang->line('listspb'); */
		$data['isi']		= $this->mmaster->bacaedit($imkt);
		$data['note']		= $this->mmaster->bacanote2($imkt);
		$data['imkt'] 		= $imkt;

		$this->load->view('mkttool/vformapproveone', $data);
	}

	function approvetwo()
	{
		$imkt  		= $this->uri->segment(4);
		$this->load->model('mkttool/mmaster');

		/* $data['page_title'] = $this->lang->line('listspb'); */
		$data['isi']		= $this->mmaster->bacaedit($imkt);
		$data['note']		= $this->mmaster->bacanote2($imkt);
		$data['imkt'] 		= $imkt;

		$this->load->view('mkttool/vformapprovetwo', $data);
	}

	function approvetri()
	{
		$imkt  		= $this->uri->segment(4);
		$level		= $this->session->userdata('level');
		$this->load->model('mkttool/mmaster');

		/* $data['page_title'] = $this->lang->line('listspb'); */
		$data['isi']		= $this->mmaster->bacaedit($imkt);
		$data['note']		= $this->mmaster->bacanote2($imkt);
		$data['imkt'] 		= $imkt;
		$data['level'] 		= $level;

		$this->load->view('mkttool/vformapprovetri', $data);
	}
	

	function save()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*view1*/
			$iloyal 	= $this->input->post('iloyal', TRUE);
			$eremark1 	= $this->input->post('eremarkpengajuan', TRUE);
			$vestimasi 	= $this->input->post('vestimasi', TRUE);
			$vestimasi  = str_replace(',', '', $vestimasi);



			/*view2*/
			$jenispromo2	= $this->input->post('jenispromo2', TRUE);
			$diskon			= $this->input->post('diskon', TRUE);
			$tandabukti		= $this->input->post('tandabukti', TRUE);
			$remark2		= $this->input->post('remark2', TRUE);
			$vtarget 		= $this->input->post('target', TRUE);
			$vtarget  	= str_replace(',', '', $vtarget);


			/*view3*/
			$jenismarket	= $this->input->post('jenismarket', TRUE);
			$panjang		= $this->input->post('panjang', TRUE);
			$lebar			= $this->input->post('lebar', TRUE);
			$tinggi			= $this->input->post('tinggi', TRUE);
			$ukuranlain		= $this->input->post('ukuranlain', TRUE);
			$jumqty			= $this->input->post('jumqty', TRUE);
			$remark3		= $this->input->post('remark3', TRUE);

			if($jenismarket == '14'){
				$toollainnya		= $this->input->post('toollainnya', TRUE);
			}else{
				$toollainnya		= '';
			}
			$user 			= $this->session->userdata('user_id');
			$this->db->trans_begin();
			$this->load->model('mkttool/mmaster');
			$imkt =$this->mmaster->runningnumber();
			$query = $this->db->query(" select * from tm_marketing_tool_tmp where i_user='$user'",false);
			if($query->num_rows()>0){ 
				$row=$query->row();
           		$isalesman = $row->i_salesman;
				$icustomer = $row->i_customer;
				$iarea 	   = $row->i_area;
				$ecustomer = $row->e_customer_name;
				$jenis 	   = $row->i_mkt_type;
			}		
			$this->mmaster->deletetmp($user);
			$this->mmaster->save($iloyal,$eremark1,$vestimasi,$isalesman,$icustomer,$iarea,$ecustomer,$jenis,$imkt,$jenispromo2,$diskon,$tandabukti,$remark2,$remark3,$jenismarket,$panjang,$lebar,$tinggi,$ukuranlain,$jumqty,$toollainnya,$vtarget);

			$fileName1 	= $imkt . '-1';
			$fileName2 	= $imkt . '-2';
			$fileName3 	= $imkt . '-3';
			$field		='userfile1';
			$field2		='userfile2';
			$field3		='userfile3';

			/*upload file 1*/
			$config['upload_path'] = 'mkttool/';
			$config['allowed_types'] = '*';
			$config['overwrite']	= TRUE;
			$config['file_name']	= $fileName1;
			$config['max_size']	= '0';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload($field)){
				$error = array('error' => $this->upload->display_errors());
			}else{
				$data = array('upload_data' => $this->upload->data());
				$ext4 = $this->upload->data('file_ext');
				$this->mmaster->updateatach1($imkt,$fileName1.$ext4);
				@chmod('mkttool/'.$fileName1.$ext4, 0777);
			}
			/*upload file 2 */

			$config['upload_path'] = 'mkttool/';
			$config['allowed_types'] = '*';
			$config['overwrite']	= TRUE;
			$config['file_name']	= $fileName2;
			$config['max_size']	= '0';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload($field2)){
				$error = array('error' => $this->upload->display_errors());
			}else{
				$data = array('upload_data' => $this->upload->data());
				$ext4 = $this->upload->data('file_ext');
				$this->mmaster->updateatach2($imkt,$fileName2.$ext4);
				@chmod('mkttool/'.$fileName2.$ext4, 0777);
			}

			/*upload file 3 */
			$config['upload_path'] = 'mkttool/';
			$config['allowed_types'] = '*';
			$config['overwrite']	= TRUE;
			$config['file_name']	= $fileName3;
			$config['max_size']	= '0';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload($field3)){
				$error = array('error' => $this->upload->display_errors());
			}else{
				
				$data = array('upload_data' => $this->upload->data());
				$ext4 = $this->upload->data('file_ext');
				$this->mmaster->updateatach3($imkt,$fileName3.$ext4);
				@chmod('mkttool/'.$fileName3.$ext4, 0777);
			}
			/* end */

			if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                 $this->db->trans_commit();
				 /* $this->db->trans_rollback(); */
                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Simpan Marketing Tools:'.$imkt;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );

                 $data['sukses'] = true;
                 $data['inomor'] = $imkt;
                 $this->load->view('nomor',$data);
              }
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*BASIC*/
			$i_mkt		= $this->input->post('i_mkt', TRUE);
			$dmkt		= $this->input->post('dmkt', TRUE);
			$isalesman	= $this->input->post('isalesman', TRUE);
			$icustomer	= $this->input->post('icustomer', TRUE);
			$jenis		= $this->input->post('imkttype', TRUE);
			$d_entry	= $this->input->post('d_entry', TRUE);
			$iarea		= substr($icustomer,0,2);
			/*view1*/
			$iloyal 	= $this->input->post('iloyal', TRUE);
			$eremark1 	= $this->input->post('eremarkpengajuan', TRUE);
			$vestimasi 	= $this->input->post('vestimasi', TRUE);
			$vestimasi        = str_replace(',', '', $vestimasi);
			$vtarget 	= $this->input->post('target', TRUE);
			$vtarget        = str_replace(',', '', $vtarget);
			/* var_dump($vtarget);
			die(); */

			/*view2*/
			$jenispromo2	= $this->input->post('jenispromo2', TRUE);
			$itype	= $this->input->post('itype', TRUE);
			
			$diskon			= $this->input->post('diskon', TRUE);
			/* $vtarget		= $this->input->post('target', TRUE); */
			$tandabukti		= $this->input->post('tandabukti', TRUE);
			$remark2		= $this->input->post('remark2', TRUE);


			/*view3*/
			$jenismarket	= $this->input->post('jenismarket', TRUE);
			$panjang		= $this->input->post('panjang', TRUE);
			$lebar			= $this->input->post('lebar', TRUE);
			$tinggi			= $this->input->post('tinggi', TRUE);
			$ukuranlain		= $this->input->post('ukuranlain', TRUE);
			$jumqty			= $this->input->post('jumqty', TRUE);
			$remark3		= $this->input->post('remark3', TRUE);

			/* All View */
			$realisai        = $this->input->post('realisai', TRUE);
			$realisai        = str_replace(',', '', $realisai);

			if($realisai == ''){
				$realisai = 0;
			}

			/* view 2 */
			$ipromo			= $this->input->post('ipromo', TRUE);
			/*Tambahan */
			$iapprove		= $this->input->post('iapprove', TRUE);
			$dapprove		= $this->input->post('dapprove', TRUE);
			$ilevel			= $this->input->post('ilevel', TRUE);
			$fmktapp			= $this->input->post('fmktapp', TRUE);
			
			if($jenismarket == '14'){
				$toollainnya		= $this->input->post('toollainnya', TRUE);
			}else{
				$toollainnya		= '';
			}
			$this->db->trans_begin();
			$this->load->model('mkttool/mmaster');
			$query = $this->db->query("SELECT * FROM tm_marketing_tool where i_mkt = '$i_mkt'");
			if($query->num_rows()>0){ 
			   $row=$query->row();
				  $attach1 	= $row->e_attch1;
				  $attach2 	= $row->e_attch2;
				  $attach3 	= $row->e_attch3;
				  $attach4 	= $row->e_attch4;

				  $appr1 	= $row->i_approve;
				  $appr2 	= $row->i_approve2;
				  $appr3 	= $row->i_approve3;
				  $appr4 	= $row->i_approve4;
				  $appr5 	= $row->i_approve5;
				  $dappr1 	= $row->d_approve;
				  $dappr2 	= $row->d_approve2;
				  $dappr3 	= $row->d_approve3;
				  $dappr4 	= $row->d_approve4;
				  $dappr5 	= $row->d_approve5;

			}
			$this->mmaster->deletemkt($i_mkt);
			$this->mmaster->insert($i_mkt,$dmkt,$isalesman,$icustomer,$jenis,$d_entry,$iarea,$iloyal,$eremark1,$vestimasi,$jenispromo2,$diskon,$tandabukti,$remark2,$jenismarket,$panjang,$lebar,$tinggi,$ukuranlain,$jumqty,$remark3,$realisai,$ipromo,$iapprove,$dapprove,$ilevel,$fmktapp,$toollainnya,$attach1,$attach2,$attach3,$attach4,$vtarget,$itype,$appr1,$appr2,$appr3,$appr4,$appr5,$dappr1,$dappr2,$dappr3,$dappr4,$dappr5);
			
			$fileName1 	= $i_mkt . '-1';
			$fileName2 	= $i_mkt . '-2';
			$fileName3 	= $i_mkt . '-3';
			$fileName4 	= $i_mkt . '-4';
			$field		='userfile1';
			$field2		='userfile2';
			$field3		='userfile3';
			$field4		='userfile4';

			/* if($jenis == '1' || $jenis == '2' || $jenis == '3'){ */
			/*upload file 1*/
			$directory = 'mkttool/';
			if($_FILES[$field]['name'] != ''){
				$alreadyexist = preg_grep('~^'.$i_mkt.'-1'.'.*~', scandir($directory));
				foreach($alreadyexist as $xx){
					if(file_exists($directory.$xx)){
						unlink($directory.$xx);
					}
				}
			}
			
			$config['upload_path'] 		= 'mkttool/';
			$config['allowed_types'] 	= '*';
			$config['overwrite']		= TRUE;
			$config['file_name']		= $fileName1;
			$config['max_size']			= '0';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload($field)){
				$error = array('error' => $this->upload->display_errors());
			}else{
				$data = array('upload_data' => $this->upload->data());
				$ext4 = $this->upload->data('file_ext');
				$this->mmaster->updateatach1($i_mkt,$fileName1.$ext4);
				@chmod('mkttool/'.$fileName1.$ext4, 0777);
			}
		/* } */
			/*upload file 2 */
			/* if($jenis == '1' || $jenis == '2'){ */
			$directory = 'mkttool/';
			if(isset($_FILES[$field2]) && $_FILES[$field2]['name'] != ''){
				$alreadyexist = preg_grep('~^'.$i_mkt.'-2'.'.*~', scandir($directory));
				foreach($alreadyexist as $xx){
					if(file_exists($directory.$xx)){
						unlink($directory.$xx);
					}
				}
			}

			$config['upload_path'] = 'mkttool/';
			$config['allowed_types'] = '*';
			$config['overwrite']	= TRUE;
			$config['file_name']	= $fileName2;
			$config['max_size']	= '0';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload($field2)){
				$error = array('error' => $this->upload->display_errors());
			}else{
				$data = array('upload_data' => $this->upload->data());
				$ext4 = $this->upload->data('file_ext');
				$this->mmaster->updateatach2($i_mkt,$fileName2.$ext4);
				@chmod('mkttool/'.$fileName2.$ext4, 0777);
				
				var_dump($fileName2.$ext4);
				/* die(); */
			}
		/* }
		if($jenis == '3'){ */
			/*upload file 3 */

			$directory = 'mkttool/';
			if(isset($_FILES[$field3]) && $_FILES[$field3]['name'] != ''){
				$alreadyexist = preg_grep('~^'.$i_mkt.'-3'.'.*~', scandir($directory));
				foreach($alreadyexist as $xx){
					if(file_exists($directory.$xx)){
						unlink($directory.$xx);
					}
				}
			}

			$config['upload_path'] 		= 'mkttool/';
			$config['allowed_types'] 	= '*';
			$config['overwrite']		= TRUE;
			$config['file_name']		= $fileName3;
			$config['max_size']			= '0';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload($field3)){
				$error = array('error' => $this->upload->display_errors());
			}else{
				$alreadyexist = preg_grep('~^'.$i_mkt.'-3'.'.*~', scandir($directory));
				$data = array('upload_data' => $this->upload->data());
				$ext4 = $this->upload->data('file_ext');
				$this->mmaster->updateatach3($i_mkt,$fileName3.$ext4);
				@chmod('mkttool/'.$fileName3.$ext4, 0777);
			}

			/*upload file 4 */

			$directory = 'mkttool/';
			if(isset($_FILES[$field4]) && $_FILES[$field4]['name'] != ''){
				$alreadyexist = preg_grep('~^'.$i_mkt.'-4'.'.*~', scandir($directory));
				foreach($alreadyexist as $xx){
					if(file_exists($directory.$xx)){
						unlink($directory.$xx);
					}
				}
			}

			$config['upload_path'] 		= 'mkttool/';
			$config['allowed_types'] 	= '*';
			$config['overwrite']		= TRUE;
			$config['file_name']		= $fileName4;
			$config['max_size']			= '0';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if ( ! $this->upload->do_upload($field4)){
				$error = array('error' => $this->upload->display_errors());
			}else{
				$alreadyexist = preg_grep('~^'.$i_mkt.'-4'.'.*~', scandir($directory));
				$data = array('upload_data' => $this->upload->data());
				$ext4 = $this->upload->data('file_ext');
				$this->mmaster->updateatach4($i_mkt,$fileName4.$ext4);
				@chmod('mkttool/'.$fileName4.$ext4, 0777);
			}
		/* } */
			/* end */

			if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                 $this->db->trans_commit();
                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Simpan Marketing Tools:'.$i_mkt;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );

                 $data['sukses'] = true;
                 $data['inomor'] = $i_mkt;
                 $this->load->view('nomor',$data);
              }
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function rollback(){
		$note		= $this->input->post('note', TRUE);	
		$i_mkt		= $this->input->post('imkt', TRUE);
		$user		= $this->input->post('userid', TRUE);	
		$dept		= $this->input->post('dept', TRUE);
		$level		= $this->input->post('level', TRUE);
		$jabatan	= $this->input->post('jabatan', TRUE);
		$roll 		= 't';

		/* approveas($i_mkt,$user,$level,$jenis,$dept,$jabatan) */
		$this->mmaster->rollbackappr($i_mkt,$user,$dept,$level,$jabatan);
		if($note !='' || $note != NULL){
			$query = $this->db->query("SELECT * FROM tm_marketing_tool_note where i_mkt = '$i_mkt' and i_dept = '$dept' and i_user = '$user'");
			if($query->num_rows()>0){ 	
				// $this->mmaster->deletenote($i_mkt,$dept,$user);
				$this->mmaster->insertnote($i_mkt,$dept,$user,$note,$roll,$level);
			}else{
				$this->mmaster->insertnote($i_mkt,$dept,$user,$note,$roll,$level);
			}
		}


		$data['sukses'] = true;
		$data['inomor'] = $i_mkt;
		$this->load->view('nomor',$data);
	}

	function reject(){
		$note		= $this->input->post('note', TRUE);	
		$i_mkt		= $this->input->post('imkt', TRUE);
		$user		= $this->input->post('userid', TRUE);	
		$dept		= $this->input->post('dept', TRUE);
		$level		= $this->input->post('level', TRUE);
		$jabatan	= $this->input->post('jabatan', TRUE);
		$roll 		= 't';
		$this->load->model('mkttool/mmaster');
		$this->mmaster->rejeckinaja($i_mkt,$note);
		$sess=$this->session->userdata('session_id');
		$id=$this->session->userdata('user_id');
		$sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
		$rs     = pg_query($sql);
		if(pg_num_rows($rs)>0){
		   while($row=pg_fetch_assoc($rs)){
			  $ip_address     = $row['ip_address'];
			  break;
		   }
		}else{
		   $ip_address='kosong';
		}
		$query  = pg_query("SELECT current_timestamp as c");
		while($row=pg_fetch_assoc($query)){
		   $now   = $row['c'];
		}
		$pesan='Reject Marketing Tools:'.$i_mkt.'Approve dari User :'.$user.'departement :'.$dept;
		$this->load->model('logger');
		$this->logger->write($id, $ip_address, $now , $pesan );
		$data['sukses'] = true;
		$data['inomor'] = $i_mkt;
		$this->load->view('nomor',$data);
	}

	function approveas()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$i_mkt		= $this->input->post('imkt', TRUE);	
			$jenis		= $this->input->post('jenis', TRUE);	
			$user		= $this->session->userdata('user_id');
			$level		= $this->session->userdata('level');
			$dept		= $this->session->userdata('departement');
			$jabatan	= $this->input->post('jabatan', TRUE);	
			$note		= $this->input->post('note1', TRUE);
			$roll 		= 'f';	

			$this->load->model('mkttool/mmaster');
			/* if( $note1 != '' ||  $note2 != '' ||  $note3 != '' ||  $note4 != '' ||  $note5 != '' || $note1 != NULL || $note2 != NULL || $note3 != NULL || $note4 != NULL || $note5 != NULL ){ */			
			if($note !='' || $note != NULL){
				$query = $this->db->query("SELECT * FROM tm_marketing_tool_note where i_mkt = '$i_mkt' and i_dept = '$dept' and i_user = '$user'");

				if($query->num_rows()>0){ 	
					// $this->mmaster->deletenote($i_mkt,$dept,$user);
					$this->mmaster->insertnote($i_mkt,$dept,$user,$note,$roll,$level);
				}else{
					$this->mmaster->insertnote($i_mkt,$dept,$user,$note,$roll,$level);
				}
			}
			$this->mmaster->approveas($i_mkt,$user,$level,$jenis,$dept,$jabatan);
			
			if ($this->db->trans_status() === FALSE)
              {
                 $this->db->trans_rollback();
              }else{
                 $this->db->trans_commit();
                $sess=$this->session->userdata('session_id');
                $id=$this->session->userdata('user_id');
                $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
                $rs     = pg_query($sql);
                if(pg_num_rows($rs)>0){
                   while($row=pg_fetch_assoc($rs)){
                      $ip_address     = $row['ip_address'];
                      break;
                   }
                }else{
                   $ip_address='kosong';
                }
                $query  = pg_query("SELECT current_timestamp as c");
                while($row=pg_fetch_assoc($query)){
                   $now   = $row['c'];
                }
                $pesan='Approve Marketing Tools:'.$i_mkt.'Approve dari User :'.$user.'departement :'.$dept;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now , $pesan );

                 $data['sukses'] = true;
                 $data['inomor'] = $i_mkt;
                 $this->load->view('nomor',$data);
              }
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('mkttool/mmaster');
			$tahun = date('Y');
			$config['base_url'] = base_url().'index.php/mkttool/cform/view/';
			$query = $this->db->query("SELECT
											a.*,
											b.e_loyal,
											c.nama,
											d.e_namamkt,
											e.e_customer_name,
											f.e_salesman_name,
											CASE
												WHEN a.i_mkt_type = 1 THEN 'Loyalitas'
												WHEN a.i_mkt_type = 2 THEN 'Promo'
												WHEN a.i_mkt_type = 3 THEN 'Marketing Tools'
											END AS marketing_type,
											CASE
												WHEN a.i_promo_type2 = '1' THEN 'SELL IN'
												WHEN a.i_promo_type2 = '2' THEN 'SELL OUT'
												WHEN a.i_promo_type2 = '3' THEN 'LISTING FEE'
											END AS promotype2
										FROM
											tm_marketing_tool a
										LEFT JOIN tr_mkttools_loyal b ON
											(a.i_loyalitas_type = b.i_loyal)
										LEFT JOIN tr_mkttools_promo c ON
											(trim(a.e_tanda_bukti) = trim(c.nama))
										LEFT JOIN tr_jns_mkttools d ON
											(a.e_jenis_market = d.i_jnsmkt)
										INNER JOIN tr_customer e ON
											(a.i_customer = e.i_customer)
										INNER JOIN tr_salesman f ON
											(a.i_salesman = f.i_salesman)
										WHERE 
											to_char(a.d_mkt,'yyyy') = '$tahun'
										ORDER BY
											a.i_mkt_type",false);
													
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['thn']  = date('Y');
			$data['page_title'] = $this->lang->line('promomarketing');
			$data['iproduct']='';
			$data['iproductgrade']='';
			$this->load->model('mkttool/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasadaya($cari,$config['per_page'],$this->uri->segment(4),$tahun);
			$this->load->view('mkttool/vformview', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariview()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('mkttool/mmaster');
			
			$tahun		= $this->input->post('tahun', TRUE);	
			$cari 		= $this->input->post('cari', FALSE);
			if ($tahun == '') $tahun = $this->uri->segment(4);
			if ($cari == '') $cari = $this->uri->segment(5);
			$cari 		= strtoupper($cari);

			$config['base_url'] = base_url().'index.php/mkttool/cform/cariview/'.$tahun.'/'.$cari;
			$query = $this->db->query("SELECT
											a.*,
											b.e_loyal,
											c.nama,
											d.e_namamkt,
											e.e_customer_name,
											f.e_salesman_name,
											CASE
												WHEN a.i_mkt_type = 1 THEN 'Loyalitas'
												WHEN a.i_mkt_type = 2 THEN 'Promo'
												WHEN a.i_mkt_type = 3 THEN 'Marketing Tools'
											END AS marketing_type,
											CASE
												WHEN a.i_promo_type2 = '1' THEN 'SELL IN'
												WHEN a.i_promo_type2 = '2' THEN 'SELL OUT'
												WHEN a.i_promo_type2 = '3' THEN 'LISTING FEE'
											END AS promotype2
										FROM
											tm_marketing_tool a
										LEFT JOIN tr_mkttools_loyal b ON
											(a.i_loyalitas_type = b.i_loyal)
										LEFT JOIN tr_mkttools_promo c ON
											(trim(a.e_tanda_bukti) = trim(c.nama))
										LEFT JOIN tr_jns_mkttools d ON
											(a.e_jenis_market = d.i_jnsmkt)
										INNER JOIN tr_customer e ON
											(a.i_customer = e.i_customer)
										INNER JOIN tr_salesman f ON
											(a.i_salesman = f.i_salesman)
										WHERE
											to_char(a.d_mkt,'yyyy') = '$tahun'
											and (upper(a.i_mkt) like '%$cari%'	
											or upper(e.i_customer) like '%$cari%'	
											or upper(e.e_customer_name) like '%$cari%'
											or upper(a.i_salesman) like '%$cari%'
											or upper(f.e_salesman_name) like '%$cari%')	
										ORDER BY
											a.i_mkt_type",false);
													
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['thn']  = $tahun;
			$data['page_title'] = $this->lang->line('promomarketing');
			$data['iproduct']='';
			$data['iproductgrade']='';
			$this->load->model('mkttool/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasadayana($cari,$config['per_page'],$this->uri->segment(6), $tahun);
			$this->load->view('mkttool/vformview', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('mkttool/mmaster');
			$imkt 	= $this->uri->segment(4);
			$bl		= substr($imkt,6,2);

			

			/* if($model->isexist($imkt) == false){
				exit('Nomor '.$imkt.' Tidak Ada');
			} */
			$query = $this->db->query("SELECT * FROM tm_marketing_tool where i_mkt = '$imkt'");
			 if($query->num_rows()>0){ 
				$row=$query->row();
           		$status = $row->f_cancel;
			 }
			 if($status == false){
				exit('Nomor '.$imkt.' Tidak Ada');
			 }
			/* if($model->isCancel($imkt) == 't'){
				exit('Pengajuan '.$imkt.' Sudah Dibatalkan');
			} */

			require_once Thirdparty . 'tcpdf/tcpdf.php';

		$pdf = new \TCPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);

		$pdf->setTitle('Print Marketing Tool');
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->setDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$pdf->setMargins(7, 8, 8);
		$pdf->setAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$pdf->setFontSubsetting(true);
		$pdf->setFont('dejavusans', '', 7, '', true);
		$pdf->AddPage();

		$data = [
			'imkt'    => $imkt,
			'month'   => mbulan($bl),
			/* 'month'   => 'MARET', */
			'isi'     => $this->mmaster->bacaprint($imkt),
			'year'    => $this->mmaster->getyear($imkt),
			'netto'   => $this->mmaster->nettolast3y($imkt),
			'ytarget' => $this->mmaster->targetlast3y($imkt),
			'biayath' => $this->mmaster->biayath($imkt),
			'mtarget' => $this->mmaster->targetmlast3y($imkt)
		];

		/* $html  = viewdata(module('vformrptleft'), $data); */
		$html = $this->load->view('mkttool/vformrptleft', $data, true);
		$data2 = [
			'imkt'    => $imkt,
			'history' => $this->mmaster->history($imkt)
		];

		/* $html2 = viewdata(module('vformrptright'), $data2); */
		$html2 = $this->load->view('mkttool/vformrptright', $data2, true);
		/* $this->load->view */

		$pdf->writeHTMLCell(140 , '', '', '', $html  , false, 0, 0, true, '' , true);
		$pdf->writeHTMLCell(140 , '', '', '', $html2 , false, 0, 0, true, '' , true);

		$pdf->Output('Print Marketing Tool.pdf', 'I');
			/* $config['base_url'] = base_url().'index.php/mkttool/cform/index/';
			$this->load->model('printsjkhusus/mmaster');
			$data['isj']=$isj;
			$data['page_title'] = $this->lang->line('printsj');
			$data['isi']=$this->mmaster->baca($isj,$area);
			$data['detail'] = $this->mmaster->bacaanalisa($imkt);*/
			$sess=$this->session->userdata('session_id'); 
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$data['iarea']	= $area;
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak SJ Area:'.$area.' No:'.$isj;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			/*if($area=='00')
			{
				$this->load->view('printsjkhusus/vformrpt',$data);
			}else{
				if($get->periodesj > '202203'){
					$this->load->view('printsjkhusus/vformrptcab_newexc',$data);
				}else{
					$this->load->view('printsjkhusus/vformrptcab',$data);
				}*/
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('mkttool/mmaster');
			$imkt = $this->uri->segment(4);
			$this->mmaster->delete($imkt);
			$config['base_url'] = base_url().'index.php/mkttool/cform/index/'.$imkt.'/';

			$query = $this->db->query("SELECT
											a.*,
											b.e_loyal,
											c.nama,
											d.e_namamkt,
											e.e_customer_name,
											f.e_salesman_name,
											CASE
												WHEN a.i_mkt_type = 1 THEN 'Loyalitas'
												WHEN a.i_mkt_type = 2 THEN 'Promo'
												WHEN a.i_mkt_type = 3 THEN 'Marketing Tools'
											END AS marketing_type,
											CASE
												WHEN a.i_promo_type2 = '1' THEN 'SELL IN'
												WHEN a.i_promo_type2 = '2' THEN 'SELL OUT'
												WHEN a.i_promo_type2 = '3' THEN 'LISTING FEE'
											END AS promotype2
										FROM
											tm_marketing_tool a
										LEFT JOIN tr_mkttools_loyal b ON
											(a.i_loyalitas_type = b.i_loyal)
										LEFT JOIN tr_mkttools_promo c ON
											(trim(a.e_tanda_bukti) = trim(c.nama))
										LEFT JOIN tr_jns_mkttools d ON
											(a.e_jenis_market = d.i_jnsmkt)
										INNER JOIN tr_customer e ON
											(a.i_customer = e.i_customer)
										INNER JOIN tr_salesman f ON
											(a.i_salesman = f.i_salesman)
										/* WHERE
											a.f_cancel = 'f' */
										ORDER BY
											a.i_mkt_type",false);
			/* var_dump('yyy');
			die();	 */										
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '20';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_productprice_manual');
			$data['iproduct']='';
			$data['iproductgrade']='';
			$this->load->model('mkttool/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasadaya($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('mkttool/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function edit()
	{
	   if (
		  (($this->session->userdata('logged_in')) &&
		  (($this->session->userdata('menu47')=='t') || ($this->session->userdata('menu57')=='t'))) ||
		  (($this->session->userdata('logged_in')) &&
		  ($this->session->userdata('allmenu')=='t'))
		  ){
		  $data['page_title'] = $this->lang->line('promomarketingedit');
		  if($this->uri->segment(4)!='')
		  {
			 $imkt = $this->uri->segment(4);
			 $dept=$this->session->userdata('departement');
			 $level=$this->session->userdata('level');
			 $this->load->model('mkttool/mmaster');
			 $data['isi']   = $this->mmaster->bacaheader($imkt);
			  $this->load->view('mkttool/vformedit',$data);
		  }else{
			 $this->load->view('mkttool/vinsert_fail',$data);
		  }
	   }else{
		  $this->load->view('awal/index.php');
	   }
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/mkttool/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_price 
					                        where (upper(tr_product_price.i_product) like '%$cari%' 
					                        or upper(tr_product_price.e_product_name) = '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '20';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_productprice_manual');
			$data['iproduct']='';
			$data['iproductgrade']='';
	 		$this->load->view('mkttool/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function salesman()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/mkttool/cform/salesman/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_salesman, e_salesman_name from tr_salesman
										where f_salesman_aktif = 't'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_salesman');
			$data['isi']=$this->mmaster->bacasalesman($config['per_page'],$this->uri->segment(5));
			$this->load->view('mkttool/vlistsalesman', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function loyal()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/mkttool/cform/loyal/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_loyal, e_loyal from tr_mkttools_loyal
										where upper(i_loyal) like '%$cari%' or upper(e_loyal) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_loyal');
			$data['isi']=$this->mmaster->bacaloyal($config['per_page'],$this->uri->segment(5));

			// @json_decode(file_get_contents(VIEWPATH . module('source/jenisloyalitas.json')))
			$this->load->view('mkttool/vlistloyal', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function mkttool()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/mkttool/cform/mkttool/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_jnsmkt, e_namamkt from tr_jns_mkttools
										where upper(i_jnsmkt) like '%$cari%' or upper(e_namamkt) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_mkttool');
			$data['isi']=$this->mmaster->bacamkttools($config['per_page'],$this->uri->segment(5));

			// @json_decode(file_get_contents(VIEWPATH . module('source/jenisloyalitas.json')))
			$this->load->view('mkttool/vlistmkttool', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function promo()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$icustomer  = $this->uri->segment(4);
			$tahun  	= $this->uri->segment(5);
			$diskon  	= $this->uri->segment(6);
			$this->load->model('mkttool/mmaster');
			$config['base_url'] = base_url() . 'index.php/mkttool/cform/promo/index/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" SELECT	
												a.i_promo,
												a.e_promo_name,
												COALESCE(sum(c.v_nota_netto),0) AS total
											FROM
												tm_promo a
											INNER JOIN tm_spb b ON
												(a.i_promo = b.i_spb_program)
											INNER JOIN tm_nota c ON
												(b.i_sj = c.i_sj
													AND b.i_area = c.i_area AND c.i_customer = '$icustomer' AND c.f_nota_cancel = 'f')
											where to_char(a.d_promo,'yyyy') = '$tahun'
											GROUP BY 1,2 order by i_promo", false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(7);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('list_promo');
			$data['icustomer']	= $icustomer;
			$data['diskon']		= $diskon;
			$data['isi'] 		= $this->mmaster->bacapromo($config['per_page'], $this->uri->segment(7),$icustomer,$tahun);
			$this->load->view('mkttool/vlistpromo', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}


	function mktpromo()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/mkttool/cform/mktpromo/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select * from tr_mkttools_promo
										where upper(nama) like '%$cari%' or upper(keterangan) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_mktpromo');
			$data['isi']=$this->mmaster->bacamktpromo($config['per_page'],$this->uri->segment(5));

			// @json_decode(file_get_contents(VIEWPATH . module('source/jenisloyalitas.json')))
			$this->load->view('mkttool/vlistmktpromo', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}



	function carisalesman()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/mkttool/cform/carisalesman/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$isalesman 	= $this->uri->segment(4);
			$periode 	= date('Ym');
			$query = $this->db->query("select i_salesman, e_salesman_name from tr_salesman
										where upper(i_salesman) like '%$cari%' or upper(e_salesman_name) like '%$cari%' and f_salesman_aktif = 't'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_salesman');
			$data['isi']=$this->mmaster->carisalesman($cari,$config['per_page'],$this->uri->segment(5),$isalesman,$periode);
			$this->load->view('mkttool/vlistsalesman', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}



	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris=$this->uri->segment(4);
			$cari =$this->uri->segment(5);
			$salesman =$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/mkttool/cform/customer/'.$baris.'/'.$cari.'/'.$salesman.'/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			// $periode=date('Ym');
			/* $periode 	= date('Ym'); */
			$periode	= '202301';
			$query = $this->db->query("SELECT
											DISTINCT a.i_customer,
											a.e_customer_name,
											a.i_area
										FROM
											tr_customer a
										INNER JOIN tr_customer_salesman b ON
											(a.i_customer = b.i_customer)
										WHERE
											b.e_periode = '$periode' AND i_salesman = '$salesman'
										",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($cari,$config['per_page'],$this->uri->segment(7),$periode,$salesman);
			$data['isalesman']=$salesman;
			$this->load->view('mkttool/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$periode 	= date('Ym');
			$salesman=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/mkttool/cform/customer/'.$salesman.'/'.'index/';
			$query = $this->db->query("SELECT
											DISTINCT a.i_customer,
											a.e_customer_name,
											a.i_area
										FROM
											tr_customer a
										INNER JOIN tr_customer_salesman b ON
											(a.i_customer = b.i_customer)
										WHERE
											b.e_periode = '$periode' AND i_salesman = '$salesman'
										AND upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(5),$salesman,$periode);
			$data['isalesman']=$salesman;
			$this->load->view('mkttool/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}


	function caripromo()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu408')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$periode 	= date('Ym');
			$icustomer=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/mkttool/cform/customer/'.$icustomer.'/'.'index/';
			$query = $this->db->query("SELECT	
											a.i_promo,
											a.e_promo_name,
											COALESCE(sum(c.v_nota_netto),0) AS total
										FROM
											tm_promo a
										INNER JOIN tm_spb b ON
											(a.i_promo = b.i_spb_program)
										INNER JOIN tm_nota c ON
											(b.i_sj = c.i_sj
												AND b.i_area = c.i_area AND c.i_customer = '$icustomer' AND c.f_nota_cancel = 'f')
										WHERE 
											upper(a.i_promo) like '%$cari%' 
											or upper(a.e_promo_name) like '%$cari%'		
										GROUP BY 1,2 order by i_promo",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('mkttool/mmaster');
			$data['page_title'] = $this->lang->line('list_promo');
			$data['isi']=$this->mmaster->caripromo($cari,$config['per_page'],$this->uri->segment(5),$icustomer);
			$data['icustomer']=$icustomer;
			$this->load->view('mkttool/vlistpromo', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}


	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu57') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$tahun		= $this->uri->segment(4);
			$objPHPExcel = new PHPExcel();
			$query = $this->mmaster->bacaexport($tahun);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
				$objPHPExcel->getProperties()->setCreator("M.I.S Dept 2023");
				$objPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept 2023");

				$objPHPExcel->getProperties()
					->setTitle("List Mkt Tools")
					->setSubject("Laporan Mkt Tools")
					->setDescription("Laporan Mkt Tools")
					->setKeywords("Laporan Mkt Tools")
					->setCategory("Laporan");

				$objPHPExcel->setActiveSheetIndex(0);
				$objPHPExcel->createSheet();
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 12
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2'
				);

				$header = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				);

				$stylehead = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER
					)
				);

				$stylehead1 = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic' => false,
						'size'  => 10
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					)
				);

				$stylerow = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic' => false,
						'size'  => 9
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					)
				);

				$stylerow1 = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic' => false,
						'size'  => 9
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER
					)
				);

				$stylerow2 = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic' => false,
						'size'  => 9
					),
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'vertical'  => Style_Alignment::VERTICAL_CENTER
					)
				);

				foreach (range('A', 'W') as $columnID) {
					$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
				}

				$objPHPExcel->getActiveSheet()->mergeCells('A2:W2');
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'DAFTAR Marketing Tools');
				$objPHPExcel->getActiveSheet()->mergeCells('A3:W3');
				$objPHPExcel->getActiveSheet()->duplicateStyleArray($header, 'A3');
				$objPHPExcel->getActiveSheet()->setCellValue('A3', $tahun);
				$objPHPExcel->getActiveSheet()->mergeCells('A4:W4');
				$objPHPExcel->getActiveSheet()->duplicateStyleArray($header, 'A4');
				$objPHPExcel->getActiveSheet()->setCellValue('A4', NmPerusahaan);

				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No Entry');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Status');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Tanggal');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Salesman');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Nama Toko');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Jenis Pengajuan');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Estimasi');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Realisasi');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Appr AS');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Tgl Appr AS');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Appr MKT');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('L6', 'Tgl Appr MKT');
				$objPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('M6', 'Appr RSM');
				$objPHPExcel->getActiveSheet()->getStyle('M6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('N6', 'Tgl Appr RSM');
				$objPHPExcel->getActiveSheet()->getStyle('N6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('O6', 'Appr GM');
				$objPHPExcel->getActiveSheet()->getStyle('O6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('P6', 'Tgl Appr GM');
				$objPHPExcel->getActiveSheet()->getStyle('P6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('Q6', 'Appr Direksi');
				$objPHPExcel->getActiveSheet()->getStyle('Q6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('R6', 'Tgl Appr Direksi');
				$objPHPExcel->getActiveSheet()->getStyle('R6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('S6', 'Entry to AS');
				$objPHPExcel->getActiveSheet()->getStyle('S6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('T6', 'AS to MKT');
				$objPHPExcel->getActiveSheet()->getStyle('T6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('U6', 'MKT to RSM');
				$objPHPExcel->getActiveSheet()->getStyle('U6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('V6', 'RSM to GM');
				$objPHPExcel->getActiveSheet()->getStyle('V6')->applyFromArray($stylehead);
				$objPHPExcel->getActiveSheet()->setCellValue('W6', 'GM to Direksi');
				$objPHPExcel->getActiveSheet()->getStyle('W6')->applyFromArray($stylehead);

				$objPHPExcel->getActiveSheet()->getStyle('A6:W6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ACEAA4');

				$j			= 7;
				$nomor		= 1;
				$jml 		= 1;
				foreach ($query->result() as $row) {
					
					$dmkt 	= $row->d_mkt == "" ? "" : date('d-m-Y', strtotime($row->d_mkt));
					$dapp1 	= $row->d_approve == "" ? "" : date('d-m-Y', strtotime($row->d_approve));
					$dapp2 	= $row->d_approve2 == "" ? "" : date('d-m-Y', strtotime($row->d_approve2));
					$dapp3 	= $row->d_approve3 == "" ? "" : date('d-m-Y', strtotime($row->d_approve3));
					$dapp4 	= $row->d_approve4 == "" ? "" : date('d-m-Y', strtotime($row->d_approve4));
					$dapp5 	= $row->d_approve5 == "" ? "" : date('d-m-Y', strtotime($row->d_approve5));

					/* ******************************** */

					$objPHPExcel->getActiveSheet()->setCellValue('A' . $j, $row->i_mkt);
					$objPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $j, $row->e_status);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $j, date('d-m-Y', strtotime($row->d_mkt)));
					$objPHPExcel->getActiveSheet()->getStyle('C' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $j, $row->i_salesman . " - " .  $row->e_salesman_name , Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('D' . $j)->applyFromArray($stylerow1);					
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $j, $row->i_customer . " - " .  $row->e_customer_name , Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->getStyle('E' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('F' . $j, $row->marketing_type);
					$objPHPExcel->getActiveSheet()->getStyle('F' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('G' . $j, $row->v_biaya_eta);
					$objPHPExcel->getActiveSheet()->getStyle('G' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->getStyle('G' . $j)->applyFromArray($stylerow2);
					$objPHPExcel->getActiveSheet()->setCellValue('H' . $j, $row->v_biaya_realisasi);
					$objPHPExcel->getActiveSheet()->getStyle('H' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
					$objPHPExcel->getActiveSheet()->getStyle('H' . $j)->applyFromArray($stylerow2);
					$objPHPExcel->getActiveSheet()->setCellValue('I' . $j, $row->i_approve);
					$objPHPExcel->getActiveSheet()->getStyle('I' . $j)->applyFromArray($stylerow1);
					/* $objPHPExcel->getActiveSheet()->setCellValue('J' . $j, date('d-m-Y', strtotime($row->d_approve))); */
					$objPHPExcel->getActiveSheet()->setCellValue('J' . $j, $row->d_approve);
					$objPHPExcel->getActiveSheet()->getStyle('J' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('K' . $j, $row->i_approve2);
					$objPHPExcel->getActiveSheet()->getStyle('K' . $j)->applyFromArray($stylerow1);
					/* $objPHPExcel->getActiveSheet()->setCellValue('L' . $j, date('d-m-Y', strtotime($row->d_approve2))); */
					$objPHPExcel->getActiveSheet()->setCellValue('L' . $j, $row->d_approve2);
					$objPHPExcel->getActiveSheet()->getStyle('L' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('M' . $j, $row->i_approve3);
					$objPHPExcel->getActiveSheet()->getStyle('M' . $j)->applyFromArray($stylerow1);
					/* $objPHPExcel->getActiveSheet()->setCellValue('N' . $j, date('d-m-Y', strtotime($row->d_approve3))); */
					$objPHPExcel->getActiveSheet()->setCellValue('N' . $j, $row->d_approve3);
					$objPHPExcel->getActiveSheet()->getStyle('N' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('O' . $j, $row->i_approve4);
					$objPHPExcel->getActiveSheet()->getStyle('O' . $j)->applyFromArray($stylerow1);
					/* $objPHPExcel->getActiveSheet()->setCellValue('P' . $j, date('d-m-Y', strtotime($row->d_approve4))); */
					$objPHPExcel->getActiveSheet()->setCellValue('P' . $j, $row->d_approve4);
					$objPHPExcel->getActiveSheet()->getStyle('P' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('Q' . $j, $row->i_approve5);
					$objPHPExcel->getActiveSheet()->getStyle('Q' . $j)->applyFromArray($stylerow1);
					/* $objPHPExcel->getActiveSheet()->setCellValue('R' . $j, date('d-m-Y', strtotime($row->d_approve5))); */
					$objPHPExcel->getActiveSheet()->setCellValue('R' . $j, $row->d_approve5);
					$objPHPExcel->getActiveSheet()->getStyle('R' . $j)->applyFromArray($stylerow1);

					$objPHPExcel->getActiveSheet()->setCellValue('S' . $j, $row->entrytoas);
					$objPHPExcel->getActiveSheet()->getStyle('S' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('T' . $j, $row->astomkt);
					$objPHPExcel->getActiveSheet()->getStyle('T' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('U' . $j, $row->mkttorsm);
					$objPHPExcel->getActiveSheet()->getStyle('U' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('V' . $j, $row->rsmtogm);
					$objPHPExcel->getActiveSheet()->getStyle('V' . $j)->applyFromArray($stylerow1);
					$objPHPExcel->getActiveSheet()->setCellValue('W' . $j, $row->gmtodir);
					$objPHPExcel->getActiveSheet()->getStyle('W' . $j)->applyFromArray($stylerow1);

					$jml++;
					$j++;
					$nomor++;
				}
			}

			$this->logger->writenew('Export Marketing Tools Tahun ' . $tahun);

			$nama_file = 'MKTTOOLS Tahun ' . $tahun . '.xls';
			// Proses file excel    
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename=' . $nama_file . ''); // Set nama file excel nya    
			header('Cache-Control: max-age=0');

			$objPHPExcel = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objPHPExcel->save('php://output', 'w');

			#####
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
?>
