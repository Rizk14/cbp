<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu334') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('exp-det-opdofc');
			$data['iperiode']	= '';
			$data['isupplier']	  = '';
			$this->load->view('exp-det-opdofc/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu334') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-det-opdofc/cform/supplier/index/';
			$config['total_rows'] = $this->db->count_all('tr_supplier');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-det-opdofc/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi'] = $this->mmaster->bacasupplier($config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-det-opdofc/vlistsupplier', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu334') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-det-opdofc/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_supplier 
										where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-det-opdofc/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi'] = $this->mmaster->carisupplier($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-det-opdofc/vlistsupplier', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu334') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-det-opdofc/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			// $iperiode	= $this->input->post('iperiode');
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$isupplier = $this->input->post('isupplier');
			// 		if($iperiode==''){
			//     $iperiode=$this->uri->segment(4);
			//   }

			//   $a=substr($iperiode,0,4);
			//     $b=substr($iperiode,4,2);
			// 	  $peri=mbulan($b)." - ".$a;

			if ($isupplier == '') $isupplier = $this->uri->segment(5);
			$qsupname	= $this->mmaster->esupname($isupplier);
			if ($qsupname->num_rows() > 0) {
				$row_supname	= $qsupname->row();
				$sname   = $row_supname->e_supplier_name;
			} else {
				$sname   = '';
			}
			$this->db->select("a.i_op, a.i_area, a.i_reff, a.d_reff, a.e_op_remark, a.d_op, b.i_product, b.e_product_name, b.n_order, c.i_do, 
      		  			 case when c.n_deliver isnull then 0 else c.n_deliver end as n_delivery, 
                  		 case when c.n_deliver isnull then b.n_order else b.n_order-c.n_deliver end as pending from tm_opfc a 
                  		 left join tm_opfc_item b on (a.i_op=b.i_op)
                  		 inner join tm_dofc_item c on (a.i_op=c.i_op and b.i_product=c.i_product)
                  		 inner join tm_dofc d on (c.i_do=d.i_do)
						  where 
						  a.d_op >= to_date('$dfrom','dd-mm-yyyy') AND a.d_op <= to_date('$dto','dd-mm-yyyy')  
                  		 and a.i_supplier='$isupplier' and d.f_do_cancel='f' order by a.i_op, c.i_do", false);

			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan Detail OP vs DO Forecast ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(9);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(9);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan Detail OP vs DO Forecast');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 9, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Tanggal : ' . $dfrom . ' s/d : ' . $dto . ' - Supplier : ' . $sname);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 9, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:H5'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'NO OP');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'KODE BARANG');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'SPB');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'NAMA');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'AREA');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'TGLOP');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'ORDER');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'NO DO');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'DELIVER');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'PENDING');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

				$i = 6;
				$j = 6;
				$no = 0;
				foreach ($query->result() as $row) {
					$no++;
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':H' . $i
					);

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->i_op, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->i_product, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->i_reff, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->e_op_remark, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->i_area, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->d_op, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->n_order, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->i_do, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $row->n_delivery, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $row->pending, Cell_DataType::TYPE_NUMERIC);

					$i++;
					$j++;
				}
				$x = $i - 1;
				$objPHPExcel->getActiveSheet()->getStyle('G6:H' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama = 'LapDetOPvsDOFC-' . $isupplier . '-' . $dfrom . ' - ' . $dto . '.xls';
			$objWriter->save('beli/' . $nama);

			// $sess = $this->session->userdata('session_id');
			// $id = $this->session->userdata('user_id');
			// $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			// $rs		= pg_query($sql);
			// if (pg_num_rows($rs) > 0) {
			// 	while ($row = pg_fetch_assoc($rs)) {
			// 		$ip_address	  = $row['ip_address'];
			// 		break;
			// 	}
			// } else {
			// 	$ip_address = 'kosong';
			// }
			// $query 	= pg_query("SELECT current_timestamp as c");
			// while ($row = pg_fetch_assoc($query)) {
			// 	$now	  = $row['c'];
			// }
			// $pesan = 'Export Detail OP DO Forecast Tanggal :' . $dfrom . ' s/d ' . $dto . ' Supplier:' . $isupplier;
			// $this->load->model('logger');
			$this->logger->writenew('Export Detail OP DO Forecast Tanggal :' . $dfrom . ' s/d ' . $dto . ' Supplier:' . $isupplier);

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$data['folder']	= 'beli';

			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
