<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('approve-by');
			$data['dfrom']='';
			$data['dto']='';
      $data['iarea']='';
			$this->load->view('akt-approve1-by/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		  = strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		  = $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/akt-approve1-by/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.i_by, a.d_by, a.i_area, b.e_area_name, a.i_customer, c.e_customer_name, a.v_by, a.v_sisa,
		                              d.e_jenis_bayarname
		                              from tm_by a, tr_area b, tr_customer c, tr_jenis_bayar d
		                              where (upper(i_by) like '%$cari%') and a.i_area = b.i_area and a.i_jenis_by= d.i_jenis_bayar
		                              and a.i_customer = c.i_customer and a.i_area='$iarea' and a.f_by_cancel='f' and a.i_approve1 is null and
		                              a.i_notapprove is null and a.d_by >= to_date('$dfrom','dd-mm-yyyy') AND a.d_by <= to_date('$dto ','dd-mm-yyyy')
		                              order by a.d_by DESC",false);
			$config['total_rows']   = $query->num_rows(); 
			$config['per_page']     = '10';
			$config['first_link']   = 'Awal';
			$config['last_link']    = 'Akhir';
			$config['next_link']    = 'Selanjutnya';
			$config['prev_link']    = 'Sebelumnya';
			$config['cur_page']     = $this->uri->segment(8);
			$this->paginationxx->initialize($config);
			$data['page_title']     = $this->lang->line('approve-by');
			$this->load->model('akt-approve1-by/mmaster');
			$data['cari']		        = '';
			$data['dfrom']		      = $dfrom;
			$data['dto']		        = $dto;
			$data['status']         = 'view';
			$data['iarea']		      = $iarea;
			$data['iby']            ='';
			$data['isi']		        = $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Biaya '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('akt-approve1-by/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('approve-by');
			$this->load->view('akt-approve1-by/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/akt-approve1-by/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_kk
										              where (upper(i_kk) like '%$cari%') and f_close='f' 
										              and i_area='$iarea' and f_kk_cancel='f' and
										              d_kk >= to_date('$dfrom','dd-mm-yyyy') AND
										              d_kk <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('approve-by');
			$this->load->model('akt-approve1-by/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('akt-approve1-by/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/akt-approve1-by/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
  			$query = $this->db->query("select * from tr_area",false);
      }else{
  			$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
	                        			   or i_area = '$area4' or i_area = '$area5'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('akt-approve1-by/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-approve1-by/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu600')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/akt-approve1-by/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
      if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('akt-approve1-by/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('akt-approve1-by/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu328')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
  		){
			$data['page_title'] = $this->lang->line('edit-akt-by');
      $iarea        = $this->input->post('iarea');
			$iby	        = $this->input->post('iby');
      $dfrom        = $this->input->post('dfrom');
			$dto	        = $this->input->post('dto');
			if($iby=='') $iby=$this->uri->segment(4);
			if($iarea=='') $iarea=$this->uri->segment(5);
			if($dfrom=='') $dfrom=$this->uri->segment(6);
			if($dto=='') $dto=$this->uri->segment(7);
			$this->load->model('akt-approve1-by/mmaster');
			$data['iby']            = $iby;
      $data['iarea']          = $iarea;
			$data['dfrom']          = $dfrom;
      $data['dto']            = $dto;
			$data['isi']		        = $this->mmaster->bacaby($iby,$iarea);
      $data['status']         = 'update';
/*			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Informasi Plafond Periode:'.$iperiodeawal.' s/d '.$iperiodeakhir;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );*/
			$this->load->view('akt-approve1-by/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
  function update(){
    if (
       (($this->session->userdata('logged_in')) &&
       ($this->session->userdata('menu600')=='t')) ||
       (($this->session->userdata('logged_in')) &&
       ($this->session->userdata('allmenu')=='t'))
       ){
			    $status=$this->uri->segment(4);
			    $iby=$this->uri->segment(5);
			    $iarea	= $this->uri->segment(6);

          $this->load->model('akt-approve1-by/mmaster');
          $data['page_title'] = $this->lang->line('approve-by');
          $this->mmaster->updateby($iarea,$iby,$status);
          
            $sess=$this->session->userdata('session_id');
            $id=$this->session->userdata('user_id');
            $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs      = pg_query($sql);
              if(pg_num_rows($rs)>0){
                while($row=pg_fetch_assoc($rs)){
                  $ip_address   = $row['ip_address'];
                  break;
                }
              }else{
                $ip_address='kosong';
              }
            $query   = pg_query("SELECT current_timestamp as c");
              while($row=pg_fetch_assoc($query)){
              $now    = $row['c'];
            }
             $sess=$this->session->userdata('session_id');
             $id=$this->session->userdata('user_id');
             $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
             $rs      = pg_query($sql);
             if(pg_num_rows($rs)>0){
                while($row=pg_fetch_assoc($rs)){
                   $ip_address   = $row['ip_address'];
                   break;
                }
             }else{
                $ip_address='kosong';
             }
             $query   = pg_query("SELECT current_timestamp as c");
             while($row=pg_fetch_assoc($query)){
                $now    = $row['c'];
             }
             $pesan='Update Biaya Area '.$iarea.' No:'.$iby;
             $this->load->model('logger');
             $this->logger->write($id, $ip_address, $now , $pesan );
             $data['sukses']         = true;
             $data['inomor']         = $iby;
             $this->load->view('nomor',$data);
             /*
			        $data['status'] = 'view';
			        $data['iarea'] = $iarea;
			        $data['iperiodeawal'] = $iperiodeawal;
			        $data['iperiodeakhir'] = $iperiodeakhir;
			        $data['isi']		= $this->mmaster->bacaperiode($iperiodeawal,$iperiodeakhir,$iarea);
		          $this->load->view('listaccplafond/vmainform', $data);*/
          }else{
            $this->load->view('awal/index.php');
          }
        }
	    function notapprove()
	    {
		    if (
			    (($this->session->userdata('logged_in')) &&
			    ($this->session->userdata('menu600')=='t')) ||
			    (($this->session->userdata('logged_in')) &&
			    ($this->session->userdata('allmenu')=='t'))
		       ){
			    $iby 		= $this->input->post('iby', TRUE);
			    $iarea		= $this->input->post('iarea',TRUE);
			    $eapprove	= $this->input->post('enotapprove',TRUE);
			    if($eapprove=='')
				    $eapprove=null;
			    $user		=$this->session->userdata('user_id');
			    $this->db->trans_begin();
			    $this->load->model('akt-approve1-by/mmaster');
			    $this->mmaster->notapprove($iby, $iarea, $eapprove,$user);
			    if ($this->db->trans_status() === FALSE)
			    {
			        $this->db->trans_rollback();
			    }else{
				    $this->db->trans_commit();
				    $data['sukses']			= true;
				    $data['inomor']			= $iby;
				    $this->load->view('nomor',$data);
			    }
		    }else{
			    $this->load->view('awal/index.php');
		    }
	    }
	    function approve()
	    {
		    if (
			    (($this->session->userdata('logged_in')) &&
			    ($this->session->userdata('menu600')=='t')) ||
			    (($this->session->userdata('logged_in')) &&
			    ($this->session->userdata('allmenu')=='t'))
		       ){
			    $iby 		= $this->input->post('iby', TRUE);
			    $iarea		= $this->input->post('iarea',TRUE);
			    $eapprove	= $this->input->post('eapprove',TRUE);
			    if($eapprove=='')
				    $eapprove=null;
			    $user		=$this->session->userdata('user_id');
			    $this->db->trans_begin();
			    $this->load->model('akt-approve1-by/mmaster');
			    $this->mmaster->approve($iby, $iarea, $eapprove,$user);
			    if ($this->db->trans_status() === FALSE)
			    {
			        $this->db->trans_rollback();
			    }else{
				    $this->db->trans_commit();
				    $data['sukses']			= true;
				    $data['inomor']			= $iby;
				    $this->load->view('nomor',$data);
			    }
		    }else{
			    $this->load->view('awal/index.php');
		    }
	    }
}
?>
