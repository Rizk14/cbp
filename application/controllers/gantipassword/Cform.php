<?php
class Cform extends CI_Controller
{
	public $title	= "Ganti Password";
	public $menu	= "100C0013";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu68') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$iuser 			= $this->session->userdata('user_id');
			$eusername 		= $this->input->post('eusername', TRUE);
			$epasswordold	= $this->input->post('epasswordold', TRUE);
			$epasswordnew1	= $this->input->post('epasswordnew1', TRUE);
			$epasswordnew2	= $this->input->post('epasswordnew2', TRUE);

			if (
				(isset($iuser) && $iuser != '') &&
				(isset($eusername) && $eusername != '') &&
				(isset($epasswordold) && $epasswordold != '') &&
				(isset($epasswordnew1) && $epasswordnew1 != '') &&
				(isset($epasswordnew2) && $epasswordnew2 != '') &&
				($epasswordnew2 == $epasswordnew1)
			) {

				// $this->db->trans_begin();

				$this->load->model('gantipassword/mmaster');

				$iuser = $this->session->userdata('user_id');
				$pass  = $this->session->userdata('xxx');

				if (md5(md5($epasswordold)) == $pass) {
					$this->mmaster->update($iuser, $eusername, md5(md5($epasswordnew1)));
				}

				// if ($this->db->trans_status() === FALSE || $this->db->trans_status() === false) {
				// 	$this->db->trans_rollback();
				// } else {
				// 	$this->db->trans_commit();

				$this->logger->writenew("Ganti Password");
				// }
			} else {
				$data['page_title'] = $this->lang->line('gantipassword');
				$data['iuser'] 		= $this->session->userdata('user_id');

				$this->load->model('gantipassword/mmaster');

				$data['isi'] = $this->mmaster->baca($this->session->userdata('user_id'));

				$this->logger->writenew("Membuka Menu : " . $this->menu . " - " . $this->title);

				$this->load->view('gantipassword/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu68') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('gantipassword');
			$this->load->view('gantipassword/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function sukses()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu68') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('gantipassword');
			$this->load->view('gantipassword/vsuksesform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
