<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu198')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customersetor/cform/index/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query("	select * from tr_customer_setor ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('master_customersetor');
			$data['isetor']='';
			$this->load->model('customersetor/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Penyetor (Pelanggan)";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('customersetor/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu198')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 				= $this->input->post('iarea', TRUE);	
			$icustomer 			= $this->input->post('icustomer', TRUE);
			$ecustomersetor		= $this->input->post('ecustomersetor', TRUE);
			$ecustomerrekening	= $this->input->post('ecustomerrekening', TRUE);
			
			if ( (isset($ecustomersetor) && $ecustomersetor != ''))
			{
				$this->load->model('customersetor/mmaster');
				$this->mmaster->insert($iarea,$icustomer,$ecustomersetor,$ecustomerrekening);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Penyetor (Pelanggan):('.$icustomer.') -'.$ecustomersetor;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		        $config['base_url'] = base_url().'index.php/customersetor/cform/index/';
				/*$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);*/
				$query = $this->db->query("	select * from tr_customer_setor ",false);
				$config['total_rows'] = $query->num_rows();				
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->pagination->initialize($config);
				$data['page_title'] = $this->lang->line('master_customersetor');
				$data['isetor']='';
				$this->load->model('customersetor/mmaster');
				$cari = $this->input->post('cari', FALSE);
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('customersetor/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu198')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customersetor');
			$this->load->view('customersetor/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu198')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customersetor')." update";
			if($this->uri->segment(4)){
				$isetor = $this->uri->segment(4);
				$data['isetor'] = $isetor;
				$this->load->model('customersetor/mmaster');
				$data['isi']=$this->mmaster->baca($isetor);
					$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Penyetor (Pelanggan) ('.$isetor.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('customersetor/vmainform',$data);
			}else{
				$this->load->view('customersetor/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu198')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isetor			= $this->input->post('isetor', TRUE);
			$iarea 			= $this->input->post('iarea', TRUE);
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomersetor	= $this->input->post('ecustomersetor', TRUE);
			$ecustomerrekening	= $this->input->post('ecustomerrekening', TRUE);
			
			$this->load->model('customersetor/mmaster');
			$this->mmaster->update($isetor,$iarea,$icustomer,$ecustomersetor,$ecustomerrekening);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Penyetor (Pelanggan):('.$icustomer.') -'.$isetor;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

	        $config['base_url'] = base_url().'index.php/customersetor/cform/index/';
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/
			$query = $this->db->query("	select * from tr_customer_setor ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('master_customersetor');
			$data['isetor']='';
			$this->load->model('customersetor/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customersetor/vmainform', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu198')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$isetor	= $this->uri->segment(4);
			$this->load->model('customersetor/mmaster');
			$this->mmaster->delete($isetor);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Delete Master Status (Pelanggan) ('.$isetor.')';
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

			$config['base_url'] = base_url().'index.php/customersetor/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_customer_setor
						   	where upper(e_customer_setorname) like '%$cari%' or upper(i_customer) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('master_customersetor');
			$data['isetor']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customersetor/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu198')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$config['base_url'] = base_url().'index.php/customersetor/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$cari=($this->input->post("cari",false));
			if($cari=='')$cari=$this->uri->segment(4);
			if($cari=='zxvf'){
				$cari='';
				$config['base_url'] = base_url().'index.php/customersetor/cform/cari/zxvf/';
			}else{
				$config['base_url'] = base_url().'index.php/customersetor/cform/cari/'.$cari.'/';
			}

			$query = $this->db->query("	select * from tr_customer_setor
						   	where upper(e_customer_setorname) ilike '%$cari%' or upper(i_customer) ilike '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customersetor/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_customersetor');
			$data['isetor']='';
	 		$this->load->view('customersetor/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function pelanggan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu198')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){	
			$baris=$this->uri->segment(4);	
			
			$config['base_url'] = base_url().'index.php/customersetor/cform/pelanggan/'.$baris.'/sikasep/';
			$query = $this->db->query("select * from tr_customer order by e_customer_name asc",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('customersetor/mmaster');
			$data['baris']=$baris;
			$data['cari']='';
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacapelanggan($config['per_page'],$this->uri->segment(6));
			$this->load->view('customersetor/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caripelanggan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu198')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){			
			/*$cari = $this->input->post('cari', FALSE)?$this->input->post('cari', FALSE):$this->input->get_post('cari', FALSE);
			$keywordcari	= $cari==''?'null':$cari;
			$config['base_url'] = base_url().'index.php/customersetor/cform/caripelanggan/index/'.$keywordcari.'/';
			
			$cari=strtoupper($cari);*/

			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/customersetor/cform/caripelanggan/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/customersetor/cform/caripelanggan/'.$baris.'/sikasep/';

			$stquery = " select * from tr_customer
						where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') order by e_customer_name asc ";

			$query = $this->db->query($stquery,false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('customersetor/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['isi']=$this->mmaster->caripelanggan($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('customersetor/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}	
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu198')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris=$this->uri->segment(4);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/customersetor/cform/area/'.$baris.'/sikasep/';
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area
										   where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
			$data['cari']='';
			$this->load->model('customersetor/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(6));
			$this->load->view('customersetor/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu198')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$baris 	= $this->input->post('baris', FALSE);
			if($baris=='')$baris=$this->uri->segment(4);
			$cari 	= ($this->input->post('cari', FALSE));
			if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
			if($cari!='sikasep')
  			$config['base_url'] = base_url().'index.php/customersetor/cform/cariarea/'.$baris.'/'.$cari.'/';
  	  		else
  			$config['base_url'] = base_url().'index.php/customersetor/cform/cariarea/'.$baris.'/sikasep/';

			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			/*$config['base_url'] = base_url().'index.php/customersetor/cform/area/index/';
			$cari = $this->input->post('cari', FALSE)?$this->input->post('cari', FALSE):$this->input->get_post('cari', FALSE);
			$cari=strtoupper($cari);
			$stquery = "select * from tr_area
						   			   where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5')
									   and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')";*/

			if($area1=='00'){
				$stquery = " select	i_area, e_area_name, i_store from tr_area 
								where (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
								order by i_area ";
			}else{
				$stquery = " select i_area, e_area_name, i_store from tr_area 
									where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5')
									and (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
									order by i_area ";
			}

			$query = $this->db->query($stquery,false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('customersetor/mmaster');
			$data['baris']=$baris;
			$data['cari']=$cari;
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($area1,$area2,$area3,$area4,$area5,$cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('customersetor/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}	
}
?>
