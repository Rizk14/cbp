<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu492')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_tunai');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listtunai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu492')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari	  = $this->uri->segment(7);
      if($cari=='' || $cari=='index')	$cari	= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
      if($cari=='' || $cari=='index'){
	  		$config['base_url'] = base_url().'index.php/listtunai/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
        $cari='';
      }else{
  			$config['base_url'] = base_url().'index.php/listtunai/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
      }
			$teksquery=" select distinct a.i_tunai AS itunai
					from tm_tunai a
					left join tr_customer c on(a.i_customer=c.i_customer)
          left join tr_area d on(a.i_area=d.i_area)
          left join tm_rtunai_item f on(a.i_tunai=f.i_tunai and a.i_area=f.i_area_tunai)
					where (upper(a.i_tunai) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(c.e_customer_name) like '%$cari%' ";
					if(!is_numeric($cari)){
						$teksquery=$teksquery.")";
					}else{
						$teksquery=$teksquery."or a.v_jumlah=$cari)";
					}
					$teksquery=$teksquery." and
					a.i_area='$iarea' and
					(a.d_tunai >= to_date('$dfrom','dd-mm-yyyy') and
					a.d_tunai <= to_date('$dto','dd-mm-yyyy')) and a.f_tunai_cancel='f'";
			$query = $this->db->query($teksquery,false);
						
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_tunai');
			$this->load->model('listtunai/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Transfer uang masuk Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listtunai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu492')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_tunai');
			$this->load->view('listtunai/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu492')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $itunai	= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$dfrom		= $this->uri->segment(6);
			$dto			= $this->uri->segment(7);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$this->load->model('listtunai/mmaster');
			$this->mmaster->delete($itunai,$iarea);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Hapus Tunai Area '.$iarea.' No:'.$itunai;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$config['base_url'] = base_url().'index.php/listtunai/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$teksquery=" select distinct a.i_tunai AS itunai
					from tm_tunai a
					left join tr_customer c on(a.i_customer=c.i_customer)
          left join tr_area d on(a.i_area=d.i_area)
          left join tm_rtunai_item f on(a.i_tunai=f.i_tunai and a.i_area=f.i_area_tunai)
					where (upper(a.i_tunai) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(c.e_customer_name) like '%$cari%' ";
					if(!is_numeric($cari)){
						$teksquery=$teksquery.")";
					}else{
						$teksquery=$teksquery."or a.v_jumlah=$cari)";
					}
					$teksquery=$teksquery." and
					a.i_area='$iarea' and
					(a.d_tunai >= to_date('$dfrom','dd-mm-yyyy') and
					a.d_tunai <= to_date('$dto','dd-mm-yyyy')) and a.f_tunai_cancel='f'";
			$query = $this->db->query($teksquery,false);
						
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_tunai');
			$this->load->model('listtunai/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listtunai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu492')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listtunai/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
						
			$teksquery=" select distinct a.i_customer AS icustomer
					from tm_tunai a
					left join tr_customer c on(a.i_customer=c.i_customer)
					left join tr_area d on(a.i_area=d.i_area)
					left join tr_customer_owner e on(a.i_customer=e.i_customer)
					left join tm_pelunasan on(a.i_tunai=tm_pelunasan.i_giro and a.d_tunai=tm_pelunasan.d_giro)
					left join tm_dt on(tm_pelunasan.i_dt=tm_dt.i_dt and tm_pelunasan.i_area=tm_dt.i_area)
					where (upper(a.i_tunai) like '%$cari%' or upper(a.i_customer) like '%$cari%' or upper(c.e_customer_name) like '%$cari%' ";
					if(!is_numeric($cari)){
						$teksquery=$teksquery.")";
					}else{
						$teksquery=$teksquery."or a.v_jumlah=$cari)";
					}
					$teksquery=$teksquery." and
					((tm_pelunasan.i_jenis_bayar!='02' and 
					tm_pelunasan.i_jenis_bayar!='01' and 
					tm_pelunasan.i_jenis_bayar!='04' and 
					tm_pelunasan.i_jenis_bayar='03') or ((tm_pelunasan.i_jenis_bayar='03') is null)) and 
					a.i_area='$iarea' and
					(a.d_tunai >= to_date('$dfrom','dd-mm-yyyy') and
					a.d_tunai <= to_date('$dto','dd-mm-yyyy')) and a.f_tunai_cancel='f'";
			$query = $this->db->query($teksquery,false);
									
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_tunai');
			$this->load->model('listtunai/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listtunai/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu492')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listtunai/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				/* Disabled 07040211
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
				*/
				$query = $this->db->query("select * from tr_area where (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5') or i_area='XX' ",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listtunai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu492')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listtunai/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5') or i_area='XX' ",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listtunai/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listtunai/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
