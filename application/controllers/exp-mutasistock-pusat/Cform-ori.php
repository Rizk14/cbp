<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu227')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('exp-mutasistock-pusat/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu227')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$dto	= $this->input->post('dto');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/exp-mutasistock-pusat/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_target a, tr_area b
										where a.i_periode = '$iperiode'
										and a.i_area=b.i_area
										and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('exp-mutasistock-pusat/mmaster');
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('exp-mutasistock-pusat/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')))
		   )
		{
			$config['base_url'] = base_url().'index.php/exp-mutasistock-pusat/cform/store/index/';
			$query = $this->db->query("select distinct c.i_store as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name
										 from tr_store_location a, tr_store b, tr_area c
                               where a.i_store = b.i_store and b.i_store=c.i_store and a.i_store='AA' ");
			$config['total_rows'] = $query->num_rows(); 	
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-mutasistock-pusat/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-mutasistock-pusat/vlistarea', $data);
		}
		else
		{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) )
		   ){
			$config['base_url'] = base_url().'index.php/exp-mutasistock-pusat/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
        $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store and a.i_store='AA'
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-mutasistock-pusat/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-mutasistock-pusat/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu227')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
      	$this->load->model('exp-mutasistock-pusat/mmaster');
      	$iperiode = $this->uri->segment(4);
      	$iarea    = $this->uri->segment(5);
      	$iproduct = $this->uri->segment(6);
			$this->load->model('exp-mutasistock-pusat/mmaster');     
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['iperiode']	= $iperiode;
			$data['iarea']	  = $iarea;
			$data['iproduct']	= $iproduct;
			$data['detail']	= $this->mmaster->detail($iperiode,$iarea,$iproduct);
			$this->load->view('exp-mutasistock-pusat/vmainform',$data);
		}
		else
		{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu227')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$this->load->model('exp-mutasistock-pusat/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
      	$iarea = $this->input->post('iarea');
      	$istore = $this->input->post('istore');
      	$istorelocation = $this->input->post('istorelocation');
			if($iperiode=='')
			{
        		$iperiode=$this->uri->segment(4);
      	}
      	define ('BOOLEAN_FIELD',   'L');
      	define ('CHARACTER_FIELD', 'C');
      	define ('DATE_FIELD',      'D');
      	define ('NUMBER_FIELD',    'N');
      	define ('READ_ONLY',  '0');
      	define ('WRITE_ONLY', '1');
      	define ('READ_WRITE', '2');

      	$a=substr($iperiode,0,4);
	    	$b=substr($iperiode,4,2);
		  	$peri=mbulan($b)." - ".$a;

      	$query = $this->db->query("select a.i_store, a.e_store_name, b.i_store_location, b.e_store_locationname
                                 from tr_store a, tr_store_location b 
                                 where a.i_store=b.i_store and a.i_store='$istore' and b.i_store_location='$istorelocation'");
      	$st=$query->row();
      	$namaarea=$st->e_store_name;
      	$storeloc=$st->i_store_location;
      	$store=$st->i_store;
      	$namastoreloc=$st->e_store_locationname;
     		$daerah='00';
      	$db_file='spb/'.$daerah.'/gd'.$daerah.$iperiode.'.dbf';
      	if(file_exists($db_file))
      	{
        		@chmod($db_file, 777);
        		@unlink($db_file);
      	}
      	$dbase_definition = array (
         	array ('KODEPROD',  CHARACTER_FIELD,  7),
         	array ('NAMAPROD',  CHARACTER_FIELD,  45),
         	array ('STOCKOPNAM',  NUMBER_FIELD,  7, 0)
      		);
      
      	$create = @ dbase_create($db_file, $dbase_definition)
                or die ("Could not create dbf file <i>$db_file</i>.");
      	$id = @ dbase_open ($db_file, READ_WRITE)
            	or die ("Could not open dbf file <i>$db_file</i>."); 

      if($iperiode>'201512'){
        $this->db->select("	* from f_mutasi_stock_pusat_saldoakhir('$iperiode')",false);
      }else{
      	$this->db->select("	* from f_mutasi_stock_pusat('$iperiode')",false);
      }
		  	$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan Mutasi ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0)
			{
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(6);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN MUTASI STOK GUDANG PUSAT');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,15,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Gudang : '.$namaarea.'    Lokasi : '.$namastoreloc.'     Bulan : '.$peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,15,3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:P6'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4,5,6,5);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'PENERIMAAN');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(7,5,9,5);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'PENGELUARAN');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Stok');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M5', '+/-');
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        		$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Urut');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Barang');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Barang');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Awal');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Pembelian');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Ret Outlet');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'B B M');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Penj.');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'B B K');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Ke Pabrik');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Akhir');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L6', 'Opnam');
				$objPHPExcel->getActiveSheet()->getStyle('L6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M6', '( pc )');
				$objPHPExcel->getActiveSheet()->getStyle('M6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N6', 'GiT');
				$objPHPExcel->getActiveSheet()->getStyle('N6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O6', 'GiT Pnjln');
				$objPHPExcel->getActiveSheet()->getStyle('O6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P6', 'Harga');
				$objPHPExcel->getActiveSheet()->getStyle('P6')->applyFromArray(
					array(
						'borders' => array(
							'bottom'=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$i=7;
				$j=7;
				$xarea='';
				$saldo=0;
        		$no=0;
        		$group='';
				foreach($query->result() as $row)
				{
          		if($group=='' || $group!=$row->e_product_groupname)
          		{
    	      		$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,15,$i);
				    	$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row->e_product_groupname);
				    	$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
					    	array(
						    'borders' => array(
							    'top' 	=> array('style' => Style_Border::BORDER_THIN)
						    ),
					      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => true
					      )
					    )
				   	);
            		$no=0;
            		$i++;
		         }
          		$group=$row->e_product_groupname;
          		$no++;
          		$opname=$row->n_saldo_stockopname+$row->n_mutasi_git+$row->n_git_penjualan;
          		$selisih=$opname-$row->n_saldo_akhir;
          		$saldoawal=$row->n_saldo_awal+$row->n_mutasi_gitasal+$row->n_git_penjualanasal;
          		$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  	array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  	),
				  	'A'.$i.':P'.$i
				  	);

					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_product);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $saldoawal);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->n_mutasi_pembelian);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->n_mutasi_returoutlet);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->n_mutasi_bbm);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->n_mutasi_penjualan);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->n_mutasi_bbk);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->n_mutasi_returpabrik);
					$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->n_saldo_akhir);
					$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $opname);
					$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $selisih);
					$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $row->n_mutasi_git);
					$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $row->n_git_penjualan);
					$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $row->v_product_retail);
					$i++;
					$j++;
          		$isi = array ($row->i_product,$row->e_product_name,0);
          		dbase_add_record ($id, $isi) or die ("Data TIDAK bisa ditambahkan ke file <i>$db_file</i>."); 
				}
        		dbase_close($id);
        		@chmod($db_file, 0777);
				$x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      	$nama='gd'.$daerah.$iperiode.'.xls';
      	if(file_exists('spb/'.$daerah.'/'.$nama))
      	{
        		@chmod('spb/'.$daerah.'/'.$nama, 0777);
        		@unlink('spb/'.$daerah.'/'.$nama);
      	}
			$objWriter->save("spb/".$daerah.'/'.$nama); 
      	$sess=$this->session->userdata('session_id');
      	$id=$this->session->userdata('user_id');
      	$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      	$rs		= pg_query($sql);
      	if(pg_num_rows($rs)>0)
      	{
	      	while($row=pg_fetch_assoc($rs))
	      	{
		      	$ip_address	  = $row['ip_address'];
		      	break;
	      	}
      	}
      	else
      	{
	      	$ip_address='kosong';
      	}
      	$query 	= pg_query("SELECT current_timestamp as c");
      	while($row=pg_fetch_assoc($query))
      	{
      		$now	  = $row['c'];
      	}
      	$pesan='Export Mutasi Periode:'.$iperiode.' Area:'.$iarea;
      	$this->load->model('logger');
      	$this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses'] = true;
			$data['inomor']	= "Laporan Mutasi Stok";
      	@chmod('spb/'.$daerah.'/'.$nama, 0777);
      	@chmod($db_file, 0777);
			$this->load->view('nomor',$data);
		}
		else
		{
			$this->load->view('awal/index.php');
		}
	}
}
?>
