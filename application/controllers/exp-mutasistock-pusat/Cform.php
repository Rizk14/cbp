<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu227') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('exp-mutasistock-pusat/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu227') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$dto	= $this->input->post('dto');
			if ($iperiode == '') $iperiode = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/exp-mutasistock-pusat/cform/view/' . $iperiode . '/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_target a, tr_area b
										where a.i_periode = '$iperiode'
										and a.i_area=b.i_area
										and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('exp-mutasistock-pusat/mmaster');
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode, $config['per_page'], $this->uri->segment(6), $cari);
			$this->load->view('exp-mutasistock-pusat/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-mutasistock-pusat/cform/store/index/';
			$query = $this->db->query("select distinct c.i_store as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name
										 from tr_store_location a, tr_store b, tr_area c
                               where a.i_store = b.i_store and b.i_store=c.i_store and a.i_store='AA' ");
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-mutasistock-pusat/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi'] = $this->mmaster->bacastore($config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-mutasistock-pusat/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-mutasistock-pusat/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store and a.i_store='AA'
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-mutasistock-pusat/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi'] = $this->mmaster->caristore($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-mutasistock-pusat/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu227') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-mutasistock-pusat/mmaster');
			$iperiode = $this->uri->segment(4);
			$iarea    = $this->uri->segment(5);
			$iproduct = $this->uri->segment(6);
			$this->load->model('exp-mutasistock-pusat/mmaster');
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['iperiode']	= $iperiode;
			$data['iarea']	  = $iarea;
			$data['iproduct']	= $iproduct;
			$data['detail']	= $this->mmaster->detail($iperiode, $iarea, $iproduct);
			$this->load->view('exp-mutasistock-pusat/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu227') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-mutasistock-pusat/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$iarea = $this->input->post('iarea');
			$istore = $this->input->post('istore');
			$istorelocation = $this->input->post('istorelocation');
			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}
			define('BOOLEAN_FIELD',   'L');
			define('CHARACTER_FIELD', 'C');
			define('DATE_FIELD',      'D');
			define('NUMBER_FIELD',    'N');
			define('READ_ONLY',  '0');
			define('WRITE_ONLY', '1');
			define('READ_WRITE', '2');

			$a = substr($iperiode, 0, 4);
			$b = substr($iperiode, 4, 2);
			$peri = mbulan($b) . " - " . $a;

			$query = $this->db->query("select a.i_store, a.e_store_name, b.i_store_location, b.e_store_locationname
                                 from tr_store a, tr_store_location b 
                                 where a.i_store=b.i_store and a.i_store='$istore' and b.i_store_location='$istorelocation'");
			$st = $query->row();
			$namaarea = $st->e_store_name;
			$storeloc = $st->i_store_location;
			$store = $st->i_store;
			$namastoreloc = $st->e_store_locationname;
			$daerah = '00';
			$db_file = 'spb/' . $daerah . '/gd' . $daerah . $iperiode . '.dbf';
			if (file_exists($db_file)) {
				@chmod($db_file, 777);
				@unlink($db_file);
			}

			//   Untuk SO
			if ($iperiode > '201512') {
				$this->db->select("	* from f_mutasi_stock_pusat_saldoakhir4('$iperiode') where e_mutasi_periode ='$iperiode'", false);
			} else {
				$this->db->select("	* from f_mutasi_stock_pusat('$iperiode')", false);
			}

			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:P6'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'KODEPROD');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'NAMAPROD');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'STOCKOPNAM');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'GRADE');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i = 2;
				$j = 2;
				$xarea = '';
				$saldo = 0;
				$no = 0;
				$group = '';
				foreach ($query->result() as $row) {
					$no++;
					$saldoawal = '00';
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':D' . $i
					);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->i_product, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->e_product_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $saldoawal, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_product_grade, Cell_DataType::TYPE_STRING);
					$i++;
					$j++;
				}
				//die();
				$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
				$nama = 'so' . $daerah . $iperiode . '.xls';
				if (file_exists('spb/' . $daerah . '/' . $nama)) {
					@chmod('spb/' . $daerah . '/' . $nama, 0777);
					@unlink('spb/' . $daerah . '/' . $nama);
				}
				$objWriter->save("spb/" . $daerah . '/' . $nama);

				$data['sukses'] = true;
				$data['folder'] = 'spb/' . $daerah;
				$data['inomor']	= $nama; /* "Laporan Mutasi Stok" */

				@chmod('spb/' . $daerah . '/' . $nama, 0777);
				// @chmod($db_file, 0777);
				$this->load->view('nomorurl', $data);
			}


			//   Untuk Mutasi
			if ($iperiode > '201512') {
				$this->db->select("	a.*, ctg.e_sales_categoryname 
									FROM
										f_mutasi_stock_pusat_saldoakhir4('$iperiode') a
										INNER JOIN tr_product b ON a.i_product = b.i_product
										LEFT JOIN tr_product_sales_category ctg ON b.i_sales_category = ctg.i_sales_category ", false);
			} else {
				$this->db->select("	* from f_mutasi_stock_pusat('$iperiode')", false);
			}
			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan Mutasi ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(6);
				// $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(6);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN MUTASI STOK GUDANG PUSAT');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 16, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Gudang : ' . $namaarea . '    Lokasi : ' . $namastoreloc . '     Bulan : ' . $peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 16, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						),
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					),
					'A5:Q6'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Kategori');
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Kode');
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Nama');
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Saldo');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5, 5, 7, 5);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'PENERIMAAN');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, 5, 10, 5);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'PENGELUARAN');
				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Saldo');
				$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Stok');
				$objPHPExcel->getActiveSheet()->setCellValue('O5', 'Selisih');

				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'Urut');
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Penjualan');
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Barang');
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Barang');
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Awal');

				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'Pembelian');
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'Retur Penjualan');
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Dari Cabang');

				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Penjualan');
				$objPHPExcel->getActiveSheet()->setCellValue('J6', 'Ke Cabang');
				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'Ke Pabrik');

				$objPHPExcel->getActiveSheet()->setCellValue('L6', 'Adj');
				$objPHPExcel->getActiveSheet()->setCellValue('M6', 'Akhir');
				$objPHPExcel->getActiveSheet()->setCellValue('N6', 'Opnam');
				$objPHPExcel->getActiveSheet()->setCellValue('O6', '( pcs )');
				$objPHPExcel->getActiveSheet()->setCellValue('P6', 'GiT');
				$objPHPExcel->getActiveSheet()->setCellValue('Q6', 'GiT Penj');
				// $objPHPExcel->getActiveSheet()->setCellValue('Q6', 'Harga');

				$objPHPExcel->getActiveSheet()->getStyle('Q6')->applyFromArray(
					array(
						'borders' => array(
							'bottom' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$i = 7;
				$j = 7;
				$xarea = '';
				$saldo = 0;
				$no = 0;
				$group = '';
				foreach ($query->result() as $row) {
					if ($group == '' || $group != $row->e_product_groupname) {
						$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, $i, 16, $i);
						$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $row->e_product_groupname);
						$objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray(
							array(
								'borders' => array(
									'top' 	=> array('style' => Style_Border::BORDER_THIN)
								),
								'alignment' => array(
									'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
									'vertical'  => Style_Alignment::VERTICAL_CENTER,
									'wrap'      => true
								)
							)
						);
						$no = 0;
						$i++;
					}
					$group = $row->e_product_groupname;
					$no++;
					$opname = $row->n_saldo_stockopname;
					//   $selisih=$opname-$row->n_saldo_akhir;

					if ($row->adjus > 1) {
						$adjus = $row->adjus;
					} else {
						$adjus = 0;
					}



					$selisih = ($opname + $row->n_mutasi_git + $row->n_git_penjualan + $adjus) - $row->n_saldo_akhir;
					$saldoawal = $row->n_saldo_awal + $row->n_mutasi_gitasal + $row->n_git_penjualanasal;
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':Q' . $i
					);

					$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $no);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->e_sales_categoryname);
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->i_product);
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $saldoawal);
					$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $row->n_mutasi_pembelian);
					$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $row->n_mutasi_returoutlet);
					$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->n_mutasi_bbm);
					$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->n_mutasi_penjualan);
					$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->n_mutasi_bbk);
					$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $row->n_mutasi_returpabrik);
					$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row->adjus);
					$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $row->n_saldo_akhir);
					$objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $opname);
					$objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $selisih);
					$objPHPExcel->getActiveSheet()->setCellValue('P' . $i, $row->n_mutasi_git);
					$objPHPExcel->getActiveSheet()->setCellValue('Q' . $i, $row->n_git_penjualan);
					// $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $row->v_product_retail);
					$i++;
					$j++;
					$isi = array($row->i_product, $row->e_product_name, 0);
				}
				// @chmod($db_file, 0777);
				$x = $i - 1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama = 'gd' . $daerah . $iperiode . '.xls';
			if (file_exists('spb/' . $daerah . '/' . $nama)) {
				@chmod('spb/' . $daerah . '/' . $nama, 0777);
				@unlink('spb/' . $daerah . '/' . $nama);
			}
			$objWriter->save("spb/" . $daerah . '/' . $nama);

			$this->logger->writenew('Export Mutasi Periode:' . $iperiode . ' Area:' . $iarea);

			$data['sukses'] = true;
			$data['folder'] = 'spb/' . $daerah;
			$data['inomor']	= $nama; /* "Laporan Mutasi Stok" */

			@chmod('spb/' . $daerah . '/' . $nama, 0777);
			// @chmod($db_file, 0777);
			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
