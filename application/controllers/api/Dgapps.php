<?php 
class Dgapps extends CI_Controller
{

    public $i_company = "5";
    public $api_key = "0f4cb37d-4779-416c-8b22-17024da9d655";
    public $url_api = 'http://192.168.0.104/dgapps/index.php/rest/';

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        set_time_limit(0);
    }
    public function postarea()
    {
        include 'CurlDgapps.php';
        $CurlDgapps = new CurlDgapps;

        $data_area = $this->db->query("select * from tr_area");
        foreach ($data_area->result() as $row) {

            $data = array(
                'action' => 'create',
                'api_key' => $this->api_key,
                'i_company' => $this->i_company,
                'i_area' => $row->i_area,
                'e_area_name' => $row->e_area_name,
                'f_active' => 'true',
            );

            $url = $this->url_api . 'area';
            $data = json_encode($data);
            $CurlDgapps->postCURL($url, $data);
        }
    }

    public function postcustomer()
    {
        include 'CurlDgapps.php';
        $CurlDgapps = new CurlDgapps;
        $tgl = date('Y-m-d', strtotime('-7 days', strtotime(date('Y-m-d'))));
/*
        $customer = $this->db->query("select a.i_customer, a.e_customer_name, a.e_customer_contact, a.e_customer_phone, a.e_customer_address||' '||c.e_city_name as e_customer_address,
        a.i_area, a.i_price_group, d.n_customer_discount1, d.n_customer_discount2, d.n_customer_discount3 from tr_customer a, tr_customer_groupar b,
        tr_city c, tr_customer_discount d
       where a.i_customer=b.i_customer and a.i_city=c.i_city and a.i_area=c.i_area and d.i_customer=a.i_customer and a.i_customer='04F08'
        order by a.d_customer_update desc
            ");
*/

        $customer = $this->db->query("select a.i_customer, a.e_customer_name, a.e_customer_contact, a.e_customer_phone, a.e_customer_address||' '||c.e_city_name as e_customer_address,
        a.i_area, a.i_price_group, d.n_customer_discount1, d.n_customer_discount2, d.n_customer_discount3 from tr_customer a, tr_customer_groupar b,
        tr_city c, tr_customer_discount d
        where a.i_customer=b.i_customer and a.i_city=c.i_city and a.i_area=c.i_area and d.i_customer=a.i_customer
        and not a.i_area in('BK','BR','C1','D4','GD','H0','H3','H4','H5','XX','PB')
        and a.i_price_group <> ''
        and a.e_customer_address <> ''
        and a.e_customer_name <> ''
        /*and a.i_customer = '07087'*/
        and
        (
            ( not a.d_customer_update isnull and a.d_customer_update >= '$tgl')
            or
            (a.d_customer_entry >= '$tgl')
            or
            (d.d_customer_update >= '$tgl')
        )
        /*and c.e_city_name not ilike '%LUAR AREA%'*/
        order by a.d_customer_update desc
            ");
       /*and a.i_customer_status <> '4'*/
        foreach ($customer->result() as $row) {
            $data = array(
                'action' => 'create',
                'api_key' => $this->api_key,
                'i_company' => $this->i_company,
                'i_customer' => $row->i_customer,
                'e_customer_name' => $row->e_customer_name,
                'e_contact_name' => $row->e_customer_contact,
                'e_phone_number' => $row->e_customer_phone,
                'e_customer_address' => $row->e_customer_address,
                'i_area' => $row->i_area,
                'i_price_group' => $row->i_price_group,
                'n_customer_discount1' => $row->n_customer_discount1,
                'n_customer_discount2' => $row->n_customer_discount2,
                'f_active' => 'true',
            );

            $url = $this->url_api . 'customer';
            $data = json_encode($data);
            $response[] = $CurlDgapps->postCURL($url, $data);
        }

        echo json_encode($response);
        die;

    }

    public function postproduct()
    {
        include 'CurlDgapps.php';
        $CurlDgapps = new CurlDgapps;
        $tgl = date('Y-m-d', strtotime('-7 days', strtotime(date('Y-m-d'))));

        $product = $this->db->query("select a.i_product, a.e_product_name, b.i_product_grade, c.i_product_motif, b.i_price_group, e.i_product_group, e.e_product_groupname, b.v_product_retail,
        f.e_price_groupname, b.d_product_priceupdate, case when a.i_product_status = '4' then 'false' else 'true' end as status_product from
        tr_product a, tr_product_price b, tr_product_motif c, tr_product_type d, tr_product_group e, tr_price_group f where a.i_product=c.i_product
        and a.i_product=b.i_product and a.i_product_type=d.i_product_type
        and d.i_product_group=e.i_product_group
        and b.i_price_group = f.i_price_group
        and b.v_product_retail <> 0
/*         and (
        (
        b.d_product_priceupdate >= '$tgl'
        )

        or
        (a.d_product_entry >= '$tgl')
        ) */
        and d.i_product_group in('01','02','03')
        order by b.d_product_priceupdate desc ");

        foreach ($product->result() as $row) {
            $data = array(
                'action' => 'create',
                'api_key' => $this->api_key,
                'i_company' => $this->i_company,
                'i_product' => $row->i_product,
                'e_product_name' => $row->e_product_name,
                'i_product_group' => $row->i_product_group,
                'e_product_groupname' => $row->e_product_groupname,
                'f_active' => $row->status_product,
            );

            $url = $this->url_api . 'product';
            $data = json_encode($data);
            $CurlDgapps->postCURL($url, $data);

            $data = array(
                'action' => 'create',
                'api_key' => $this->api_key,
                'i_company' => $this->i_company,
                'i_product' => $row->i_product,
                'e_product_name' => $row->e_product_name,
                'i_product_grade' => $row->i_product_grade,
                'i_price_group' => $row->i_price_group,
                'e_price_groupname' => $row->e_price_groupname,
                'v_product_price' => $row->v_product_retail,

            );

            $url = $this->url_api . 'productprice';
            $data = json_encode($data);
            $response = $CurlDgapps->postCURL($url, $data);

        }

    }

    public function postrrkh()
    {
        include 'CurlDgapps.php';
        $CurlDgapps = new CurlDgapps;
        $tgl = date('Y-m-d', strtotime('-7 days', strtotime(date('Y-m-d'))));

        $query = $this->db->query("select a.* from tm_rrkh_item a, tm_rrkh b where
        a.i_area = b.i_area
        and a.i_salesman = b.i_salesman
        and a.d_rrkh = b.d_rrkh
        and b.d_rrkh >= '$tgl' and not b.i_approve isnull /*and a.i_area in('01','02')*/");

        foreach ($query->result() as $row) {
            $data = array(
                'action' => 'create',
                'api_key' => $this->api_key,
                'i_company' => $this->i_company,
                'i_area' => $row->i_area,
                'username' => $row->i_salesman,
                'd_rrkh' => $row->d_rrkh,
                'i_customer' => $row->i_customer,
            );

            $url = $this->url_api . 'rrkh';
            $data = json_encode($data);
            $CurlDgapps->postCURL($url, $data);
        }

    }

    public function getsalesorder()
    {
        include 'CurlDgapps.php';
        $CurlDgapps = new CurlDgapps;
        $startTime = date('Y-m-d', strtotime('-3 days', strtotime(date('Y-m-d'))));
        $endTime = date('Y-m-d');
        $fulfilled = 'false';

        $url = $this->url_api . "salesorder?action=list&api_key=" . $this->api_key . "&i_company=" . $this->i_company . "&starttime=" . $startTime . "&endtime=" . $endTime . "&fulfilled=" . $fulfilled;
        $response = $CurlDgapps->get_curl($url);

        if ($response['data']) {
            foreach ($response['data'] as $row) {
                $i_spb_reff = $row['i_spb'];
                $i_customer = $row['i_customer'];
                $i_area = $row['i_area'];
                $i_salesman = $row['i_staff'];
                $d_spb = $row['d_spb'];
                $i_product_group = $row['i_product_group'];
                $i_price_group = $row['i_price_group'];
                $e_remark_head = $row['e_remark'];
                $n_spb_discount1 = $row['n_spb_discount1'];
                $n_spb_discount2 = $row['n_spb_discount2'];
                $n_spb_discount3 = $row['n_spb_discount3'];
                $v_spb_discount1 = $row['v_spb_discount1'];
                $v_spb_discount2 = $row['v_spb_discount2'];
                $v_spb_discount3 = $row['v_spb_discount3'];
                $v_spb_discounttotal = $row['v_spb_discounttotal'];
                $v_spb_gross = $row['v_spb_gross'];
                $v_spb_netto = $row['v_spb_netto'];
                $thbl = date("Ym", strtotime($d_spb));

                $cek_spb = $this->db->query("select i_reff_advo from tm_spb where i_reff_advo = '$i_spb_reff' and i_area = '$i_area' and i_customer = '$i_customer'");
                $datadisc = $this->db->query(" SELECT n_customer_discount1, n_customer_discount2, n_customer_discount3 FROM tr_customer_discount WHERE i_customer = '$i_customer' ", FALSE)->row();
                    $n_spb_discount1     = $datadisc->n_customer_discount1;
                    $n_spb_discount2     = $datadisc->n_customer_discount2;
                    $n_spb_discount3     = $datadisc->n_customer_discount3;
                if ($cek_spb->num_rows() > 0) {

                } else {
                    $this->load->model('middguadvotics/mmaster');
                    $this->db->trans_begin();
                    $i_spb = $this->mmaster->runningnumber($i_area, $thbl);

                    $ispbpo = null;
                    $data_customer = $this->db->query("select * from tr_customer where i_customer = '$i_customer'")->row();
                    $nspbtoplength = $data_customer->n_customer_toplength;
                    $fspbop = 'f';
                    $data_customer_pkp = $this->db->query("select * from tr_customer_pkp where i_customer = '$i_customer'");
                    if ($data_customer_pkp->num_rows() > 0) {
                        $data_customer_pkp = $data_customer_pkp->row();
                        $ecustomerpkpnpwp = $data_customer_pkp->e_customer_pkpnpwp;
                        $fspbpkp = 't';
                    } else {
                        $ecustomerpkpnpwp = null;
                        $fspbpkp = 'f';
                    }

                    $fspbplusppn = $data_customer->f_customer_plusppn;
                    $fspbplusdiscount = 'f';
                    if ($i_area == '00' || $i_area == '02' || $i_area == '06' || $i_area == '09' || $i_area == '10' || $i_area == '11' || $i_area == '12' || $i_area == '13' || $i_area == '16' || $i_area == '17' || $i_area == '18' || $i_area == '19' || $i_area == '21' || $i_area == '22' || $i_area == '23' || $i_area == '27' || $i_area == '40' || $i_area == '41' || $i_area == '43' || $i_area == '45' || $i_area == 'XX' ) {
                        $fspbstockdaerah = 'f';
                    } else {
                        $fspbstockdaerah = 't';
                    }
                    $fspbprogram = 'f';
                    $fspbvalid = 'f';
                    $fspbsiapnotagudang = 'f';
                    $fspbcancel = 'f';
                    $fspbconsigment = 'f';
                    $ispbold = null;
                    $f_status_transfer = 'f';

                    
                    $no = 1;
                    $totalgross = 0;
                    $disctotal = 0;
                    foreach ($row['items'] as $raw) {
                        $iproduct = $raw['i_product'];
                        $iproductgrade = $raw['i_product_grade'];
                        $iproductmotif = '00';
                        $norder = $raw['n_order'];
                        
                        $harga_master = $this->db->query("select v_product_retail from tr_product_price where i_product = '$iproduct' and i_price_group = '$i_price_group' and i_product_grade = '$iproductgrade';");

                        if ($harga_master->num_rows() > 0) {
                            $vunitprice = $harga_master->row()->v_product_retail;
                        } else {
                            $vunitprice = $raw['v_unit_price'];
                        }

                        $e_remark = $raw['e_remark'];

                        $totalgross = $totalgross + ($norder*$vunitprice);

                        if (strlen($e_remark) > 50) {
                            $e_remark = substr($e_remark, 0, 50);
                        }

                        $data_barang = $this->db->query("select * from tr_product where i_product='$iproduct'");

                        foreach ($data_barang->result() as $barang) {

                            $iproductstatus = $barang->i_product_status;

                            $eproductname = $barang->e_product_name;

                        }
                        $ndeliver = null;
                        $this->mmaster->insertdetail($i_spb, $i_area, $iproduct, $iproductstatus, $iproductgrade, $eproductname, $norder, $ndeliver, $vunitprice, $iproductmotif, $e_remark, $no);
                        $no++;
                    }

                    if ($n_spb_discount1 > 0) {
                        $disc1 = $totalgross * $n_spb_discount1 / 100;
                    } else {
                        $disc1 = 0;
                    }
                    
                    if ($n_spb_discount2 > 0) {
                        $disc2 = ($totalgross-$disc1) * $n_spb_discount2 / 100;
                    } else {
                        $disc2 = 0;
                    }

                    if ($n_spb_discount3 > 0) {
                        $disc3 = ($totalgross-$disc1-$disc2) * $n_spb_discount3 / 100;
                    } else {
                        $disc3 = 0;
                    }

                    $disctotal = $disc1+$disc2+$disc3;

                    $this->mmaster->insertheader($i_spb, $d_spb, $i_customer, $i_area, $ispbpo, $nspbtoplength, $i_salesman,
                        $i_price_group, $fspbop, $ecustomerpkpnpwp, $fspbpkp,
                        $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
                        $fspbsiapnotagudang, $fspbcancel, $n_spb_discount1,
                        $n_spb_discount2, $n_spb_discount3, $disc1, $disc2,
                        $disc3, $disctotal, $totalgross, $fspbconsigment, $ispbold, $e_remark_head, $i_spb_reff, $f_status_transfer, $i_product_group);

                    if (($this->db->trans_status() === false)) {
                        $this->db->trans_rollback();
                    } else {
                        $data_fulfill = array(
                            'action' => 'fulfill',
                            'api_key' => $this->api_key,
                            'i_company' => $this->i_company,
                            'i_spb' => $i_spb_reff,
                            'i_area' => $i_area,
                            'i_customer' => $i_customer,
                        );

                        $url_fulfill = $this->url_api . 'salesorder';
                        $data_fulfill = json_encode($data_fulfill);
                        $CurlDgapps->postCURL($url_fulfill, $data_fulfill);

                        $this->db->trans_commit();
                        $hasil[$no]['i_reff_advo'] = $i_spb_reff;
                        $hasil[$no]['i_spb'] = $i_spb;
                        $no++;
                    }
                }

            }

        }
    }

    public function stok()
    {
        include 'CurlDgapps.php';
        $CurlDgapps = new CurlDgapps;

        $periode = date('Ym');
        $stok = $this->db->query("
            SELECT i_store, i_product, n_saldo_akhir as n_quantity_stock FROM f_mutasi_stock_pusat_saldoakhir('$periode') 
            where i_product_grade = 'A' and n_saldo_akhir > 0 
            union all
            SELECT i_store, i_product, n_saldo_akhir as n_quantity_stock FROM f_mutasi_stock_daerah_all_saldoakhir('$periode')
            where i_product_grade = 'A' and n_saldo_akhir > 0 
        ");

        $data = array(
            'action' => 'create',
            'api_key' => $this->api_key,
            'i_company' => $this->i_company,
            'data' => $stok->result_array(),
        );

        $url = $this->url_api . 'stokproduct';
        $data = json_encode($data);
        $response = $CurlDgapps->postCURL($url, $data);
        echo $response;
        die;

    }

    public function postproductstatus()
    {

        $cari = strtoupper($this->uri->segment('4'));

        include 'CurlDgapps.php';
        $CurlDgapps = new CurlDgapps;

        $product = $this->db->query("select a.i_product, a.e_product_name, b.i_product_group,
        case when a.i_product_status = '4' then 'f' else 't' end as status_product from tr_product a, tr_product_type b
        where a.i_product_type = b.i_product_type
        and b.i_product_group in('01','02','06')
        and a.i_product like '%$cari%'
        ");

        $data = array(
            'action' => 'create',
            'api_key' => $this->api_key,
            'i_company' => $this->i_company,
            'data' => $product->result_array(),
        );

        $url = $this->url_api . 'productstatus';
        $data = json_encode($data);
        $response = $CurlDgapps->postCURL($url, $data);

        echo json_encode($response);
    }

    public function customer_salesman()
    {
        include 'CurlDgapps.php';
        $CurlDgapps = new CurlDgapps;

        $i_periode = date('Ym');
        $query = $this->db->query("
            select a.i_salesman as i_staff, i_product_group, array_agg(a.i_customer order by a.i_customer asc) as i_customer 
            from tr_customer_salesman a
            inner join tr_salesman b on (a.i_salesman = b.i_salesman)
            inner join tr_customer c on (a.i_customer = c.i_customer)
            where b.f_salesman_aktif = true and c.f_customer_aktif = true and a.i_customer is not null and e_periode = '$i_periode' and i_product_group in ('02','01','03')
            group by 1,2
            order by 1,2
        ");
        if ($query->num_rows() > 0) {
            $data = array(
                'action' => 'create',
                'api_key' => $this->api_key,
                'i_company' => $this->i_company,
                'i_periode' => $i_periode,
                'data' => $query->result_array(),
            );
            $url = $this->url_api . 'customer_salesman';
            $data = json_encode($data);
            $response = $CurlDgapps->postCURL($url, $data);
            echo $response;
            die;
        }
    }

}
