<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Coba extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        /*         $this->load->helper('directory');
        $this->load->helper('file');
        $this->load->library('excel');
        require_once "php/fungsi.php"; */
        $this->obj = &get_instance();
    }

    public function va()
    {
        $query = $this->db->query("select a.i_area,a.i_customer, b.i_customer_groupbayar from tr_customer a, tr_customer_groupbayar b
        where
        a.i_customer = b.i_customer
        and a.i_customer not in(select i_customer from tr_customer_va)
	and a.i_customer in('01H53',
'01H54',
'01H55',
'01H56',
'01H75',
'01H76',
'01H77',
'01H93',
'01I01',
'01I02',
'01I05',
'01I06',
'01I07',
'01I08',
'01I34',
'01I35',
'01I62',
'01I63',
'01I67',
'01I68',
'01H52')");

        $va_perusahaan = "50020";
        foreach ($query->result() as $row) {
            $i_area = $row->i_area;
            $qnoterakhir = $this->db->query("select n_modul_no as max from tm_dgu_no
            where i_modul='VA'
            and i_area='$i_area'");
            if ($qnoterakhir->num_rows() > 0) {
                $qnoterakhir = $qnoterakhir->row();
                $terakhir = $qnoterakhir->max;
                $no_va = $terakhir + 1;
                $this->db->query("update tm_dgu_no
                set n_modul_no=$no_va
                where i_modul='VA'
                and i_area='$i_area'");
            } else {
                $thbl = date('Ym');
                $this->db->query(" insert into tm_dgu_no(i_modul, i_area, e_periode, n_modul_no)
                values ('VA','$i_area','$thbl',1)");
                $no_va = 1;
            }
            $nosekarang = $no_va;
            settype($nosekarang, "string");
            $a = strlen($nosekarang);
            while ($a < 5) {
                $nosekarang = "0" . $nosekarang;
                $a = strlen($nosekarang);
            }
            $i_customer_va = $va_perusahaan . $row->i_area . $nosekarang;
            // $i_customer_va = '500200100410';
            $data = array(
                'i_customer' => $row->i_customer,
                'i_customer_groupbayar' => $row->i_customer_groupbayar,
                'i_customer_va' => $i_customer_va,
            );

            $this->db->insert('tr_customer_va', $data);
        }
    }

    public function closingkasbank()
    {
        $sekarang     = date('Ym');
        $sebelumnya = date('Ym', strtotime('-1 month', strtotime(date('Y-m-d'))));
        $this->load->model('api/mclosing');
        $this->db->trans_begin();
        /*----------  Hitung Kas Kecil Bulan Sebelumnya  ----------*/
        $this->mclosing->kaskecil($sekarang, $sebelumnya);
        /*----------  Hitung Kas Besar Bulan Sebelumnya  ----------*/
        $this->mclosing->kasbesar($sekarang, $sebelumnya);
        /*----------  Hitung Kas Besar Bulan Sebelumnya  ----------*/
        $this->mclosing->bank($sekarang, $sebelumnya);
        /*----------  Update Saldo Awal Bulan Sekarang Berdasarkan Saldo Akhir Bulan Sebelumnya Hasil Hitung  ----------*/
        $this->mclosing->closingall($sekarang, $sebelumnya);
        /*----------  Update Periode dan Closing  ----------*/
        $this->mclosing->updateperiode($sekarang, $sebelumnya);
        if ($this->db->trans_status() === FALSE || $this->db->trans_status() === false) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
            $this->load->model('logger');
            $query     = pg_query("SELECT current_timestamp as c");
            while ($row = pg_fetch_assoc($query)) {
                $now      = $row['c'];
            }
            $id = 'admin';
            $ip_address = '127.0.0.1';
            $pesan = 'Closing Kas Bank Periode ' . $sebelumnya . '  ke Periode:' . $sekarang;
            $this->logger->write($id, $ip_address, $now, $pesan);
            // $this->logger->write('Auto Closing Tanggal '.date('d-m-Y').' Transaksi Kas Dan Bank Periode : '.$sebelumnya);
        }
    }

    public function hutang1()
    {
        $qperiode = $this->db->query("select i_periode from tm_periode")->row();
        $periode = $qperiode->i_periode;
        $query = $this->db->query("
        select x.* from(
            select a.i_op, a.i_do, a.i_supplier, a.i_product, round(a.v_pabrik, 2) as v_pabrik from tm_dtap_item a, tm_dtap b where
            a.i_area = b.i_area
            and a.i_dtap = b.i_dtap
            and a.i_supplier = b.i_supplier
            and a.n_dtap_year = b.n_dtap_year
            and to_char(b.d_dtap,'yyyymm')='$periode'
            and b.f_dtap_cancel = 'f'
            order by a.i_op
            ) as x
            where
            x.i_do||x.i_supplier||x.i_product||x.v_pabrik not in(
            select x.i_do||x.i_supplier||x.i_product||x.v_product_mill from(
            select a.i_do, a.i_supplier, a.i_product, round(a.v_product_mill, 2) as v_product_mill from tm_do_item a, tm_do b
            where to_char(a.d_do,'yyyymm')='$periode'
            and a.i_op = b.i_op
            and b.f_do_cancel = 'f'
            ) as x

            )
        ");

        foreach ($query->result() as $row) {
            $kode_barang = $row->i_product;
            $i_op = $row->i_op;
            $i_do = $row->i_do;
            $i_supplier = $row->i_supplier;
            $harga = $row->v_pabrik;
            $this->db->query("update tm_do_item set v_product_mill = $harga where i_op = '$i_op' and i_do = '$i_do' and i_product = '$kode_barang'");
        }
        // echo "berhasil";
    }

    public function hutang2()
    {
        $qperiode = $this->db->query("select i_periode from tm_periode")->row();
        $periode = $qperiode->i_periode;
        $query = $this->db->query("
        select x.* from(
            select a.i_op, a.i_do, a.i_supplier, a.i_product, round(a.v_pabrik, 2) as v_pabrik from tm_dtap_item a, tm_dtap b where
            a.i_area = b.i_area
            and a.i_dtap = b.i_dtap
            and a.i_supplier = b.i_supplier
            and a.n_dtap_year = b.n_dtap_year
            and to_char(b.d_dtap,'yyyymm')='$periode'
            and b.f_dtap_cancel = 'f'
            order by a.i_op
            ) as x
            where
            x.i_op||x.i_supplier||x.i_product||x.v_pabrik not in(
            select x.i_op||x.i_supplier||x.i_product||x.v_product_mill from(
            select a.i_op, a.i_product, round(a.v_product_mill, 2) as v_product_mill, b.i_supplier from tm_op_item a, tm_op b
            where to_char(b.d_op,'yyyymm')='$periode'
            and a.i_op = b.i_op
            and b.f_op_cancel = 'f'
            ) as x

            )
        ");

        foreach ($query->result() as $row) {
            $kode_barang = $row->i_product;
            $i_op = $row->i_op;
            $harga = $row->v_pabrik;
            $this->db->query("update tm_op_item set v_product_mill = $harga where i_op = '$i_op' and i_product = '$kode_barang'");
        }
        // echo "berhasil";
    }

    public function update_sisa()
    {
        $this->db->query("update tm_nota set v_sisa = v_nota_netto where
        v_sisa > v_nota_netto
        and f_nota_cancel = 'f'
        and i_nota isnull");
        echo "berhasil";
    }

    public function trend()
    {
        $ohari_ini = "01-01-" . date("Y");
        $hari_ini = date("d-m-Y");

        $olhari_ini = "01-01-" . date('Y', strtotime('-1 year', strtotime($ohari_ini)));
        $lhari_ini = date('d-m-Y', strtotime('-1 year', strtotime($hari_ini)));

        $conn = pg_connect("host=192.168.0.93 port=5432 dbname=report user=dedy password=g#>m[J2P^^") or die('Gagal');

        pg_query("ALTER SEQUENCE tt_trendtokoperitem_id_seq restart");
        pg_query("DELETE from tt_trendtokoperitem");

        /**DGU**/
        /*pg_query("  INSERT INTO dgu_report (i_area, e_area_name, i_nota, d_nota, i_customer, e_customer_name,
        e_customer_classname, e_product_categoryname, i_product, e_product_name, vnota, qnota, oa, prevvnota,
        prevqnota, prevoa, e_company)
        SELECT *,'DGU' FROM baca_trenditem('$ohari_ini','$hari_ini','$olhari_ini','$lhari_ini')");*/

        pg_query("  insert into tt_trendtokoperitem
                    (i_area, e_area_name, i_nota, d_nota, i_customer, e_customer_name, e_customer_classname, e_product_categoryname,
                    i_product, e_product_name, vnota, qnota, oa, prevvnota, prevqnota, prevoa, e_company)
                    select * from dblink('host=192.168.0.93 user=dedy password=g#>m[J2P^^ dbname=dialogue_new','
                        SELECT *,''DGU'' FROM baca_trenditem(''$ohari_ini'',''$hari_ini'',''$olhari_ini'',''$lhari_ini'')')as b
                        (i_area character(2), e_area_name character varying(50), i_nota character(15), d_nota date, i_customer character(5), e_customer_name character varying(50), e_customer_classname character varying(25), e_product_categoryname character varying(30), i_product character(7), e_product_name character varying(100), vnota numeric(12,0), qnota numeric(10,0), oa numeric(10,0), prevvnota numeric(12,0), prevqnota numeric(10,0), prevoa numeric(10,0), e_company character varying(50));");

        /**BLN**/
        pg_query(" insert into tt_trendtokoperitem
                            (i_area, e_area_name, i_nota, d_nota, i_customer, e_customer_name, i_customer_dgu, e_customer_classname, e_product_categoryname,
                            i_product, e_product_name, vnota, qnota, oa, prevvnota, prevqnota, prevoa, e_company)
                            select * from dblink('host=192.168.0.93 user=dedy password=g#>m[J2P^^ dbname=bln','
                            SELECT *,''BLN'' FROM baca_trenditem(''$ohari_ini'',''$hari_ini'',''$olhari_ini'',''$lhari_ini'')')as b
                            (i_area character(2), e_area_name character varying(50), i_nota character(15), d_nota date, i_customer character(5), e_customer_name character varying(50),  i_customer_dgu character(5), e_customer_classname character varying(25), e_product_categoryname character varying(30), i_product character(7), e_product_name character varying(100), vnota numeric(12,0), qnota numeric(10,0), oa numeric(10,0), prevvnota numeric(12,0), prevqnota numeric(10,0), prevoa numeric(10,0), e_company character varying(50));");

        /**DPP**/
        pg_query(" insert into tt_trendtokoperitem
                            (i_area, e_area_name, i_nota, d_nota, i_customer, e_customer_name, i_customer_dgu, e_customer_classname, e_product_categoryname,
                            i_product, e_product_name, vnota, qnota, oa, prevvnota, prevqnota, prevoa, e_company)
                            select * from dblink('host=192.168.0.93 user=dedy password=g#>m[J2P^^ dbname=dpp','
                            SELECT *,''DPP'' FROM baca_trenditem(''$ohari_ini'',''$hari_ini'',''$olhari_ini'',''$lhari_ini'')')as b
                            (i_area character(2), e_area_name character varying(50), i_nota character(15), d_nota date, i_customer character(5), e_customer_name character varying(50),  i_customer_dgu character(5), e_customer_classname character varying(25), e_product_categoryname character varying(30), i_product character(7), e_product_name character varying(100), vnota numeric(12,0), qnota numeric(10,0), oa numeric(10,0), prevvnota numeric(12,0), prevqnota numeric(10,0), prevoa numeric(10,0), e_company character varying(50));");

        /**PRS**/
        pg_query(" insert into tt_trendtokoperitem
                            (i_area, e_area_name, i_nota, d_nota, i_customer, e_customer_name, i_customer_dgu, e_customer_classname, e_product_categoryname,
                            i_product, e_product_name, vnota, qnota, oa, prevvnota, prevqnota, prevoa, e_company)
                            select * from dblink('host=192.168.0.93 user=dedy password=g#>m[J2P^^ dbname=prs','
                            SELECT *,''PRS'' FROM baca_trenditem(''$ohari_ini'',''$hari_ini'',''$olhari_ini'',''$lhari_ini'')')as b
                            (i_area character(2), e_area_name character varying(50), i_nota character(15), d_nota date, i_customer character(5), e_customer_name character varying(50),  i_customer_dgu character(5), e_customer_classname character varying(25), e_product_categoryname character varying(30), i_product character(7), e_product_name character varying(100), vnota numeric(12,0), qnota numeric(10,0), oa numeric(10,0), prevvnota numeric(12,0), prevqnota numeric(10,0), prevoa numeric(10,0), e_company character varying(50));");

        pg_close($conn);
    }

    public function opvsdoimma()
    {
        $ohari_ini = date("Y") . "-01-01";
        $hari_ini = date("Y-m-d");

        $conn = pg_connect("host=192.168.0.93 port=5432 dbname=sysdbimma user=dedy password=g#>m[J2P^^") or die('Gagal');

        pg_query("ALTER SEQUENCE tt_op_vs_do_id_seq restart");
        pg_query("DELETE from tt_op_vs_do");

        pg_query("  insert into tt_op_vs_do
                    (codecus, namecab, alamat, kota, area, sales, tglop, noop, opreferensi, tgldo, nodo, i_faktur_code, imotif, emotif, harga, qty, v_discount, v_discount1, v_discount2, qtydo, qtysumdo, i_group_code, tglspb)
                    select * from f_exp_op_vs_do ('$ohari_ini','$hari_ini');");

        pg_close($conn);
    }

    public function update_stock()
    {
        // $thbln = $this->db->query("select i_periode from tm_periode")->row()->i_periode;
        $thbln = date('Ym');
        // $thblnkemarin = '202003';
        $area = $this->db->query("SELECT  i_store, i_store_location
        from f_mutasi_stock_daerah_all_saldoakhir('$thbln')
        group by i_store, i_store_location
        order by i_store");

        foreach ($area->result() as $area) {
            $i_store = $area->i_store;
            $istorelocation = $area->i_store_location;
            // $cek_saldo_awal = $this->db->query("select * from tm_mutasi_saldoawal where e_mutasi_periode = '$thbln' and i_store = '$i_store'");

            // if($cek_saldo_awal->num_rows() > 0){
            $this->db->query("UPDATE tm_ic SET n_quantity_stock = 0 WHERE i_store='$i_store' and i_store_location='$istorelocation'");
            $que2 = $this->db->query("select i_product, i_store, i_product_grade, i_product_motif, i_store_location, (n_saldo_akhir)-(n_saldo_git+n_git_penjualan) as saldo_akhir from(
                    select e_product_groupname, i_product, i_product_grade,i_product_motif,sum(n_saldo_awal) as n_saldo_awal,
                    sum(n_mutasi_pembelian) as n_mutasi_pembelian,
                    sum(n_mutasi_returoutlet) as n_mutasi_returoutlet, sum(n_mutasi_bbm) as n_mutasi_bbm,
                    sum(n_mutasi_penjualan) as n_mutasi_penjualan,
                    sum(n_mutasi_bbk) as n_mutasi_bbk, sum(n_saldo_akhir) as n_saldo_akhir, sum(n_mutasi_ketoko) as n_mutasi_ketoko,
                    sum(n_mutasi_daritoko) as n_mutasi_daritoko, sum(n_mutasi_git) as n_saldo_git, e_mutasi_periode, i_store,
                    sum(n_git_penjualan) as n_git_penjualan, sum(n_git_penjualanasal) as n_git_penjualanasal,
                    sum(n_mutasi_gitasal) as n_mutasi_gitasal,
                    sum(n_saldo_stockopname) as n_saldo_stockopname, e_product_name, i_store_location
                    from f_mutasi_stock_daerah_saldoakhir('$thbln','$i_store','$istorelocation')
                    group by i_product, i_product_grade, e_product_groupname, e_mutasi_periode, i_store, e_product_name,
                             i_store_location, i_product_motif
                    order by e_product_groupname, e_product_name, i_product) as z");

            if ($que2->num_rows() > 0) {
                foreach ($que2->result() as $xx2) {

                    $this->db->query("UPDATE tm_ic
                            SET n_quantity_stock = $xx2->saldo_akhir
                            WHERE
                             i_product='$xx2->i_product' and i_store='$xx2->i_store' and i_product_grade='$xx2->i_product_grade' and i_store_location='$xx2->i_store_location' and i_product_motif='$xx2->i_product_motif'");
                }
            }
            // }else{
            //     $this->db->query("
            //     delete from tm_mutasi_saldoawal  where e_mutasi_periode='$thbln' and i_store = '$i_store';
            //     insert into tm_mutasi_saldoawal
            //     SELECT   i_store, i_store_location, i_store_locationbin, '$thbln', i_product, i_product_grade, i_product_motif, n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_awal
            //     from f_mutasi_stock_daerah_all_saldoakhir('$thblnkemarin') where i_store = '$i_store'");
            // }
        }
        echo 'sudah';
        die;
    }

    public function update_jatuh_tempo()
    {

        $query = $this->db->query("select a.i_nota, a.i_spb, a.i_sj, a.i_area,a.d_nota, b.n_spb_toplength, a.d_sj_receive, a.d_jatuh_tempo, a.d_dkb, a.i_dkb from tm_nota a, tm_spb b where
        a.i_nota = b.i_nota
        and a.i_area = b.i_area
        and a.i_spb = b.i_spb
        and a.i_nota in('FP-1912-0000615',
        'FP-1912-0000616',
        'FP-1912-0000617',
        'FP-1912-0000618',
        'FP-1912-0000619',
        'FP-1912-0104130',
        'FP-1912-0104131',
        'FP-1912-0104132',
        'FP-1912-0104133',
        'FP-1912-0104134',
        'FP-1912-0104135',
        'FP-1912-0104136',
        'FP-1912-0104137',
        'FP-1912-0104138',
        'FP-1912-0104139',
        'FP-1912-0104140',
        'FP-1912-0104141',
        'FP-1912-0104142',
        'FP-1912-0104143',
        'FP-1912-0104144',
        'FP-1912-0104145',
        'FP-1912-0104146',
        'FP-1912-0104147',
        'FP-1912-0104148',
        'FP-1912-0104149',
        'FP-1912-0104150',
        'FP-1912-0104151',
        'FP-1912-0104152',
        'FP-1912-0104153',
        'FP-1912-0104154',
        'FP-1912-0104155',
        'FP-1912-0104156',
        'FP-1912-0104157',
        'FP-1912-0104158',
        'FP-1912-0104159',
        'FP-1912-0104160',
        'FP-1912-0104161',
        'FP-1912-0104162',
        'FP-1912-0104163',
        'FP-1912-0104164',
        'FP-1912-0104165',
        'FP-1912-0104166',
        'FP-1912-0104167',
        'FP-1912-0104168',
        'FP-1912-0104169',
        'FP-1912-0207535',
        'FP-1912-0207536',
        'FP-1912-0207537',
        'FP-1912-0207538',
        'FP-1912-0207539',
        'FP-1912-0207540',
        'FP-1912-0207541',
        'FP-1912-0207542',
        'FP-1912-0207543',
        'FP-1912-0207544',
        'FP-1912-0207545',
        'FP-1912-0207546',
        'FP-1912-0207547',
        'FP-1912-0207548',
        'FP-1912-0207549',
        'FP-1912-0207550',
        'FP-1912-0207551',
        'FP-1912-0207552',
        'FP-1912-0207553',
        'FP-1912-0207554',
        'FP-1912-0207555',
        'FP-1912-0207556',
        'FP-1912-0207557',
        'FP-1912-0207558',
        'FP-1912-0207559',
        'FP-1912-0207560',
        'FP-1912-0207561',
        'FP-1912-0207562',
        'FP-1912-0207563',
        'FP-1912-0207564',
        'FP-1912-0207565',
        'FP-1912-0207566',
        'FP-1912-0207567',
        'FP-1912-0207568',
        'FP-1912-0207569',
        'FP-1912-0207570',
        'FP-1912-0207571',
        'FP-1912-0207572',
        'FP-1912-0207573',
        'FP-1912-0207574',
        'FP-1912-0207575',
        'FP-1912-0207576',
        'FP-1912-0207577',
        'FP-1912-0207578',
        'FP-1912-0207579',
        'FP-1912-0207580',
        'FP-1912-0207581',
        'FP-1912-0207582',
        'FP-1912-0207583',
        'FP-1912-0207584',
        'FP-1912-0207585',
        'FP-1912-0207586',
        'FP-1912-0207587',
        'FP-1912-0207588',
        'FP-1912-0207589',
        'FP-1912-0207590',
        'FP-1912-0207591',
        'FP-1912-0207592',
        'FP-1912-0207593',
        'FP-1912-0207594',
        'FP-1912-0207595',
        'FP-1912-0207596',
        'FP-1912-0207597',
        'FP-1912-0207598',
        'FP-1912-0207599',
        'FP-1912-0207600',
        'FP-1912-0207601',
        'FP-1912-0207602',
        'FP-1912-0207603',
        'FP-1912-0207604',
        'FP-1912-0207605',
        'FP-1912-0207606',
        'FP-1912-0207607',
        'FP-1912-0207608',
        'FP-1912-0207609',
        'FP-1912-0207610',
        'FP-1912-0207611',
        'FP-1912-0207612',
        'FP-1912-0207613',
        'FP-1912-0207614',
        'FP-1912-0207615',
        'FP-1912-0207616',
        'FP-1912-0207617',
        'FP-1912-0207618',
        'FP-1912-0207619',
        'FP-1912-0207620',
        'FP-1912-0207621',
        'FP-1912-0207622',
        'FP-1912-0207623',
        'FP-1912-0207624',
        'FP-1912-0207625',
        'FP-1912-0207626',
        'FP-1912-0207627',
        'FP-1912-0207628',
        'FP-1912-0207629',
        'FP-1912-0207630',
        'FP-1912-0207631',
        'FP-1912-0207632',
        'FP-1912-0207633',
        'FP-1912-0207634',
        'FP-1912-0207635',
        'FP-1912-0207636',
        'FP-1912-0207637',
        'FP-1912-0207638',
        'FP-1912-0207639',
        'FP-1912-0207640',
        'FP-1912-0207641',
        'FP-1912-0207642',
        'FP-1912-0207643',
        'FP-1912-0207644',
        'FP-1912-0207645',
        'FP-1912-0207646',
        'FP-1912-0207647',
        'FP-1912-0207648',
        'FP-1912-0207649',
        'FP-1912-0207650',
        'FP-1912-0207651',
        'FP-1912-0207652',
        'FP-1912-0207653',
        'FP-1912-0305478',
        'FP-1912-0305479',
        'FP-1912-0305480',
        'FP-1912-0305481',
        'FP-1912-0305482',
        'FP-1912-0305483',
        'FP-1912-0305484',
        'FP-1912-0305485',
        'FP-1912-0305486',
        'FP-1912-0305487',
        'FP-1912-0305488',
        'FP-1912-0305489',
        'FP-1912-0305490',
        'FP-1912-0305491',
        'FP-1912-0305492',
        'FP-1912-0305493',
        'FP-1912-0305494',
        'FP-1912-0305495',
        'FP-1912-0305496',
        'FP-1912-0305497',
        'FP-1912-0305498',
        'FP-1912-0305499',
        'FP-1912-0305500',
        'FP-1912-0305501',
        'FP-1912-0305502',
        'FP-1912-0305503',
        'FP-1912-0305504',
        'FP-1912-0305505',
        'FP-1912-0305506',
        'FP-1912-0305507',
        'FP-1912-0305508',
        'FP-1912-0305509',
        'FP-1912-0305510',
        'FP-1912-0305511',
        'FP-1912-0305512',
        'FP-1912-0305513',
        'FP-1912-0305514',
        'FP-1912-0305515',
        'FP-1912-0305516',
        'FP-1912-0305517',
        'FP-1912-0305518',
        'FP-1912-0305519',
        'FP-1912-0305520',
        'FP-1912-0305521',
        'FP-1912-0305522',
        'FP-1912-0305523',
        'FP-1912-0305524',
        'FP-1912-0305525',
        'FP-1912-0305526',
        'FP-1912-0305527',
        'FP-1912-0305528',
        'FP-1912-0305529',
        'FP-1912-0305530',
        'FP-1912-0305531',
        'FP-1912-0305532',
        'FP-1912-0305533',
        'FP-1912-0305534',
        'FP-1912-0305535',
        'FP-1912-0305536',
        'FP-1912-0305537',
        'FP-1912-0305538',
        'FP-1912-0305539',
        'FP-1912-0305540',
        'FP-1912-0305541',
        'FP-1912-0305542',
        'FP-1912-0405887',
        'FP-1912-0405888',
        'FP-1912-0405889',
        'FP-1912-0405890',
        'FP-1912-0405891',
        'FP-1912-0405892',
        'FP-1912-0405893',
        'FP-1912-0405894',
        'FP-1912-0405895',
        'FP-1912-0405896',
        'FP-1912-0405897',
        'FP-1912-0405898',
        'FP-1912-0405899',
        'FP-1912-0405900',
        'FP-1912-0405901',
        'FP-1912-0405902',
        'FP-1912-0405903',
        'FP-1912-0405904',
        'FP-1912-0405905',
        'FP-1912-0405906',
        'FP-1912-0405907',
        'FP-1912-0405908',
        'FP-1912-0405909',
        'FP-1912-0405910',
        'FP-1912-0405911',
        'FP-1912-0405912',
        'FP-1912-0405913',
        'FP-1912-0405914',
        'FP-1912-0405915',
        'FP-1912-0405916',
        'FP-1912-0405917',
        'FP-1912-0405918',
        'FP-1912-0405919',
        'FP-1912-0405920',
        'FP-1912-0405921',
        'FP-1912-0405922',
        'FP-1912-0405923',
        'FP-1912-0405924',
        'FP-1912-0405925',
        'FP-1912-0405926',
        'FP-1912-0405927',
        'FP-1912-0405928',
        'FP-1912-0405929',
        'FP-1912-0405930',
        'FP-1912-0405931',
        'FP-1912-0405932',
        'FP-1912-0405933',
        'FP-1912-0405934',
        'FP-1912-0405935',
        'FP-1912-0405936',
        'FP-1912-0405937',
        'FP-1912-0405938',
        'FP-1912-0405939',
        'FP-1912-0405940',
        'FP-1912-0405941',
        'FP-1912-0405942',
        'FP-1912-0405943',
        'FP-1912-0405944',
        'FP-1912-0405945',
        'FP-1912-0405946',
        'FP-1912-0405947',
        'FP-1912-0405948',
        'FP-1912-0405949',
        'FP-1912-0405950',
        'FP-1912-0405951',
        'FP-1912-0405952',
        'FP-1912-0405953',
        'FP-1912-0405954',
        'FP-1912-0405955',
        'FP-1912-0405956',
        'FP-1912-0405957',
        'FP-1912-0405958',
        'FP-1912-0405959',
        'FP-1912-0405960',
        'FP-1912-0405961',
        'FP-1912-0405962',
        'FP-1912-0405963',
        'FP-1912-0405964',
        'FP-1912-0502342',
        'FP-1912-0502343',
        'FP-1912-0502344',
        'FP-1912-0502345',
        'FP-1912-0502346',
        'FP-1912-0502347',
        'FP-1912-0502348',
        'FP-1912-0502349',
        'FP-1912-0502350',
        'FP-1912-0502351',
        'FP-1912-0502352',
        'FP-1912-0502353',
        'FP-1912-0502354',
        'FP-1912-0502355',
        'FP-1912-0502356',
        'FP-1912-0502357',
        'FP-1912-0502358',
        'FP-1912-0502359',
        'FP-1912-0502360',
        'FP-1912-0502361',
        'FP-1912-0502362',
        'FP-1912-0502363',
        'FP-1912-0502364',
        'FP-1912-0502365',
        'FP-1912-0502366',
        'FP-1912-0502367',
        'FP-1912-0502368',
        'FP-1912-0502369',
        'FP-1912-0502370',
        'FP-1912-0502371',
        'FP-1912-0502372',
        'FP-1912-0502373',
        'FP-1912-0502374',
        'FP-1912-0600768',
        'FP-1912-0600769',
        'FP-1912-0700641',
        'FP-1912-0700642',
        'FP-1912-0700643',
        'FP-1912-0700644',
        'FP-1912-0700645',
        'FP-1912-0700646',
        'FP-1912-0700647',
        'FP-1912-0700648',
        'FP-1912-0700649',
        'FP-1912-0700650',
        'FP-1912-0700651',
        'FP-1912-0700652',
        'FP-1912-0700653',
        'FP-1912-0901163',
        'FP-1912-0901164',
        'FP-1912-0901165',
        'FP-1912-0901166',
        'FP-1912-0901167',
        'FP-1912-0901168',
        'FP-1912-0901169',
        'FP-1912-0901170',
        'FP-1912-0901171',
        'FP-1912-0901172',
        'FP-1912-0901173',
        'FP-1912-0901174',
        'FP-1912-0901175',
        'FP-1912-0901176',
        'FP-1912-0901177',
        'FP-1912-0901178',
        'FP-1912-0901179',
        'FP-1912-1001436',
        'FP-1912-1001437',
        'FP-1912-1001438',
        'FP-1912-1001439',
        'FP-1912-1001440',
        'FP-1912-1001441',
        'FP-1912-1001442',
        'FP-1912-1001443',
        'FP-1912-1001444',
        'FP-1912-1001445',
        'FP-1912-1001446',
        'FP-1912-1001447',
        'FP-1912-1001448',
        'FP-1912-1001449',
        'FP-1912-1001450',
        'FP-1912-1001451',
        'FP-1912-1001452',
        'FP-1912-1001453',
        'FP-1912-1001454',
        'FP-1912-1001455',
        'FP-1912-1001456',
        'FP-1912-1101570',
        'FP-1912-1101571',
        'FP-1912-1101572',
        'FP-1912-1101573',
        'FP-1912-1200542',
        'FP-1912-1200543',
        'FP-1912-1200544',
        'FP-1912-1200545',
        'FP-1912-1200546',
        'FP-1912-1200547',
        'FP-1912-1200548',
        'FP-1912-1200549',
        'FP-1912-1200550',
        'FP-1912-1200551',
        'FP-1912-1500363',
        'FP-1912-1500364',
        'FP-1912-1500365',
        'FP-1912-1500366',
        'FP-1912-1500367',
        'FP-1912-1500368',
        'FP-1912-1500369',
        'FP-1912-1500370',
        'FP-1912-1500371',
        'FP-1912-1500372',
        'FP-1912-1500373',
        'FP-1912-1500374',
        'FP-1912-1500375',
        'FP-1912-1500376',
        'FP-1912-1500377',
        'FP-1912-1500378',
        'FP-1912-1500379',
        'FP-1912-1500380',
        'FP-1912-1700991',
        'FP-1912-1700992',
        'FP-1912-1700993',
        'FP-1912-1700994',
        'FP-1912-1700995',
        'FP-1912-1700996',
        'FP-1912-1700997',
        'FP-1912-1700998',
        'FP-1912-1700999',
        'FP-1912-1701000',
        'FP-1912-1701001',
        'FP-1912-1701002',
        'FP-1912-1701003',
        'FP-1912-1800121',
        'FP-1912-1800122',
        'FP-1912-2000161',
        'FP-1912-2000162',
        'FP-1912-2000163',
        'FP-1912-2000164',
        'FP-1912-2200339',
        'FP-1912-2200340',
        'FP-1912-2300797',
        'FP-1912-2300798',
        'FP-1912-2300799',
        'FP-1912-2300800',
        'FP-1912-2300801',
        'FP-1912-2300802',
        'FP-1912-2401091',
        'FP-1912-2401092',
        'FP-1912-2401093',
        'FP-1912-2401094',
        'FP-1912-2401095',
        'FP-1912-3102790',
        'FP-1912-3102791',
        'FP-1912-3102792',
        'FP-1912-3102793',
        'FP-1912-3102794',
        'FP-1912-3102795',
        'FP-1912-3102796',
        'FP-1912-3102797',
        'FP-1912-3102798',
        'FP-1912-3102799',
        'FP-1912-3102800',
        'FP-1912-3102801',
        'FP-1912-3102802',
        'FP-1912-3102803',
        'FP-1912-3102804',
        'FP-1912-3102805',
        'FP-1912-3102806',
        'FP-1912-3102807',
        'FP-1912-3102808',
        'FP-1912-3102809',
        'FP-1912-3102810',
        'FP-1912-3102811',
        'FP-1912-3102812',
        'FP-1912-3102813',
        'FP-1912-3102814',
        'FP-1912-3102815',
        'FP-1912-3102816',
        'FP-1912-3102817',
        'FP-1912-3102818',
        'FP-1912-3102819',
        'FP-1912-PB05824',
        'FP-1912-PB05825',
        'FP-1912-PB05826',
        'FP-1912-PB05827',
        'FP-1912-PB05828',
        'FP-1912-PB05829',
        'FP-1912-PB05781',
        'FP-1912-PB05782',
        'FP-1912-PB05783',
        'FP-1912-PB05784',
        'FP-1912-PB05785',
        'FP-1912-PB05786',
        'FP-1912-PB05787',
        'FP-1912-PB05788',
        'FP-1912-PB05789',
        'FP-1912-PB05790',
        'FP-1912-PB05791',
        'FP-1912-PB05792',
        'FP-1912-PB05793',
        'FP-1912-PB05794',
        'FP-1912-PB05795',
        'FP-1912-PB05796',
        'FP-1912-PB05797',
        'FP-1912-PB05798',
        'FP-1912-PB05799',
        'FP-1912-PB05800',
        'FP-1912-PB05801',
        'FP-1912-PB05802',
        'FP-1912-PB05803',
        'FP-1912-PB05804',
        'FP-1912-PB05805',
        'FP-1912-PB05806',
        'FP-1912-PB05807',
        'FP-1912-PB05808',
        'FP-1912-PB05809',
        'FP-1912-PB05810',
        'FP-1912-PB05811',
        'FP-1912-PB05812',
        'FP-1912-PB05813',
        'FP-1912-PB05814',
        'FP-1912-PB05815',
        'FP-1912-PB05816',
        'FP-1912-PB05817',
        'FP-1912-PB05818',
        'FP-1912-PB05819',
        'FP-1912-PB05820',
        'FP-1912-PB05821',
        'FP-1912-PB05822',
        'FP-1912-PB05823')");
        $this->load->library('fungsi');

        foreach ($query->result() as $row) {
            $i_nota = $row->i_nota;
            $i_spb = $row->i_spb;
            $i_sj = $row->i_sj;
            $i_area = $row->i_area;
            $d_dkb = $row->d_dkb;
            $i_dkb = $row->i_dkb;
            $d_nota = '2019-12-27';
            $n_spb_toplength = $row->n_spb_toplength;
            $d_sj_receive = $row->d_sj_receive;
            $d_jatuh_tempo = $row->d_jatuh_tempo;
            $dudet = $this->fungsi->dateAdd("d", $n_spb_toplength, $d_nota);

            $this->db->query("update tm_spb set d_nota = '$d_nota', d_sj = '$d_nota' where i_nota ='$i_nota' and i_spb = '$i_spb' and i_area = '$i_area'");
            $this->db->query("update tm_nota_item set d_nota = '$d_nota' where i_nota ='$i_nota' and i_sj = '$i_sj' and i_area = '$i_area'");
            if ($d_sj_receive != '') {
                // SUdah DI Receive
                $this->db->query("update tm_nota set d_nota = '$d_nota', d_jatuh_tempo = '$dudet', d_sj = '$d_nota', d_sj_receive = '$d_nota' where i_nota ='$i_nota' and i_spb = '$i_spb' and i_area = '$i_area'");
            } else {
                $this->db->query("update tm_nota set d_nota = '$d_nota', d_jatuh_tempo = '$dudet', d_sj = '$d_nota' where i_nota ='$i_nota' and i_spb = '$i_spb' and i_area = '$i_area'");
            }

            if ($d_dkb != '') {

                $this->db->query("update tm_nota set d_dkb = '$d_nota' where i_nota ='$i_nota' and i_spb = '$i_spb' and i_area = '$i_area'");
                $this->db->query("update tm_dkb_item set d_sj = '$d_nota' where i_sj ='$i_sj' and i_area = '$i_area' and i_dkb = '$i_dkb'");
                $this->db->query("update tm_dkb set d_dkb = '$d_nota' where i_area = '$i_area' and i_dkb = '$i_dkb'");
            }
            // $dudet = $this->fungsi->dateAdd("d",60,$d_spb);
            // echo $d_spb."   ".$dudet."<br>";
            // $this->db->query("update tm_spb set n_spb_toplength = '60' where i_nota ='$i_nota' and i_spb = '$i_spb'");
            // $this->db->query("update tm_nota set d_jatuh_tempo = '$dudet', n_nota_toplength = '60' where i_nota ='$i_nota' and i_spb = '$i_spb'");

        }

        // echo "berhasil";
    }

    public function pindah_saldo()
    {
        $qsebelum = $this->db->query("select i_coa, v_saldo_akhir from tm_coa_saldo where i_periode = '202002'
        and i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kec%')");

        foreach ($qsebelum->result() as $row) {
            $i_coa = $row->i_coa;
            $v_saldo_akhir = $row->v_saldo_akhir;

            $data_saldo = $this->db->query("select v_mutasi_debet, v_mutasi_kredit from tm_coa_saldo where i_coa = '$i_coa' and i_periode = '202003'");
            if ($data_saldo->num_rows() > 0) {
                $data_saldo = $data_saldo->row();
                $v_mutasi_debet = $data_saldo->v_mutasi_debet;
                $v_mutasi_kredit = $data_saldo->v_mutasi_kredit;
                $akhir = ($v_saldo_akhir + $v_mutasi_debet - $v_mutasi_kredit);

                $this->db->query("update tm_coa_saldo set v_saldo_awal = '$v_saldo_akhir', v_saldo_akhir = '$akhir' where i_coa = '$i_coa'
                and i_periode = '202003'");
            }
            // $this->db->query()
        }
        // echo 'berhasil';
    }

    public function hitung_saldo()
    {
        $data_saldo = $this->db->query("select * from tm_coa_saldo where i_periode = '202002'
        and i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kec%')");

        foreach ($data_saldo->result() as $row) {

            $i_coa = $row->i_coa;
            $v_saldo_awal = $row->v_saldo_awal;
            $v_mutasi_debet = $row->v_mutasi_debet;
            $v_mutasi_kredit = $row->v_mutasi_kredit;
            $akhir = ($v_saldo_awal + $v_mutasi_debet - $v_mutasi_kredit);

            $this->db->query("update tm_coa_saldo set v_saldo_akhir = '$akhir' where i_coa = '$i_coa'
            and i_periode = '202002'");
        }
    }

    public function cari_debet_kredit()
    {
        $data = $this->db->query("select x.i_coa, z.i_area, sum(z.debet) as debet, sum(z.kredit) as kredit from(
            select x.i_area, sum(x.v_kk) as debet, 0 as kredit from(

            select a.* from(
             select i_kb as i_kk, d_kb as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_kb as v_kk, a.i_area, d_bukti, i_coa
             from tm_kb a
             inner join tr_area on (a.i_area=tr_area.i_area)
             where a.i_periode='202002' and to_char(a.d_kb,'yyyymm')='202002'
             and a.f_debet='t' and a.f_kb_cancel='f' and a.i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kecil%')
             and a.v_kb not in (select b.v_kk as v_kb from tm_kk b where
             b.d_kk = a.d_kb and b.i_area=a.i_area and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '900-000%'
             and b.i_periode='202002')
             union all
             select i_kbank as i_kk, d_bank as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_bank as v_kk, a.i_area, d_bank as d_bukti, i_coa
             from tm_kbank a
             inner join tr_area on (a.i_area=tr_area.i_area)
             where a.i_periode='202002'
             and to_char(a.d_bank,'yyyymm')='202002'
             and a.f_debet='t' and a.f_kbank_cancel='f' and a.i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kecil%')
             and a.v_bank not in (select b.v_kk as v_kb from tm_kk b where
             b.d_kk = a.d_bank and b.i_area=a.i_area and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '110-2%'
             and b.i_periode='202002')
             union all
             select a.i_kk as i_kk, a.d_kk, a.f_debet, a.e_description, a.i_kendaraan, a.n_km, a.v_kk, a.i_area, d_bukti, i_coa
             from tm_kk a, tr_area b
             where to_char(a.d_kk,'yyyymm')='202002'
             and a.i_area=b.i_area and a.f_kk_cancel='f'
             ) as a
             order by a.i_area, a.d_kk, a.i_kk


            ) as x
            where x.f_debet = 'f' or (substr(x.i_kk,1,2) = 'KB' or substr(x.i_kk,1,2) = 'BK')
            group by x.i_area
            union all
            /* Pemisah */
            select x.i_area, 0 as debet, sum(x.v_kk) as kredit from(

                select a.* from(
                    select i_kb as i_kk, d_kb as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_kb as v_kk, a.i_area, d_bukti, i_coa
                    from tm_kb a
                    inner join tr_area on (a.i_area=tr_area.i_area)
                    where a.i_periode='202002' and to_char(a.d_kb,'yyyymm')='202002'
                    and a.f_debet='t' and a.f_kb_cancel='f' and a.i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kecil%')
                    and a.v_kb not in (select b.v_kk as v_kb from tm_kk b where
                    b.d_kk = a.d_kb and b.i_area=a.i_area and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '900-000%'
                    and b.i_periode='202002')
                    union all
                    select i_kbank as i_kk, d_bank as d_kk, f_debet, e_description, '' as i_kendaraan, 0 as n_km, v_bank as v_kk, a.i_area, d_bank as d_bukti, i_coa
                    from tm_kbank a
                    inner join tr_area on (a.i_area=tr_area.i_area)
                    where a.i_periode='202002'
                    and to_char(a.d_bank,'yyyymm')='202002'
                    and a.f_debet='t' and a.f_kbank_cancel='f' and a.i_coa in(select i_coa from tr_coa where i_area <> '' and e_coa_name like '%Kas Kecil%')
                    and a.v_bank not in (select b.v_kk as v_kb from tm_kk b where
                    b.d_kk = a.d_bank and b.i_area=a.i_area and b.f_kk_cancel='f' and b.f_debet='f' and b.i_coa like '110-2%'
                    and b.i_periode='202002')
                    union all
                    select a.i_kk as i_kk, a.d_kk, a.f_debet, a.e_description, a.i_kendaraan, a.n_km, a.v_kk, a.i_area, d_bukti, i_coa
                    from tm_kk a, tr_area b
                    where to_char(a.d_kk,'yyyymm')='202002'
                    and a.i_area=b.i_area and a.f_kk_cancel='f'
                    ) as a
                    order by a.i_area, a.d_kk, a.i_kk

            ) as x
            where x.f_debet = 't'
            group by x.i_area
            ) as z
            inner join tr_coa x on(z.i_area = x.i_area and x.e_coa_name like '%Kas Keci%')
            group by z.i_area, x.i_coa");

        foreach ($data->result() as $row) {
            $i_coa = $row->i_coa;
            $v_mutasi_debet = $row->debet;
            $v_mutasi_kredit = $row->kredit;

            $this->db->query("update tm_coa_saldo set v_mutasi_debet = '$v_mutasi_debet', v_mutasi_kredit = '$v_mutasi_kredit'
                where i_coa = '$i_coa'
                and i_periode = '202002'");
        }
    }

    public function update_so()
    {
        $periode = '202004';
        $i_store = 'AA';
        $i_area = '00';

        $i_stockopname = $this->db->query("select i_stockopname_akhir from tm_mutasi_header where e_mutasi_periode = '$periode' and i_store = '$i_store'")->row()->i_stockopname_akhir;

        $data_baru = $this->db->query("select i_product, sum(n_deliver) as n_deliver from tm_nota_item where
        i_sj in('SJ-2001-000358',
        'SJ-2001-000359',
        'SJ-2001-000360',
        'SJ-2001-000361',
        'SJ-2001-000362',
        'SJ-2001-000363',
        'SJ-2001-000364',
        'SJ-2001-000365') and i_area = '32'
        group by i_product");

        foreach ($data_baru->result() as $row) {
            $i_product = $row->i_product;
            $n_deliver = $row->n_deliver;

            $cek_barang = $this->db->query("select * from tm_stockopname_item where i_stockopname = '$i_stockopname' and i_area ='$i_area' AND i_store = '$i_store'
            AND i_product = '$i_product' and i_product_grade = 'A' ");

            if ($cek_barang->num_rows() > 0) {
                $n_stockopname = $cek_barang->row()->n_stockopname;
                $stok_baru = $n_stockopname - $n_deliver;

                $this->db->query("update tm_stockopname_item set n_stockopname = '$stok_baru' where i_stockopname = '$i_stockopname' and i_area ='$i_area' AND i_store = '$i_store'
                AND i_product = '$i_product' and i_product_grade = 'A'");
            }
            // else {
            //     $data_barang = $this->db->query("select * from tm_stockopname_item where i_product = '$i_product' order by i_stockopname desc limit 1")->row();

            //     $d_stockopname = $this->db->query("select * from tm_stockopname where i_stockopname = '$i_stockopname' and i_area ='$i_area' AND i_store = '$i_store' ")->row()->d_stockopname;

            //     $data_insert = array(
            //         'i_stockopname' => $i_stockopname,
            //         'i_product' => $i_product,
            //         'i_product_grade' => 'A',
            //         'e_product_name' => $data_barang->e_product_name,
            //         'n_stockopname' => $n_deliver,
            //         'i_product_motif' => $data_barang->i_product_motif,
            //         'i_store' => $i_store,
            //         'i_store_location' => '01',
            //         'i_store_locationbin' => '00',
            //         'd_stockopname' => $d_stockopname,
            //         'i_area' => $i_area,
            //         'e_mutasi_periode' => $periode,
            //         'n_item_no' => '99',

            //     );

            //     $this->db->insert('tm_stockopname_item', $data_insert);

            // }
        }
    }

    public function harga()
    {
        $data = $this->db->query("select * from tr_product_price where i_price_group = '01'");

        foreach ($data->result() as $row) {

            $vproductretail = $row->v_product_retail;
            $diskon = ($vproductretail * 10) / 100;
            $row->v_product_retail = (int) $vproductretail - (int) $diskon;

            $data = array(
                'i_product' => $row->i_product,
                'i_product_grade' => $row->i_product_grade,
                'i_price_group' => 'G1',
                'e_product_name' => $row->e_product_name,
                'v_product_retail' => $row->v_product_retail,
                'v_product_mill' => $row->v_product_mill,
                'd_product_priceentry' => $row->d_product_priceentry,
                'd_product_priceupdate' => $row->d_product_priceupdate,
                'n_product_margin' => $row->n_product_margin,
            );

            $this->db->insert('tr_product_price', $data);
        }
    }

    public function update_saldo_coa()
    {
        date_default_timezone_set("Asia/Jakarta");

        $tgl = date("d-m-Y");
        $periodex   = date('Ym', strtotime('-1 month', strtotime(date('Y-m'))));
        $periodey   = date('Ym');

        //$periodex   = date('Ym', strtotime('-1 month', strtotime('2021-02-28')));
        //$periodey   = '202102';

        $this->db->trans_begin();
        /* CLOSING KAS BESAR & BANK */
        // Update Debet Kredit Kas Besar Karena Sering berbeda saldonya ^_^ #Wahyu 2020-07-07
        $saldo_kb = $this->db->query("select sum(z.debet) as debet, sum(z.kredit) as kredit from(
            select x.d_kb, x.d_bukti, c.i_rv, d.i_pv, x.i_kb, x.e_description, x.i_area||' - '||b.e_area_name as i_area, x.i_coa, sum(x.debet) as debet, sum(x.kredit) as kredit from(
            select d_kb, d_bukti, i_kb, e_description, i_area, i_coa, e_coa_name, 0 as debet, v_kb as kredit from tm_kb where
            to_char(d_kb,'yyyymm')='$periodex' and f_debet = 't' and f_kb_cancel = 'f'
            union all
            select d_kb, d_bukti, i_kb, e_description, i_area, i_coa, e_coa_name, v_kb as debet,  0 as kredit from tm_kb where
            to_char(d_kb,'yyyymm')='$periodex' and f_debet = 'f' and f_kb_cancel = 'f'
            ) as x
            inner join tr_area b on(x.i_area = b.i_area)
            left join tm_rv_item c on(x.i_kb = c.i_kk and x.i_area = c.i_area_kb)
            left join tm_pv_item d on(x.i_kb = d.i_kk and x.i_area = d.i_area_kb)
            group by x.d_kb, x.i_kb, x.e_description, x.i_area, x.i_coa, b.e_area_name, x.d_bukti, c.i_rv, d.i_pv
            order by x.d_kb
            ) as z");

        if ($saldo_kb->num_rows() > 0) {
            $data_coa_saldo = $this->db->query("select i_coa, v_saldo_awal from tm_coa_saldo where i_periode = '$periodex' and e_coa_name like '%Kas Besar%'")->row();
            $saldo_kb = $saldo_kb->row();
            $kb_coa = $data_coa_saldo->i_coa;
            $kb_awal = $data_coa_saldo->v_saldo_awal;
            $kb_debet = $saldo_kb->debet;
            $kb_kredit = $saldo_kb->kredit;
            $kb_akhir = ((int) $kb_awal + (int) $kb_debet) - (int) $kb_kredit;

            $this->db->query("update tm_coa_saldo set v_mutasi_debet = '$kb_debet', v_mutasi_kredit = '$kb_kredit', v_saldo_akhir = '$kb_akhir' where i_periode = '$periodex'
            and i_coa = '$kb_coa' and e_coa_name like '%Kas Besar%'");
            //echo $kb_debet."|".$kb_kredit."|".$kb_akhir."|".$periodex."|".$kb_coa."<br>";
        }

        ###### closing
        $this->db->select(" * from tm_coa_saldo where i_periode = '$periodex' order by i_coa", false);
        $que = $this->db->get();
        if ($que->num_rows() > 0) {
            foreach ($que->result() as $ro) {
                /* 1. MULAI UPDATE DULU SALDO DEBET KREDIT SAHIR NYA */
                /* UPDATE DEBET KREDIT SAHIR BANK */
                $data1  =   $this->db->query("  SELECT
                                                    i_coa_bank,
                                                    sum(sawal) AS sawal,
                                                    sum(debet) AS debet,
                                                    sum(kredit) AS kredit,
                                                    (sum(sawal)+sum(debet)-sum(kredit)) AS sahir
                                                FROM
                                                    (
                                                        SELECT
                                                            i_coa AS i_coa_bank,
                                                            i_periode,
                                                            sum(v_saldo_awal) AS sawal,
                                                            0 AS debet,
                                                            0 AS kredit
                                                        FROM
                                                            tm_coa_saldo
                                                        WHERE
                                                            i_coa LIKE '110-20%'
                                                        GROUP BY
                                                            i_coa,
                                                            i_periode
                                                    UNION ALL
                                                        SELECT
                                                            i_coa_bank,
                                                            i_periode,
                                                            0 AS sawal,
                                                            sum(v_bank) AS debet,
                                                            0 AS kredit
                                                        FROM
                                                            tm_kbank
                                                        WHERE
                                                            f_debet = 'f'
                                                            AND f_kbank_cancel = 'f'
                                                        GROUP BY
                                                            i_coa_bank,
                                                            i_periode
                                                    UNION ALL
                                                        SELECT
                                                            i_coa_bank,
                                                            i_periode,
                                                            0 AS sawal,
                                                            0 AS debet,
                                                            sum(v_bank) AS kredit
                                                        FROM
                                                            tm_kbank
                                                        WHERE
                                                            f_debet = 't'
                                                            AND f_kbank_cancel = 'f'
                                                        GROUP BY
                                                            i_coa_bank,
                                                            i_periode ) AS a
                                                WHERE 
                                                    a.i_periode = '$periodex'
                                                GROUP BY
                                                    i_coa_bank");
                foreach ($data1->result() as $row) {
                    $this->db->query("  UPDATE
                                            tm_coa_saldo
                                        SET
                                            v_mutasi_debet  = '$row->debet',
                                            v_mutasi_kredit = '$row->kredit',
                                            v_saldo_akhir   = '$row->sahir'
                                        WHERE
                                            i_coa = '$row->i_coa_bank'
                                            AND i_periode = '$periodex' ");
                }
                /* END UPDATE DEBET KREDIT SAHIR BANK */

                /* UPDATE DEBET KREDIT SAHIR KAS KECIL */
                $data2  =   $this->db->query("  SELECT
                                                    a.i_coa,
                                                    e_coa_name ,
                                                    sum(sawal) AS sawal,
                                                    sum(debet) AS debet,
                                                    sum(kredit) AS kredit,
                                                    (sum(sawal)+ sum(debet)-sum(kredit)) AS sahir
                                                FROM
                                                    (
                                                    SELECT
                                                        i_coa,
                                                        i_periode,
                                                        sum(v_saldo_awal) AS sawal,
                                                        0 AS debet,
                                                        0 AS kredit
                                                    FROM
                                                        tm_coa_saldo
                                                    WHERE
                                                        i_coa LIKE '110-12%'
                                                    GROUP BY
                                                        i_coa,
                                                        i_periode
                                                UNION ALL
                                                    SELECT
                                                        b.i_coa,
                                                        i_periode,
                                                        0 AS sawal,
                                                        sum(v_kk) AS debet,
                                                        0 AS kredit
                                                    FROM
                                                        tm_kk a
                                                    INNER JOIN tr_coa b ON
                                                        (a.i_area = b.i_area)
                                                    WHERE
                                                        f_debet = 'f'
                                                        AND f_kk_cancel = 'f'
                                                    GROUP BY
                                                        b.i_coa,
                                                        i_periode
                                                UNION ALL
                                                    SELECT
                                                        b.i_coa,
                                                        i_periode,
                                                        0 AS sawal,
                                                        0 AS debet,
                                                        sum(v_kk) AS kredit
                                                    FROM
                                                        tm_kk a
                                                    INNER JOIN tr_coa b ON
                                                        (a.i_area = b.i_area)
                                                    WHERE
                                                        f_debet = 't'
                                                        AND f_kk_cancel = 'f'
                                                    GROUP BY
                                                        b.i_coa,
                                                        i_periode ) AS a
                                                        INNER JOIN tr_coa b ON
                                                        (a.i_coa = b.i_coa)
                                                WHERE
                                                    a.i_periode = '$periodex'
                                                GROUP BY
                                                    a.i_coa,e_coa_name
                                                ORDER BY a.i_coa ");
                foreach ($data2->result() as $row) {
                    $this->db->query("  UPDATE
                                            tm_coa_saldo
                                        SET
                                            v_mutasi_debet  = '$row->debet',
                                            v_mutasi_kredit = '$row->kredit',
                                            v_saldo_akhir   = '$row->sahir'
                                        WHERE
                                            i_coa = '$row->i_coa'
                                            AND i_periode = '$periodex' ");
                }
                /* END DEBET KREDIT SAHIR KAS KECIL */
                /* NAH QUERY UPDATE SALDO DEBET KREDIT SAHIR NYA SAMPE SINI*/

                /* 2. BARU PINDAH SALDO DISINI */
                $this->db->select("v_saldo_akhir as jum from tm_coa_saldo where i_periode='$periodex' and i_coa='$ro->i_coa'", false);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    foreach ($query->result() as $row) {
                        if ($row->jum == null) $row->jum = 0;
                        $quer     = $this->db->query("SELECT current_timestamp as c");
                        $saldo    = $row->jum;
                        $riw       = $quer->row();
                        $update    = $riw->c;
                        $coa = $ro->i_coa;

                        $qu = $this->db->query("select e_coa_name from tr_coa where i_coa like '$coa'");
                        if ($qu->num_rows() > 0) {
                            foreach ($qu->result() as $tes) {
                                $coaname = $tes->e_coa_name;
                            }
                            $qu->free_result();
                        }
                        #select data periode yang sudah ada
                        $this->db->select("* from tm_coa_saldo where i_periode='$periodey' and i_coa='$ro->i_coa'", false);
                        $qu = $this->db->get();
                        if ($qu->num_rows() == 0) {
                            $quer     = $this->db->query("SELECT current_timestamp as c");
                            $row       = $quer->row();
                            $entry    = $row->c;
                            $coa = $ro->i_coa;
                            $user = $this->session->userdata('user_id');
                            #update saldo akhir=saldo+debet-kredit (yang di update saldo awal, saldo akhir)
                            $this->db->query(" insert into tm_coa_saldo 
                                            (i_periode, i_coa, v_saldo_awal, v_mutasi_debet, v_mutasi_kredit, v_saldo_akhir, d_entry,
                                            i_entry, e_coa_name)
                                            values
                                            ('$periodey','$coa',$saldo,0,0,$saldo,'$entry','$user','$coaname')");
                        } else {
                            $quer     = $this->db->query("SELECT current_timestamp as c");
                            $row       = $quer->row();
                            $update    = $row->c;
                            $coa = $ro->i_coa;
                            $user = $this->session->userdata('user_id');
                            $this->db->query(" update tm_coa_saldo set v_saldo_awal=$saldo, v_saldo_akhir=$saldo+v_mutasi_debet-v_mutasi_kredit, 
                                                d_update='$update', i_update='$user' where i_periode='$periodey' and i_coa='$coa'");
                        }
                    }/* END FOREACH */
                }/* END BACA DATA COA SALDO */
            }
        }

        if (($this->db->trans_status() === false)) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();

            $id         = "SYSTEM";
            $ip_address = 'kosong';

            $query = pg_query("SELECT current_timestamp as c");
            while ($row = pg_fetch_assoc($query)) {
                $now = $row['c'];
            }

            $pesan = 'Update Saldo COA Otomatis ' . $tgl;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now, $pesan);
        }
    } //END closing_kasbank

    public function closing_kasbank()
    {
        date_default_timezone_set("Asia/Jakarta");

        /* CLOSING PERIODE KAS BANK */
        $hariini  = date('Y-m-d');

        // * AMBIL TANGGAL CLOSING sudah dari tabel closing kas bank berdasarkan info dari FS
        $get      = $this->db->query(" SELECT d_closing_time, i_periode FROM tm_closing_kas_bank ")->row();

        // PENGKONDISIAN KALAU ENGGA ADA PERUBAHAN TGL CLOSING DEFAULTNYA TANGGAL 3 (21 OKT 2022)
        // DI CRONTAB DISETTING RUN JAM 03:13 - 03:25 (UPDATE 04 JUL 2023)
        if (date('Ym', strtotime($get->d_closing_time)) == date('Ym')) {
            $closingdate = $get->d_closing_time;
        } else {
            $closingdate = date('Y-m-04');
            $this->db->query(" UPDATE tm_closing_kas_bank SET d_closing_time = '$closingdate' "); // UPDATE SETIAP GANTI BULAN
        }

        $lastperiode    = $get->i_periode;

        //    if (date('d', strtotime($hariini))==$closingdate) {
        if ($hariini == $closingdate) {
            $tanggal1   = date('Y-m') . '-01';
            $hariini    = date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))));
            $iperiode   = date('Ym');

            $this->db->trans_begin();

            $data = $this->db->query(" SELECT * FROM tm_closing_kas_bank ");
            if ($data->num_rows() > 0) {
                $this->db->query("
                    UPDATE 
                        tm_closing_kas_bank
                    SET 
                        d_closing_kb		= '$hariini',
                        d_open_kb			= '$tanggal1',
                        d_closing_kbin		= '$hariini',
                        d_open_kbin			= '$tanggal1',
                        d_closing_kbank		= '$hariini',
                        d_open_kbank		= '$tanggal1',
                        d_closing_kbankin	= '$hariini',
                        d_open_kbankin		= '$tanggal1',
                        d_closing_kk		= '$hariini',
                        d_open_kk			= '$tanggal1',
                        d_closing_kkin		= '$hariini',
                        d_open_kkin			= '$tanggal1',
                        i_periode			= '$iperiode'
                ");
            } else {
                $this->db->query("  INSERT INTO tm_closing_kas_bank
                                    VALUES('$hariini','$tanggal1','$hariini','$tanggal1','$hariini','$tanggal1'
                                    ,'$hariini','$tanggal1','$hariini','$tanggal1','$hariini','$tanggal1','$hariini','$iperiode') ");
            }
            /* END  CLOSING PERIODE KAS BESAR & BANK */

            if (($this->db->trans_status() === false)) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();

                $this->logger->writenew('Closing Periode Kas Bank Otomatis : ' . $hariini);

                echo "BERHASIL CLOSING KAS BANK!";
            }
        } else {
            $th = substr($lastperiode, 0, 4);
            $bl = substr($lastperiode, 4, 2);

            $tanggal1 = $th . "-" . $bl . "-01";
            // $hariini1 = date('Y-m-03', strtotime('-1 month', strtotime($closingdate)));
            $hariini1 = date('Y-m-03');

            $this->db->trans_begin();

            $data = $this->db->query(" SELECT * FROM tm_closing_kas_bank ");
            if ($data->num_rows() > 0) {
                $this->db->query("
                    UPDATE 
                        tm_closing_kas_bank
                    SET 
                        d_closing_kb		= '$hariini1',
                        d_open_kb			= '$tanggal1',
                        d_closing_kbin		= '$hariini1',
                        d_open_kbin			= '$tanggal1',
                        d_closing_kbank		= '$hariini1',
                        d_open_kbank		= '$tanggal1',
                        d_closing_kbankin	= '$hariini1',
                        d_open_kbankin		= '$tanggal1',
                        d_closing_kk		= '$hariini1',
                        d_open_kk			= '$tanggal1',
                        d_closing_kkin		= '$hariini1',
                        d_open_kkin			= '$tanggal1'
                ");
            }

            // UPDATE tm_periode
            $this->db->query(" UPDATE tm_periode SET i_periode = '$lastperiode' ", FALSE);

            if (($this->db->trans_status() === false)) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();

                $this->logger->writenew('Refresh Tanggal Open Kas Bank Otomatis : ' . $hariini);

                echo "BERHASIL REFRESH TANGGAL OPEN!";
            }
        }
    }

    public function pindah_mutasi_saldoawal()
    {
        date_default_timezone_set("Asia/Jakarta");
        $tgl = date("d-m-Y");

        if (date('d', strtotime($tgl)) == '01') {
            $periode        = date('Ym');
            $periodekemarin = date('Ym', strtotime('-1 month', strtotime($periode)));

            $this->db->trans_begin();

            $this->db->query(" delete from tm_mutasi_saldoawal where e_mutasi_periode='$periode'");

            /**************SALDO AWAL STOK BUAT GUDANG CABANG*******************/
            $data1 = $this->db->query(" SELECT i_store, i_store_location, i_store_locationbin, '$periode' as e_mutasi_periode, i_product, i_product_grade, i_product_motif, 
                                        n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_awal  
                                        from f_mutasi_stock_daerah_all_saldoakhir('$periodekemarin')");
            foreach ($data1->result() as $row) {
                $data1 = array(
                    'i_store'               => $row->i_store,
                    'i_store_location'      => $row->i_store_location,
                    'i_store_locationbin'   => $row->i_store_locationbin,
                    'e_mutasi_periode'      => $row->e_mutasi_periode,
                    'i_product'             => $row->i_product,
                    'i_product_grade'       => $row->i_product_grade,
                    'i_product_motif'       => $row->i_product_motif,
                    'n_saldo_awal'          => $row->n_saldo_awal,
                );
                $this->db->insert('tm_mutasi_saldoawal', $data1);
            }

            /**************SALDO AWAL STOK BUAT GUDANG PUSAT*******************/
            $data2 = $this->db->query(" SELECT i_store, i_store_location, i_store_locationbin, '$periode' as e_mutasi_periode, i_product, i_product_grade, i_product_motif, 
                                        n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_awal  
                                        from f_mutasi_stock_pusat_saldoakhir('$periodekemarin')");

            foreach ($data2->result() as $row) {
                $data2 = array(
                    'i_store'               => $row->i_store,
                    'i_store_location'      => $row->i_store_location,
                    'i_store_locationbin'   => $row->i_store_locationbin,
                    'e_mutasi_periode'      => $row->e_mutasi_periode,
                    'i_product'             => $row->i_product,
                    'i_product_grade'       => $row->i_product_grade,
                    'i_product_motif'       => $row->i_product_motif,
                    'n_saldo_awal'          => $row->n_saldo_awal,
                );
                $this->db->insert('tm_mutasi_saldoawal', $data2);
            }

            if (($this->db->trans_status() === false)) {
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();

                $id         = "SYSTEM";
                $ip_address = 'kosong';

                $query = pg_query("SELECT current_timestamp as c");
                while ($row = pg_fetch_assoc($query)) {
                    $now = $row['c'];
                }

                $pesan = 'Update Mutasi Saldo Awal Otomatis ' . $tgl;
                $this->load->model('logger');
                $this->logger->write($id, $ip_address, $now, $pesan);
            }
        }
    }
    public function update_saldo_piutang()
    {
        date_default_timezone_set("Asia/Jakarta");
        $tgl = date("d-m-Y");

        $this->db->trans_begin();

        $query = $this->db->query(" SELECT i_area, i_customer, e_customer_name, v_flapond, piutang, v_saldo FROM v_saldo_piutang ");

        foreach ($query->result() as $row) {
            if ($row->v_saldo == null) {
                $row->v_saldo = 0;
            }
            $this->db->query("  UPDATE
                                    tr_customer_groupar
                                SET
                                    v_saldo = '$row->v_saldo'
                                WHERE
                                    i_customer = '$row->i_customer' ");
        }

        if (($this->db->trans_status() === false)) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();

            $id         = "SYSTEM";
            $ip_address = 'kosong';

            $query = pg_query("SELECT current_timestamp as c");
            while ($row = pg_fetch_assoc($query)) {
                $now = $row['c'];
            }

            $pesan = 'Update Saldo Piutang Otomatis ' . $tgl;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now, $pesan);
        }
    }
    public function getmenu()
    {
        date_default_timezone_set("Asia/Jakarta");

        $no = 1;

        $data = $this->db->query(" SELECT * FROM tm_user ORDER BY 1 ");
        foreach ($data->result() as $row) {
            $menu0 = $row->f_menu0;
            $menu1 = $row->f_menu1;
            $menu2 = $row->f_menu2;
            $menu3 = $row->f_menu3;
            $menu4 = $row->f_menu4;
            $menu5 = $row->f_menu5;
            $menu6 = $row->f_menu6;
            $menu7 = $row->f_menu7;
            $menu8 = $row->f_menu8;
            $menu9 = $row->f_menu9;
            $menu10 = $row->f_menu10;
            $menu11 = $row->f_menu11;
            $menu12 = $row->f_menu12;
            $menu13 = $row->f_menu13;
            $menu14 = $row->f_menu14;
            $menu15 = $row->f_menu15;
            $menu16 = $row->f_menu16;
            $menu17 = $row->f_menu17;
            $menu18 = $row->f_menu18;
            $menu19 = $row->f_menu19;
            $menu20 = $row->f_menu20;
            $menu21 = $row->f_menu21;
            $menu22 = $row->f_menu22;
            $menu23 = $row->f_menu23;
            $menu24 = $row->f_menu24;
            $menu25 = $row->f_menu25;
            $menu26 = $row->f_menu26;
            $menu27 = $row->f_menu27;
            $menu28 = $row->f_menu28;
            $menu29 = $row->f_menu29;
            $menu30 = $row->f_menu30;
            $menu31 = $row->f_menu31;
            $menu32 = $row->f_menu32;
            $menu33 = $row->f_menu33;
            $menu34 = $row->f_menu34;
            $menu35 = $row->f_menu35;
            $menu36 = $row->f_menu36;
            $menu37 = $row->f_menu37;
            $menu38 = $row->f_menu38;
            $menu39 = $row->f_menu39;
            $menu40 = $row->f_menu40;
            $menu41 = $row->f_menu41;
            $menu42 = $row->f_menu42;
            $menu43 = $row->f_menu43;
            $menu44 = $row->f_menu44;
            $menu45 = $row->f_menu45;
            $menu46 = $row->f_menu46;
            $menu47 = $row->f_menu47;
            $menu48 = $row->f_menu48;
            $menu49 = $row->f_menu49;
            $menu50 = $row->f_menu50;
            $menu51 = $row->f_menu51;
            $menu52 = $row->f_menu52;
            $menu53 = $row->f_menu53;
            $menu54 = $row->f_menu54;
            $menu55 = $row->f_menu55;
            $menu56 = $row->f_menu56;
            $menu57 = $row->f_menu57;
            $menu58 = $row->f_menu58;
            $menu59 = $row->f_menu59;
            $menu60 = $row->f_menu60;
            $menu61 = $row->f_menu61;
            $menu62 = $row->f_menu62;
            $menu63 = $row->f_menu63;
            $menu64 = $row->f_menu64;
            $menu65 = $row->f_menu65;
            $menu65 = $row->f_menu65;
            $menu66 = $row->f_menu66;
            $menu67 = $row->f_menu67;
            $menu68 = $row->f_menu68;
            $menu69 = $row->f_menu69;
            $menu70 = $row->f_menu70;
            $menu71 = $row->f_menu71;
            $menu72 = $row->f_menu72;
            $menu73 = $row->f_menu73;
            $menu74 = $row->f_menu74;
            $menu75 = $row->f_menu75;
            $menu76 = $row->f_menu76;
            $menu77 = $row->f_menu77;
            $menu78 = $row->f_menu78;
            $menu79 = $row->f_menu79;
            $menu80 = $row->f_menu80;
            $menu81 = $row->f_menu81;
            $menu82 = $row->f_menu82;
            $menu83 = $row->f_menu83;
            $menu84 = $row->f_menu84;
            $menu85 = $row->f_menu85;
            $menu86 = $row->f_menu86;
            $menu87 = $row->f_menu87;
            $menu88 = $row->f_menu88;
            $menu89 = $row->f_menu89;
            $menu90 = $row->f_menu90;
            $menu91 = $row->f_menu91;
            $menu92 = $row->f_menu92;
            $menu93 = $row->f_menu93;
            $menu93 = $row->f_menu93;
            $menu94 = $row->f_menu94;
            $menu95 = $row->f_menu95;
            $menu96 = $row->f_menu96;
            $menu97 = $row->f_menu97;
            $menu98 = $row->f_menu98;
            $menu99 = $row->f_menu99;
            $menu100 = $row->f_menu100;
            $menu101 = $row->f_menu101;
            $menu102 = $row->f_menu102;
            $menu103 = $row->f_menu103;
            $menu104 = $row->f_menu104;
            $menu105 = $row->f_menu105;
            $menu106 = $row->f_menu106;
            $menu107 = $row->f_menu107;
            $menu108 = $row->f_menu108;
            $menu109 = $row->f_menu109;
            $menu110 = $row->f_menu110;
            $menu111 = $row->f_menu111;
            $menu112 = $row->f_menu112;
            $menu113 = $row->f_menu113;
            $menu114 = $row->f_menu114;
            $menu115 = $row->f_menu115;
            $menu116 = $row->f_menu116;
            $menu117 = $row->f_menu117;
            $menu118 = $row->f_menu118;
            $menu119 = $row->f_menu119;
            $menu120 = $row->f_menu120;
            $menu121 = $row->f_menu121;
            $menu122 = $row->f_menu122;
            $menu123 = $row->f_menu123;
            $menu124 = $row->f_menu124;
            $menu125 = $row->f_menu125;
            $menu126 = $row->f_menu126;
            $menu127 = $row->f_menu127;
            $menu128 = $row->f_menu128;
            $menu129 = $row->f_menu129;
            $menu130 = $row->f_menu130;
            $menu131 = $row->f_menu131;
            $menu132 = $row->f_menu132;
            $menu133 = $row->f_menu133;
            $menu134 = $row->f_menu134;
            $menu135 = $row->f_menu135;
            $menu136 = $row->f_menu136;
            $menu137 = $row->f_menu137;
            $menu138 = $row->f_menu138;
            $menu139 = $row->f_menu139;
            $menu140 = $row->f_menu140;
            $menu141 = $row->f_menu141;
            $menu142 = $row->f_menu142;
            $menu143 = $row->f_menu143;
            $menu144 = $row->f_menu144;
            $menu145 = $row->f_menu145;
            $menu146 = $row->f_menu146;
            $menu147 = $row->f_menu147;
            $menu148 = $row->f_menu148;
            $menu149 = $row->f_menu149;
            $menu150 = $row->f_menu150;
            $menu151 = $row->f_menu151;
            $menu152 = $row->f_menu152;
            $menu153 = $row->f_menu153;
            $menu154 = $row->f_menu154;
            $menu155 = $row->f_menu155;
            $menu156 = $row->f_menu156;
            $menu157 = $row->f_menu157;
            $menu158 = $row->f_menu158;
            $menu159 = $row->f_menu159;
            $menu160 = $row->f_menu160;
            $menu161 = $row->f_menu161;
            $menu162 = $row->f_menu162;
            $menu163 = $row->f_menu163;
            $menu164 = $row->f_menu164;
            $menu165 = $row->f_menu165;
            $menu166 = $row->f_menu166;
            $menu167 = $row->f_menu167;
            $menu168 = $row->f_menu168;
            $menu169 = $row->f_menu169;
            $menu170 = $row->f_menu170;
            $menu171 = $row->f_menu171;
            $menu172 = $row->f_menu172;
            $menu173 = $row->f_menu173;
            $menu174 = $row->f_menu174;
            $menu175 = $row->f_menu175;
            $menu176 = $row->f_menu176;
            $menu177 = $row->f_menu177;
            $menu178 = $row->f_menu178;
            $menu179 = $row->f_menu179;
            $menu180 = $row->f_menu180;
            $menu181 = $row->f_menu181;
            $menu182 = $row->f_menu182;
            $menu183 = $row->f_menu183;
            $menu184 = $row->f_menu184;
            $menu185 = $row->f_menu185;
            $menu186 = $row->f_menu186;
            $menu187 = $row->f_menu187;
            $menu188 = $row->f_menu188;
            $menu189 = $row->f_menu189;
            $menu190 = $row->f_menu190;
            $menu191 = $row->f_menu191;
            $menu192 = $row->f_menu192;
            $menu193 = $row->f_menu193;
            $menu194 = $row->f_menu194;
            $menu195 = $row->f_menu195;
            $menu196 = $row->f_menu196;
            $menu197 = $row->f_menu197;
            $menu198 = $row->f_menu198;
            $menu199 = $row->f_menu199;
            $menu200 = $row->f_menu200;
            $menu201 = $row->f_menu201;
            $menu202 = $row->f_menu202;
            $menu203 = $row->f_menu203;
            $menu204 = $row->f_menu204;
            $menu205 = $row->f_menu205;
            $menu206 = $row->f_menu206;
            $menu207 = $row->f_menu207;
            $menu208 = $row->f_menu208;
            $menu209 = $row->f_menu209;
            $menu210 = $row->f_menu210;
            $menu211 = $row->f_menu211;
            $menu212 = $row->f_menu212;
            $menu213 = $row->f_menu213;
            $menu214 = $row->f_menu214;
            $menu215 = $row->f_menu215;
            $menu216 = $row->f_menu216;
            $menu217 = $row->f_menu217;
            $menu218 = $row->f_menu218;
            $menu219 = $row->f_menu219;
            $menu220 = $row->f_menu220;
            $menu221 = $row->f_menu221;
            $menu222 = $row->f_menu222;
            $menu223 = $row->f_menu223;
            $menu224 = $row->f_menu224;
            $menu225 = $row->f_menu225;
            $menu226 = $row->f_menu226;
            $menu227 = $row->f_menu227;
            $menu228 = $row->f_menu228;
            $menu229 = $row->f_menu229;
            $menu230 = $row->f_menu230;
            $menu231 = $row->f_menu231;
            $menu232 = $row->f_menu232;
            $menu233 = $row->f_menu233;
            $menu234 = $row->f_menu234;
            $menu235 = $row->f_menu235;
            $menu236 = $row->f_menu236;
            $menu237 = $row->f_menu237;
            $menu238 = $row->f_menu238;
            $menu239 = $row->f_menu239;
            $menu240 = $row->f_menu240;
            $menu241 = $row->f_menu241;
            $menu242 = $row->f_menu242;
            $menu243 = $row->f_menu243;
            $menu244 = $row->f_menu244;
            $menu245 = $row->f_menu245;
            $menu246 = $row->f_menu246;
            $menu247 = $row->f_menu247;
            $menu248 = $row->f_menu248;
            $menu249 = $row->f_menu249;
            $menu250 = $row->f_menu250;
            $menu251 = $row->f_menu251;
            $menu252 = $row->f_menu252;
            $menu253 = $row->f_menu253;
            $menu254 = $row->f_menu254;
            $menu255 = $row->f_menu255;
            $menu256 = $row->f_menu256;
            $menu257 = $row->f_menu257;
            $menu258 = $row->f_menu258;
            $menu259 = $row->f_menu259;
            $menu260 = $row->f_menu260;
            $menu261 = $row->f_menu261;
            $menu262 = $row->f_menu262;
            $menu263 = $row->f_menu263;
            $menu264 = $row->f_menu264;
            $menu265 = $row->f_menu265;
            $menu266 = $row->f_menu266;
            $menu267 = $row->f_menu267;
            $menu268 = $row->f_menu268;
            $menu269 = $row->f_menu269;
            $menu270 = $row->f_menu270;
            $menu271 = $row->f_menu271;
            $menu272 = $row->f_menu272;
            $menu273 = $row->f_menu273;
            $menu274 = $row->f_menu274;
            $menu275 = $row->f_menu275;
            $menu276 = $row->f_menu276;
            $menu277 = $row->f_menu277;
            $menu278 = $row->f_menu278;
            $menu279 = $row->f_menu279;
            $menu280 = $row->f_menu280;
            $menu281 = $row->f_menu281;
            $menu282 = $row->f_menu282;
            $menu283 = $row->f_menu283;
            $menu284 = $row->f_menu284;
            $menu285 = $row->f_menu285;
            $menu286 = $row->f_menu286;
            $menu287 = $row->f_menu287;
            $menu288 = $row->f_menu288;
            $menu289 = $row->f_menu289;
            $menu290 = $row->f_menu290;
            $menu291 = $row->f_menu291;
            $menu292 = $row->f_menu292;
            $menu293 = $row->f_menu293;
            $menu294 = $row->f_menu294;
            $menu295 = $row->f_menu295;
            $menu296 = $row->f_menu296;
            $menu297 = $row->f_menu297;
            $menu298 = $row->f_menu298;
            $menu299 = $row->f_menu299;
            $menu300 = $row->f_menu300;
            $menu301 = $row->f_menu301;
            $menu302 = $row->f_menu302;
            $menu303 = $row->f_menu303;
            $menu304 = $row->f_menu304;
            $menu305 = $row->f_menu305;
            $menu306 = $row->f_menu306;
            $menu307 = $row->f_menu307;
            $menu308 = $row->f_menu308;
            $menu309 = $row->f_menu309;
            $menu310 = $row->f_menu310;
            $menu311 = $row->f_menu311;
            $menu312 = $row->f_menu312;
            $menu313 = $row->f_menu313;
            $menu314 = $row->f_menu314;
            $menu315 = $row->f_menu315;
            $menu316 = $row->f_menu316;
            $menu317 = $row->f_menu317;
            $menu318 = $row->f_menu318;
            $menu319 = $row->f_menu319;
            $menu320 = $row->f_menu320;
            $menu321 = $row->f_menu321;
            $menu322 = $row->f_menu322;
            $menu323 = $row->f_menu323;
            $menu324 = $row->f_menu324;
            $menu325 = $row->f_menu325;
            $menu326 = $row->f_menu326;
            $menu327 = $row->f_menu327;
            $menu328 = $row->f_menu328;
            $menu329 = $row->f_menu329;
            $menu330 = $row->f_menu330;
            $menu331 = $row->f_menu331;
            $menu332 = $row->f_menu332;
            $menu333 = $row->f_menu333;
            $menu334 = $row->f_menu334;
            $menu335 = $row->f_menu335;
            $menu336 = $row->f_menu336;
            $menu337 = $row->f_menu337;
            $menu338 = $row->f_menu338;
            $menu339 = $row->f_menu339;
            $menu340 = $row->f_menu340;
            $menu341 = $row->f_menu341;
            $menu342 = $row->f_menu342;
            $menu343 = $row->f_menu343;
            $menu344 = $row->f_menu344;
            $menu345 = $row->f_menu345;
            $menu346 = $row->f_menu346;
            $menu347 = $row->f_menu347;
            $menu348 = $row->f_menu348;
            $menu349 = $row->f_menu349;
            $menu350 = $row->f_menu350;
            $menu351 = $row->f_menu351;
            $menu352 = $row->f_menu352;
            $menu353 = $row->f_menu353;
            $menu354 = $row->f_menu354;
            $menu355 = $row->f_menu355;
            $menu356 = $row->f_menu356;
            $menu357 = $row->f_menu357;
            $menu358 = $row->f_menu358;
            $menu359 = $row->f_menu359;
            $menu360 = $row->f_menu360;
            $menu361 = $row->f_menu361;
            $menu362 = $row->f_menu362;
            $menu363 = $row->f_menu363;
            $menu364 = $row->f_menu364;
            $menu365 = $row->f_menu365;
            $menu366 = $row->f_menu366;
            $menu367 = $row->f_menu367;
            $menu368 = $row->f_menu368;
            $menu369 = $row->f_menu369;
            $menu370 = $row->f_menu370;
            $menu371 = $row->f_menu371;
            $menu372 = $row->f_menu372;
            $menu373 = $row->f_menu373;
            $menu374 = $row->f_menu374;
            $menu375 = $row->f_menu375;
            $menu376 = $row->f_menu376;
            $menu377 = $row->f_menu377;
            $menu378 = $row->f_menu378;
            $menu379 = $row->f_menu379;
            $menu380 = $row->f_menu380;
            $menu381 = $row->f_menu381;
            $menu382 = $row->f_menu382;
            $menu383 = $row->f_menu383;
            $menu384 = $row->f_menu384;
            $menu385 = $row->f_menu385;
            $menu386 = $row->f_menu386;
            $menu387 = $row->f_menu387;
            $menu388 = $row->f_menu388;
            $menu389 = $row->f_menu389;
            $menu390 = $row->f_menu390;
            $menu391 = $row->f_menu391;
            $menu392 = $row->f_menu392;
            $menu393 = $row->f_menu393;
            $menu394 = $row->f_menu394;
            $menu395 = $row->f_menu395;
            $menu396 = $row->f_menu396;
            $menu397 = $row->f_menu397;
            $menu398 = $row->f_menu398;
            $menu399 = $row->f_menu399;
            $menu400 = $row->f_menu400;
            $menu401 = $row->f_menu401;
            $menu402 = $row->f_menu402;
            $menu403 = $row->f_menu403;
            $menu404 = $row->f_menu404;
            $menu405 = $row->f_menu405;
            $menu406 = $row->f_menu406;
            $menu407 = $row->f_menu407;
            $menu408 = $row->f_menu408;
            $menu409 = $row->f_menu409;
            $menu410 = $row->f_menu410;
            $menu411 = $row->f_menu411;
            $menu412 = $row->f_menu412;
            $menu413 = $row->f_menu413;
            $menu414 = $row->f_menu414;
            $menu415 = $row->f_menu415;
            $menu416 = $row->f_menu416;
            $menu417 = $row->f_menu417;
            $menu418 = $row->f_menu418;
            $menu419 = $row->f_menu419;
            $menu420 = $row->f_menu420;
            $menu421 = $row->f_menu421;
            $menu422 = $row->f_menu422;
            $menu423 = $row->f_menu423;
            $menu424 = $row->f_menu424;
            $menu425 = $row->f_menu425;
            $menu426 = $row->f_menu426;
            $menu427 = $row->f_menu427;
            $menu428 = $row->f_menu428;
            $menu429 = $row->f_menu429;
            $menu430 = $row->f_menu430;
            $menu431 = $row->f_menu431;
            $menu432 = $row->f_menu432;
            $menu433 = $row->f_menu433;
            $menu434 = $row->f_menu434;
            $menu435 = $row->f_menu435;
            $menu436 = $row->f_menu436;
            $menu437 = $row->f_menu437;
            $menu438 = $row->f_menu438;
            $menu439 = $row->f_menu439;
            $menu440 = $row->f_menu440;
            $menu441 = $row->f_menu441;
            $menu442 = $row->f_menu442;
            $menu443 = $row->f_menu443;
            $menu444 = $row->f_menu444;
            $menu445 = $row->f_menu445;
            $menu446 = $row->f_menu446;
            $menu447 = $row->f_menu447;
            $menu448 = $row->f_menu448;
            $menu449 = $row->f_menu449;
            $menu450 = $row->f_menu450;
            $menu451 = $row->f_menu451;
            $menu452 = $row->f_menu452;
            $menu453 = $row->f_menu453;
            $menu454 = $row->f_menu454;
            $menu455 = $row->f_menu455;
            $menu456 = $row->f_menu456;
            $menu457 = $row->f_menu457;
            $menu458 = $row->f_menu458;
            $menu459 = $row->f_menu459;
            $menu460 = $row->f_menu460;
            $menu461 = $row->f_menu461;
            $menu462 = $row->f_menu462;
            $menu463 = $row->f_menu463;
            $menu464 = $row->f_menu464;
            $menu465 = $row->f_menu465;
            $menu466 = $row->f_menu466;
            $menu467 = $row->f_menu467;
            $menu468 = $row->f_menu468;
            $menu469 = $row->f_menu469;
            $menu470 = $row->f_menu470;
            $menu471 = $row->f_menu471;
            $menu472 = $row->f_menu472;
            $menu473 = $row->f_menu473;
            $menu474 = $row->f_menu474;
            $menu475 = $row->f_menu475;
            $menu476 = $row->f_menu476;
            $menu477 = $row->f_menu477;
            $menu478 = $row->f_menu478;
            $menu479 = $row->f_menu479;
            $menu480 = $row->f_menu480;
            $menu481 = $row->f_menu481;
            $menu482 = $row->f_menu482;
            $menu483 = $row->f_menu483;
            $menu484 = $row->f_menu484;
            $menu485 = $row->f_menu485;
            $menu486 = $row->f_menu486;
            $menu487 = $row->f_menu487;
            $menu488 = $row->f_menu488;
            $menu489 = $row->f_menu489;
            $menu490 = $row->f_menu490;
            $menu491 = $row->f_menu491;
            $menu492 = $row->f_menu492;
            $menu493 = $row->f_menu493;
            $menu494 = $row->f_menu494;
            $menu495 = $row->f_menu495;
            $menu496 = $row->f_menu496;
            $menu497 = $row->f_menu497;
            $menu498 = $row->f_menu498;
            $menu499 = $row->f_menu499;
            $menu500 = $row->f_menu500;
            $menu501 = $row->f_menu501;
            $menu502 = $row->f_menu502;
            $menu503 = $row->f_menu503;
            $menu504 = $row->f_menu504;
            $menu505 = $row->f_menu505;
            $menu506 = $row->f_menu506;
            $menu507 = $row->f_menu507;
            $menu508 = $row->f_menu508;
            $menu509 = $row->f_menu509;
            $menu510 = $row->f_menu510;
            $menu511 = $row->f_menu511;
            $menu512 = $row->f_menu512;
            $menu513 = $row->f_menu513;
            $menu514 = $row->f_menu514;
            $menu515 = $row->f_menu515;
            $menu516 = $row->f_menu516;
            $menu517 = $row->f_menu517;
            $menu518 = $row->f_menu518;
            $menu519 = $row->f_menu519;
            $menu520 = $row->f_menu520;
            $menu521 = $row->f_menu521;
            $menu522 = $row->f_menu522;
            $menu523 = $row->f_menu523;
            $menu524 = $row->f_menu524;
            $menu525 = $row->f_menu525;
            $menu526 = $row->f_menu526;
            $menu527 = $row->f_menu527;
            $menu528 = $row->f_menu528;
            $menu529 = $row->f_menu529;
            $menu530 = $row->f_menu530;
            $menu531 = $row->f_menu531;
            $menu532 = $row->f_menu532;
            $menu533 = $row->f_menu533;
            $menu534 = $row->f_menu534;
            $menu535 = $row->f_menu535;
            $menu536 = $row->f_menu536;
            $menu537 = $row->f_menu537;
            $menu538 = $row->f_menu538;
            $menu539 = $row->f_menu539;
            $menu540 = $row->f_menu540;
            $menu541 = $row->f_menu541;
            $menu542 = $row->f_menu542;
            $menu543 = $row->f_menu543;
            $menu544 = $row->f_menu544;
            $menu545 = $row->f_menu545;
            $menu546 = $row->f_menu546;
            $menu547 = $row->f_menu547;
            $menu548 = $row->f_menu548;
            $menu549 = $row->f_menu549;
            $menu550 = $row->f_menu550;
            $menu551 = $row->f_menu551;
            $menu552 = $row->f_menu552;
            $menu553 = $row->f_menu553;
            $menu554 = $row->f_menu554;
            $menu555 = $row->f_menu555;
            $menu556 = $row->f_menu556;
            $menu557 = $row->f_menu557;
            $menu558 = $row->f_menu558;
            $menu559 = $row->f_menu559;
            $menu560 = $row->f_menu560;
            $menu561 = $row->f_menu561;
            $menu562 = $row->f_menu562;
            $menu563 = $row->f_menu563;
            $menu564 = $row->f_menu564;
            $menu565 = $row->f_menu565;
            $menu566 = $row->f_menu566;
            $menu567 = $row->f_menu567;
            $menu568 = $row->f_menu568;
            $menu569 = $row->f_menu569;
            $menu570 = $row->f_menu570;
            $menu571 = $row->f_menu571;
            $menu572 = $row->f_menu572;
            $menu573 = $row->f_menu573;
            $menu574 = $row->f_menu574;
            $menu575 = $row->f_menu575;
            $menu576 = $row->f_menu576;
            $menu577 = $row->f_menu577;
            $menu578 = $row->f_menu578;
            $menu579 = $row->f_menu579;
            $menu580 = $row->f_menu580;
            $menu581 = $row->f_menu581;
            $menu582 = $row->f_menu582;
            $menu583 = $row->f_menu583;
            $menu584 = $row->f_menu584;
            $menu585 = $row->f_menu585;
            $menu586 = $row->f_menu586;
            $menu587 = $row->f_menu587;
            $menu588 = $row->f_menu588;
            $menu589 = $row->f_menu589;
            $menu590 = $row->f_menu590;
            $menu591 = $row->f_menu591;
            $menu592 = $row->f_menu592;
            $menu593 = $row->f_menu593;
            $menu594 = $row->f_menu594;
            $menu595 = $row->f_menu595;
            $menu596 = $row->f_menu596;
            $menu597 = $row->f_menu597;
            $menu598 = $row->f_menu598;
            $menu599 = $row->f_menu599;
            $menu600 = $row->f_menu600;
            $level = $row->i_level;
            $id = $row->i_user;
            $status = $row->i_status;
            $departement = $row->i_departement;

            // * GET MENU PER USER NYA DISINI
            // for($i = 0;$i <= 600;$i++){

            //     $data1 = $this->db->query(" SELECT i_user, f_menu$i AS menu, i_level, i_departement FROM tm_user WHERE i_user = '$row->i_user' ")->row();

            //     if($data1->menu == "t"){
            //         // echo $data1->i_user.";f_menu".$i.";".$menuuser."<br>";

            //         // $menuuser = "menu".$i;

            //         $menu.$i = 


            //     } // * END IF MENU NYA TRUE 
            //     $no++;
            // } // * END FOR GET MENU USER

            $credentials = array(
                'menu0' => $menu0, 'menu1' => $menu1, 'menu2' => $menu2, 'menu3' => $menu3, 'menu4' => $menu4,
                'menu5' => $menu5, 'menu6' => $menu6, 'menu7' => $menu7, 'menu8' => $menu8, 'menu9' => $menu9,
                'menu10' => $menu10, 'menu11' => $menu11, 'menu12' => $menu12, 'menu13' => $menu13,
                'menu14' => $menu14, 'menu15' => $menu15, 'menu16' => $menu16, 'menu17' => $menu17, 'menu18' => $menu18,
                'menu19' => $menu19, 'menu20' => $menu20, 'menu21' => $menu21, 'menu22' => $menu22, 'menu23' => $menu23,
                'menu24' => $menu24, 'menu25' => $menu25, 'menu26' => $menu26, 'menu27' => $menu27, 'menu28' => $menu28,
                'menu29' => $menu29, 'menu30' => $menu30, 'menu31' => $menu31, 'menu32' => $menu32, 'menu33' => $menu33,
                'menu34' => $menu34, 'menu35' => $menu35, 'menu36' => $menu36, 'menu37' => $menu37, 'menu38' => $menu38,
                'menu39' => $menu39, 'menu40' => $menu40, 'menu41' => $menu41, 'menu42' => $menu42, 'menu43' => $menu43,
                'menu44' => $menu44, 'menu45' => $menu45, 'menu46' => $menu46, 'menu47' => $menu47, 'menu48' => $menu48,
                'menu49' => $menu49, 'menu50' => $menu50, 'menu51' => $menu51, 'menu52' => $menu52, 'menu53' => $menu53,
                'menu54' => $menu54, 'menu55' => $menu55, 'menu56' => $menu56, 'menu57' => $menu57, 'menu58' => $menu58,
                'menu59' => $menu59, 'menu60' => $menu60, 'menu61' => $menu61, 'menu62' => $menu62, 'menu63' => $menu63,
                'menu64' => $menu64, 'menu65' => $menu65, 'menu66' => $menu66, 'menu67' => $menu67, 'menu68' => $menu68,
                'menu69' => $menu69, 'menu70' => $menu70, 'menu71' => $menu71, 'menu72' => $menu72, 'menu73' => $menu73,
                'menu74' => $menu74, 'menu75' => $menu75, 'menu76' => $menu76, 'menu77' => $menu77, 'menu78' => $menu78,
                'menu79' => $menu79, 'menu80' => $menu80, 'menu81' => $menu81, 'menu82' => $menu82, 'menu83' => $menu83,
                'menu84' => $menu84, 'menu85' => $menu85, 'menu86' => $menu86, 'menu87' => $menu87, 'menu88' => $menu88,
                'menu89' => $menu89, 'menu90' => $menu90, 'menu91' => $menu91, 'menu92' => $menu92, 'menu93' => $menu93,
                'menu94' => $menu94, 'menu95' => $menu95, 'menu96' => $menu96, 'menu97' => $menu97, 'menu98' => $menu98,
                'menu99' => $menu99, 'menu100' => $menu100, 'menu101' => $menu101, 'menu102' => $menu102, 'menu103' => $menu103,
                'menu104' => $menu104, 'menu105' => $menu105, 'menu106' => $menu106, 'menu107' => $menu107, 'menu108' => $menu108,
                'menu109' => $menu109, 'menu110' => $menu110, 'menu111' => $menu111, 'menu112' => $menu112, 'menu113' => $menu113,
                'menu114' => $menu114, 'menu115' => $menu115, 'menu116' => $menu116, 'menu117' => $menu117, 'menu118' => $menu118,
                'menu119' => $menu119, 'menu120' => $menu120, 'menu121' => $menu121, 'menu122' => $menu122, 'menu123' => $menu123,
                'menu124' => $menu124, 'menu125' => $menu125, 'menu126' => $menu126, 'menu127' => $menu127, 'menu128' => $menu128,
                'menu129' => $menu129, 'menu130' => $menu130, 'menu131' => $menu131, 'menu132' => $menu132, 'menu133' => $menu133,
                'menu134' => $menu134, 'menu135' => $menu135, 'menu136' => $menu136, 'menu137' => $menu137, 'menu138' => $menu138,
                'menu139' => $menu139, 'menu140' => $menu140, 'menu141' => $menu141, 'menu142' => $menu142, 'menu143' => $menu143,
                'menu144' => $menu144, 'menu145' => $menu145, 'menu146' => $menu146, 'menu147' => $menu147, 'menu148' => $menu148,
                'menu149' => $menu149, 'menu150' => $menu150, 'menu151' => $menu151, 'menu152' => $menu152, 'menu153' => $menu153,
                'menu154' => $menu154, 'menu155' => $menu155, 'menu156' => $menu156, 'menu157' => $menu157, 'menu158' => $menu158,
                'menu159' => $menu159, 'menu160' => $menu160, 'menu161' => $menu161, 'menu162' => $menu162, 'menu163' => $menu163,
                'menu164' => $menu164, 'menu165' => $menu165, 'menu166' => $menu166, 'menu167' => $menu167, 'menu168' => $menu168,
                'menu169' => $menu169, 'menu170' => $menu170, 'menu171' => $menu171, 'menu172' => $menu172, 'menu173' => $menu173,
                'menu174' => $menu174, 'menu175' => $menu175, 'menu176' => $menu176, 'menu177' => $menu177, 'menu178' => $menu178,
                'menu179' => $menu179, 'menu180' => $menu180, 'menu181' => $menu181, 'menu182' => $menu182, 'menu183' => $menu183,
                'menu184' => $menu184, 'menu185' => $menu185, 'menu186' => $menu186, 'menu187' => $menu187, 'menu188' => $menu188,
                'menu189' => $menu189, 'menu190' => $menu190, 'menu191' => $menu191, 'menu192' => $menu192, 'menu193' => $menu193,
                'menu194' => $menu194, 'menu195' => $menu195, 'menu196' => $menu196, 'menu197' => $menu197, 'menu198' => $menu198,
                'menu199' => $menu199, 'menu200' => $menu200, 'menu201' => $menu201, 'menu202' => $menu202, 'menu203' => $menu203,
                'menu204' => $menu204, 'menu205' => $menu205, 'menu206' => $menu206, 'menu207' => $menu207, 'menu208' => $menu208,
                'menu209' => $menu209, 'menu210' => $menu210, 'menu211' => $menu211, 'menu212' => $menu212, 'menu213' => $menu213,
                'menu214' => $menu214, 'menu215' => $menu215, 'menu216' => $menu216, 'menu217' => $menu217, 'menu218' => $menu218,
                'menu219' => $menu219, 'menu220' => $menu220, 'menu221' => $menu221, 'menu222' => $menu222, 'menu223' => $menu223,
                'menu224' => $menu224, 'menu225' => $menu225, 'menu226' => $menu226, 'menu227' => $menu227, 'menu228' => $menu228,
                'menu229' => $menu229, 'menu230' => $menu230, 'menu231' => $menu231, 'menu232' => $menu232, 'menu233' => $menu233,
                'menu234' => $menu234, 'menu235' => $menu235, 'menu236' => $menu236, 'menu237' => $menu237, 'menu238' => $menu238,
                'menu239' => $menu239, 'menu240' => $menu240, 'menu241' => $menu241, 'menu242' => $menu242, 'menu243' => $menu243,
                'menu244' => $menu244, 'menu245' => $menu245, 'menu246' => $menu246, 'menu247' => $menu247, 'menu248' => $menu248,
                'menu249' => $menu249, 'menu250' => $menu250, 'menu251' => $menu251, 'menu252' => $menu252, 'menu253' => $menu253,
                'menu254' => $menu254, 'menu255' => $menu255, 'menu256' => $menu256, 'menu257' => $menu257, 'menu258' => $menu258,
                'menu259' => $menu259, 'menu260' => $menu260, 'menu261' => $menu261, 'menu262' => $menu262, 'menu263' => $menu263,
                'menu264' => $menu264, 'menu265' => $menu265, 'menu266' => $menu266, 'menu267' => $menu267, 'menu268' => $menu268,
                'menu269' => $menu269, 'menu270' => $menu270, 'menu271' => $menu271, 'menu272' => $menu272, 'menu273' => $menu273,
                'menu274' => $menu274, 'menu275' => $menu275, 'menu276' => $menu276, 'menu277' => $menu277, 'menu278' => $menu278,
                'menu279' => $menu279, 'menu280' => $menu280, 'menu281' => $menu281, 'menu282' => $menu282, 'menu283' => $menu283,
                'menu284' => $menu284, 'menu285' => $menu285, 'menu286' => $menu286, 'menu287' => $menu287, 'menu288' => $menu288,
                'menu289' => $menu289, 'menu290' => $menu290, 'menu291' => $menu291, 'menu292' => $menu292, 'menu293' => $menu293,
                'menu294' => $menu294, 'menu295' => $menu295, 'menu296' => $menu296, 'menu297' => $menu297, 'menu298' => $menu298,
                'menu299' => $menu299, 'menu300' => $menu300, 'menu301' => $menu301, 'menu302' => $menu302, 'menu303' => $menu303,
                'menu304' => $menu304, 'menu305' => $menu305, 'menu306' => $menu306, 'menu307' => $menu307, 'menu308' => $menu308,
                'menu309' => $menu309, 'menu310' => $menu310, 'menu311' => $menu311, 'menu312' => $menu312, 'menu313' => $menu313,
                'menu314' => $menu314, 'menu315' => $menu315, 'menu316' => $menu316, 'menu317' => $menu317, 'menu318' => $menu318,
                'menu319' => $menu319, 'menu320' => $menu320, 'menu321' => $menu321, 'menu322' => $menu322, 'menu323' => $menu323,
                'menu324' => $menu324, 'menu325' => $menu325, 'menu326' => $menu326, 'menu327' => $menu327, 'menu328' => $menu328,
                'menu329' => $menu329, 'menu330' => $menu330, 'menu331' => $menu331, 'menu332' => $menu332, 'menu333' => $menu333,
                'menu334' => $menu334, 'menu335' => $menu335, 'menu336' => $menu336, 'menu337' => $menu337, 'menu338' => $menu338,
                'menu339' => $menu339, 'menu340' => $menu340, 'menu341' => $menu341, 'menu342' => $menu342, 'menu343' => $menu343,
                'menu344' => $menu344, 'menu345' => $menu345, 'menu346' => $menu346, 'menu347' => $menu347, 'menu348' => $menu348,
                'menu349' => $menu349, 'menu350' => $menu350, 'menu351' => $menu351, 'menu352' => $menu352, 'menu353' => $menu353,
                'menu354' => $menu354, 'menu355' => $menu355, 'menu356' => $menu356, 'menu357' => $menu357, 'menu358' => $menu358,
                'menu359' => $menu359, 'menu360' => $menu360, 'menu361' => $menu361, 'menu362' => $menu362, 'menu363' => $menu363,
                'menu364' => $menu364, 'menu365' => $menu365, 'menu366' => $menu366, 'menu367' => $menu367, 'menu368' => $menu368,
                'menu369' => $menu369, 'menu370' => $menu370, 'menu371' => $menu371, 'menu372' => $menu372, 'menu373' => $menu373,
                'menu374' => $menu374, 'menu375' => $menu375, 'menu376' => $menu376, 'menu377' => $menu377, 'menu378' => $menu378,
                'menu379' => $menu379, 'menu380' => $menu380, 'menu381' => $menu381, 'menu382' => $menu382, 'menu383' => $menu383,
                'menu384' => $menu384, 'menu385' => $menu385, 'menu386' => $menu386, 'menu387' => $menu387, 'menu388' => $menu388,
                'menu389' => $menu389, 'menu390' => $menu390, 'menu391' => $menu391, 'menu392' => $menu392, 'menu393' => $menu393,
                'menu394' => $menu394, 'menu395' => $menu395, 'menu396' => $menu396, 'menu397' => $menu397, 'menu398' => $menu398,
                'menu399' => $menu399, 'menu400' => $menu400, 'menu401' => $menu401, 'menu402' => $menu402, 'menu403' => $menu403,
                'menu404' => $menu404, 'menu405' => $menu405, 'menu406' => $menu406, 'menu407' => $menu407, 'menu408' => $menu408,
                'menu409' => $menu409, 'menu410' => $menu410, 'menu411' => $menu411, 'menu412' => $menu412, 'menu413' => $menu413,
                'menu414' => $menu414, 'menu415' => $menu415, 'menu416' => $menu416, 'menu417' => $menu417, 'menu418' => $menu418,
                'menu419' => $menu419, 'menu420' => $menu420, 'menu421' => $menu421, 'menu422' => $menu422, 'menu423' => $menu423,
                'menu424' => $menu424, 'menu425' => $menu425, 'menu426' => $menu426, 'menu427' => $menu427, 'menu428' => $menu428,
                'menu429' => $menu429, 'menu430' => $menu430, 'menu431' => $menu431, 'menu432' => $menu432, 'menu433' => $menu433,
                'menu434' => $menu434, 'menu435' => $menu435, 'menu436' => $menu436, 'menu437' => $menu437, 'menu438' => $menu438,
                'menu439' => $menu439, 'menu440' => $menu440, 'menu441' => $menu441, 'menu442' => $menu442, 'menu443' => $menu443,
                'menu444' => $menu444, 'menu445' => $menu445, 'menu446' => $menu446, 'menu447' => $menu447, 'menu448' => $menu448,
                'menu449' => $menu449, 'menu450' => $menu450, 'menu451' => $menu451, 'menu452' => $menu452, 'menu453' => $menu453,
                'menu454' => $menu454, 'menu455' => $menu455, 'menu456' => $menu456, 'menu457' => $menu457, 'menu458' => $menu458,
                'menu459' => $menu459, 'menu460' => $menu460, 'menu461' => $menu461, 'menu462' => $menu462, 'menu463' => $menu463,
                'menu464' => $menu464, 'menu465' => $menu465, 'menu466' => $menu466, 'menu467' => $menu467, 'menu468' => $menu468,
                'menu469' => $menu469, 'menu470' => $menu470, 'menu471' => $menu471, 'menu472' => $menu472, 'menu473' => $menu473,
                'menu474' => $menu474, 'menu475' => $menu475, 'menu476' => $menu476, 'menu477' => $menu477, 'menu478' => $menu478,
                'menu479' => $menu479, 'menu480' => $menu480, 'menu481' => $menu481, 'menu482' => $menu482, 'menu483' => $menu483,
                'menu484' => $menu484, 'menu485' => $menu485, 'menu486' => $menu486, 'menu487' => $menu487, 'menu488' => $menu488,
                'menu489' => $menu489, 'menu490' => $menu490, 'menu491' => $menu491, 'menu492' => $menu492, 'menu493' => $menu493,
                'menu494' => $menu494, 'menu495' => $menu495, 'menu496' => $menu496, 'menu497' => $menu497, 'menu498' => $menu498,
                'menu499' => $menu499, 'menu500' => $menu500,
                'menu501' => $menu501, 'menu502' => $menu502, 'menu503' => $menu503,
                'menu504' => $menu504, 'menu505' => $menu505, 'menu506' => $menu506, 'menu507' => $menu507, 'menu508' => $menu508,
                'menu509' => $menu509, 'menu510' => $menu510, 'menu511' => $menu511, 'menu512' => $menu512, 'menu513' => $menu513,
                'menu514' => $menu514, 'menu515' => $menu515, 'menu516' => $menu516, 'menu517' => $menu517, 'menu518' => $menu518,
                'menu519' => $menu519, 'menu520' => $menu520, 'menu521' => $menu521, 'menu522' => $menu522, 'menu523' => $menu523,
                'menu524' => $menu524, 'menu525' => $menu525, 'menu526' => $menu526, 'menu527' => $menu527, 'menu528' => $menu528,
                'menu529' => $menu529, 'menu530' => $menu530, 'menu531' => $menu531, 'menu532' => $menu532, 'menu533' => $menu533,
                'menu534' => $menu534, 'menu535' => $menu535, 'menu536' => $menu536, 'menu537' => $menu537, 'menu538' => $menu538,
                'menu539' => $menu539, 'menu540' => $menu540, 'menu541' => $menu541, 'menu542' => $menu542, 'menu543' => $menu543,
                'menu544' => $menu544, 'menu545' => $menu545, 'menu546' => $menu546, 'menu547' => $menu547, 'menu548' => $menu548,
                'menu549' => $menu549, 'menu550' => $menu550, 'menu551' => $menu551, 'menu552' => $menu552, 'menu553' => $menu553,
                'menu554' => $menu554, 'menu555' => $menu555, 'menu556' => $menu556, 'menu557' => $menu557, 'menu558' => $menu558,
                'menu559' => $menu559, 'menu560' => $menu560, 'menu561' => $menu561, 'menu562' => $menu562, 'menu563' => $menu563,
                'menu564' => $menu564, 'menu565' => $menu565, 'menu566' => $menu566, 'menu567' => $menu567, 'menu568' => $menu568,
                'menu569' => $menu569, 'menu570' => $menu570, 'menu571' => $menu571, 'menu572' => $menu572, 'menu573' => $menu573,
                'menu574' => $menu574, 'menu575' => $menu575, 'menu576' => $menu576, 'menu577' => $menu577, 'menu578' => $menu578,
                'menu579' => $menu579, 'menu580' => $menu580, 'menu581' => $menu581, 'menu582' => $menu582, 'menu583' => $menu583,
                'menu584' => $menu584, 'menu585' => $menu585, 'menu586' => $menu586, 'menu587' => $menu587, 'menu588' => $menu588,
                'menu589' => $menu589, 'menu590' => $menu590, 'menu591' => $menu591, 'menu592' => $menu592, 'menu593' => $menu593,
                'menu594' => $menu594, 'menu595' => $menu595, 'menu596' => $menu596, 'menu597' => $menu597, 'menu598' => $menu598,
                'menu599' => $menu599, 'menu600' => $menu600, 'level' => $level, 'departement' => $departement, 'user_id' => $id,
                'status' => $status
            );

            $this->obj->session->set_userdata($credentials);

            // **********************************************************************

            echo '<ul>';
            if (
                ($this->session->userdata('menu1') == 't') or ($this->session->userdata('menu2') == 't') or
                ($this->session->userdata('menu9') == 't') or ($this->session->userdata('menu10') == 't') or
                ($this->session->userdata('menu11') == 't') or ($this->session->userdata('menu12') == 't') or
                ($this->session->userdata('menu13') == 't') or ($this->session->userdata('menu14') == 't') or
                ($this->session->userdata('menu15') == 't') or ($this->session->userdata('menu16') == 't') or
                ($this->session->userdata('menu17') == 't') or ($this->session->userdata('menu88') == 't') or
                ($this->session->userdata('menu18') == 't') or ($this->session->userdata('menu19') == 't') or
                ($this->session->userdata('menu20') == 't') or ($this->session->userdata('menu21') == 't') or
                ($this->session->userdata('menu22') == 't') or ($this->session->userdata('menu23') == 't') or
                ($this->session->userdata('menu24') == 't') or ($this->session->userdata('menu25') == 't') or
                ($this->session->userdata('menu26') == 't') or ($this->session->userdata('menu27') == 't') or
                ($this->session->userdata('menu28') == 't') or ($this->session->userdata('menu29') == 't') or
                ($this->session->userdata('menu30') == 't') or ($this->session->userdata('menu31') == 't') or
                ($this->session->userdata('menu32') == 't') or ($this->session->userdata('menu33') == 't') or
                ($this->session->userdata('menu34') == 't') or ($this->session->userdata('menu35') == 't') or
                ($this->session->userdata('menu36') == 't') or ($this->session->userdata('menu37') == 't') or
                ($this->session->userdata('menu38') == 't') or ($this->session->userdata('menu39') == 't') or
                ($this->session->userdata('menu40') == 't') or ($this->session->userdata('menu41') == 't') or
                ($this->session->userdata('menu42') == 't') or ($this->session->userdata('menu43') == 't') or
                ($this->session->userdata('menu44') == 't') or ($this->session->userdata('menu45') == 't') or
                ($this->session->userdata('menu46') == 't') or ($this->session->userdata('menu53') == 't') or
                ($this->session->userdata('menu3') == 't') or ($this->session->userdata('menu4') == 't') or
                ($this->session->userdata('menu5') == 't') or ($this->session->userdata('menu6') == 't') or
                ($this->session->userdata('menu144') == 't') or ($this->session->userdata('menu185') == 't') or
                ($this->session->userdata('menu217') == 't') or ($this->session->userdata('menu225') == 't') or
                ($this->session->userdata('menu260') == 't') or ($this->session->userdata('menu0') == 't') or
                ($this->session->userdata('menu68') == 't') or ($this->session->userdata('menu93') == 't') or
                ($this->session->userdata('menu232') == 't') or ($this->session->userdata('menu357') == 't') or
                ($this->session->userdata('menu428') == 't')
            ) {
                echo '<li><a href="#">Master</a><ul>';
                if (($this->session->userdata('menu1') == 't') or ($this->session->userdata('menu2') == 't')) {
                    echo '<li><a href="#">Supplier</a><ul>';
                    if ($this->session->userdata('menu1') == 't') {
                        echo $row->i_user . ",Group Supplier,suppliergroup/cform/<br>";
                    }
                    if ($this->session->userdata('menu2') == 't') {
                        echo $row->i_user . ",Supplier,supplier/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu9') == 't') or ($this->session->userdata('menu10') == 't') or
                    ($this->session->userdata('menu11') == 't') or ($this->session->userdata('menu12') == 't') or
                    ($this->session->userdata('menu13') == 't') or ($this->session->userdata('menu14') == 't') or
                    ($this->session->userdata('menu15') == 't') or ($this->session->userdata('menu16') == 't') or
                    ($this->session->userdata('menu17') == 't') or ($this->session->userdata('menu260') == 't')
                ) {
                    echo '<li><a href="#">Barang</a><ul>';
                    if ($this->session->userdata('menu14') == 't') {
                        echo $row->i_user . ",Master Barang,productbase/cform/<br>";
                    }
                    if ($this->session->userdata('menu9') == 't') {
                        echo $row->i_user . ",Master Group Barang,group/cform/<br>";
                    }
                    if ($this->session->userdata('menu9') == 't') {
                        echo $row->i_user . ",Master Seri Barang,seri/cform/<br>";
                    }
                    if ($this->session->userdata('menu10') == 't') {
                        echo $row->i_user . ",Master Status Barang,status/cform/<br>";
                    }
                    if ($this->session->userdata('menu11') == 't') {
                        echo $row->i_user . ",Master Kategori Barang,class/cform/<br>";
                    }
                    if ($this->session->userdata('menu12') == 't') {
                        echo $row->i_user . ",Master Sub Kategori Barang,category/cform/<br>";
                    }
                    if ($this->session->userdata('menu13') == 't') {
                        echo $row->i_user . ",Master Jenis Barang,type/cform/<br>";
                    }
                    if ($this->session->userdata('menu15') == 't') {
                        echo $row->i_user . ",Master Motif Barang,productmotif/cform/<br>";
                    }
                    if ($this->session->userdata('menu16') == 't') {
                        echo $row->i_user . ",Master Grade Barang,productgrade/cform/<br>";
                    }
                    if ($this->session->userdata('menu17') == 't') {
                        echo $row->i_user . ",Master Harga Barang,productprice/cform/<br>";
                    }
                    if ($this->session->userdata('menu408') == 't') {
                        echo $row->i_user . ",Master Harga Barang (Manual),productpricemanual/cform/<br>";
                    }
                    if ($this->session->userdata('menu260') == 't') {
                        echo $row->i_user . ",Master Harga Beli,hargabeli/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu18') == 't') or ($this->session->userdata('menu19') == 't') or
                    ($this->session->userdata('menu20') == 't') or ($this->session->userdata('menu21') == 't') or
                    ($this->session->userdata('menu22') == 't') or ($this->session->userdata('menu23') == 't') or
                    ($this->session->userdata('menu24') == 't') or ($this->session->userdata('menu25') == 't') or
                    ($this->session->userdata('menu26') == 't') or ($this->session->userdata('menu27') == 't') or
                    ($this->session->userdata('menu428') == 't')
                ) {
                    echo '<li><a href="#">Area</a><ul>';
                    if ($this->session->userdata('menu18') == 't') {
                        echo $row->i_user . ",Master Jenis Area,areatype/cform/<br>";
                    }
                    if ($this->session->userdata('menu19') == 't') {
                        echo $row->i_user . ",Master Area,area/cform/<br>";
                    }
                    if ($this->session->userdata('menu467') == 't') {
                        echo $row->i_user . ",Master Area Cover,areacover/cform/<br>";
                    }
                    //* STANDARISASI
                    if ($this->session->userdata('menu467') == 't') {
                        echo $row->i_user . ",Master Area Rayon,arearayon/cform/<br>";
                    }
                    if ($this->session->userdata('menu467') == 't') {
                        echo $row->i_user . ",List Area Cover,listareacover/cform/<br>";
                    }
                    if ($this->session->userdata('menu466') == 't') {
                        echo $row->i_user . ",Master User Area,userarea/cform/<br>";
                    }
                    if ($this->session->userdata('menu20') == 't') {
                        echo $row->i_user . ",Master Salesman,salesman/cform/<br>";
                    }
                    if ($this->session->userdata('menu428') == 't') {
                        echo $row->i_user . ",Pindah Periode Salesman,ppsales/cform/<br>";
                    }
                    if ($this->session->userdata('menu21') == 't') {
                        echo $row->i_user . ",Master Negara,country/cform/<br>";
                    }
                    if ($this->session->userdata('menu22') == 't') {
                        echo $row->i_user . ",Master Teritori,teritori/cform/<br>";
                    }
                    if ($this->session->userdata('menu23') == 't') {
                        echo $row->i_user . ",Master Jenis Kota,citytype/cform/<br>";
                    }
                    if ($this->session->userdata('menu24') == 't') {
                        echo $row->i_user . ",Master Status Kota,citystatus/cform/<br>";
                    }
                    if ($this->session->userdata('menu25') == 't') {
                        echo $row->i_user . ",Master Jenis Kota per Area,citytypeperarea/cform/<br>";
                    }
                    if ($this->session->userdata('menu26') == 't') {
                        echo $row->i_user . ",Master Group Kota,citygroup/cform/<br>";
                    }
                    if ($this->session->userdata('menu27') == 't') {
                        echo $row->i_user . ",Master Kota,city/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu28') == 't') or ($this->session->userdata('menu29') == 't') or
                    ($this->session->userdata('menu30') == 't') or ($this->session->userdata('menu31') == 't') or
                    ($this->session->userdata('menu32') == 't') or ($this->session->userdata('menu33') == 't') or
                    ($this->session->userdata('menu34') == 't') or ($this->session->userdata('menu35') == 't') or
                    ($this->session->userdata('menu36') == 't') or ($this->session->userdata('menu37') == 't') or
                    ($this->session->userdata('menu38') == 't') or ($this->session->userdata('menu39') == 't') or
                    ($this->session->userdata('menu40') == 't') or ($this->session->userdata('menu41') == 't') or
                    ($this->session->userdata('menu42') == 't') or ($this->session->userdata('menu43') == 't') or
                    ($this->session->userdata('menu44') == 't') or ($this->session->userdata('menu45') == 't') or
                    ($this->session->userdata('menu46') == 't') or ($this->session->userdata('menu198') == 't') or
                    ($this->session->userdata('menu217') == 't') or ($this->session->userdata('menu357') == 't') or
                    ($this->session->userdata('menu413') == 't')
                ) {
                    echo '<li><a href="#">Pelanggan</a><ul>';
                    echo '<li><a href="#" >Detail</a><ul>';
                    if ($this->session->userdata('menu28') == 't') {
                        echo $row->i_user . ",Master Kelompok Harga,pricegroup/cform/<br>";
                    }
                    if ($this->session->userdata('menu29') == 't') {
                        echo $row->i_user . ",Master Area Pelanggan,customerarea/cform/<br>";
                    }
                    if ($this->session->userdata('menu30') == 't') {
                        echo $row->i_user . ",Master Salesman per Pelanggan,customersalesman/cform/<br>";
                    }
                    if ($this->session->userdata('menu37') == 't') {
                        echo $row->i_user . ",Master Group Pelanggan,customergroup/cform/<br>";
                    }
                    if ($this->session->userdata('menu437') == 't') {
                        echo $row->i_user . ",Master Group Bayar,customergroupbayar/cform/<br>";
                    }
                    if ($this->session->userdata('menu461') == 't') {
                        echo $row->i_user . ",Master Group AR,customergroupar/cform/<br>";
                    }
                    if ($this->session->userdata('menu38') == 't') {
                        echo $row->i_user . ",Master Bank Pelanggan,customerbank/cform/<br>";
                    }
                    if ($this->session->userdata('menu39') == 't') {
                        echo $row->i_user . ",Master Group PLU,customerplugroup/cform/<br>";
                    }
                    if ($this->session->userdata('menu40') == 't') {
                        echo $row->i_user . ",Master PLU,customerplu/cform/<br>";
                    }
                    if ($this->session->userdata('menu41') == 't') {
                        echo $row->i_user . ",Master Pemilik (Pelanggan),customerowner/cform/<br>";
                    }
                    if ($this->session->userdata('menu198') == 't') {
                        echo $row->i_user . ",Master Penyetor (Pelanggan),customersetor/cform/<br>";
                    }
                    if ($this->session->userdata('menu42') == 't') {
                        echo $row->i_user . ",Master PKP (Pelanggan),customerpkp/cform/<br>";
                    }
                    if ($this->session->userdata('menu43') == 't') {
                        echo $row->i_user . ",Master Discount (Pelanggan),customerdiscount/cform/<br>";
                    }
                    if ($this->session->userdata('menu44') == 't') {
                        echo $row->i_user . ",Master Status (Pelanggan),customerstatus/cform/<br>";
                    }
                    echo '</ul></li>
                                    <li><a href="#" >Atribut</a><ul>';
                    if ($this->session->userdata('menu31') == 't') {
                        echo $row->i_user . ",Master Layanan Pelanggan,customerservice/cform/<br>";
                    }
                    if ($this->session->userdata('menu32') == 't') {
                        echo $row->i_user . ",Master Ukuran Pelanggan,customergrade/cform/<br>";
                    }
                    if ($this->session->userdata('menu33') == 't') {
                        echo $row->i_user . ",Master Cara Penjualan Pelanggan,customersalestype/cform/<br>";
                    }
                    if ($this->session->userdata('menu34') == 't') {
                        echo $row->i_user . ",Master Kelas Pelanggan,customerclass/cform/<br>";
                    }
                    if ($this->session->userdata('menu35') == 't') {
                        echo $row->i_user . ",Master Tipe Produk Pelanggan,customerproducttype/cform/<br>";
                    }
                    if ($this->session->userdata('menu36') == 't') {
                        echo $row->i_user . ",Master Produk Khusus (Pelanggan),customerspecialproduct/cform/<br>";
                    }
                    echo '</ul></li>';
                    if ($this->session->userdata('menu45') == 't') {
                        echo $row->i_user . ",Master Pelanggan,customer/cform/<br>";
                    }
                    if ($this->session->userdata('menu46') == 't') {
                        echo $row->i_user . ",Master Approval Pelanggan,customerapprove/cform/<br>";
                    }
                    if ($this->session->userdata('menu217') == 't') {
                        echo $row->i_user . ",Update Kode Pelanggan,customerupdate/cform/<br>";
                    }
                    if ($this->session->userdata('menu357') == 't') {
                        echo $row->i_user . ",Pelanggan non SPB,customernewnonspb/cform/<br>";
                    }
                    if ($this->session->userdata('menu413') == 't') {
                        echo $row->i_user . ",Pelanggan Pareto,customerpareto/cform/<br>";
                    }
                    echo '</ul>';
                }

                //MENU MASTER PELANGGAN UNTUK AR
                if (
                    ($this->session->userdata('menu460') == 't')
                ) {
                    if ($this->session->userdata('menu460') == 't') {
                        //echo $row->i_user.",Master Pelanggan,customerar/cform/<br>";
                    }
                }
                if (
                    ($this->session->userdata('menu3') == 't') or ($this->session->userdata('menu4') == 't') or
                    ($this->session->userdata('menu5') == 't') or ($this->session->userdata('menu6') == 't')
                ) {
                    echo '<li><a href="#">Gudang</a><ul>';
                    if ($this->session->userdata('menu3') == 't') {
                        echo $row->i_user . ",Master Gudang,store/cform/<br>";
                    }
                    if ($this->session->userdata('menu4') == 't') {
                        echo $row->i_user . ",Master Lokasi Gudang,storelocation/cform/<br>";
                    }
                    if ($this->session->userdata('menu5') == 't') {
                        echo $row->i_user . ",Master Jenis BBK,bbktype/cform/<br>";
                    }
                    if ($this->session->userdata('menu6') == 't') {
                        echo $row->i_user . ",Master Jenis BBM,bbmtype/cform/<br>";
                    }
                    if ($this->session->userdata('menu206') == 't') {
                        echo $row->i_user . ",Master Expedisi,exp/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu225') == 't')
                ) {
                    echo '<li><a href="#">Pembelian</a><ul>';
                    if ($this->session->userdata('menu225') == 't') {
                        echo $row->i_user . ",Master Harga (HJP),producthjp/cform/<br>";
                    }
                    echo '</ul>';
                }
                if ($this->session->userdata('menu88') == 't') {
                    echo $row->i_user . ",Jenis Promo,promotype/cform/<br>";
                }
                if ($this->session->userdata('menu53') == 't') {
                    echo $row->i_user . ",Status OP,opstatus/cform/<br>";
                }
                if (
                    ($this->session->userdata('menu443') == 't') or ($this->session->userdata('menu444') == 't')
                    or ($this->session->userdata('menu445') == 't') or ($this->session->userdata('menu448') == 't')
                ) {
                    //* DICOMMENT TIDAK DIPAKAI DI CINTAKA
                    echo '<li><a href="#">Konsinyasi</a><ul>';
                    if ($this->session->userdata('menu443') == 't') {
                        echo $row->i_user . ",SPG,spg/cform/<br>";
                    }
                    if ($this->session->userdata('menu444') == 't') {
                        echo $row->i_user . ",Toko Kosinyasi,tokokons/cform/<br>";
                    }
                    if ($this->session->userdata('menu445') == 't') {
                        echo $row->i_user . ",Harga Barang Kosinyasi,productpriceco/cform/<br>";
                    }
                    if ($this->session->userdata('menu448') == 't') {
                        echo $row->i_user . ",List Data Harga Barang Kosinyasi,listbrgkons/cform/<br>";
                    }
                    echo '</ul>';
                }
                if ($this->session->userdata('departement') == "1") {
                } else if (
                    ($this->session->userdata('menu600') == 't')
                ) {
                    //* DICOMMENT TIDAK DIPAKAI DI CINTAKA
                    echo '<li><a href="#">Karyawan</a><ul>';
                    if ($this->session->userdata('menu600') == 't') {
                        echo $row->i_user . ",Karyawan,karyawan/cform/<br>";
                    }
                    if ($this->session->userdata('menu598') == 't') {
                        echo $row->i_user . ",Reminder Kontrak Karyawan,reminderkontrak/cform/<br>";
                    }
                    if (
                        ($this->session->userdata('menu191') == 't')
                    ) {
                        echo '<li><a href="#">Cuti</a><ul>';
                        if ($this->session->userdata('menu588') == 't') {
                            echo $row->i_user . ",Data Cuti,karyawancuti/cform/<br>";
                        }
                        /*if ($this->session->userdata('menu589') == 't') {
                            echo $row->i_user.",Input Cuti,karyawancutiinput/cform/<br>";
                        }*/
                        echo '</ul>';
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu144') == 't') or ($this->session->userdata('menu185') == 't')
                ) {
                    //* DICOMMENT TIDAK DIPAKAI DI CINTAKA
                    echo '<li><a href="#">Kendaraan</a><ul>';
                    if ($this->session->userdata('menu144') == 't') {
                        echo $row->i_user . ",Input Data Kendaraan,kendaraan/cform/<br>";
                    }
                    if ($this->session->userdata('menu185') == 't') {
                        echo $row->i_user . ",Pindah Periode,ppkendaraan/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu0') == 't') or ($this->session->userdata('menu68') == 't') or
                    ($this->session->userdata('menu93') == 't') or ($this->session->userdata('menu232') == 't') or
                    ($this->session->userdata('menu500') == 't')
                ) {
                    echo '<li><a href="#">Setting</a><ul>';
                    if ($this->session->userdata('menu0') == 't') {
                        echo $row->i_user . ",Admin User,user/cform/<br>";
                    }
                    if ($this->session->userdata('menu93') == 't') {
                        echo $row->i_user . ",Printer,printersetting/cform/<br>";
                    }
                    if ($this->session->userdata('menu68') == 't') {
                        echo $row->i_user . ",Password,gantipassword/cform/<br>";
                    }
                    if ($this->session->userdata('menu232') == 't') {
                        echo $row->i_user . ",User Log,userlog/cform/<br>";
                    }
                    if ($this->session->userdata('menu500') == 't') {
                        # $row->i_user.e,cho "Transfer All,transall/cform/<br>";
                    }
                    echo '</ul>';
                }
                echo '</ul>';
            }

            //* PARENT GUDANG
            if (
                ($this->session->userdata('menu8') == 't') or ($this->session->userdata('menu156') == 't') or
                ($this->session->userdata('menu86') == 't') or ($this->session->userdata('menu73') == 't') or
                ($this->session->userdata('menu74') == 't') or ($this->session->userdata('menu7') == 't') or
                ($this->session->userdata('menu75') == 't') or ($this->session->userdata('menu77') == 't') or
                ($this->session->userdata('menu79') == 't') or ($this->session->userdata('menu84') == 't') or
                ($this->session->userdata('menu50') == 't') or ($this->session->userdata('menu51') == 't') or
                ($this->session->userdata('menu87') == 't') or ($this->session->userdata('menu109') == 't') or
                ($this->session->userdata('menu156') == 't') or ($this->session->userdata('menu193') == 't') or
                ($this->session->userdata('menu160') == 't') or ($this->session->userdata('menu182') == 't') or
                ($this->session->userdata('menu75') == 't') or ($this->session->userdata('menu172') == 't') or
                ($this->session->userdata('menu179') == 't') or ($this->session->userdata('menu194') == 't') or
                ($this->session->userdata('menu195') == 't') or ($this->session->userdata('menu196') == 't') or
                ($this->session->userdata('menu207') == 't') or ($this->session->userdata('menu208') == 't') or
                ($this->session->userdata('menu210') == 't') or ($this->session->userdata('menu213') == 't') or
                ($this->session->userdata('menu215') == 't') or ($this->session->userdata('menu223') == 't') or
                ($this->session->userdata('menu224') == 't') or ($this->session->userdata('menu256') == 't') or
                ($this->session->userdata('menu183') == 't') or ($this->session->userdata('menu72') == 't') or
                ($this->session->userdata('menu288') == 't') or ($this->session->userdata('menu289') == 't') or
                ($this->session->userdata('menu299') == 't') or ($this->session->userdata('menu311') == 't') or
                ($this->session->userdata('menu314') == 't') or ($this->session->userdata('menu319') == 't') or
                ($this->session->userdata('menu321') == 't') or ($this->session->userdata('menu330') == 't') or
                ($this->session->userdata('menu331') == 't') or ($this->session->userdata('menu349') == 't') or
                ($this->session->userdata('menu366') == 't') or ($this->session->userdata('menu370') == 't') or
                ($this->session->userdata('menu379') == 't') or ($this->session->userdata('menu391') == 't') or
                ($this->session->userdata('menu419') == 't') or ($this->session->userdata('menu420') == 't') or
                ($this->session->userdata('menu424') == 't') or ($this->session->userdata('menu425') == 't') or
                ($this->session->userdata('menu425') == 't')
            ) {
                echo '<li><a href="#">Gudang</a><ul>';
                if ($this->session->userdata('menu391') == 't') {
                    if ($this->session->userdata('departement') != '1') {
                        echo $row->i_user . ",Nilai Stock (HPP),hpp/cform/<br>";
                    }
                    echo $row->i_user . ",Mutasi Nilai Stock (HPP),mutasihpp/cform/<br>";
                }
                if (
                    ($this->session->userdata('menu79') == 't') or ($this->session->userdata('menu84') == 't')
                ) {
                    echo '<li><a href="#">TTB</a><ul>';
                    if ($this->session->userdata('menu79') == 't') {
                        echo $row->i_user . ",TTB Tolak,ttbtolak/cform/<br>";
                    }
                    if ($this->session->userdata('menu84') == 't') {
                        echo $row->i_user . ",TTB Retur,ttbretur/cform/<br>";
                    }
                    echo '</ul>';
                }
                if ($this->session->userdata('menu424') == 't') {
                    echo $row->i_user . ",Bon Masuk,bonmasuk/cform/<br>";
                }
                if ($this->session->userdata('menu425') == 't') {
                    echo $row->i_user . ",Bon Keluar,bonkeluar/cform/<br>";
                }
                if (
                    ($this->session->userdata('menu86') == 't') or ($this->session->userdata('menu156') == 't') or
                    ($this->session->userdata('menu207') == 't') or ($this->session->userdata('menu208') == 't') or
                    ($this->session->userdata('menu210') == 't') or ($this->session->userdata('menu213') == 't') or
                    ($this->session->userdata('menu215') == 't') or ($this->session->userdata('menu289') == 't') or
                    ($this->session->userdata('menu299') == 't') or ($this->session->userdata('menu301') == 't') or
                    ($this->session->userdata('menu309') == 't') or ($this->session->userdata('menu311') == 't') or
                    ($this->session->userdata('menu314') == 't') or ($this->session->userdata('menu330') == 't') or
                    ($this->session->userdata('menu505') == 't')
                ) {
                    echo '<li><a href="#">Surat Jalan</a><ul>';
                    if ($this->session->userdata('menu86') == 't') {
                        echo $row->i_user . ",Surat Jalan,sj/cform/<br>";
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu442') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('SJ mundur,sjmds/cform/<br>";
                    }
                    //*
                    //* DICOMMENT MO
                    if ($this->session->userdata('menu289') == 't') {
                        // $row->i_user.e,cho "Surat Jalan PB,sjpb/cform/<br>";
                    }
                    if ($this->session->userdata('menu490') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Surat Jalan PB Manual,sjpbmanual2/cform/<br>";
                    }
                    //*
                    //* DICOMMENT
                    if ($this->session->userdata('menu569') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Surat Jalan E-Commerce,sje/cform/<br>";
                    }
                    if ($this->session->userdata('menu597') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Surat Jalan PB Cabang,sjpbcabang/cform/<br>";
                    }
                    //*
                    if ($this->session->userdata('menu156') == 't') {
                        echo $row->i_user . ",SJ Pinjaman,sjp/cform/<br>";
                    }
                    if ($this->session->userdata('menu207') == 't') {
                        echo $row->i_user . ",SJP Receive,sjpreceive/cform/<br>";
                    }
                    if ($this->session->userdata('menu208') == 't') {
                        echo $row->i_user . ",SJ Retur,sjretur/cform/<br>";
                    }
                    if ($this->session->userdata('menu505') == 't') {
                        echo $row->i_user . ",SJ Terima Toko,sjreceive/cform/<br>";
                    }
                    if ($this->session->userdata('menu210') == 't') {
                        echo $row->i_user . ",SJR Receive,sjrreceive/cform/<br>";
                    }
                    if ($this->session->userdata('menu213') == 't') {
                        echo $row->i_user . ",SJ Retur Toko,sjrt/cform/<br>";
                    }
                    if ($this->session->userdata('menu215') == 't') {
                        echo $row->i_user . ",SJ Tolak,sjtolak/cform/<br>";
                    }
                    if ($this->session->userdata('menu234') == 't') {
                        echo $row->i_user . ",Transfer SJ,transsj/cform/<br>";
                    }
                    if ($this->session->userdata('menu299') == 't') {
                        echo $row->i_user . ",SJPB Receive,sjpbreceive/cform/<br>";
                    }
                    if ($this->session->userdata('menu301') == 't') {
                        echo $row->i_user . ",SJPB Retur,sjpbretur/cform/<br>";
                    }
                    if ($this->session->userdata('menu311') == 't') {
                        echo $row->i_user . ",SJB Retur Konsinyasi,sjbretur/cform/<br>";
                    }
                    if ($this->session->userdata('menu309') == 't') {
                        echo $row->i_user . ",SJPB Retur Receive,sjpbreturreceive/cform/<br>";
                    }
                    if ($this->session->userdata('menu314') == 't') {
                        echo $row->i_user . ",SJB Retur Receive,sjbrreceive/cform/<br>";
                    }
                    if ($this->session->userdata('menu330') == 't') {
                        echo $row->i_user . ",SJ Receive Toko,sjreceivetoko/cform/<br>";
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu379') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Rekap DO,rekapdo/cform/<br>";
                    }
                    if ($this->session->userdata('menu379') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Rekap DO FC,rekapdofc/cform/<br>";
                    }
                    //*
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu8') == 't') or ($this->session->userdata('menu193') == 't') or
                    ($this->session->userdata('menu73') == 't') or ($this->session->userdata('menu74') == 't') or
                    ($this->session->userdata('menu195') == 't') or ($this->session->userdata('menu196') == 't') or
                    ($this->session->userdata('menu256') == 't') or ($this->session->userdata('menu183') == 't') or
                    ($this->session->userdata('menu72') == 't') or ($this->session->userdata('menu288') == 't') or
                    ($this->session->userdata('menu331') == 't') or ($this->session->userdata('menu379') == 't')
                ) {
                    echo '<li><a href="#">SPmB</a><ul>';
                    if ($this->session->userdata('menu8') == 't') {
                        echo $row->i_user . ",SPmB,spmbnew/cform/<br>";
                    }
                    if ($this->session->userdata('menu256') == 't') {
                        echo $row->i_user . ",SPmB Manual,spmbnewmanual/cform/<br>";
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu507') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('SPmB Manual (MO),spmbnewmanualmo/cform/<br>";
                    }
                    //*
                    if ($this->session->userdata('menu331') == 't') {
                        echo $row->i_user . ",SPmB BBM-AP,spmbbbmap/cform/<br>";
                    }
                    if ($this->session->userdata('menu183') == 't') {
                        echo $row->i_user . ",SPmB Acc,spmbacc/cform/<br>";
                    }
                    if ($this->session->userdata('menu72') == 't') {
                        echo $row->i_user . ",Approve,spmbapprovefa/cform/<br>";
                    }
                    if ($this->session->userdata('menu73') == 't') {
                        echo $row->i_user . ",Realisasi,spmbrealisasi/cform/<br>";
                    }
                    if ($this->session->userdata('menu74') == 't') {
                        echo $row->i_user . ",Pemenuhan,spmbop/cform/<br>";
                    }
                    if ($this->session->userdata('menu379') == 't') {
                        echo $row->i_user . ",Rekap SPB,rekapspb/cform/<br>";
                    }
                    if ($this->session->userdata('menu379') == 't') {
                        echo $row->i_user . ",Rekap SPB Khusus,rekapspbpemenuhan/cform/<br>";
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu446') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Rekap Order Konsinyasi,rekaporderkons/cform/<br>";
                    }
                    //*
                    if ($this->session->userdata('menu193') == 't') {
                        echo $row->i_user . ",Closing,spmbclose/cform/<br>";
                    }
                    if ($this->session->userdata('menu195') == 't') {
                        echo $row->i_user . ",Import,spmbimport/cform/<br>";
                    }
                    if ($this->session->userdata('menu288') == 't') {
                        echo $row->i_user . ",Import Konsinyasi,spmbimportkons/cform/<br>";
                    }
                    if ($this->session->userdata('menu196') == 't') {
                        echo $row->i_user . ",Upload,spmbupload/upload/<br>";
                    }
                    if ($this->session->userdata('menu298') == 't') {
                        echo $row->i_user . ",Transfer SPMB,transspmb/cform/<br>";
                    }
                    echo '</ul>';
                }
                //* DICOMMENT
                if (($this->session->userdata('menu194') == 't') or ($this->session->userdata('menu475') == 't') or ($this->session->userdata('menu321') == 't')) {
                    // echo '<li><a href="#">Mutasi</a><ul>';
                    // 	if($this->session->userdata('menu194')=='t'){
                    // $row->i_user.	,echo "Update Mutasi Stock,mutasi/cform/<br>";
                    // 	}
                    // 	//* DICOMMENT
                    // 	if($this->session->userdata('menu194')=='t'){
                    // $row->i_user.	,	// echo "Update Mutasi Stock On Hand Pusat,mutasiohpusat/cform/<br>";
                    // 	}
                    // 	//*
                    // 	if($this->session->userdata('menu475')=='t'){
                    // $row->i_user.	,	echo "Update Mutasi Stock On Hand,mutasioh/cform/<br>";
                    // 	}
                    // 	if($this->session->userdata('menu321')=='t'){
                    // $row->i_user.	,	echo "Update Mutasi Kons,mutasipb/cform/<br>";
                    // 	}
                    // echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu160') == 't') or ($this->session->userdata('menu182') == 't')
                ) {
                    echo '<li><a href="#">DKB</a><ul>';
                    if ($this->session->userdata('menu160') == 't') {
                        echo $row->i_user . ",DKB,dkb/cform/<br>";
                    }
                    if ($this->session->userdata('menu182') == 't') {
                        echo $row->i_user . ",Approve,dkbapprove/cform/<br>";
                    }
                    echo '</ul>';
                }
                //* DICOMMENT
                if ($this->session->userdata('menu453') == 't') {
                    // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Bon Masuk Lain2,bonmasuklain/cform/<br>";
                }
                //*

                if (
                    ($this->session->userdata('menu7') == 't') or ($this->session->userdata('menu75') == 't') or
                    ($this->session->userdata('menu223') == 't') or ($this->session->userdata('menu224') == 't') or
                    ($this->session->userdata('menu319') == 't') or ($this->session->userdata('menu366') == 't') or
                    ($this->session->userdata('menu419') == 't') or ($this->session->userdata('menu420') == 't') or
                    ($this->session->userdata('menu432') == 't')
                ) {
                    echo '<li><a href="#">Stock</a><ul>';
                    if ($this->session->userdata('menu223') == 't') {
                        echo $row->i_user . ",Upload SO,soupload/upload/<br>";
                    }
                    if ($this->session->userdata('menu224') == 't') {
                        echo $row->i_user . ",Transfer SO,transferso/cform/<br>";
                    }
                    if ($this->session->userdata('menu7') == 't') {
                        echo $row->i_user . ",Stock Opname,stockopname/cform/<br>";
                    }
                    if ($this->session->userdata('menu319') == 't') {
                        echo $row->i_user . ",SO Konsinyasi,sopb/cform/<br>";
                    }
                    if ($this->session->userdata('menu366') == 't') {
                        echo $row->i_user . ",Transfer SO Konsinyasi (Counter),transfersokons/cform/<br>";
                    }
                    if ($this->session->userdata('menu75') == 't') {
                        echo $row->i_user . ",Konversi Stock,stockconvertion/cform/<br>";
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu75') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Konversi Stock A ke B,stockconvertionab/cform/<br>";
                    }
                    if ($this->session->userdata('menu75') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Konversi Stock B ke A,stockconvertionba/cform/<br>";
                    }
                    //*
                    if ($this->session->userdata('menu432') == 't') {
                        echo $row->i_user . ",Konversi Stock BBM,stockconvertionbbm/cform/<br>";
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu476') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Konversi Stock SJR,stockconvertionsjr/cform/<br>";
                    }
                    //*
                    if ($this->session->userdata('menu419') == 't') {
                        echo $row->i_user . ",Upload SO PB,sopbupload/upload/<br>";
                    }
                    if ($this->session->userdata('menu420') == 't') {
                        echo $row->i_user . ",Transfer SO PB,transfersopb/cform/<br>";
                    }
                    if ($this->session->userdata('menu483') == 't') {
                        echo $row->i_user . ",Stock Adjustment,adjustment/cform/<br>";
                    }
                    if ($this->session->userdata('menu485') == 't') {
                        echo $row->i_user . ",Adjustment Approve,adjustmentapprove/cform/<br>";
                    }
                    if ($this->session->userdata('menu596') == 't') {
                        echo $row->i_user . ",Adjustment Counter,adjustmentcounter/cform/<br>";
                    }
                    if ($this->session->userdata('menu595') == 't') {
                        echo $row->i_user . ",Adjustment Counter Approve,adjustmentapprovecounter/cform/<br>";
                    }
                    echo '</ul>';
                }
                if ($this->session->userdata('menu77') == 't') {
                    echo $row->i_user . ",BBM-AP,bbmap/cform/<br>";
                }
                if (
                    ($this->session->userdata('menu50') == 't') or ($this->session->userdata('menu51') == 't') or
                    ($this->session->userdata('menu87') == 't') or ($this->session->userdata('menu370') == 't')
                ) {
                    echo '<li><a href="#">SPB</a><ul>';
                    if ($this->session->userdata('menu370') == 't') {
                        echo $row->i_user . ",Terima SPB,terimaspb/cform/<br>";
                    }
                    if ($this->session->userdata('menu50') == 't') {
                        echo $row->i_user . ",Realisasi,spbrealisasi/cform/<br>";
                    }
                    if ($this->session->userdata('menu50') == 't') {
                        echo $row->i_user . ",Realisasi ke Pemenuhan,spbrealisasipemenuhan/cform/<br>";
                        echo $row->i_user . ",Realisasi Ulang,spbrealisasiulang/cform/<br>";
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu447') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Realisasi Konsinyasi,realisasispbkons/cform/<br>";
                    }
                    //*
                    if ($this->session->userdata('menu51') == 't') {
                        echo $row->i_user . ",Pemenuhan,spbop/cform/<br>";
                    }
                    if ($this->session->userdata('menu87') == 't') {
                        echo $row->i_user . ",Siap SJ,siapnotagudang/cform/<br>";
                    }
                    echo '</ul>';
                }
                if ($this->session->userdata('menu109') == 't') {
                    echo $row->i_user . ",BBM-Retur,bbmretur/cform/<br>";
                    echo $row->i_user . ",BBM-Retur(Barter),bbmbarter/cform/<br>";
                }
                if ($this->session->userdata('menu349') == 't') {
                    echo $row->i_user . ",BBK-Hadiah,bbkhadiah/cform/<br>";
                }
                //* DICOMMENT
                if ($this->session->userdata('menu349') == 't') {
                    // $row->i_user.e,cho "BBK-Retur,bbkbarter/cform/<br>";
                }
                //*
                //* DICOMMENT
                if ($this->session->userdata('menu468') == 't') {
                    // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('BBK-Retur (Supplier),bbkretur/cform/<br>";
                }
                //*
                if ($this->session->userdata('menu349') == 't') {
                    echo $row->i_user . ",BBK-Retur,bbkbarter/cform/<br>";
                }
                if (
                    ($this->session->userdata('menu172') == 't') or ($this->session->userdata('menu179') == 't')
                ) {
                    echo '<li><a href="#">BAPB</a><ul>';
                    if ($this->session->userdata('menu172') == 't') {
                        echo $row->i_user . ",BAPB-SJ,bapb/cform/<br>";
                    }
                    if ($this->session->userdata('menu179') == 't') {
                        echo $row->i_user . ",BAPB-SJP,bapbsjp/cform/<br>";
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu172') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('BAPB-SJPB,bapbsjpb/cform/<br>";
                    }
                    if ($this->session->userdata('menu179') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('BAPB-SJP Receive,bapbsjpReceive/cform/<br>";
                    }
                    //*
                    echo '</ul>';
                }
                echo '</ul>';
            }

            //* PARENT PENJUALAN				
            if (
                ($this->session->userdata('menu47') == 't') or ($this->session->userdata('menu48') == 't') or
                ($this->session->userdata('menu161') == 't') or ($this->session->userdata('menu97') == 't') or
                ($this->session->userdata('menu91') == 't') or ($this->session->userdata('menu89') == 't') or
                ($this->session->userdata('menu103') == 't') or ($this->session->userdata('menu183') == 't') or
                ($this->session->userdata('menu187') == 't') or ($this->session->userdata('menu168') == 't') or
                ($this->session->userdata('menu197') == 't') or ($this->session->userdata('menu218') == 't') or
                ($this->session->userdata('menu219') == 't') or ($this->session->userdata('menu221') == 't') or
                ($this->session->userdata('menu245') == 't') or ($this->session->userdata('menu246') == 't') or
                ($this->session->userdata('menu249') == 't') or ($this->session->userdata('menu159') == 't') or
                ($this->session->userdata('menu250') == 't') or ($this->session->userdata('menu293') == 't') or
                ($this->session->userdata('menu296') == 't') or ($this->session->userdata('menu340') == 't') or
                ($this->session->userdata('menu341') == 't') or ($this->session->userdata('menu412') == 't')
            ) {
                echo '<li><a href="#">Penjualan</a><ul>';
                if (
                    ($this->session->userdata('menu47') == 't') or ($this->session->userdata('menu48') == 't') or
                    ($this->session->userdata('menu91') == 't') or ($this->session->userdata('menu159') == 't') or
                    ($this->session->userdata('menu161') == 't') or ($this->session->userdata('menu187') == 't') or
                    ($this->session->userdata('menu103') == 't') or ($this->session->userdata('menu197') == 't') or
                    ($this->session->userdata('menu218') == 't') or ($this->session->userdata('menu219') == 't') or
                    ($this->session->userdata('menu221') == 't') or ($this->session->userdata('menu249') == 't') or
                    ($this->session->userdata('menu250') == 't') or ($this->session->userdata('menu341') == 't') or
                    ($this->session->userdata('menu412') == 't') or ($this->session->userdata('menu442') == 't') or
                    ($this->session->userdata('menu449') == 't')
                ) {
                    echo '<li><a href="#">SPB</a><ul>';
                    if (
                        ($this->session->userdata('menu47') == 't') or ($this->session->userdata('menu159') == 't') or
                        ($this->session->userdata('menu197') == 't') or ($this->session->userdata('menu249') == 't') or
                        ($this->session->userdata('menu442') == 't')
                    ) {
                        echo '<li><a href="#">SPB</a><ul>';
                        if (($this->session->userdata('menu47') == 't' && substr($this->session->userdata('user_id'), 5, 2) != 'bj') or ($this->session->userdata('menu47') == 't' && substr($this->session->userdata('user_id'), 1, 5) != 'sales')) {
                            echo $row->i_user . ",Baby Bedding,spbbaby/cform/<br>";
                        }
                        //* DICOMMENT
                        if ($this->session->userdata('menu161') == 't') {
                            // $row->i_user.e,cho "Dialogue Home,spbpromoreguler/cform/<br>";
                        }
                        //*
                        if (($this->session->userdata('menu249') == 't' && substr($this->session->userdata('user_id'), 5, 2) == 'bj') or ($this->session->userdata('menu249') == 't' && substr($this->session->userdata('user_id'), 1, 5) != 'sales')) {
                            echo $row->i_user . ",Baby Non Bedding,spbnb/cform/<br>";
                        }
                        //* DICOMMENT
                        if ($this->session->userdata('menu249') == 't') {
                            // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Dialogue Fashion,spbfashion/cform/<br>";
                        }
                        //*
                        if ($this->session->userdata('menu197') == 't') {
                            echo $row->i_user . ",Pelanggan Baru,customernew/cform/<br>";
                        }
                        if ($this->session->userdata('menu442') == 't') {
                            //* DICOMMENT
                            // $row->i_user.e,cho "Konsinyasi,spbkonsinyasi/cform/<br>";
                            //*
                        }
                        if ($this->session->userdata('menu442') == 't') {
                            //* DICOMMENT
                            // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Konsinyasi Mundur,spbkonsinyasimds/cform/<br>";
                            //*
                        }
                        if ($this->session->userdata('menu481') == 't') {
                            echo $row->i_user . ",MO,spbmo/cform/<br>";
                            echo $row->i_user . ",MO + Margin,spbmoplus/cform/<br>";
                            //*
                        }
                        //* DICOMMENT
                        if ($this->session->userdata('menu47') == 't') {
                            // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('E-Commerce,spboo/cform/<br>";
                        }
                        echo '</ul>';
                    }
                    if (
                        ($this->session->userdata('menu91') == 't') or ($this->session->userdata('menu161') == 't') or
                        ($this->session->userdata('menu250') == 't')
                    ) {
                        echo '<li><a href="#">SPB Promo</a><ul>';
                        if ($this->session->userdata('menu91') == 't') {
                            echo $row->i_user . ",Beby Bedding,spbpromobaby/cform/<br>";
                        }
                        if ($this->session->userdata('menu161') == 't') {
                            echo $row->i_user . ",Dialogue Home,spbpromoreguler/cform/<br>";
                        }
                        if ($this->session->userdata('menu250') == 't') {
                            echo $row->i_user . ",Baby Non Bedding,spbpromonb/cform/<br>";
                        }
                        echo '</ul>';
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu249') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Dialogue Fashion,spbpromofashion/cform/<br>";
                    }
                    //*
                    if ($this->session->userdata('menu48') == 't') {
                        echo $row->i_user . ",Approve,spbapprovesales/cform/<br>";
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu500') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Not Approve,spbnotapprovefa/cform/<br>";
                    }
                    //*
                    if ($this->session->userdata('menu187') == 't') {
                        echo $row->i_user . ",Cek SPB,spbceksales/cform/<br>";
                    }
                    if ($this->session->userdata('menu239') == 't') {
                        echo $row->i_user . ",Cek SPB Cabang,spbcekcabang/cform/<br>";
                    }
                    if ($this->session->userdata('menu103') == 't') {
                        echo $row->i_user . ",Siap SJ,siapnotasales/cform/<br>";
                    }
                    if ($this->session->userdata('menu218') == 't') {
                        echo $row->i_user . ",Transfer SPB Baru,transspb/cform/<br>";
                    }
                    if ($this->session->userdata('menu221') == 't') {
                        echo $row->i_user . ",Upload SPB Lama,spbupload/upload/<br>";
                    }
                    if ($this->session->userdata('menu219') == 't') {
                        echo $row->i_user . ",Transfer SPB Lama,transspblama/cform/<br>";
                    }
                    if ($this->session->userdata('menu341') == 't') {
                        echo $row->i_user . ",Pindah Periode,spbpindahperiode/cform/<br>";
                    }
                    if ($this->session->userdata('menu412') == 't') {
                        echo $row->i_user . ",Ubah Kode Sales,ubahsalesspb/cform/<br>";
                    }
                    if ($this->session->userdata('menu442') == 't') {
                        echo $row->i_user . ",Transfer SPB Kons,transspbkons/cform/<br>";
                    }
                    echo '</ul>';
                }
                if ($this->session->userdata('menu89') == 't') {
                    echo $row->i_user . ",Promo,promo/cform/<br>";
                }
                if ($this->session->userdata('menu97') == 't') {
                    echo $row->i_user . ",Target Penjualan,salestarget/cform/<br>";
                }
                if ($this->session->userdata('menu340') == 't') {
                    echo $row->i_user . ",Update Penjualan,updatepenjualan/cform/<br>";
                }
                if ($this->session->userdata('menu343') == 't') {
                    echo $row->i_user . ",Update Kunjungan,updatekunjungan/cform/<br>";
                }
                if ($this->session->userdata('menu168') == 't') {
                    echo $row->i_user . ",RRKH,rrkh/cform/<br>";
                }
                if ($this->session->userdata('menu245') == 't') {
                    echo $row->i_user . ",Transfer RRKH,transrrkh/cform/<br>";
                }
                if ($this->session->userdata('menu246') == 't') {
                    echo $row->i_user . ",Transfer TTB Retur,transttbretur/cform/<br>";
                }
                if (($this->session->userdata('menu293') == 't') or ($this->session->userdata('menu296') == 't')) {
                    //* NON AKTIF 19 JUN 2021
                    // echo '<li><a href="#">Mutasi Stock</a><ul>';
                    if ($this->session->userdata('menu293') == 't') {
                        // $row->i_user.e,cho "Mutasi STock Subdisk,mutstock/cform/<br>";
                    }
                    //*
                    //* DICOMMENT
                    // echo '<li><a href="#">Konsinyasi</a><ul>';
                    // 	if($this->session->userdata('menu293')=='t'){
                    // $row->i_user.	,	echo "Penjualan Konsinyasi,penjualankonsinyasi/cform/<br>";
                    // 	}
                    // 	if($this->session->userdata('menu296')=='t'){
                    // $row->i_user.	,	echo "Order Konsinyasi,orderkonsinyasi/cform/<br>";
                    // 	}
                    // 	if($this->session->userdata('menu449')=='t'){
                    // $row->i_user.	,	echo "Cek Penjualan Konsinyasi,cekbonkons/cform/<br>";
                    // 	}
                    // 	if($this->session->userdata('menu449')=='t'){
                    // $row->i_user.	,	echo "BON Konsinyasi,bonkonsinyasi/cform/<br>";
                    // 	}
                    // 	//* DICOMMENT
                    // 	if ($this->session->userdata('menu146') == 't') {
                    // $row->i_user.	,	echo "<li>" . $this->pquery->link_to_remote('SJPB Ke SJ,sjpbsj/cform/<br>";
                    // 	}
                    // echo '</ul>';
                    //*
                }
                if ($this->session->userdata('menu49') == 't') {
                    //* DICOMMENT
                    // $row->i_user.e,cho "Penjualan Manual,penjualan_manual/cform/<br>";
                    //*
                }
                echo '</ul>';
            }

            //* PARENT PEMBELIAN		
            if (($this->session->userdata('departement') == '1')) {
            } else if (
                ($this->session->userdata('menu52') == 't') or ($this->session->userdata('menu54') == 't') or
                ($this->session->userdata('menu55') == 't') or ($this->session->userdata('menu65') == 't') or
                ($this->session->userdata('menu66') == 't') or ($this->session->userdata('menu67') == 't') or
                ($this->session->userdata('menu181') == 't') or ($this->session->userdata('menu240') == 't') or
                ($this->session->userdata('menu241') == 't') or ($this->session->userdata('menu278') == 't') or
                ($this->session->userdata('menu279') == 't') or ($this->session->userdata('menu381') == 't')
            ) {
                echo '<li><a href="#">Pembelian</a><ul>';
                if (
                    ($this->session->userdata('menu52') == 't') or ($this->session->userdata('menu54') == 't') or
                    ($this->session->userdata('menu55') == 't') or ($this->session->userdata('menu181') == 't') or
                    ($this->session->userdata('menu278') == 't') or ($this->session->userdata('menu281') == 't') or
                    ($this->session->userdata('menu381') == 't')
                ) {
                    echo '<li><a href="#">OP</a><ul>';
                    if ($this->session->userdata('menu52') == 't') {
                        echo $row->i_user . ",OP,opnew/cform/<br>";
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu52') == 't') {
                        // $row->i_user.e,cho "OP (diskon item),opnew-diskonitem/cform/<br>";
                    }
                    //*
                    if ($this->session->userdata('menu381') == 't') {
                        echo $row->i_user . ",OP (Rekap SPB),opnew-rekapspb/cform/<br>";
                    }
                    if ($this->session->userdata('menu54') == 't') {
                        echo $row->i_user . ",Closing OP,opclose/cform/<br>";
                    }
                    if ($this->session->userdata('menu55') == 't') {
                        echo $row->i_user . ",Cancel OP,opcancel/cform/<br>";
                    }
                    if ($this->session->userdata('menu181') == 't') {
                        echo $row->i_user . ",OP (BBM-AP),opbbmap/cform/<br>";
                    }
                    if ($this->session->userdata('menu278') == 't') {
                        echo $row->i_user . ",Transfer OP Baru ke Lama,transop/cform/<br>";
                    }
                    if ($this->session->userdata('menu281') == 't') {
                        echo $row->i_user . ",Transfer OP ke Excel,transoputksupp/cform/<br>";
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu529') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('OP Forecast,opforecast/cform/<br>";
                    }
                    if ($this->session->userdata('menu529') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('OP Forecast New,opfcnew/cform/<br>";
                    }
                    if ($this->session->userdata('menu529') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Input Saldo Awal Forecast,saldofc/cform/<br>";
                    }
                    if ($this->session->userdata('menu529') == 't') {
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Mutasi Forecast,listmutasifc/cform/<br>";
                    }
                    //*
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu65') == 't') or ($this->session->userdata('menu66') == 't') or
                    ($this->session->userdata('menu67') == 't') or ($this->session->userdata('menu222') == 't') or
                    ($this->session->userdata('menu279') == 't')
                ) {
                    echo '<li><a href="#">Delivery Order</a><ul>';
                    if ($this->session->userdata('menu65') == 't') {
                        echo $row->i_user . ",Transfer Duta,transdo/cform/<br>";
                        //* DICOMMENT
                        // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Transfer Duta FC,transdofc/cform/<br>";
                        //*
                    }
                    if ($this->session->userdata('menu65') == 't') {
                        //* DICOMMENT
                        // $row->i_user.e,cho "Transfer DO,transdo/cform/<br>";
                        //*
                    }
                    if (($this->session->userdata('menu65') == 't') or ($this->session->userdata('allmenu') == 't')) {
                        echo $row->i_user . ",Transfer DO Tirai,transferdo-tirai/cform/<br>";
                    }
                    if ($this->session->userdata('menu222') == 't') {
                        echo $row->i_user . ",Transfer Other,transdoother/cform/<br>";
                    }
                    if ($this->session->userdata('menu279') == 't') {
                        echo $row->i_user . ",Transfer Baru Ke Lama,transdobl/cform/<br>";
                    }
                    if ($this->session->userdata('menu66') == 't') {
                        echo $row->i_user . ",Upload,doupload/upload/<br>";
                    }
                    if ($this->session->userdata('menu67') == 't') {
                        echo $row->i_user . ",Manual,domanual/cform/<br>";
                    }
                    //* DICOMMENT
                    if ($this->session->userdata('menu67') == 't') {
                        // $row->i_user.e,cho "DO (diskon item),btb-diskonitem/cform/<br>";
                    }
                    //*
                    //* DICOMMENT
                    if ($this->session->userdata('menu530') == 't') {
                        // $row->i_user.e,cho "Forecast,doforecast/cform/<br>','update' => '#main'))."</li>";
                    }
                    //*
                    echo '</ul>';
                }
                if ($this->session->userdata('menu240') == 't') {
                    echo $row->i_user . ",Transfer Harga Baru,transharga/cform/<br>";
                }
                if ($this->session->userdata('menu241') == 't') {
                    echo $row->i_user . ",Upload Harga Baru,uploadharga/upload/<br>";
                }
                echo '</ul>';
            }

            //* PARENT KEUANGAN				
            if (
                ($this->session->userdata('menu49') == 't') or ($this->session->userdata('menu56') == 't') or
                ($this->session->userdata('menu81') == 't') or ($this->session->userdata('menu101') == 't') or
                ($this->session->userdata('menu105') == 't') or ($this->session->userdata('menu106') == 't') or
                ($this->session->userdata('menu111') == 't') or ($this->session->userdata('menu113') == 't') or
                ($this->session->userdata('menu134') == 't') or ($this->session->userdata('menu135') == 't') or
                ($this->session->userdata('menu136') == 't') or ($this->session->userdata('menu140') == 't') or
                ($this->session->userdata('menu165') == 't') or ($this->session->userdata('menu171') == 't') or
                ($this->session->userdata('menu188') == 't') or ($this->session->userdata('menu189') == 't') or
                ($this->session->userdata('menu190') == 't') or ($this->session->userdata('menu202') == 't') or
                ($this->session->userdata('menu226') == 't') or ($this->session->userdata('menu243') == 't') or
                ($this->session->userdata('menu253') == 't') or ($this->session->userdata('menu271') == 't') or
                ($this->session->userdata('menu272') == 't') or ($this->session->userdata('menu318') == 't') or
                ($this->session->userdata('menu328') == 't') or ($this->session->userdata('menu329') == 't') or
                ($this->session->userdata('menu332') == 't') or ($this->session->userdata('menu335') == 't') or
                ($this->session->userdata('menu336') == 't') or ($this->session->userdata('menu346') == 't') or
                ($this->session->userdata('menu395') == 't') or ($this->session->userdata('menu399') == 't') or
                ($this->session->userdata('menu400') == 't') or ($this->session->userdata('menu417') == 't') or
                ($this->session->userdata('menu418') == 't')
            ) {
                echo '<li><a href="#">Keuangan</a><ul>';
                if (
                    ($this->session->userdata('menu56') == 't') or ($this->session->userdata('menu81') == 't') or
                    ($this->session->userdata('menu271') == 't') or ($this->session->userdata('menu400') == 't')
                ) {
                    echo '<li><a href="#">Nota</a><ul>';
                    if ($this->session->userdata('menu56') == 't') {
                        echo $row->i_user . ",Nota,nota/cform/<br>";
                        # $row->i_user. ,            echo "Nota (auto),notaauto/cform/<br>";
                    }
                    if ($this->session->userdata('menu81') == 't') {
                        echo $row->i_user . ",Koreksi Nota,notakoreksi/cform/<br>";
                    }
                    if ($this->session->userdata('menu271') == 't') {
                        echo $row->i_user . ",Transfer Nota,transnota/cform/<br>";
                    }
                    if ($this->session->userdata('menu400') == 't') {
                        echo $row->i_user . ",Ubah Keterangan,updatenotainsentif/cform/<br>";
                    }
                    if ($this->session->userdata('menu417') == 't') {
                        echo $row->i_user . ",TTD Nota,ttdnota/cform/<br>";
                    }
                    if ($this->session->userdata('menu452') == 't') {
                        echo $row->i_user . ",Batal Nota,notabatal/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu49') == 't')
                ) {
                    echo '<li><a href="#">SPB</a><ul>';
                    if ($this->session->userdata('menu49') == 't') {
                        echo $row->i_user . ",Approve,spbapprovefa/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu188') == 't') or ($this->session->userdata('menu189') == 't') or
                    ($this->session->userdata('menu190') == 't') or ($this->session->userdata('menu418') == 't') or
                    ($this->session->userdata('menu494') == 't') or ($this->session->userdata('menu525') == 't')
                ) {
                    echo '<li><a href="#">Cek Pelaporan Daerah</a><ul>';
                    if ($this->session->userdata('menu525') == 't') {
                        echo $row->i_user . ",Daftar Tagihan,cekdt/cform/<br>";
                    }
                    if ($this->session->userdata('menu600') == 't') {
                        echo $row->i_user . ",Alokasi Kredit Nota,cekknalokasi/cform/<br>";
                    }
                    if ($this->session->userdata('menu188') == 't') {
                        echo $row->i_user . ",Alokasi Bank Masuk,cekalokasi/cform/<br>";
                    }
                    if ($this->session->userdata('menu188') == 't') {
                        echo $row->i_user . ",Pelunasan Piutang,cekpelunasan/cform/<br>";
                    }
                    if ($this->session->userdata('menu189') == 't') {
                        echo $row->i_user . ",IKHP,cekikhp/cform/<br>";
                    }
                    if ($this->session->userdata('menu190') == 't') {
                        echo $row->i_user . ",IKHP (Pengeluaran) ,cekikhpkeluar/cform/<br>";
                    }
                    if ($this->session->userdata('menu418') == 't') {
                        echo $row->i_user . ",Edit Ket cek Pelunasan,cekplubahket/cform/<br>";
                    }
                    if ($this->session->userdata('menu494') == 't') {
                        echo $row->i_user . ",Setor Tunai,cekrtunai/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu226') == 't')
                ) {
                    echo '<li><a href="#">Pelanggan</a><ul>';
                    if ($this->session->userdata('menu226') == 't') {
                        echo $row->i_user . ",Approve,customerapprovear/cform/<br>";
                    }
                    echo '</ul>';
                }
                if ($this->session->userdata('menu101') == 't') {
                    echo $row->i_user . ",Daftar Tagihan,daftartagihan/cform/<br>";
                }
                if ($this->session->userdata('menu318') == 't') {
                    #echo $row->i_user.",DT Manual,dtmanual/cform/<br>";
                }
                if ($this->session->userdata('menu328') == 't') {
                    #echo $row->i_user.",Hitung Plafond Pelunasan,hitungplafond/cform/<br>";
                    echo $row->i_user . ",Hitung Plafond Alokasi,hitungplafond/cform/<br>";
                    echo $row->i_user . ",Upload Acc Plafond,upload-plafonacc/upload/<br>";
                    echo $row->i_user . ",Transfer Acc Plafond,transferaccplafon/cform/<br>";
                }
                if (
                    ($this->session->userdata('menu335') == 't') or ($this->session->userdata('menu336') == 't')
                ) {
                    echo '<li><a href="#">Collection</a><ul>';
                    if ($this->session->userdata('menu335') == 't') {
                        echo $row->i_user . ",Update Target Collection(New),targetcollectionnew/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu329') == 't') or ($this->session->userdata('menu346') == 't')
                ) {
                    echo '<li><a href="#">Insentif</a><ul>';
                    if ($this->session->userdata('menu329') == 't') {
                        echo $row->i_user . ",Hitung Insentif,hitunginsentif/cform/<br>";
                    }
                    if ($this->session->userdata('menu346') == 't') {
                        echo $row->i_user . ",Hitung Insentif AS,hitunginsentifas/cform/<br>";
                    }
                    echo '</ul>';
                }
                // if(
                //   ($this->session->userdata('menu105')=='t')or($this->session->userdata('menu243')=='t')
                //   )
                // {
                // 	echo '<li><a href="#">Pelunasan Piutang</a><ul>';
                // 	if($this->session->userdata('menu105')=='t'){
                // $row->i_user.	,  echo "Input Pelunasan,pelunasan/cform/<br>";
                //   }
                // 	if($this->session->userdata('menu243')=='t'){
                // $row->i_user.	,	echo "Transfer Pelunasan,transpl/cform/<br>";
                //   }
                // 	echo '</ul>';
                // }//tadinya tag pelunasan piutang di comment 

                if (
                    ($this->session->userdata('menu105') == 't') or ($this->session->userdata('menu243') == 't') or
                    ($this->session->userdata('menu523') == 't') or ($this->session->userdata('menu521') == 't') or
                    ($this->session->userdata('menu526') == 't')
                ) {
                    echo '<li><a href="#">Alokasi</a><ul>';
                    if ($this->session->userdata('menu105') == 't') {
                        echo $row->i_user . ",Alokasi Kredit Nota(Non Retur),knalokasi/cform/<br>";
                        #	$row->i_user.	,						echo "Input Pelunasan,pelunasan/cform/<br>";
                    }
                    if ($this->session->userdata('menu521') == 't') {
                        echo $row->i_user . ",Alokasi Kredit Nota Retur,knralokasi/cform/<br>";
                    }
                    if ($this->session->userdata('menu243') == 't') {
                        #	$row->i_user.	,						echo "Transfer Pelunasan,transpl/cform/<br>";
                    }
                    if ($this->session->userdata('menu523') == 't') {
                        echo $row->i_user . ",Bank (Masuk Alokasi),akt-bankin-multialloc/cform/<br>";
                        echo $row->i_user . ",Bank (Masuk Alokasi+Meterai),akt-bankin-multiallocnew/cform/<br>";
                        echo $row->i_user . ",Bank (Masuk Alokasi Meterai),akt-bankin-mt/cform/<br>";
                    }
                    //* PEMUTIHAN METERAI DEVELOP 14 JUN 2021
                    if (/* $this->session->userdata('menu536') == 't' */$this->session->userdata('user_id') == 'admin') {
                        echo $row->i_user . ",Alokasi Meterai,akt-meterai/cform/<br>";
                    }
                    if ($this->session->userdata('menu502') == 't') {
                        echo $row->i_user . ",Hutang Lain2,alokasihutanglain/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu105') == 't') or ($this->session->userdata('menu243') == 't')
                ) {
                    echo '<li><a href="#">Alokasi Debet Nota AP</a><ul>';
                    if ($this->session->userdata('menu105') == 't') {
                        echo $row->i_user . ",Alokasi Debet Nota AP,dnalokasi/cform/<br>";
                        #	$row->i_user.	,						echo "Input Pelunasan,pelunasan/cform/<br>";
                    }
                    if ($this->session->userdata('menu243') == 't') {
                        #	$row->i_user.	,						echo "Transfer Pelunasan,transpl/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu106') == 't') or ($this->session->userdata('menu202') == 't') or
                    ($this->session->userdata('menu136') == 't')
                ) {
                    echo '<li><a href="#">Giro</a><ul>';
                    if ($this->session->userdata('menu106') == 't') {
                        echo $row->i_user . ",Giro Pelanggan,giro/cform/<br>";
                    }
                    if ($this->session->userdata('menu202') == 't') {
                        echo $row->i_user . ",Giro Cair Pelanggan,girocair/cform/<br>";
                    }
                    if ($this->session->userdata('menu136') == 't') {
                        echo $row->i_user . ",Giro DGU,giro-dgu/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu111') == 't') or ($this->session->userdata('menu113') == 't') or
                    ($this->session->userdata('menu272') == 't')
                ) {
                    echo '<li><a href="#">Kredit Nota</a><ul>';
                    if ($this->session->userdata('menu111') == 't') {
                        echo $row->i_user . ",Kredit Nota Retur,knretur/cform/<br>";
                    }
                    if ($this->session->userdata('menu113') == 't') {
                        echo $row->i_user . ",Kredit Nota non Retur,knnonretur/cform/<br>";
                    }
                    if ($this->session->userdata('menu272') == 't') {
                        echo $row->i_user . ",Transfer Kredit Nota,transkn/cform/<br>";
                    }
                    echo '</ul>';
                }
                if ($this->session->userdata('menu253') == 't') {
                    echo $row->i_user . ",Debet Nota,debetnota/cform/<br>";
                }

                #25 Agustus 2021
                if ($this->session->userdata('menu519') == 't') {
                    echo $row->i_user . ",Debet Nota A/P,debetnota-ap/cform/<br>";
                }

                if ($this->session->userdata('menu134') == 't') {
                    echo $row->i_user . ",Hutang Dagang,daftartagihanap/cform/<br>";
                }
                if ($this->session->userdata('menu374') == 't') {
                    echo $row->i_user . ",Transfer Hutang Dagang,transap/cform/<br>";
                }
                if ($this->session->userdata('menu332') == 't') {
                    echo $row->i_user . ",Hutang Dagang (BBM-AP),daftartagihanbbmap/cform/<br>";
                }
                if ($this->session->userdata('menu524') == 't') {

                    echo '<li><a href="#">Pelunasan Hutang</a><ul>';
                    #	$row->i_user.	,					echo "Input Pelunasan,pelunasan-ap/cform/<br>";
                    #	$row->i_user.	,					echo "Transfer Pelunasan,transplap/cform/<br>";
                    echo $row->i_user . ",Bank (Keluar / Alokasi),akt-bank-multialloc/cform/<br>";
                    echo $row->i_user . ",Kas Besar (Keluar / Alokasi),akt-kb-multialloc/cform/<br>";
                    echo '</ul>';
                }
                // 		if($this->session->userdata('menu135')=='t'){
                // $row->i_user.e,cho "Pelunasan Hutang,pelunasan-ap/cform/<br>";
                // 		}
                if ($this->session->userdata('menu399') == 't') {
                    echo $row->i_user . ",Cek Pelunasan Hutang,cekpelunasanhutang/cform/<br>";
                }
                if (
                    ($this->session->userdata('menu140') == 't') or ($this->session->userdata('menu165') == 't')
                ) {
                    echo '<li><a href="#">KU</a><ul>';
                    if ($this->session->userdata('menu140') == 't') {
                        echo $row->i_user . ",KU / Transfer Masuk,transferuangmasuk/cform/<br>";
                    }
                    if ($this->session->userdata('menu165') == 't') {
                        echo $row->i_user . ",KU / Transfer Keluar,transferuangkeluar/cform/<br>";
                    }
                    echo '</ul>';
                }
                if ($this->session->userdata('menu490') == 't') {
                    echo $row->i_user . ",Tunai Item ,tunaidetail/cform/<br>";
                }
                if ($this->session->userdata('menu491') == 't') {
                    echo $row->i_user . ",Setor Tunai ,rtunai/cform/<br>";
                }
                if ($this->session->userdata('menu171') == 't') {
                    echo $row->i_user . ",IKHP (Pengeluaran) ,ikhpkeluar/cform/<br>";
                }
                echo '</ul>';
            }

            if (
                (($this->session->userdata('level') == '6') && ($this->session->userdata('departement') == '1'))
            ) {
                echo '<li><a href="#">Akunting</a><ul>';
                if ($this->session->userdata('user_id') == 'fs1' || $this->session->userdata('user_id') == 'fs2') {
                    echo $row->i_user . ",Closing Kas Bank,closingkasbank/cform/<br>";
                }
                echo '</ul>';
            } else if (
                (($this->session->userdata('menu115') == 't') or ($this->session->userdata('menu118') == 't') or
                    ($this->session->userdata('menu119') == 't') or ($this->session->userdata('menu120') == 't') or
                    ($this->session->userdata('menu121') == 't') or ($this->session->userdata('menu122') == 't') or
                    ($this->session->userdata('menu123') == 't') or ($this->session->userdata('menu124') == 't') or
                    ($this->session->userdata('menu125') == 't') or ($this->session->userdata('menu127') == 't') or
                    ($this->session->userdata('menu128') == 't') or ($this->session->userdata('menu142') == 't') or
                    ($this->session->userdata('menu146') == 't') or ($this->session->userdata('menu151') == 't') or
                    ($this->session->userdata('menu143') == 't') or ($this->session->userdata('menu242') == 't') or
                    ($this->session->userdata('menu502') == 't') or ($this->session->userdata('menu231') == 't')) &&
                ($this->session->userdata('user_id') != 'akt-cbp' /*&& $this->session->userdata('user_id')!='fs1' && $this->session->userdata('user_id')!='fs2'*/)
            ) {
                echo '<li><a href="#">Akunting</a><ul>';
                if ($this->session->userdata('menu115') == 't') {
                    echo $row->i_user . ",Master Saldo Account,coasaldo/cform/<br>";
                }
                if ($this->session->userdata('menu118') == 't') {
                    echo $row->i_user . ",CoA,coa/cform/<br>";
                }
                if ($this->session->userdata('menu125') == 't') {
                    echo $row->i_user . ",Jurnal Umum,akt-gj/cform/<br>";
                }

                if (
                    ($this->session->userdata('menu146') == 't') or ($this->session->userdata('menu231') == 't') or
                    ($this->session->userdata('menu151') == 't') or ($this->session->userdata('menu242') == 't') or
                    ($this->session->userdata('menu502') == 't') or ($this->session->userdata('menu512') == 't')
                ) {
                    echo '<li><a href="#">Kas</a><ul>';
                    if ($this->session->userdata('menu502') == 't') {
                        echo $row->i_user . ",Kas Besar (keluar),akt-kb-multi/cform/<br>";
                        echo $row->i_user . ",Kas Besar (Masuk),akt-kbin-multi/cform/<br>";
                        echo $row->i_user . ",Bank (keluar),akt-bank-multi/cform/<br>";
                        echo $row->i_user . ",Bank (Masuk),akt-bankin-multi/cform/<br>";
                        # $row->i_user. ,							echo "Bank (Masuk / Alokasi),akt-bankin-multialloc/cform/<br>";
                        #	$row->i_user.	,					echo "Bank (Keluar / Alokasi),akt-bank-multialloc/cform/<br>";
                    }
                    if ($this->session->userdata('menu231') == 't') {
                        echo $row->i_user . ",Kas Kecil,akt-kk-multi/cform/<br>";
                    }
                    if ($this->session->userdata('menu151') == 't') {
                        echo $row->i_user . ",Pengisian Kas Kecil,akt-pkk/cform/<br>";
                    }
                    if ($this->session->userdata('menu242') == 't') {
                        echo $row->i_user . ",Close Periode(Kas Kecil),closing/cform/<br>";
                    }
                    if ($this->session->userdata('menu512') == 't') {
                        echo $row->i_user . ",Close Periode All,closingperiode/cform/<br>";
                    }

                    if (
                        (($this->session->userdata('level') == '4') && ($this->session->userdata('departement') == '1')) ||
                        (($this->session->userdata('level') == '0') && ($this->session->userdata('departement') == '0'))
                    ) {
                        echo $row->i_user . ",Closing Kas Bank,closingkasbank/cform/<br>";
                    }
                    echo '</ul>';

                    echo '<li><a href="#">Cek Pelaporan Daerah</a><ul>';
                    if ($this->session->userdata('menu191') == 't') {
                        echo $row->i_user . ",Kas Kecil ,cek-akt-kk/cform/<br>";
                    }
                    echo '</ul>';
                }

                if (
                    ($this->session->userdata('menu121') == 't') or ($this->session->userdata('menu119') == 't') or
                    ($this->session->userdata('menu123') == 't') or ($this->session->userdata('menu127') == 't') or
                    ($this->session->userdata('menu142') == 't')
                ) {
                    echo '<li><a href="#">Posting</a><ul>';
                    if ($this->session->userdata('menu121') == 't') {
                        echo $row->i_user . ",Penjualan,akt-nota/cform/<br>";
                    }
                    if ($this->session->userdata('menu119') == 't') {
                        echo $row->i_user . ",Pelunasan Piutang,akt-pelunasan/cform/<br>";
                    }
                    if ($this->session->userdata('menu142') == 't') {
                        echo $row->i_user . ",Pelunasan Hutang,akt-pelunasanap/cform/<br>";
                    }
                    if ($this->session->userdata('menu123') == 't') {
                        echo $row->i_user . ",KN,akt-kn/cform/<br>";
                    }
                    if ($this->session->userdata('menu148') == 't') {
                        echo $row->i_user . ",Kas Kecil,akt-kk-pst/cform/<br>";
                    }
                    if ($this->session->userdata('menu152') == 't') {
                        echo $row->i_user . ",Pengisian Kas Kecil,akt-pkk-pst/cform/<br>";
                    }
                    if ($this->session->userdata('menu127') == 't') {
                        echo $row->i_user . ",Jurnal Umum,akt-gj-pst/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu122') == 't') or ($this->session->userdata('menu120') == 't') or
                    ($this->session->userdata('menu124') == 't') or ($this->session->userdata('menu128') == 't') or
                    ($this->session->userdata('menu143') == 't')
                ) {
                    echo '<li><a href="#">UnPosting</a><ul>';
                    if ($this->session->userdata('menu122') == 't') {
                        echo $row->i_user . ",Penjualan,akt-nota-un/cform/<br>";
                    }
                    if ($this->session->userdata('menu120') == 't') {
                        echo $row->i_user . ",Pelunasan Piutang,akt-pelunasan-un/cform/<br>";
                    }
                    if ($this->session->userdata('menu143') == 't') {
                        echo $row->i_user . ",Pelunasan Hutang,akt-pelunasanap-un/cform/<br>";
                    }
                    if ($this->session->userdata('menu124') == 't') {
                        echo $row->i_user . ",KN,akt-kn-un/cform/<br>";
                    }
                    if ($this->session->userdata('menu149') == 't') {
                        echo $row->i_user . ",Kas Kecil,akt-kk-un/cform/<br>";
                    }
                    if ($this->session->userdata('menu153') == 't') {
                        echo $row->i_user . ",Pengisian Kas Kecil,akt-pkk-un/cform/<br>";
                    }
                    if ($this->session->userdata('menu128') == 't') {
                        echo $row->i_user . ",Jurnal Umum,akt-gj-un/cform/<br>";
                    }
                    echo '</ul>';
                }
                echo '</ul>';
            }
            if (
                ($this->session->userdata('menu57') == 't') or ($this->session->userdata('menu58') == 't') or
                ($this->session->userdata('menu59') == 't') or ($this->session->userdata('menu60') == 't') or
                ($this->session->userdata('menu61') == 't') or ($this->session->userdata('menu70') == 't') or
                ($this->session->userdata('menu76') == 't') or ($this->session->userdata('menu78') == 't') or
                ($this->session->userdata('menu80') == 't') or ($this->session->userdata('menu82') == 't') or
                ($this->session->userdata('menu85') == 't') or ($this->session->userdata('menu90') == 't') or
                ($this->session->userdata('menu92') == 't') or ($this->session->userdata('menu96') == 't') or
                ($this->session->userdata('menu98') == 't') or ($this->session->userdata('menu99') == 't') or
                ($this->session->userdata('menu100') == 't') or ($this->session->userdata('menu112') == 't') or
                ($this->session->userdata('menu114') == 't') or ($this->session->userdata('menu116') == 't') or
                ($this->session->userdata('menu129') == 't') or ($this->session->userdata('menu130') == 't') or
                ($this->session->userdata('menu131') == 't') or ($this->session->userdata('menu132') == 't') or
                ($this->session->userdata('menu133') == 't') or ($this->session->userdata('menu135') == 't') or
                ($this->session->userdata('menu137') == 't') or ($this->session->userdata('menu145') == 't') or
                ($this->session->userdata('menu211') == 't') or ($this->session->userdata('menu209') == 't') or
                ($this->session->userdata('menu212') == 't') or ($this->session->userdata('menu214') == 't') or
                ($this->session->userdata('menu216') == 't') or ($this->session->userdata('menu228') == 't') or
                ($this->session->userdata('menu229') == 't') or ($this->session->userdata('menu237') == 't') or
                ($this->session->userdata('menu244') == 't') or ($this->session->userdata('menu252') == 't') or
                ($this->session->userdata('menu254') == 't') or ($this->session->userdata('menu258') == 't') or
                ($this->session->userdata('menu259') == 't') or ($this->session->userdata('menu263') == 't') or
                ($this->session->userdata('menu265') == 't') or ($this->session->userdata('menu266') == 't') or
                ($this->session->userdata('menu270') == 't') or ($this->session->userdata('menu273') == 't') or
                ($this->session->userdata('menu277') == 't') or ($this->session->userdata('menu283') == 't') or
                ($this->session->userdata('menu291') == 't') or ($this->session->userdata('menu292') == 't') or
                ($this->session->userdata('menu294') == 't') or ($this->session->userdata('menu295') == 't') or
                ($this->session->userdata('menu317') == 't') or ($this->session->userdata('menu300') == 't') or
                ($this->session->userdata('menu302') == 't') or ($this->session->userdata('menu310') == 't') or
                ($this->session->userdata('menu312') == 't') or ($this->session->userdata('menu315') == 't') or
                ($this->session->userdata('menu320') == 't') or ($this->session->userdata('menu306') == 't') or
                ($this->session->userdata('menu322') == 't') or ($this->session->userdata('menu324') == 't') or
                ($this->session->userdata('menu325') == 't') or ($this->session->userdata('menu327') == 't') or
                ($this->session->userdata('menu337') == 't') or ($this->session->userdata('menu338') == 't') or
                ($this->session->userdata('menu343') == 't') or ($this->session->userdata('menu347') == 't') or
                ($this->session->userdata('menu352') == 't') or ($this->session->userdata('menu354') == 't') or
                ($this->session->userdata('menu359') == 't') or ($this->session->userdata('menu361') == 't') or
                ($this->session->userdata('menu362') == 't') or ($this->session->userdata('menu364') == 't') or
                ($this->session->userdata('menu363') == 't') or ($this->session->userdata('menu371') == 't') or
                ($this->session->userdata('menu376') == 't') or ($this->session->userdata('menu387') == 't') or
                ($this->session->userdata('menu388') == 't') or ($this->session->userdata('menu389') == 't') or
                ($this->session->userdata('menu390') == 't') or ($this->session->userdata('menu397') == 't') or
                ($this->session->userdata('menu402') == 't') or ($this->session->userdata('menu403') == 't') or
                ($this->session->userdata('menu410') == 't') or ($this->session->userdata('menu415') == 't') or
                ($this->session->userdata('menu422') == 't') or ($this->session->userdata('menu430') == 't') or
                ($this->session->userdata('menu434') == 't') or ($this->session->userdata('menu435') == 't') or
                ($this->session->userdata('menu426') == 't') or ($this->session->userdata('menu427') == 't') or
                ($this->session->userdata('menu529') == 't') or ($this->session->userdata('menu530') == 't') or
                ($this->session->userdata('menu267') == 't') or ($this->session->userdata('menu147') == 't')
            ) {
                echo '<li><a href="#">Informasi</a><ul>';
                if (
                    ($this->session->userdata('menu57') == 't') or ($this->session->userdata('menu94') == 't') or
                    ($this->session->userdata('menu95') == 't') or ($this->session->userdata('menu96') == 't') or
                    ($this->session->userdata('menu98') == 't') or ($this->session->userdata('menu99') == 't') or
                    ($this->session->userdata('menu100') == 't') or ($this->session->userdata('menu90') == 't') or
                    ($this->session->userdata('menu169') == 't') or ($this->session->userdata('menu170') == 't') or
                    ($this->session->userdata('menu327') == 't') or ($this->session->userdata('menu343') == 't') or
                    ($this->session->userdata('menu359') == 't') or ($this->session->userdata('menu361') == 't') or
                    ($this->session->userdata('menu362') == 't') or ($this->session->userdata('menu364') == 't') or
                    ($this->session->userdata('menu354') == 't') or ($this->session->userdata('menu397') == 't') or
                    ($this->session->userdata('menu403') == 't') or ($this->session->userdata('menu410') == 't') or
                    ($this->session->userdata('menu415') == 't') or ($this->session->userdata('menu422') == 't') or
                    ($this->session->userdata('menu430') == 't') or ($this->session->userdata('menu529') == 't') or
                    ($this->session->userdata('menu530') == 't')
                ) {
                    echo '<li><a href="#">Penjualan</a><ul>';
                    if (
                        ($this->session->userdata('menu57') == 't') or ($this->session->userdata('menu94') == 't') or
                        ($this->session->userdata('menu95') == 't') or ($this->session->userdata('menu96') == 't') or
                        ($this->session->userdata('menu283') == 't') or ($this->session->userdata('menu327') == 't') or
                        ($this->session->userdata('menu376') == 't') or ($this->session->userdata('menu402') == 't') or
                        ($this->session->userdata('menu403') == 't') or ($this->session->userdata('menu430') == 't') or
                        ($this->session->userdata('menu529') == 't') or ($this->session->userdata('menu530') == 't')
                    ) {
                        echo '<li><a href="#">SPB</a><ul>';
                        if ($this->session->userdata('menu57') == 't') {
                            echo $row->i_user . ",SPB,listspb/cform/<br>";
                            if ($this->session->userdata('departement') != '1') {
                                // $row->i_user. , echo "Status,listcabangspb/cform/<br>";
                            }
                        }
                        if ($this->session->userdata('menu94') == 't') {
                            echo $row->i_user . ",Per Area,spbperarea/cform/<br>";
                        }
                        if ($this->session->userdata('menu95') == 't') {
                        }
                        if ($this->session->userdata('menu96') == 't') {
                            echo $row->i_user . ",per Salesman,spbpersalesman/cform/<br>";
                        }
                        if ($this->session->userdata('menu430') == 't') {
                            echo $row->i_user . ",Laporan SPB per Bulan,listspbperbulan/cform/<br>";
                        }
                        if ($this->session->userdata('menu327') == 't') {
                            echo $row->i_user . ",Pending,listspbpending/cform/<br>";
                        }
                        if ($this->session->userdata('menu533') == 't') {
                            echo $row->i_user . ",List SPB Tidak 100%,listspbtidakpenuh/cform/<br>";
                        }
                        if ($this->session->userdata('menu376') == 't') {
                            echo $row->i_user . ",Terima SPB,listterimaspb/cform/<br>";
                        }
                        if ($this->session->userdata('menu403') == 't') {
                            echo $row->i_user . ",Per Divisi,listspbperdivisi/cform/<br>";
                            echo $row->i_user . ",Per Divisi (Rekap Area),listspbperdivisirekap/cform/<br>";
                        }
                        if ($this->session->userdata('menu406') == 't') {
                            echo $row->i_user . ",Realisasi Pemenuhan,listspbrealisasi/cform/<br>";
                            echo $row->i_user . ",Realisasi Pemenuhan Pending,listspbrealisasipending/cform/<br>";
                        }
                        if ($this->session->userdata('menu465') == 't') {
                            echo $row->i_user . ",SPB vs SJ,spbvssj/cform/<br>";
                            //echo $row->i_user.",SPB vs SJ Per Group Barang,spbvssjgrupbrg/cform/<br>";
                            echo $row->i_user . ",Rekap Realisasi SPB Per Area,realisasispbvssj/cform/<br>";
                            echo $row->i_user . ",Rekap Realisasi SPB Per Produk,realisasispbperproduk/cform/<br>";
                        }
                        if ($this->session->userdata('menu529') == 't') {
                            echo $row->i_user . ",Informasi Status,infostatus/cform/<br>";
                        }
                        if ($this->session->userdata('menu530') == 't') {
                            echo '<li><a href="#">Informasi Gudang</a><ul>';
                            echo $row->i_user . ",Gudang Pusat,infogudangpusat/cform/<br>";
                            echo $row->i_user . ",Gudang Cabang,infogudangcabang/cform/<br>";
                            echo '</ul></li>';
                        }
                        echo '</ul></li>';
                    }

                    if (
                        ($this->session->userdata('menu292') == 't') or ($this->session->userdata('menu359') == 't') or
                        ($this->session->userdata('menu362') == 't') or ($this->session->userdata('menu364') == 't') or
                        ($this->session->userdata('menu361') == 't') or ($this->session->userdata('menu371') == 't') or
                        ($this->session->userdata('menu387') == 't') or ($this->session->userdata('menu410') == 't') or
                        ($this->session->userdata('menu422') == 't')
                    ) {
                        echo '<li><a href="#">Penjualan</a><ul>';
                        if ($this->session->userdata('menu497') == 't') {
                            echo $row->i_user . ",Harga Beli,listpenjualanhargabeli/cform/<br>";
                            echo $row->i_user . ",Harga Jual,listpenjualanhargajual/cform/<br>";
                            echo $row->i_user . ",Jenis Barang,listpenjualanjenisbarang/cform/<br>";
                        }
                        if ($this->session->userdata('menu292') == 't') {
                            echo '<li><a href="#">per Pelanggan</a><ul>';
                            echo $row->i_user . ",per bulan,listpenjualanperpelanggan/cform/<br>";
                            echo $row->i_user . ",per periode,listpenjualanperpelangganrata/cform/<br>";
                            echo '</ul></li>';
                        }
                        if ($this->session->userdata('menu359') == 't') {
                            echo $row->i_user . ",per Salesman Rp.,listpenjualanpersalesman/cform/<br>";
                            echo $row->i_user . ",per Salesman Qty,listpenjualanpersalesmanqty/cform/<br>";
                        }
                        if ($this->session->userdata('menu500') == 't') {
                            //echo $row->i_user.",per Salesman (Daerah),listpenjualanpersalesmandaerah/cform/<br>";
                        }
                        if ($this->session->userdata('menu361') == 't') {
                            // $row->i_user.e,cho "per Produk Qty,listpenjualanperprodukbln/cform/<br>";
                            echo $row->i_user . ",per Produk Qty,listpenjualanperproduk/cform/<br>";
                            echo $row->i_user . ",per Produk Rp.,listpenjualanperprodukrpbln/cform/<br>";
                            echo $row->i_user . ",per Produk/Toko Qty,listpenjualanperproduktk/cform/<br>";
                        }
                        if ($this->session->userdata('menu362') == 't') {
                            echo $row->i_user . ",per Pelanggan (SPB),listpenjualanperpelangganspb/cform/<br>";
                        }
                        if ($this->session->userdata('menu364') == 't') {
                            echo $row->i_user . ",per Produk (SPB),listpenjualanperprodukspb/cform/<br>";
                        }
                        if ($this->session->userdata('menu371') == 't') {
                            echo '<li><a href="#">per Nota</a><ul>';
                            echo $row->i_user . ",per Area,listpenjualanpernota/cform/<br>";

                            echo $row->i_user . ",Nasional,listpenjualanpernotanasional/cform/<br>";
                            echo '</ul></li>';
                        }
                        if ($this->session->userdata('menu387') == 't') {
                            echo $row->i_user . ",per Divisi,listpenjualanperdivisi/cform/<br>";
                            echo $row->i_user . ",per Divisi per Kelas,listpenjualanperdivisikelas-bln/cform/<br>";
                            echo $row->i_user . ",per Divisi per Product,listpenjualanperdivisiperproduk/cform/<br>";
                        }
                        if ($this->session->userdata('menu410') == 't') {
                            echo $row->i_user . ",Laporan AS,lapas/cform/<br>";
                        }
                        if ($this->session->userdata('menu422') == 't') {
                            echo $row->i_user . ",Laporan per Bulan,listpenjualanperbulan/cform/<br>";
                            echo $row->i_user . ",Laporan per Bulan (Gross),listpenjualanperbulangross/cform/<br>";
                            echo '<li><a href="#">Laporan per Bulan per Divisi</a><ul>';
                            if ($this->session->userdata('menu422') == 't') {
                                echo $row->i_user . ",Moms Baby,listpenjualanperbulanbedding/cform/<br>";
                                echo $row->i_user . ",Baby Joy,listpenjualanperbulannonbedding/cform/<br>";
                            }
                            echo "</ul></li>";
                            echo $row->i_user . ",Laporan per Kota,listpenjualanperkota/cform/<br>";
                            echo $row->i_user . ",Customer Card (Rp),listcustomercardrp/cform/<br>";
                            echo $row->i_user . ",Customer Card (Nota),listcustomercard/cform/<br>";
                            echo $row->i_user . ",Customer Card (Supplier),listcustomercardpersupp/cform/<br>";
                            echo $row->i_user . ",Customer Card (Supplier Rp.),listcustomercardpersupprp/cform/<br>";
                            echo $row->i_user . ",Customer Card (SPB),listcustomercardspb/cform/<br>";
                            echo $row->i_user . ",Customer Card (SPB) Rp.,listcustomercardspbrp/cform/<br>";
                        }
                        if ($this->session->userdata('user_id') == 'admin') {
                            echo $row->i_user . ",Customer Card (NEW),listcustomercardmix/cform/<br>";
                        }
                        echo '</ul>';
                    }
                    if (
                        ($this->session->userdata('menu98') == 't') or ($this->session->userdata('menu99') == 't') or
                        ($this->session->userdata('menu100') == 't') or ($this->session->userdata('menu177') == 't')
                    ) {
                        echo '<li><a href="#">Target Penjualan</a><ul>';
                        if ($this->session->userdata('menu98') == 't') {
                            echo $row->i_user . ",per Area,listtpperarea/cform/<br>";
                        }
                        if ($this->session->userdata('menu99') == 't') {
                            echo $row->i_user . ",per Salesman,listtppersalesman/cform/<br>";
                        }
                        if ($this->session->userdata('menu100') == 't') {
                            echo $row->i_user . ",per Kota,listtppercity/cform/<br>";
                        }
                        if ($this->session->userdata('menu177') == 't') {
                        }
                        echo '</ul></li>';
                    }
                    //SALES REPORT						
                    if (
                        ($this->session->userdata('menu598') == 't') or ($this->session->userdata('menu599') == 't')
                    ) {
                        echo '<li><a href="#">Sales Report</a><ul>';
                        if ($this->session->userdata('menu598') == 't') {
                            echo $row->i_user . ",% Ctr by Island,ctrbyisland/cform/<br>";
                            echo $row->i_user . ",% Ctr by Island detail,ctrbyislandperarea/cform/<br>";
                            echo $row->i_user . ",% Ctr by Category detail,ctrbycategorydetail-perpulau/cform/<br>";
                            echo $row->i_user . ",% Ctr by Area,ctrbyarea/cform/<br>";
                        }
                        if ($this->session->userdata('menu599') == 't') {
                            echo $row->i_user . ",Sales Performance,salesperformance/cform/<br>";
                            echo $row->i_user . ",% Growth SO VS OA,growthsovsoa/cform/<br>";
                            echo $row->i_user . ",% Ctr by Klasifikasi,ctrbyklasifikasimarket/cform/<br>";
                            echo $row->i_user . ",% Ctr by Brand,ctrbybrand/cform/<br>";
                            echo $row->i_user . ",10 Toko Pareto,list-tokopareto/cform/<br>";
                            echo $row->i_user . ",% Ctr by Category,ctrbycategory/cform/<br>";
                            echo $row->i_user . ",% OA vs OB by Item Product,oaobbyitemproduct/cform/<br>";
                            echo $row->i_user . ",% Growth by item product,growthbyitemproduct/cform/<br>";
                            echo $row->i_user . ",% Growth per item toko,growthperitemtoko/cform/<br>";
                            echo $row->i_user . ",Activity Salesman,activitysalesman/cform/<br>";
                        }
                        echo '</ul></li>';
                        echo '<li><a href="#">Sales Monitoring</a><ul>';
                        echo $row->i_user . ",Penjualan per Area,srpenjualanperarea/cform/<br>";
                        echo $row->i_user . ",Penjualan per Area per Kota,srpenjualanperareaperkota/cform/<br>";
                        echo $row->i_user . ",Penjualan per Area per Toko,srpenjualanperareapertoko/cform/<br>";
                        echo $row->i_user . ",Penjualan per Area per Produk,srpenjualanperareaperproduk/cform/<br>";
                        echo $row->i_user . ",Penjualan per minggu per Area,srpenjualanpermingguperarea/cform/<br>";
                        echo $row->i_user . ",Penjualan per minggu per Area Kota Sales,srpenjualanpermingguperareaperkotasales/cform/<br>";
                        echo $row->i_user . ",Penjualan per minggu per Area per Toko Produk,srpenjualanpermingguperareapertoko/cform/<br>";
                        echo $row->i_user . ",Penjualan per minggu per Produk,srpenjualanpermingguperproduk/cform/<br>";
                        echo '</ul></li>';
                    }
                    //END SALES REPORT
                    //SALES REPORT 2	

                    if (($this->session->userdata('menu599') == 't')) {
                        echo '<li><a href="#">Sales Report 2</a><ul>';
                        if (($this->session->userdata('menu599') == 't') or ($this->session->userdata('menu2') == 't')) {
                            echo '<li><a href="#">Sales Performance</a><ul>';
                            if ($this->session->userdata('menu599') == 't') {
                                echo $row->i_user . ",Sales Performance All(Rp),salesperformance_new/cform/<br>";
                            }
                            if ($this->session->userdata('menu599') == 't') {
                                echo $row->i_user . ",Sales Performance Cintaka(Rp),salesperformance_new_cintaka/cform/<br>";
                            }
                            if ($this->session->userdata('menu599') == 't') {
                                echo $row->i_user . ",Sales Performance Subdist(Rp),salesperformance_new_subdisk/cform/<br>";
                            }
                            if ($this->session->userdata('menu1') == 't') {
                                echo $row->i_user . ",Sales Performance All(Qty),salesperformance_newqty/cform/<br>";
                            }
                            if ($this->session->userdata('menu1') == 't') {
                                echo $row->i_user . ",Sales Performance Cintaka(Qty),salesperformance_new_cintakaqty/cform/<br>";
                            }
                            if ($this->session->userdata('menu1') == 't') {
                                echo $row->i_user . ",Sales Performance Subdist(Qty),salesperformance_new_subdiskqty/cform/<br>";
                            }
                            echo '</ul>';
                        }
                        #echo $row->i_user.",Sales Performance,salesperformance_new/cform/<br>";
                        echo $row->i_user . ",Sales Performance By Island,ctrbyisland_new/cform/<br>";
                        echo $row->i_user . ",Sales Performance By Trend Toko Per Item,trendperitemtoko/cform/<br>";
                        echo $row->i_user . ",Sales Performance By Trend Per Toko,trendpertoko/cform/<br>";
                        echo $row->i_user . ",Sales Performance By Klasifikasi Outlet,ctrbyklasifikasimarketnew/cform/<br>";
                        echo $row->i_user . ",Sales Performance By City,ctrbycity_new/cform/<br>";
                        echo $row->i_user . ",Sales Performance By Area,ctrbyarea_new/cform/<br>";
                        echo $row->i_user . ",Sales Performance By Provinsi,ctrbyprovinsi_new/cform/<br>";
                        echo $row->i_user . ",Sales Performance By Brand,ctrbybrand_new/cform/<br>";
                        echo $row->i_user . ",Sales Performance By Item Product,oaobbyitemproduct_new/cform/<br>";




                        //echo $row->i_user.",Sales Performance By Monthly,ctrbymonthly_new/cform/<br>";
                        echo '<li><a href="#">Sales Performance By Monthly</a><ul>';
                        if ($this->session->userdata('menu1') == 't') {
                            echo $row->i_user . ",Sales Performance By Monthly All ,ctrbymonthly_new/cform/<br>";
                        }
                        if ($this->session->userdata('menu1') == 't') {
                            echo $row->i_user . ",Sales Performance By Monthly Cintaka,ctrbymonthly_new_cintaka/cform/<br>";
                        }
                        if ($this->session->userdata('menu1') == 't') {
                            echo $row->i_user . ",Sales Performance By Monthly Subdist,ctrbymonthly_new_subdist/cform/<br>";
                        }
                        echo '</ul>';


                        echo $row->i_user . ",Sales Performance By SPB VS SJ Nasional,spbvssjnasional/cform/<br>";
                        echo $row->i_user . ",Sales Performance By Salesman,bysalesman/cform/<br>";
                        echo $row->i_user . ",Sales Performance By Category,ctrbycategory_new/cform/<br>";
                        echo $row->i_user . ",Sales Performance By Sub Category,ctrbysubcategory/cform/<br>";
                        echo $row->i_user . ",Sales Performance By Trend Perseries,ctrbyseries_new/cform/<br>";
                        echo '</ul></li>';
                    }


                    if (
                        ($this->session->userdata('menu599') == 't')
                    ) {
                        echo '<li><a href="#">Sales Report MTD</a><ul>';
                        if ($this->session->userdata('menu599') == 't') {
                            // if($this->session->userdata('user_id') == 'admin'){
                            echo $row->i_user . ",Sales Performance MTD,salesperformance_mtd/cform/<br>";
                            echo $row->i_user . ",Sales Performance By Island MTD,ctrbyisland_mtd/cform/<br>";
                            // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Sales Performance By Trend Toko Per Item,trendperitemtoko/cform/<br>";
                            echo $row->i_user . ",Sales Performance By Trend Per Toko MTD,trendpertoko_mtd/cform/<br>";
                            echo $row->i_user . ",Sales Performance By Area MTD,ctrbyarea_mtd/cform/<br>";
                            // }
                        }
                        if ($this->session->userdata('menu598') == 't') {
                        }
                        echo '</ul>';
                    }
                    //END SALES REPORT

                    if ($this->session->userdata('menu397') == 't') {
                        echo $row->i_user . ",Price List,pricelist/cform/<br>";
                    }
                    if ($this->session->userdata('menu90') == 't') {
                        echo $row->i_user . ",Promo,listpromo/cform/<br>";
                    }
                    if ($this->session->userdata('menu169') == 't') {
                        echo $row->i_user . ",RRKH,listrrkh/cform/<br>";
                    }
                    if ($this->session->userdata('menu352') == 't') {
                        echo $row->i_user . ",Penjualan,laporanpenjualan/cform/<br>";
                        echo $row->i_user . ",Penjualan per Bulan,laporanpenjualanperbulan/cform/<br>";
                    }
                    if ($this->session->userdata('menu354') == 't') {
                        echo $row->i_user . ",Penjualan Cabang,laporanpenjualancabang/cform/<br>";
                    }
                    if ($this->session->userdata('menu402') == 't') {
                        echo $row->i_user . ",Rekap RRKH,rekaprrkh/cform/<br>";
                    }
                    if ($this->session->userdata('menu360') == 't') {
                        echo $row->i_user . ",Sales to Market/Kota,salestomarketperkota/cform/<br>";
                    }
                    if ($this->session->userdata('menu415') == 't') {
                        echo $row->i_user . ",Pareto,listpenjualanpareto/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu294') == 't') or ($this->session->userdata('menu295') == 't') or
                    ($this->session->userdata('menu306') == 't') or ($this->session->userdata('menu307') == 't') or
                    ($this->session->userdata('menu317') == 't') or ($this->session->userdata('menu300') == 't') or
                    ($this->session->userdata('menu302') == 't') or ($this->session->userdata('menu320') == 't') or
                    ($this->session->userdata('menu322') == 't')
                ) {
                    echo '<li><a href="#">Konsinyasi</a><ul>';
                    if ($this->session->userdata('menu294') == 't') {
                        echo $row->i_user . ",Penjualan Konsinyasi,listpenjualankonsinyasi-bon/cform/<br>";
                    }
                    if ($this->session->userdata('menu295') == 't') {
                        echo $row->i_user . ",Order Konsinyasi,listorderkonsinyasi/cform/<br>";
                    }
                    if ($this->session->userdata('menu300') == 't') {
                        echo $row->i_user . ",SJPB Receive,listsjpbreceive/cform/<br>";
                    }
                    if ($this->session->userdata('menu302') == 't') {
                        echo $row->i_user . ",SJPB Retur,listsjpbretur/cform/<br>";
                    }
                    if ($this->session->userdata('menu320') == 't') {
                        echo $row->i_user . ",Stock Opname,listsopb/cform/<br>";
                    }
                    if ($this->session->userdata('menu317') == 't') {
                        echo $row->i_user . ",SJPB,listsjpb/cform/<br>";
                    }
                    if ($this->session->userdata('menu306') == 't') {
                        echo $row->i_user . ",BON,listbonkonsi/cform/<br>";
                    }
                    if ($this->session->userdata('menu307') == 't') {
                        echo $row->i_user . ",Order SPC,listorderspc/cform/<br>";
                    }
                    if ($this->session->userdata('menu322') == 't') {
                        echo $row->i_user . ",Mutasi Stock Per Toko,listmutasipb/cform/<br>";
                        echo $row->i_user . ",Mutasi Stock MO Daerah,listmutasipbdaerah/cform/<br>";
                    }
                    if ($this->session->userdata('menu460') == 't') {
                        echo $row->i_user . ",SPB,listspbkonsinyasi/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu58') == 't') or ($this->session->userdata('menu59') == 't') or
                    ($this->session->userdata('menu157') == 't') or ($this->session->userdata('menu244') == 't') or
                    ($this->session->userdata('menu291') == 't') or ($this->session->userdata('menu333') == 't') or
                    ($this->session->userdata('menu375') == 't')
                ) {
                    echo '<li><a href="#">Pembelian</a><ul>';
                    if ($this->session->userdata('menu58') == 't') {
                        echo $row->i_user . ",OP,listop/cform/<br>";
                    }
                    if ($this->session->userdata('menu59') == 't') {
                        echo $row->i_user . ",DO,listdo/cform/<br>";
                    }
                    if ($this->session->userdata('menu375') == 't') {
                        echo $row->i_user . ",DO All Area,listdoallarea/cform/<br>";
                    }
                    if ($this->session->userdata('menu157') == 't') {
                        echo $row->i_user . ",OP vs DO,opvsdo/cform/<br>";
                    }
                    if ($this->session->userdata('menu333') == 't') {
                        echo '<li><a href="#">Laporan OP vs DO</a><ul>';
                        echo $row->i_user . ",Per Supplier,lap-opdo/cform/<br>";
                        echo $row->i_user . ",Nasional,lap-opdonas/cform/<br>";
                        echo $row->i_user . ",Per Area,lap-opdoarea/cform/<br>";
                        echo '</ul>';
                    }
                    if ($this->session->userdata('menu244') == 't') {
                        echo $row->i_user . ",Daftar Harga,listbrgprice/cform/<br>";
                    }
                    if ($this->session->userdata('menu291') == 't') {
                        echo '<li><a href="#">Daftar Barang</a><ul>';
                        echo $row->i_user . ",Semua,listbrgall/cform/<br>";
                        echo $row->i_user . ",Rutin Producksi,listbrgrutin/cform/<br>";
                        echo $row->i_user . ",Temporer,listbrgtemporer/cform/<br>";

                        echo $row->i_user . ",Khusus,listbrgkhusus/cform/<br>";
                        echo $row->i_user . ",Stop Produksi,listbrgstop/cform/<br>";
                        echo '</ul>';
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu70') == 't') or ($this->session->userdata('menu61') == 't') or
                    ($this->session->userdata('menu76') == 't') or ($this->session->userdata('menu78') == 't') or
                    ($this->session->userdata('menu80') == 't') or ($this->session->userdata('menu85') == 't') or
                    ($this->session->userdata('menu92') == 't') or ($this->session->userdata('menu110') == 't') or
                    ($this->session->userdata('menu209') == 't') or ($this->session->userdata('menu167') == 't') or
                    ($this->session->userdata('menu158') == 't') or ($this->session->userdata('menu163') == 't') or
                    ($this->session->userdata('menu172') == 't') or ($this->session->userdata('menu180') == 't') or
                    ($this->session->userdata('menu211') == 't') or ($this->session->userdata('menu212') == 't') or
                    ($this->session->userdata('menu214') == 't') or ($this->session->userdata('menu237') == 't') or
                    ($this->session->userdata('menu252') == 't') or ($this->session->userdata('menu259') == 't') or
                    ($this->session->userdata('menu273') == 't') or ($this->session->userdata('menu277') == 't') or
                    ($this->session->userdata('menu310') == 't') or ($this->session->userdata('menu312') == 't') or
                    ($this->session->userdata('menu315') == 't') or ($this->session->userdata('menu350') == 't') or
                    ($this->session->userdata('menu363') == 't') or ($this->session->userdata('menu380') == 't') or
                    ($this->session->userdata('menu383') == 't') or ($this->session->userdata('menu423') == 't') or
                    ($this->session->userdata('menu426') == 't') or ($this->session->userdata('menu427') == 't') or
                    ($this->session->userdata('menu508') == 't')
                ) {
                    echo '<li><a href="#">Gudang</a><ul>';
                    if (
                        ($this->session->userdata('menu167') == 't') or ($this->session->userdata('menu158') == 't') or
                        ($this->session->userdata('menu252') == 't') or ($this->session->userdata('menu259') == 't') or
                        ($this->session->userdata('menu209') == 't') or ($this->session->userdata('menu211') == 't') or
                        ($this->session->userdata('menu212') == 't') or ($this->session->userdata('menu214') == 't') or
                        ($this->session->userdata('menu252') == 't') or ($this->session->userdata('menu259') == 't') or
                        ($this->session->userdata('menu280') == 't') or ($this->session->userdata('menu310') == 't') or
                        ($this->session->userdata('menu312') == 't') or ($this->session->userdata('menu315') == 't') or
                        ($this->session->userdata('menu342') == 't') or ($this->session->userdata('menu363') == 't') or
                        ($this->session->userdata('menu383') == 't') or ($this->session->userdata('menu398') == 't') or
                        ($this->session->userdata('menu423') == 't') or
                        ($this->session->userdata('menu508') == 't') or ($this->session->userdata('menu505') == 't')
                    ) {

                        if ($this->session->userdata('menu508') == 't') {
                            echo $row->i_user . ",Rekap Stock Opname,rekapstockopname/cform/<br>";
                        }
                        echo '<li><a href="#">Surat Jalan</a><ul>';
                        if ($this->session->userdata('menu167') == 't') {
                            echo $row->i_user . ",SJ,listsj/cform/<br>";
                        }
                        if ($this->session->userdata('menu158') == 't') {
                            echo $row->i_user . ",SJ Pinjaman,listsjp/cform/<br>";
                        }
                        if ($this->session->userdata('menu398') == 't') {
                            echo $row->i_user . ",SJP Nasional,listsjpnas/cform/<br>";
                        }
                        if ($this->session->userdata('menu252') == 't') {
                            echo $row->i_user . ",SJ Pinjaman Blm Terima,listsjpnotreceive/cform/<br>";
                        }
                        if ($this->session->userdata('menu259') == 't') {
                            echo $row->i_user . ",SJP Git,sjpgit/cform/<br>";
                        }
                        if ($this->session->userdata('menu209') == 't') {
                            echo $row->i_user . ",SJ Retur,listsjretur/cform/<br>";
                        }
                        if ($this->session->userdata('menu505') == 't') {
                            echo $row->i_user . ",SJ Receive,listsjreceive/cform/<br>";
                        }
                        if ($this->session->userdata('menu505') == 't') {
                            echo $row->i_user . ",SJ Belum Receipt,listsjblmreceive/cform/<br>";
                        }
                        if ($this->session->userdata('menu211') == 't') {
                            echo $row->i_user . ",SJR Receive,listsjrreceive/cform/<br>";
                        }
                        if ($this->session->userdata('menu212') == 't') {
                            echo $row->i_user . ",SJP Receive,listsjpreceive/cform/<br>";
                        }
                        if ($this->session->userdata('menu214') == 't') {
                            echo $row->i_user . ",SJR Toko,listsjrt/cform/<br>";
                        }
                        if ($this->session->userdata('menu310') == 't') {
                            echo $row->i_user . ",SJPB Retur Receive,listsjpbrreceive/cform/<br>";
                        }
                        if ($this->session->userdata('menu312') == 't') {
                            echo $row->i_user . ",SJ Retur Konsinyasi,listsjbretur/cform/<br>";
                        }
                        if ($this->session->userdata('menu315') == 't') {
                            echo $row->i_user . ",SJB Retur Receive,listsjbrreceive/cform/<br>";
                        }
                        if ($this->session->userdata('menu214') == 't') {
                            echo $row->i_user . ",SJR Toko,listsjrt/cform/<br>";
                        }
                        if ($this->session->userdata('menu280') == 't') {
                            echo $row->i_user . ",SJ Pusat,listsjpusat/cform/<br>";
                        }
                        if ($this->session->userdata('menu363') == 't') {
                            echo $row->i_user . ",SJ blm DKB,listsjblmdkb/cform/<br>";
                            echo $row->i_user . ",SJ blm Nota,listsjblmnota/cform/<br>";
                            echo $row->i_user . ",SJ Receive blm Nota,listsjrecblmnota/cform/<br>";
                        }
                        if ($this->session->userdata('menu383') == 't') {
                            echo $row->i_user . ",SJP Deliver vs Receive,listsjpbedaterima/cform/<br>";
                        }
                        echo '</ul>';
                    }
                    if ($this->session->userdata('menu540') == 't') {
                        echo $row->i_user . ",Update Stock On Hand,stockonhandupdate/cform/<br>";
                    }
                    if ($this->session->userdata('menu541') == 't') {
                        echo $row->i_user . ",Update Stock On Hand(Cabang),stockonhandupdatecabang/cform/<br>";
                    }
                    if (
                        ($this->session->userdata('menu277') == 't') or ($this->session->userdata('menu380') == 't')
                    ) {
                        echo '<li><a href="#">Pengiriman</a><ul>';
                        if ($this->session->userdata('menu277') == 't') {
                            echo $row->i_user . ",Kirim-SJ,listkirim/cform/<br>";
                        }
                        if ($this->session->userdata('menu380') == 't') {
                            echo $row->i_user . ",Kirim-SJP,listkirimsjp/cform/<br>";
                        }
                        echo '</ul>';
                    }
                    if ($this->session->userdata('menu237') == 't') {
                        //echo $row->i_user.",Mutasi Stock Pusat (Produksi),listmutasipusatproduksi/cform/<br>"
                        echo $row->i_user . ",Mutasi Stock Pusat (New),listmutasipusat/cform/<br>";
                        if ($this->session->userdata('departement') != '1') {
                            echo $row->i_user . ",Mutasi Stock Pusat (Harian New),listmutasipusatdaily/cform/<br>";
                        };
                    }
                    if ($this->session->userdata('menu471') == 't') {
                        echo $row->i_user . ",Mutasi Stock Daerah,listmutasidaerah/cform/<br>";
                    }
                    if ($this->session->userdata('menu61') == 't') {
                        echo $row->i_user . ",Stock on hand,liststock/cform/<br>";
                    }
                    if ($this->session->userdata('menu92') == 't') {
                        echo $row->i_user . ",Stock Opname,liststockopname/cform/<br>";
                    }
                    if ($this->session->userdata('menu70') == 't') {
                        echo $row->i_user . ",SPmB,listspmb/cform/<br>";
                    }
                    if ($this->session->userdata('menu342') == 't') {
                        echo $row->i_user . ",SPmB Blm Terealisasi,listspmbblmreal/cform/<br>";
                    }
                    // if($this->session->userdata('menu391')=='t'){
                    // $row->i_user.	,echo "Mutasi HPP,listhppgeneral/cform/<br>";
                    // }
                    if ($this->session->userdata('menu391') == 't') {
                        echo $row->i_user . ",Nilai Stock (HPP),listhpp/cform/<br>";
                        echo $row->i_user . ",Mutasi HPP,listmutasihpp/cform/<br>";
                    }
                    if ($this->session->userdata('menu76') == 't') {
                        echo $row->i_user . ",Konversi Stock,listks/cform/<br>";
                    }
                    if ($this->session->userdata('menu78') == 't') {
                        echo $row->i_user . ",BBM - AP,listbbmap/cform/<br>";
                    }
                    if ($this->session->userdata('menu80') == 't') {
                        echo $row->i_user . ",TTB - Tolakan,listttbtolak/cform/<br>";
                    }
                    if ($this->session->userdata('menu85') == 't') {
                        echo $row->i_user . ",TTB - Retur,listttbretur/cform/<br>";
                    }
                    if ($this->session->userdata('menu423') == 't') {
                        echo $row->i_user . ",TTB Blm BBM/KN,listttbblmbbm/cform/<br>";
                    }
                    if ($this->session->userdata('menu110') == 't') {
                        echo $row->i_user . ",BBM - Retur,listbbmretur/cform/<br>";
                        echo $row->i_user . ",BBM - Barter,listbbmbarter/cform/<br>";
                    }
                    if ($this->session->userdata('menu350') == 't') {
                        echo $row->i_user . ",BBK - Hadiah,listbbkhadiah/cform/<br>";
                        echo $row->i_user . ",BBK - Barter,listbbkbarter/cform/<br>";
                    }
                    if ($this->session->userdata('menu163') == 't') {
                        echo $row->i_user . ",DKB,listdkb/cform/<br>";
                    }
                    if ($this->session->userdata('menu426') == 't') {
                        echo $row->i_user . ",Bon Masuk,listbonmasuk/cform/<br>";
                        echo $row->i_user . ",Realisasi Bon Masuk,listbonmasukrekap/cform/<br>";
                    }
                    if ($this->session->userdata('menu427') == 't') {
                        echo $row->i_user . ",Bon Keluar,listbonkeluar/cform/<br>";
                    }
                    if (
                        ($this->session->userdata('menu173') == 't') or ($this->session->userdata('menu180') == 't')
                    ) {
                        echo '<li><a href="#">BAPB</a><ul>';
                        if ($this->session->userdata('menu173') == 't') {
                            echo $row->i_user . ",BAPB-SJ,listbapb/cform/<br>";
                        }
                        if ($this->session->userdata('menu180') == 't') {
                            echo $row->i_user . ",BAPB-SJP,listbapbsjp/cform/<br>";
                        }
                        echo '</ul>';
                    }
                    if ($this->session->userdata('menu484') == 't') {
                        echo $row->i_user . ",Adjustment,listadjustment/cform/<br>";
                    }
                    if ($this->session->userdata('menu594') == 't') {
                        echo $row->i_user . ",Adjustment Counter,listadjustmentcounter/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu60') == 't') or ($this->session->userdata('menu82') == 't') or
                    ($this->session->userdata('menu102') == 't') or ($this->session->userdata('menu107') == 't') or
                    ($this->session->userdata('menu108') == 't') or ($this->session->userdata('menu112') == 't') or
                    ($this->session->userdata('menu114') == 't') or ($this->session->userdata('menu135') == 't') or
                    ($this->session->userdata('menu170') == 't') or ($this->session->userdata('menu216') == 't') or
                    ($this->session->userdata('menu203') == 't') or ($this->session->userdata('menu204') == 't') or
                    ($this->session->userdata('menu137') == 't') or ($this->session->userdata('menu138') == 't') or
                    ($this->session->userdata('menu141') == 't') or ($this->session->userdata('menu166') == 't') or
                    ($this->session->userdata('menu174') == 't') or ($this->session->userdata('menu228') == 't') or
                    ($this->session->userdata('menu229') == 't') or ($this->session->userdata('menu230') == 't') or
                    ($this->session->userdata('menu258') == 't') or ($this->session->userdata('menu263') == 't') or
                    ($this->session->userdata('menu265') == 't') or ($this->session->userdata('menu266') == 't') or
                    ($this->session->userdata('menu270') == 't') or ($this->session->userdata('menu290') == 't') or
                    ($this->session->userdata('menu304') == 't') or ($this->session->userdata('menu313') == 't') or
                    ($this->session->userdata('menu325') == 't') or ($this->session->userdata('menu337') == 't') or
                    ($this->session->userdata('menu338') == 't') or ($this->session->userdata('menu347') == 't') or
                    ($this->session->userdata('menu389') == 't') or ($this->session->userdata('menu390') == 't') or
                    ($this->session->userdata('menu396') == 't') or ($this->session->userdata('menu404') == 't') or
                    ($this->session->userdata('menu434') == 't') or ($this->session->userdata('menu435') == 't') or
                    ($this->session->userdata('menu267') == 't')
                ) {
                    echo '<li><a href="#">Keuangan</a><ul>';
                    if (
                        ($this->session->userdata('menu60') == 't') or ($this->session->userdata('menu270') == 't') or
                        ($this->session->userdata('menu325') == 't') or ($this->session->userdata('menu396') == 't')
                    ) {
                        if ($this->session->userdata('menu503') == 't') {
                            echo $row->i_user . ",Informasi Plafond ACC,listaccplafond/cform/<br>";
                        }
                        echo '<li><a href="#">Nota</a><ul>';
                        if (($this->session->userdata('menu60') == 't') && ($this->session->userdata('departement') != '4')) {
                            // $row->i_user.e,cho "Nasional,listnotanas/cform/<br>";    
                            echo $row->i_user . ",Per Area,listnota/cform/<br>";
                            echo $row->i_user . ",Jumlah Nota,listjumlahnota/cform/<br>";
                        } else {
                            echo $row->i_user . ",Nasional,listnotanas/cform/<br>";
                            echo $row->i_user . ",Per Area,listnota/cform/<br>";
                            echo $row->i_user . ",Cancel,listnotacancel/cform/<br>";
                            echo $row->i_user . ",Nota Revisi(Khusus),notarevisi/cform/<br>";
                        }
                        if ($this->session->userdata('menu270') == 't') {
                            echo $row->i_user . ",Per Salesman,listnotapersalesman/cform/<br>";
                        }
                        if ($this->session->userdata('menu325') == 't') {
                            echo $row->i_user . ",Cabang,listnotacabang/cform/<br>";
                        }
                        if ($this->session->userdata('menu396') == 't') {
                            echo $row->i_user . ",History DT,historynotadt/cform/<br>";
                        }
                        echo '</ul>';
                    }
                    if (
                        ($this->session->userdata('menu347') == 't') or ($this->session->userdata('menu348') == 't')
                    ) {
                        echo '<li><a href="#">Insentif</a><ul>';
                        if ($this->session->userdata('menu347') == 't') {
                            echo $row->i_user . ",Salesman,listinsentif/cform/<br>";
                        }
                        if ($this->session->userdata('menu348') == 't') {
                            echo $row->i_user . ",AS,listinsentifas/cform/<br>";
                        }
                        if ($this->session->userdata('menu347') == 't') {
                            echo $row->i_user . ",Sales Award,listinsentifaward/cform/<br>";
                        }
                        echo '</ul>';
                    }
                    if ($this->session->userdata('menu102') == 't') {
                        echo $row->i_user . ",Daftar Tagihan,listdaftartagihan/cform/<br>";
                    }

                    //MENU PELUNASAN           
                    if (
                        ($this->session->userdata('menu108') == 't' || $this->session->userdata('menu138') == 't') &&
                        $this->session->userdata('user_id') == 'auditdmg' || $this->session->userdata('user_id') == 'spvar'
                    ) {
                        echo '<li><a href="#">Pelunasan</a><ul>';
                        if ($this->session->userdata('menu108') == 't') {
                            echo $row->i_user . ",Pelunasan Piutang,listpelunasan/cform/<br>";
                        }
                        if ($this->session->userdata('menu138') == 't') {
                            echo $row->i_user . ",Pelunasan Hutang,listpelunasanap/cform/<br>";
                        }
                        echo '</ul></li>';
                    }
                    //END MENU PELUNASAN

                    if (
                        ($this->session->userdata('menu108') == 't') or ($this->session->userdata('menu138') == 't') or
                        ($this->session->userdata('menu504') == 't') or ($this->session->userdata('menu521') == 't') or
                        ($this->session->userdata('menu527') == 't')
                    ) {
                        echo '<li><a href="#">Alokasi</a><ul>';
                        if ($this->session->userdata('menu504') == 't') {
                            #	$row->i_user.	,				          echo "Rekap Pelunasan Piutang Per Area (Jenis Bayar),listpelunasanpiutang_areajenisbayar/cform/<br>";
                        }
                        if ($this->session->userdata('menu108') == 't') {
                            echo $row->i_user . ",Alokasi Kredit Nota,listknalokasi/cform/<br>";
                            #	$row->i_user.	,								echo "Pelunasan Piutang,listpelunasan/cform/<br>";
                        }
                        if ($this->session->userdata('menu521') == 't') {
                            echo $row->i_user . ",Alokasi Kredit Nota Retur,listknralokasi/cform/<br>";
                        }
                        if ($this->session->userdata('menu138') == 't') {
                            #	$row->i_user.	,								echo "Pelunasan Hutang,listpelunasanap/cform/<br>";
                        }
                        if ($this->session->userdata('menu527') == 't') {
                            echo $row->i_user . ",Alokasi Hutang Lain2,listalokasihutanglain/cform/<br>";
                        }
                        echo '</ul></li>';
                    }
                    if (
                        ($this->session->userdata('menu107') == 't') or ($this->session->userdata('menu204') == 't') or
                        ($this->session->userdata('menu203') == 't') or ($this->session->userdata('menu137') == 't')
                    ) {
                        echo '<li><a href="#">Giro</a><ul>';
                        if ($this->session->userdata('menu107') == 't') {
                            echo $row->i_user . ",Giro Pelanggan,listgiro/cform/<br>";
                        }
                        if (
                            ($this->session->userdata('menu204') == 't') or ($this->session->userdata('menu203') == 't')
                        ) {
                            echo '<li><a href="#">Giro Cair Pelanggan</a>';
                            echo '<ul>';
                            if ($this->session->userdata('menu204') == 't') {
                                echo $row->i_user . ",Giro Belum Cair,listgirobcair/cform/<br>";
                            }
                            if ($this->session->userdata('menu203') == 't') {
                                echo $row->i_user . ",Giro Sudah Cair,listgiroscair/cform/<br>";
                            }
                            echo '</ul></li>';
                        }
                        if ($this->session->userdata('menu137') == 't') {

                            echo $row->i_user . ",Giro DGU,listgirodgu/cform/<br>";
                        }
                        echo '</ul></li>';
                    }
                    if ($this->session->userdata('menu492') == 't') {
                        /*echo $row->i_user.",Tunai,listtunai/cform/<br>";*/
                    }
                    if ($this->session->userdata('menu492') == 't') {
                        echo $row->i_user . ",Tunai Item,listtunai-item/cform/<br>";
                    }
                    if ($this->session->userdata('menu493') == 't') {
                        //echo '<li><a href="#">Setor Tunai</a><ul>';
                        echo $row->i_user . ",Setor Tunai,listrtunai/cform/<br>";
                        /*echo $row->i_user.",Detail,listrtunaidetail/cform/<br>";
                                            echo '</ul></li>';*/
                    }
                    if (
                        ($this->session->userdata('menu112') == 't') or ($this->session->userdata('menu114') == 't')
                    ) {
                        echo '<li><a href="#">Kredit Nota</a><ul>';
                        if ($this->session->userdata('menu112') == 't') {
                            echo $row->i_user . ",KN Retur,listknretur/cform/<br>";
                        }
                        if ($this->session->userdata('menu114') == 't') {
                            echo $row->i_user . ",KN non Retur,listknnonretur/cform/<br>";
                        }
                        if ($this->session->userdata('status') == '1') {
                            echo $row->i_user . ",Rekap KN,listrekapkn/cform/<br>";
                        }
                        echo '</ul></li>';
                    }
                    if ($this->session->userdata('menu135') == 't') {
                        echo $row->i_user . ",Hutang Dagang,listdaftartagihanap/cform/<br>";
                    }
                    if (
                        ($this->session->userdata('menu141') == 't') or ($this->session->userdata('menu166') == 't') or
                        ($this->session->userdata('menu230') == 't')
                    ) {
                        echo '<li><a href="#">Transfer Uang</a><ul>';
                        if ($this->session->userdata('menu141') == 't') {
                            echo $row->i_user . ",Transfer Uang Masuk,listtransferuangmasuk/cform/<br>";
                        }
                        if ($this->session->userdata('menu166') == 't') {
                            echo $row->i_user . ",Transfer Uang Keluar,listtransferuangkeluar/cform/<br>";
                        }
                        if ($this->session->userdata('menu230') == 't') {
                            echo $row->i_user . ",KU Masuk (Cabang),listtransferuangmasukcabang/cform/<br>";
                        }
                        echo '</ul></li>';
                    }
                    if (
                        ($this->session->userdata('menu170') == 't') or ($this->session->userdata('menu174') == 't')
                    ) {
                        echo '<li><a href="#">IKHP</a><ul>';
                        if ($this->session->userdata('menu170') == 't') {
                            echo $row->i_user . ",IKHP,listikhp/cform/<br>";
                        }
                        if ($this->session->userdata('menu174') == 't') {
                            echo $row->i_user . ",IKHP (Pengeluaran),listikhpkeluar/cform/<br>";
                        }
                        if ($this->session->userdata('menu170') == 't') {
                            echo $row->i_user . ",IKHP Tunai,listikhptunai/cform/<br>";
                        }
                        echo '</ul></li>';
                    }
                    if (
                        ($this->session->userdata('menu337') == 't') or ($this->session->userdata('menu338') == 't')
                    ) {
                        echo '<li><a href="#">Target Collection</a><ul>';
                        if ($this->session->userdata('menu337') == 't') {
                            echo $row->i_user . ",Target Collection Insentif,listtargetcollection/cform/<br>";
                            echo $row->i_user . ",Target Collection Real Time,listtargetcollectionrealtime/cform/<br>";
                            echo $row->i_user . ",Target Collection Real Time(Pelunasan),listtargetcollectionrealtimeold/cform/<br>";

                            /*echo $row->i_user.",Credit,listtargetcollectioncr/cform/<br>";
                                            echo $row->i_user.",per Nota Credit,listcollectionpernotacredit/cform/<br>";
                                            */
                        }
                        if ($this->session->userdata('menu338') == 't') {
                            /*
                                            echo $row->i_user.",Cash,listtargetcollectioncash/cform/<br>";
                                            echo $row->i_user.",per Nota Cash,listcollectionpernotacash/cform/<br>";
                                            */
                        }
                        if ($this->session->userdata('menu337') == 't') {
                            echo $row->i_user . ",Target Collection All,listtargetcollectionall/cform/<br>";
                        }
                        echo '</ul></li>';
                    }
                    if (
                        ($this->session->userdata('menu228') == 't') or ($this->session->userdata('menu229') == 't') or ($this->session->userdata('menu404') == 't')
                    ) {
                        echo '<li><a href="#">Opname Nota (piutang)</a><ul>';
                        if ($this->session->userdata('menu228') == 't') {
                            echo $row->i_user . ",Per Pelanggan,opnnotacustomer/cform/<br>";
                        }
                        if ($this->session->userdata('menu229') == 't') {
                            echo $row->i_user . ",Per Area,opnnotaarea/cform/<br>";
                            // $row->i_user.e,cho "<li>" . $this->pquery->link_to_remote('Pemakaian Meterai,listmeterai/cform/<br>";
                            echo $row->i_user . ",Opname Meterai,listopnamemeterai/cform/<br>";
                            echo $row->i_user . ",Pemakaian Meterai,listmeterai/cform/<br>";
                        }
                        if ($this->session->userdata('menu404') == 't') {
                            echo $row->i_user . ",Nasional,opnnotanas/cform/<br>";
                        }
                        echo '</ul>';
                    }
                    if ($this->session->userdata('menu389') == 't') {
                        echo $row->i_user . ",Opname Nota (hutang),opnnotahutang/cform/<br>";
                    }
                    if ($this->session->userdata('menu304') == 't') {
                        echo '<li><a href="#">Opname KN/DN</a><ul>';
                        echo $row->i_user . ",KN Retur,opnknretur/cform/<br>";
                        echo $row->i_user . ",KN Non Retur,opnknnonr/cform/<br>";
                        echo $row->i_user . ",DN,opndn/cform/<br>";
                        echo '</ul>';
                    }
                    if (
                        ($this->session->userdata('menu266') == 't') or ($this->session->userdata('menu267') == 't') or ($this->session->userdata('menu531') == 't')
                    ) {
                        echo '<li><a href="#">Kartu Piutang</a><ul>';
                        if ($this->session->userdata('menu266') == 't') {
                            echo $row->i_user . ",per Periode,kartupiutang/cform/<br>";
                        }
                        if ($this->session->userdata('menu267') == 't') {
                            echo $row->i_user . ",per Tanggal Area,kartupiutangpertgl/cform/<br>";
                        }
                        if ($this->session->userdata('menu531') == 't') {
                            echo $row->i_user . ",per Tanggal,kartupiutangperdate1/cform/<br>";
                        }
                        echo '</ul>';
                    }
                    if ($this->session->userdata('menu71') == 't') {
                        echo $row->i_user . ",Mutasi Piutang,mutasipiutang/cform/<br>";
                    }
                    if ($this->session->userdata('menu390') == 't') {
                        echo $row->i_user . ",Kartu Hutang,kartuhutang/cform/<br>";
                    }
                    if (
                        ($this->session->userdata('menu265') == 't') or ($this->session->userdata('menu258') == 't') or
                        ($this->session->userdata('menu388') == 't')
                    ) {
                        echo '<li><a href="#">Aging</a><ul>';
                        if ($this->session->userdata('menu500') == 't') {
                            echo $row->i_user . ",Aging Pelunasan New,agingpelunasannew/cform/<br>";
                        }
                        if ($this->session->userdata('menu265') == 't') {
                            echo $row->i_user . ",Aging Pelunasan,agingpelunasan/cform/<br>";
                        }
                        if ($this->session->userdata('menu258') == 't') {
                            echo $row->i_user . ",Keterlambatan Per Customer,agingpiutang/cform/<br>";
                            echo $row->i_user . ",Keterlambatan Per Salesman,agingpiutangpersalesman/cform/<br>";
                        }
                        if ($this->session->userdata('menu478') == 't') {
                            echo $row->i_user . ",Rata-Rata Keterlambatan,avketerlambatan/cform/<br>";
                        }
                        if ($this->session->userdata('menu388') == 't') {
                            echo $row->i_user . ",Laporan Keterlambatan Per Nota,listagingpiutang/cform/<br>";
                            #	$row->i_user.	,					  	echo "Laporan Keterlambatan (total),listagingpiutangnasional/cform/<br>";
                        }
                        echo '</ul></li>';
                    }
                    if (
                        ($this->session->userdata('menu263') == 't') or ($this->session->userdata('menu313') == 't') or
                        ($this->session->userdata('menu290') == 't')
                    ) {
                        echo '<li><a href="#">Bayar Piutang</a><ul>';
                        if ($this->session->userdata('menu263') == 't') {
                            echo $row->i_user . ",Daftar Bayar,listbayar/cform/<br>";
                        }
                        if ($this->session->userdata('menu313') == 't') {
                            echo $row->i_user . ",Daftar Bayar KN,listbayarkn/cform/<br>";
                        }
                        if ($this->session->userdata('menu290') == 't') {
                            echo $row->i_user . ",Lebih Bayar,listlebihbayar/cform/<br>";
                        }
                        echo '</ul></li>';
                    }
                    if (
                        ($this->session->userdata('menu385') == 't') or ($this->session->userdata('menu386') == 't')
                    ) {
                        echo '<li><a href="#">Bayar Hutang Dagang</a><ul>';
                        if ($this->session->userdata('menu385') == 't') {
                            echo $row->i_user . ",Daftar Bayar,listbayarap/cform/<br>";
                        }
                        if ($this->session->userdata('menu386') == 't') {
                            echo $row->i_user . ",Lebih Bayar,listlebihbayarap/cform/<br>";
                        }
                        echo '</ul></li>';
                    }
                    if ($this->session->userdata('menu269') == 't') {
                        echo '<li><a href="#">Faktur Komersial</a><ul>';
                        if ($this->session->userdata('menu269') == 't') {
                            echo $row->i_user . ",per Area,listfkom/cform/<br>";
                        }
                        if ($this->session->userdata('menu269') == 't') {
                            echo $row->i_user . ",Nasional,listfkomnas/cform/<br>";
                        }
                        echo '</ul></li>';
                    }
                    if ($this->session->userdata('menu255') == 't') {
                        echo $row->i_user . ",Debet Nota,listdebetnota/cform/<br>";
                    }

                    #25 Agustus 2021
                    if ($this->session->userdata('menu522') == 't') {
                        echo $row->i_user . ",Debet Nota/AP,listdebetnota-ap/cform/<br>";
                    }
                    if ($this->session->userdata('menu105') == 't') {
                        echo $row->i_user . ",Alokasi Debet Nota/AP,listallocdebetnota-ap/cform/<br>";
                    }

                    if ($this->session->userdata('menu216') == 't') {
                        echo $row->i_user . ",History Penjualan,historyjual/cform/<br>";
                    }
                    if ($this->session->userdata('menu273') == 't') {
                        echo '<li><a href="#">BBM Belum Dibuat KN</a><ul>';
                        echo $row->i_user . ",List BBM,listbbmreturblmkn/cform/<br>";
                        echo $row->i_user . ",Nota Sudah Dikoreksi,listcekbbmreturblmkn/cform/<br>";
                    }
                    if ($this->session->userdata('menu82') == 't') {
                        echo $row->i_user . ",Nota Koreksi,listnotakoreksi/cform/<br>";
                    }
                    if ($this->session->userdata('menu434') == 't') {
                        echo $row->i_user . ",Pelunasan AP per nota,listpelunasanappernota/cform/<br>";
                    }
                    if ($this->session->userdata('menu435') == 't') {
                        echo $row->i_user . ",Nota vs Pelunasan,notavspelunasan/cform/<br>";
                    }
                    if ($this->session->userdata('menu464') == 't') {
                        echo $row->i_user . ",Group AR,listcustomergroupar/cform/<br>";
                    }
                    if ($this->session->userdata('menu463') == 't') {
                        echo $row->i_user . ",Group Bayar,listcustomergroupbayar/cform/cari/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu116') == 't') or ($this->session->userdata('menu117') == 't') or
                    ($this->session->userdata('menu126') == 't') or ($this->session->userdata('menu129') == 't') or
                    ($this->session->userdata('menu130') == 't') or ($this->session->userdata('menu131') == 't') or
                    ($this->session->userdata('menu132') == 't') or ($this->session->userdata('menu147') == 't') or
                    ($this->session->userdata('menu502') == 't') or ($this->session->userdata('menu523') == 't')
                ) {
                    echo '<li><a href="#">Akunting</a><ul>';
                    if ($this->session->userdata('menu116') == 't') {
                        echo $row->i_user . ",Master Saldo Account,listcoasaldo/cform/<br>";
                    }
                    if ($this->session->userdata('menu117') == 't') {
                        echo $row->i_user . ",CoA,listcoa/cform/<br>";
                    }
                    if ($this->session->userdata('menu147') == 't') {
                        echo $row->i_user . ",Kas Kecil,list-akt-kk/cform/<br>";
                    }
                    if ($this->session->userdata('menu502') == 't') {
                        echo $row->i_user . ",Kas Besar,list-akt-kb/cform/<br>";
                    }
                    if (($this->session->userdata('menu502') == 't')/* &&($this->session->userdata('user_id')!='akt-cbp') */) {
                        echo $row->i_user . ",Bank,list-akt-bk/cform/<br>";
                        echo $row->i_user . ",Bank Masuk,list-akt-bk-area/cform/<br>";
                        echo $row->i_user . ",Alokasi Bank Keluar,list-akt-bkoutalloc/cform/<br>";
                        echo $row->i_user . ",Alokasi Kas Besar Keluar,list-akt-kboutalloc/cform/<br>";
                        #echo $row->i_user.",Alokasi Bank Masuk,list-akt-bkinalloc/cform/<br>";
                    }
                    if ($this->session->userdata('menu523') == 't') {
                        echo $row->i_user . ",Alokasi Bank Masuk,list-akt-bkinalloc/cform/<br>";
                    }
                    //* LIST METERAI DEVELOP 16 JUN 2021
                    if ($this->session->userdata('menu537') == 't' /*$this->session->userdata('user_id') == 'admin'*/) {
                        echo $row->i_user . ",Alokasi Meterai,list-akt-meterai/cform/<br>";
                    }
                    if ($this->session->userdata('menu126') == 't') {
                        echo $row->i_user . ",Jurnal Umum,list-akt-gj/cform/<br>";
                    }
                    if ($this->session->userdata('menu129') == 't') {
                        echo $row->i_user . ",Jurnal Transaksi Harian,akt-jth/cform/<br>";
                    }
                    if ($this->session->userdata('menu130') == 't') {
                        echo $row->i_user . ",Buku Besar,akt-bb/cform/<br>";
                    }
                    if ($this->session->userdata('menu131') == 't') {
                        echo $row->i_user . ",Neraca Saldo,akt-neracasaldo/cform/<br>";
                    }
                    if ($this->session->userdata('menu133') == 't') {
                        echo $row->i_user . ",Laba Rugi,akt-lr/cform/<br>";
                    }
                    if ($this->session->userdata('menu132') == 't') {
                        echo $row->i_user . ",Neraca,akt-neraca/cform/<br>";
                    }
                    echo '</ul>';
                }
                if (
                    ($this->session->userdata('menu145') == 't') or ($this->session->userdata('menu254') == 't') or
                    ($this->session->userdata('menu257') == 't')
                ) {
                    echo '<li><a href="#">Umum</a><ul>';
                    if ($this->session->userdata('menu145') == 't') {
                        echo $row->i_user . ",Kendaraan,listkendaraan/cform/<br>";
                    }
                    if ($this->session->userdata('menu254') == 't') {
                        echo $row->i_user . ",Pelanggan,listcustomer/cform/<br>";
                    }
                    if ($this->session->userdata('menu257') == 't') {
                        echo $row->i_user . ",Ekspedisi,listekspedisi/cform/<br>";
                    }
                    echo '</ul>';
                }
                echo '</ul>';
            }
            if (
                ($this->session->userdata('menu184') == 't') or ($this->session->userdata('menu324') == 't') or
                ($this->session->userdata('menu351') == 't') or ($this->session->userdata('menu365') == 't') or
                ($this->session->userdata('menu377') == 't') or ($this->session->userdata('menu378') == 't') or
                ($this->session->userdata('menu382') == 't') or ($this->session->userdata('menu392') == 't') or
                ($this->session->userdata('menu411') == 't') or ($this->session->userdata('menu416') == 't')
            ) {
                echo '<li><a href="#">Pajak</a><ul>';
                if ($this->session->userdata('menu184') == 't') {
                    echo $row->i_user . ",Faktur Komersial,fkom/cform/<br>";
                }
                if ($this->session->userdata('menu365') == 't') {
                    echo $row->i_user . ",Faktur Komersial (Pjk Pengganti),fkompengganti/cform/<br>";
                }
                if ($this->session->userdata('menu377') == 't') {
                    echo $row->i_user . ",Faktur Komersial Auto,fkomauto/cform/<br>";
                    echo $row->i_user . ",SJ Komersial Auto,skomauto/cform/<br>";
                }
                if ($this->session->userdata('menu382') == 't') {
                    echo $row->i_user . ",Faktur Komersial (BBK-Hadiah),fkom-bbkhadiah/cform/<br>";
                }
                if ($this->session->userdata('menu324') == 't') {
                    echo $row->i_user . ",Laporan Buku Penjualan,listlappenjualan/cform/<br>";
                }
                if ($this->session->userdata('menu351') == 't') {
                    echo $row->i_user . ",PPN Keluaran,ppnkeluaran/cform/<br>";
                }
                if ($this->session->userdata('menu378') == 't') {
                    echo $row->i_user . ",PJNA,pjna/cform/<br>";
                }
                if ($this->session->userdata('menu392') == 't') {
                    echo $row->i_user . ",Approve Revisi Nota,notaapprovepajak/cform/<br>";
                }
                if ($this->session->userdata('menu411') == 't') {
                    echo $row->i_user . ",Batal Faktur Pajak,fpajakbatal/cform/<br>";
                }
                if ($this->session->userdata('menu416') == 't') {
                    echo $row->i_user . ",TTD Pajak,ttdpajak/cform/<br>";
                }
                echo '</ul>';
            }
            if (
                ($this->session->userdata('menu150') == 't') or ($this->session->userdata('menu227') == 't') or
                ($this->session->userdata('menu233') == 't') or ($this->session->userdata('menu235') == 't') or
                ($this->session->userdata('menu236') == 't') or ($this->session->userdata('menu238') == 't') or
                ($this->session->userdata('menu264') == 't') or ($this->session->userdata('menu268') == 't') or
                ($this->session->userdata('menu275') == 't') or ($this->session->userdata('menu282') == 't') or
                ($this->session->userdata('menu287') == 't') or ($this->session->userdata('menu303') == 't') or
                ($this->session->userdata('menu305') == 't') or ($this->session->userdata('menu316') == 't') or
                ($this->session->userdata('menu323') == 't') or ($this->session->userdata('menu326') == 't') or
                ($this->session->userdata('menu334') == 't') or ($this->session->userdata('menu339') == 't') or
                ($this->session->userdata('menu344') == 't') or ($this->session->userdata('menu368') == 't') or
                ($this->session->userdata('menu369') == 't') or ($this->session->userdata('menu401') == 't') or
                ($this->session->userdata('menu405') == 't') or ($this->session->userdata('menu409') == 't') or
                ($this->session->userdata('menu429') == 't') or ($this->session->userdata('menu462') == 't') or
                ($this->session->userdata('menu472') == 't') or ($this->session->userdata('menu474') == 't') or
                ($this->session->userdata('menu520') == 't') or ($this->session->userdata('menu528') == 't')
            ) {
                echo '<li><a href="#">Export</a><ul>';
                if ($this->session->userdata('menu528') == 't') {
                    echo $row->i_user . ",Export Setor Tunai,exp-rtunai/cform/<br>";
                    echo $row->i_user . ",Export Alokasi Bank Masuk,exp-akt-bkinalloc/cform/<br>";
                }
                if ($this->session->userdata('menu511') == 't') {

                    #	$row->i_user.	,			    echo "<li><a href='".site_url()."/exp-efaktur/cform/<br>' target='_blank'>Pajak E-Faktur</a></li>";
                    echo $row->i_user . ",Pajak E-Faktur,exp-efaktur/cform/<br>";
                }
                if ($this->session->userdata('menu520') == 't') {
                    echo $row->i_user . ",Bank,exp-akt-bk/cform/<br>";
                    if (($this->session->userdata('departement') != '1')) {
                        echo $row->i_user . ",Bank+Alokasi,exp-akt-bkalloc/cform/<br>";
                    }
                }
                if ($this->session->userdata('menu486') == 't') {
                    echo $row->i_user . ",Plafon Acc,exp-plafonacc/cform/<br>";
                }
                //  if($this->session->userdata('menu525')=='t'){
                // $row->i_user.	,	echo "DO,exp-btb/cform/<br>";
                // }
                if ($this->session->userdata('menu474') == 't') {
                    echo $row->i_user . ",OP,exp-op/cform/<br>";
                }
                if ($this->session->userdata('menu474') == 't') {
                    echo $row->i_user . ",DO,exp-do/cform/<br>";
                }
                if ($this->session->userdata('menu401') == 't') {
                    echo $row->i_user . ",PPN Keluaran,exp-ppnkeluaran/cform/<br>";
                }
                if ($this->session->userdata('menu150') == 't') {
                    echo $row->i_user . ",LKH,exp-kk-lkh/cform/<br>";
                }
                // 	if($this->session->userdata('menu150')=='t'){
                // $row->i_user.	,	echo "Kas Besar,exp-kb/cform/<br>";
                //   }
                //   if($this->session->userdata('menu150')=='t'){
                // $row->i_user.	,	echo "Kas Besar All Area,exp-kb-allarea/cform/<br>";
                //   }
                if ($this->session->userdata('menu409') == 't') {
                    echo $row->i_user . ",LKH All Area,exp-kk-lkhall/cform/<br>";
                }
                //KHUSUS KABAGKEUANGAN
                if ($this->session->userdata('user_id') == 'kabagkeuangan') {
                    echo $row->i_user . ",SJ Blm Nota,exp-sjblmnota/cform/<br>export";
                }
                if ($this->session->userdata('menu578') == 't') {
                    echo $row->i_user . ",Kas Besar,exp-kball/cform/<br>";
                }
                if ($this->session->userdata('menu264') == 't') {
                    echo $row->i_user . ",Rekap SJ,exp-rekapsj/cform/<br>";
                }

                if ($this->session->userdata('menu134') == 't') {
                    echo $row->i_user . ",Hutang Dagang,listdaftartagihanap_new/cform/<br>";
                }
                if ($this->session->userdata('menu134') == 't') {
                    echo $row->i_user . ",Pelunasan Pembelian,exp-pelunasan-pembelian/cform/<br>";
                }
                if (($this->session->userdata('user_id') == 'admgud') || ($this->session->userdata('user_id') == 'admin')) {
                    echo $row->i_user . ",DKB,exp-dkb/cform/<br>";
                }
                if ($this->session->userdata('menu227') == 't') {
                    echo $row->i_user . ",Mutasi Stok Pusat (Produksi),exp-mutasistock-pusatproduksi/cform/<br>";
                }

                if ($this->session->userdata('menu227') == 't') {
                    echo $row->i_user . ",Mutasi Stok Pusat,exp-mutasistock-pusat/cform/<br>";
                }

                if ($this->session->userdata('menu472') == 't') {
                    echo $row->i_user . ",Mutasi Stok Daerah,exp-mutasistock-daerah/cform/<br>";
                }
                if ($this->session->userdata('menu227') == 't') {
                    echo $row->i_user . ",Mutasi Stok MO,exp-mutasistock-mo/cform/<br>";
                }
                if ($this->session->userdata('menu369') == 't') {
                    echo $row->i_user . ",Mutasi Konsinyasi,exp-mutasikons/cform/<br>";
                }
                if ($this->session->userdata('menu233') == 't') {
                    echo $row->i_user . ",SPmB,exp-spmb/cform/<br>";
                }
                if ($this->session->userdata('menu235') == 't') {
                    echo $row->i_user . ",IKHP,exp-ikhp/cform/<br>";
                }
                if ($this->session->userdata('menu282') == 't') {
                    echo $row->i_user . ",IKHP (Non Cek),exp-ikhpnc/cform/<br>";
                }
                if ($this->session->userdata('menu275') == 't') {
                    echo $row->i_user . ",DT,exp-dt/cform/<br>";
                }
                if ($this->session->userdata('menu236') == 't') {
                    echo $row->i_user . ",Giro Pelanggan,exp-giro/cform/<br>";
                }
                if ($this->session->userdata('menu238') == 't') {
                    echo '<li><a href="#">Opname Nota</a><ul>';
                    echo $row->i_user . ",Opname Nota,exp-opnnota/cform/<br>";
                    if ($this->session->userdata('menu500') == 't' || ($this->session->userdata('user_id') == 'fs1'
                        || $this->session->userdata('user_id') == 'fs2')) {
                        echo $row->i_user . ",Opname Nota Nas,exp-opnnotanas/cform/<br>";
                    }


                    if (/*$this->session->userdata('user_id')=='kabagkeuangan'||*/$this->session->userdata('user_id') == 'admin') {
                        echo $row->i_user . ",Opname Nota Nas(TOP Intern),exp-opnnotanasplus/cform/<br>";
                        #echo $row->i_user.",Opname Nota Nas(Periode),exp-opnnotanasplusperi/cform/<br>";
                    }
                    echo '</ul>';
                }

                if ($this->session->userdata('menu238') == 't') {
                    echo $row->i_user . ",Daftar Detail Nota,exp-detail-daftarnota-prs/cform/<br>";
                }


                if ($this->session->userdata('menu268') == 't') {
                    echo $row->i_user . ",Nota (F-Komersial),exp-daftarnota/cform/<br>";
                }
                if ($this->session->userdata('menu274') == 't') {
                    echo '<li><a href="#">SJP</a><ul>';
                    echo $row->i_user . ",SJP,exp-sjp/cform/<br>";
                    echo $row->i_user . ",SJP GIT,exp-sjpgit/cform/<br>";
                    echo '</ul>';
                }
                if ($this->session->userdata('menu276') == 't') {
                    echo $row->i_user . ",IKHP (Pengeluaran),exp-ikhpkeluar/cform/<br>";
                }
                if ($this->session->userdata('menu287') == 't') {
                    echo $row->i_user . ",Transfer Uang Masuk,exp-kum/cform/<br>";
                }
                if ($this->session->userdata('menu429') == 't') {
                    echo $row->i_user . ",Transfer Uang Keluar,exp-kuk/cform/<br>";
                }
                if ($this->session->userdata('menu303') == 't') {
                    echo $row->i_user . ",Lebih Bayar,exp-lebihbayar/cform/<br>";
                }
                if ($this->session->userdata('menu305') == 't') {
                    echo '<li><a href="#">KN/DN</a><ul>';
                    echo $row->i_user . ",KN/DN,exp-kn/cform/<br>";
                    echo $row->i_user . ",KN/DN per item,exp-kn-item/cform/<br>";
                    echo '</ul>';
                }
                if ($this->session->userdata('menu462') == 't') {
                    echo $row->i_user . ",Opname KN/DN,exp-opnkn/cform/<br>";
                }
                if ($this->session->userdata('menu316') == 't') {
                    echo $row->i_user . ",Lap. AP,exp-ap/cform/<br>";
                }
                if ($this->session->userdata('menu323') == 't') {
                    echo $row->i_user . ",Order Kons,exp-orderkons/cform/<br>";
                }
                if ($this->session->userdata('menu368') == 't') {
                    echo $row->i_user . ",Penjualan Kons,exp-notapb/cform/<br>";
                }
                if ($this->session->userdata('menu405') == 't') {
                    echo $row->i_user . ",Customer List,exp-customerlist/cform/<br>";
                }
                if ($this->session->userdata('menu326') == 't') {
                    echo '<li><a href="#">Container</a><ul>';
                    echo $row->i_user . ",SJP,exp-container/cform/<br>";
                    echo $row->i_user . ",SJ,exp-contsj/cform/<br>";
                    echo '</ul>';
                }
                if ($this->session->userdata('menu344') == 't') {
                    echo '<li><a href="#">Laporan Gudang</a><ul>';
                    echo $row->i_user . ",Per Divisi Per Area,exp-lapgudperdivarea/cform/<br>";
                    echo $row->i_user . ",Per Divisi Nasional,exp-lapgudperdivnas/cform/<br>";
                    echo $row->i_user . ",Per Divisi Per Area Nasional,exp-lapgudperdivareanas/cform/<br>";
                    echo $row->i_user . ",Per Divisi Per Area Nasional (SJ Pusat),exp-lapgudperdivareanaspst/cform/<br>";
                    echo '</ul>';
                }
                if ($this->session->userdata('menu334') == 't') {
                    echo '<li><a href="#">Laporan OP vs DO</a><ul>';
                    echo $row->i_user . ",Per Supplier,exp-opdo/cform/<br>";
                    echo $row->i_user . ",Per Supplier Detail,exp-det-opdo/cform/<br>";
                    echo $row->i_user . ",Nasional,exp-opdonas/cform/<br>";
                    echo '</ul>';
                }
                if ($this->session->userdata('menu339') == 't') {
                    echo '<li><a href="#">Laporan Pembelian</a><ul>';
                    echo $row->i_user . ",PerSupplier/Item,exp-beliperitem/cform/<br>";

                    echo $row->i_user . ",PerPeriode/Item,exp-beliperiode/cform/<br>";
                    echo $row->i_user . ",Per Nota,exp-belipernota/cform/<br>";
                    echo $row->i_user . ",Total Nota,exp-belinotasupplier/cform/<br>";
                    echo $row->i_user . ",DO-Nota,exp-donotabeli/cform/<br>";
                    echo '</ul>';
                }
                echo '</ul>';
            }
            if (
                ($this->session->userdata('menu62') == 't') or ($this->session->userdata('menu63') == 't') or
                ($this->session->userdata('menu64') == 't') or ($this->session->userdata('menu69') == 't') or
                ($this->session->userdata('menu83') == 't') or ($this->session->userdata('menu104') == 't') or
                ($this->session->userdata('menu162') == 't') or ($this->session->userdata('menu178') == 't') or
                ($this->session->userdata('menu175') == 't') or ($this->session->userdata('menu164') == 't') or
                ($this->session->userdata('menu139') == 't') or ($this->session->userdata('menu176') == 't') or
                ($this->session->userdata('menu199') == 't') or ($this->session->userdata('menu200') == 't') or
                ($this->session->userdata('menu186') == 't') or ($this->session->userdata('menu192') == 't') or
                ($this->session->userdata('menu201') == 't') or ($this->session->userdata('menu205') == 't') or
                ($this->session->userdata('menu247') == 't') or ($this->session->userdata('menu248') == 't') or
                ($this->session->userdata('menu251') == 't') or ($this->session->userdata('menu286') == 't') or
                ($this->session->userdata('menu297') == 't') or ($this->session->userdata('menu345') == 't') or
                ($this->session->userdata('menu353') == 't') or ($this->session->userdata('menu355') == 't') or
                ($this->session->userdata('menu356') == 't') or ($this->session->userdata('menu367') == 't') or
                ($this->session->userdata('menu373') == 't') or ($this->session->userdata('menu384') == 't') or
                ($this->session->userdata('menu416') == 't') or ($this->session->userdata('menu421') == 't') or
                ($this->session->userdata('menu436') == 't') or ($this->session->userdata('menu532') == 't') && ($this->session->userdata('user_id') != 'fs1' && $this->session->userdata('user_id') != 'fs2')
            ) {
                echo '<li><a href="#">Cetak</a><ul>';
                if ($this->session->userdata('menu489') == 't') {
                    echo $row->i_user . ",Realisasi DT,printrealisasidt/cform/<br>";
                }
                if ($this->session->userdata('menu509') == 't') {
                    echo $row->i_user . ",Surat Konfirmasi,printpiutang/cform/<br>";
                }

                if ($this->session->userdata('menu62') == 't') {
                    echo '<li><a href="#">SPB</a><ul>';
                    echo $row->i_user . ",Tertentu,printspb/cform/<br>";
                    echo $row->i_user . ",Kelompok,printspbkelompok/cform/<br>";
                    if ($this->session->userdata('menu284') == 't') {
                        echo $row->i_user . ",Pelanggan Baru,printcustomerbaru/cform/<br>";
                    }
                    if ($this->session->userdata('menu285') == 't') {
                        echo $row->i_user . ",SPB Pelanggan Baru,printspbbaru/cform/<br>";
                        echo $row->i_user . ",SPB Pelanggan Baru(Khusus),printspbbarukhusus/cform/<br>";
                    }
                    if ($this->session->userdata('menu353') == 't') {
                        echo $row->i_user . ",SPB Khusus,printspbkhusus/cform/<br>";
                    }
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu421') == 't') {
                    echo $row->i_user . ",Target Penjualan,printtp/cform/<br>";
                }
                if ($this->session->userdata('menu63') == 't' or ($this->session->userdata('menu384') == 't')) {
                    echo '<li><a href="#">OP</a><ul>';
                    echo $row->i_user . ",Tertentu,printop/cform/<br>";
                    echo $row->i_user . ",Kelompok,printopkelompok/cform/<br>";
                    echo $row->i_user . ",Khusus,printopkhusus/cform/<br>";
                    echo $row->i_user . ",Rekap SPB,printoprekap/cform/<br>";
                    echo "</ul></li>";
                }
                if ($this->session->userdata('menu286') == 't') {
                    echo '<li><a href="#">Kontra Bon</a><ul>';
                    echo $row->i_user . ",Periode Tgl,printkontrabon/cform/<br>";
                    echo $row->i_user . ",Periode Dok,printkontrabondoc/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu64') == 't') {

                    echo '<li><a href="#">Nota</a><ul>';
                    echo $row->i_user . ",Tertentu,printnota/cform/<br>";
                    echo $row->i_user . ",Kelompok,printnotakelompok/cform/<br>";
                    echo $row->i_user . ",Khusus,printnotakhusus/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu69') == 't') {
                    echo '<li><a href="#">SPMB</a><ul>';
                    echo $row->i_user . ",Tertentu,printspmb/cform/<br>";
                    echo $row->i_user . ",Khusus,printspmbkhusus/cform/<br>";
                    echo $row->i_user . ",Kelompok,printspmbkelompok/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu83') == 't') {
                    echo '<li><a href="#">Nota Koreksi</a><ul>';
                    echo $row->i_user . ",Tertentu,printnotakoreksi/cform/<br>";
                    echo $row->i_user . ",Kelompok,printnotakoreksikelompok/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu355') == 't') {
                    echo '<li><a href="#">BBK Hadiah</a><ul>';
                    echo $row->i_user . ",Tertentu,printbbkhadiah/cform/<br>";
                    echo $row->i_user . ",Khusus,printbbkhadiahkhusus/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu355') == 't') {
                    echo $row->i_user . ",BBK Barter,printbbkbarterkhusus/cform/<br>";
                }
                if ($this->session->userdata('menu482') == 't') {
                    echo '<li><a href="#">Bon Masuk</a><ul>';
                    echo $row->i_user . ",Tertentu,printbonmasuk/cform/<br>";
                    echo $row->i_user . ",Khusus,printbonmasukkhusus/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu482') == 't') {
                    echo '<li><a href="#">Bon Keluar</a><ul>';
                    echo $row->i_user . ",Tertentu,printbonkeluar/cform/<br>";
                    echo $row->i_user . ",Khusus,printbonkeluarkhusus/cform/<br>";
                    echo '</ul></li>';
                }
                /*					if($this->session->userdata('menu600')=='t'){
                    echo $row->i_user.",Masuk,printbonmasukkhusus/cform/<br>";
                    echo $row->i_user.",Keluar,printbonkeluarkhusus/cform/<br>";
                    }*/
                if ($this->session->userdata('menu104') == 't') {
                    echo '<li><a href="#">Surat Jalan</a><ul>';
                    echo $row->i_user . ",Tertentu,printsj/cform/<br>";
                    echo $row->i_user . ",Khusus,printsjkhusus/cform/<br>";
                    echo $row->i_user . ",Kelompok,printsjkelompok/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu356') == 't') {
                    echo '<li><a href="#">Surat Jalan PB</a><ul>';
                    echo $row->i_user . ",Tertentu,printsjpb/cform/<br>";
                    echo $row->i_user . ",Khusus,printsjpbkhusus/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu248') == 't') {
                    echo '<li><a href="#">SJ Retur</a><ul>';
                    echo $row->i_user . ",Tertentu,printsjr/cform/<br>";
                    echo $row->i_user . ",Khusus,printsjrkhusus/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu297') == 't') {
                    echo '<li><a href="#">SJ Retur Toko</a><ul>';
                    echo $row->i_user . ",Tertentu,printsjrt/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu247') == 't') {
                    echo '<li><a href="#">BBM-Retur</a><ul>';
                    echo $row->i_user . ",Tertentu,printbbmretur/cform/<br>";
                    echo $row->i_user . ",Khusus,printbbmreturkhusus/cform/<br>";
                    echo $row->i_user . ",Kelompok,printbbmreturkelompok/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu162') == 't') {
                    echo '<li><a href="#">BBM-AP</a><ul>';
                    echo $row->i_user . ",Tertentu,printbbmap/cform/<br>";
                    echo $row->i_user . ",Kelompok,printbbmapkelompok/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu178') == 't') {
                    echo '<li><a href="#">KN-Retur</a><ul>';
                    echo $row->i_user . ",Tertentu,printknretur/cform/<br>";
                    echo $row->i_user . ",Kelompok,printknreturkelompok/cform/<br>";
                    echo $row->i_user . ",Khusus,printknreturkhusus/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu436') == 't') {
                    echo '<li><a href="#">KN-Non Retur</a><ul>';
                    echo $row->i_user . ",Tertentu,printknnonretur/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu175') == 't') {
                    echo '<li><a href="#">SJ Pinjaman</a><ul>';
                    echo $row->i_user . ",Khusus,printsjpkhusus/cform/<br>";
                    echo $row->i_user . ",Tertentu,printsjp/cform/<br>";
                    echo $row->i_user . ",Kelompok,printsjpkelompok/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu164') == 't') {
                    echo '<li><a href="#">DKB</a><ul>';
                    echo $row->i_user . ",Tertentu,printdkb/cform/<br>";
                    echo $row->i_user . ",Kelompok,printdkbkelompok/cform/<br>";
                    echo $row->i_user . ",Khusus,printdkbkhusus/cform/<br>";

                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu139') == 't') {
                    echo '<li><a href="#">BAPB</a><ul>';
                    echo $row->i_user . ",Tertentu,printbapb/cform/<br>";
                    echo $row->i_user . ",Kelompok,printbapbkelompok/cform/<br>";
                    echo $row->i_user . ",Khusus,printbapbkhusus/cform/<br>";
                    if ($this->session->userdata('user_id') == 'admgud') {
                        echo $row->i_user . ",Khusus Pusat,printbapbkhusus1/cform/<br>";
                    }
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu262') == 't') {
                    echo '<li><a href="#">BAPB SJP</a><ul>';
                    echo $row->i_user . ",Tertentu,printbapbsjp/cform/<br>";
                    echo $row->i_user . ",Khusus,printbapbsjpkhusus/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu176') == 't') {
                    echo '<li><a href="#">Daftar Tagihan</a><ul>';
                    echo $row->i_user . ",Tertentu,printdaftartagihan/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu199') == 't') {
                    echo $row->i_user . ",Giro Pelanggan,printgiro/cform/<br>";
                }
                if ($this->session->userdata('menu200') == 't') {
                    echo $row->i_user . ",Transfer Uang Masuk,printtransferuangmasuk/cform/<br>";
                }
                if ($this->session->userdata('menu186') == 't') {
                    echo '<li><a href="#">SJ Komersial</a><ul>';
                    echo $row->i_user . ",Tertentu,printsjk/cform/<br>";
                    echo $row->i_user . ",Kelompok,printsjkkelompok/cform/<br>";
                    echo $row->i_user . ",Khusus,printsjkkhusus/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu186') == 't') {
                    echo '<li><a href="#">Faktur Komersial</a><ul>';
                    echo $row->i_user . ",Tertentu,printfkom/cform/<br>";
                    echo $row->i_user . ",Kelompok,printfkomkelompok/cform/<br>";
                    echo $row->i_user . ",Khusus,printfkomkhusus/cform/<br>";
                    echo $row->i_user . ",BBK-Hadiah,printfkombbk/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu192') == 't') {
                    echo '<li><a href="#">Faktur Pajak Standar</a><ul>';
                    echo $row->i_user . ",Tertentu,printfpajak/cform/<br>";
                    echo $row->i_user . ",Kelompok,printfpajakkelompok/cform/<br>";
                    echo $row->i_user . ",BBK-Hadiah,printfpajakbbk/cform/<br>";
                    echo $row->i_user . ",Kumulatif,printfpajakkum/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu367') == 't') {
                    echo '<li><a href="#">Faktur Pajak Pengganti</a><ul>';
                    echo $row->i_user . ",Tertentu,printfpajakpengganti/cform/<br>";
                    /*
                echo $row->i_user.",Kelompok,printfpajakpenggantikelompok/cform/<br>";
                */
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu201') == 't') {
                    echo $row->i_user . ",LKH,printlkh/cform/<br>";
                }
                if ($this->session->userdata('menu205') == 't') {
                    echo $row->i_user . ",IKHP,printikhp/cform/<br>";
                    echo $row->i_user . ",IKHP Tunai,printikhptunai/cform/<br>";
                }
                if ($this->session->userdata('menu251') == 't') {
                    echo '<li><a href="#">Nota Retur</a><ul>';
                    echo $row->i_user . ",Tertentu,printnotaretur/cform/<br>";
                    echo $row->i_user . ",Khusus,printnotareturkhusus/cform/<br>";
                    echo '</ul></li>';
                }
                if ($this->session->userdata('menu532') == 't') {
                    echo $row->i_user . ",Kartu Langganan,kartulangganan1/cform/<br>";
                }
                echo '</ul>';
            }
            echo '<li><a href="../manual/index.php">Panduan Manual</a></li>';
            echo '</ul>';
        } // * END FOREACH
    }
    public function updatemenu()
    {
        for ($i = 0; $i <= 600; $i++) {

            $data1 = $this->db->query(" SELECT i_user, f_menu$i AS menu FROM tm_user WHERE i_user = 'admin' ")->row();

            if ($data1->menu == 'f') {
                echo "f_menu" . $i . " " . $data1->menu . " > ";

                $this->db->query(" UPDATE tm_user SET f_menu$i = 't' WHERE i_user = '$data1->i_user' ");

                $data2 = $this->db->query(" SELECT i_user, f_menu$i AS menu FROM tm_user WHERE i_user = '$data1->i_user' ")->row();

                echo $data2->menu . "<br>";
            }
        }
    }
}

/* End of file Coba.php */
