<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu118')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('coa');
			$this->load->model('coa/mmaster');
			$data['icoa']='';
			$this->load->view('coa/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu118')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('coa');
			$this->load->view('coa/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu118')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('coa')." update";
			if(
				$this->uri->segment(4)
			  ){
				$data['icoa']		= $this->uri->segment(4);
				$icoa				= $this->uri->segment(4);
				$this->load->model("coa/mmaster");
				$data['isi']=$this->mmaster->baca($icoa);
		 		$this->load->view('coa/vformupdate',$data);
			}else{
				$this->load->view('coa/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu118')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$icoa 			= $this->input->post('icoa', TRUE);
			$ecoaname		= $this->input->post('ecoaname', TRUE);
			if (
				(isset($icoa) && $icoa != '')
			   )
			{
				$this->load->model('coa/mmaster');
				$this->db->trans_begin();
				$this->mmaster->update($icoa,$ecoaname);
				$nomor=$icoa." - ".$ecoaname;
			}
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
          while($row=pg_fetch_assoc($rs)){
	          $ip_address	  = $row['ip_address'];
	          break;
          }
        }else{
          $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
          $now	  = $row['c'];
        }
        $pesan='Update CoA:('.$icoa.')-'.$ecoaname;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $nomor;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu118')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$icoa 			= $this->input->post('icoa', TRUE);
			$ecoaname		= $this->input->post('ecoaname', TRUE);
			if (
				(isset($icoa) && $icoa != '')
			   )
			{
				$this->load->model('coa/mmaster');
				$this->db->trans_begin();
				$cek=$this->mmaster->cek($icoa);
				if(!$cek){							
					$this->mmaster->insert($icoa,$ecoaname);

          $sess=$this->session->userdata('session_id');
          $id=$this->session->userdata('user_id');
          $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
          $rs		= pg_query($sql);
          if(pg_num_rows($rs)>0){
            while($row=pg_fetch_assoc($rs)){
              $ip_address	  = $row['ip_address'];
              break;
            }
          }else{
            $ip_address='kosong';
          }
          $query 	= pg_query("SELECT current_timestamp as c");
          while($row=pg_fetch_assoc($query)){
            $now	  = $row['c'];
          }
          $pesan='Simpan CoA:('.$icoa.')-'.$ecoaname;
          $this->load->model('logger');
          $this->logger->write($id, $ip_address, $now , $pesan );

					$nomor=$icoa." - ".$ecoaname;
				}else{
					$nomor="Kode rekening ".$icoa." sudah ada, untuk mengubah lewat menu edit CoA";
				}
			}
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
				$data['sukses']			= true;
				$data['inomor']			= $nomor;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
}
?>
