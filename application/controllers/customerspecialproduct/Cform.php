<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu36')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerspecialproduct');
			$data['icustomerspecialproduct']='';
			$this->load->model('customerspecialproduct/mmaster');
			$data['isi']=$this->mmaster->bacasemua();

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Produk Khusus (Pelanggan)";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('customerspecialproduct/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu36')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerspecialproduct 	= $this->input->post('icustomerspecialproduct', TRUE);
			$ecustomerspecialproductname 	= $this->input->post('ecustomerspecialproductname', TRUE);

			if ((isset($icustomerspecialproduct) && $icustomerspecialproduct != '') && (isset($ecustomerspecialproductname) && $ecustomerspecialproductname != ''))
			{
				$this->load->model('customerspecialproduct/mmaster');
				$this->mmaster->insert($icustomerspecialproduct,$ecustomerspecialproductname);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master Produk Khusus (Pelanggan):('.$icustomerspecialproduct.') -'.$ecustomerspecialproductname;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

				$data['page_title'] = $this->lang->line('master_customerspecialproduct');
				$data['icustomerspecialproduct']='';
				$data['isi']=$this->mmaster->bacasemua();
				$this->load->view('customerspecialproduct/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu36')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerspecialproduct');
			$this->load->view('customerspecialproduct/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu36')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerspecialproduct')." update";
			if($this->uri->segment(4)){
				$icustomerspecialproduct = $this->uri->segment(4);
				$data['icustomerspecialproduct'] = $icustomerspecialproduct;
				$this->load->model('customerspecialproduct/mmaster');
				$data['isi']=$this->mmaster->baca($icustomerspecialproduct);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Produk Khusus (Pelanggan):('.$icustomerspecialproduct.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		 		$this->load->view('customerspecialproduct/vmainform',$data);
			}else{
				$this->load->view('customerspecialproduct/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu36')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerspecialproduct	= $this->input->post('icustomerspecialproduct', TRUE);
			$ecustomerspecialproductname 	= $this->input->post('ecustomerspecialproductname', TRUE);
			$this->load->model('customerspecialproduct/mmaster');
			$this->mmaster->update($icustomerspecialproduct,$ecustomerspecialproductname);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master Produk Khusus (Pelanggan):('.$icustomerspecialproduct.') -'.$ecustomerspecialproductname;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

			$data['page_title'] = $this->lang->line('master_customerspecialproduct');
			$data['icustomerspecialproduct']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('customerspecialproduct/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu36')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomerspecialproduct	= $this->uri->segment(4);
			$this->load->model('customerspecialproduct/mmaster');
			$this->mmaster->delete($icustomerspecialproduct);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
			  $ip_address	  = $row['ip_address'];
			  break;
			}
			}else{
			$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
			$now	  = $row['c'];
			}
			$pesan='Delete Master Produk Khusus (Pelanggan):'.$icustomerspecialproduct;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);
			
			$data['page_title'] = $this->lang->line('master_customerspecialproduct');
			$data['icustomerspecialproduct']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('customerspecialproduct/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
