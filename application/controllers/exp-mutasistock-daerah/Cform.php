<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu472') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('exp-mutasistock-daerah/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu472') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-mutasistock-daerah/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$iarea = $this->input->post('iarea');
			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}
			if ($iarea == '') $iarea = $this->uri->segment(5);
			$query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
			$st = $query->row();
			$store = $st->i_store;
			$this->mmaster->proses($iperiode, $iarea);
			$config['base_url'] = base_url() . 'index.php/exp-mutasistock-daerah/cform/view/' . $iperiode . '/' . $store . '/';
			$query = $this->db->query(" select i_product from tm_mutasi where e_mutasi_periode = '$iperiode' and i_store='$store'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['istore']	= $store;
			$data['isi']		= $this->mmaster->baca($iperiode, $store, $config['per_page'], $this->uri->segment(6), $cari);
			$this->load->view('exp-mutasistock-daerah/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu472') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-mutasistock-daerah/mmaster');
			$iperiode	= $this->input->post('iperiode');
			$iarea = $this->input->post('iarea');
			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}
			if ($iarea == '') $iarea = $this->uri->segment(5);
			if (strlen($iarea) == 1) $iarea = '0' . $iarea;
			$query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
			$st = $query->row();
			$store = $st->i_store;
			$config['base_url'] = base_url() . 'index.php/exp-mutasistock-daerah/cform/view/' . $iperiode . '/' . $store . '/';
			$query = $this->db->query(" select i_product from tm_mutasi where e_mutasi_periode = '$iperiode' and i_store='$store'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('expmutasi');
			$cari = '';
			$data['cari'] = $cari;
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['istore']	= $store;
			$data['isi']		= $this->mmaster->baca($iperiode, $store, $config['per_page'], $this->uri->segment(6), $cari);
			$this->load->view('exp-mutasistock-daerah/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu472') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$dto	= $this->input->post('dto');
			if ($iperiode == '') $iperiode = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/exp-mutasistock-daerah/cform/view/' . $iperiode . '/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_target a, tr_area b
										where a.i_periode = '$iperiode'
										and a.i_area=b.i_area
										and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('exp-mutasistock-daerah/mmaster');
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode, $config['per_page'], $this->uri->segment(6), $cari);
			$this->load->view('exp-mutasistock-daerah/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu472') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-mutasistock-daerah/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($area1 == '00' or $area2 == '00' or $area3 == '00' or $area4 == '00' or $area5 == '00') {
				$query = $this->db->query("select * from tr_area", false);
			} else {
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('listspmb/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu472') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url() . 'index.php/exp-mutasistock-daerah/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if ($area1 == '00' or $area2 == '00' or $area3 == '00' or $area4 == '00' or $area5 == '00') {
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')", false);
			} else {
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listspmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('listspmb/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-mutasistock-daerah/cform/store/index/';
			$iuser   = $this->session->userdata('user_id');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
			                        from tr_area a, tr_store b, tr_store_location c
                        			where a.i_store=b.i_store and b.i_store=c.i_store
			                        and (a.i_area in ( select i_area from tm_user_area where i_user='$iuser') )
			                        and not a.i_store in ('AA','PB') and c.i_store_location='00' 
			                        ", false);

			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-mutasistock-daerah/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi'] = $this->mmaster->bacastore($config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('exp-mutasistock-daerah/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-mutasistock-daerah/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$iuser   = $this->session->userdata('user_id');
			$config['base_url'] = base_url() . 'index.php/listmutasidaerah/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
			                        from tr_area a, tr_store b, tr_store_location c
                        			where a.i_store=b.i_store and b.i_store=c.i_store
			                        and (a.i_area in ( select i_area from tm_user_area where i_user='$iuser') )
			                        and not a.i_store in ('AA','PB') and c.i_store_location='00'
			                        ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-mutasistock-daerah/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi'] = $this->mmaster->caristore($cari, $config['per_page'], $this->uri->segment(5), $iuser);
			$this->load->view('exp-mutasistock-daerah/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu472') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-mutasistock-daerah/mmaster');
			$iperiode = $this->uri->segment(4);
			$iarea    = $this->uri->segment(5);
			$iproduct = $this->uri->segment(6);
			$this->load->model('exp-mutasistock-daerah/mmaster');
			$data['page_title'] = $this->lang->line('expmutasi');
			$data['iperiode']	= $iperiode;
			$data['iarea']	  = $iarea;
			$data['iproduct']	= $iproduct;
			$data['detail']	= $this->mmaster->detail($iperiode, $iarea, $iproduct);
			$this->load->view('exp-mutasistock-daerah/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu472') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-mutasistock-daerah/mmaster');

			$cari			= strtoupper($this->input->post('cari'));
			$iperiode		= $this->input->post('iperiode');
			$iarea 			= $this->input->post('iarea');
			$istore 		= $this->input->post('istore');
			$istorelocation = $this->input->post('istorelocation');

			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}

			define('BOOLEAN_FIELD',   'L');
			define('CHARACTER_FIELD', 'C');
			define('DATE_FIELD',      'D');
			define('NUMBER_FIELD',    'N');
			define('READ_ONLY',  '0');
			define('WRITE_ONLY', '1');
			define('READ_WRITE', '2');

			$a	  = substr($iperiode, 0, 4);
			$b	  = substr($iperiode, 4, 2);
			$peri = mbulan($b) . " - " . $a;

			if ($iarea == '') $iarea = $this->uri->segment(5);

			$query 	= $this->db->query("SELECT a.i_store, a.e_area_name, b.i_store_location, b.e_store_locationname
                                 			FROM tr_area a, tr_store_location b 
                                 			WHERE a.i_store = b.i_store AND a.i_area = '$istore' AND b.i_store_location = '$istorelocation'");

			$st			  = $query->row();
			$namaarea	  = $st->e_area_name;
			$storeloc	  = $st->i_store_location;
			$store		  = $st->i_store;
			$namastoreloc = $st->e_store_locationname;

			if ($istore == 'AA') {
				$daerah = '00';
			} else {
				$daerah = $istore;
			}

			$db_file = 'spb/' . $daerah . '/gd' . $daerah . $iperiode . '.dbf';
			if (file_exists($db_file)) {
				// @chmod($db_file, 777);
				@unlink($db_file);
			}

			$dbase_definition = array(
				array('KODEPROD',  CHARACTER_FIELD,  7),
				array('NAMAPROD',  CHARACTER_FIELD,  45),
				array('STOCKOPNAM',  NUMBER_FIELD,  7, 0),
				array('GRADE',  CHARACTER_FIELD,  1),
			);

			//!  $create = @ dbase_create($db_file, $dbase_definition)
			//!  or die ("Could not create dbf file <i>$db_file</i>.");
			//!  $id = @ dbase_open ($db_file, READ_WRITE)
			//!  or die ("Could not open dbf file <i>$db_file</i>."); 

			//* Untuk SO
			if ($iperiode > '201512') {
				$this->db->select("	* from f_mutasi_stock_daerah_saldoakhir2('$iperiode','$store','$storeloc')
									order by e_product_groupname, e_product_name", false);
			} else {
				$this->db->select("	* from f_mutasi_stock_daerah('$iperiode','$store','$storeloc')
								order by e_product_groupname, e_product_name", false);
			}

			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:P6'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'KODEPROD');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'NAMAPROD');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'STOCKOPNAM');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'GRADE');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i = 2;
				$j = 2;
				$xarea = '';
				$saldo = 0;
				$no = 0;
				$group = '';
				foreach ($query->result() as $row) {
					$group = $row->e_product_groupname;
					$no++;
					$opname = $row->n_saldo_stockopname;
					$selisih = $opname - $row->n_saldo_akhir;
					$saldoawal = '00';
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':D' . $i
					);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->i_product, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->e_product_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $saldoawal, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->i_product_grade, Cell_DataType::TYPE_STRING);
					$i++;
					$j++;
				}
				//die();
				$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
				$nama = 'so' . $daerah . $iperiode . '.xls';
				if (file_exists('spb/' . $daerah . '/' . $nama)) {
					@chmod('spb/' . $daerah . '/' . $nama, 0777);
					@unlink('spb/' . $daerah . '/' . $nama);
				}
				$objWriter->save("spb/" . $daerah . '/' . $nama);

				$data['sukses'] = true;
				$data['folder'] = 'spb/' . $daerah;
				$data['inomor']	= $nama; /* SO */

				$this->load->view('nomorurl', $data);
			}
			##############################################################################################################

			//*   Laporan Mutasi
			if ($iperiode > '201512') {
				$this->db->select("		a.*,
										ctg.e_sales_categoryname
									FROM
										f_mutasi_stock_daerah_saldoakhir2('$iperiode','$store','$storeloc') a
									INNER JOIN tr_product b ON
										a.i_product = b.i_product
									LEFT JOIN tr_product_sales_category ctg ON
										b.i_sales_category = ctg.i_sales_category
									ORDER BY
										e_product_groupname,
										e_product_name ", false);
			} else {
				$this->db->select("	* from f_mutasi_stock_daerah('$iperiode','$store','$storeloc')
								order by e_product_groupname, e_product_name", false);
			}

			$query = $this->db->get();

			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');

			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan Mutasi ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);

			if ($query->num_rows() > 0) {

				//* STYLE UNTUK JUDUL A2 SAMPAI A4
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment'  => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'   => Style_Alignment::VERTICAL_CENTER,
							'wrap'       => true
						)
					),
					'A2:A4'
				);

				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getRowDimension('6')->setRowHeight(25);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN MUTASI STOK GUDANG DAERAH');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 16, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Gudang : ' . $namaarea . '    Lokasi : ' . $namastoreloc . '     Bulan : ' . $peri);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 16, 3);

				//* STYLE HURUF JUDUL KOLOM
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:Q6'
				);

				$borderheader = array(
					'borders' => array(
						'top' 	=> array('style' => Style_Border::BORDER_THIN),
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					)
				);

				$bordersubjudul = array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => false,
						'italic' => false,
						'size'  => 12
					),
					'borders' => array(
						'bottom' => array('style' => Style_Border::BORDER_THIN),
						'left'  => array('style' => Style_Border::BORDER_THIN),
						'right' => array('style' => Style_Border::BORDER_THIN)
					),
					'alignment' => array(
						'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				);

				$objPHPExcel->getActiveSheet()->mergeCells('A5:A6');
				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No Urut');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray($borderheader);
				$objPHPExcel->getActiveSheet()->mergeCells('B5:B6');
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Kategori Penjualan');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray($borderheader);

				$objPHPExcel->getActiveSheet()->mergeCells('C5:C6');
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Kode Barang');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray($borderheader);

				$objPHPExcel->getActiveSheet()->mergeCells('D5:D6');
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'Nama Barang');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray($borderheader);

				//* SALDO AWAL
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4, 5, 6, 5); //* cara baca mergecell kolom awal, no baris, no kolom akhir, no baris
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Saldo Awal');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray($borderheader);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'Saldo Awal');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray($borderheader);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'GIT Awal');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray($borderheader);
				$objPHPExcel->getActiveSheet()->setCellValue('G6', 'GIT Jual Awal');
				$objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray($borderheader);
				//* MASUK
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Penerimaan');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray($borderheader);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Dari Pusat');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray($borderheader);
				//* KELUAR
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8, 5, 10, 5);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Pengeluaran');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray($borderheader);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Penjualan');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray($borderheader);

				if ($iarea == 'AA') {
					$judul = 'Ke Cabang';
				} else {
					$judul = 'Ke Pusat';
				}

				$objPHPExcel->getActiveSheet()->setCellValue('J6', $judul);
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray($borderheader);
				$objPHPExcel->getActiveSheet()->setCellValue('K6', 'MO');
				$objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray($borderheader);
				//* ADJUSMENT
				$objPHPExcel->getActiveSheet()->mergeCells('L5:L6');
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Adjus');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray($borderheader);
				//* SALDO AKHIR

				$objPHPExcel->getActiveSheet()->mergeCells('M5:M6');
				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Saldo Akhir');
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray($borderheader);
				//* STOCKOPNAME
				$objPHPExcel->getActiveSheet()->mergeCells('N5:N6');
				$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Stockopname');
				$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray($borderheader);
				//* SELISIH
				$objPHPExcel->getActiveSheet()->mergeCells('O5:O6');
				$objPHPExcel->getActiveSheet()->setCellValue('O5', 'Selisih (Pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray($borderheader);
				//* GIT
				$objPHPExcel->getActiveSheet()->mergeCells('P5:P6');
				$objPHPExcel->getActiveSheet()->setCellValue('P5', 'GIT');
				$objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray($borderheader);
				//* GIT Penj
				$objPHPExcel->getActiveSheet()->mergeCells('Q5:Q6');
				$objPHPExcel->getActiveSheet()->setCellValue('Q5', 'GIT Penj');
				$objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray($borderheader);

				$i		= 7;
				$j		= 7;
				$xarea	= '';
				$saldo	= 0;

				$no		= 0;
				$group	= '';
				$selisih = 0;

				foreach ($query->result() as $row) {
					if ($group == '' || $group != $row->e_product_groupname) {
						$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, $i, 16, $i);
						$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $row->e_product_groupname);
						$objPHPExcel->getActiveSheet()->getStyle('A' . $i)->applyFromArray($bordersubjudul);
						$objPHPExcel->getActiveSheet()->getStyle('A' . $i . ':Q' . $i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('A5FCB6');

						$no = 0;
						$i++;
					}

					//* DEKLARASIIN n_mutasi_git JADI n_saldo_git mengikuti query di model informasi mutasi stok daerah
					$row->n_saldo_git = $row->n_mutasi_git;

					$group		 = $row->e_product_groupname;
					$opname		 = $row->n_saldo_stockopname + $row->n_mutasi_git + $row->n_git_penjualan;

					//* RUMUS SELISIH SEBELUMNYA (DI COMMENT 28 MEI 2021)
					//! $selisih	 = $opname-$row->n_saldo_akhir;

					//* RUMUS SELISIH SEKARANG (PER 28 MEI 2021) MENGIKUTI RUMUS DI INFORMASI MUTASI STOK DAERAH
					if ($row->adjus > 1) {
						$adjus = $row->adjus;
					} else {
						$adjus = 0;
					}


					$selisih	 = $selisih = $selisih + (($row->n_saldo_stockopname + $row->n_saldo_git + $row->n_git_penjualan + $adjus) - $row->n_saldo_akhir);
					$saldoawal	 = $row->n_saldo_awal + $row->n_mutasi_gitasal + $row->n_git_penjualanasal;
					$gitawal	 = $row->n_mutasi_gitasal;
					$gitjualawal = $row->n_git_penjualanasal;

					$no++;

					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							),
							'borders' => array(
								'bottom' => array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							)
						),
						'A' . $i . ':Q' . $i
					);

					$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $no);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, $row->e_sales_categoryname);
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $row->i_product);
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $saldoawal);
					$objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $gitawal);
					$objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $gitjualawal);
					$objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $row->n_mutasi_bbm);
					$objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $row->n_mutasi_penjualan);
					$objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $row->n_mutasi_bbk);
					$objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $row->n_mutasi_ketoko);
					$objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $row->adjus);
					$objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $row->n_saldo_akhir);
					$objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $row->n_saldo_stockopname);
					$objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $row->selisih);
					$objPHPExcel->getActiveSheet()->setCellValue('P' . $i, $row->n_saldo_git);
					$objPHPExcel->getActiveSheet()->setCellValue('Q' . $i, $row->n_git_penjualan);

					$i++;
					$j++;

					$isi = array($row->i_product, $row->e_product_name, 0, 'A'); #$row->i_product_grade);
					//! dbase_add_record ($id, $isi) or die ("Data TIDAK bisa ditambahkan ke file <i>$db_file</i>."); 
				}
				//! dbase_close($id);
				//! @chmod($db_file, 0777);
				$x = $i - 1;
			}

			$objWriter 	= IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama		= 'gd' . $daerah . $iperiode . '.xls';
			if (file_exists('spb/' . $daerah . '/' . $nama)) {
				// @chmod('spb/'.$daerah.'/'.$nama, 0777);
				@unlink('spb/' . $daerah . '/' . $nama);
			}
			$objWriter->save("spb/" . $daerah . '/' . $nama);
			//chmod('spb/'.$daerah.'/'.$nama, 777);

			$this->logger->writenew('Export Mutasi Periode:' . $iperiode . ' Area:' . $iarea);

			$data['sukses'] = true;
			$data['folder'] = 'spb/' . $daerah;
			$data['inomor']	= $nama; /* "Laporan Mutasi Stok Daerah" */
			@chmod('spb/' . $daerah . '/' . $nama, 0777);
			// @chmod($db_file, 0777);

			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
