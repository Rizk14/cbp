<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->dbutil();
    $this->load->helper('file');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu503')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listaccplafond');
			$data['iperiodeawal']='';
			$data['iperiodeakhir']='';
			$data['iarea']='';
			$this->load->view('listaccplafond/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu503')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listaccplafond');
			$iperiodeawal	  = $this->input->post('iperiodeawal');
			$iperiodeakhir	= $this->input->post('iperiodeakhir');
			$iarea		      = $this->input->post('iarea');
			
			if($iperiodeawal=='') $iperiodeawal=$this->uri->segment(4);
			if($iperiodeakhir=='') $iperiodeakhir=$this->uri->segment(5);
			if($iarea=='') $iarea=$this->uri->segment(6);
			$this->load->model('listaccplafond/mmaster');
#			$data['status']         = 'view';
			$data['iarea']          = $iarea;
			$data['iperiodeawal']   = $iperiodeawal;
			$data['iperiodeakhir']  = $iperiodeakhir;
			$data['isi']		        = $this->mmaster->bacaperiode($iperiodeawal,$iperiodeakhir,$iarea);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Informasi Plafond Periode:'.$iperiodeawal.' s/d '.$iperiodeakhir;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('listaccplafond/vformview', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu503')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listaccplafond');
			$this->load->view('listaccplafond/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu503')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispmb	= $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$dfrom	= $this->uri->segment(7);
			$dto	= $this->uri->segment(8);
			$this->load->model('listaccplafond/mmaster');
			$this->mmaster->delete($ispmb);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/listaccplafond/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_spmb a, tr_area b
								where a.i_area=b.i_area and a.f_spmb_cancel='f'
								and (upper(a.i_spmb) like '%$cari%')
								and a.i_area='$iarea' and
								a.d_spmb >= to_date('$dfrom','dd-mm-yyyy') AND
								a.d_spmb <= to_date('$dto','dd-mm-yyyy') order by b.e_area_name asc ",false);																
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listaccplafond');
			$this->load->model('listaccplafond/mmaster');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listaccplafond/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu503')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listaccplafond/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_spmb a, tr_area b
																	where a.i_area=b.i_area and a.f_spmb_cancel='f'
																	and (upper(a.i_spmb) like '%$cari%')
																	and a.i_area='$iarea' and
																	a.d_spmb >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_spmb <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('listaccplafond');
			$this->load->model('listaccplafond/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listaccplafond/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu503')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listaccplafond/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';

			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listaccplafond/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listaccplafond/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu503')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listaccplafond/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listaccplafond/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listaccplafond/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu503')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listaccplafond/cform/store/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
				$query = $this->db->query("select distinct(c.i_store) as i_store
                                  from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store");
			} else {
				$query = $this->db->query("select distinct(c.i_store) as i_store
													         from tr_store_location a, tr_store b, tr_area c
													         where a.i_store = b.i_store and b.i_store=c.i_store
													         and (c.i_area = '$area1' or c.i_area = '$area2' or
													         c.i_area = '$area3' or c.i_area = '$area4' or
													         c.i_area = '$area5')");				
			}
			$config['total_rows'] = $query->num_rows(); 	
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listaccplafond/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listaccplafond/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu503')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listaccplafond/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
        $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')",false);
      }else{
			  $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')
                                    and (c.i_area = '$area1' or c.i_area = '$area2' or
                                    c.i_area = '$area3' or c.i_area = '$area4' or
                                    c.i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listaccplafond/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listaccplafond/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export(){
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu503')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('listaccplafond/mmaster');
			$iperiodeawal  	= $this->input->post('iperiodeawal');
			$iperiodeakhir	= $this->input->post('iperiodeakhir');
      $a=substr($iperiodeawal,2,2);
	    $b=substr($iperiodeawal,4,2);
		  $periodeawal=mbulan($b)." - ".$a;
      $a=substr($iperiodeakhir,2,2);
	    $b=substr($iperiodeakhir,4,2);
		  $periodeakhir=mbulan($b)." - ".$a;
      $report = $this->mmaster->bacano($iperiodeawal,$iperiodeakhir);
      $nama='plafon_acc_'.$iperiodeawal.'-'.$iperiodeakhir.'.csv';

      if(file_exists('excel/00/'.$nama)){
        @chmod('excel/00/'.$nama, 0777);
        unlink('excel/00/'.$nama);
      }
      $new_report = $this->dbutil->csv_from_result($report);
      write_file('excel/00/'.$nama,$new_report);
      @chmod('excel/00/'.$nama, 0777);
		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
		  $data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Export Plafond ACC Periode:'.$periodeawal.' s/d '.$periodeakhir;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu503')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
  		){
			$data['page_title'] = $this->lang->line('listaccplafond');
      $iarea        = $this->input->post('iarea');
			$icustomer	  = $this->input->post('icustomer');
			$iperiodeawal	= $this->input->post('iperiodeawal');
			$iperiodeakhir= $this->input->post('iperiodeakhir');
			if($icustomer=='') $icustomer=$this->uri->segment(4);
			if($iperiodeawal=='') $iperiodeawal=$this->uri->segment(5);
			if($iperiodeakhir=='') $iperiodeakhir=$this->uri->segment(6);
			if($iarea=='') $iarea=$this->uri->segment(7);

			$this->load->model('listaccplafond/mmaster');
			$data['icustomer']      = $icustomer;
			$data['iperiodeawal']   = $iperiodeawal;
			$data['iperiodeakhir']  = $iperiodeakhir;
      $data['iarea']          = $iarea;
			$data['isi']		        = $this->mmaster->bacapercustomer($iperiodeawal,$iperiodeakhir,$icustomer,$iarea);
      $data['status']         = 'update';
/*			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Informasi Plafond Periode:'.$iperiodeawal.' s/d '.$iperiodeakhir;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );*/
			$this->load->view('listaccplafond/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
  function update(){
    if (
       (($this->session->userdata('logged_in')) &&
       ($this->session->userdata('menu503')=='t')) ||
       (($this->session->userdata('logged_in')) &&
       ($this->session->userdata('allmenu')=='t'))
       ){
          $iarea          = $this->input->post('iarea', FALSE);
          $icustomer      = $this->input->post('icustomer', FALSE);
          $vplafondacc	  = $this->input->post('vplafondacc', TRUE);
          $vplafondacc    = str_replace(',','',$vplafondacc);
		      $iperiodeawal	  = $this->input->post('iperiodeawal');
		      $iperiodeakhir	= $this->input->post('iperiodeakhir');
    			$iarea		      = $this->input->post('iarea');
    		  $id=$this->session->userdata('user_id');
    		  
          $this->load->model('listaccplafond/mmaster');
          $data['page_title'] = $this->lang->line('updateplafond');
          $this->mmaster->updateplafond($iarea,$icustomer,$vplafondacc,$iperiodeawal,$iperiodeakhir,$id);
          $this->mmaster->updategroupbayar($icustomer,$vplafondacc);
            $sess=$this->session->userdata('session_id');
            $id=$this->session->userdata('user_id');
            $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs      = pg_query($sql);
              if(pg_num_rows($rs)>0){
                while($row=pg_fetch_assoc($rs)){
                  $ip_address   = $row['ip_address'];
                  break;
                }
              }else{
                $ip_address='kosong';
              }
            $query   = pg_query("SELECT current_timestamp as c");
              while($row=pg_fetch_assoc($query)){
              $now    = $row['c'];
            }
            $pesan='Update Plafon '.$icustomer;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now , $pesan );

			      $data['status'] = 'view';
			      $data['iarea'] = $iarea;
			      $data['iperiodeawal'] = $iperiodeawal;
			      $data['iperiodeakhir'] = $iperiodeakhir;
			      $data['isi']		= $this->mmaster->bacaperiode($iperiodeawal,$iperiodeakhir,$iarea);
		        $this->load->view('listaccplafond/vmainform', $data);
          }else{
            $this->load->view('awal/index.php');
          }
        }      
  }
?>
