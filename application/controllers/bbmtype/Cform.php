<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu6')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_bbmtype');
			$data['ibbmtype']='';
			$this->load->model('bbmtype/mmaster');
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('bbmtype/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu6')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ibbmtype 	= $this->input->post('ibbmtype', TRUE);
			$ebbmtypename 	= $this->input->post('ebbmtypename', TRUE);

			if ((isset($ibbmtype) && $ibbmtype != '') && (isset($ebbmtypename) && $ebbmtypename != ''))
			{
				$this->load->model('bbmtype/mmaster');
				$this->mmaster->insert($ibbmtype,$ebbmtypename);

			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Input Jenis BBM:('.$ibbmtype.')-'.$ebbmtypename;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );

			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu6')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_bbmtype');
			$this->load->view('bbmtype/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu6')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_bbmtype')." update";
			if($this->uri->segment(4)){
				$ibbmtype = $this->uri->segment(4);
				$data['ibbmtype'] = $ibbmtype;
				$this->load->model('bbmtype/mmaster');
				$data['isi']=$this->mmaster->baca($ibbmtype);
		 		$this->load->view('bbmtype/vmainform',$data);
			}else{
				$this->load->view('bbmtype/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu6')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ibbmtype	= $this->input->post('ibbmtype', TRUE);
			$ebbmtypename 	= $this->input->post('ebbmtypename', TRUE);
			$this->load->model('bbmtype/mmaster');
			$this->mmaster->update($ibbmtype,$ebbmtypename);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
		  while($row=pg_fetch_assoc($query)){
			  $now	  = $row['c'];
		  }
		  $pesan='Update Jenis BBM:('.$ibbmtype.')-'.$ebbmtypename;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu6')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ibbmtype	= $this->uri->segment(4);
			$this->load->model('bbmtype/mmaster');
			$this->mmaster->delete($ibbmtype);

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		= pg_query($sql);
		  if(pg_num_rows($rs)>0){
			  while($row=pg_fetch_assoc($rs)){
				  $ip_address	  = $row['ip_address'];
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }
		  $query 	= pg_query("SELECT current_timestamp as c");
		  while($row=pg_fetch_assoc($query)){
			  $now	  = $row['c'];
		  }
		  $pesan='Delete Jenis BBM:('.$ibbmtype.')-'.$ebbmtypename;
		  $this->load->model('logger');
		  $this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('master_bbmtype');
			$data['ibbmtype']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('bbmtype/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
