<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu164')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printdkb');
			$data['dkbfrom']='';
			$data['dkbto']	='';
			$this->load->view('printdkbkelompok/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function dkbfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu164')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= strtoupper($this->input->post('cari'));
			$iarea= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/printdkbkelompok/cform/dkbfrom/'.$iarea.'/';
			$query = $this->db->query("	select i_dkb from tm_dkb where upper(i_dkb) like '%$cari%' and i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printdkbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('listdkb');
			$data['iarea']=$iarea;
			$data['isi']=$this->mmaster->bacadkb($cari,$iarea,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printdkbkelompok/vlistdkbfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caridkbfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu164')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari		= strtoupper($this->input->post('cari'));
			$iarea	= $this->input->post('iarea');
			$config['base_url'] = base_url().'index.php/printdkbkelompok/cform/dkbfrom/'.$iarea.'/'.$cari;
			$query = $this->db->query("	select i_dkb from tm_dkb where upper(i_dkb) like '%$cari%' and i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('printdkbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('listdkb');
			$data['iarea']=$iarea;
			$data['isi']=$this->mmaster->bacadkb($cari,$iarea,$config['per_page'],$this->uri->segment(6));
			$this->load->view('printdkbkelompok/vlistdkbfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function dkbto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu164')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= $this->input->post('cari');
			$cari	= strtoupper($cari);
			$iarea= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/printdkbkelompok/cform/dkbto/'.$iarea.'/';
			$query = $this->db->query("	select i_dkb from tm_dkb
																	where upper(i_dkb) like '%$cari%'
																	and i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printdkbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('listdkb');
			$data['iarea']=$iarea;
			$data['isi']=$this->mmaster->bacadkb($cari,$iarea,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printdkbkelompok/vlistdkbto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caridkbto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu164')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= strtoupper($this->input->post('cari'));
			$iarea	= $this->input->post('iarea');
			$config['base_url'] = base_url().'index.php/printdkbkelompok/cform/dkbto/'.$iarea.'/'.$cari;
			$query = $this->db->query("	select i_dkb from tm_dkb
																	where upper(i_dkb) like '%$cari%'
																	and i_area='$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printdkbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('listdkb');
			$data['iarea']=$iarea;
			$data['isi']=$this->mmaster->bacadkb($cari,$iarea,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printdkbkelompok/vlistdkbto', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu164')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	= $this->input->post('iarea');
			$dkbfrom= $this->input->post('dkbfrom');
			$dkbto	= $this->input->post('dkbto');
			$this->load->model('printdkbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['master']=$this->mmaster->bacamaster($area,$dkbfrom,$dkbto);
			$data['area']=$area;
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak DKB Area '.$area.' No:'.$dkbfrom.' s/d '.$dkbto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printdkbkelompok/vformrpt',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu164')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/printdkbkelompok/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('printdkbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printdkbkelompok/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu164')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/printdkbkelompok/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printdkbkelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printdkbkelompok/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
