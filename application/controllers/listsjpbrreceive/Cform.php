<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu310')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listsjpbrreceive');
			$data['dfrom']='';
			$data['dto']='';
			$data['isjpbr']='';
			$this->load->view('listsjpbrreceive/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu310')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$iarea		= $this->input->post('iarea');
			if($iarea=='') $iarea=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listsjpbrreceive/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';

#      $ispg=strtoupper($this->session->userdata("user_id"));
#      $iarea=$this->session->userdata("i_area");
#      $query = $this->db->query(" select a.i_customer, a.i_area, a.e_spg_name, b.e_area_name, c.e_customer_name 
#                                  from tr_spg a, tr_area b, tr_customer c
#                                  where upper(a.i_spg) = '$ispg' and a.i_area='$iarea' and a.i_area=b.i_area 
#                                  and a.i_customer=c.i_customer");
#      if($query->num_rows>0){
#        foreach($query->result() as $xx){
#          $ispg=$ispg;
#          $iarea=$iarea;
#    			$icustomer = $xx->i_customer;
#        }
#      }else{
#        $ispg=$ispg;
#        $iarea=$iarea;
#  			$icustomer = '';
#      }
			$query = $this->db->query(" select a.i_sjpbr from tm_sjpbr a, tr_customer b
																	where (upper(a.i_sjpbr) like '%$cari%') and a.i_area='$iarea' and a.i_customer=b.i_customer and
                                  not a.d_sjpbr_receive isnull and
																	a.d_sjpbr >= to_date('$dfrom','dd-mm-yyyy') AND a.d_sjpbr <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpbrreceive');
			$this->load->model('listsjpbrreceive/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isjpbr']   = '';
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SJPB Retur Receive Area'.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listsjpbrreceive/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu310')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listsjpbrreceive');
			$this->load->view('listsjpbrreceive/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu310')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	 	= strtoupper($this->input->post('cari', FALSE));
			$isjpbr	= $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$dfrom	= $this->uri->segment(6);
			$dto		= $this->uri->segment(7);
			$this->load->model('listsjpbrreceive/mmaster');
			$this->mmaster->delete($isjpbr,$iarea);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus SJPB Retur Receive No:'.$isjpbr.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$config['base_url'] = base_url().'index.php/listsjpbrreceive/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
		  $ispg=strtoupper($this->session->userdata("user_id"));
      $iarea=$this->session->userdata("i_area");
      $query = $this->db->query(" select a.i_customer, a.i_area, a.e_spg_name, b.e_area_name, c.e_customer_name 
                                  from tr_spg a, tr_area b, tr_customer c
                                  where upper(a.i_spg) = '$ispg' and a.i_area='$iarea' and a.i_area=b.i_area 
                                  and a.i_customer=c.i_customer");
      if($query->num_rows>0){
        foreach($query->result() as $xx){
          $ispg=$ispg;
          $iarea=$iarea;
    			$icustomer = $xx->i_customer;
        }
      }else{
        $ispg=$ispg;
        $iarea=$iarea;
  			$icustomer = '';
      }
			$query = $this->db->query(" select a.i_sjpbr from tm_sjpbr a, tr_customer b
																	where a.f_sjpbr_cancel='f' and (upper(a.i_sjpbr) like '%$cari%') and
                                  a.i_customer='$icustomer' and a.i_area='$iarea' and a.i_customer=b.i_customer and
																	a.d_sjpbr >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_sjpbr <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpbrreceive');
			$this->load->model('listsjpbrreceive/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isjpbr']   = '';
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$icustomer,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$this->load->view('listsjpbrreceive/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu310')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listsjpbrreceive/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_sjp a, tr_area b
																	where a.i_area=b.i_area
																	and (upper(a.i_sjp) like '%$cari%')
																	and a.i_area='$iarea' and not a.d_sjp_receive is null and
																	a.d_sjp_receive >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_sjp_receive <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjpbrreceive');
			$this->load->model('listsjpbrreceive/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['isjp']   = '';
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listsjpbrreceive/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu310')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listsjpbrreceive/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listsjpbrreceive/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listsjpbrreceive/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu310')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listsjpbrreceive/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listsjpbrreceive/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listsjpbrreceive/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu310')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjpbreturreceive').' Update';
			if($this->uri->segment(4)!=''){
				$isjpbr= $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$dfrom = $this->uri->segment(6);
				$dto = $this->uri->segment(7);
				$data['isjpbr']= $isjpbr;
				$data['iarea']= $iarea;
        $data['dfrom']=$dfrom;
        $data['dto']  =$dto;
  			$data['pst']	= $this->session->userdata('i_area');
				$query 	= $this->db->query("select i_sjpbr from tm_sjpbr_item where i_sjpbr = '$isjpbr' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('listsjpbrreceive/mmaster');
				$data['isi']=$this->mmaster->baca($isjpbr,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isjpbr,$iarea);
		 		$this->load->view('listsjpbrreceive/vmainform',$data);
			}else{
				$this->load->view('listsjpbrreceive/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu310')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isjpbr      = $this->input->post('isj', TRUE);
			$iarea  		= $this->input->post('iarea', TRUE);
			$dsjreceive = $this->input->post('dreceive', TRUE);
			if($dsjreceive!=''){
				$tmp=explode("-",$dsjreceive);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsjreceive=$th."-".$bl."-".$hr;
				$thbl	  = substr($th,2,2).$bl;
				$tmpsj	= explode("-",$isjpbr);
				$firstsj= $tmpsj[0];
				$lastsj	= $tmpsj[2];
				$newsj	= $firstsj."-".$thbl."-".$lastsj;				
			}
      $dsjpbr = $this->input->post('dsj', TRUE);
      if($dsjpbr!=''){
				$tmp=explode("-",$dsjpbr);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsjpbr=$th."-".$bl."-".$hr;
      }
			$icustomer  = $this->input->post('icustomer', TRUE);
			$ispg       = $this->input->post('ispg', TRUE);
			$vsjpbr     = $this->input->post('vsjpbr', TRUE);
			$vsjpbr     = str_replace(',','',$vsjpbr);
			$vsjpbrrec  = $this->input->post('vsjpbrrec', TRUE);
			$vsjpbrrec  = str_replace(',','',$vsjpbrrec);

      $istore           = $iarea;
      $istorelocation   = 'PB';
      $istorelocationbin= '00';

			$jml	    = $this->input->post('jml', TRUE);
			$gaono=true;
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$gaono=false;
				}
				if(!$gaono) break;
			}
			if( (!$gaono)&&($dsjreceive!='') ){
				$this->db->trans_begin();
				$this->load->model('listsjpbrreceive/mmaster');
				$this->mmaster->updatesjheader($isjpbr,$iarea,$dsjreceive,$vsjpbr,$vsjpbrrec);
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
						$iproduct		= $this->input->post('iproduct'.$i, TRUE);
						$iproductgrade	= 'A';
						$iproductmotif	= $this->input->post('motif'.$i, TRUE);
						$ndeliver		= $this->input->post('nretur'.$i, TRUE);
						$ndeliver		= str_replace(',','',$ndeliver);
						$nreceive		= $this->input->post('nreceive'.$i, TRUE);
						$nreceive		= str_replace(',','',$nreceive);
						$ntmp		    = $this->input->post('ntmp'.$i, TRUE);
						$ntmp   		= str_replace(',','',$ntmp);
						$this->mmaster->deletesjdetail( $isjpbr, $iarea, $iproduct, $iproductgrade, $iproductmotif, $ndeliver);
						$th=substr($dsjreceive,0,4);
						$bl=substr($dsjreceive,5,2);
						$emutasiperiode=$th.$bl;
            $thsj=substr($dsjpbr,0,4);
						$blsj=substr($dsjpbr,5,2);
						$emutasiperiodesj=$thsj.$blsj;
						$tra=$this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isjpbr);
            if( ($ntmp!='') && ($ntmp!=0) ){
					    $this->mmaster->updatemutasi04($icustomer,$iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ntmp,$emutasiperiode,$emutasiperiodesj);
					    $this->mmaster->updateic04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$ntmp);
            }
						if($cek=='on'){
						  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice		= $this->input->post('vunitprice'.$i, TRUE);
						  $vunitprice		= str_replace(',','',$vunitprice);
						  $this->mmaster->insertsjpbdetail( $iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                                    			      $vunitprice,$isjpbr,$iarea,$i,$dsjpbr,$nreceive);
						  if($ndeliver>0){

						    $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
						    if(isset($trans)){
							    foreach($trans as $itrans)
							    {
							      $q_aw =$itrans->n_quantity_awal;
							      $q_ak =$itrans->n_quantity_akhir;
							      $q_in =$itrans->n_quantity_in;
							      $q_out=$itrans->n_quantity_out;
							      break;
							    }
						    }else{

							    $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$icustomer,$istore,$istorelocation,$istorelocationbin);
							    if(isset($trans)){
							      foreach($trans as $itrans)
							      {
								      $q_aw =$itrans->n_quantity_stock;
								      $q_ak =$itrans->n_quantity_stock;
								      $q_in =0;
								      $q_out=0;
								      break;
							      }
							    }else{
							      $q_aw=0;
							      $q_ak=0;
							      $q_in=0;
							      $q_out=0;
							    }
						    }

						    $this->mmaster->inserttrans1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isjpbr,$q_in,$q_out,$nreceive,$q_aw,$q_ak);

						    $ada=$this->mmaster->cekmutasi2($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode);
						    if($ada=='ada')
						    {
							    $this->mmaster->updatemutasi1($icustomer,$iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nreceive,$emutasiperiode,$emutasiperiodesj,$iarea);
						    }else{
							    $this->mmaster->insertmutasi1($icustomer,$iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nreceive,$emutasiperiode,$emutasiperiodesj,$iarea);
						    }
						    if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
						    {
							    $this->mmaster->updateic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nreceive,$q_ak);
						    }else{
							    $this->mmaster->insertic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nreceive);
						    }
  #							$this->mmaster->updatespmbitem($ispmb,$iproduct,$iproductgrade,$iproductmotif,$nreceive,$iarea);
						  }
						}else{
              $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
						  $vunitprice		= str_replace(',','',$vunitprice);
						  $eremark  		= $this->input->post('eremark'.$i, TRUE);
						  if($eremark=='')$eremark=null;
						  $this->mmaster->insertsjpbdetail( $iproduct,$iproductgrade,$iproductmotif,$eproductname,$ndeliver,
                                    			      $vunitprice,$isjpb,$iarea,$i,$dsjpb,$nreceive);
            }
					}
#					$sjnew=0;
				if ( ($this->db->trans_status() === FALSE) )
				{
					$this->db->trans_rollback();
				}else{
#					$this->db->trans_rollback();
					$this->db->trans_commit();

			    $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		= pg_query($sql);
			    if(pg_num_rows($rs)>0){
				    while($row=pg_fetch_assoc($rs)){
					    $ip_address	  = $row['ip_address'];
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }
			    $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	        	$now	  = $row['c'];
			    }
			    $pesan='Update SJPB Retur Receive No:'.$isjpbr.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );  

					$data['sukses']			= true;
					$data['inomor']			= $newsj;
					$this->load->view('nomor',$data);
				}
			}
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu310')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari = strtoupper($this->input->post('cari', FALSE));
      $baris = strtoupper($this->input->post('baris', FALSE));
 			if($baris=='') $baris=$this->uri->segment(4);
      if($this->uri->segment(5)!='x01'){
        if($cari=='') $cari=$this->uri->segment(5);
        $config['base_url'] = base_url().'index.php/listlistsjpbrreceive/cform/product/'.$baris.'/'.$cari.'/';
      }else{
        $config['base_url'] = base_url().'index.php/listlistsjpbrreceive/cform/product/'.$baris.'/x01/';
      }
			$query = $this->db->query(" select a.i_product, a.e_product_name, b.v_product_retail, c.i_product_motif, c.e_product_motifname 
                                  from tr_product a, tr_product_price b, tr_product_motif c
                                  where a.i_product=b.i_product and b.i_price_group='00'
                                  and (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
                                  and a.i_product=c.i_product",false);# and a.i_product_status<>'4'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
 			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('listlistsjpbrreceive/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$this->uri->segment(4);
 			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('listlistsjpbrreceive/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
