<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
    require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu328')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('transferaccplafon/mmaster');
			$data['page_title'] = $this->lang->line('transferaccplafon');
			$data['isi']= directory_map('./excel/00/');
			$data['file']='';
			$data['status']='';
			$this->load->view('transferaccplafon/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu328')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$file= $this->input->post('namafile', TRUE);
			$data['$namafile'] = $file;
      $data['periodeawal'] ='';
      $data['periodeakhir'] ='';
			$store	= $this->session->userdata('store');
			$data['file']='excel/00/'.$file;
			$data['page_title'] = $this->lang->line('transferaccplafon');
			$this->load->view('transferaccplafon/vfile', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu328')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		      $jml          = $this->input->post('jml', TRUE);
				  $periodeawal  = $this->input->post('periodeawal',TRUE);
				  $periodeakhir = $this->input->post('periodeakhir',TRUE);
	  			$namafile= $this->input->post('namafile', TRUE);
			    if($jml=='') $jml	= $this->uri->segment(4);
			    if($periodeawal=='') $periodeawal	= $this->uri->segment(5);
			    if($periodeakhir=='') $periodeakhir	= $this->uri->segment(6);
			    if($namafile=='') $namafile	= $this->uri->segment(7);
	  			$file='excel/00/'.$namafile;
			    $this->load->model('transferaccplafon/mmaster');
			    $this->db->trans_begin();

          $handle = fopen($file, 'r');
          $x=1;
          while (($data = fgetcsv($handle, 1024, ',')) !== FALSE) {
            $num = count($data);
            if($x>1){
              for ($c=0; $c < $num; $c++) {
	              if($c==0)
		              $iarea=$data[$c];
	              if($c==1)
		              $icust=$data[$c];
	              if($c==3)
		              $iperiodeawal=$data[$c];
	              if($c==4)
		              $iperiodeakhir=$data[$c];
	              if($c==15)
		              $plafonacc=$data[$c];
              }
              $this->mmaster->updateplafond($icust,$iperiodeawal,$iperiodeakhir,$plafonacc);
              $this->mmaster->updategroupbayar($icust,$plafonacc);
      		    $query = $this->db->query("select * from tr_customer_groupbayar where i_customer_groupbayar = '$icust'");
              
              if ($query->num_rows() > 0){
                $raw	= $query->row();
                $icust_bayar    = $raw->i_customer_groupbayar;
                $icust          = $raw->i_customer;
                $this->db->query("update tr_customer_groupar set v_flapond ='$plafonacc' where i_customer='$icust'");
              }
            }
            $x++;
          }
			if ( ($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
				$this->load->view('transferaccplafon/vformgagal',$data);
			}else{
		        $this->db->trans_commit();
#		        $this->db->trans_rollback();
            $data['status']='berhasil';
				    $data['sukses']=true;
				    $data['inomor']=$periodeawal.'-'.$periodeakhir;
        		$data['page_title'] = $this->lang->line('transferaccplafon');
				    $this->load->view('transferaccplafon/vmainform',$data);
				    #$this->load->view('transferaccplafon/vformsukses',$data);
			}			
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
