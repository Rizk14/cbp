<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu384')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printoprekap/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select distinct(a.i_op) from tm_op a, tr_supplier b, tm_spb c, tm_spmb_item d
                                  where a.i_supplier=b.i_supplier
                                  and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
                                  or upper(a.i_op) like '%$cari%')
                                  and d.i_op=a.i_op and d.i_spmb=c.i_spmb and not c.i_spmb isnull",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printoprekap');
			$this->load->model('printoprekap/mmaster');
			$data['iop']='';
			$data['cari']=$cari;
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('printoprekap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu384')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iop = $this->uri->segment(4);
			$this->load->model('printoprekap/mmaster');
			$data['iop']=$iop;
			$data['page_title'] = $this->lang->line('printoprekap');
			$data['isi']=$this->mmaster->baca($iop);
			$data['detail']=$this->mmaster->bacadetail($iop);
			$data['spb']=$this->mmaster->bacadetailspb($iop);
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak OP No:'.$iop;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printoprekap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetakspb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu384')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iop = $this->uri->segment(4);
			$this->load->model('printoprekap/mmaster');
			$data['iop']=$iop;
			$data['page_title'] = $this->lang->line('printoprekap');
			$data['spb']=$this->mmaster->bacadetailspb($iop);
			$this->load->view('printoprekap/vformrptspb', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu384')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printoprekap');
			$this->load->view('printoprekap/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu384')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printoprekap/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select distinct(a.i_op) from tm_op a, tr_supplier b, tm_spb c, tm_spmb_item d
                                  where a.i_supplier=b.i_supplier
                                  and (upper(a.i_supplier) like '%$cari%' or upper(b.e_supplier_name) like '%$cari%'
                                  or upper(a.i_op) like '%$cari%')
                                  and d.i_op=a.i_op and d.i_spmb=c.i_spmb and not c.i_spmb isnull",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('printoprekap/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('printoprekap');
			$data['cari']=$cari;
			$data['iop']='';
			$data['detail']='';
	 		$this->load->view('printoprekap/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
