<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu313')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbayarkn');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listbayarkn/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu313')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listbayarkn/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_kn a, tr_customer b
																	where a.i_customer=b.i_customer 
                                  and a.f_kn_cancel='f'
																	and not a.i_kn isnull
																	and (upper(a.i_kn) like '%$cari%'
																		or upper(a.i_customer) like '%$cari%' 
																		or upper(b.e_customer_name) like '%$cari%')
																	and a.i_area='$iarea' and
																	a.d_kn >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_kn <= to_date('$dto','dd-mm-yyyy') order by a.i_kn",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('listbayarkn/mmaster');
			$data['page_title'] = $this->lang->line('listbayarkn');
			$data['cari']		= $cari;
			$data['keyword']	= '';	
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Bayar KN Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listbayarkn/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu313')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listbayarkn');
			$this->load->view('listbayarkn/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu313')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dto		= $this->uri->segment(8);
			$dfrom	= $this->uri->segment(7);
			$iarea	= $this->uri->segment(6);
			$ispb		= $this->uri->segment(5);
			$inota	= $this->uri->segment(4);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$this->load->model('listbayarkn/mmaster');
			$this->mmaster->delete($inota,$ispb,$iarea);
			$config['base_url'] = base_url().'index.php/listbayarkn/cform/view/'.$inota.'/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_kn a, tr_customer b
																	where a.i_customer=b.i_customer 
                                  and a.f_kn_cancel='f'
																	and not a.i_kn isnull
																	and (upper(a.i_kn) like '%$cari%'
																		or upper(a.i_customer) like '%$cari%' 
																		or upper(b.e_customer_name) like '%$cari%')
																	and a.i_area='$iarea' and
																	a.d_kn >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_kn <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listbayarkn');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['keyword']	= '';	
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listbayarkn/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu313')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($cari=='') $cari	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/listbayarkn/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_kn a, tr_customer b
																	where a.i_customer=b.i_customer 
                                  and a.f_kn_cancel='f'
																	and not a.i_kn isnull
																	and (upper(a.i_kn) like '%$cari%'
																		or upper(a.i_customer) like '%$cari%' 
																		or upper(b.e_customer_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_kn >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_kn <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('listbayarkn/mmaster');
			$data['page_title'] = $this->lang->line('listbayarkn');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listbayarkn/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu313')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$keyword = strtoupper($this->uri->segment(7));	
			$config['base_url'] = base_url().'index.php/listbayarkn/cform/cariperpages/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$keyword.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_kn a, tr_customer b
																	where a.i_customer=b.i_customer 
                                  and a.f_kn_cancel='f'
																	and not a.i_kn isnull
																	and (upper(a.i_kn) like '%$keyword%'
																		or upper(a.i_customer) like '%$keyword%' 
																		or upper(b.e_customer_name) like '%$keyword%')
										and a.i_area='$iarea' and
										a.d_kn >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_kn <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('listbayarkn/mmaster');
			$data['page_title'] = $this->lang->line('listbayarkn');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$keyword);
			$this->load->view('listbayarkn/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu313')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listbayarkn/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listbayarkn/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listbayarkn/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu313')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listbayarkn/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listbayarkn/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listbayarkn/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function bayar()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu313')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $ikn=$this->uri->segment(4);
      $iarea=$this->uri->segment(5);
			$this->load->model('listbayarkn/mmaster');
			$data['page_title'] = $this->lang->line('listbayarkn');
			$data['isi']=$this->mmaster->bacabayar($ikn,$iarea);
			$this->load->view('listbayarkn/vlistbayar', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
