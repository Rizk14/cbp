<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu341')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('pindahper');
			$data['dfrom']='';
			$data['dto']='';				
			$this->load->view('spbpindahperiode/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu341')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspbpindah');
			$this->load->view('spbpindahperiode/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu341')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
			if ($cari == '') {
				$cari=$this->uri->segment(7);
			}
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea'); 
			if($dfrom=='') $dfrom = $this->uri->segment(4);
			if($dto=='') $dto = $this->uri->segment(5);
			if($iarea=='') $iarea = $this->uri->segment(6);
			
			$config['base_url'] = base_url().'index.php/spbpindahperiode/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/index/';
			
						
			$sql= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
					where a.i_customer=b.i_customer
					and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
					or upper(a.i_spb) like '%$cari%') 
					and a.i_area='$iarea' and
						(a.d_spb >= to_date('$dfrom','dd-mm-yyyy') and
						a.d_spb <= to_date('$dto','dd-mm-yyyy')) order by a.i_spb desc ";
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			
			$this->load->model('spbpindahperiode/mmaster');
			$data['page_title'] = $this->lang->line('listspbpindah');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('spbpindahperiode/vmainform',$data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function paging() 
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu341')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($this->uri->segment(5)!=''){
				if($this->uri->segment(4)!='sikasep'){
					$cari=$this->uri->segment(4);
				}else{
					$cari='';
				}
			}elseif($this->uri->segment(4)!='sikasep'){
				$cari=$this->uri->segment(4);
			}else{
				$cari='';
			}
			if($cari=='')
				$config['base_url'] = base_url().'index.php/spbpindahperiode/cform/paging/sikasep/';
			else
				$config['base_url'] = base_url().'index.php/spbpindahperiode/cform/paging/'.$cari.'/';
			if($this->uri->segment(4)=='sikasep')
				$cari='';
			$cari	= $this->input->post('cari', TRUE);
			$cari  	= strtoupper($cari);
			if($this->session->userdata('level')=='0'){
				$sql= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
						where a.i_customer=b.i_customer
						and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						or upper(a.i_spb) like '%$cari%') order by a.i_spb desc ";
			}else{
				$sql= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
						where a.i_customer=b.i_customer
						and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						or upper(a.i_spb) like '%$cari%')
						and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
						or a.i_area='$area5') order by a.i_spb desc ";
			}
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('listspbpindah');
			$this->load->model('spbpindahperiode/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('spbpindahperiode/vform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu341')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spbpindahperiode/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
      else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('spbpindahperiode/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('spbpindahperiode/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu341')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/spbpindahperiode/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spbpindahperiode/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('spbpindahperiode/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu341')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			$is_cari= $this->input->post('is_cari'); 
				
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea=$this->uri->segment(6);
				
			if ($is_cari == '')
				$is_cari= $this->uri->segment(8);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(7);
			
			if ($is_cari=="1") { 
				$config['base_url'] = base_url().'index.php/spbpindahperiode/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/'.$is_cari.'/index/';
			}
			else { 
				$config['base_url'] = base_url().'index.php/spbpindahperiode/cform/view2/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			} 
			
			if ($is_cari != "1") {
				$sql= " select a.i_spb
                from tm_spb a 
								left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
								left join tr_customer b on(a.i_customer=b.i_customer)
								left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
								, tr_area c
                where 
                a.i_area=c.i_area and					
                a.i_area='$iarea' and
                (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                a.d_spb <= to_date('$dto','dd-mm-yyyy'))";
			}
			else {
				$sql= " select a.i_spb
                from tm_spb a 
								left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f') 
								left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
								left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
								, tr_area c
                where 
                a.i_area=c.i_area and					
                a.i_area='$iarea' and
                (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                a.d_spb <= to_date('$dto','dd-mm-yyyy')) AND
                (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                or upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')";
			}
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			if ($is_cari=="1")
				$config['cur_page'] = $this->uri->segment(10);
			else
				$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			
			$this->load->model('spbpindahperiode/mmaster');
			$data['page_title'] = $this->lang->line('listspbpindah');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;

			if ($is_cari=="1")
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(10),$cari);
			else
				$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SPB Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('spbpindahperiode/vmainform',$data);			

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu341')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('spb')." update";
			if(
				($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')
			  )
			{
				$ispb = $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$dfrom	= $this->uri->segment(6);
				$dto	= $this->uri->segment(7);
				$iarea_awal	= $this->uri->segment(8);
				$ipricegroup	= $this->uri->segment(9);
				$query = $this->db->query("select * from tm_spb_item where i_spb = '$ispb' and i_area='$iarea'");
				$data['jmlitem']= $query->num_rows(); 				
				$data['ispb'] 	= $ispb;
				$data['departement']=$this->session->userdata('departement');
				$this->load->model('spbpindahperiode/mmaster');
				$data['isi']	= $this->mmaster->baca($ispb,$iarea);
				$data['detail']	= $this->mmaster->bacadetail($ispb,$iarea,$ipricegroup);

				$qnilaispb	= $this->mmaster->bacadetailnilaispb($ispb,$iarea,$ipricegroup);
				if($qnilaispb->num_rows()>0){
					$row_nilaispb	= $qnilaispb->row();
					$data['nilaispb']	= $row_nilaispb->nilaispb;
				}else{
					$data['nilaispb']	= 0;
				}
				$qnilaiorderspb	= $this->mmaster->bacadetailnilaiorderspb($ispb,$iarea,$ipricegroup);
				if($qnilaiorderspb->num_rows()>0){
					$row_nilaiorderspb	= $qnilaiorderspb->row();
					$data['nilaiorderspb']	= $row_nilaiorderspb->nilaiorderspb;
				}else{
					$data['nilaiorderspb']	= 0;
				}
				$qeket	= $this->db->query(" SELECT e_remark1 as keterangan from tm_spb where i_spb ='$ispb' and i_area='$iarea' ");
				if($qeket->num_rows()>0){
					$row_eket	= $qeket->row();
					$data['keterangan']	= $row_eket->keterangan;
				}				
				
				//
				$data['dfrom'] = $dfrom;
				$data['dto'] = $dto;
				$data['iarea_awal'] = $iarea_awal;
				
		 		$this->load->view('spbpindahperiode/vformupdate',$data);
			}else{
				$this->load->view('spbpindahperiode/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu47')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb 	= $this->input->post('ispb', TRUE);
			$dspb 	= $this->input->post('dspb', TRUE);
			if($dspb!=''){
				$tmp=explode("-",$dspb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspb=$th."-".$bl."-".$hr;
			}
			$iarea		        = $this->input->post('iarea', TRUE);
			if(($ispb!=''))
			{
				$benar="false";
				$this->db->trans_begin();
				$this->load->model('spbbaby/mmaster');
				$this->mmaster->updateheader($ispb, $iarea, $dspb, $icustomer, $ispbpo, $nspbtoplength, $isalesman, 
						 $ipricegroup, $dspbreceive, $fspbop, $ecustomerpkpnpwp, $fspbpkp, 
						 $fspbplusppn, $fspbplusdiscount, $fspbstockdaerah, $fspbprogram, $fspbvalid,
						 $fspbsiapnota, $fspbcancel, $nspbdiscount1, 
						 $nspbdiscount2, $nspbdiscount3, $vspbdiscount1, $vspbdiscount2,
						 $vspbdiscount3, $vspbdiscounttotal, $vspb, $fspbconsigment, $ispbold, $eremarkx);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			= $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade= 'A';
				  $iproductmotif= $this->input->post('motif'.$i, TRUE);
				  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		= $this->input->post('vproductretail'.$i, TRUE);
				  $vunitprice		= str_replace(',','',$vunitprice);
				  $norder			  = $this->input->post('norder'.$i, TRUE);
				  $ndeliver			= $this->input->post('ndeliver'.$i, TRUE);
				  $ndeliverx		= $this->input->post('ndeliverx'.$i, TRUE);
          if($ndeliver=='')$ndeliver=null;
          if($ndeliverx=='')$ndeliverx=null;
				  
				  $eremark			= $this->input->post('eremark'.$i, TRUE);
				  $data['iproduct']		  = $iproduct;
				  $data['iproductgrade']= $iproductgrade;
				  $data['iproductmotif']= $iproductmotif;
				  $data['eproductname']	= $eproductname;
				  $data['vunitprice']		= $vunitprice;
				  $data['norder']		    = $norder;
				  $data['ndeliver']		  = $ndeliver;
				  $this->mmaster->deletedetail($ispb, $iarea, $iproduct, $iproductgrade, $iproductmotif);
				  if($norder>0){
				    $this->mmaster->insertdetail( $ispb,$iarea,$iproduct,$iproductgrade,$eproductname,$norder,$ndeliver,
												$vunitprice,$iproductmotif,$eremark,$i);
				  }
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();

          $sess=$this->session->userdata('session_id');
			    $id=$this->session->userdata('user_id');
			    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			    $rs		=  $this->db->query($sql);
			    if($rs->num_rows>0){
				    foreach($rs->result() as $tes){
					    $ip_address	  = $tes->ip_address;
					    break;
				    }
			    }else{
				    $ip_address='kosong';
			    }

			    $data['user']	= $this->session->userdata('user_id');
    #			$data['host']	= $this->session->userdata('printerhost');
			    $data['host']	= $ip_address;
			    $data['uri']	= $this->session->userdata('printeruri');
			    $query 	= pg_query("SELECT current_timestamp as c");
			    while($row=pg_fetch_assoc($query)){
				    $now	  = $row['c'];
			    }
			    $pesan='Update SPB Pindah Periode No:'.$ispb.' Area:'.$iarea;
			    $this->load->model('logger');
			    $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ispb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
