<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu82')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['dfrom']	= '';
			$data['dto']	= '';
			$data['inota']	= '';
			$data['ittb']	= '';
			$data['page_title'] = $this->lang->line('listnotakoreksi');
			$this->load->view('notakoreksi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu81')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			
			$config['base_url'] = base_url().'index.php/notakoreksi/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';

			$query = $this->db->query(" select a.*, b.e_customer_name from tm_ttbtolak a, tr_customer b
                                  where a.i_customer=b.i_customer and 
					                        a.i_nota not in(select i_nota from tm_nota where f_nota_koreksi='f') and 
					                        (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                                 	or upper(a.i_ttb) like '%$cari%') and a.i_area='$iarea' and
					                        (a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')) ",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('notakoreksi');
			$this->load->model('notakoreksi/mmaster');
			$data['ittb']='';
			$data['inota']='';
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;

			$data['isi']=$this->mmaster->bacasemua($dfrom,$dto,$iarea,$cari,$config['per_page'],$this->uri->segment(8));
			$this->load->view('notakoreksi/vmainform', $data);
		}elseif($this->session->userdata('logged_in')){
			$this->load->view('errorauthority');
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu81')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listspb');
			$this->load->view('notakoreksi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu81')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispb	= $this->input->post('ispbdelete', TRUE);
			$this->load->model('notakoreksi/mmaster');
			$this->mmaster->delete($ispb);
			$data['page_title'] = $this->lang->line('listspb');
			$data['ispb']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('notakoreksi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu81')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/notakoreksi/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);

			if($iarea=='') $iarea	= $this->uri->segment(6);

			$query = $this->db->query(" select a.*, b.e_customer_name from tm_ttbtolak a, tr_customer b
					    		where a.i_customer=b.i_customer and
							a.i_nota not in(select i_nota from tm_notakoreksi) and 
							(upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%') and
							a.i_area='$iarea' and
							(a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and
							a.d_nota <= to_date('$dto','dd-mm-yyyy')) ",false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('notakoreksi');
			$this->load->model('notakoreksi/mmaster');
			$data['ittb']='';
			$data['inota']='';
			$data['isi']=$this->mmaster->cari($dfrom,$dto,$iarea,$cari,$config['per_page'],$this->uri->segment(8));
			$this->load->view('notakoreksi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function approve()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu81')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('notakoreksi');
			$ittb 		= $this->uri->segment(4);
			$iarea 		= $this->uri->segment(5);
			$nttbyear 	= $this->uri->segment(6);
			$inota 		= $this->uri->segment(7);
			if( ($inota!='')  && ($ittb!='') && ($iarea!='') && ($nttbyear!='') ){
				$query = $this->db->query("select * from tm_nota_item where i_nota = '$inota' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['inota']='';
				$data['ittb']=$ittb;
				$this->load->model('notakoreksi/mmaster');
				$data['isi']=$this->mmaster->bacanota($inota,$iarea);
				$data['detail']=$this->mmaster->bacadetailnota($inota,$iarea);
		 		$this->load->view('notakoreksi/vmainform',$data);
			}else{
				$this->load->view('notakoreksi/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu81')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('notakoreksi/mmaster');
			$inota 	= $this->input->post('inota', TRUE);
			$nnotadiscount1		= $this->input->post('ncustomerdiscount1',TRUE);
			$nnotadiscount1		= str_replace(',','',$nnotadiscount1);
			$nnotadiscount2		= $this->input->post('ncustomerdiscount2',TRUE);
			$nnotadiscount2		= str_replace(',','',$nnotadiscount2);
			$nnotadiscount3		= $this->input->post('ncustomerdiscount3',TRUE);
			$nnotadiscount3		= str_replace(',','',$nnotadiscount3);
			$vnotadiscount1		= $this->input->post('vcustomerdiscount1',TRUE);
			$vnotadiscount1		= str_replace(',','',$vnotadiscount1);
			$vnotadiscount2		= $this->input->post('vcustomerdiscount2',TRUE);
			$vnotadiscount2		= str_replace(',','',$vnotadiscount2);
			$vnotadiscount3		= $this->input->post('vcustomerdiscount3',TRUE);
			$vnotadiscount3		= str_replace(',','',$vnotadiscount3);
			$vnotadiscounttotal	= $this->input->post('vnotadiscounttotal',TRUE);
			$vnotadiscounttotal	= str_replace(',','',$vnotadiscounttotal);
			$vnotagross			  = $this->input->post('vnotagross',TRUE);
			$vnotagross			  = str_replace(',','',$vnotagross);
			$vnotanetto			  = $this->input->post('vnotanetto',TRUE);
			$vnotanetto			  = str_replace(',','',$vnotanetto);
			$jml				      = $this->input->post('jml', TRUE);
			$fnotaplusppn		  = $this->input->post('fnotaplusppn',TRUE);
			$fnotaplusdiscount= $this->input->post('fnotaplusdiscount',TRUE);
			$nnotatoplength		= $this->input->post('nnotatoplength',TRUE);
			$ecustomerpkpnpwp	= $this->input->post('ecustomerpkpnpwp',TRUE);
			$icustomer			  = $this->input->post('icustomer',TRUE);
			$ispb				      = $this->input->post('ispb',TRUE);
			$iarea			      = $this->input->post('iarea',TRUE);
			if($ecustomerpkpnpwp=='')
				$fnotapkp='f';
			else
				$fnotapkp='t';
			if(($inota!='')){
				$this->db->trans_begin();
				$this->mmaster->insertheader($inota);
				$this->mmaster->insertdetail($inota);
				$this->mmaster->updatenotakoreksi(	$inota,$nnotadiscount1,$nnotadiscount2,$nnotadiscount3,$icustomer,
													$vnotadiscount1,$vnotadiscount2,$vnotadiscount3,$vnotadiscounttotal,
													$vnotagross,$vnotanetto,$fnotaplusppn,$fnotaplusdiscount,$nnotatoplength);
				$this->mmaster->updatespb( 	$icustomer,$ecustomerpkpnpwp,$fnotapkp,$fnotaplusppn,$fnotaplusdiscount,$nnotatoplength,
											$nnotadiscount1,$nnotadiscount2,$nnotadiscount3,$vnotadiscount1,$vnotadiscount2,$vnotadiscount3,
											$vnotadiscounttotal,$vnotadiscounttotal,$vnotanetto,$vnotanetto,$ispb,$iarea);
				for($i=1;$i<=$jml;$i++){
				  $iproduct					=$this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade			='A';
				  $iproductmotif			=$this->input->post('motif'.$i, TRUE);
				  $eproductname				=$this->input->post('eproductname'.$i, TRUE);
				  $vunitprice				=$this->input->post('vproductretail'.$i, TRUE);
				  $vunitprice				=str_replace(',','',$vunitprice);
				  $ndeliver					=$this->input->post('ndeliver'.$i, TRUE);
				  $this->mmaster->updatedetailnotakoreksi($inota,$iproduct,$iproductgrade,$eproductname,$ndeliver,
												$vunitprice,$iproductmotif);
				  $this->mmaster->updatespbdetail($ispb,$iproduct,$iproductgrade,$ndeliver,$iproductmotif,$iarea);
				}
			}
			if ($this->db->trans_status() === FALSE)
			{
		    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
				$sess=$this->session->userdata('session_id');
				$id=$this->session->userdata('user_id');
				$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
				$rs		= pg_query($sql);
				if(pg_num_rows($rs)>0){
					while($row=pg_fetch_assoc($rs)){
						$ip_address	  = $row['ip_address'];
						break;
					}
				}else{
					$ip_address='kosong';
				}
				$query 	= pg_query("SELECT current_timestamp as c");
				while($row=pg_fetch_assoc($query)){
					$now	  = $row['c'];
				}
				$pesan='Input Koreksi Nota No:'.$inota;
				$this->load->model('logger');
				$this->logger->write($id, $ip_address, $now , $pesan );
				$data['sukses']			= true;
				$data['inomor']			= $inota;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu81')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('notakoreksi')." update";
			if(($this->uri->segment(4)!='') && ($this->uri->segment(5)!='')){
				$inota= $this->uri->segment(4);
				$ispb = $this->uri->segment(5);
				$iarea= $this->uri->segment(6);
				$dto 	= $this->uri->segment(8);
				$dfrom= $this->uri->segment(7);
				$query = $this->db->query("select * from tm_nota_item where i_nota = '$inota' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispb'] 	= $ispb;
				$data['ittb']	= '';
				$data['inota']	= $inota;
				$data['iarea']	= $iarea;
				$data['dfrom']	= $dfrom;
				$data['dto']		= $dto;
				$this->load->model('notakoreksi/mmaster');
				$data['isi']=$this->mmaster->bacanotakoreksi($inota,$ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetailnotakoreksi($inota,$iarea);
		 		$this->load->view('notakoreksi/vmainform',$data);
			}else{
				$this->load->view('notakoreksi/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function updatenota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu81')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('notakoreksi/mmaster');
			$inota 	= $this->input->post('inota', TRUE);
			$iarea 	= $this->input->post('iarea', TRUE);
			$nnotadiscount1		= $this->input->post('ncustomerdiscount1',TRUE);
			$nnotadiscount1		= str_replace(',','',$nnotadiscount1);
			$nnotadiscount2		= $this->input->post('ncustomerdiscount2',TRUE);
			$nnotadiscount2		= str_replace(',','',$nnotadiscount2);
			$nnotadiscount3		= $this->input->post('ncustomerdiscount3',TRUE);
			$nnotadiscount3		= str_replace(',','',$nnotadiscount3);
			$vnotadiscount1		= $this->input->post('vcustomerdiscount1',TRUE);
			$vnotadiscount1		= str_replace(',','',$vnotadiscount1);
			$vnotadiscount2		= $this->input->post('vcustomerdiscount2',TRUE);
			$vnotadiscount2		= str_replace(',','',$vnotadiscount2);
			$vnotadiscount3		= $this->input->post('vcustomerdiscount3',TRUE);
			$vnotadiscount3		= str_replace(',','',$vnotadiscount3);
			$vnotadiscounttotal	= $this->input->post('vnotadiscounttotal',TRUE);
			$vnotadiscounttotal	= str_replace(',','',$vnotadiscounttotal);
			$vnotagross			= $this->input->post('vnotagross',TRUE);
			$vnotagross			= str_replace(',','',$vnotagross);
			$vnotanetto			= $this->input->post('vnotanetto',TRUE);
			$vnotanetto			= str_replace(',','',$vnotanetto);
			$jml				= $this->input->post('jml', TRUE);
			$fnotaplusppn		= $this->input->post('fnotaplusppn',TRUE);
			$fnotaplusdiscount	= $this->input->post('fnotaplusdiscount',TRUE);
			$nnotatoplength		= $this->input->post('nnotatoplength',TRUE);
			$ecustomerpkpnpwp	= $this->input->post('ecustomerpkpnpwp',TRUE);
			$icustomer			= $this->input->post('icustomer',TRUE);
			$ispb				= $this->input->post('ispb',TRUE);
			if($ecustomerpkpnpwp=='')
				$fnotapkp='f';
			else
				$fnotapkp='t';
			if(($inota!='')){
				$this->db->trans_begin();
				$this->mmaster->updatenotakoreksi(	$inota,$nnotadiscount1,$nnotadiscount2,$nnotadiscount3,$icustomer,
													$vnotadiscount1,$vnotadiscount2,$vnotadiscount3,$vnotadiscounttotal,
													$vnotagross,$vnotanetto,$fnotaplusppn,$fnotaplusdiscount,$nnotatoplength);
				$this->mmaster->updatespb( 	$vnotadiscounttotal,$vnotanetto,$ispb,$iarea);
				for($i=1;$i<=$jml;$i++){
				  $iproduct					=$this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade			='A';
				  $iproductmotif			=$this->input->post('motif'.$i, TRUE);
				  $eproductname				=$this->input->post('eproductname'.$i, TRUE);
				  $vunitprice				=$this->input->post('vproductretail'.$i, TRUE);
				  $vunitprice				=str_replace(',','',$vunitprice);
				  $ndeliver					=$this->input->post('ndeliver'.$i, TRUE);
				  $this->mmaster->updatedetailnotakoreksi($inota,$iproduct,$iproductgrade,$eproductname,$ndeliver,
												$vunitprice,$iproductmotif);
				  $this->mmaster->updatespbdetail($ispb,$iarea,$iproduct,$iproductgrade,$ndeliver,$iproductmotif);
				}
			}
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
				$this->db->trans_commit();
				$data['sukses']			= true;
				$data['inomor']			= $inota;
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu81')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/notakoreksi/cform/customer/'.$iarea.'/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_customer where i_area = '$iarea'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('notakoreksi/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('notakoreksi/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu81')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/notakoreksi/cform/customer/'.$iarea.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select * from tr_customer
						   where i_area='$iarea' 
							 and (upper(i_customer) like '%$cari%' 
						      	  or upper(e_customer_name) like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('notakoreksi/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari, $iarea,$config['per_page'],$this->uri->segment(6));
			$data['iarea']=$iarea;
			$this->load->view('notakoreksi/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/nota/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
  			$query = $this->db->query("select * from tr_area",false);
      }else{
        $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
	                        			   or i_area = '$area4' or i_area = '$area5'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listnota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('nota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu60')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/nota/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('nota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('nota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
