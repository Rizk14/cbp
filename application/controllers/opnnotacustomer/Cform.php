<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnamenota');
			$data['iarea']='';
			$data['dto']='';
      $data['nt']='';
      $data['jt']='';
			$this->load->view('opnnotacustomer/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	= strtoupper($this->input->post('cari'));
      $nt   = $this->input->post('chkntx');
      $jt   = $this->input->post('chkjtx');
			$dto	= $this->input->post('dto');
			$iarea= $this->input->post('iarea');
			if($dto=='') $dto=$this->uri->segment(4);
			if($nt=='') $nt=$this->uri->segment(5);
			if($jt=='') $jt=$this->uri->segment(6);
			if($iarea=='') $iarea	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/opnnotacustomer/cform/view/'.$dto.'/'.$nt.'/'.$jt.'/'.$iarea.'/';
      if($nt=='qqq'){
			  $query = $this->db->query(" select a.* from (
			                              select distinct(a.i_customer), a.i_area, a.i_salesman, c.e_salesman_name, b.e_customer_name,
			                              b.n_customer_toplength
                                    from tm_nota a
                                    inner join tr_customer b on (a.i_customer=b.i_customer) 
                                    inner join tr_salesman c on (a.i_salesman=c.i_salesman)
                                    where a.d_nota <= to_date('$dto','dd-mm-yyyy') and a.i_area='$iarea' and (upper(a.i_nota) like '%$cari%' 
                                    or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                                    or upper(b.e_customer_name) like '%$cari%') and a.f_nota_cancel='f' and a.v_sisa>0
                                    union all
                                    select distinct(a.i_customer), a.i_area, a.i_salesman, c.e_salesman_name, b.e_customer_name, 
                                    b.n_customer_toplength
                                    from tm_nota a
                                    inner join tr_customer b on (a.i_customer=b.i_customer)
                                    inner join tr_customer_consigment d on (a.i_customer=d.i_customer and d.i_area_real='$iarea')
                                    inner join tr_salesman c on (a.i_salesman=c.i_salesman)
                                    where a.d_nota <= to_date('$dto','dd-mm-yyyy') and (upper(a.i_nota) like '%$cari%' 
                                    or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                                    or upper(b.e_customer_name) like '%$cari%') and a.f_nota_cancel='f' and a.v_sisa>0
                                    ) as a ",false);
      }elseif($jt=='qqq'){
			  $query = $this->db->query(" select a.* from (
			                              select distinct(a.i_customer), a.i_area, a.i_salesman, c.e_salesman_name, b.e_customer_name, 
			                              b.n_customer_toplength
                                    from tm_nota a
                                    inner join tr_customer b on (a.i_customer=b.i_customer)
                                    inner join tr_salesman c on (a.i_salesman=c.i_salesman)
                                    where a.d_jatuh_tempo <= to_date('$dto','dd-mm-yyyy') and a.i_area='$iarea' 
                                    and (upper(a.i_nota) like '%$cari%' or upper(a.i_spb) like '%$cari%' 
                                    or upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%')
                                    and a.f_nota_cancel='f' and a.v_sisa>0
                                    union all
                                    select distinct(a.i_customer), a.i_area, a.i_salesman, c.e_salesman_name, b.e_customer_name, 
                                    b.n_customer_toplength
                                    from tm_nota a
                                    inner join tr_customer b on (a.i_customer=b.i_customer)
                                    inner join tr_customer_consigment d on (a.i_customer=d.i_customer and d.i_area_real='$iarea')
                                    inner join tr_salesman c on (a.i_salesman=c.i_salesman)
                                    where a.d_jatuh_tempo <= to_date('$dto','dd-mm-yyyy') and (upper(a.i_nota) like '%$cari%' 
                                    or upper(a.i_spb) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                                    or upper(b.e_customer_name) like '%$cari%') and a.f_nota_cancel='f' and a.v_sisa>0
                                    ) as a",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('opnnotacustomer/mmaster');
      if($nt=='qqq'){
	  		$data['page_title'] = $this->lang->line('opnamenota').' per tanggal nota';
      }else{
  			$data['page_title'] = $this->lang->line('opnamenota').' per tanggal jatuh tempo';
      }
			$data['cari']		= $cari;
			$data['keyword']= '';	
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['nt']     = $nt;
      $data['jt']     = $jt;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dto,$config['per_page'],$this->uri->segment(8),$cari,$nt,$jt);
			$this->load->view('opnnotacustomer/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnamenota');
			$this->load->view('opnnotacustomer/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if($cari=='') $cari	= $this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/opnnotacustomer/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$cari%' 
										  or upper(a.i_spb) like '%$cari%' 
										  or upper(a.i_customer) like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('opnnotacustomer/mmaster');
			$data['page_title'] = $this->lang->line('opnamenota');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('opnnotacustomer/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$keyword = strtoupper($this->uri->segment(7));	
			$config['base_url'] = base_url().'index.php/opnnotacustomer/cform/cariperpages/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$keyword.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$keyword%' 
										  or upper(a.i_spb) like '%$keyword%' 
										  or upper(a.i_customer) like '%$keyword%' 
										  or upper(b.e_customer_name) like '%$keyword%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('opnnotacustomer/mmaster');
			$data['page_title'] = $this->lang->line('opnamenota');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$keyword);
			$this->load->view('opnnotacustomer/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opnnotacustomer/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('opnnotacustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('opnnotacustomer/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opnnotacustomer/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('opnnotacustomer/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('opnnotacustomer/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
  function opname()
  {
		if (
			(($this->session->userdata('logged_in')) &&
     	 ($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $icustomer=$this->uri->segment(4);
      $isalesman=$this->uri->segment(5);
      $dto=$this->uri->segment(6);
      $nt=$this->uri->segment(7);
			$jt=$this->uri->segment(8);
			$config['base_url'] = base_url().'index.php/opnnotacustomer/cform/opname/'.$icustomer.'/'.$isalesman.'/'.$dto.'/'.$nt.'/'.$jt.'/index/';
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
      if($nt=='qqq'){
	    $query= $this->db->query("select i_customer, i_salesman, i_nota, d_nota, d_jatuh_tempo, v_nota_netto, v_sisa
                        from tm_nota
                        where i_customer='$icustomer' and i_salesman='$isalesman'
                        and v_sisa>0 and f_nota_cancel='f' and d_nota<='$dto'
                        order by i_nota",false);
      }else{
	    $query= $this->db->query("select i_customer, i_salesman, i_nota, d_nota, d_jatuh_tempo, v_nota_netto, v_sisa
                        from tm_nota
                        where i_customer='$icustomer' and i_salesman='$isalesman'
                        and v_sisa>0 and f_nota_cancel='f' and d_jatuh_tempo<='$dto'
                        order by i_nota",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(10);
      $uri = $this->uri->segment(10);
			$this->paginationxx->initialize($config);

			$this->load->model('opnnotacustomer/mmaster');
			$data['page_title'] = $this->lang->line('listnota');
			$data['icustomer']	= $icustomer;
			$data['isalesman']	= $isalesman;
			$data['dto']		= $dto;
			$data['nt']		= $nt;
			$data['jt']		= $jt;
			$data['isi']=$this->mmaster->bacaopname($icustomer,$isalesman,$dto,$nt,$jt,$config['per_page'],$this->uri->segment(10));
			$this->load->view('opnnotacustomer/vlistopname', $data);
		}else{
			$this->load->view('awal/index.php');
		}
  }
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
     	 ($this->session->userdata('menu228')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dto	    = $this->input->post('dto');
			$icustomer= $this->input->post('icustomer');
			$isalesman= $this->input->post('isalesman');
      $nt   = $this->input->post('chkntx');
      $jt   = $this->input->post('chkjtx');
      if($icustomer=='')$icustomer=$this->uri->segment(4);
      if($isalesman=='')$isalesman=$this->uri->segment(5);
      if($dto=='')$dto=$this->uri->segment(6);
      if($nt=='') $nt=$this->uri->segment(7);
			if($jt=='') $jt=$this->uri->segment(8);
			$this->load->model('opnnotacustomer/mmaster');
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$data['dto']  = $dto;
			$data['icustomer']= $icustomer;
			$data['isalesman']= $isalesman;
			$data['nt']  = $nt;
			$data['jt']  = $jt;
			$data['page_title'] = $this->lang->line('printopnnotacustomer');
			$data['isi']=$this->mmaster->baca($icustomer,$isalesman,$dto,$nt,$jt);
			$data['user']	= $this->session->userdata('user_id');
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
	    $query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
		    $now	  = $row['c'];
	    }
	    $pesan='Cetak Opname Nota Per Customer sampai tanggal '.$dto.' Pelanggan:'.$icustomer;
	    $this->load->model('logger');
	    $this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('opnnotacustomer/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
