<?php
class Cform extends CI_Controller
{
	public $menu 	= "20060012";
	public $title 	= "SPmB Manual";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$ispmb 		= $this->input->post('ispmb', TRUE);
			$ispmbold 	= $this->input->post('ispmbold', TRUE);
			$dspmb 		= $this->input->post('dspmb', TRUE);
			if ($dspmb != '') {
				$tmp = explode("-", $dspmb);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dspmb = $th . "-" . $bl . "-" . $hr;
				$thbl = substr($th, 2, 2) . $bl;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$eareaname = $this->input->post('eareaname', TRUE);
			$eremark	= $this->input->post('eremark', TRUE);
			$jml		= $this->input->post('jml', TRUE);
			$fop		= 'f';
			$nprint		= 0;
			if ($dspmb != '' && $eareaname != '') {
				$this->db->trans_begin();
				$this->load->model('spmbnewmanual/mmaster');
				$ispmb	= $this->mmaster->runningnumber($thbl);
				$this->mmaster->insertheader($ispmb, $dspmb, $iarea, $fop, $nprint, $ispmbold, $eremark);
				for ($i = 1; $i <= $jml; $i++) {
					$iproduct			  = $this->input->post('iproduct' . $i, TRUE);
					$iproductgrade	= 'A';
					$iproductmotif	= $this->input->post('motif' . $i, TRUE);
					$eproductname		= $this->input->post('eproductname' . $i, TRUE);
					$vunitprice		  = $this->input->post('vproductmill' . $i, TRUE);
					$vunitprice	  	= str_replace(',', '', $vunitprice);
					$norder	    		= $this->input->post('norder' . $i, TRUE);
					$nacc 	    		= $this->input->post('nacc' . $i, TRUE);
					$eremark		  	= $this->input->post('eremark' . $i, TRUE);
					if ($norder > 0) {
						$this->mmaster->insertdetail($ispmb, $iproduct, $iproductgrade, $eproductname, $norder, $nacc, $vunitprice, $iproductmotif, $eremark, $iarea, $i);
					}
				}
				if (($this->db->trans_status() === FALSE)) {
					$this->db->trans_rollback();
				} else {
					#			    $this->db->trans_rollback();
					$this->db->trans_commit();

					$sess = $this->session->userdata('session_id');
					$id = $this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		=  $this->db->query($sql);
					if ($rs->num_rows > 0) {
						foreach ($rs->result() as $tes) {
							$ip_address	  = $tes->ip_address;
							break;
						}
					} else {
						$ip_address = 'kosong';
					}

					$data['user']	= $this->session->userdata('user_id');
					#			$data['host']	= $this->session->userdata('printerhost');
					$data['host']	= $ip_address;
					$data['uri']	= $this->session->userdata('printeruri');
					$query 	= pg_query("SELECT current_timestamp as c");
					while ($row = pg_fetch_assoc($query)) {
						$now	  = $row['c'];
					}
					$pesan = 'Input SPMB Area:' . $iarea . ' No:' . $ispmb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now, $pesan);

					$data['sukses']			= true;
					$data['inomor']			= $ispmb;
					$this->load->view('nomor', $data);
				}
			} else {
				$data['page_title'] = $this->lang->line('spmbmanual');

				$this->logger->writenew("Membuka Menu : " . $this->menu . " - " . $this->title);

				$data['ispmb'] = '';
				$this->load->model('spmbnewmanual/mmaster');
				$data['isi'] = ''; #$this->mmaster->bacasemua();
				$data['detail'] = "";
				$data['jmlitem'] = "";
				$data['tgl'] = date('d-m-Y');
				$this->load->view('spmbnewmanual/vmainform', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('spmbmanual');
			$this->load->view('spmbnewmanual/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('spmbmanual') . " update";
			if ($this->uri->segment(4) != '') {
				$ispmb  = $this->uri->segment(4);
				$iarea  = $this->uri->segment(5);
				$dfrom  = $this->uri->segment(6);
				$dto 	  = $this->uri->segment(7);
				$peraw 	= $this->uri->segment(8);
				$perak 	= $this->uri->segment(9);
				$data['ispmb'] = $ispmb;
				$data['iarea'] = $iarea;
				$data['dfrom'] = $dfrom;
				$data['dto']	 = $dto;
				$data['peraw']	 = $peraw;
				$data['perak']	 = $perak;
				$query = $this->db->query("select i_product from tm_spmb_item where i_spmb='$ispmb'");
				$data['jmlitem'] = $query->num_rows();
				$this->load->model('spmbnewmanual/mmaster');
				$data['isi'] = $this->mmaster->baca($ispmb);
				$data['detail'] = $this->mmaster->bacadetail($ispmb);
				$this->load->view('spmbnewmanual/vmainform', $data);
			} else {
				$this->load->view('spmbnewmanual/vinsert_fail', $data);
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$ispmb 		= $this->input->post('ispmb', TRUE);
			$ispmbold 	= $this->input->post('ispmbold', TRUE);
			$dspmb 		= $this->input->post('dspmb', TRUE);
			$eremark	= $this->input->post('eremark', TRUE);
			if ($dspmb != '') {
				$tmp = explode("-", $dspmb);
				$th = $tmp[2];
				$bl = $tmp[1];
				$hr = $tmp[0];
				$dspmb = $th . "-" . $bl . "-" . $hr;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$eareaname	= $this->input->post('eareaname', TRUE);
			$jml		= $this->input->post('jml', TRUE);
			$fop		= 'f';
			$nprint		= 0;
			if ($dspmb != '' && $eareaname != '') {
				$this->db->trans_begin();
				$this->load->model('spmbnewmanual/mmaster');
				$this->mmaster->updateheader($ispmb, $dspmb, $iarea, $ispmbold, $eremark);
				for ($i = 1; $i <= $jml; $i++) {
					$iproduct		  	= $this->input->post('iproduct' . $i, TRUE);
					$iproductgrade	= 'A';
					$iproductmotif	= $this->input->post('motif' . $i, TRUE);
					$eproductname		= $this->input->post('eproductname' . $i, TRUE);
					$vunitprice	  	= $this->input->post('vproductmill' . $i, TRUE);
					$vunitprice 		= str_replace(',', '', $vunitprice);
					$norder   			= $this->input->post('norder' . $i, TRUE);
					$nacc 	    		= $this->input->post('nacc' . $i, TRUE);
					$eremark		  	= $this->input->post('eremark' . $i, TRUE);
					$this->mmaster->deletedetail($iproduct, $iproductgrade, $ispmb, $iproductmotif);
					if ($norder > 0) {
						$this->mmaster->insertdetail($ispmb, $iproduct, $iproductgrade, $eproductname, $norder, $nacc, $vunitprice, $iproductmotif, $eremark, $iarea, $i);
					}
				}
				if (($this->db->trans_status() === FALSE)) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();

					$sess = $this->session->userdata('session_id');
					$id = $this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		=  $this->db->query($sql);
					if ($rs->num_rows > 0) {
						foreach ($rs->result() as $tes) {
							$ip_address	  = $tes->ip_address;
							break;
						}
					} else {
						$ip_address = 'kosong';
					}

					$data['user']	= $this->session->userdata('user_id');
					#			$data['host']	= $this->session->userdata('printerhost');
					$data['host']	= $ip_address;
					$data['uri']	= $this->session->userdata('printeruri');
					$query 	= pg_query("SELECT current_timestamp as c");
					while ($row = pg_fetch_assoc($query)) {
						$now	  = $row['c'];
					}
					$pesan = 'Update SPMB Area:' . $iarea . ' No:' . $ispmb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now, $pesan);

					$data['sukses']			= true;
					$data['inomor']			= $ispmb;
					$this->load->view('nomor', $data);
				}
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$ispmb	= $this->input->post('ispmbdelete', TRUE);
			$this->load->model('spmbnewmanual/mmaster');
			$this->mmaster->delete($ispmb);

			$sess = $this->session->userdata('session_id');
			$id = $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if ($rs->num_rows > 0) {
				foreach ($rs->result() as $tes) {
					$ip_address	  = $tes->ip_address;
					break;
				}
			} else {
				$ip_address = 'kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
			#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while ($row = pg_fetch_assoc($query)) {
				$now	  = $row['c'];
			}
			$pesan = 'Hapus SPMB No:' . $ispmb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$data['page_title'] = $this->lang->line('spmbmanual');
			$data['ispmb'] = '';
			$data['jmlitem'] = '';
			$data['detail'] = '';
			$data['isi'] = $this->mmaster->bacasemua();
			$this->load->view('spmbnewmanual/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$ispmb			= $this->input->post('ispmbdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$iarea = $this->input->post('iareadel', TRUE);
			$dfrom = $this->input->post('dfrom', TRUE);
			$dto 	= $this->input->post('dto', TRUE);

			$this->db->trans_begin();
			$this->load->model('spmbnewmanual/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $ispmb, $iproductmotif);
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();

				$data['ispmb'] = $ispmb;
				$data['iarea'] = $iarea;
				$data['dfrom'] = $dfrom;
				$data['dto']	 = $dto;
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ispmb'");
				$data['jmlitem'] = $query->num_rows();

				$this->load->model('spmbnewmanual/mmaster');
				$data['isi'] = $this->mmaster->baca($ispmb);
				$data['detail'] = $this->mmaster->bacadetail($ispmb);




				$this->load->view('spmbnewmanual/vmainform', $data);

				/*
  			$data['page_title'] = $this->lang->line('spmbmanual')." Update";
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ispmb'");
				$data['jmlitem']= $query->num_rows(); 				
				$data['ispmb']  = $ispmb;
				$data['isi']    =$this->mmaster->baca($ispmb);
				$data['detail']=$this->mmaster->bacadetail($ispmb);
				$this->load->view('spmbnewmanual/vmainform', $data);
			*/
			}
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari = ($this->input->post("cari"));
			$baris = $this->input->post("baris");
			$peraw = $this->input->post("peraw");
			$perak = $this->input->post("perak");
			$area = $this->input->post("area");
			if ($baris == '') $baris = $this->uri->segment(4);
			if ($peraw == '') $peraw = $this->uri->segment(5);
			if ($perak == '') $perak = $this->uri->segment(6);
			if ($area == '') $area = $this->uri->segment(7);
			$config['base_url'] = base_url() . 'index.php/spmbnewmanual/cform/product/' . $baris . '/' . $peraw . '/' . $perak . '/' . $area . '/sikasep/';

			/*
select trunc(sum(n_deliver)/3) as rata, i_product, i_area from tm_nota_item
where i_nota>'FP-1107' and i_nota<'FP-1111'
group by i_product, i_area
order by i_product, i_area
*/

			$query = $this->db->query(" select a.i_product
										              from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product and upper(a.i_product) like '%$cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);

			$this->load->model('spmbnewmanual/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris'] = $baris;
			$data['peraw'] = $peraw;
			$data['perak'] = $perak;
			$data['area'] = $area;
			$data['cari'] = '';
			$data['isi'] = $this->mmaster->bacaproduct($config['per_page'], $this->uri->segment(9), $peraw, $perak, $cari);
			$this->load->view('spmbnewmanual/vlistproduct', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$baris   = $this->input->post('baris', FALSE);
			if ($baris == '') $baris = $this->uri->segment(4);
			$cari   = ($this->input->post('cari', FALSE));
			if ($cari == '' && $this->uri->segment(5) != 'sikasep') $cari = $this->uri->segment(5);
			$peraw = $this->input->post("peraw");
			$perak = $this->input->post("perak");
			$area = $this->input->post("area");

			if ($peraw == '') $peraw = $this->uri->segment(6);
			if ($perak == '') $perak = $this->uri->segment(7);
			if ($area == '') $area = $this->uri->segment(8);

			if ($cari != 'sikasep')
				$config['base_url'] = base_url() . 'index.php/spmbnewmanual/cform/cariproduct/' . $baris . '/' . $cari . '/' . $peraw . '/' . $perak . '/' . $area . '/';
			else
				$config['base_url'] = base_url() . 'index.php/spmbnewmanual/cform/cariproduct/' . $baris . '/' . $peraw . '/' . $perak . '/' . $area . '/sikasep/';

			$query = $this->db->query("	select a.i_product as kode, a.i_product_motif as motif,
										a.e_product_motifname as namamotif, 
										c.e_product_name as nama,c.v_product_retail as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
									   	and (upper(a.i_product) ilike '%$cari%' or upper(c.e_product_name) ilike '%$cari%') order by c.e_product_name asc ", false);
			#c.v_product_mill as harga
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$this->load->model('spmbnewmanual/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi'] = $this->mmaster->cariproduct($cari, $config['per_page'], $this->uri->segment(9));
			$data['baris'] = $baris;
			$data['cari'] = $cari;
			$data['peraw'] = $peraw;
			$data['perak'] = $perak;
			$data['area'] = $area;
			$this->load->view('spmbnewmanual/vlistproduct', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/spmbnewmanual/cform/area/index/';
			$allarea	= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($allarea == 't') {
				$query = $this->db->query("select * from tr_area ", false);
			} elseif ($allarea == 'f' && ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00')) {
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%'
						or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') ", false);
			} else {
				$query = $this->db->query("select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3'
						or i_area='$area4' or i_area='$area5' ", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('spmbnewmanual/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5, $allarea);
			$this->load->view('spmbnewmanual/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/spmbnewmanual/cform/area/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$allarea	= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($allarea == 't') {
				$query = $this->db->query("select * from tr_area ", false);
			} elseif ($allarea == 'f' && ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00')) {
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%'
						or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') ", false);
			} else {
				$query = $this->db->query("select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3'
						or i_area='$area4' or i_area='$area5' ", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spmbnewmanual/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] = $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $area1, $area2, $area3, $area4, $area5, $allarea);
			$this->load->view('spmbnewmanual/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url() . 'index.php/spmbnewmanual/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('spmbnewmanual/mmaster');
			$data['isi'] = $this->mmaster->cari($cari, $config['per_page'], $this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['ispmb'] = '';
			$data['jmlitem'] = '';
			$data['detail'] = '';
			$this->load->view('spmbnewmanual/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			#			$data['baris']=$this->uri->segment(4);
			#			$baris=$this->uri->segment(4);
			$cari = strtoupper($this->input->post("cari"));
			$baris = $this->input->post("baris");
			$peraw = $this->input->post("peraw");
			$perak = $this->input->post("perak");
			$area = $this->input->post("area");
			if ($baris == '') $baris = $this->uri->segment(4);
			if ($peraw == '') $peraw = $this->uri->segment(5);
			if ($perak == '') $perak = $this->uri->segment(6);
			if ($area == '') $area = $this->uri->segment(7);
			$config['base_url'] = base_url() . 'index.php/spmbnewmanual/cform/productupdate/' . $baris . '/' . $peraw . '/' . $perak . '/' . $area . '/';

			#			$config['base_url'] = base_url().'index.php/spmbnewmanual/cform/productupdate/'.$baris.'/index/';
			$query = $this->db->query(" select a.i_product from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('spmbnewmanual/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi'] = $this->mmaster->bacaproduct($config['per_page'], $this->uri->segment(8), $peraw, $perak, $cari);
			$data['baris'] = $baris;
			$data['peraw'] = $peraw;
			$data['perak'] = $perak;
			$data['area'] = $area;
			$this->load->view('spmbnewmanual/vlistproductupdate', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cariproductupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['baris'] = $this->uri->segment(4);
			$baris = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/spmbnewmanual/cform/productupdate/' . $baris . '/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(
				"	select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_retail as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
										and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc ",
				false
			);
			# c.v_product_mill as harga
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('spmbnewmanual/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi'] = $this->mmaster->cariproduct($cari, $config['per_page'], $this->uri->segment(6));
			$data['baris'] = $baris;
			$this->load->view('spmbnewmanual/vlistproductupdate', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$baris = $this->uri->segment(4);
			$config['base_url'] = base_url() . 'index.php/spmbnewmanual/cform/store/' . $baris . '/sikasep/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
				$query = $this->db->query(" select distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
								from tr_store_location a, tr_store b, tr_area c
								where a.i_store = b.i_store and b.i_store=c.i_store
								order by c.i_store", false);
			} else {
				$query = $this->db->query(" select distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
								from tr_store_location a, tr_store b, tr_area c
								where a.i_store = b.i_store and b.i_store=c.i_store
								and (c.i_area = '$area1' or c.i_area = '$area2' or
								c.i_area = '$area3' or c.i_area = '$area4' or
								c.i_area = '$area5')
                            	order by c.i_store", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris'] = $baris;
			$data['cari'] = '';
			$this->load->model('spmbnewmanual/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi'] = $this->mmaster->bacastore($config['per_page'], $this->uri->segment(6), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('spmbnewmanual/vliststore', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu256') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			/*$config['base_url'] = base_url().'index.php/spmbnewmanual/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);*/

			$baris   = $this->input->post('baris', FALSE);
			if ($baris == '') $baris = $this->uri->segment(4);
			$cari   = ($this->input->post('cari', FALSE));
			if ($cari == '' && $this->uri->segment(5) != 'sikasep') $cari = $this->uri->segment(5);
			if ($cari != 'sikasep')
				$config['base_url'] = base_url() . 'index.php/spmbnewmanual/cform/cariarea/' . $baris . '/' . $cari . '/';
			else
				$config['base_url'] = base_url() . 'index.php/spmbnewmanual/cform/cariarea/' . $baris . '/sikasep/';

			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if ($area1 == '00' || $area2 == '00' || $area3 == '00' || $area4 == '00' || $area5 == '00') {
				$query = $this->db->query(" select distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
									from tr_store_location a, tr_store b, tr_area c
									where a.i_store = b.i_store and b.i_store=c.i_store
									and (upper(a.i_store) ilike '%$cari%' 
									or upper(b.e_store_name) ilike '%$cari%'
									or upper(a.i_store_location) ilike '%$cari%'
									or upper(a.e_store_locationname) ilike '%$cari%')
									order by c.i_store", false);
			} else {
				$query = $this->db->query(" 	select distinct(c.i_store) as i_store , a.i_store_location, a.e_store_locationname,b.e_store_name 
												from tr_store_location a, tr_store b, tr_area c
												where a.i_store = b.i_store and b.i_store=c.i_store
												and (upper(a.i_store) ilike '%$cari%' 
												or upper(b.e_store_name) ilike '%$cari%'
												or upper(a.i_store_location) ilike '%$cari%'
												or upper(a.e_store_locationname) ilike '%$cari%')
												and (c.i_area = '$area1' or c.i_area = '$area2' or
												c.i_area = '$area3' or c.i_area = '$area4' or
												c.i_area = '$area5')
												order by c.i_store", false);
			}
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('spmbnewmanual/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['baris'] = $baris;
			$data['cari'] = $cari;
			$data['isi'] = $this->mmaster->caristore($cari, $config['per_page'], $this->uri->segment(6), $area1, $area2, $area3, $area4, $area5);
			$this->load->view('spmbnewmanual/vliststore', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
