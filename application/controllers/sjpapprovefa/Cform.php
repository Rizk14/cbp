<?php
class Cform extends CI_Controller
{
	public $menu 	= '5001A01';
	public $title 	= 'SJP Approve Keuangan';
	public $folder 	= 'sjpapprovefa';

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->model($this->folder . '/mmaster');
	}

	function index()
	{
		$iuser	= $this->session->userdata('user_id');
		$cari	= '';

		$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/index/';

		$query = $this->db->query(" select a.*, b.e_area_name, c.f_spmb_consigment, c.f_retur, dkb.i_approve1, a.i_approve as approvefa
									from tm_sjp a
									left join tm_dkb_sjp dkb on a.i_dkb = dkb.i_dkb and a.i_area = dkb.i_area, tr_area b, tm_spmb c
									where a.i_area=b.i_area and a.i_spmb=c.i_spmb
									and (upper(a.i_sjp) like '%$cari%' or upper(a.i_sjp_old) like '%$cari%')
									and (a.i_area in (select i_area from tm_user_area where i_user = '$iuser' ))
									and a.i_approve is null and a.f_sjp_cancel='f' 
									AND (NOT a.i_dkb ISNULL AND a.i_dkb <> '')
									ORDER BY a.i_sjp desc", false);
		$config['total_rows'] 	= $query->num_rows();
		$config['per_page'] 	= '10';
		$config['first_link'] 	= 'Awal';
		$config['last_link'] 	= 'Akhir';
		$config['next_link'] 	= 'Selanjutnya';
		$config['prev_link'] 	= 'Sebelumnya';
		$config['cur_page'] 	= $this->uri->segment(4);
		$this->pagination->initialize($config);

		$data['page_title'] = $this->title;
		$data['folder'] 	= $this->folder;
		$data['isjp'] 		= '';
		$data['cari'] 		= '';
		$data['isi']		= $this->mmaster->bacaperiode($iuser, $config['per_page'], $this->uri->segment(4), $cari);

		$this->logger->writenew("Membuka Menu : " . $this->menu . " - " . $this->title);

		$this->load->view($this->folder . '/vmainform', $data);
	}

	function edit()
	{

		$data['page_title'] = $this->title;
		$data['folder'] 	= $this->folder;

		if ($this->uri->segment(4) != '') {
			$isjp				= $this->uri->segment(4);
			$iarea 				= $this->uri->segment(5);
			$ispmb 				= $this->uri->segment(6);
			$fspmbretur			= $this->uri->segment(7); /* Cek Status SPmB Apakah Dari Returan yang akan dijadikan stok cabang atau tidak (08 Ags 2023) */

			$data['isjp'] 		= $isjp;
			$data['iarea'] 		= $iarea;
			$data['fspmbretur']	= $fspmbretur;
			$data['pst']		= $this->session->userdata('i_area');

			$query 				= $this->db->query("select * from tm_sjp_item where i_sjp = '$isjp' and i_area='$iarea'");
			$data['jmlitem'] 	= $query->num_rows();

			$data['isi'] 	= $this->mmaster->baca($isjp, $iarea);
			$data['detail'] = $this->mmaster->bacadetail($isjp, $iarea, $ispmb);

			$this->logger->writenew("Lihat Detail SJP No : " . $isjp . " Area : " . $iarea);

			$this->load->view($this->folder . '/vmainform', $data);
		} else {
			$this->load->view($this->folder . '/vinsert_fail', $data);
		}
	}

	function update()
	{
		$isj	     	= $this->input->post('isj', TRUE);
		$dapprovefa 	= $this->input->post('dapprovefa', TRUE);
		$iarea			= $this->input->post('iarea', TRUE);

		if ($dapprovefa != '') {
			$tmp = explode("-", $dapprovefa);
			$th = $tmp[2];
			$bl = $tmp[1];
			$hr = $tmp[0];
			$dapprovefa = $th . "-" . $bl . "-" . $hr;
			$thbl	  = substr($th, 2, 2) . $bl;
			$tmpsj	= explode("-", $isj);
			$firstsj = $tmpsj[0];
			$lastsj	= $tmpsj[2];
			$newsj	= $firstsj . "-" . $thbl . "-" . $lastsj;
		}

		$this->db->trans_begin();

		$this->mmaster->approve($isj, $iarea, $dapprovefa);

		if (($this->db->trans_status() === FALSE)) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();

			$data['folder'] 	= $this->folder;

			$this->logger->writenew('Approve Keuangan SJP No : ' . $isj . ' Area : ' . $iarea);

			$data['sukses']			= true;
			$data['inomor']			= $newsj;
			$this->load->view('nomor', $data);
		}
	}

	function cari()
	{


		$iuser	= $this->session->userdata('user_id');
		$cari = ($this->input->post("cari", false));

		if ($cari == '') $cari = $this->uri->segment(4);
		if ($cari == 'zxvf') {
			$cari = '';
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/cari/zxvf/';
		} else {
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/cari/' . $cari . '/';
		}

		$query = $this->db->query(" select a.*, b.e_area_name, c.f_spmb_consigment, c.f_retur, dkb.i_approve1, a.i_approve as approvefa
										from tm_sjp a
										left join tm_dkb_sjp dkb on a.i_dkb = dkb.i_dkb and a.i_area = dkb.i_area, tr_area b, tm_spmb c
										where a.i_area=b.i_area and a.i_spmb=c.i_spmb
										and (upper(a.i_sjp) like '%$cari%' or upper(a.i_sjp_old) like '%$cari%')
										and (a.i_area in (select i_area from tm_user_area where i_user = '$iuser' ))
										and a.i_approve is null and a.f_sjp_cancel='f' 
										AND (NOT a.i_dkb ISNULL AND a.i_dkb <> '')
										ORDER BY a.i_sjp desc ", false);

		$config['total_rows'] = $query->num_rows();
		$config['per_page'] = '10';
		$config['first_link'] = 'Awal';
		$config['last_link'] = 'Akhir';
		$config['next_link'] = 'Selanjutnya';
		$config['prev_link'] = 'Sebelumnya';
		$config['cur_page'] = $this->uri->segment(5);
		$this->pagination->initialize($config);

		$data['page_title'] = $this->lang->line('listsjpreceive');
		$data['folder'] 	= $this->folder;
		$data['cari'] 		= $cari;
		$data['isjp']   	= '';
		$data['isi']    	= $this->mmaster->cari($cari, $config['per_page'], $this->uri->segment(5));

		$this->logger->writenew("Cari Data Berdasarkan Keyword : " . $cari . " Menu : " . $this->menu . " " . $this->title);

		$this->load->view($this->folder . '/vmainform', $data);
	}

	function hitungminimalterima()
	{
		$isjp		= $this->input->post('isjp') ? $this->input->get_post('isjp') : $this->input->get_post('isjp');
		$dsjp		= $this->input->post('dsjp') ? $this->input->get_post('dsjp') : $this->input->get_post('dsjp');
		$iarea		= $this->input->post('iarea') ? $this->input->get_post('iarea') : $this->input->get_post('iarea');
		$fretur		= $this->input->post('fretur') ? $this->input->get_post('fretur') : $this->input->get_post('fretur');
		$dreceive	= $this->input->post('dreceive') ? $this->input->get_post('dreceive') : $this->input->get_post('dreceive');

		$query	= $this->db->query("SELECT distinct(c.i_store), s1.n_tolerance
									from tm_sjp a, tr_area b, tm_sjp_item c, tm_dkb_sjp d1, tm_send_tolerance s1
									where 
									a.i_area=b.i_area and a.i_sjp=c.i_sjp
									and a.i_area=c.i_area and a.f_sjp_cancel='f'
									AND a.i_dkb = d1.i_dkb AND d1.i_dkb_via = s1.i_send_type AND d1.i_area = s1.i_area 
									and a.i_sjp ='$isjp' and a.i_area='$iarea' ");
		if ($query->num_rows() > 0) {
			$get_tolerance = $query->row()->n_tolerance;
		}

		$ntolerance = $fretur == 'f' ? $get_tolerance : 0;

		$dreceivelimit = date('Y-m-d', strtotime($ntolerance . ' day', strtotime($dsjp)));

		echo $dreceivelimit;
	}
}
