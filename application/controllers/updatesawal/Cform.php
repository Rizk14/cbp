<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('updatesawal');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('updatesawal/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			
      $this->load->model('updatesawal/mmaster');
	  $cari				= strtoupper($this->input->post('cari'));
	  $iperiode			= $this->input->post('iperiode');
      $iarea 			= $this->input->post('iarea');
      $store 			= $this->input->post('istore');
      $istorelocation 	= $this->input->post('istorelocation');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
		if($iarea=='') $iarea=$this->uri->segment(5);
		if($istorelocation=='') $istorelocation=$this->uri->segment(6);
     	if($iarea=='AA')$store='AA';
      	if($store==''){
        $query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
        $st=$query->row();
        $store=$st->i_store;
	  	}
		  $xx = substr($iperiode,4,2);
		  /* var_dump($xx);
		  die; */
		/*$periodex   = date('Ym', strtotime('-1 month', strtotime($iperiode)));
        $periodey   = date('Ym'); */

	  	if($xx == 01){
				  $thnprev 		= substr($iperiode,0,4) - 1;
				  $bulanprev 	= '12';
	  	}else{
				$thnprev = substr($iperiode,0,4);
				$bulanprev = substr($iperiode,4,2) - 1;
	  }
	  $jumbul = strlen($bulanprev);
	  if($jumbul == 1){
		$iperiodeprev = $thnprev.'0'.$bulanprev;
	  }else{
		$iperiodeprev = $thnprev.$bulanprev;
	  }
	
		$this->db->query("	DELETE FROM tm_mutasi_saldoawal
							WHERE i_store='$store' and e_mutasi_periode = '$iperiode'
							and i_store_location='$istorelocation'",false);

		if($store == 'AA'){
			$que2	= $this->db->query("SELECT i_store, i_store_location, i_store_locationbin, '$iperiode' as i_periode, i_product, i_product_grade, i_product_motif, 
										n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_awal  
										from f_mutasi_stock_pusat_saldoakhir('$iperiodeprev')",false);
		}else if($store == 'PB'){
			$que2	= $this->db->query("SELECT   i_store, i_store_location, i_store_locationbin, '$iperiode' as i_periode, i_product, i_product_grade, i_product_motif, 
										n_saldo_akhir-(n_git + n_git_asal) as n_saldo_awal  
										from f_mutasi_stock_mo_pb_saldoakhircoba('$iperiodeprev')",false);
		}else{
			$que2	= $this->db->query("SELECT i_store, i_store_location, i_store_locationbin, '$iperiode' as i_periode, i_product, i_product_grade, i_product_motif, 
										n_saldo_akhir-(n_mutasi_git+n_git_penjualan) as n_saldo_awal  
										from f_mutasi_stock_daerah_all_saldoakhir('$iperiodeprev') where i_store = '$store'",false);
		}							
				if($que2->num_rows()>0){
				   foreach($que2->result() as $xx){
					$this->db->query("INSERT INTO tm_mutasi_saldoawal
							(i_store, i_store_location, i_store_locationbin, e_mutasi_periode, i_product, i_product_grade, i_product_motif, n_saldo_awal)
					VALUES  ('$xx->i_store','$xx->i_store_location','$xx->i_store_locationbin','$xx->i_periode','$xx->i_product','$xx->i_product_grade','$xx->i_product_motif','$xx->n_saldo_awal')",false);
				   }
				$message = "STOCK TERUPDATE";
				echo $message;
		 }
		$data['page_title'] = $this->lang->line('updatesawal');
		$data['iperiode']	= '';
		$data['iarea']	  = '';
		$this->load->view('updatesawal/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('updatesawal/mmaster');
			$iperiode	= $this->input->post('iperiode');
      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			if($iarea=='') $iarea=$this->uri->segment(5);
      if(strlen($iarea)==1) $iarea='0'.$iarea;
      $query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
      $st=$query->row();
      $store=$st->i_store;
			$config['base_url'] = base_url().'index.php/updatesawal/cform/view/'.$iperiode.'/'.$store.'/';
			if($iperiode>'201512'){
			  $query = $this->db->query("select i_product from f_mutasi_stock_pusat_saldoakhir('$iperiode')",false);
			}else{
			  $query = $this->db->query("select i_product from f_mutasi_stock_pusat('$iperiode')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listmutasi');
      $cari='';
      $data['cari']=$cari;
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
			$data['istore']	= $store;
			$data['isi']		= $this->mmaster->baca($iperiode,$store,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('updatesawal/vmainform',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$dto	= $this->input->post('dto');
			if($iperiode=='') $iperiode=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/updatesawal/cform/view/'.$iperiode.'/index/';
			$query = $this->db->query(" select a.*, b.e_area_name from tm_target a, tr_area b
										where a.i_periode = '$iperiode'
										and a.i_area=b.i_area
										and(upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('updatesawal/mmaster');
			$data['page_title'] = $this->lang->line('listmutasi');
			$data['cari']	= $cari;
			$data['iperiode']	= $iperiode;
			$data['isi']	= $this->mmaster->cari($iperiode,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('updatesawal/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	/* function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/updatesawal/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" 	select a.*, b.i_store_location
											from tr_store a 
											inner join tr_store_location b on(a.i_store = b.i_store)
											where a.i_store = '16'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '100';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('updatesawal/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('updatesawal/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/updatesawal/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store and (upper(a.i_area) like '%$cari%' or upper(a.e_area_name) like '%$cari%')
                                    and i_area in ( select i_area from tm_user_area where i_user='$iuser')
                                    group by a.i_store, b.e_store_name",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('updatesawal/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('updatesawal/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	} */

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu471')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/updatesawal/cform/area/index/';
			$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query(" select
										distinct (b.i_store),
										b.e_store_name,
										c.i_store_location,
										c.e_store_locationname
									from
										tr_store b,
										tr_store_location c
									where
										b.i_store = c.i_store
									order by
										b.i_store,
										c.i_store_location",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('updatesawal/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('updatesawal/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu471')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/updatesawal/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query = $this->db->query("select
											distinct (b.i_store),
											b.e_store_name,
											c.i_store_location,
											c.e_store_locationname
										from
											tr_store b,
											tr_store_location c
										where
											b.i_store = c.i_store
											and (upper(b.e_store_name) ilike '%$cari%'
											or upper(b.i_store) ilike '%$cari%')
										order by
											b.i_store,
											c.i_store_location",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('updatesawal/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('updatesawal/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}



	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('updatesawal/mmaster');
      $iperiode = $this->uri->segment(4);
      $iarea    = $this->uri->segment(5);
      $iproduct = $this->uri->segment(6);
      $saldo    = $this->uri->segment(7);
      $istorelocation = $this->uri->segment(8);
      $iproductgrade = $this->uri->segment(9);
			$this->load->model('updatesawal/mmaster');     
			$data['page_title'] = $this->lang->line('listmutasi');
			$data['iperiode']	  = $iperiode;
			$data['iarea']	    = $iarea;
			$data['iproduct']	  = $iproduct;
			$data['saldo']	    = $saldo;
      $data['istorelocation']=$istorelocation;
			$data['detail']	    = $this->mmaster->detail($istorelocation,$iperiode,$iarea,$iproduct,$iproductgrade);
			$this->load->view('updatesawal/vmainform',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetakdetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu540')=='t')) ||
			(($this->session->userdata('logged_in')) &&

			($this->session->userdata('allmenu')=='t'))
		   ){
      $this->load->model('updatesawal/mmaster');
      $iperiode = $this->uri->segment(4);
      $iarea    = $this->uri->segment(5);
      $iproduct = $this->uri->segment(6);
      $saldo    = $this->uri->segment(7);
      $istorelocation  = $this->uri->segment(8);
			$this->load->model('updatesawal/mmaster');     
			$data['page_title'] = $this->lang->line('listmutasi');
			$data['iperiode']	  = $iperiode;
			$data['iarea']	    = $iarea;
			$data['iproduct']	  = $iproduct;
			$data['saldo']	    = $saldo;
			$data['istorelocation'] = $istorelocation;
			$data['detail']	    = $this->mmaster->detail($istorelocation,$iperiode,$iarea,$iproduct);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak Detail Mutasi Periode:'.$iperiode.' Product:'.$iproduct.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('updatesawal/vformprintdetail',$data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu219')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('updatesawal/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('pperiode');
      $iarea = $this->input->post('iarea');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
			if($iarea=='') $iarea=$this->uri->segment(5);
      $query = $this->db->query("select i_store from tr_area where i_area='$iarea'");
      $st=$query->row();
      $store=$st->i_store;
      if($iperiode>'201512'){
        $this->db->select("	* from f_mutasi_stock_pusat_saldoakhir('$iperiode')",false);#->limit($num,$offset);
      }else{
        $this->db->select("	* from f_mutasi_stock_pusat('$iperiode')",false);#->limit($num,$offset);
      }
		  $query = $this->db->get();

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Export Mutasi Periode:'.$iperiode.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan Mutasi ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 12
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN MUTASI STOK GUDANG REGULER');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,11,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Area :                     Bulan :                    Hal :');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,11,3);
				$objPHPExcel->getActiveSheet()->setCellValue('A6', 'No');
				$objPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B6', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C6', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D6', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E6', 'PENERIMAAN');
				$objPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5,6,6,6);
				$objPHPExcel->getActiveSheet()->setCellValue('F6', 'PENGELUARAN');
				$objPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray(

					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H6', 'Saldo');
				$objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I6', 'Stok');
				$objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J6', '+/-');
				$objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
        $objPHPExcel->getActiveSheet()->setCellValue('A7', 'Urut');
				$objPHPExcel->getActiveSheet()->getStyle('A7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B7', 'Barang');
				$objPHPExcel->getActiveSheet()->getStyle('B7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C7', 'Barang');
				$objPHPExcel->getActiveSheet()->getStyle('C7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D7', 'Awal');
				$objPHPExcel->getActiveSheet()->getStyle('D7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E7', 'd/Pusat');
				$objPHPExcel->getActiveSheet()->getStyle('E7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F7', 'Penj.');
				$objPHPExcel->getActiveSheet()->getStyle('F7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G7', 'KePusat');
				$objPHPExcel->getActiveSheet()->getStyle('G7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H7', 'Akhir');
				$objPHPExcel->getActiveSheet()->getStyle('H7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I7', 'Opnam');
				$objPHPExcel->getActiveSheet()->getStyle('I7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J7', '( pc )');
				$objPHPExcel->getActiveSheet()->getStyle('J7')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN),
							'bottom'=> array('style' => Style_Border::BORDER_THIN),
							'left'  => array('style' => Style_Border::BORDER_THIN),
							'right' => array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i=7;
				$j=7;
				$xarea='';
				$saldo=0;
        $no=0;
				foreach($query->result() as $row){
          $no++;
          $selisih=$row->n_saldo_stockopname-$row->n_saldo_akhir;
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no);
					$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_product);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->n_saldo_awal);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->n_mutasi_bbm);
					$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->n_mutasi_penjualan);
					$objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->n_mutasi_bbk);
					$objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->n_saldo_akhir);
					$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->n_saldo_stockopname);
					$objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $selisih);
					$objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
						array(
							'borders' => array(
								'top' 	=> array('style' => Style_Border::BORDER_THIN),
								'bottom'=> array('style' => Style_Border::BORDER_THIN),
								'left'  => array('style' => Style_Border::BORDER_THIN),
								'right' => array('style' => Style_Border::BORDER_THIN)
							),
						)
					);
					$i++;
					$j++;
				}
				$x=$i-1;
				$objPHPExcel->getActiveSheet()->getStyle('G7:J'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
			}
			$objPHPExcel->getActiveSheet()->getStyle('A6:I6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='SO-'.$iperiode.'.xls';
			$objWriter->save("excel/".$iarea.'/'.$nama);
			$data['sukses'] = true;
			$data['inomor']	= "Laporan Mutasi Stok Reguler";
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
