<?php 
class Cform extends CI_Controller
{
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      require_once("php/fungsi.php");
   }

   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu235')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
      {
         $data['page_title'] = $this->lang->line('exp-ikhp');
         $data['datefrom'] ='';
         $data['dateto']   ='';
         $this->load->view('exp-ikhp/vmainform', $data);
      }
      else
      {
         $this->load->view('awal/index.php');
      }
   }

   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu235')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
      {
         $this->load->model('exp-ikhp/mmaster');
         $iarea      = $this->input->post('iarea');
         $datefrom   = $this->input->post('datefrom');
         $dateto     = $this->input->post('dateto');
         $datefromx  = $datefrom;
         $datetox    = $dateto;
         $que        = $this->db->query(" select distinct(i_cek) as cek from
                                 (
                                   select i_cek from tm_ikhp
                                   where i_area='$iarea' and 
                                          d_bukti >= to_date('$datefromx','dd-mm-yyyy') and
                                          d_bukti <= to_date('$datetox','dd-mm-yyyy') and
                                          (i_cek='' or i_cek isnull)
                                   union all
                                   select i_cek from tm_pelunasan
                                   where i_area='$iarea' and
                                          i_jenis_bayar='02' and
                                          f_pelunasan_cancel='f' and
                                          (i_cek='' or i_cek isnull) and
                                          d_bukti >= to_date('$datefromx','dd-mm-yyyy') and
                                          d_bukti <= to_date('$datetox','dd-mm-yyyy')
                                   union all
                                   select i_cek from tm_pelunasan 
                                   where i_area='$iarea' and
                                          i_jenis_bayar='01' and
                                          f_pelunasan_cancel='f' and
                                          (i_cek='' or i_cek isnull) and
                                          d_bukti >= to_date('$datefromx','dd-mm-yyyy') and
                                          d_bukti <= to_date('$datetox','dd-mm-yyyy')
                                 ) as x"
                              );
         if ($que->num_rows() == 0)
         {
         
            $fromname='';
            if($datefrom!='')
            {
               $tmp=explode("-",$datefrom);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $fromname=$fromname.$hr;
               $datefrom=$th."-".$bl."-".$hr;
               $periodeawal   = $hr." ".mbulan($bl)." ".$th;
            }
            $tmp           = explode("-", $dateto);
            $det           = $tmp[0];
            $mon           = $tmp[1];
            $yir           = $tmp[2];
            $dtos          = $yir."/".$mon."/".$det;
            $fromname      = $fromname.$det;
            $periodeakhir  = $det." ".mbulan($mon)." ".$yir;
            $dtos          = $this->mmaster->dateAdd("d",1,$dtos);
            $tmp           = explode("-", $dtos);
            $det1          = $tmp[2];
            $mon1          = $tmp[1];
            $yir1          = $tmp[0];
            $dtos          = $yir1."-".$mon1."-".$det1;
            $data['page_title'] = $this->lang->line('exp-ikhp');
            $qareaname	= $this->mmaster->eareaname($iarea);
            if($qareaname->num_rows()>0)
            {
               $row_areaname	= $qareaname->row();
               $aname   = $row_areaname->e_area_name;
            }
            else
            {
               $aname   = '';
            }
            
            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("Laporan KHP")
                        ->setDescription(NmPerusahaan);
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
               array(
                  'font'   => array(
                     'name'   => 'Arial',
                     'bold'   => true,
                     'italic' => false,
                     'size'  => 12
                  ),
                  'alignment' => array(
                     'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
                     'vertical'  => Style_Alignment::VERTICAL_CENTER,
                     'wrap'      => true
                  )
               ),
               'A1:A4'
            );
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(11);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(11);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(11);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(11);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);

            $objPHPExcel->getActiveSheet()->setCellValue('A1',NmPerusahaan);
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,11,1);
            $objPHPExcel->getActiveSheet()->setCellValue('A2', 'LAPORAN KHP - '.$aname);
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,11,2);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$periodeawal.' - '.$periodeakhir);
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,11,3);
            $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,5,0,6);
            $objPHPExcel->getActiveSheet()->getStyle('A5:A6')->applyFromArray(
               array(
                  'borders'   => array(
                     'top'       => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Tgl DT');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(1,5,1,6);
            $objPHPExcel->getActiveSheet()->getStyle('B5:B6')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tgl Byr');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(2,5,2,6);
            $objPHPExcel->getActiveSheet()->getStyle('C5:C6')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('D5', 'No Bukti');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(3,5,3,6);
            $objPHPExcel->getActiveSheet()->getStyle('D5:D6')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Uraian');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(4,5,4,6);
            $objPHPExcel->getActiveSheet()->getStyle('E5:E6')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('F5', 'CoA');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(5,5,5,6);
            $objPHPExcel->getActiveSheet()->getStyle('F5:F6')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Penerimaan');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(6,5,7,5);
            $objPHPExcel->getActiveSheet()->getStyle('G5:H5')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
            $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Pengeluaran');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(8,5,9,5);
            $objPHPExcel->getActiveSheet()->getStyle('I5:J5')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Saldo Akhir');
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(10,5,11,5);
            $objPHPExcel->getActiveSheet()->getStyle('K5:L5')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('G6', 'Tunai');
            $objPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('H6', 'Giro');
            $objPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('I6', 'Tunai');
            $objPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('J6', 'Giro');
            $objPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('K6', 'Tunai');
            $objPHPExcel->getActiveSheet()->getStyle('K6')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('l6', 'Giro');
            $objPHPExcel->getActiveSheet()->getStyle('l6')->applyFromArray(
               array(
                  'borders' => array(
                     'top' 	=> array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
               array(
                  'font' => array(
                     'name'	=> 'Arial',
                     'bold'  => false,
                     'italic'=> false,
                     'size'  => 12
                  ),
                  'alignment' => array(
                     'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
                     'vertical'  => Style_Alignment::VERTICAL_CENTER,
                     'wrap'      => true
                  )
               ),
               'A5:L6'
            );
            //$buktitmp='xxx';
            //$namatmp ='yyy';
            //$coatmp  ='zzz';
            $terimatunai= 0;
            $terimagiro	= 0;
            $keluartunai= 0;
            $keluargiro = 0;
            if(!isset($sahirt)) $sahirt   = 0;
            if(!isset($sahirg)) $sahirg   = 0;
            $saldo=$this->mmaster->bacasaldo($iarea,$datefromx,$dtos);
            if($saldo)
            {
               foreach($saldo as $raw)
               {
                  $this->db->select("  sum(v_jumlah)-sum(v_lebih) as v_terima_tunai
                                                from tm_pelunasan a, tr_area c
                                                where 
                                                      a.i_jenis_bayar='02' and
                                                      a.i_area='$iarea' and
                                                      a.i_area=c.i_area and a.f_pelunasan_cancel='f' and
                                                      a.d_bukti > '$raw->d_bukti' and
                                                      a.d_bukti < to_date('$datefromx','dd-mm-yyyy')
                                                group by a.i_area",false);
                  $query = $this->db->get();
                  $cunai=0;
                  if ($query->num_rows() > 0)
                  {
                     foreach($query->result() as $siw)
                     {
                        $cunai=$siw->v_terima_tunai;
                     }
                  }
                  $this->db->select("  sum(v_jumlah)-sum(v_lebih) as v_terima_giro
                                                from tm_pelunasan a, tr_area c
                                                where 
                                                   a.i_jenis_bayar='01' and
                                                   a.i_area='$iarea' and
                                                   a.i_area=c.i_area and a.f_pelunasan_cancel='f' AND
                                                   a.d_bukti > '$raw->d_bukti' AND
                                                   a.d_bukti < to_date('$datefromx','dd-mm-yyyy')
                                                group by a.i_area",false);
                  $querys = $this->db->get();
                  $ghiro=0;
                  if ($querys->num_rows() > 0)
                  {
                     foreach($querys->result() as $siw)
                     {
                        $ghiro=$siw->v_terima_giro;
                     }
                  }
          #####
                  $this->db->select("	sum(v_terima_tunai) as ttunai, sum(v_terima_giro) as tgiro, sum(v_keluar_tunai) as ktunai, 
                                      sum(v_keluar_giro) as kgiro from tm_ikhp where 
	                                    i_area='$iarea' and d_bukti > '$raw->d_bukti' AND d_bukti < to_date('$datefromx','dd-mm-yyyy') 
                                      group by i_area",false);
                  $query = $this->db->get();
                  $kcunai=0;
                  $kgiro=0;
                  if ($query->num_rows() > 0){
                    foreach($query->result() as $siw){
                      $cunai=$cunai+$siw->ttunai;
                      $ghiro=$ghiro+$siw->tgiro;
                      $kcunai=$siw->ktunai;
                      $kgiro=$siw->kgiro;
                    }
                  }
          #####
#                  $sahirt=$raw->v_saldo_akhirtunai+$cunai;
#                  $sahirg=$raw->v_saldo_akhirgiro+$ghiro;
#                  $sawalt=$raw->v_saldo_akhirtunai+$cunai;
#                  $sawalg=$raw->v_saldo_akhirgiro+$ghiro;
###
        $sahirt=($raw->v_saldo_akhirtunai+$cunai)-$kcunai;
		    $sahirg=($raw->v_saldo_akhirgiro+$ghiro)-$kgiro;
		    $sawalt=number_format(($raw->v_saldo_akhirtunai+$cunai)-$kcunai);
		    $sawalg=number_format(($raw->v_saldo_akhirgiro+$ghiro)-$kgiro);
###
               }
            }
            else
            {
               $this->db->select(" sum(v_jumlah)-sum(v_lebih) as v_terima_tunai
                                             from tm_pelunasan a, tr_area c
                                             where 
                                                a.i_jenis_bayar='02' and
                                                      a.i_area='$iarea' and
                                                      a.i_area=c.i_area and a.f_pelunasan_cancel='f' AND
                                                      a.d_bukti > '$raw->d_bukti' AND
                                                      a.d_bukti < to_date('$datefromx','dd-mm-yyyy')
                                             group by a.i_area",false);
               $query = $this->db->get();
               $cunai=0;
               if ($query->num_rows() > 0)
               {
                  foreach($query->result() as $siw)
                  {
                     $cunai=$siw->v_terima_tunai;
                  }
               }
               $this->db->select("  sum(v_jumlah)-sum(v_lebih) as v_terima_giro
                                             from tm_pelunasan a, tr_area c
                                             where 
                                                      a.i_jenis_bayar='01' and
                                                      a.i_area='$iarea' and
                                                      a.i_area=c.i_area and a.f_pelunasan_cancel='f' AND
                                                      a.d_bukti > '$raw->d_bukti' AND
                                                      a.d_bukti < to_date('$datefromx','dd-mm-yyyy')
                                             group by a.i_area",false);
               $querys = $this->db->get();
               $ghiro=0;
               if ($querys->num_rows() > 0)
               {
                  foreach($querys->result() as $siw)
                  {
                     $ghiro=$siw->v_terima_giro;
                  }
               }
               $sahirt=$cunai;
               $sahirg=$ghiro;
               $sawalt=$cunai;
               $sawalg=$ghiro;
            }
            $this->db->select(" * from(
                                          select 
                                             '01' as jenis,
                                             a.d_bukti as d_dt,
                                             a.d_bukti,
                                             a.i_bukti,
                                             b.e_ikhp_typename,
                                             a.i_coa,
                                             sum(a.v_terima_tunai) as v_terima_tunai,
                                             sum(a.v_terima_giro) as v_terima_giro, 
                                             sum(a.v_keluar_tunai) as v_keluar_tunai,
                                             sum(a.v_keluar_giro) as v_keluar_giro,
                                             c.e_area_name,
                                             a.i_area
                                          from tm_ikhp a, tr_ikhp_type b, tr_area c
                                          where
                                             a.i_ikhp_type=b.i_ikhp_type and
                                             a.i_area='$iarea' and
                                             a.i_area=c.i_area and
                                             d_bukti >= to_date('$datefromx','dd-mm-yyyy') AND
                                             d_bukti <= to_date('$datetox','dd-mm-yyyy')
                                          group by
                                             a.d_bukti,
                                             a.i_bukti,
                                             b.e_ikhp_typename,
                                             a.i_coa,
                                             c.e_area_name,
                                             a.i_area
                                          union all
                                          select 
                                             '02' as jenis,
                                             a.d_dt,
                                             a.d_bukti,
                                             substr(a.i_pelunasan,1,7) as i_bukti,
                                             'Hasil Tagihan' as e_ikhp_typename,
                                             '112.2' as i_coa, 
                                             sum(a.v_jumlah) as v_terima_tunai,
                                             0 as v_terima_giro,
                                             0 as v_keluar_tunai,
                                             0 as v_keluar_giro,
                                             c.e_area_name,
                                             a.i_area
                                             from tm_pelunasan a, tr_area c
                                             where 
                                                a.i_jenis_bayar='02' and
                                                a.i_area='$iarea' and
                                                a.i_area=c.i_area and
                                                a.f_pelunasan_cancel='f' and
                                                a.d_bukti >= to_date('$datefromx','dd-mm-yyyy') AND
                                                a.d_bukti <= to_date('$datetox','dd-mm-yyyy')
                                             group by
                                                a.d_dt,
                                                a.d_bukti,
                                                substr(a.i_pelunasan,1,7),
                                                c.e_area_name,
                                                a.i_area
                                             union all
                                             select 
                                                '02' as jenis,
                                                a.d_dt, a.d_bukti,
                                                substr(a.i_pelunasan,1,7) as i_bukti,
                                                'Hasil Tagihan' as e_ikhp_typename,
                                                '112.2' as i_coa, 
                                                0 as v_terima_tunai,
                                                sum(a.v_jumlah) as v_terima_giro,
                                                0 as v_keluar_tunai,
                                                0 as v_keluar_giro,
                                                c.e_area_name,
                                                a.i_area
                                             from tm_pelunasan a, tr_area c
                                             where 
                                                a.i_jenis_bayar='01' and
                                                a.i_area='$iarea' and
                                                a.i_area=c.i_area and
                                                a.f_pelunasan_cancel='f' and
                                                a.d_bukti >= to_date('$datefromx','dd-mm-yyyy') AND
                                                a.d_bukti <= to_date('$datetox','dd-mm-yyyy')
                                             group by
                                                a.d_dt, 
                                                a.d_bukti,
                                                substr(a.i_pelunasan,1,7),
                                                c.e_area_name,
                                                a.i_area
                                       ) as x
                                       order by x.d_dt, x.d_bukti, substr(x.i_bukti,1,7)",false);
            $query = $this->db->get();
            if ($query->num_rows() > 0)
            {
               $i=7;
               $j=7;
               $xarea='';
               $saldo=0;
               $objPHPExcel->getActiveSheet()->getStyle('A7')->applyFromArray(
                  array(
                        'borders' => array(
                           'top'    => array('style' => Style_Border::BORDER_THIN),
                           'bottom' => array('style' => Style_Border::BORDER_THIN),
                           'left'   => array('style' => Style_Border::BORDER_THIN),
                           'right'  => array('style' => Style_Border::BORDER_THIN)
                        )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('B7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('C7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('D7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('E7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('F7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('G7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('H7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('I7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('J7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('K7')->applyFromArray(
                  array(
                     'borders' => array(
                                    'top' 	=> array('style' => Style_Border::BORDER_THIN),
                                    'bottom'=> array('style' => Style_Border::BORDER_THIN),
                                    'left'  => array('style' => Style_Border::BORDER_THIN),
                                    'right' => array('style' => Style_Border::BORDER_THIN)
                                    ),
                     'alignment' => array(
                                    'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT
                                    )
                        )
               );
               $objPHPExcel->getActiveSheet()->getStyle('L7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  'alignment' => array(
                     'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT
                  )
                  )
               );

               $objPHPExcel->getActiveSheet()->setCellValue('C7', $datefromx);
               $objPHPExcel->getActiveSheet()->setCellValue('E7', "Saldo Awal Per ".$periodeawal);
               $objPHPExcel->getActiveSheet()->setCellValue('F7', '111.3'.$iarea);
               $objPHPExcel->getActiveSheet()->setCellValue('K7', $sawalt);
               $objPHPExcel->getActiveSheet()->setCellValue('L7', $sawalg);

               foreach($query->result() as $row)
               {
                  $buktitmp   = substr($row->i_bukti,0,8);
                  if ($row->jenis == '01')
                  { 
                     $bukti = $row->i_bukti;
                  }
                  else $bukti = substr($buktitmp,0,7);
                  
                  $terimatunai= $row->v_terima_tunai;
                  $terimagiro	= $row->v_terima_giro;
                  $keluartunai= $row->v_keluar_tunai;
                  $keluargiro = $row->v_keluar_giro;
                  $sahirt     = $sahirt+$row->v_terima_tunai-$row->v_keluar_tunai;
                  $sahirg     = $sahirg+$row->v_terima_giro-$row->v_keluar_giro;
                  $namatmp    = $row->e_ikhp_typename;
                  $dbuktitmp  = $row->d_bukti;
                  $ddttmp     = $row->d_dt;
                  if(strlen($row->i_coa)==5)
                  {
                     $coatmp  = substr($row->i_coa,0,5).$iarea;
                  }
                  else
                  {
                     $coatmp  = $row->i_coa;
                  }
                  $i++;
                  $j++;
                  //if(!$bukti) $bukti='';

                  $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j-7);
                  $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom' => array('style' => Style_Border::BORDER_THIN),
                              'left'   => array('style' => Style_Border::BORDER_THIN),
                              'right'  => array('style' => Style_Border::BORDER_THIN)
                           ),
                           'alignment' => array(
                              'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT
                           )
                     )
                  );
                  $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $ddttmp);
                  $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top' 	=> array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $dbuktitmp);
                  $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top' 	=> array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $bukti);
                  $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top' 	=> array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $namatmp);
                  $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top' 	=> array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $coatmp);
                  $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top' 	=> array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $terimatunai);
                  $objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top' 	=> array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                           'alignment' => array(
                              'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT
                           )
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $terimagiro);
                  $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top' 	=> array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                           'alignment' => array(
                              'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT
                           )
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $keluartunai);
                  $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top' 	=> array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                           'alignment' => array(
                              'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT
                           )
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $keluargiro);
                  $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top' 	=> array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                           'alignment' => array(
                              'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT
                           )
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $sahirt);
                  $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top' 	=> array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                           'alignment' => array(
                              'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT
                           )
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $sahirg);
                  $objPHPExcel->getActiveSheet()->getStyle('L'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom' => array('style' => Style_Border::BORDER_THIN),
                              'left'   => array('style' => Style_Border::BORDER_THIN),
                              'right'  => array('style' => Style_Border::BORDER_THIN)
                           ),
                           'alignment' => array(
                              'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT
                           )
                     )
                  );
               }
               $x=$i-1;
               $objPHPExcel->getActiveSheet()->getStyle('F7:F'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
            }else{
$i=7;
               $j=7;
               $xarea='';
               $saldo=0;
               $objPHPExcel->getActiveSheet()->getStyle('A7')->applyFromArray(
                  array(
                        'borders' => array(
                           'top'    => array('style' => Style_Border::BORDER_THIN),
                           'bottom' => array('style' => Style_Border::BORDER_THIN),
                           'left'   => array('style' => Style_Border::BORDER_THIN),
                           'right'  => array('style' => Style_Border::BORDER_THIN)
                        )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('B7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('C7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),

                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('D7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('E7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('F7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('G7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('H7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('I7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('J7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('K7')->applyFromArray(
                  array(
                     'borders' => array(
                                    'top' 	=> array('style' => Style_Border::BORDER_THIN),
                                    'bottom'=> array('style' => Style_Border::BORDER_THIN),
                                    'left'  => array('style' => Style_Border::BORDER_THIN),
                                    'right' => array('style' => Style_Border::BORDER_THIN)
                                    ),
                     'alignment' => array(
                                    'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT
                                    )
                        )
               );
               $objPHPExcel->getActiveSheet()->getStyle('L7')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  'alignment' => array(
                     'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT
                  )
                  )
               );

               $objPHPExcel->getActiveSheet()->setCellValue('C7', $datefromx);
               $objPHPExcel->getActiveSheet()->setCellValue('E7', "Saldo Awal Per ".$periodeawal);
               $objPHPExcel->getActiveSheet()->setCellValue('F7', '111.3'.$iarea);
               $objPHPExcel->getActiveSheet()->setCellValue('K7', $sawalt);
               $objPHPExcel->getActiveSheet()->setCellValue('L7', $sawalg);
            }
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $nama='ikhp-'.$iarea.'-'.$fromname.'-'.substr($datefrom,5,2).'-'.substr($datefrom,0,4).'.xls';
            $area=$iarea;
            if($iarea=='22')
               $area='09';
            elseif($iarea=='24') 
               $area='05';
            elseif(($iarea=='20')||($iarea=='26'))
               $area='07';
            elseif($iarea=='06')
               $area='23';
            elseif(($iarea=='14')||($iarea=='15'))
               $area='12';
            elseif($iarea=='21')
               $area='13';
            elseif(($iarea=='30')||($iarea=='32')||($iarea=='18')||($iarea=='19')||($iarea=='29')||($iarea=='28')||($iarea=='27'))
               $area='11';
            if(file_exists('excel/'.$area.'/'.$nama))
            {
               @chmod('excel/'.$area.'/'.$nama, 0777);
               @unlink('excel/'.$area.'/'.$nama);
            }
            $objWriter->save('excel/'.$area.'/'.$nama);
            @chmod('excel/'.$area.'/'.$nama, 0777);
            $data['sukses']   = true;
            $data['inomor']   = "IKHP - ".$area.'/'.$nama;
            $this->load->view('nomor',$data);
         }
         else
         {
            $data['sukses']   = true;
            $data['inomor']   = "IKHP masih ada yg belum di cek oleh pusat !!!!!";

            $sess=$this->session->userdata('session_id');
            $id=$this->session->userdata('user_id');
            $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs		= pg_query($sql);
            if(pg_num_rows($rs)>0){
	            while($row=pg_fetch_assoc($rs)){
		            $ip_address	  = $row['ip_address'];
		            break;
	            }
            }else{
	            $ip_address='kosong';
            }
            $query 	= pg_query("SELECT current_timestamp as c");
            while($row=pg_fetch_assoc($query)){
            	$now	  = $row['c'];
            }
            $pesan='Export IKHP Area:'.$iarea.' tanggal:'.$datefrom.' sampai:'.$dateto;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now , $pesan ); 

            $this->load->view('expikhpada',$data);
         }
      }
      else
      {
         $this->load->view('awal/index.php');
      }
   }
   
   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu235')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
         {
        $config['base_url'] = base_url().'index.php/exp-ikhp/cform/area/index/';
        $iuser   = $this->session->userdata('user_id');
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

        $config['total_rows'] = $query->num_rows(); 
        $config['per_page'] = '10';
        $config['first_link'] = 'Awal';
        $config['last_link'] = 'Akhir';
        $config['next_link'] = 'Selanjutnya';
        $config['prev_link'] = 'Sebelumnya';
        $config['cur_page'] = $this->uri->segment(5);
        $this->pagination->initialize($config);

        $this->load->model('exp-ikhp/mmaster');
        $data['page_title'] = $this->lang->line('list_area');
  			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
        $this->load->view('exp-ikhp/vlistarea', $data);
      }
      else
      {
         $this->load->view('awal/index.php');
      }
   }
   
   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu235')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
         {
          $config['base_url'] = base_url().'index.php/exp-ikhp/cform/area/index/';
          $cari 	= $this->input->post('cari', FALSE);
          $cari	= strtoupper($cari);
          $iuser   = $this->session->userdata('user_id');
          $query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
							          and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
          $config['total_rows'] = $query->num_rows(); 
          $config['per_page'] = '10';
          $config['first_link'] = 'Awal';
          $config['last_link'] = 'Akhir';
          $config['next_link'] = 'Selanjutnya';
          $config['prev_link'] = 'Sebelumnya';
          $config['cur_page'] = $this->uri->segment(5);
          $this->pagination->initialize($config);
          $this->load->model('exp-ikhp/mmaster');
          $data['page_title'] = $this->lang->line('list_area');
    			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
          $this->load->view('exp-ikhp/vlistarea', $data);
      }
      else
      {
         $this->load->view('awal/index.php');
      }
   }
}
?>
