<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$istockopname 	= $this->input->post('istockopname', TRUE);
			$dstockopname 	= $this->input->post('dstockopname', TRUE);
			if($dstockopname!=''){
				$tmp=explode("-",$dstockopname);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dstockopname=$th."-".$bl."-".$hr;
        $thbl=substr($th,2,2).$bl;
			}
			$istore			= $this->input->post('istore', TRUE);
			$iarea			= $this->input->post('iarea', TRUE);
			$istorelocation		= $this->input->post('istorelocation', TRUE);
			$istorelocationbin	= '00';
			$jml			= $this->input->post('jml', TRUE);

			if ((isset($dstockopname) && $dstockopname != '') 
			&& (isset($istore) && $istore != '') 
			&& (isset($istorelocation) && $istorelocation != '')
			&& ($istockopname == ''))
			{
				$benar="false";
				$this->db->trans_begin();
				$this->load->model('stockopname/mmaster');
				$istockopname	=$this->mmaster->runningnumber($iarea,$thbl);
				$this->mmaster->insertheader($istockopname,$dstockopname,$istore,$istorelocation,$iarea);
				for($i=1;$i<=$jml;$i++){
				  $iproduct		= $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= $this->input->post('iproductgrade'.$i, TRUE);
				  $iproductmotif	= $this->input->post('iproductmotif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $nstockopname		= $this->input->post('nstockopname'.$i, TRUE);
				  $nstockopname		= str_replace(',','',$nstockopname);
				  $this->mmaster->insertdetail($iproduct, $iproductgrade, $eproductname, $nstockopname,$istockopname, $istore, 
												                $istorelocation, $istorelocationbin,$iproductmotif,$dstockopname,$iarea,$i);
###########
          $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
          if(isset($trans)){
            foreach($trans as $itrans)
            {
              $q_aw =$itrans->n_quantity_awal;
              $q_ak =$itrans->n_quantity_akhir;
              $q_in =$itrans->n_quantity_in;
              $q_out=$itrans->n_quantity_out;
              break;
            }
          }else{
            $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){              
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_stock;
                $q_ak =$itrans->n_quantity_stock;
                $q_in =0;
                $q_out=0;
                break;
              }
            }else{
              $q_aw=0;
              $q_ak=0;
              $q_in=0;
              $q_out=0;
            }
          }
          $this->mmaster->inserttrans4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$istockopname,$q_in,$q_out,$nstockopname,$q_aw,$q_ak);
#          $th=substr($dstockopname,0,4);
#          $bl=substr($dstockopname,5,2);
#          $emutasiperiode=$th.$bl;
          $emutasiperiode='20'.substr($istockopname,3,4);
          if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
          {
            $this->mmaster->updatemutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nstockopname,$emutasiperiode);
          }else{
            $this->mmaster->insertmutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nstockopname,$emutasiperiode);
          }
          if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
          {
            $this->mmaster->updateic4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nstockopname,$q_ak);
          }else{
            $this->mmaster->insertic4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nstockopname);
          }
###########
				  $benar="true";
				}
				if ( ($this->db->trans_status() === FALSE) || ($benar=="false") )
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();
					$data['sukses']=true;
					$data['inomor']=$istockopname;
					$this->load->view('nomor',$data);
				    
				}
			}
			else
			{
				$this->load->model('stockopname/mmaster');
				$data['page_title'] = $this->lang->line('master_stockopname');
				$data['istockopname']='';
				$data['isi']	="";
				$data['detail']	="";
				$data['jmlitem']="";
				$this->load->view('stockopname/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_stockopname');
			$this->load->view('stockopname/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_stockopname')." update";
			if($this->uri->segment(4)!=''){
				$istockopname		= $this->uri->segment(4);
				$istore 				= $this->uri->segment(5);
				$istorelocation = $this->uri->segment(6);
				$iarea 					= $this->uri->segment(7);
				$dfrom	 				= $this->uri->segment(8);
				$dto		 				= $this->uri->segment(9);
				$query = $this->db->query("	select * from tm_stockopname_item
							   				where i_stockopname = '$istockopname' 
											and i_store = '$istore'
											and i_store_location = '$istorelocation'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['istockopname'] 	= $istockopname;
				$data['istore'] 				= $istore;
				$data['istorelocation'] = $istorelocation;
				$data['iarea'] 					= $iarea;
				$data['dfrom']	 				= $dfrom;
				$data['dto']		 				= $dto;
				$this->load->model('stockopname/mmaster');
				$data['isi']=$this->mmaster->baca($istockopname,$istore,$istorelocation);
				$data['detail']=$this->mmaster->bacadetail($istockopname,$istore,$istorelocation);
		 		$this->load->view('stockopname/vmainform',$data);
			}else{
				$this->load->view('stockopname/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$istockopname 	= $this->input->post('istockopname', TRUE);
			$dstockopname 	= $this->input->post('dstockopname', TRUE);
			if($dstockopname!=''){
				$tmp=explode("-",$dstockopname);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dstockopname=$th."-".$bl."-".$hr;
			}
			$iarea			 	= $this->input->post('iarea', TRUE);
			$istore				= $this->input->post('istore', TRUE);
			$istorelocation		= $this->input->post('istorelocation', TRUE);
			$istorelocationbin	= '00';
			$jml				= $this->input->post('jml', TRUE);
			if ((isset($dstockopname) && $dstockopname != '') && (isset($istore) && $istore != '') && (isset($istorelocation) && $istorelocation != ''))
			{
				$this->db->trans_begin();
				$this->load->model('stockopname/mmaster');
				$this->mmaster->updateheader($istockopname,$dstockopname,$istore,$istorelocation);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			= $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= $this->input->post('iproductgrade'.$i, TRUE);
				  $iproductmotif	= $this->input->post('iproductmotif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $nstockopname		= $this->input->post('nstockopname'.$i, TRUE);
				  $nstockopname	  	= str_replace(',','',$nstockopname);
				  $this->mmaster->deletedetail($iproduct, $iproductgrade, $istockopname, 
							       			   $istore, $istorelocation, $istorelocationbin, $iproductmotif);
				  $this->mmaster->insertdetail($iproduct, $iproductgrade, $eproductname, $nstockopname,
							       			   $istockopname, $istore, $istorelocation, $istorelocationbin,$iproductmotif,$dstockopname,$iarea,$i);
				}
				if ($this->db->trans_status() === FALSE)
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();
					$data['sukses']=true;
				    $data['inomor']=$istockopname;
				    $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$istockopname	= $this->input->post('istockopnamedelete', TRUE);
			$istore			= $this->input->post('istoredelete', TRUE);
			$this->load->model('stockopname/mmaster');
			$this->mmaster->delete($istockopname,$istore);
			$data['page_title'] = $this->lang->line('master_stockopname');
			$data['istockopname']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('stockopname/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$istockopname			= $this->uri->segment(4);
			$iproductmotif		= $this->uri->segment(5);
			$iproduct					= $this->uri->segment(6);
			$iproductgrade		= $this->uri->segment(7);
			$istore						= $this->uri->segment(8);
			$istorelocation		= $this->uri->segment(9);
			$istorelocationbin= $this->uri->segment(10);

			$iarea	 					= $this->uri->segment(11);
			$dfrom		 				= $this->uri->segment(12);
			$dto			 				= $this->uri->segment(13);
			$data['iarea'] 		= $iarea;
			$data['dfrom']		= $dfrom;
			$data['dto']			= $dto;

			$this->load->model('stockopname/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $istockopname, 
					       			   	 $istore, $istorelocation, $istorelocationbin, $iproductmotif);
			$query = $this->db->query("	select * from tm_stockopname_item
														 			where i_stockopname = '$istockopname' 
																	and i_store = '$istore'
																	and i_store_location = '$istorelocation'");
			$data['jmlitem'] = $query->num_rows(); 				
			$data['istockopname'] = $istockopname;
			$data['istore'] = $istore;
			$data['istorelocation'] = $istorelocation;
			$this->load->model('stockopname/mmaster');
			$data['isi']=$this->mmaster->baca($istockopname,$istore,$istorelocation);
			$data['detail']=$this->mmaster->bacadetail($istockopname,$istore,$istorelocation);
			$data['page_title'] = $this->lang->line('master_stockopname');
			$this->load->view('stockopname/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['istore']=$this->uri->segment(5);
			$istore=$this->uri->segment(5);
			$data['istorelocation']=$this->uri->segment(6);
			$istorelocation=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/stockopname/cform/product/'.$baris.'/'.$istore.'/'.$istorelocation.'/index/';
			$query = $this->db->query(" select a.*,b.e_product_motifname from tm_ic a, tr_product_motif b
																	where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
																	and a.i_store='$istore' and a.i_store_location='$istorelocation'
																	and a.i_product_motif=b.i_product_motif
																	and a.i_product=b.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('stockopname/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($cari,$config['per_page'],$this->uri->segment(8),$istore,$istorelocation);
			$this->load->view('stockopname/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['istore']=$this->uri->segment(5);
			$istore=$this->uri->segment(5);
			$data['istorelocation']=$this->uri->segment(6);
			$istorelocation=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/stockopname/cform/product/'.$baris.'/'.$istore.'/'.$istorelocation.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select a.*,b.e_product_motifname from tm_ic a, tr_product_motif b
										where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
										and a.i_store='$istore' and a.i_store_location='$istorelocation'
										and a.i_product_motif=b.i_product_motif
										and a.i_product=b.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('stockopname/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(8),$istore,$istorelocation);
			$data['baris']=$baris;
			$this->load->view('stockopname/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/stockopname/cform/store/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$query = $this->db->query("	select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
									  	where a.i_store = b.i_store and b.i_store=c.i_store
										and (c.i_area = '$area1' or c.i_area = '$area2' or
											 c.i_area = '$area3' or c.i_area = '$area4' or
											 c.i_area = '$area5')");
			$config['total_rows'] = $query->num_rows(); 	
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('stockopname/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('stockopname/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/stockopname/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$query = $this->db->query("select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
						   where a.i_store=b.i_store and b.i_store=c.i_store
						     and(  upper(a.i_store) like '%$cari%' 
						      or upper(a.e_store_name) like '%$cari%'
						      or upper(b.i_store_location) like '%$cari%'
						      or upper(b.e_store_locationname) like '%$cari%')
							and (c.i_area = '$area1' or c.i_area = '$area2' or
								 c.i_area = '$area3' or c.i_area = '$area4' or
								 c.i_area = '$area5')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('stockopname/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('stockopname/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/stockopname/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select * from tm_stockopname
						   where upper(a.i_stockopname) like '%cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('stockopname/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_stockopname');
			$data['istockopname']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('stockopname/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['istore']=$this->uri->segment(5);
			$istore=$this->uri->segment(5);
			$data['istorelocation']=$this->uri->segment(6);
			$istorelocation=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/stockopname/cform/productupdate/'.$baris.'/'.$istore.'/'.$istorelocation.'/index/';
			$query = $this->db->query(" select a.*,b.e_product_motifname from tm_ic a, tr_product_motif b
										where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
										and a.i_store='$istore' and a.i_store_location='$istorelocation'
										and a.i_product_motif=b.i_product_motif
										and a.i_product=b.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('stockopname/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($cari,$config['per_page'],$this->uri->segment(8),$istore,$istorelocation);
			$this->load->view('stockopname/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu7')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['istore']=$this->uri->segment(5);
			$istore=$this->uri->segment(5);
			$data['istorelocation']=$this->uri->segment(6);
			$istorelocation=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/stockopname/cform/productupdate/'.$baris.'/'.$istore.'/'.$istorelocation.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select a.*,b.e_product_motifname from tm_ic a, tr_product_motif b
										where (upper(a.i_product) like '%$cari%' or upper(a.e_product_name) like '%$cari%')
										and a.i_store='$istore' and a.i_store_location='$istorelocation'
										and a.i_product_motif=b.i_product_motif
										and a.i_product=b.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('stockopname/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(8),$istore,$istorelocation);
			$data['baris']=$baris;
			$this->load->view('stockopname/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
