<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu8')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispmb 		= $this->input->post('ispmb', TRUE);
			$ispmbold 	= $this->input->post('ispmbold', TRUE);
			$dspmb 		= $this->input->post('dspmb', TRUE);
			if($dspmb!=''){
				$tmp=explode("-",$dspmb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspmb=$th."-".$bl."-".$hr;
        $thbl=substr($th,2,2).$bl;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$eareaname= $this->input->post('eareaname', TRUE);
			$eremark	= $this->input->post('eremark', TRUE);
			$jml		= $this->input->post('jml', TRUE);
			$fop		= 'f';
			$nprint		= 0;
			if($dspmb!='' && $eareaname!='')
			{
				$this->db->trans_begin();
				$this->load->model('spmb/mmaster');
				$ispmb	=$this->mmaster->runningnumber($thbl);
				$this->mmaster->insertheader($ispmb, $dspmb, $iarea, $fop, $nprint, $ispmbold, $eremark);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			=$this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	='A';
				  $iproductmotif	=$this->input->post('motif'.$i, TRUE);
				  $eproductname		=$this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		=$this->input->post('vproductmill'.$i, TRUE);
				  $vunitprice		=str_replace(',','',$vunitprice);
				  $norder			=$this->input->post('norder'.$i, TRUE);
				  $eremark			=$this->input->post('eremark'.$i, TRUE);
				  if($norder>0){
				    $this->mmaster->insertdetail( $ispmb,$iproduct,$iproductgrade,$eproductname,$norder,$vunitprice,$iproductmotif,$eremark,$iarea);
				  }
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input SPMB Area '.$iarea.' No:'.$ispmb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ispmb;
					$this->load->view('nomor',$data);
				}
			}else{
				$data['page_title'] = $this->lang->line('spmb');
				$data['ispmb']='';
				$this->load->model('spmb/mmaster');
				$data['isi']='';//$this->mmaster->bacasemua();
				$data['detail']="";
				$data['jmlitem']="";
        $data['tgl']=date('d-m-Y');
				$this->load->view('spmb/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu8')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('spmb');
			$this->load->view('spmb/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu8')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('spmb')." update";
			if($this->uri->segment(4)!=''){
				$ispmb = $this->uri->segment(4);
				$iarea = $this->uri->segment(5);
				$dfrom= $this->uri->segment(6);
				$dto 	= $this->uri->segment(7);
				$data['ispmb'] = $ispmb;
				$data['iarea'] = $iarea;
				$data['dfrom'] = $dfrom;
				$data['dto']	 = $dto;

				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ispmb'");
			
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('spmb/mmaster');
				$data['isi']=$this->mmaster->baca($ispmb);
				$data['detail']=$this->mmaster->bacadetail($ispmb);
		 		$this->load->view('spmb/vmainform',$data);
			}else{
				$this->load->view('spmb/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu8')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispmb 		= $this->input->post('ispmb', TRUE);
			$ispmbold 	= $this->input->post('ispmbold', TRUE);
			$dspmb 		= $this->input->post('dspmb', TRUE);
      $eremark	= $this->input->post('eremark', TRUE);
			if($dspmb!=''){
				$tmp=explode("-",$dspmb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dspmb=$th."-".$bl."-".$hr;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$eareaname	= $this->input->post('eareaname', TRUE);
			$jml		= $this->input->post('jml', TRUE);
			$fop		= 'f';
			$nprint		= 0;
			if($dspmb!='' && $eareaname!='')
			{
				$this->db->trans_begin();
				$this->load->model('spmb/mmaster');
				$this->mmaster->updateheader($ispmb, $dspmb, $iarea, $ispmbold, $eremark);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			=$this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	='A';
				  $iproductmotif	=$this->input->post('motif'.$i, TRUE);
				  $eproductname		=$this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		=$this->input->post('vproductmill'.$i, TRUE);
				  $vunitprice		=str_replace(',','',$vunitprice);
				  $norder			=$this->input->post('norder'.$i, TRUE);
				  $eremark			=$this->input->post('eremark'.$i, TRUE);
				  $this->mmaster->deletedetail( $iproduct,$iproductgrade,$ispmb,$iproductmotif);
				  if($norder>0){
				    $this->mmaster->insertdetail( $ispmb,$iproduct,$iproductgrade,$eproductname,$norder,$vunitprice,$iproductmotif,$eremark,$iarea);
				  }
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
				    $this->db->trans_rollback();
				}else{
				    $this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update SPMB Area '.$iarea.' No:'.$ispmb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ispmb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu8')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispmb	= $this->input->post('ispmbdelete', TRUE);
			$this->load->model('spmb/mmaster');
			$this->mmaster->delete($ispmb);

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Hapus SPMB No:'.$ispmb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('spmb');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('spmb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$ispmb			= $this->input->post('ispmbdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
  			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$this->db->trans_begin();
			$this->load->model('spmb/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $ispmb, $iproductmotif);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Hapus Detail SPMB No:'.$ispmb;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

				$data['page_title'] = $this->lang->line('spmb')." Update";
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ispmb'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['ispmb'] = $ispmb;
				$data['isi']=$this->mmaster->baca($ispmb);
				$data['detail']=$this->mmaster->bacadetail($ispmb);
				$this->load->view('spmb/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/spmb/cform/product/'.$baris.'/index/';
			$query = $this->db->query(" select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_mill as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('spmb/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/spmb/cform/product/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_mill as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
										and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') "
									  ,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('spmb/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu8')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/spmb/cform/area/index/';
			$query = $this->db->query("select * from tr_area ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('spmb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu8')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/spmb/cform/area/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_area
						   where upper(i_area) like '%$cari%' 
						      or upper(e_area_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('spmb/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('spmb/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu8')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/spmb/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('spmb/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['ispmb']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('spmb/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
