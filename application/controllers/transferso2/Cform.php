<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->helper('directory');
		$this->load->helper('file');
    $this->load->library('excel');
    require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu224')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transferso');
			$store	= $this->session->userdata('store');
      if($store=='AA') $store='00';
			$data['isi']= directory_map('./so/'.$store);
			$data['file']='';
			$this->load->view('transferso/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function load()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu224')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$file= $this->input->post('namafile', TRUE);
			$tgl = $this->input->post('tgl', TRUE);
			$store	= $this->session->userdata('store');
      if($store=='AA') $store='00';
      $fileName = 'Import-'.date('Ym-D');
                  $ext4='.xls';
                  $inputFileName = 'so/'.$store.'/'.$file;
                  $data['filename'] = $fileName.$ext4;
                  $objPHPExcel = IOFactory::load($inputFileName);
                  
                  //$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                  $sheet = $objPHPExcel->getSheet(0);
                  $hrow = $sheet->getHighestRow();
                  $aray = array();
                  for ($n=2; $n<=$hrow; $n++){
                    $aray[] = array(
                    'KODEPROD'      => $objPHPExcel->getActiveSheet()->getCell('A'.$n)->getCalculatedValue(),
                    'NAMAPROD'      => $objPHPExcel->getActiveSheet()->getCell('B'.$n)->getCalculatedValue(),
                    'STOCKOPNAM'    => $objPHPExcel->getActiveSheet()->getCell('C'.$n)->getCalculatedValue(),
                    'GRADE'         => $objPHPExcel->getActiveSheet()->getCell('D'.$n)->getCalculatedValue(),
                    );
                  }
                  $data['items'] = $aray;
                  $data['file']=$file;

			//$data['file']='so/'.$store.'/'.$file;
			$data['tgl'] =$tgl;
			$this->load->view('transferso/vfile', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu224')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $filespb = $this->input->post('fileso', TRUE);
      $tglso = $this->input->post('tglso', TRUE);
      $this->load->model('transferso/mmaster');
      $this->db->trans_begin();
                  $istorelocationbin = '00';
                  $iarea =substr($filespb,2,2);
                  if ($iarea =='00'){
                    $istorelocation = '01';
                    $istore = 'AA';
                  }else{
                    $istore =  $iarea;
                    $istorelocation = '00';
                  }
                  $per = substr($filespb,4,6);
                  $dstockopname   = $this->input->post('tglso', TRUE);
                  if($dstockopname!=''){
                  $tmp=explode("-",$dstockopname);
                  $th=$tmp[2];
                  $bl=$tmp[1];
                  $hr=$tmp[0];
                  $dstockopname=$th."-".$bl."-".$hr;
                  $thbl=substr($th,2,2).$bl;
                  }
                  $istockopname =$this->mmaster->runningnumber($iarea,$thbl);
                  //var_dump($istockopname,$dstockopname,$istore,$istorelocation,$iarea);
                  $this->mmaster->insertheader($istockopname,$dstockopname,$istore,$istorelocation,$iarea);
                  //$store='00';
                  //if($store=='AA') $store='00';
                  $fileName = 'Import-'.date('Ym-D');
                  $ext4='.xls';
                  $inputFileName = 'so/'.$iarea.'/'.$filespb;
                  //echo $inputFileName;
                  $data['filename'] = $fileName.$ext4;
                  $objPHPExcel = IOFactory::load($inputFileName);
                  
                  //$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
                  $sheet = $objPHPExcel->getSheet(0);
                  $hrow = $sheet->getHighestRow();
                  $hcol = $sheet->getHighestColumn();
                  $aray = array();
                  for ($n=2; $n<=$hrow; $n++){
                    $aray = array(
                    'i_stockopname'       => $istockopname,
                    'd_stockopname'       => $dstockopname,
                    'i_store'             => $istore,
                    'i_store_location'    => $istorelocation,
                    'i_store_locationbin' => $istorelocationbin,
                    'i_product'           => $objPHPExcel->getActiveSheet()->getCell('A'.$n)->getCalculatedValue(),
                    'e_product_name'      => $objPHPExcel->getActiveSheet()->getCell('B'.$n)->getCalculatedValue(),
                    'n_stockopname'       => $objPHPExcel->getActiveSheet()->getCell('C'.$n)->getCalculatedValue(),
                    'i_product_grade'     => $objPHPExcel->getActiveSheet()->getCell('D'.$n)->getCalculatedValue(),
                    'i_product_motif'     =>  '00',  
                    'i_area'              => $iarea,
                    'e_mutasi_periode'    => $per,
                    'n_item_no'           => $n-1
                    ); 
                  
                  
                  if ($aray <> null){
                    //echo "Masuk Pak Eko";
                    //var_dump($aray);
                    $insert = $this->db->insert("tm_stockopname_item",$aray);
                  }
                  $iproduct         = $objPHPExcel->getActiveSheet()->getCell('A'.$n)->getCalculatedValue();
                  $eproductname     = $objPHPExcel->getActiveSheet()->getCell('B'.$n)->getCalculatedValue();
                  $nstockopname     = $objPHPExcel->getActiveSheet()->getCell('C'.$n)->getCalculatedValue();
                  $iproductgrade    = $objPHPExcel->getActiveSheet()->getCell('D'.$n)->getCalculatedValue();
                  $iproductmotif  =  '00';
              //var_dump($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
               $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
        if(isset($trans)){
          foreach($trans as $itrans)
          {
          $q_aw =$itrans->n_quantity_awal;
          $q_ak =$itrans->n_quantity_akhir;
          $q_in =$itrans->n_quantity_in;
          $q_out=$itrans->n_quantity_out;
          break;
          }
        }else{
          //var_dump($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
          $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
          if(isset($trans)){              
          foreach($trans as $itrans)
          {
            $q_aw =$itrans->n_quantity_stock;
            $q_ak =$itrans->n_quantity_stock;
            $q_in =0;
            $q_out=0;
            break;
          }
          }else{
          $q_aw=0;
          $q_ak=0;
          $q_in=0;
          $q_out=0;
          }
        }
        $emutasiperiode='20'.substr($istockopname,3,4);
        //var_dump($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode);
        if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
        {
          $this->mmaster->updatemutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nstockopname,$emutasiperiode);
        }else{
          $this->mmaster->insertmutasi4x($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nstockopname,$emutasiperiode);
        }

        }
                  //$data['items'] = $aray;
                //var_dump($aray);
                  /*$insert1 = array(
                 'i_stockopname'      => $istockopname,
                 'd_stockopname'       => $dstockopname,
                 'i_store'             => $istore,
                 'i_store_location'    => $istorelocation,
                 'i_store_locationbin' => $istorelocationbin,
                 'i_product'           => $rowData[1][0],
                 'i_product_grade'     => $rowData[1][3],
                 'e_product_name'      => $rowData[1][1],
                 'i_product_motif'     =>  '00',
                 'n_stockopname'       =>  $rowData[1][2],
                 'i_area'              => $iarea,
                 'e_mutasi_periode'    => $per,
                 'n_item_no'           => $row-1
                    
                 
                );*/
        //var_dump($data['items'] = $aray);
      /*$this->db->trans_begin();
      if($db1){
        $record_numbers1=dbase_numrecords($db1);
        $noitem=0;
        $iarea =substr($filespb,8,2);
		    $istore	= $this->session->userdata('store');
        if($istore=='AA') $istorelocation='01'; else $istorelocation='00';
		    $istorelocationbin	= '00';
        for($j=1;$j<=$record_numbers1;$j++){
          $noitem++;
          $col=dbase_get_record_with_names($db1,$j);
          $iproduct		  = substr($col['KODEPROD'],0,7);
          $que=$this->db->query("select e_product_name from tr_product where i_product='$iproduct'", false);
          if($que->num_rows()>0){
        		$tes=$que->row();
            $eproductname=$tes->e_product_name;
          }
      	 else 
      	 {
      	 	$eproductname='';
      	 }    
/*
          $que=$this->db->query("select i_product_grade from tm_ic where i_product='$iproduct' and i_store='$istore' 
                                 and i_store_location='$istorelocation'", false);
          if($que->num_rows()>0){
        		$tes=$que->row();
            $iproductgrade=$tes->i_product_grade;
          }else{
            $que=$this->db->query("select i_product_a from tr_product_ab where i_product_a='$iproduct'", false);
            if($que->num_rows()>0){
              $iproductgrade='A';
            }else{
              $iproductgrade='B';
            }
          }
*/

			if ( ($this->db->trans_status() === FALSE) )
			{
				$this->db->trans_rollback();
				$this->load->view('transferso/vformgagal',$data);
			}else{
				$this->db->trans_commit();
#				$this->db->trans_rollback();
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
          $data['inomor'] = $istockopname;
					$pesan='Transfer SO Area '.$iarea.' No:'.$istockopname;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );
				$this->load->view('transferso/vformsukses',$data);
			}			
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
