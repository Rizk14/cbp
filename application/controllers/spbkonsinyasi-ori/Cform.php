<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('spbkonsinyasi');
			$data['iperiode']='';
			$this->load->view('spbkonsinyasi/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iperiode		= $this->input->post('iperiode');
			if($iperiode=='') $iperiode	= $this->uri->segment(4);
			$this->load->model('spbkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('spbkonsinyasi');
			$data['iperiode']		= $iperiode;
      $data['diskon']     = $this->mmaster->bacadiskon($iperiode);
			$data['isi']		    = $this->mmaster->bacaperiode($iperiode);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Buka Rekap SPB Konsinyasi Periode: '.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('spbkonsinyasi/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('spbkonsinyasi');
			$this->load->view('spbkonsinyasi/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu442')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml  = $this->input->post('jml', TRUE);
			$iperiode= $this->input->post('iperiode', TRUE);
			$this->db->trans_begin();
      $dspb=date('Y-m-d');
      $tmp=explode("-",$dspb);
      $th=$tmp[0];
      $bl=$tmp[1];
      $dt=$tmp[2];
      $thbl=substr($th,2,2).$bl;
      $ispb  = '';
      $ispbx = '';
			$this->load->model('spbkonsinyasi/mmaster');
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$iarea     = $this->input->post('iarea'.$i, TRUE);
					$ecustomername= $this->input->post('ecustomername'.$i, TRUE);
					$icustomer= $this->input->post('icustomer'.$i, TRUE);
          $awalnama=substr($ecustomername,0,5);
          if($awalnama=='GRIYA' or $awalnama=='SUSAN' or $awalnama=='ASIA '){
            $namagrup='YOG';
          }elseif($awalnama=='TIARA'){
            $namagrup='CLA';
          }else{
            $namagrup=substr($ecustomername,0,3);
          }
          $que=$this->db->query(" select a.i_area, a.i_product, a.e_product_name, a.v_unit_price, a.i_product_grade,
                                  a.i_product_motif, sum(a.n_quantity) as quantity,
                                  b.i_cek, b.f_spb_rekap
                                  from tm_notapb_item a, tm_notapb b
                                  where a.i_customer='$icustomer'
                                  and to_char(a.d_notapb::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                                  and not b.i_cek is null and b.f_spb_rekap='f' and b.f_notapb_cancel='f'
                                  and a.i_notapb=b.i_notapb and a.i_customer=b.i_customer
                                  group by a.i_area, a.i_product,a.e_product_name, a.v_unit_price, a.i_product_grade, a.i_product_motif,
                                  b.i_cek, b.f_spb_rekap
                                  order by a.i_product", false);
          if($que->num_rows()>0){
            $x=0;
            $j=0;
            $nomorspb='';
            foreach($que->result() as $row){
              $j++;
              $iproduct=$row->i_product;
              $eproductname=$row->e_product_name;
              $iproductmotif=$row->i_product_motif;
              $iproductgrade=$row->i_product_grade;
              $harga=$row->v_unit_price;
              $quantity=$row->quantity;
              $length = count($row);
#              for($j=0;$j<=$length;$j++){
              if( ($ispb=='' && $ispbx=='') || ($j%21==0)) {
                $x=0;
                $ispb=$this->mmaster->runningnumberspb($thbl,$namagrup,$iarea);
                $this->mmaster->insertheader($ispb, $dspb, $iarea, $icustomer);
                $this->mmaster->updatebon($icustomer,$iperiode,$ispb);
                if($nomorspb==''){
                  $nomorspb=$ispb;
                }else{
                  $nomorspb=$nomorspb.' - '.$ispb;
                }
              }
              $x++;
#              $this->mmaster->insertdetail($ispb,$iarea,$icustomer,$iperiode);
              $this->mmaster->insertdetail($ispb,$iarea,$iproduct,$eproductname,$iproductgrade,$iproductmotif,$harga,$quantity,$x);
					    $this->mmaster->updatespb($ispb,$iarea);
              $ispbx=$ispb;
 #             }
            }
          }
				}
			}

			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
#				$this->db->trans_rollback();
				$this->db->trans_commit();
  
        $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		=  $this->db->query($sql);
		    if($rs->num_rows>0){
			    foreach($rs->result() as $tes){
				    $ip_address	  = $tes->ip_address;
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }

		    $data['user']	= $this->session->userdata('user_id');
		    $data['host']	= $ip_address;
		    $data['uri']	= $this->session->userdata('printeruri');
		    $query 	= pg_query("SELECT current_timestamp as c");
		    while($row=pg_fetch_assoc($query)){
			    $now	  = $row['c'];
		    }
		    $pesan='Input SPB Konsinyasi (rekap-bon) No:'.$ispb;
		    $this->load->model('logger');
		    $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']			= true;
				$data['inomor']			= $nomorspb;
				$this->load->view('nomor',$data);

			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
