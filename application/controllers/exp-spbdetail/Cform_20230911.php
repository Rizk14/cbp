<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu524')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exp-spbdetail');
			$data['dfrom']	= '';
			$data['dto']	= '';
			$this->load->view('exp-spbdetail/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
		function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu524')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-spbdetail/cform/area/index/';
			$iuser	= $this->session->userdata('user_id');
			$allarea= $this->session->userdata('allarea');

			if($allarea=='t'){
				$query = $this->db->query(" select * from tr_area order by i_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-spbdetail/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('exp-spbdetail/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
		function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu524')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
            $allarea = $this->session->userdata('allarea');
            $iuser   = $this->session->userdata('user_id');
			$config['base_url'] = base_url().'index.php/exp-spbdetail/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($allarea=='t'){
				$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area",false);
			}else{
				$query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-spbdetail/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('exp-spbdetail/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu524')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-spbdetail/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			if($dfrom!=''){
				$tmp=explode("-",$dfrom);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dfrom  =$th."-".$bl."-".$hr;
			}
			if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto  =$th."-".$bl."-".$hr;
			}
      $iarea = $this->input->post('iarea');
  	  if($iarea=='') $iarea=$this->uri->segment(5);
  	  //update penambahan area nasional 26-01-2017 by efan
  	  if ($iarea == 'NS'){
  	  		/*$this->db->select("data.*, data.v_unit_price*data.n_order as totspb, data.n_deliver*data.v_unit_price as totnota
							from( select a.i_spb, a.i_sj, a.i_nota, a.i_customer, d.e_customer_name, a.f_spb_cancel, 
							b.i_product, c.e_product_name, b.n_order, b.n_deliver, b.v_unit_price
							from tm_spb a, tm_spb_item b, tr_product c, tr_customer d
							where a.i_spb=b.i_spb and a.i_area = b.i_area and a.f_spb_cancel = 'f' and a.d_spb>='$dfrom' and a.d_spb<='$dto' 
							and b.i_product=c.i_product and a.i_customer=d.i_customer 
							)as data",false);*/
  	  		$this->db->select(" data.*, data.v_unit_price*data.n_order as totspb, data.n_deliver*data.v_unit_price as totnota, 
									((data.v_unit_price*data.n_order)/NULLIF(data.v_spb,0))*data.v_spb_discounttotal as diskonSPB,
									((data.v_unit_price*data.n_deliver)/data.v_nota_gross)*data.v_spb_discounttotalafter as diskonSJ,
									(data.n_deliver*data.v_unit_price)-(((data.v_unit_price*data.n_deliver)/data.v_nota_gross)*data.v_spb_discounttotalafter)  as bersihsj 
								from( select a.i_area, a.i_spb, a.d_spb, f.e_salesman_name, a.i_sj, a.d_sj, a.i_nota, a.i_customer, d.e_customer_name, a.f_spb_cancel, 
								b.i_product, c.e_product_name, b.n_order, b.n_deliver, b.v_unit_price,a.v_spb, a.v_spb_discounttotal,a.v_spb_after,
								a.v_spb_discounttotalafter, e.v_nota_gross, a.f_spb_stockdaerah
								from tm_spb a left join  tm_nota e on(a.i_spb = e.i_spb and a.i_nota = e.i_nota), tm_spb_item b, tr_product c, tr_customer d, tr_salesman f
								where a.i_spb=b.i_spb and a.i_area = b.i_area 
								and a.d_spb>='$dfrom' and a.d_spb<='$dto'
								and b.i_product=c.i_product and a.i_customer=d.i_customer
								and a.i_salesman = f.i_salesman
								)as data",false);
  	  }else {
  	  		/*$this->db->select("data.*, data.v_unit_price*data.n_order as totspb, data.n_deliver*data.v_unit_price as totnota
							from( select a.i_spb, a.i_sj, a.i_nota, a.i_customer, d.e_customer_name, a.f_spb_cancel, 
							b.i_product, c.e_product_name, b.n_order, b.n_deliver, b.v_unit_price
							from tm_spb a, tm_spb_item b, tr_product c, tr_customer d
							where a.i_spb=b.i_spb and a.i_area = b.i_area and a.f_spb_cancel = 'f' and a.d_spb>='$dfrom' and a.d_spb<='$dto' and a.i_area='$iarea' 
							and b.i_product=c.i_product and a.i_customer=d.i_customer 
							)as data",false);*/
  	  		/*$this->db->select(" data.*, data.v_unit_price*data.n_order as totspb, data.n_deliver*data.v_unit_price as totnota, 
  	  							((data.v_unit_price*data.n_order)/NULLIF(data.v_spb,0))*data.v_spb_discounttotal as diskonspb,
								((data.v_unit_price*data.n_deliver)/data.v_nota_gross)*data.v_spb_discounttotalafter as diskonsj,
								(data.n_deliver*data.v_unit_price)-(((data.v_unit_price*data.n_deliver)/data.v_nota_gross)*data.v_spb_discounttotalafter)  as bersihsj 
							from( select a.i_area, a.i_spb, f.e_salesman_name, a.i_sj, a.i_nota, a.i_customer, d.e_customer_name, a.f_spb_cancel, 
							b.i_product, c.e_product_name, b.n_order, b.n_deliver, b.v_unit_price,a.v_spb, a.v_spb_discounttotal,a.v_spb_after, a.v_spb_discounttotalafter, e.v_nota_gross
							from tm_spb a left join  tm_nota e on(a.i_spb = e.i_spb and a.i_nota = e.i_nota), tm_spb_item b, tr_product c, tr_customer d, tr_salesman f
							where a.i_spb=b.i_spb and a.i_area = b.i_area 
							and  a.d_spb>='$dfrom' and a.d_spb<='$dto' and a.i_area='$iarea' 
							and b.i_product=c.i_product and a.i_customer=d.i_customer
							and a.i_salesman = f.i_salesman
							)as data",false);*/
			$this->db->select(" data.*, data.v_unit_price*data.n_order as totspb, data.n_deliver*data.v_unit_price as totnota, 
  	  							((data.v_unit_price*data.n_order)/NULLIF(data.v_spb,0))*data.v_spb_discounttotal as diskonspb,
								((data.v_unit_price*data.n_deliver)/data.v_nota_gross)*data.v_spb_discounttotalafter as diskonsj,
								(data.n_deliver*data.v_unit_price)-(((data.v_unit_price*data.n_deliver)/data.v_nota_gross)*data.v_spb_discounttotalafter)  as bersihsj 
							from( select a.i_area, a.i_spb,a.d_spb, f.e_salesman_name, a.i_sj, a.d_sj , a.i_nota, a.i_customer, d.e_customer_name, a.f_spb_cancel, 
							b.i_product, c.e_product_name, b.n_order, b.n_deliver, b.v_unit_price,a.v_spb, a.v_spb_discounttotal,a.v_spb_after, 
							a.v_spb_discounttotalafter, e.v_nota_gross, a.f_spb_stockdaerah
							from tm_spb a left join  tm_nota e on(a.i_spb = e.i_spb and a.i_nota = e.i_nota), tm_spb_item b, tr_product c, tr_customer d, tr_salesman f
							where a.i_spb=b.i_spb and a.i_area = b.i_area 
							and  a.d_spb>='$dfrom' and a.d_spb<='$dto' and a.i_area='$iarea' 
							and b.i_product=c.i_product and a.i_customer=d.i_customer
							and a.i_salesman = f.i_salesman
							)as data",false);
  	  }
		  
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar SPB")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar SPB');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,19,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Awal Tanggal : '.$dfrom);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,19,3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:S5'
				);
				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Kode Area');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);	
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Nama Salesman');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);	
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'No SPB');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);				
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'No Surat Jalan');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'No Nota');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Nama Toko');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			    $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Kode Produk');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Nama Produk');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			    $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Qty Spb');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Qty SJ');
				$objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K5', 'Total SPB');
				$objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L5', 'Total SJ');
				$objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M5', 'Status Cancel');
				$objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N5', 'Diskon SPB');
				$objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O5', 'Diskon SJ');
				$objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P5', 'Nilai Bersih SJ');
				$objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Tanggal Spb');
				$objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('R5', 'Tanggal SJ');
				$objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('S5', 'Pemenuhan');
				$objPHPExcel->getActiveSheet()->getStyle('S5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$i=6;
				$j=6;
        		$no=0;
				foreach($query->result() as $row)
				{
          		$no++;
          		$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  	array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':S'.$i
				  );  
				  
				if($row->f_spb_stockdaerah == 't'){
					$pemenuhan = 'DAERAH';
				}else{
					$pemenuhan = 'PUSAT';
				}  

				if($row->f_spb_cancel == 't'){
					$stat = 'Batal';
				}else{
					$stat = 'Aktif';
				}  
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->e_salesman_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_spb, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_sj, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->i_product, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->e_product_name, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->n_order, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->n_deliver, Cell_DataType::TYPE_NUMERIC);
		  $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->totspb, Cell_DataType::TYPE_NUMERIC);
		  $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$i, $row->totnota, Cell_DataType::TYPE_NUMERIC);
		  $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $stat, Cell_DataType::TYPE_STRING);
		  $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$i, $row->diskonspb, Cell_DataType::TYPE_NUMERIC);
		  $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $row->diskonsj, Cell_DataType::TYPE_NUMERIC);
		  $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $row->bersihsj, Cell_DataType::TYPE_NUMERIC);
		  $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$i, $row->d_spb, Cell_DataType::TYPE_STRING);
		  $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->d_sj, Cell_DataType::TYPE_STRING);
		  $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $pemenuhan, Cell_DataType::TYPE_STRING);
					$i++;
					$j++;
				}
				$x=$i-1;
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      	$nama='SPB Detail'.$dfrom.'-'.$dto.'.xls';
			$objWriter->save('excel/00/'.$nama); 

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
      }
      $pesan='Export Daftar SPB Detail:'.$nama;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['sukses'] = true;
			$data['inomor']	= "Export Daftar SPB Detail ".$dfrom;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
