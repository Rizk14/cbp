<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu23')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_citytype');
			$data['icitytype']='';
			$this->load->model('citytype/mmaster');
			$data['isi']=$this->mmaster->bacasemua();

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master Jenis Kota";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('citytype/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu23')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icitytype 	= $this->input->post('icitytype', TRUE);
			$ecitytypename 	= $this->input->post('ecitytypename', TRUE);

			if ((isset($icitytype) && $icitytype != '') && (isset($ecitytypename) && $ecitytypename != ''))
			{
				$this->load->model('citytype/mmaster');
				$this->mmaster->insert($icitytype,$ecitytypename);

		        $sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
			        while($row=pg_fetch_assoc($rs)){
				        $ip_address	  = $row['ip_address'];
				        break;
			        }
		        }else{
			        $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
			        $now	  = $row['c'];
		        }
		        $pesan='Input Jenis Kota:('.$icitytype.')-'.$ecitytypename;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now , $pesan );

		        $data['page_title'] = $this->lang->line('master_citytype');
				$data['icitytype']='';
				$this->load->model('citytype/mmaster');
				$data['isi']=$this->mmaster->bacasemua();
				$this->load->view('citytype/vmainform', $data);

			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu23')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_citytype');
			$this->load->view('citytype/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu23')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_citytype')." update";
			if($this->uri->segment(4)){
				$icitytype = $this->uri->segment(4);
				$data['icitytype'] = $icitytype;
				$this->load->model('citytype/mmaster');
				$data['isi']=$this->mmaster->baca($icitytype);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master Jenis Kota:('.$icitytype.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);
		        
		 		$this->load->view('citytype/vmainform',$data);
			}else{
				$this->load->view('citytype/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu23')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icitytype	= $this->input->post('icitytype', TRUE);
			$ecitytypename 	= $this->input->post('ecitytypename', TRUE);
			$this->load->model('citytype/mmaster');
			$this->mmaster->update($icitytype,$ecitytypename);

		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
		        while($row=pg_fetch_assoc($rs)){
			        $ip_address	  = $row['ip_address'];
			        break;
		        }
		      }else{
		        $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
		        $now	  = $row['c'];
		      }
		      $pesan='Update Jenis Kota:('.$icitytype.')-'.$ecitytypename;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );

				$data['page_title'] = $this->lang->line('master_citytype');
				$data['icitytype']='';
				$this->load->model('citytype/mmaster');
				$data['isi']=$this->mmaster->bacasemua();
				$this->load->view('citytype/vmainform', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu23')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icitytype	= $this->uri->segment(4);
			$this->load->model('citytype/mmaster');
			$this->mmaster->delete($icitytype);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
        while($row=pg_fetch_assoc($rs)){
	        $ip_address	  = $row['ip_address'];
	        break;
        }
      }else{
        $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
        $now	  = $row['c'];
      }
      $pesan='Delete Jenis Kota:('.$icitytype.')-'.$ecitytypename;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('master_citytype');
			$data['icitytype']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('citytype/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
