<?php 
class Cform extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->library('paginationxx');
        $this->load->library('fungsi');
    }
    public function index()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu391') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $data['page_title'] = 'Mutasi '.$this->lang->line('hpp');
            $data['iperiode'] = '';
            $this->load->view('listmutasihpp/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function view()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu391') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $iperiode = $this->input->post('iperiode');
            if ($iperiode == '') {
                $iperiode = $this->uri->segment(4);
            }

            $this->load->model('listmutasihpp/mmaster');
            $data['page_title'] = $this->lang->line('hpp');
            $data['iperiode'] = $iperiode;
            $data['isi'] = $this->mmaster->bacaperiode($iperiode);
            $sess = $this->session->userdata('session_id');
            $id = $this->session->userdata('user_id');
            $sql = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs = pg_query($sql);
            if (pg_num_rows($rs) > 0) {
                while ($row = pg_fetch_assoc($rs)) {
                    $ip_address = $row['ip_address'];
                    break;
                }
            } else {
                $ip_address = 'kosong';
            }
            $query = pg_query("SELECT current_timestamp as c");
            while ($row = pg_fetch_assoc($query)) {
                $now = $row['c'];
            }
            $pesan = 'Lihat mutasi HPP Periode:' . $iperiode;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now, $pesan);

            $this->load->view('listmutasihpp/vmainform', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
    public function insert_fail()
    {
        if (
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('menu391') == 't')) ||
            (($this->session->userdata('logged_in')) &&
                ($this->session->userdata('allmenu') == 't'))
        ) {
            $data['page_title'] = $this->lang->line('hpp');
            $this->load->view('listmutasihpp/vinsert_fail', $data);
        } else {
            $this->load->view('awal/index.php');
        }
    }
}
