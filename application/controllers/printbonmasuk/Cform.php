<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->model('printbonmasuk/mmaster');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu482')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['ibm']='';
			$data['detail']='';
      $cari = strtoupper($this->input->post('cari', FALSE));
      if($cari==''){
      $config['base_url'] = base_url().'index.php/printbonmasuk/cform/index/oranglucu';
      }else{
      $config['base_url'] = base_url().'index.php/printbonmasuk/cform/index/'.$cari.'/';
      }
			$query = $this->db->query("select * from tm_bm where f_bm_cancel ='f' and (upper(i_bm) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printbonmasuk/mmaster');
			$data['page_title']   = $this->lang->line('printbonmasuk');
  		$data['isi']          = $this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printbonmasuk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu482')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ibm  = $this->uri->segment(4);
			$this->load->model('printbonmasuk/mmaster');
			$data['ibm']=$ibm;
			$data['page_title'] = $this->lang->line('printbonmasuk');
			$data['isi']=$this->mmaster->bacaheader($ibm);
			$data['detail']=$this->mmaster->bacadetail($ibm);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
/*	    print_r ($data['isi']);
	    echo '<br><br><br>';
	    print_r ($data['detail']);
	    die;*/
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak Bon Masuk No:'.$ibm;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printbonmasuk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}?>
