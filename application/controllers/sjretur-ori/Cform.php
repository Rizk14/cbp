<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isjold	= $this->input->post('isjold', TRUE);
			$dsj 	= $this->input->post('dsj', TRUE);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$eareaname	= $this->input->post('eareaname', TRUE);
			$vspbnetto=$this->input->post('vsj', TRUE);
			$vspbnetto= str_replace(',','',$vspbnetto);
			$jml	  = $this->input->post('jml', TRUE);
			if($dsj!='' && $eareaname!='')
			{
				$gaono=true;
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					if($cek=='on'){
						$gaono=false;
					}
					if(!$gaono) break;
				}
				if(!$gaono){
					$this->db->trans_begin();
					$this->load->model('sjretur/mmaster');
					$istore	  			= $this->input->post('istore', TRUE);
					if($istore=='AA'){
						$istorelocation		= '01';
					}else{
						$istorelocation		= '00';
					}
					$istorelocationbin	= '00';
					$isj		 		= $this->mmaster->runningnumbersj($iarea,$thbl);
					$this->mmaster->insertsjheader($isj,$dsj,$iarea,$vspbnetto,$isjold);
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
						if($cek=='on'){
						  $iproduct			= $this->input->post('iproduct'.$i, TRUE);
						  $iproductgrade= 'A';
						  $iproductmotif= $this->input->post('motif'.$i, TRUE);
						  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
						  $vunitprice		= str_replace(',','',$vunitprice);
						  $nretur 			= $this->input->post('nretur'.$i, TRUE);
						  $nretur		  	= str_replace(',','',$nretur);
						  $nreceive	  	= $this->input->post('nreceive'.$i, TRUE);
						  $nreceive		  = str_replace(',','',$nreceive);
						  $eremark  		= $this->input->post('eremark'.$i, TRUE);
						  if($eremark=='')$eremark=null;
						  if($nretur>0){
							$this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$nreceive,$nretur,
										                          $vunitprice,$isj,$dsj,$iarea,$istore,$istorelocation,
                                              $istorelocationbin,$eremark,$i,$i);
#***************#
              $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_awal;
                  $q_ak =$itrans->n_quantity_akhir;
                  $q_in =$itrans->n_quantity_in;
                  $q_out=$itrans->n_quantity_out;
                  break;
                }
              }else{
                $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                if(isset($trans)){
                  foreach($trans as $itrans)
                  {
                    $q_aw =$itrans->n_quantity_stock;
                    $q_ak =$itrans->n_quantity_stock;
                    $q_in =0;
                    $q_out=0;
                    break;
                  }
                }else{
                  $q_aw=0;
                  $q_ak=0;
                  $q_in=0;
                  $q_out=0;
                }
              }
              $this->mmaster->inserttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$nretur,$q_aw,$q_ak);
              $th=substr($dsj,0,4);
              $bl=substr($dsj,5,2);
              $emutasiperiode=$th.$bl;
              $ada=$this->mmaster->cekmutasi2($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode);
              if($ada=='ada')
              {
                $this->mmaster->updatemutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nretur,$emutasiperiode);
              }else{
                $this->mmaster->insertmutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nretur,$emutasiperiode,$q_aw,$q_ak);
              }
              if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
              {
                $this->mmaster->updateic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nretur,$q_ak);
              }else{
                $this->mmaster->insertic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nretur);
              }
#***************#
###############
						  }
						}
					}
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
#						$this->db->trans_rollback();
						$this->db->trans_commit();
						$sess=$this->session->userdata('session_id');
						$id=$this->session->userdata('user_id');
						$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
						$rs		= pg_query($sql);
						if(pg_num_rows($rs)>0){
							while($row=pg_fetch_assoc($rs)){
								$ip_address	  = $row['ip_address'];
								break;
							}
						}else{
							$ip_address='kosong';
						}
						$query 	= pg_query("SELECT current_timestamp as c");
						while($row=pg_fetch_assoc($query)){
							$now	  = $row['c'];
						}
						$pesan='Input SJ Retur No:'.$isj;
						$this->load->model('logger');
						$this->logger->write($id, $ip_address, $now , $pesan );
						$data['sukses']			= true;
						$data['inomor']			= $isj;
						$this->load->view('nomor',$data);
					}
				}
			}else{
				$this->load->model('sjretur/mmaster');
				$data['page_title'] = $this->lang->line('sjr');
				$data['isjr']				= '';
				$data['isi']				= '';#$this->mmaster->bacasemua();
				$data['detail']			= "";
				$data['jmlitem']		= "";
				$data['dsjr']				= date('Y-m-d');
				$data['iarea']			= '';
				$data['istore']			= '';
				$data['isjold']			= '';
				$data['eareaname']	= $eareaname;
				$this->load->view('sjretur/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjr');
			$this->load->view('sjretur/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			($this->session->userdata('logged_in'))
		){
			$data['page_title'] = $this->lang->line('sjr')." update";
			if($this->uri->segment(4)!=''){
				$isjr	= $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$dfrom= $this->uri->segment(6);
				$dto 	= $this->uri->segment(7);
				$data['isjr'] 	= $isjr;
				$data['iarea']= $iarea;
				$data['dfrom']= $dfrom;
				$data['dto']	= $dto;
				$query 	= $this->db->query("select * from tm_sjr_item where i_sjr = '$isjr' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('sjretur/mmaster');
				$data['isi']=$this->mmaster->baca($isjr,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isjr,$iarea);
		 		$this->load->view('sjretur/vmainform',$data);
			}else{
				$this->load->view('sjretur/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			($this->session->userdata('logged_in'))
		){
			$isj	= $this->input->post('isj', TRUE);
			$isjold	= $this->input->post('isjold', TRUE);
			$dsj 	= $this->input->post('dsj', TRUE);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
				$thbl	= substr($th,2,2).$bl;
				$tmpsj	= explode("-",$isj);
				$firstsj= $tmpsj[0];
				$lastsj	= $tmpsj[2];
				$newsj	= $firstsj."-".$thbl."-".$lastsj;				
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$eareaname	= $this->input->post('eareaname', TRUE);
			$vspbnetto= $this->input->post('vsj', TRUE);
			$vspbnetto= str_replace(',','',$vspbnetto);
			$jml	  = $this->input->post('jml', TRUE);
			$gaono=true;
      $gablas=true;
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$gaono=false;
				}
				if(!$gaono) break;
			}
			if(!$gaono){
				$this->db->trans_begin();
				$this->load->model('sjretur/mmaster');
				$istore	  			= $this->input->post('istore', TRUE);
				if($istore=='AA'){
					$istorelocation		= '01';
				}else{
					$istorelocation		= '00';
				}
				$istorelocationbin	= '00';
#				$Qseachsjdaer	= $this->mmaster->searchsjheader($isj,$iarea);
#				$nserachsjdaer	= $Qseachsjdaer->num_rows();
				$this->mmaster->updatesjheader($isj,$iarea,$isjold,$dsj,$vspbnetto);
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					$iproduct		= $this->input->post('iproduct'.$i, TRUE);
					$iproductgrade	= 'A';
					$iproductmotif	= $this->input->post('motif'.$i, TRUE);
					$nretur		= $this->input->post('nretur'.$i, TRUE);
					$nretur		= str_replace(',','',$nretur);
          $nreceive	= $this->input->post('nreceive'.$i, TRUE);
					$nreceive	= str_replace(',','',$nreceive);
          $nasal  	= $this->input->post('nasal'.$i, TRUE);
					$nasal  	= str_replace(',','',$nasal);

					$this->mmaster->deletesjdetail( $isj, $iarea, $iproduct, $iproductgrade, $iproductmotif);

					$th=substr($dsj,0,4);
					$bl=substr($dsj,5,2);
					$emutasiperiode=$th.$bl;
          $tra=$this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj);
				  $this->mmaster->updatemutasi01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nasal,$emutasiperiode);
				  $this->mmaster->updateic01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nasal);
					if($cek=='on'){
					  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
					  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
					  $vunitprice		= str_replace(',','',$vunitprice);
					  $nreceive		 	= $this->input->post('nreceive'.$i, TRUE);
					  $nreceive		  = str_replace(',','',$nreceive);
					  $nretur		  	= $this->input->post('nretur'.$i, TRUE);
					  $nretur			  = str_replace(',','',$nretur);
					  $eremark  		= $this->input->post('eremark'.$i, TRUE);
					  if($eremark=='')$eremark=null;
					  if($nretur>0){
              $gablas=false;
						  $this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$nreceive,$nretur,
                                             $vunitprice,$isj,$dsj,$iarea,$istore,$istorelocation,
                                             $istorelocationbin,$eremark,$i);                      
					    $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
					    if(isset($trans)){
						    foreach($trans as $itrans)
						    {
						      $q_aw =$itrans->n_quantity_awal;
						      $q_ak =$itrans->n_quantity_akhir;
						      $q_in =$itrans->n_quantity_in;
						      $q_out=$itrans->n_quantity_out;
						      break;
						    }
					    }else{
						    $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
						    if(isset($trans)){
						      foreach($trans as $itrans)
						      {
							      $q_aw =$itrans->n_quantity_stock;
							      $q_ak =$itrans->n_quantity_stock;
							      $q_in =0;
							      $q_out=0;
							      break;
						      }
						    }else{
						      $q_aw=0;
						      $q_ak=0;
						      $q_in=0;
						      $q_out=0;
						    }
					    }
					    $this->mmaster->inserttrans1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$nretur,$q_aw,$q_ak,$tra);
					    $th=substr($dsj,0,4);
					    $bl=substr($dsj,5,2);
					    $emutasiperiode=$th.$bl;
					    $ada=$this->mmaster->cekmutasi2($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode);
					    if($ada=='ada')
					    {
						    $this->mmaster->updatemutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nretur,$emutasiperiode);
					    }else{
						    $this->mmaster->insertmutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nretur,$emutasiperiode,$q_aw,$q_ak);
					    }
					    if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
					    {
						    $this->mmaster->updateic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nretur,$q_ak);
					    }else{
						    $this->mmaster->insertic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nretur);
					    }
					  }
					}
				}				
				if ( ($this->db->trans_status() === FALSE) || ($gablas) )
				{
					$this->db->trans_rollback();
				}else{
#					$this->db->trans_rollback();
					$this->db->trans_commit();
						$sess=$this->session->userdata('session_id');
						$id=$this->session->userdata('user_id');
						$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
						$rs		= pg_query($sql);
						if(pg_num_rows($rs)>0){
							while($row=pg_fetch_assoc($rs)){
								$ip_address	  = $row['ip_address'];
								break;
							}
						}else{
							$ip_address='kosong';
						}
						$query 	= pg_query("SELECT current_timestamp as c");
						while($row=pg_fetch_assoc($query)){
							$now	  = $row['c'];
						}
						$pesan='Edit SJ Retur No:'.$isj;
						$this->load->model('logger');
						$this->logger->write($id, $ip_address, $now , $pesan );
					$data['sukses']			= true;
					$data['inomor']			= $newsj;
					$this->load->view('nomor',$data);
				}
			}
    }
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj	= $this->input->post('isjdelete', TRUE);
			$this->load->model('sjretur/mmaster');
			$this->mmaster->delete($isj);
			$data['page_title'] = $this->lang->line('sjr');
			$data['isj']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('sjretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj			= $this->input->post('isjdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
  			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$this->db->trans_begin();
			$this->load->model('sjretur/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $isj, $iproductmotif);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
				$data['page_title'] = $this->lang->line('sjr')." Update";
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$isj'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['isj'] = $isj;
				$data['isi']=$this->mmaster->baca($isj);
				$data['detail']=$this->mmaster->bacadetail($isj);
				$this->load->view('sjretur/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
/*
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['spmb']=$this->uri->segment(5);
			$spmb=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/sjretur/cform/product/'.$baris.'/'.$spmb.'/';
			$query = $this->db->query(" 	select a.i_product as kode, a.i_product_motif as motif,
							a.e_product_motifname as namamotif, 
							c.e_product_name as nama,c.v_product_mill as harga
							
							from tr_product_motif a,tr_product c, tm_spmb_item b
							
							where a.i_product=c.i_product 
							and b.i_product_motif=a.i_product_motif
							and c.i_product=b.i_product and b.i_spmb='$spmb' order by a.e_product_motifname asc ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('sjretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($spmb,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$data['spmb']=$spmb;
			$this->load->view('sjretur/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/sjretur/cform/product/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_mill as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
										and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc "
									  ,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('sjretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('sjretur/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
*/

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/sjretur/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    group by a.i_store, b.e_store_name",false);
			}else{
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                    or a.i_area = '$area4' or a.i_area = '$area5')
                                    group by a.i_store, b.e_store_name",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('sjretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('sjretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/sjretur/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query(" select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store and (upper(a.i_area) like '%$cari%' or upper(a.e_area_name) like '%$cari%')
                                    group by a.i_store, b.e_store_name",false);
			}else{
				$query = $this->db->query("select distinct on (a.i_store) a.i_store, b.e_store_name 
                                    from tr_area a, tr_store b 
                                    where a.i_store=b.i_store 
                                    and (a.i_area = '$area1' or a.i_area = '$area2' or a.i_area = '$area3'
                                    or a.i_area = '$area4' or a.i_area = '$area5')
                                    and (upper(a.i_area) like '%$cari%' or upper(a.e_area_name) like '%$cari%')
                                    group by a.i_store, b.e_store_name
                                    order by a.i_store, b.e_store_name",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sjretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('sjretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function spmb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/sjretur/cform/spmb/'.$area.'/';
			$query = $this->db->query(" select distinct(a.i_spmb) from tm_spmb a, tm_spmb_item b
							where a.i_area='$area' and a.f_spmb_cancel='f' and a.f_spmb_pemenuhan='t'
							and a.i_spmb=b.i_spmb and a.i_area=b.i_area and a.f_spmb_acc='t'
							and b.n_acc>b.n_deliver and a.f_spmb_close='f'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('sjretur/mmaster');
			$data['page_title'] = $this->lang->line('list_spmb');
			$data['area']=$area;
			$data['isi']=$this->mmaster->bacaspmb($area,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sjretur/vlistspmb', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carispmb()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area = $this->input->post('iarea', FALSE);
			$config['base_url'] = base_url().'index.php/sjretur/cform/spmb/'.$area.'/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" select distinct(a.*) from tm_spmb a, tm_spmb_item b
								where a.i_area='$area' and a.f_spmb_cancel='f' and a.f_spmb_close='f'
								and a.i_spmb=b.i_spmb and a.i_area=b.i_area and a.f_spmb_acc='t' and a.f_spmb_pemenuhan='t'
								and b.n_acc>b.n_deliver and (upper(a.i_spmb)like '%$cari%') ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sjretur/mmaster');
			$data['area']=$area;
			$data['page_title'] = $this->lang->line('list_spmb');
			$data['isi']=$this->mmaster->carispmb($cari,$area,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sjretur/vlistspmb', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/sjretur/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('sjretur/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['isj']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('sjretur/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function hitung()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ispmb		= $this->uri->segment(4);
			$query = $this->db->query("	select a.i_product as kode, a.i_product_motif as motif,
							                    a.e_product_motifname as namamotif, b.n_order as order,
							                    c.e_product_name as nama,c.v_product_retail as harga
							                    from tr_product_motif a,tr_product_price c, tm_spmb_item b
							                    where a.i_product=c.i_product 
							                    and b.i_product_motif=a.i_product_motif
							                    and c.i_product=b.i_product
                                  and c.i_price_group='00'
                                  and c.i_product_grade='A'
							                    and b.i_spmb='$ispmb' and b.n_deliver<b.n_acc order by b.n_item_no ",false);
			$data['jmlitem'] = $query->num_rows(); 
			$dsj 	= $this->uri->segment(8);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
			}
			$ispmb		  = $this->uri->segment(4);
			$dspmb		  = $this->uri->segment(5);
			$iarea		  = $this->uri->segment(6);
			$eareaname	= $this->uri->segment(7);
			$istore		  = $this->uri->segment(9);
			$isjold		  = $this->uri->segment(10);
			$data['page_title'] = $this->lang->line('sjr');
			$data['isjr']	= '';
			$this->load->model('sjretur/mmaster');
			$data['isi']	= "xxxxx";
			$data['dsjr']	= $dsj;
			$data['ispmb']	= $ispmb;
			$data['dspmb']	= $dspmb;
			$data['iarea']	= $iarea;
			$data['istore']	= $istore;
			$data['isjold']	= $isjold;
			$data['eareaname']= $eareaname;
			$data['detail']	= $this->mmaster->product($ispmb);
			$this->load->view('sjretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu208')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari = strtoupper($this->input->post('cari', FALSE));
      $baris = strtoupper($this->input->post('baris', FALSE));
      $istore = strtoupper($this->input->post('istore', FALSE));
      $istorelocation = strtoupper($this->input->post('istorelocation', FALSE));
 			if($baris=='') $baris=$this->uri->segment(4);
 			if($istore=='') $istore=$this->uri->segment(5);
 			if($istorelocation=='') $istorelocation=$this->uri->segment(6);
      if($this->uri->segment(7)!='x01'){
        if($cari=='') $cari=$this->uri->segment(7);
        $config['base_url'] = base_url().'index.php/sjretur/cform/product/'.$baris.'/'.$istore.'/'.$istorelocation.'/'.$cari.'/';
      }else{
        $config['base_url'] = base_url().'index.php/sjretur/cform/product/'.$baris.'/'.$istore.'/'.$istorelocation.'/x01/';
      }
      $area1	= $this->session->userdata('i_area');
      $query = $this->db->query("select a.i_product as kode, b.e_product_name as nama, b.v_product_retail as harga, a.n_quantity_stock as stok,
                                  a.i_product_motif as motif, c.e_product_motifname as namamotif
                                  from tm_ic a, tr_product b, tr_product_motif c
                                  where a.i_product=b.i_product and b.i_product=c.i_product
                                  and a.i_product_motif=c.i_product_motif and i_store='$istore' and i_store_location='$istorelocation'
                                  and (upper(a.i_product) like '%$cari%' or upper(b.e_product_name) like '%$cari%')
                                  order by b.e_product_name",false);
                                  
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
 			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('sjretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$this->uri->segment(4);
			$data['istore']=$istore;
			$data['istorelocation']=$istorelocation;
 			$data['isi']=$this->mmaster->bacaproduct($istore,$istorelocation,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('sjretur/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
