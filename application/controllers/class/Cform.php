<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu11')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/class/cform/index/';
			$cari=$this->input->post('cari');
			$cari=strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_class
										where upper(i_product_class) like '%$cari%' 
										or upper(e_product_classname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('master_class');
			$data['iclass']='';
			$this->load->model('class/mmaster');
			$data['isi']=$this->mmaster->bacasemua($config['per_page'],$this->uri->segment(4));
			$this->load->view('class/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu11')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iclass 	= $this->input->post('iclass', TRUE);
			$eclassname 	= $this->input->post('eclassname', TRUE);
			$dclassregister = $this->input->post('dclassregister', TRUE);

			if ($iclass != '' && $eclassname != '')
			{
				$this->load->model('class/mmaster');
				$this->mmaster->insert($iclass,$eclassname,$dclassregister);

        $sess=$this->session->userdata('session_id');
        $id=$this->session->userdata('user_id');
        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
        $rs		= pg_query($sql);
        if(pg_num_rows($rs)>0){
          while($row=pg_fetch_assoc($rs)){
	          $ip_address	  = $row['ip_address'];
	          break;
          }
        }else{
          $ip_address='kosong';
        }
        $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
          $now	  = $row['c'];
        }
        $pesan='Input Kelas Pelanggan:('.$iclass.')-'.$eclassname;
        $this->load->model('logger');
        $this->logger->write($id, $ip_address, $now , $pesan );

			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu11')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_class');
			$this->load->view('class/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu11')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_class')." update";
			if($this->uri->segment(4)){
				$iclass = $this->uri->segment(4);
				$data['iclass'] = $iclass;
				$this->load->model('class/mmaster');
				$data['isi']=$this->mmaster->baca($iclass);
		 		$this->load->view('class/vmainform',$data);
			}else{
				$this->load->view('class/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu11')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iclass			= $this->input->post('iclass', TRUE);
			$eclassname 	= $this->input->post('eclassname', TRUE);
			$dclassregister = $this->input->post('dclassregister', TRUE);
			$this->load->model('class/mmaster');
			$this->mmaster->update($iclass,$eclassname,$dclassregister);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
        while($row=pg_fetch_assoc($rs)){
          $ip_address	  = $row['ip_address'];
          break;
        }
      }else{
        $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
        $now	  = $row['c'];
      }
      $pesan='Update Kelas Pelanggan:('.$iclass.')-'.$eclassname;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu11')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iclass		= $this->uri->segment(4);
			$this->load->model('class/mmaster');
			$this->mmaster->delete($iclass);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
        while($row=pg_fetch_assoc($rs)){
          $ip_address	  = $row['ip_address'];
          break;
        }
      }else{
        $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
        $now	  = $row['c'];
      }
      $pesan='Delete Kelas Pelanggan:('.$iclass.')-'.$eclassname;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$config['base_url'] = base_url().'index.php/class/cform/index/';
			$cari=$this->input->post('cari');
			$cari=strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_class
										where upper(i_product_class) like '%$cari%' 
										or upper(e_product_classname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('master_class');
			$data['iclass']='';
			$data['isi']=$this->mmaster->bacasemua($config['per_page'],$this->uri->segment(4));
			$this->load->view('class/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu11')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/class/cform/index/';
			$cari=$this->input->post('cari');
			$cari=strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_class
										where upper(i_product_class) like '%$cari%' 
										or upper(e_product_classname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('class/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_class');
			$data['iclass']='';
	 		$this->load->view('class/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
