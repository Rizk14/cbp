<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listikhp');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listikhp/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$this->load->model('listikhp/mmaster');
			$data['page_title'] = $this->lang->line('listikhp');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea']	= $iarea;
		//	$data['saldo']	= $this->mmaster->bacasaldo($iarea,$dfrom,$dto);
			$data['start_skrg']	= $this->uri->segment(10);
			
			if ($data['start_skrg'] == '') {
				// jika halaman ke-1, maka saldo awal = bacasaldo

				$saldoawal	= $this->mmaster->bacasaldo($iarea,$dfrom,$dto);
				
				if ($saldoawal) {
					// foreach saldo
					foreach($saldoawal as $cek1){
					$this->db->select("		sum(v_jumlah) as v_terima_tunai
																from tm_pelunasan a, tr_area c
																where 
																		a.i_jenis_bayar='02' and
																		a.i_area='$iarea' and
																		a.i_area=c.i_area and
																		a.d_bukti > '$cek1->d_bukti' AND
																		a.d_bukti < to_date('$dfrom','dd-mm-yyyy')
																group by a.i_area",false);
					$query = $this->db->get();
					$cunai=0;
					if ($query->num_rows() > 0){
						foreach($query->result() as $siw){
							$cunai=$siw->v_terima_tunai;
						}
					}
					$this->db->select("		sum(v_jumlah) as v_terima_giro
																from tm_pelunasan a, tr_area c
																where 
																	a.i_jenis_bayar='01' and
																	a.i_area='$iarea' and
																	a.i_area=c.i_area and
																	a.d_bukti > '$cek1->d_bukti' AND
																	a.d_bukti < to_date('$dfrom','dd-mm-yyyy')
																group by a.i_area",false);
					$querys = $this->db->get();
					$ghiro=0;
					if ($querys->num_rows() > 0){
						foreach($querys->result() as $siw){
							$ghiro=$siw->v_terima_giro;
						}
					}
					$sahirt=$cek1->v_saldo_akhirtunai+$cunai;
					$sahirg=$cek1->v_saldo_akhirgiro+$ghiro;
				//	$sawalt=number_format($raw->v_saldo_akhirtunai+$cunai);
				//	$sawalg=number_format($raw->v_saldo_akhirgiro+$ghiro); 
					
					$sawalt=$cek1->v_saldo_akhirtunai+$cunai;
					$sawalg=$cek1->v_saldo_akhirgiro+$ghiro;
					
				  } // end foreach
				  //echo $sahirt." ".$sahirg."<br>"; die();
			   }
			   else {
					$this->db->select("		sum(v_jumlah) as v_terima_tunai
														from tm_pelunasan a, tr_area c
														where 
		    													a.i_jenis_bayar='02' and
																	a.i_area='$iarea' and
																	a.i_area=c.i_area and
																	a.d_bukti > '$cek1->d_bukti' AND
																	a.d_bukti < to_date('$dfrom','dd-mm-yyyy')
														group by a.i_area",false);
					$query = $this->db->get();
					$cunai=0;
					if ($query->num_rows() > 0){
						foreach($query->result() as $siw){
							$cunai=$siw->v_terima_tunai;
						}
					}
					$this->db->select("		sum(v_jumlah) as v_terima_giro
																from tm_pelunasan a, tr_area c
																where 
																			a.i_jenis_bayar='01' and
																			a.i_area='$iarea' and
																			a.i_area=c.i_area and
																			a.d_bukti > '$cek1->d_bukti' AND
																			a.d_bukti < to_date('$dfrom','dd-mm-yyyy')
																group by a.i_area",false);
					$querys = $this->db->get();
					$ghiro=0;
					if ($querys->num_rows() > 0){
						foreach($querys->result() as $siw){
							$ghiro=$siw->v_terima_giro;
						}
					}
					$sahirt=$cunai;
					$sahirg=$ghiro;
				//	$sawalt=number_format($cunai);
				//	$sawalg=number_format($ghiro);
					
					$sawalt=$cunai;
					$sawalg=$ghiro;
			   } // end if saldoawal
			//---------------
				$data['sahirt'] = $sahirt;
				$data['sahirg'] = $sahirg;
				$data['sawalt'] = $sawalt;
				$data['sawalg'] = $sawalg;
				
				//echo $sahirt." ".$sahirg."<br>"; die();
			} // end hal 1
			else {
				// hal 2 (11 s/d 20): saldo awal = saldoawal hal 1 - history page 1
				// cek 1 halaman sebelumnya
				//$batas = ($this->uri->segment(10))-10;
				//$sahirt = $this->uri->segment(7);
				//$sahirg = $this->uri->segment(8);
				
				// hitung saldonya dari awal
				// ===================================================================
				$saldoawal	= $this->mmaster->bacasaldo($iarea,$dfrom,$dto);
				
				if ($saldoawal) {
					// foreach saldo
					foreach($saldoawal as $cek1){
					$this->db->select("		sum(v_jumlah) as v_terima_tunai
																from tm_pelunasan a, tr_area c
																where 
																		a.i_jenis_bayar='02' and
																		a.i_area='$iarea' and
																		a.i_area=c.i_area and
																		a.d_bukti > '$cek1->d_bukti' AND
																		a.d_bukti < to_date('$dfrom','dd-mm-yyyy')
																group by a.i_area",false);
					$query = $this->db->get();
					$cunai=0;
					if ($query->num_rows() > 0){
						foreach($query->result() as $siw){
							$cunai=$siw->v_terima_tunai;
						}
					}
					$this->db->select("		sum(v_jumlah) as v_terima_giro
																from tm_pelunasan a, tr_area c
																where 
																	a.i_jenis_bayar='01' and
																	a.i_area='$iarea' and
																	a.i_area=c.i_area and
																	a.d_bukti > '$cek1->d_bukti' AND
																	a.d_bukti < to_date('$dfrom','dd-mm-yyyy')
																group by a.i_area",false);
					$querys = $this->db->get();
					$ghiro=0;
					if ($querys->num_rows() > 0){
						foreach($querys->result() as $siw){
							$ghiro=$siw->v_terima_giro;
						}
					}
					$sahirt=$cek1->v_saldo_akhirtunai+$cunai;
					$sahirg=$cek1->v_saldo_akhirgiro+$ghiro;
				//	$sawalt=number_format($raw->v_saldo_akhirtunai+$cunai);
				//	$sawalg=number_format($raw->v_saldo_akhirgiro+$ghiro); 
					
					$sawalt=$cek1->v_saldo_akhirtunai+$cunai;
					$sawalg=$cek1->v_saldo_akhirgiro+$ghiro;
					
				  } // end foreach
			   }
			   else {
					$this->db->select("		sum(v_jumlah) as v_terima_tunai
														from tm_pelunasan a, tr_area c
														where 
		    													a.i_jenis_bayar='02' and
																	a.i_area='$iarea' and
																	a.i_area=c.i_area and
																	a.d_bukti > '$cek1->d_bukti' AND
																	a.d_bukti < to_date('$dfrom','dd-mm-yyyy')
														group by a.i_area",false);
					$query = $this->db->get();
					$cunai=0;
					if ($query->num_rows() > 0){
						foreach($query->result() as $siw){
							$cunai=$siw->v_terima_tunai;
						}
					}
					$this->db->select("		sum(v_jumlah) as v_terima_giro
																from tm_pelunasan a, tr_area c
																where 
																			a.i_jenis_bayar='01' and
																			a.i_area='$iarea' and
																			a.i_area=c.i_area and
																			a.d_bukti > '$cek1->d_bukti' AND
																			a.d_bukti < to_date('$dfrom','dd-mm-yyyy')
																group by a.i_area",false);
					$querys = $this->db->get();
					$ghiro=0;
					if ($querys->num_rows() > 0){
						foreach($querys->result() as $siw){
							$ghiro=$siw->v_terima_giro;
						}
					}
					$sahirt=$cunai;
					$sahirg=$ghiro;
				//	$sawalt=number_format($cunai);
				//	$sawalg=number_format($ghiro);
					
					$sawalt=$cunai;
					$sawalg=$ghiro;
			   } // end if saldoawal
			 
				$data['sahirt'] = $sahirt;
				$data['sahirg'] = $sahirg;
							   
			   // perbaikan 29-12-2010
			  // 0. hitung jumlah saldo terakhir berdasarkan batas awal dan batas akhir (DIHAPUSS) 
			   //========================================
				//====================================================================
			} // end else (jika halaman 2 dst)
			
			// test disini udh GOOD
			//echo $sahirt." ".$sahirg."<br> aya teu"; die();
##############
			//	$sahirt=0;
			//	$sahirg=0;
			//	$sawalt=0;
			//	$sawalg=0;
			
      $config['base_url'] = base_url().'index.php/listikhp/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$sahirt.'/'.$sahirg.'/index/';

      // me, ini ga dipake lagi, soalnya perhitungan jumlah datanya dari hasil filtering
/*      $query = $this->db->query("select * from(
														select a.d_bukti, a.i_bukti, b.e_ikhp_typename, a.i_coa, a.v_terima_tunai, a.v_terima_giro, 
															a.v_keluar_tunai, a.v_keluar_giro, c.e_area_name
														from tm_ikhp a, tr_ikhp_type b, tr_area c
														where
														a.i_ikhp_type=b.i_ikhp_type and
														a.i_area='$iarea' and
														a.i_area=c.i_area and
														d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
														d_bukti <= to_date('$dto','dd-mm-yyyy')
														union all
														select 	a.d_bukti, a.i_pelunasan as i_bukti, 'Hasil Tagihan' as e_ikhp_typename, '112.2' as i_coa, 
															a.v_jumlah as v_terima_tunai, 0 as v_terima_giro, 0 as v_keluar_tunai, 0 as v_keluar_giro, c.e_area_name
														from tm_pelunasan a, tr_area c
														where 
														a.i_jenis_bayar='02' and
														a.i_area='$iarea' and
														a.i_area=c.i_area and
                            a.f_pelunasan_cancel='f' and
														a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
														a.d_bukti <= to_date('$dto','dd-mm-yyyy')
														union all
														select 	a.d_bukti, a.i_pelunasan as i_bukti, 'Hasil Tagihan' as e_ikhp_typename, '112.2' as i_coa, 
															0 as v_terima_tunai, a.v_jumlah as v_terima_giro, 0 as v_keluar_tunai, 0 as v_keluar_giro, c.e_area_name 
														from tm_pelunasan a, tr_area c
														where 
														a.i_jenis_bayar='01' and
														a.i_area='$iarea' and
														a.i_area=c.i_area and
                            a.f_pelunasan_cancel='f' and
														a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
														a.d_bukti <= to_date('$dto','dd-mm-yyyy')
												) as x", false); 
			*/ 											 
			// end of me -----------------------------------------------------
			
			// start filtering...
			
		// 1. cek jumlah row after filtering
				$jum_isi_baru= array();
				$buktitmp='xxx';
				$namatmp ='yyy';
				$terimatunai= 0;
				$terimagiro	= 0;
				$keluartunai= 0;
				$keluargiro	= 0;
				$cek_datatanpalimit= $this->mmaster->bacaperiodetanpalimit($iarea,$dfrom,$dto);
				
				$tempsahirt=$sahirt;
				$tempsahirg=$sahirg;
				
			foreach($cek_datatanpalimit as $cek3) {
				$tmp=explode('-',$cek3->d_bukti);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$cek3->d_bukti=$tgl.'-'.$bln.'-'.$thn;
					 
					  if(($buktitmp!=substr($cek3->i_bukti,0,8)) && $buktitmp=='xxx'){
						$bukti=substr($cek3->i_bukti,0,8);
						$nama = $cek3->e_ikhp_typename;
							if(strlen($cek3->i_coa)==5){
								$coa  = $cek3->i_coa.$iarea;
							}
							$terimatunai= $terimatunai+$cek3->v_terima_tunai;
							$terimagiro	= $terimagiro+$cek3->v_terima_giro;
							$keluartunai= $keluartunai+$cek3->v_keluar_tunai;
							$keluargiro	= $keluargiro+$cek3->v_keluar_giro;
							  $tempsahirt			= $tempsahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
							  $tempsahirg			= $tempsahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
							  
							  //echo $sahirt." ".$sahirg; die();
							  
					  }
					  elseif( ($buktitmp==substr($cek3->i_bukti,0,8)) && $buktitmp!='xxx'){
				
						$bukti=substr($cek3->i_bukti,0,8);
						$nama = $cek3->e_ikhp_typename;
							if(strlen($cek3->i_coa)==5){
								$coa  = $cek3->i_coa.$iarea;
							}
							$terimatunai= $terimatunai+$cek3->v_terima_tunai;
							$terimagiro	= $terimagiro+$cek3->v_terima_giro;
							$keluartunai= $keluartunai+$cek3->v_keluar_tunai;
							$keluargiro	= $keluargiro+$cek3->v_keluar_giro;
							  $tempsahirt			= $tempsahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
							  $tempsahirg			= $tempsahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
						
					  }elseif( ($buktitmp!=substr($cek3->i_bukti,0,8)) && $buktitmp!='xxx'){
						$nama = $cek3->e_ikhp_typename;
							if(strlen($cek3->i_coa)==5){
								$coa  = $cek3->i_coa.$iarea;
							}else{
						  $coa  = substr($cek3->i_coa,0,5).$iarea;
						}
								$jum_isi_baru[] = array('dbuktitmp'=> $dbuktitmp,
											'buktitmp'=> $buktitmp,
											'namatmp'=> $namatmp,
											'coa'=> $coa,
											'e_area_name'=> $cek3->e_area_name,
											'terimatunai'=> $terimatunai,
											'terimagiro'=> $terimagiro,
											'keluartunai'=> $keluartunai,
											'keluargiro'=> $keluargiro,
											'sahirt'=> $sahirt,
											'sahirg'=> $sahirg
										);
						
						$terimatunai= 0;
						$terimagiro	= 0;
						$keluartunai= 0;
						$keluargiro	= 0;
							$terimatunai= $terimatunai+$cek3->v_terima_tunai;
							$terimagiro	= $terimagiro+$cek3->v_terima_giro;
							$keluartunai= $keluartunai+$cek3->v_keluar_tunai;
							$keluargiro	= $keluargiro+$cek3->v_keluar_giro;
							  $tempsahirt			= $tempsahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
							  $tempsahirg			= $tempsahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
							  
					  } 
					  $buktitmp=substr($cek3->i_bukti,0,8);
					  $namatmp =$cek3->e_ikhp_typename;
					  $dbuktitmp=$cek3->d_bukti;
					  
				
			} 
			// tes disini udh GOOD
			//echo $sahirt." ".$sahirg."<br> aya teu 2"; die();
			$jum_isi_baru[] = array('dbuktitmp'=> $dbuktitmp,
											'buktitmp'=> $buktitmp,
											'namatmp'=> $namatmp,
											'coa'=> $coa,
											'e_area_name'=> $cek3->e_area_name,
											'terimatunai'=> $terimatunai,
											'terimagiro'=> $terimagiro,
											'keluartunai'=> $keluartunai,
											'keluargiro'=> $keluargiro,
											'sahirt'=> $sahirt,
											'sahirg'=> $sahirg
										); 
				//print_r($jum_isi_baru); die();
				//echo count($jum_isi_baru); die();
				$data['tempsahirt'] = $tempsahirt;
				$data['tempsahirg'] = $tempsahirg;
		//--------------------------------------	
		
		// 2. ambil data per 10 data dlm paging	
			$cek_datatanpalimit= $this->mmaster->bacaperiodetanpalimit($iarea,$dfrom,$dto);
				
				// perubahan rencana, sahirt dan sahirg ambil dari bacasaldo keseluruhan
				// supaya lebih akurat
				
				$hitung_row = 0;
				$isi_baru = array();
				$batas_awal = $this->uri->segment(10);
				
				//$batas_awal = 10;
				if ($batas_awal == '')
					$batas_awal = 0;
				$batas_akhir = $batas_awal+9; 
				// hal 2, batas_awal = 10, batas akhir = 19
			//	echo $batas_awal." ".$batas_akhir. " ".count($jum_isi_baru); die();
				
				  $buktitmp='xxx';
				  $namatmp ='yyy';

				$terimatunai= 0;
				$terimagiro	= 0;
				$keluartunai= 0;
				$keluargiro	= 0;
				
				//echo count($cek_datatanpalimit); die(); // ini 26 record, dari tgl 1 nov s/d 28/12/2010

			foreach($cek_datatanpalimit as $cek3) {
				//echo $hitung_row." ".$cek3->i_bukti."<br>";
				$tmp=explode('-',$cek3->d_bukti);
					$tgl=$tmp[2];
					$bln=$tmp[1];
					$thn=$tmp[0];
					$cek3->d_bukti=$tgl.'-'.$bln.'-'.$thn;
					 
					  if(($buktitmp!=substr($cek3->i_bukti,0,8)) && $buktitmp=='xxx'){
						$bukti=substr($cek3->i_bukti,0,8);
						$nama = $cek3->e_ikhp_typename;
							if(strlen($cek3->i_coa)==5){
								$coa  = $cek3->i_coa.$iarea;
							}
						//	if ($hitung_row>=$batas_awal) {
								$terimatunai= $terimatunai+$cek3->v_terima_tunai;
								$terimagiro	= $terimagiro+$cek3->v_terima_giro;
								$keluartunai= $keluartunai+$cek3->v_keluar_tunai;
								$keluargiro	= $keluargiro+$cek3->v_keluar_giro;
								$sahirt			= $sahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
								$sahirg			= $sahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
						//	}  
							  //echo $sahirt." ".$sahirg." woi <br>";
							  
					  }
					  elseif( ($buktitmp==substr($cek3->i_bukti,0,8)) && $buktitmp!='xxx'){
				
						$bukti=substr($cek3->i_bukti,0,8);
						$nama = $cek3->e_ikhp_typename;
							if(strlen($cek3->i_coa)==5){
								$coa  = $cek3->i_coa.$iarea;
							}
							//if ($hitung_row>=$batas_awal) {
								$terimatunai= $terimatunai+$cek3->v_terima_tunai;
								$terimagiro	= $terimagiro+$cek3->v_terima_giro;
								$keluartunai= $keluartunai+$cek3->v_keluar_tunai;
								$keluargiro	= $keluargiro+$cek3->v_keluar_giro;
								$sahirt			= $sahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
								$sahirg			= $sahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
							//}
						//echo $sahirt." ".$sahirg." woi <br>";
						
					  }elseif( ($buktitmp!=substr($cek3->i_bukti,0,8)) && $buktitmp!='xxx'){
						$nama = $cek3->e_ikhp_typename;
							if(strlen($cek3->i_coa)==5){
								$coa  = $cek3->i_coa.$iarea;
							}else{
						  $coa  = substr($cek3->i_coa,0,5).$iarea;
						}
						// echo $hitung_row." ".$buktitmp." ".$keluargiro."<br>";

							// if ($hitung_row >= $batas_awal) {
								// echo $buktitmp." ".$keluargiro."<br>";
								 //echo $cek3->v_terima_tunai." ".$cek3->v_terima_giro." ".$cek3->v_keluar_tunai." ".$cek3->v_keluar_giro."<br>";
								if ($hitung_row >= $batas_awal) {
									$isi_baru[] = array('dbuktitmp'=> $dbuktitmp,
												'buktitmp'=> $buktitmp,
												'namatmp'=> $namatmp,
												'coa'=> $coa,
												'e_area_name'=> $cek3->e_area_name,
												'terimatunai'=> $terimatunai,
												'terimagiro'=> $terimagiro,
												'keluartunai'=> $keluartunai,
												'keluargiro'=> $keluargiro,
												'sahirt'=> $sahirt,
												'sahirg'=> $sahirg
											);
								}
					
								$terimatunai= 0;
								$terimagiro	= 0;
								$keluartunai= 0;
								$keluargiro	= 0;
									$terimatunai= $terimatunai+$cek3->v_terima_tunai;
									$terimagiro	= $terimagiro+$cek3->v_terima_giro;
									$keluartunai= $keluartunai+$cek3->v_keluar_tunai;
									$keluargiro	= $keluargiro+$cek3->v_keluar_giro;
									  $sahirt			= $sahirt+$cek3->v_terima_tunai-$cek3->v_keluar_tunai;
									  $sahirg			= $sahirg+$cek3->v_terima_giro-$cek3->v_keluar_giro;
							  
						 //    }
							
							  $hitung_row++;
							 
						//echo $sahirt." ".$sahirg." woi <br>";	  
					  } 
					  $buktitmp=substr($cek3->i_bukti,0,8);
					  $namatmp =$cek3->e_ikhp_typename;
					  $dbuktitmp=$cek3->d_bukti;
					
					  //echo $hitung_row."<br>";
					  if ($hitung_row > $batas_akhir)
						break;

			} //echo $hitung_row." ".count($jum_isi_baru)."<br>"; die();
			  //echo $sahirt." ".$sahirg." woi <br>";
			 //die();
		if ($batas_akhir >= count($jum_isi_baru))
			$isi_baru[] = array('dbuktitmp'=> $dbuktitmp,
											'buktitmp'=> $buktitmp,
											'namatmp'=> $namatmp,
											'coa'=> $coa,
											'terimatunai'=> $terimatunai,
											'terimagiro'=> $terimagiro,
											'keluartunai'=> $keluartunai,
											'keluargiro'=> $keluargiro,
											'sahirt'=> $sahirt,
											'sahirg'=> $sahirg,
											'e_area_name'=> $cek3->e_area_name
										);
			//print_r($isi_baru); die();
			
			//=================================================================

							//$config['total_rows'] = $query->num_rows(); 
							$config['total_rows'] = count($jum_isi_baru); 
							$config['per_page'] = '10';
							$config['first_link'] = 'Awal';
							$config['last_link'] = 'Akhir';
							$config['next_link'] = 'Selanjutnya';
							$config['prev_link'] = 'Sebelumnya';
							$config['cur_page'] = $this->uri->segment(10);
							$this->pagination->initialize($config);					 
												
		/*	$query = $this->db->query(" select distinct substr(x.i_bukti,1,7) as tes, x.d_bukti from(
														      select a.d_bukti, a.i_bukti
														      from tm_ikhp a, tr_ikhp_type b, tr_area c
														      where
														      a.i_ikhp_type=b.i_ikhp_type and
														      a.i_area='$iarea' and
														      a.i_area=c.i_area and
														      d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
														      d_bukti <= to_date('$dto','dd-mm-yyyy')
														      union all
														      select 	a.d_bukti, a.i_pelunasan as i_bukti
														      from tm_pelunasan a, tr_area c
														      where 
														      a.i_jenis_bayar='02' and
														      a.i_area='$iarea' and
														      a.i_area=c.i_area and
                                  a.f_pelunasan_cancel='f' and
														      a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
														      a.d_bukti <= to_date('$dto','dd-mm-yyyy')
														      union all
														      select 	a.d_bukti, a.i_pelunasan as i_bukti
														      from tm_pelunasan a, tr_area c
														      where 
														      a.i_jenis_bayar='01' and
														      a.i_area='$iarea' and
														      a.i_area=c.i_area and
                                  a.f_pelunasan_cancel='f' and
														      a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
														      a.d_bukti <= to_date('$dto','dd-mm-yyyy')
												      ) as x
                              order by x.d_bukti, substr(x.i_bukti,1,7)",false);
			*/
			

##############
			$data['isi_baru'] = $isi_baru;
			//$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(10),$cari);
			//$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,100,$this->uri->segment(8),$cari);

			$this->load->view('listikhp/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listikhp');
			$this->load->view('listikhp/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dto		= $this->uri->segment(8);
			$dfrom	= $this->uri->segment(7);
			$iarea	= $this->uri->segment(6);
			$ispb		= $this->uri->segment(5);
			$inota	= $this->uri->segment(4);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$this->load->model('listikhp/mmaster');
			$this->mmaster->delete($inota,$ispb,$iarea);
			$config['base_url'] = base_url().'index.php/listikhp/cform/view/'.$inota.'/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
																	where a.i_customer=b.i_customer 
																	and a.f_ttb_tolak='f'
																	and not a.i_nota isnull
																	and (upper(a.i_nota) like '%$cari%' 
																		or upper(a.i_spb) like '%$cari%' 
																		or upper(a.i_customer) like '%$cari%' 
																		or upper(b.e_customer_name) like '%$cari%')
																	and a.i_area='$iarea' and
																	a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
																	a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listikhp');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listikhp/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listikhp/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
										where a.i_customer=b.i_customer 
										and a.f_ttb_tolak='f'
										and not a.i_nota isnull
										and (upper(a.i_nota) like '%$cari%' 
										  or upper(a.i_spb) like '%$cari%' 
										  or upper(a.i_customer) like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%')
										and a.i_area='$iarea' and
										a.d_nota >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_nota <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '13';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('listikhp/mmaster');
			$data['page_title'] = $this->lang->line('listikhp');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listikhp/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listikhp/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
        $query = $this->db->query("select * from tr_area ",false);
      }else{			
        $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									   or i_area = '$area4' or i_area = '$area5'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listikhp/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listikhp/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu170')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listikhp/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
      if($area1=='00'){
			  $query 	= $this->db->query("select * from tr_area
									     	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
									     	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
									     	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listikhp/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listikhp/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
