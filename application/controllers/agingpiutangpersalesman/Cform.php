<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu258')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('agingpiutangpersalesman');
      $data['djt']='';
			$this->load->view('agingpiutangpersalesman/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu258')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	= strtoupper($this->input->post('cari'));
      $djt	= $this->input->post('djt');
			if($djt=='') $djt	= $this->uri->segment(4);
#      if($cari=='')$cari=$this->uri->segment(6);
      if($cari=='' && ($this->uri->segment(5)=='index' or $this->uri->segment(5)=='')){
  			$config['base_url'] = base_url().'index.php/agingpiutangpersalesman/cform/view/'.$djt.'/index/';
      }else{
   			if($cari=='') $cari	= $this->uri->segment(5);
        $config['base_url'] = base_url().'index.php/agingpiutangpersalesman/cform/view/'.$djt.'/'.$cari.'/';
      }
      $query = $this->db->query(" select sum(a.v_sisa) as v_sisa, sum(a.v_nota_netto) as v_nota_netto, a.i_salesman, b.e_salesman_name
                                  from tm_nota a, tr_salesman b
					                        where a.i_salesman=b.i_salesman
					                        and a.f_ttb_tolak='f' and not a.i_nota isnull and (upper(a.i_nota) like '%$cari%' 
				                          or upper(a.i_spb) like '%$cari%' or upper(a.i_salesman) like '%$cari%' or upper(b.e_salesman_name) like '%$cari%')
					                        and a.v_sisa>0 and a.f_nota_cancel='f' 
					                        group by a.i_salesman, b.e_salesman_name
					                        ORDER BY a.i_salesman",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('agingpiutangpersalesman/mmaster');
  		$data['page_title'] = $this->lang->line('agingpiutangpersalesman');
			$data['cari']		= $cari;
			$data['djt']	  = $djt;
			$data['isi']		= $this->mmaster->bacaperiode($cari,$config['per_page'],$this->uri->segment(6));

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Buka Aging piutang Per Salesman:'.$djt;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('agingpiutangpersalesman/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu258')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('agingpiutangpersalesman');
			$this->load->view('agingpiutangpersalesman/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
		function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
     	 ($this->session->userdata('menu258')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dto	    = $this->input->post('dto');
			$iarea= $this->input->post('iarea');
      if($iarea=='')$icustomer=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$this->load->model('agingpiutangpersalesman/mmaster');
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$data['dto']  = $dto;
			$data['iarea']= $iarea;
			$data['page_title'] = $this->lang->line('printopnnotaarea');
			$data['isi']=$this->mmaster->baca($iarea,$dto);
			$data['user']	= $this->session->userdata('user_id');
      $sess = $this->session->userdata('session_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Buka Aging piutang jatuh tempo:'.$dto.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('agingpiutangpersalesman/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu258')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isalesman	= $this->uri->segment(4);
			$djt	= $this->uri->segment(6);
			$esalesman_name	= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/agingpiutangpersalesman/cform/detail/'.$isalesman.'/'.$esalesman_name.'/'.$djt.'/';
      $query = $this->db->query(" select a.i_customer, a.v_sisa, a.i_nota, a.d_nota, a.d_jatuh_tempo, a.v_nota_netto,
                                  a.n_nota_toplength, a.i_sj, d.i_product_group
                                  from tm_nota a, tr_customer b, tr_customer_groupar c, tm_spb d
                                  where a.i_customer=b.i_customer and a.i_customer=c.i_customer 
                                  and a.f_ttb_tolak='f'  and a.i_nota=d.i_nota and a.i_spb=d.i_spb
                                  and not a.i_nota isnull and b.i_customer=d.i_customer
                                  and a.i_area=d.i_area and b.i_area=d.i_area
                                  and a.i_salesman='$isalesman' and a.v_sisa>0 
                                  and a.f_nota_cancel='f'
                                  order by a.i_nota",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);
			$this->load->model('agingpiutangpersalesman/mmaster');
  		$data['page_title'] = $this->lang->line('agingpiutangpersalesman');
			$data['isalesman']	= $isalesman;
			$data['esalesman_name']	= $esalesman_name;
			$data['djt']	= $djt;
			$data['isi']		= $this->mmaster->bacadetail($isalesman,$djt,$config['per_page'],$this->uri->segment(7));
			$this->load->view('agingpiutangpersalesman/vformdetail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
