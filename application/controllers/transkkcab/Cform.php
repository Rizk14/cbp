<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu155')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transkkcab');
			$data['dfrom']	= '';
			$data['dto']	= '';
			$this->load->view('transkkcab/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu155')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dfrom	= $this->uri->segment(4);
			$dto	= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/transkkcab/cform/view/'.$dfrom.'/'.$dto.'/';
			$this->load->model('transkkcab/mmaster');
			$data['page_title'] = $this->lang->line('transkkcab');
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isi']	= $this->mmaster->bacaperiode($dfrom,$dto);
			$this->load->view('transkkcab/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function transfer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu155')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$jml = $this->input->post('jml', TRUE);
			$this->db->trans_begin();
			$this->load->model('opclose/mmaster');
			$fp	= fopen('transfer/kas-kecil.csv','w');
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$iarea 		= $this->input->post('iarea'.$i, TRUE);
					$ikk 		= $this->input->post('ikk'.$i, TRUE);
					$iperiode 	= $this->input->post('iperiode'.$i, TRUE);
					$icoa 		= $this->input->post('icoa'.$i, TRUE);
					$ikendaraan = $this->input->post('ikendaraan'.$i, TRUE);
					$vkk 		= $this->input->post('vkk'.$i, TRUE);
					$dkk 		= $this->input->post('dkk'.$i, TRUE);
					$ecoaname 	= $this->input->post('ecoaname'.$i, TRUE);
					$edescription = $this->input->post('edescription'.$i, TRUE);
					$ejamin 	= $this->input->post('ejamin'.$i, TRUE);
					$ejamout 	= $this->input->post('ejamout'.$i, TRUE);
					$nkm 		= $this->input->post('nkm'.$i, TRUE);
					$dentry 	= $this->input->post('dentry'.$i, TRUE);
					$dupdate 	= $this->input->post('dupdate'.$i, TRUE);
					$fposting 	= $this->input->post('fposting'.$i, TRUE);
					$fclose 	= $this->input->post('fclose'.$i, TRUE);
					$etempat 	= $this->input->post('etempat'.$i, TRUE);
					$fdebet 	= $this->input->post('fdebet'.$i, TRUE);
					$dbukti 	= $this->input->post('dbukti'.$i, TRUE);
					$enamatoko 	= $this->input->post('enamatoko'.$i, TRUE);
					$list=array($iarea,$ikk,$iperiode,$icoa,$ikendaraan,$vkk,$dkk,
							   $ecoaname,$edescription,$ejamin,$ejamout,$nkm,$dentry,
							   $dupdate,$fposting,$fclose,$etempat,$fdebet,$dbukti,$enamatoko);
					fputcsv($fp, $list,'|');
				}
			}
			fclose($fp);

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Transfer KK Cabang Area '.$iarea.' Periode:'.$iperiode;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses']			= true;
			$this->load->view('status',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
