<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari					= strtoupper($this->input->post('xcari'));
			$dfrom					= $this->input->post('xdfrom');
			$dto					= $this->input->post('xdto');
			$iarea					= $this->input->post('xiarea');
			$is_cari				= $this->input->post('xis_cari'); 
			if($dfrom=='') $dfrom	= $this->uri->segment(4);
			if($dto=='') $dto		= $this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			if ($is_cari == '')
				$is_cari			= $this->uri->segment(8);
			if ($cari == '' && $is_cari == "1") 
			$cari					= $this->uri->segment(7);
			$this->load->model('listspbtidakpenuh/mmaster');
			
			$data['page_title'] = $this->lang->line('listspbtidakpenuh');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea'] 		= $iarea;

		$sql=" * FROM v_persen_spb WHERE";
      if($iarea!='NA'){
        $sql .=" i_area='$iarea' and ";
      }
      $sql .=" d_spb  >=  to_date('$dfrom','dd-mm-yyyy')
			   and d_spb <= to_date('$dto','dd-mm-yyyy') and deliver < 100
               order by i_area, i_spb ";
      $this->db->select($sql,false);
		$query 	= $this->db->get();
		$sess	= $this->session->userdata('session_id');
		$id		= $this->session->userdata('user_id');
		$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		$rs		= pg_query($sql);
			
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$queryy 	= pg_query("SELECT current_timestamp as c");
	    
	    while($row=pg_fetch_assoc($queryy)){
	    	$now	  = $row['c'];
			}
			$pesan='Export Daftar SPB Tidak Penuh Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

      $this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Daftar SPB Tidak Penuh ")->setDescription("PT. Bahtera Laju Nusantara");
			$objPHPExcel->setActiveSheetIndex(0);
    
      if ($query->num_rows() > 0){
     		$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);//No SPB
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);//Lang
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(4);//Area
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(4);//%
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);//Daerah
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);//Kode
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(40);//Nama Barang
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);//Pesan
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);//Relalisasi SJ
				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar SPB Tidak 100%');
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Periode :'. $dfrom . ' s/d '. $dto);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,10,2);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:S6'
				);

				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'No SPB');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Lang');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Area');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', '%');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Daerah');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Nama Barang');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Pesan');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Realisasi SJ');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
        $i=7;
		$j=7;
        $no=0;
				foreach($query->result() as $row){
         $no++;
         $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  	=> false,
						  'italic'	=> false,
						  'size' 	=> 10
					  )
				  ),
				  'A'.$i.':S'.$i
				  );
    
          if($row->f_spb_stockdaerah=='t')
          {
            $daerah='Ya';
          }else{
            $daerah='Tidak';
          }
          
          if($row->n_deliver==''){
								$persen='0.00';
							  }else{
								$persen=number_format(($row->n_deliver/$row->n_order)*100,2);
							  }
          
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row->i_spb);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, "(".$row->i_customer.')'.$row->e_customer_name);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, "'".$row->i_area);
					$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $persen);
					$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $daerah);
					$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->i_product);
					$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->e_product_name);
					$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->n_order);
					$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->n_deliver);
					$i++;
				}
			}
			$objPHPExcel->getActiveSheet()->getStyle('F7:G'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$objPHPExcel->getActiveSheet()->getStyle('K7:K'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='SPBTidakPenuh'.$iarea.'.xls';
	  if($iarea=='NA')$iarea='00';
      if(file_exists('spb/'.$iarea.'/'.$nama)){
        @chmod('spb/'.$iarea.'/'.$nama, 0777);
        @unlink('spb/'.$iarea.'/'.$nama);
      }
			$objWriter->save("spb/".$iarea.'/'.$nama); 
      @chmod('spb/'.$iarea.'/'.$nama, 0777);
      echo $nama;

		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			//$config['base_url'] = base_url().'index.php/listspbtidakpenuh/cform/index/';
			$data['page_title'] = $this->lang->line('listspbtidakpenuh');
			$data['dfrom']='';
			$data['dto']='';	
			//$data['isi']='';			
			$this->load->view('listspbtidakpenuh/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listspbtidakpenuh');
			$this->load->view('listspbtidakpenuh/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
			if ($cari == '') {
				$cari=$this->uri->segment(7);
			}
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea'); 
			if($dfrom=='') $dfrom = $this->uri->segment(4);
			if($dto=='') $dto = $this->uri->segment(5);
			if($iarea=='') $iarea = $this->uri->segment(6);
			
			$config['base_url'] = base_url().'index.php/listspbtidakpenuh/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/index/';
			
						
			$sql= " select * FROM v_persen_spb WHERE d_spb >= to_date('$dfrom','dd-mm-yyyy') and d_spb <= to_date('$dto','dd-mm-yyyy') 
		    		and i_area='$iarea' and deliver < 100 
		    		and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%'
					or upper(i_spb) like '%$cari%' or upper(i_product) like '%$cari%')" ;
			
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			
			$this->load->model('listspbtidakpenuh/mmaster');
			$data['page_title'] = $this->lang->line('listspbtidakpenuh');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['iarea'] = $iarea;
			$data['isi']	= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listspbtidakpenuh/vmainform',$data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

function paging() 
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($this->uri->segment(5)!=''){
				if($this->uri->segment(4)!='sikasep'){
					$cari=$this->uri->segment(4);
				}else{
					$cari='';
				}
			}elseif($this->uri->segment(4)!='sikasep'){
				$cari=$this->uri->segment(4);
			}else{
				$cari='';
			}
			if($cari=='')
				$config['base_url'] = base_url().'index.php/listspbtidakpenuh/cform/paging/sikasep/';
			else
				$config['base_url'] = base_url().'index.php/listspbtidakpenuh/cform/paging/'.$cari.'/';
			if($this->uri->segment(4)=='sikasep')
				$cari='';
			$cari	= $this->input->post('cari', TRUE);
			$cari  	= strtoupper($cari);
			if($this->session->userdata('level')=='0'){
				$sql= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
						where a.i_customer=b.i_customer and a.i_nota is null
						and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						or upper(a.i_spb) like '%$cari%') order by a.i_spb desc ";
			}else{
				$sql= " select a.*, b.e_customer_name from tm_spb a, tr_customer b
						where a.i_customer=b.i_customer and a.i_nota is null
						and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						or upper(a.i_spb) like '%$cari%')
						and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4'
						or a.i_area='$area5') order by a.i_spb desc ";
			}
			$query 	= $this->db->query($sql,false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$data['cari'] = $cari;
			$data['page_title'] = $this->lang->line('listspbtidakpenuh');
			$this->load->model('listspbtidakpenuh/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspbtidakpenuh/vform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listspbtidakpenuh/cform/area/index/';
         $iuser   = $this->session->userdata('user_id');
         $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('listspbtidakpenuh/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listspbtidakpenuh/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

/*function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listspbtidakpenuh/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
											or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listspbtidakpenuh/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspbtidakpenuh/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}*/

function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listspbtidakpenuh/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link']  = 'Akhir';
			$config['next_link']  = 'Selanjutnya';
			$config['prev_link']  = 'Sebelumnya';
			$config['cur_page']   = $this->uri->segment(5);
			
			$this->pagination->initialize($config);
			$this->load->model('listspbtidakpenuh/mmaster');
			
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listspbtidakpenuh/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			   
			$dfrom					= $this->input->post('dfrom');
			$dto  					= $this->input->post('dto');
			$iarea					= $this->input->post('iarea');
			$cari					= strtoupper($this->input->post('cari'));
			$is_cari				= $this->input->post('is_cari'); 
			
			if($iarea=='')$iarea	= $this->uri->segment(4);
			if($dfrom=='')$dfrom	= $this->uri->segment(5);
			if($dto=='')$dto		= $this->uri->segment(6);
			//$config['base_url'] 	= base_url().'index.php/listspbtidakpenuh/cform/view/'.$iarea.'/'.$dfrom.'/'.$dto.'/'; 
		
			if ($is_cari == '')
				$is_cari= $this->uri->segment(8);
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(7);
			
			if ($is_cari=="1") { 
				$config['base_url'] = base_url().'index.php/listspbtidakpenuh/cform/view/'.$iarea.'/'.$dfrom.'/'.$dto.'/'.$cari.'/'.$is_cari.'/index/';
			}
			else { 
				$config['base_url'] = base_url().'index.php/listspbtidakpenuh/cform/view/'.$iarea.'/'.$dfrom.'/'.$dto.'/index/';
			} 
			
			if ($is_cari != "1") {
				$sql= " select * FROM v_persen_spb WHERE
						d_spb >= to_date('$dfrom','dd-mm-yyyy') and d_spb <= to_date('$dto','dd-mm-yyyy') and deliver < 100 ";
					
			if($iarea!='NA'){		
				 $sql .=" and i_area='$iarea' and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%'
				   or upper(i_spb) like '%$cari%' or upper(i_product) like '%$cari%')";	
			}
				}else{
			$sql=" select * FROM v_persen_spb WHERE d_spb  >=  to_date('$dfrom','dd-mm-yyyy')
				   and d_spb <= to_date('$dto','dd-mm-yyyy') and deliver < 100 
				   and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%'
				   or upper(i_spb) like '%$cari%' or upper(i_product) like '%$cari%') ";
			/*}else{
			$sql=" select * FROM v_persen_spb WHERE d_spb >= '$dfrom' 
							   and d_spb <= '$dto' and i_area='$iarea' and deliver < 100 
							   and (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%'
							   or upper(i_spb) like '%$cari%' or upper(i_product) like '%$cari%') ";*/
			
			 if($iarea!='NA'){
          $sql .=" and i_area='$iarea' ORDER BY i_spb ASC";
			}}
			$query 					= $this->db->query($sql,false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= 'all';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			//$config['cur_page'] 	= $this->uri->segment(7);
			if ($is_cari=="1")
				$config['cur_page'] = $this->uri->segment(10);
			else
				$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config); 

			$this->load->model('listspbtidakpenuh/mmaster');
			$data['page_title'] 	= $this->lang->line('listspbtidakpenuh');
			$data['dfrom']			= $dfrom;
			$data['dto']			= $dto;
			$data['iarea']			= $iarea;
			$data['cari']			= $cari;
			
			if ($is_cari=="1")
				$data['isi']			= $this->mmaster->bacaperiodespb($dfrom,$dto,$iarea,$cari,$config['per_page'],$this->uri->segment(10));
			else
				$data['isi']			= $this->mmaster->bacaperiodespb($dfrom,$dto,$iarea,$cari,$config['per_page'],$this->uri->segment(8));
			
			$this->load->view('listspbtidakpenuh/vmainform', $data);
			}else{
					$this->load->view('Awal/index.php');
				 }
	}
	
/*Modif	
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu533')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea'); //textfield area
			$is_cari= $this->input->post('is_cari'); //saat pilih area
				
			if($dfrom=='') $dfrom	=$this->uri->segment(4);
			if($dto=='')   $dto		=$this->uri->segment(5);
			if($iarea=='') $iarea	=$this->uri->segment(6);
				
			if ($cari == '' && $is_cari == "1") $cari=$this->uri->segment(7);
			if ($is_cari == '')
				$is_cari= $this->uri->segment(8);
			if ($is_cari=="1") 
			{ 
				$config['base_url']=base_url().'index.php/listspbtidakpenuh/cform/view/'.$iarea.'/'.$dfrom.'/'.$to.'/'.$cari.'/'.$is_cari.'/index/';
			}
			else{ 
				  $config['base_url'] = base_url().'index.php/listspbtidakpenuh/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
				} 
			
			if ($is_cari != "1") {
				$sql= " select a.i_spb
                from tm_spb a 
								left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f')
								left join tr_customer b on(a.i_customer=b.i_customer)
								left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
								, tr_area c
                where 
                a.i_area=c.i_area and (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                a.d_spb <= to_date('$dto','dd-mm-yyyy'))";
        
//script pilih area Nasional--->    
        if($iarea!='NA'){
          $sql .=" and a.i_area='$iarea'";
        }
			}else
			{
				$sql= " select a.i_spb
                from tm_spb a 
								left join tm_nota d on(a.i_spb=d.i_spb and a.i_area=d.i_area and d.f_nota_cancel='f') 
								left join tr_customer b on(a.i_customer=b.i_customer and a.i_area=b.i_area)
								left join tr_customer_tmp x on(a.i_customer=x.i_customer and a.i_spb=x.i_spb and a.i_area=x.i_area)
								, tr_area c
                where 
                a.i_area=c.i_area and					
                (a.d_spb >= to_date('$dfrom','dd-mm-yyyy') AND
                a.d_spb <= to_date('$dto','dd-mm-yyyy')) AND
                (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
                or upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%')";
        
				if($iarea!='NA')
				{
					$sql .=" and a.i_area='$iarea'";
				} 
			}//<---script pilih area Nasional     
			
			$query 					= $this->db->query($sql,false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link']	= 'Sebelumnya';
			
			if ($is_cari=="1")
				$config['cur_page'] = $this->uri->segment(10);
			else
				$config['cur_page'] = $this->uri->segment(8);
			
			$this->pagination->initialize($config);
			
			$this->load->model('listspbtidakpenuh/mmaster');
			$data['page_title'] = $this->lang->line('listspbtidakpenuh');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea'] 		= $iarea;

			if ($is_cari=="1")									
				$data['isi']	= $this->mmaster->bacaperiodespb($dfrom,$dto,$iarea,$config['per_page'],$this->uri->segment(10),$cari);
			else
				$data['isi']	= $this->mmaster->bacaperiodespb($dfrom,$dto,$iarea,$config['per_page'],$this->uri->segment(8),$cari);

			//$sess				= $this->session->userdata('session_id');
			//$id				= $this->session->userdata('user_id');
			//$sql				= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			//$rs				= pg_query($sql);
			
		/*	if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs))
				{
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
					$ip_address='kosong';
				 }
				 
			$query 	= pg_query("SELECT current_timestamp as c");
	    
	   while($row = pg_fetch_assoc($query))
			{
			  $now	  = $row['c'];
			}
			$pesan='Membuka Data SPB Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('listspbtidakpenuh/vmainform',$data);			
			}else{
					$this->load->view('awal/index.php');
				 }
	}*/
	
	
}
?>
