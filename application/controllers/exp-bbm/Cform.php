<?php 
class Cform extends CI_Controller
{
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      require_once("php/fungsi.php");
   }

   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu326')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
      {
         $data['page_title'] = $this->lang->line('exp-bbm')." BBM";
         $data['datefrom'] ='';
         $data['dateto']   ='';
         $this->load->view('exp-bbm/vmainform', $data);
      }
      else
      {
         $this->load->view('awal/index.php');
      }
   }

   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu326')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
      {
         $this->load->model('exp-bbm/mmaster');
         $iarea      = $this->input->post('iarea');
         $datefrom   = $this->input->post('datefrom');
         $dateto     = $this->input->post('dateto');
         $datefromx  = $datefrom;
         $datetox    = $dateto;

         //update penambahan field 30-01-2017
         $que        = $this->db->query(" select a.i_bbm, c.d_bbm, c.i_area, b.e_area_name, f.i_ttb , g.e_customer_name,
                                   a.i_product, a.e_product_name, a.n_quantity, a.i_refference_document
                                          from tm_bbm_item a, tr_area b, tm_bbm c, tm_ttbretur f, tr_customer g
                                          where c.i_area=b.i_area and c.f_bbm_cancel  ='f' and c.i_bbm = f.i_bbm and f.i_customer = g.i_customer 
                                          and a.i_bbm=c.i_bbm and c.i_area='$iarea'
                                          and c.d_bbm >= to_date('$datefromx','dd-mm-yyyy') and
                                          c.d_bbm <= to_date('$datetox','dd-mm-yyyy')
                                          order by a.i_bbm");

         /*$que        = $this->db->query(" select a.i_bbm, c,d_bbm, c.i_area, b.e_area_name, 
                                   a.i_product, a.e_product_name, a.n_quantity, a.i_refference_document
                                          from tm_bbm_item a, tr_area b, tm_bbm c
                                          where c.i_area=b.i_area and c.f_bbm_cancel  ='f'
                                          and a.i_bbm=c.i_bbm and c.i_area='$iarea'
                                          and c.d_bbm >= to_date('$datefromx','dd-mm-yyyy') and
                                          c.d_bbm <= to_date('$datetox','dd-mm-yyyy')
                                          order by a.i_bbm");*/
#         and d.i_price_group='03'
            $fromname='';
            if($datefrom!='')
            {
               $tmp=explode("-",$datefrom);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $fromname=$fromname.$hr;
               $datefrom=$th."-".$bl."-".$hr;
               $periodeawal   = $hr." ".mbulan($bl)." ".$th;
            }
            $tmp           = explode("-", $dateto);
            $det           = $tmp[0];
            $mon           = $tmp[1];
            $yir           = $tmp[2];
            $dtos          = $yir."/".$mon."/".$det;
            $fromname      = $fromname.$det;
            $periodeakhir  = $det." ".mbulan($mon)." ".$yir;
            $dtos          = $this->mmaster->dateAdd("d",1,$dtos);
            $tmp           = explode("-", $dtos);
            $det1          = $tmp[2];
            $mon1          = $tmp[1];
            $yir1          = $tmp[0];
            $dtos          = $yir1."-".$mon1."-".$det1;
            $data['page_title'] = $this->lang->line('exp-bbm')." SJP";
            $qareaname  = $this->mmaster->eareaname($iarea);
            if($qareaname->num_rows()>0)
            {
               $row_areaname  = $qareaname->row();
               $aname   = $row_areaname->e_area_name;
            }
            else
            {
               $aname   = '';
            }
            
            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("Daftar BBM")
                        ->setDescription(NmPerusahaan);
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
               array(
                  'font'   => array(
                     'name'   => 'Arial',
                     'bold'   => true,
                     'italic' => false,
                     'size'  => 12
                  ),
                  'alignment' => array(
                     'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
                     'vertical'  => Style_Alignment::VERTICAL_CENTER,
                     'wrap'      => true
                  )
               ),
               'A1:A4'
            );
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(3);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(4);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);


            $objPHPExcel->getActiveSheet()->setCellValue('A1',NmPerusahaan);
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,1,9,1);
            $objPHPExcel->getActiveSheet()->setCellValue('A2', 'DAFTAR BBM - '.$aname);
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,9,2);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', 'PERIODE : '.$periodeawal.' - '.$periodeakhir);
            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,9,3);
            $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
            $objPHPExcel->getActiveSheet()->getStyle('A5:A5')->applyFromArray(
               array(
                  'borders'   => array(
                     'top'       => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('B5', 'No BBM');
            $objPHPExcel->getActiveSheet()->getStyle('B5:B5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Tgl BBM');
            $objPHPExcel->getActiveSheet()->getStyle('C5:C5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Area');
            $objPHPExcel->getActiveSheet()->getStyle('D5:D5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Nama Area');
            $objPHPExcel->getActiveSheet()->getStyle('E5:E5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('F5', 'No TTB');
            $objPHPExcel->getActiveSheet()->getStyle('F5:F5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Nama Customer');
            $objPHPExcel->getActiveSheet()->getStyle('G5:G5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Kode Prod');
            $objPHPExcel->getActiveSheet()->getStyle('H5:H5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Nama Produk');
            $objPHPExcel->getActiveSheet()->getStyle('I5:I5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
            $objPHPExcel->getActiveSheet()->setCellValue('J5', 'QTY');
            $objPHPExcel->getActiveSheet()->getStyle('J5:J5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
            $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Refference');
            $objPHPExcel->getActiveSheet()->getStyle('K5:K5')->applyFromArray(
               array(
                  'borders' => array(
                     'top'    => array('style' => Style_Border::BORDER_THIN),
                     'bottom'=> array('style' => Style_Border::BORDER_THIN),
                     'left'  => array('style' => Style_Border::BORDER_THIN),
                     'right' => array('style' => Style_Border::BORDER_THIN)
                  ),
               )
            );
 
            $objPHPExcel->getActiveSheet()->duplicateStyleArray(
               array(
                  'font' => array(
                     'name'   => 'Liberation Sans',
                     'bold'  => false,
                     'italic'=> false,
                     'size'  => 12
                  ),
                  'alignment' => array(
                     'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
                     'vertical'  => Style_Alignment::VERTICAL_CENTER,
                     'wrap'      => true
                  )
               ),
               'A5:J5'
            );

            //update penambahan field ttb dan nama customer 30-01-2017
            $this->db->select(" a.i_bbm, c.d_bbm, c.i_area, b.e_area_name, f.i_ttb , g.e_customer_name,
                                a.i_product, a.e_product_name, a.n_quantity, a.i_refference_document
                                from tm_bbm_item a, tr_area b, tm_bbm c , tm_ttbretur f, tr_customer g
                                where c.i_area=b.i_area and c.f_bbm_cancel='f' and c.i_bbm = f.i_bbm and f.i_customer = g.i_customer 
                                and a.i_bbm=c.i_bbm and c.i_area='$iarea'
                                and c.d_bbm >= to_date('$datefromx','dd-mm-yyyy') and
                                c.d_bbm <= to_date('$datetox','dd-mm-yyyy')
                                order by a.i_bbm",false);

            /*$this->db->select(" a.i_bbm, c,d_bbm, c.i_area, b.e_area_name, 
                                   a.i_product, a.e_product_name, a.n_quantity, a.i_refference_document
                                          from tm_bbm_item a, tr_area b, tm_bbm c
                                          where c.i_area=b.i_area and c.f_bbm_cancel='f'
                                          and a.i_bbm=c.i_bbm and c.i_area='$iarea'
                                          and c.d_bbm >= to_date('$datefromx','dd-mm-yyyy') and
                                          c.d_bbm <= to_date('$datetox','dd-mm-yyyy')
                                          order by a.i_bbm",false);*/
            $query = $this->db->get();
            if ($query->num_rows() > 0)
            {
               $i=5;
               $j=5;
               $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
                  array(
                        'borders' => array(
                           'top'    => array('style' => Style_Border::BORDER_THIN),
                           'bottom' => array('style' => Style_Border::BORDER_THIN),
                           'left'   => array('style' => Style_Border::BORDER_THIN),
                           'right'  => array('style' => Style_Border::BORDER_THIN)
                        )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom' => array('style' => Style_Border::BORDER_THIN),
                        'left'   => array('style' => Style_Border::BORDER_THIN),
                        'right'  => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );

               $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );

               $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN),
                        'bottom'=> array('style' => Style_Border::BORDER_THIN),
                        'left'  => array('style' => Style_Border::BORDER_THIN),
                        'right' => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );

               foreach($query->result() as $row)
               {
                  
                 // $total=$row->n_quantity_deliver*$row->v_product_retail;                  
                 
                  $i++;
                  $j++;

                  $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $j-5);
                  $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom' => array('style' => Style_Border::BORDER_THIN),
                              'left'   => array('style' => Style_Border::BORDER_THIN),
                              'right'  => array('style' => Style_Border::BORDER_THIN)
                           ),
                           'alignment' => array(
                              'horizontal'=> Style_Alignment::HORIZONTAL_RIGHT
                           )
                     )
                  );
                  $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_bbm);
                  $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->d_bbm);
                  $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->i_area);
                  $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->e_area_name);
                  $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );  

                  $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->i_ttb);
                  $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );    

                  $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->e_customer_name);
                  $objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );

                  $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->i_product);
                  $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->e_product_name);
                  $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->n_quantity);
                  $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),

                     )
                  );      
                  $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->i_refference_document);
                  $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray(
                     array(
                           'borders' => array(
                              'top'    => array('style' => Style_Border::BORDER_THIN),
                              'bottom'=> array('style' => Style_Border::BORDER_THIN),
                              'left'  => array('style' => Style_Border::BORDER_THIN),
                              'right' => array('style' => Style_Border::BORDER_THIN)
                           ),
                     )
                  );      
    

               }
               $x=$i-1;
              // $objPHPExcel->getActiveSheet()->getStyle('G6:H'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
            }
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $nama='BBM-'.$iarea.'-'.$fromname.'-'.substr($datefrom,5,2).'-'.substr($datefrom,0,4).'.xls';
            $area=$iarea;
 
            if(file_exists('excel/'.$area.'/'.$nama))
            {
               @chmod('excel/'.$area.'/'.$nama, 0777);
               @unlink('excel/'.$area.'/'.$nama);
            }
            $objWriter->save('excel/'.$area.'/'.$nama);
            @chmod('excel/'.$area.'/'.$nama, 0777);

            $sess=$this->session->userdata('session_id');
            $id=$this->session->userdata('user_id');
            $sql  = "select * from dgu_session where session_id='$sess' and not user_data isnull";
            $rs      = pg_query($sql);
            if(pg_num_rows($rs)>0){
               while($row=pg_fetch_assoc($rs)){
                  $ip_address   = $row['ip_address'];
                  break;
               }
            }else{
               $ip_address='kosong';
            }
            $query   = pg_query("SELECT current_timestamp as c");
            while($row=pg_fetch_assoc($query)){
               $now    = $row['c'];
            }
            $pesan='Export BBM Area:'.$iarea.' tanggal:'.$datefrom.' sampai:'.$dateto;
            $this->load->model('logger');
            $this->logger->write($id, $ip_address, $now , $pesan ); 

            $data['sukses']   = true;
            $data['inomor']   = "BBM - ".$area.'/'.$nama;
            $this->load->view('nomor',$data);
         }
      else
      {
         $this->load->view('awal/index.php');
      }
   }
   
   function area()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu326')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $config['base_url'] = base_url().'index.php/exp-bbm/cform/area/index/';
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
                  
         if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
            $query = $this->db->query("select * from tr_area",false);
         }else{
            $query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
                           or i_area = '$area4' or i_area = '$area5'",false);
         }
         $config['total_rows'] = $query->num_rows(); 
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);

         $this->load->model('exp-bbm/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
         $this->load->view('exp-bbm/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   
   function cariarea()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu326')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         $config['base_url'] = base_url().'index.php/exp-bbm/cform/area/index/';
         $cari    = $this->input->post('cari', FALSE);
         $cari = strtoupper($cari);
         if($area1=='00'){# or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
            $query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
         }else{
            $query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
                                 or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
         }
         $config['total_rows'] = $query->num_rows(); 
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(5);
         $this->pagination->initialize($config);
         $this->load->model('exp-bbm/mmaster');
         $data['page_title'] = $this->lang->line('list_area');
         $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
         $this->load->view('exp-bbm/vlistarea', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
}
?>
