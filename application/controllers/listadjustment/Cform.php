<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu484')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listadjustment');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listadjustment/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu484')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listadjustment/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      if($iarea=='NA'){
			  $query = $this->db->query(" select a.* from tm_adj a
										                where (upper(a.i_adj) like '%$cari%')
										                and a.d_adj >= to_date('$dfrom','dd-mm-yyyy') AND
										                a.d_adj <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
			  $query = $this->db->query(" select a.* from tm_adj a
										                where (upper(a.i_adj) like '%$cari%')
										                and a.i_area='$iarea' and
										                a.d_adj >= to_date('$dfrom','dd-mm-yyyy') AND
										                a.d_adj <= to_date('$dto','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listadjustment');
			$this->load->model('listadjustment/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data TTB retur Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listadjustment/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu484')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listadjustment');
			$this->load->view('listadjustment/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu484')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iadj	= $this->uri->segment(4);
			$iarea= $this->uri->segment(5);
			$from	= $this->uri->segment(6);
			$to		= $this->uri->segment(7);
			$this->load->model('listadjustment/mmaster');
			$this->mmaster->delete($iadj,$iarea);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Hapus Adjustment No:'.$iadj.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$cari		= '';
			$config['base_url'] = base_url().'index.php/listadjustment/cform/view/'.$from.'/'.$to.'/'.$iarea.'/index/';
      if($iarea=='NA'){
			  $query = $this->db->query(" select a.* from tm_adj a
										                where (upper(a.i_adj) like '%$cari%')
										                and a.d_adj >= to_date('$from','dd-mm-yyyy') AND
										                a.d_adj <= to_date('$to','dd-mm-yyyy')",false);
      }else{
			  $query = $this->db->query(" select a.* from tm_adj a
										                where (upper(a.i_adj) like '%$cari%')
										                and a.i_area='$iarea' and
										                a.d_adj >= to_date('$from','dd-mm-yyyy') AND
										                a.d_adj <= to_date('$to','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listadjustment');
			$this->load->model('listadjustment/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $from;
			$data['dto']		= $to;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$from,$to,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listadjustment/vmainform', $data);
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu484')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listadjustment/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      if($iarea=='NA'){
			  $query = $this->db->query(" select a.*, b.e_customer_name from tm_adj a, tr_customer b
										  where a.i_customer=b.i_customer and (a.i_customer like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
										  and a.d_ttb >= to_date('$dfrom','dd-mm-yyyy') AND
										  a.d_ttb <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
			  $query = $this->db->query(" select a.*, b.e_customer_name from tm_adj a, tr_customer b
										  where a.i_customer=b.i_customer and (a.i_customer like '%$cari%' 
										  or upper(b.e_customer_name) like '%$cari%' or upper(a.i_ttb) like '%$cari%')
										  and a.i_area='$iarea' and
										  a.d_ttb >= to_date('$dfrom','dd-mm-yyyy') AND
										  a.d_ttb <= to_date('$dto','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listadjustment');
			$this->load->model('listadjustment/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listadjustment/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu484')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listadjustment/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listadjustment/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listadjustment/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu484')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listadjustment/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listadjustment/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listadjustment/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
