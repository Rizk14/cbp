<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu363')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listsjrecblmnota');
      $data['iarea']='';
			$this->load->view('listsjrecblmnota/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu363')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$iarea		= $this->input->post('iarea');
			if($iarea=='') $iarea	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/listsjrecblmnota/cform/view/'.$iarea.'/index/';
      $area1	= $this->session->userdata('i_area');
			if($area1=='00'){
			  if($iarea=='NA'){
			    $query = $this->db->query(" select a.i_sj from tm_nota a, tr_area b, tr_customer c
											where a.i_area=b.i_area and a.i_customer=c.i_customer and
											(upper(a.i_sj) like '%$cari%') and 
											not a.d_sj_receive isnull and a.i_nota isnull and a.f_nota_cancel='f' order by a.i_sj asc, a.d_sj desc
 ",false);
        }else{
			    $query = $this->db->query(" select a.i_sj from tm_nota a, tr_area b, tr_customer c
				  where a.i_area=b.i_area and a.i_customer=c.i_customer and
					  (upper(a.i_sj) like '%$cari%') and 
					  (substring(a.i_sj,9,2)='$iarea' or (substring(a.i_sj,9,2)='BK' and a.i_area='$iarea') ) and 
					  not a.d_sj_receive isnull and a.i_nota isnull  and a.f_nota_cancel='f' order by a.i_sj asc, a.d_sj desc ",false);
			  }
      }else{
			  $query = $this->db->query(" select a.i_sj from tm_nota a, tr_area b, tr_customer c
				where a.i_area=b.i_area and a.i_customer=c.i_customer and
					(upper(a.i_sj) like '%$cari%') and 
					(substring(a.i_sj,9,2)='$iarea' or (substring(a.i_sj,9,2)='BK' and a.i_area='$iarea') ) and 
					not a.d_sj_receive isnull and a.i_nota isnull  and a.f_nota_cancel='f' order by a.i_sj asc, a.d_sj desc ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			if (($this->session->userdata('user_id')=='admsales')||($this->session->userdata('user_id')=='admin')) {
			$config['per_page'] = 'all';
			}
			else{
			$config['per_page'] = '10';	
			}
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjrecblmnota');
			$this->load->model('listsjrecblmnota/mmaster');
			$data['cari']		= $cari;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$config['per_page'],$this->uri->segment(6),$cari);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data SJ Receive Belum Nota Area '.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listsjrecblmnota/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}	
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu363')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listsjrecblmnota');
			$this->load->view('listsjrecblmnota/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu363')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj	= $this->uri->segment(4);
			$iarea= $this->uri->segment(5);
			$dfrom= $this->uri->segment(6);
			$dto	= $this->uri->segment(7);
			$this->load->model('listsjrecblmnota/mmaster');
			$this->mmaster->delete($isj,$iarea);

	    $sess=$this->session->userdata('session_id');
	    $id=$this->session->userdata('user_id');
	    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	    $rs		= pg_query($sql);
	    if(pg_num_rows($rs)>0){
		    while($row=pg_fetch_assoc($rs)){
			    $ip_address	  = $row['ip_address'];
			    break;
		    }
	    }else{
		    $ip_address='kosong';
	    }
	    $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
	    }
	    $pesan='Hapus Surat Jalan No:'.$isj.' Area:'.$iarea;
	    $this->load->model('logger');
	    $this->logger->write($id, $ip_address, $now , $pesan );  

			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/listsjrecblmnota/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      $area1	= $this->session->userdata('i_area');
			if($area1=='00'){
			  $query = $this->db->query(" select a.i_sj from tm_nota a, tr_area b, tr_customer c
				where a.i_area=b.i_area and a.i_customer=c.i_customer and
					(upper(a.i_sj) like '%$cari%') and 
					substring(a.i_sj,9,2)='$iarea' and 
					(a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')) order by a.i_sj asc, a.d_sj desc ",false);
      }else{
			  $query = $this->db->query(" select a.i_sj from tm_nota a, tr_area b, tr_customer c
				where a.i_area=b.i_area and a.i_customer=c.i_customer and
					(upper(a.i_sj) like '%$cari%') and 
					substring(a.i_sj,9,2)='$iarea' and 
					(a.d_sj >= to_date('$dfrom','dd-mm-yyyy') and a.d_sj <= to_date('$dto','dd-mm-yyyy')) order by a.i_sj asc, a.d_sj desc ",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjrecblmnota');
			$this->load->model('listsjrecblmnota/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listsjrecblmnota/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu363')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listsjrecblmnota/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
      $area1	= $this->session->userdata('i_area');
			if($area1=='00'){
			  $query = $this->db->query(" select a.*, b.e_area_name , c.e_customer_name from tm_sj a, tr_area b, tr_customer c
																	  where a.i_area_to=b.i_area and a.i_sj_type='04' and a.i_customer=c.i_customer
																	  or (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
																	  and a.i_area_to='$iarea' 
																	  and a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
																	  a.d_sj <= to_date('$dto','dd-mm-yyyy')",false);
      }else{
			  $query = $this->db->query(" select a.*, b.e_area_name , c.e_customer_name from tm_sj a, tr_area b, tr customer c
																	  where a.i_area_from=b.i_area and a.i_sj_type='04' and a.i_customer=c.i_customer
																	  or (upper(a.i_sj) like '%$cari%' or upper(a.i_sj_old) like '%$cari%' or upper(a.i_spb) like '%$cari%')
																	  and a.i_area_to='$iarea'
                                    and a.f_sj_daerah='t'
                                    and (a.i_sj like '%$cari%' or a.i_sj_old like '%$cari%')
                                    and                                    
																	  a.d_sj >= to_date('$dfrom','dd-mm-yyyy') AND
																	  a.d_sj <= to_date('$dto','dd-mm-yyyy')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listsjrecblmnota');
			$this->load->model('listsjrecblmnota/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listsjrecblmnota/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu363')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listsjrecblmnota/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			/*$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');*/
			$iuser 	= $this->session->userdata('user_id');

			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area in(select i_area from tm_user_area where i_user='$iuser')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listsjrecblmnota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$iuser);
			$this->load->view('listsjrecblmnota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu363')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			/*$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');*/
			$config['base_url'] = base_url().'index.php/listsjrecblmnota/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')order by i_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area in(select i_area from tm_user_area where i_user='$iuser')
											(upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')order by i_area",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listsjrecblmnota/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$iuser);
			$this->load->view('listsjrecblmnota/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
