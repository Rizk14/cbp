<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu42')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/customerpkp/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_pkp');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('master_customerpkp');
			$data['icustomer']='';
			$cari = $this->input->post('cari', FALSE);
			/*$cari = strtoupper($cari);*/
			$this->load->model('customerpkp/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));

			$sess	= $this->session->userdata('session_id');
			$id 	= $this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan="Membuka Menu Master PKP (Pelanggan)";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now, $pesan);

			$this->load->view('customerpkp/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu42')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 		= $this->input->post('icustomer', TRUE);
			$ecustomerpkpname 	= $this->input->post('ecustomerpkpname', TRUE);
			$ecustomerpkpaddress 	= $this->input->post('ecustomerpkpaddress', TRUE);
			$ecustomerpkpnpwp	= $this->input->post('ecustomerpkpnpwp', TRUE);

//			if ((isset($icustomer) && $icustomer != '') && (isset($ecustomerpkpname) && $ecustomerpkpname != ''))
			if ((isset($icustomer) && $icustomer != ''))
			{
				$this->load->model('customerpkp/mmaster');
				$this->mmaster->insert($icustomer,$ecustomerpkpname,$ecustomerpkpaddress,$ecustomerpkpnpwp);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Input Master PKP (Pelanggan):('.$icustomer.') -'.$ecustomerpkpname;
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);

		        $config['base_url'] = base_url().'index.php/customerpkp/cform/index/';
				$config['total_rows'] = $this->db->count_all('tr_customer_pkp');
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->pagination->initialize($config);

				$data['page_title'] = $this->lang->line('master_customerpkp');
				$data['icustomer']='';
				$cari = $this->input->post('cari', FALSE);
				/*$cari = strtoupper($cari);*/
				$this->load->model('customerpkp/mmaster');
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('customerpkp/vmainform', $data);
			/* } */
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu42')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerpkp');
			$this->load->view('customerpkp/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu42')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_customerpkp')." update";
			if($this->uri->segment(4)){
				$icustomer = $this->uri->segment(4);
				$data['icustomer'] = $icustomer;
				$this->load->model('customerpkp/mmaster');
				$data['isi']=$this->mmaster->baca($icustomer);

				$sess=$this->session->userdata('session_id');
		        $id=$this->session->userdata('user_id');
		        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		        $rs		= pg_query($sql);
		        if(pg_num_rows($rs)>0){
		          while($row=pg_fetch_assoc($rs)){
			          $ip_address	  = $row['ip_address'];
			          break;
		          }
		        }else{
		          $ip_address='kosong';
		        }
		        $query 	= pg_query("SELECT current_timestamp as c");
		        while($row=pg_fetch_assoc($query)){
		          $now	  = $row['c'];
		        }
		        $pesan='Membuka Edit Master PKP (Pelanggan):('.$icustomer.')';
		        $this->load->model('logger');
		        $this->logger->write($id, $ip_address, $now, $pesan);
		 		$this->load->view('customerpkp/vmainform',$data);
			}else{
				$this->load->view('customerpkp/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu42')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomerpkpname 	= $this->input->post('ecustomerpkpname', TRUE);
			$ecustomerpkpaddress	= $this->input->post('ecustomerpkpaddress', TRUE);
			$ecustomerpkpnpwp	= $this->input->post('ecustomerpkpnpwp', TRUE);
			$this->load->model('customerpkp/mmaster');
			$this->mmaster->update($icustomer,$ecustomerpkpname,$ecustomerpkpaddress,$ecustomerpkpnpwp);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Update Master PKP (Pelanggan):('.$icustomer.') -'.$ecustomerpkpname;
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);

	        $config['base_url'] = base_url().'index.php/customerpkp/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_pkp');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('master_customerpkp');
			$data['icustomer']='';
			$cari = $this->input->post('cari', FALSE);
			/*$cari = strtoupper($cari);*/
			$this->load->model('customerpkp/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customerpkp/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu42')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer	= $this->uri->segment(4);
			$this->load->model('customerpkp/mmaster');
			$this->mmaster->delete($icustomer);

			$sess=$this->session->userdata('session_id');
	        $id=$this->session->userdata('user_id');
	        $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	        $rs		= pg_query($sql);
	        if(pg_num_rows($rs)>0){
	          while($row=pg_fetch_assoc($rs)){
		          $ip_address	  = $row['ip_address'];
		          break;
	          }
	        }else{
	          $ip_address='kosong';
	        }
	        $query 	= pg_query("SELECT current_timestamp as c");
	        while($row=pg_fetch_assoc($query)){
	          $now	  = $row['c'];
	        }
	        $pesan='Delete Master PKP (Pelanggan):('.$icustomer.')';
	        $this->load->model('logger');
	        $this->logger->write($id, $ip_address, $now, $pesan);
			$config['base_url'] = base_url().'index.php/customerpkp/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_customer_pkp');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['page_title'] = $this->lang->line('master_customerpkp');
			$data['icustomer']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('customerpkp/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu42')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			/*$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);	
			$config['base_url'] = base_url().'index.php/customerpkp/cform/index/';*/

			$cari=($this->input->post("cari",false));
			if($cari=='')$cari=$this->uri->segment(4);
			if($cari=='zxvf'){
				$cari='';
				$config['base_url'] = base_url().'index.php/customerpkp/cform/cari/zxvf/';
			}else{
				$config['base_url'] = base_url().'index.php/customerpkp/cform/cari/'.$cari.'/';
			}

			$query = $this->db->query("	select * from tr_customer_pkp
						   	where upper(e_customer_pkpname) ilike '%$cari%' or upper(i_customer) ilike '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('customerpkp/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_customerpkp');
			$data['icustomer']='';
	 		$this->load->view('customerpkp/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
