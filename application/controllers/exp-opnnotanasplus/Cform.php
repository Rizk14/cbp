<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationx');
      $this->load->library('fungsi');
      require_once("php/fungsi.php");
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu238')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['page_title'] = $this->lang->line('opntole');
         $data['dto']='';
      $data['nt']='';
      $data['jt']='';
         $this->load->view('exp-opnnotanasplus/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu238')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['page_title'] = $this->lang->line('opntole');
         $this->load->view('exp-opnnotanasplus/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu238')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
         {
            $this->load->model('exp-opnnotanasplus/mmaster');
            $dto  = $this->input->post('dto');
            $nt   = $this->input->post('chkntx');
            $jt   = $this->input->post('chkjtx');
            if($dto=='')$dto=$this->uri->segment(4);
            if($nt=='') $nt=$this->uri->segment(5);
            if($jt=='') $jt=$this->uri->segment(6);
            $this->load->model('exp-opnnotanasplus/mmaster');
            if($dto!='')
            {
               $tmp=explode("-",$dto);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $dto=$th."-".$bl."-".$hr;
               //$peri=$hr.' '.mbulan($bl).' '.$th;
               $peri = substr($th,2,2).$bl;
            }

            if($nt=='qqq')
            {
               $this->db->select("  distinct
                                    b.i_area,
                                    b.e_area_shortname,
                                    b.e_area_name,
                                    c.i_customer,
                                    c.e_customer_name,
                                    a.i_nota,
                                    a.d_nota,
                                    c.n_customer_toplength_print,
                                    c.n_customer_toplength,
                                    a.d_jatuh_tempo,
                                   
                                    /*case when substring(a.i_sj,9,2)='00' 
                                    then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                                    else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
                                    end as d_jatuh_tempo_plustoleransi,

                                    case when substring(a.i_sj,9,2)='00' 
                                    then g.n_toleransi_pusat
                                    else g.n_toleransi_cabang
                                    end as n_toleransi,*/
                                   
                                    h.f_spb_stockdaerah,
                                    a.v_nota_netto,
                                    a.v_sisa,
                                    a.i_sj,
                                    f.e_salesman_name,
                                    x.e_pelunasan_remark, 
                                    d.e_remark as remlunas

                                    from tm_spb h, tr_product_group i, tm_nota a
                                    left join tr_area b on b.i_area = a.i_area
                                    left join tr_customer c on c.i_customer = a.i_customer
                                    left join tr_salesman f on f.i_salesman=a.i_salesman
                                    left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                                    left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
                                    left join tm_pelunasan e on (d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area and e.f_pelunasan_cancel='f')
                                    left join tr_city g on c.i_city = g.i_city and c.i_area = g.i_area
                                    where
                                    a.d_nota<='$dto' and a.v_sisa>0 and a.f_nota_cancel='f'
                                    and a.i_spb = h.i_spb and a.i_area = h.i_area and h.i_product_group = i.i_product_group
                                    order by b.i_area, c.e_customer_name, a.i_nota desc",false);

               /*$this->db->select("  distinct
                                    a.i_area,
                                    area.e_area_shortname,
                                    area.e_area_name,
                                    a.i_customer,
                                    c.e_customer_name,
                                    a.i_nota,
                                    a.d_nota,
                                    a.n_nota_toplength,
                                    a.d_jatuh_tempo,
                                    y.n_toleransi_cabang,
                                    z.f_spb_stockdaerah,
                                    a.v_nota_netto,
                                    a.v_sisa,
                                    a.i_sj,
                                    b.e_salesman_name,
                                    x.e_pelunasan_remark, 
                                    d.e_remark as remlunas

                                    from tr_customer c, tr_salesman b, 
                                    tm_nota a
                                    left join tr_area area on (area.i_area=a.i_area)
                                    left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                                    left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
                                    left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan
                                    and d.i_dt=e.i_dt and d.i_area=e.i_area
                                    and e.f_pelunasan_cancel='f')
                                    left join tm_spb z on(z.i_customer=a.i_customer 
                                    and z.i_area=a.i_area and z.i_nota=a.i_nota and z.i_spb=a.i_spb)
                                    left join tr_city y on(y.i_area=a.i_area)

                                    where
                                    a.d_nota<='$dto'
                                    and a.i_area=c.i_area 
                                    and a.v_sisa>0 
                                    and a.i_salesman=b.i_salesman
                                    and a.i_customer=c.i_customer 
                                    and a.f_nota_cancel='f'
                                    and y.i_city=c.i_city

                                    order by a.i_area, c.e_customer_name, a.i_nota desc",false);*/

               /*$this->db->select(" distinct
                                    a.*,b.e_salesman_name,
                                    c.e_customer_name,
                                    x.e_pelunasan_remark, d.e_remark as remlunas,
                                    area.e_area_shortname,
                                    area.e_area_name
                                 from tr_customer c, tr_salesman b, tm_nota a
                                 left join tr_area area on area.i_area=a.i_area
                                 left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                                 left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
                                 left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan
                                       and d.i_dt=e.i_dt and d.i_area=e.i_area
                                       and e.f_pelunasan_cancel='f')
                                 where a.d_nota<='$dto' and a.v_sisa>0 and a.i_salesman=b.i_salesman
                                       and a.i_customer=c.i_customer and a.f_nota_cancel='f'
                                 order by a.i_area, c.e_customer_name, a.i_nota desc",false);*/
            }
            else
            {
               $this->db->select(" distinct
                                    b.i_area,
                                    b.e_area_shortname,
                                    b.e_area_name,
                                    c.i_customer,
                                    c.e_customer_name,
                                    a.i_nota,
                                    a.d_nota,
                                    c.n_customer_toplength_print,
                                    c.n_customer_toplength
                                    a.d_jatuh_tempo,
                                   
                                    /*case when substring(a.i_sj,9,2)='00' 
                                    then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                                    else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
                                    end as d_jatuh_tempo_plustoleransi,

                                    case when substring(a.i_sj,9,2)='00' 
                                    then g.n_toleransi_pusat
                                    else g.n_toleransi_cabang
                                    end as n_toleransi,*/
                                   
                                    h.f_spb_stockdaerah,
                                    a.v_nota_netto,
                                    a.v_sisa,
                                    a.i_sj,
                                    f.e_salesman_name,
                                    x.e_pelunasan_remark, 
                                    d.e_remark as remlunas

                                    from tm_spb h, tr_product_group i, tm_nota a
                                    left join tr_area b on b.i_area = a.i_area
                                    left join tr_customer c on c.i_customer = a.i_customer
                                    left join tr_salesman f on f.i_salesman=a.i_salesman
                                    left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                                    left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
                                    left join tm_pelunasan e on (d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area and e.f_pelunasan_cancel='f')
                                    left join tr_city g on c.i_city = g.i_city and c.i_area = g.i_area
                                    where
                                    substr(a.i_nota,4,4)='$peri' and a.v_sisa>0 and a.f_nota_cancel='f'
                                    and a.i_spb = h.i_spb and a.i_area = h.i_area and h.i_product_group = i.i_product_group
                                    order by b.i_area, c.e_customer_name, a.i_nota desc",false);
            }
            $query = $this->db->get();
            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("Opname Nota")->setDescription(NmPerusahaan);
            $objPHPExcel->setActiveSheetIndex(0);
            if ($query->num_rows() > 0)
            {
               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'   => 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A2:A4'
               );
               $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);   //NO
               $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(6);   //AREA
               $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);  //NAMA AREA
               $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(6);   //KODE LANG
               $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);  //NAMA TOKO
               $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);  //TOP INTERN
               $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(8);  //TOP NOTA
               $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);  //STOCK DAERAH
               $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);   //NO NOTA
               $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(9);   //TANGGAL NOTA
               $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);  //TANGGAL JATUH TEMPO NOTA
               $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);  //TANGGAL JATUH TEMPO+TOP INTERN
               $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(8);  //JUMLAH
               $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(8);  //SISA
               $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(9);   //LAMA
               $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(12);  //NO SJ
               $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);  //SALESMAN
               $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(6);   //STATUS

               $objPHPExcel->getActiveSheet()->setCellValue('A2', 'DAFTAR NOTA YANG BELUM LUNAS PER '.$peri);
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,14,2);
               $objPHPExcel->getActiveSheet()->setCellValue('A3', "Nasional");
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,14,3);

               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'   => 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => false
                     )
                  ),
                  'A5:R5'
               );


               $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
               $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Area');
               $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Nama Area');
               $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('D5', 'KdLang');
               $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Nama');
               $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('F5', 'TOP Intern');
               $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('G5', 'TOP Nota');
               $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Stock Daerah');
               $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('I5', 'No Nota');
               $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               /*$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Toleransi');
               $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );*/
               $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Tanggal Nota');
               $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Tanggal Jatuh Tempo(Intern)');
               $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Tanggal Jatuh Tempo(Nota)');
               $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('M5', 'Jumlah');
               $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('N5', 'Sisa');
               $objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('O5', 'Lama');
               $objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('P5', 'No. SJ');
               $objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('Q5', 'Salesman');
               $objPHPExcel->getActiveSheet()->getStyle('Q5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('R5', 'Status');
               $objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
                
               $i=6;
               $j=6;
               $xarea='';
               $saldo=0;
               $no=1;
               $nonota='';
               $epelunasan_remark='';

               foreach($query->result() as $row)
               {
                  $objPHPExcel->getActiveSheet()->duplicateStyleArray
                  (
                     array(
                        'font' => array
                        (
                           'name'   => 'Arial',
                           'bold'   => false,
                           'italic' => false,
                           'size'   => 10
                        )
                     ),
                     'A'.$i.':R'.$i
                  );

                  $status=$row->v_nota_netto-$row->v_sisa;
                  if($status==0){
                    $ketstatus="Utuh";
                  }else{
                    $ketstatus="Sisa";
                  }

//TANGGAL NOTA
                  $tmp1    = explode("-", $row->d_nota);
                  $th1     = $tmp1[0];
                  $bl1     = $tmp1[1];
                  $hr1     = $tmp1[2];
                  $dnota   = $hr1.'-'.$bl1.'-'.$th1;

//TANGGAL JATUH TEMPO NOTA
                  $jt1        = $this->fungsi->dateAdd("d",$row->n_customer_toplength_print,$dnota);
                  $jtnota     = $jt1;

                  $tmpx     = explode("-", $jtnota);
                  $yir1     = $tmpx[0];
                  $mon1     = $tmpx[1];
                  $det1     = $tmpx[2];
                  $row->d_jatuh_tempo_nota   = $yir1."-".$mon1."-".$det1;
//END TANGGAL JATUH TEMPO NOTA                  

//TANGGAL JATUH TEMPO INTERN
                  $jt2        = $this->fungsi->dateAdd("d",$row->n_customer_toplength,$dnota);
                  $jtintern   = $jt2;

                  $tmp2                      = explode("-", $jtintern);
                  $yir2                      = $tmp2[0];
                  $mon2                      = $tmp2[1];
                  $det2                      = $tmp2[2];
                  $row->d_jatuh_tempo_intern = $yir2."-".$mon2."-".$det2;
//END TANGGAL JATUH TEMPO INTERN                  
      
                  if( $nonota <> $row->i_nota)
                  {
                     $lama=datediff("d",$row->d_jatuh_tempo_nota,date("d-m-Y"),false);
                     $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no);
                     $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(1,$i, $row->i_area.'-'.$row->e_area_shortname, Cell_DataType::TYPE_STRING);
                     $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(2,$i, $row->e_area_name, Cell_DataType::TYPE_STRING);
                     $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(3,$i, "'".$row->i_customer, Cell_DataType::TYPE_NUMERIC;

//KASIH WARNA STATUS JATUH TEMPO OPNAME NOTA
                     if($lama>0 && $lama<=7){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':R'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF00FF00');
                     }elseif($lama>8 && $lama<=15){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':R'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
                     }elseif($lama>16){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':R'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF0000');
                     }
//END
                     $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->e_customer_name);
                     $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(5,$i, $row->i_nota, Cell_DataType::TYPE_STRING);
                     $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->n_customer_toplength);
                     $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->n_customer_toplength_print);

                     if($row->f_spb_stockdaerah=='t'){
                        $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, "Ya");
                     }else{
                        $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, "Tidak");
                     }

                     $sj = substr($row->i_sj,9,2);

                     $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
                     $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->d_nota, Cell_DataType::TYPE_STRING);
                     $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->d_jatuh_tempo_intern, Cell_DataType::TYPE_STRING);
                     $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row->d_jatuh_tempo_nota, Cell_DataType::TYPE_STRING);
                     $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $row->v_nota_netto);
                     $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $row->v_sisa);
                     $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $lama);
                     $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $row->i_sj);
                     $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $row->e_salesman_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $ketstatus);
                     
                     /*var_dump($row->d_jatuh_tempo_nota,$row->d_jatuh_tempo_intern);
                     die;*/
                     
                     $no++;
                     $i++;
                     $j++;
                  }
                  
                  $nonota=$row->i_nota;
               }
               $x=$i-1;
            }
            $objPHPExcel->getActiveSheet()->getStyle('M6:N'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $nama='OPN+TOPIntern'.$dto.'.xls';
            #$area='00';
            if(file_exists('mimin/'.$nama))
            {
               @chmod('mimin/'.$nama, 0777);
               @unlink('mimin/'.$nama);
            }
            $objWriter->save('mimin/'.$nama);
            @chmod('mimin/'.$nama, 0777);

               $sess    = $this->session->userdata('session_id');
               $id      = $this->session->userdata('user_id');
               $sql     = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
               $now    = $row['c'];
               }
               $pesan='Export Opname Nota+TOP Intern sampai tanggal:'.$dto;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );

            $data['sukses']   = true;
            $data['inomor']   = "Export Opname Nota+TOP Intern berhasil ke file ".$nama;
            $this->load->view('nomor',$data);
         }
         else
         {
            $this->load->view('awal/index.php');
      }
   }
}
?>
