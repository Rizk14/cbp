<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnamenotanas');
			$data['dto']='';
      $data['nt']='';
      $data['jt']='';
			$this->load->view('exp-opnnotanas/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu238')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('opnamenotanas');
			$this->load->view('exp-opnnotanas/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu238')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
         {
            $this->load->model('exp-opnnotanas/mmaster');
            $dto	= $this->input->post('dto');
            $nt   = $this->input->post('chkntx');
            $jt   = $this->input->post('chkjtx');
            if($dto=='')$dto=$this->uri->segment(4);
            if($nt=='') $nt=$this->uri->segment(5);
            if($jt=='') $jt=$this->uri->segment(6);
            $this->load->model('exp-opnnotanas/mmaster');
            if($dto!='')
            {
               $tmp=explode("-",$dto);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $dto=$th."-".$bl."-".$hr;
               $peri=$hr.' '.mbulan($bl).' '.$th;
            }

            if($nt=='qqq')
            {
               $this->db->select(" distinct 
                                    a.*,
                                    c.e_customer_name,
                                    x.e_pelunasan_remark, d.e_remark
                                 from tr_customer c, tm_nota a
                                 left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                                 left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
                                 left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan
                                       and d.i_dt=e.i_dt and d.i_area=e.i_area
                                       and e.f_pelunasan_cancel='f')
                                 where a.d_nota<='$dto' and a.v_sisa>0
                                       and a.i_customer=c.i_customer and a.f_nota_cancel='f'
                                 order by a.i_area, c.e_customer_name, a.i_nota desc",false);
            }
            else
            {
               $this->db->select(" distinct 
                                    a.*,
                                    c.e_customer_name,
                                    x.e_pelunasan_remark, d.e_remark
                                 from tr_customer c, tm_nota a
                                 left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                                 left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
                                 left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan
                                       and d.i_dt=e.i_dt and d.i_area=e.i_area 
                                       and e.f_pelunasan_cancel='f')
                                 where a.d_jatuh_tempo<='$dto' and a.v_sisa>0
                                    and a.i_customer=c.i_customer
                                    and a.f_nota_cancel='f'
                                 order by a.i_area, c.e_customer_name, a.i_nota desc",false);
            }
            $query = $this->db->get();
            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("Opname Nota")->setDescription("PT. Dialogue Garmindo Utama");
            $objPHPExcel->setActiveSheetIndex(0);
            if ($query->num_rows() > 0)
            {
               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'	=> 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A2:A4'
               );
               $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
               $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(4);
               $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
               $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(9);
               $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(9);
               $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
               $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(12);

               $objPHPExcel->getActiveSheet()->setCellValue('A2', 'DAFTAR NOTA YANG BELUM LUNAS PER '.$peri);
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,10,2);
               $objPHPExcel->getActiveSheet()->setCellValue('A3', "Nasional");
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,10,3);

               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'	=> 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A5:L5'
               );


               $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
               $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Area');
               $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('C5', 'KdLang');
               $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Nama');
               $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('E5', 'No. Nota');
               $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Tgl. Nota');
               $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Tgl. JT');
               $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Jumlah');
               $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Sisa Tagihan');
               $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Lama');
               $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Keterangan');
               $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('L5', 'No. SJ');
               $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top' 	=> array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $i=6;
               $j=6;
               $xarea='';
               $saldo=0;
               $no=1;
               $nonota='';
               $epelunasan_remark='';
               foreach($query->result() as $row)
               {
                  $objPHPExcel->getActiveSheet()->duplicateStyleArray
                  (
                     array(
                        'font' => array
                        (
                           'name'   => 'Arial',
                           'bold'   => false,
                           'italic' => false,
                           'size'   => 10
                        )
                     ),
                     'A'.$i.':L'.$i
                  );

                  if( $nonota <> $row->i_nota)
                  {
                     $lama=datediff("d",$row->d_jatuh_tempo,date("Y-m-d"),false);

                     $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no);
                     $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
                     $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, "'".$row->i_customer);
                     $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->e_customer_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->i_nota);
                     $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->d_nota);
                     $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->d_jatuh_tempo);
                     $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->v_nota_netto);
                     $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->v_sisa);
                     $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $lama);
                     if($row->e_pelunasan_remark!='')
                     {
                        $epelunasan_remark = $row->e_pelunasan_remark.",".$row->e_remark;
                        $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $epelunasan_remark);
                     }
                     $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row->i_sj);
                     $no++;
                     $i++;
                     $j++;
                  }
                  else
                  {
                     if($row->e_pelunasan_remark!='')
                     {
                        $k = $i-1;
                        $epelunasan_remark = $epelunasan_remark.",".$row->e_pelunasan_remark.",".$row->e_remark;
                        $objPHPExcel->getActiveSheet()->setCellValue('K'.$k, $epelunasan_remark);
                     }
                  }
                  $nonota=$row->i_nota;
               }
               $x=$i-1;
            }
            $objPHPExcel->getActiveSheet()->getStyle('H6:I'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $nama='OPNALL'.$dto.'.xls';
            $area='00';#$iarea;
/*
            switch($iarea){
              case '06':
                $area='23';
                break;
              case '08':
                $area='01';
                break;
              case '14':
                $area='12';
                break;
              case '15':
                $area='12';
                break;
              case '18':
                $area='11';
                break;
              case '19':
                $area='11';
                break;
              case '20':
                $area='07';
                break;
              case '21':
                $area='13';
                break;
              case '22':
                $area='09';
                break;
              case '24':
                $area='05';
                break;
              case '26':
                $area='07';
                break;
              case '27':
                $area='11';
                break;
              case '28':
                $area='11';
                break;
              case '29':
                $area='11';
                break;
              case '30':
                $area='11';
                break;
              case '32':
                $area='11';
                break;
              case '33':
                $area='11';
                break;
            }
*/
            if(file_exists('excel/'.$area.'/'.$nama))
            {
               chmod('excel/'.$area.'/'.$nama, 0777);
               @unlink('excel/'.$area.'/'.$nama);
            }
            $objWriter->save("excel/".$area.'/'.$nama); 

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
	          while($row=pg_fetch_assoc($query)){
	          	$now	  = $row['c'];
			      }
			      $pesan='Export Opname Nota sampai tanggal:'.$dto.' area:'.$area;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan ); 

            $data['sukses']   = true;
            $data['inomor']   = "Ekspor Opname Nota Nasional berhasil ke file ".$nama;
            $this->load->view('nomor',$data);
         }
         else
         {
            $this->load->view('awal/index.php');
      }
   }
}
?>
