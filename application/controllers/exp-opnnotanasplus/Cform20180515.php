<?php 
class Cform extends CI_Controller {
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      $this->load->library('paginationx');
      $this->load->library('fungsi');
      require_once("php/fungsi.php");
   }
   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu238')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['page_title'] = $this->lang->line('opntole');
         $data['dto']='';
      $data['nt']='';
      $data['jt']='';
         $this->load->view('exp-opnnotanasplus/vmainform', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function insert_fail()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu238')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
      ){
         $data['page_title'] = $this->lang->line('opntole');
         $this->load->view('exp-opnnotanasplus/vinsert_fail',$data);
      }else{
         $this->load->view('awal/index.php');
      }
   }
   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu238')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
         {
            $this->load->model('exp-opnnotanasplus/mmaster');
            $dto  = $this->input->post('dto');
            $nt   = $this->input->post('chkntx');
            $jt   = $this->input->post('chkjtx');
            if($dto=='')$dto=$this->uri->segment(4);
            if($nt=='') $nt=$this->uri->segment(5);
            if($jt=='') $jt=$this->uri->segment(6);
            $this->load->model('exp-opnnotanasplus/mmaster');
            if($dto!='')
            {
               $tmp=explode("-",$dto);
               $th=$tmp[2];
               $bl=$tmp[1];
               $hr=$tmp[0];
               $dto=$th."-".$bl."-".$hr;
               //$peri=$hr.' '.mbulan($bl).' '.$th;
               $peri = substr($th,2,2).$bl;
            }

            if($nt=='qqq')
            {
               $this->db->select("  distinct
                                    b.i_area,
                                    b.e_area_shortname,
                                    b.e_area_name,
                                    c.i_customer,
                                    c.e_customer_name,
                                    a.i_nota,
                                    a.d_nota,
                                    a.n_nota_toplength,
                                    a.d_jatuh_tempo,
                                   
                                    case when substring(a.i_sj,9,2)='00' 
                                    then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                                    else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
                                    end as d_jatuh_tempo_plustoleransi,

                                    case when substring(a.i_sj,9,2)='00' 
                                    then g.n_toleransi_pusat
                                    else g.n_toleransi_cabang
                                    end as n_toleransi,
                                   
                                    h.f_spb_stockdaerah,
                                    a.v_nota_netto,
                                    a.v_sisa,
                                    a.i_sj,
                                    f.e_salesman_name,
                                    x.e_pelunasan_remark, 
                                    d.e_remark as remlunas

                                    from tm_spb h, tr_product_group i, tm_nota a
                                    left join tr_area b on b.i_area = a.i_area
                                    left join tr_customer c on c.i_customer = a.i_customer
                                    left join tr_salesman f on f.i_salesman=a.i_salesman
                                    left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                                    left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
                                    left join tm_pelunasan e on (d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area and e.f_pelunasan_cancel='f')
                                    left join tr_city g on c.i_city = g.i_city and c.i_area = g.i_area
                                    where
                                    a.d_nota<='$dto' and a.v_sisa>0 and a.f_nota_cancel='f'
                                    and a.i_spb = h.i_spb and a.i_area = h.i_area and h.i_product_group = i.i_product_group
                                    order by b.i_area, c.e_customer_name, a.i_nota desc",false);

               /*$this->db->select("  distinct
                                    a.i_area,
                                    area.e_area_shortname,
                                    area.e_area_name,
                                    a.i_customer,
                                    c.e_customer_name,
                                    a.i_nota,
                                    a.d_nota,
                                    a.n_nota_toplength,
                                    a.d_jatuh_tempo,
                                    y.n_toleransi_cabang,
                                    z.f_spb_stockdaerah,
                                    a.v_nota_netto,
                                    a.v_sisa,
                                    a.i_sj,
                                    b.e_salesman_name,
                                    x.e_pelunasan_remark, 
                                    d.e_remark as remlunas

                                    from tr_customer c, tr_salesman b, 
                                    tm_nota a
                                    left join tr_area area on (area.i_area=a.i_area)
                                    left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                                    left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
                                    left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan
                                    and d.i_dt=e.i_dt and d.i_area=e.i_area
                                    and e.f_pelunasan_cancel='f')
                                    left join tm_spb z on(z.i_customer=a.i_customer 
                                    and z.i_area=a.i_area and z.i_nota=a.i_nota and z.i_spb=a.i_spb)
                                    left join tr_city y on(y.i_area=a.i_area)

                                    where
                                    a.d_nota<='$dto'
                                    and a.i_area=c.i_area 
                                    and a.v_sisa>0 
                                    and a.i_salesman=b.i_salesman
                                    and a.i_customer=c.i_customer 
                                    and a.f_nota_cancel='f'
                                    and y.i_city=c.i_city

                                    order by a.i_area, c.e_customer_name, a.i_nota desc",false);*/

               /*$this->db->select(" distinct
                                    a.*,b.e_salesman_name,
                                    c.e_customer_name,
                                    x.e_pelunasan_remark, d.e_remark as remlunas,
                                    area.e_area_shortname,
                                    area.e_area_name
                                 from tr_customer c, tr_salesman b, tm_nota a
                                 left join tr_area area on area.i_area=a.i_area
                                 left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                                 left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
                                 left join tm_pelunasan e on(d.i_pelunasan=e.i_pelunasan
                                       and d.i_dt=e.i_dt and d.i_area=e.i_area
                                       and e.f_pelunasan_cancel='f')
                                 where a.d_nota<='$dto' and a.v_sisa>0 and a.i_salesman=b.i_salesman
                                       and a.i_customer=c.i_customer and a.f_nota_cancel='f'
                                 order by a.i_area, c.e_customer_name, a.i_nota desc",false);*/
            }
            else
            {
               $this->db->select(" distinct
                                    b.i_area,
                                    b.e_area_shortname,
                                    b.e_area_name,
                                    c.i_customer,
                                    c.e_customer_name,
                                    a.i_nota,
                                    a.d_nota,
                                    a.n_nota_toplength,
                                    a.d_jatuh_tempo,
                                   
                                    case when substring(a.i_sj,9,2)='00' 
                                    then a.d_jatuh_tempo + interval '1' day * g.n_toleransi_pusat
                                    else a.d_jatuh_tempo + interval '1' day * g.n_toleransi_cabang
                                    end as d_jatuh_tempo_plustoleransi,

                                    case when substring(a.i_sj,9,2)='00' 
                                    then g.n_toleransi_pusat
                                    else g.n_toleransi_cabang
                                    end as n_toleransi,
                                   
                                    h.f_spb_stockdaerah,
                                    a.v_nota_netto,
                                    a.v_sisa,
                                    a.i_sj,
                                    f.e_salesman_name,
                                    x.e_pelunasan_remark, 
                                    d.e_remark as remlunas

                                    from tm_spb h, tr_product_group i, tm_nota a
                                    left join tr_area b on b.i_area = a.i_area
                                    left join tr_customer c on c.i_customer = a.i_customer
                                    left join tr_salesman f on f.i_salesman=a.i_salesman
                                    left join tm_pelunasan_item d on(a.i_nota=d.i_nota)
                                    left join tr_pelunasan_remark x on(x.i_pelunasan_remark=d.i_pelunasan_remark)
                                    left join tm_pelunasan e on (d.i_pelunasan=e.i_pelunasan and d.i_dt=e.i_dt and d.i_area=e.i_area and e.f_pelunasan_cancel='f')
                                    left join tr_city g on c.i_city = g.i_city and c.i_area = g.i_area
                                    where
                                    substr(a.i_nota,4,4)='$peri' and a.v_sisa>0 and a.f_nota_cancel='f'
                                    and a.i_spb = h.i_spb and a.i_area = h.i_area and h.i_product_group = i.i_product_group
                                    order by b.i_area, c.e_customer_name, a.i_nota desc",false);
            }
            $query = $this->db->get();
            $this->load->library('PHPExcel');
            $this->load->library('PHPExcel/IOFactory');
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getProperties()->setTitle("Opname Nota")->setDescription(NmPerusahaan);
            $objPHPExcel->setActiveSheetIndex(0);
            if ($query->num_rows() > 0)
            {
               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'   => 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A2:A4'
               );
               $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
               $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
               $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(9);
               $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(9);
               $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
               $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(12);
               $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
               $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(6);
               $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(6);

               $objPHPExcel->getActiveSheet()->setCellValue('A2', 'DAFTAR NOTA YANG BELUM LUNAS PER '.$peri);
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,14,2);
               $objPHPExcel->getActiveSheet()->setCellValue('A3', "Nasional");
               $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,14,3);

               $objPHPExcel->getActiveSheet()->duplicateStyleArray(
                  array(
                     'font' => array(
                        'name'   => 'Arial',
                        'bold'  => true,
                        'italic'=> false,
                        'size'  => 10
                     ),
                     'alignment' => array(
                        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                     )
                  ),
                  'A5:Q5'
               );


               $objPHPExcel->getActiveSheet()->setCellValue('A5', 'No');
               $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Area');
               $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('C5', 'Nama Area');
               $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('D5', 'KdLang');
               $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Nama');
               $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Stok Daerah');
               $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('G5', 'No. Nota');
               $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('H5', 'Tgl. Nota');
               $objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('I5', 'Tgl. JT');
               $objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               /*$objPHPExcel->getActiveSheet()->setCellValue('J5', 'Toleransi');
               $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );*/
               $objPHPExcel->getActiveSheet()->setCellValue('J5', 'Tgl JT TOP Intern');
               $objPHPExcel->getActiveSheet()->getStyle('J5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('K5', 'Jumlah');
               $objPHPExcel->getActiveSheet()->getStyle('K5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     )
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('L5', 'Sisa');
               $objPHPExcel->getActiveSheet()->getStyle('L5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('M5', 'Lama');
               $objPHPExcel->getActiveSheet()->getStyle('M5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('N5', 'No. SJ');
               $objPHPExcel->getActiveSheet()->getStyle('N5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('O5', 'Salesman');
               $objPHPExcel->getActiveSheet()->getStyle('O5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               $objPHPExcel->getActiveSheet()->setCellValue('P5', 'Status');
               $objPHPExcel->getActiveSheet()->getStyle('P5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
               
                /*$objPHPExcel->getActiveSheet()->setCellValue('R5', 'Keterangan');
               $objPHPExcel->getActiveSheet()->getStyle('R5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );
                $objPHPExcel->getActiveSheet()->setCellValue('S5', 'Ket2');
               $objPHPExcel->getActiveSheet()->getStyle('S5')->applyFromArray(
                  array(
                     'borders' => array(
                        'top'    => array('style' => Style_Border::BORDER_THIN)
                     ),
                  )
               );*/
               $i=6;
               $j=6;
               $xarea='';
               $saldo=0;
               $no=1;
               $nonota='';
               $epelunasan_remark='';

               foreach($query->result() as $row)
               {
                  $objPHPExcel->getActiveSheet()->duplicateStyleArray
                  (
                     array(
                        'font' => array
                        (
                           'name'   => 'Arial',
                           'bold'   => false,
                           'italic' => false,
                           'size'   => 10
                        )
                     ),
                     'A'.$i.':S'.$i
                  );

                  $status=$row->v_nota_netto-$row->v_sisa;
                  if($status==0){
                    $ketstatus="Utuh";
                  }else{
                    $ketstatus="Sisa";
                  }

//TANGGAL NOTA
                  $tmp1    = explode("-", $row->d_nota);
                  $th1     = $tmp1[0];
                  $bl1     = $tmp1[1];
                  $hr1     = $tmp1[2];
                  $dnota   = $hr1.'-'.$bl1.'-'.$th1;

//TANGGAL JATUH TEMPO
                  $tmp2    = explode("-", $row->d_jatuh_tempo);
                  $th2     = $tmp2[0];
                  $bl2     = $tmp2[1];
                  $hr2     = $tmp2[2];
                  $djtemp  = $hr2.'-'.$bl2.'-'.$th2;                  

//PERHITUNGAN JATUH TEMPO + TOP Intern
                  //$nnotaplus        = $row->n_toleransi;
                  $jt         = $row->d_jatuh_tempo;
                  $nnotaplus  = $row->n_nota_toplength;

                  if($nnotaplus==30||$nnotaplus==45){
                  $nnotaplus  = $nnotaplus*-1;
                  $dudet      = $this->fungsi->dateAdd("d",15,$jt);
                  $djatuh     = $dudet;

                  $tmp     = explode("-", $djatuh);
                  $yir     = $tmp[0];
                  $mon     = $tmp[1];
                  $det     = $tmp[2];
                  $djatuhtempoplus   = $det."-".$mon."-".$yir;
               }else{
                  $nnotaplus  = $nnotaplus*-1;
                  $dudet      = $this->fungsi->dateAdd("d",10,$jt);
                  $djatuh     = $dudet;

                  $tmp     = explode("-", $djatuh);
                  $yir     = $tmp[0];
                  $mon     = $tmp[1];
                  $det     = $tmp[2];
                  $djatuhtempoplus   = $det."-".$mon."-".$yir;
               }
//END PERHITUNGAN JATUH TEMPO + TOP Intern

                  if( $nonota <> $row->i_nota)
                  {
                     //echo 'baris = '.$i.'<br>';
                     $lama=datediff("d",$row->d_jatuh_tempo,date("Y-m-d"),false);
                     $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $no);
                     $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(1,$i, $row->i_area.'-'.$row->e_area_shortname, Cell_DataType::TYPE_STRING);
                     $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(2,$i, $row->e_area_name, Cell_DataType::TYPE_STRING);
                     $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(3,$i, $row->i_customer, Cell_DataType::TYPE_STRING);

//KASIH WARNA STATUS JATUH TEMPO OPNAME NOTA
                     if($lama>0 && $lama<=7){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':Q'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FF00FF00');
                     }elseif($lama>8 && $lama<=15){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':Q'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFFFF00');
                     }elseif($lama>16){
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$i.':Q'.$i)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setARGB('FFFF0000');
                     }
//END
                     $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->e_customer_name);
                     $objPHPExcel->getActiveSheet()->setCellValueExplicitByColumnAndRow(5,$i, $row->i_nota, Cell_DataType::TYPE_STRING);
                     
                     if($row->f_spb_stockdaerah=='t'){
                        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, "Ya");
                     }else{
                        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, "Tidak");
                     }

                     $sj = substr($row->i_sj,9,2);

                     if($sj=='00'){
                        $jenis = 'Pusat';
                     }else{
                        $jenis = 'Cabang';
                     }

                     $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->i_nota, Cell_DataType::TYPE_STRING);
                     $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $dnota, Cell_DataType::TYPE_STRING);
                     $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $djtemp, Cell_DataType::TYPE_STRING);
                     //$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $row->n_toleransi.' ('.$jenis.')');
                     $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, $djatuhtempoplus);
                     $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->v_nota_netto);
                     $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row->v_sisa);
                     $objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $lama);
                     //$objPHPExcel->getActiveSheet()->setCellValue(''.$i, $row->n_toleransi_cabang);
                    /* if(trim($row->e_pelunasan_remark)!='' && $row->e_pelunasan_remark!=null)
                     {
                        //$epelunasan_remark = $row->e_pelunasan_remark.",".$row->remlunas;
                        $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $epelunasan_remark);
                     }else{
                        $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $row->e_remark);
                     }*/
#                     {
#                        $epelunasan_remark = $row->e_pelunasan_remark.",".$row->e_remark;
#                        $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $epelunasan_remark);
#                     }
                     $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $row->i_sj);
                     $objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $row->e_salesman_name);
                     $objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $ketstatus);
                     
                     
                    /*if(trim($row->e_pelunasan_remark)!='' && $row->e_pelunasan_remark!=null)
                     {
                        $epelunasan_remark = $row->e_pelunasan_remark.",".$row->remlunas;
                        $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $epelunasan_remark);
                     }else{
                        $objPHPExcel->getActiveSheet()->setCellValue('R'.$i, $row->e_remark);
                     }*/
                     //$objPHPExcel->getActiveSheet()->setCellValue('S'.$i, $ketstatus);
                     $no++;
                     $i++;
                     $j++;
                  }
                  else
                  {
                     /*if($row->e_pelunasan_remark!='')
                     {
                        $k = $i-1;
                        $epelunasan_remark = $epelunasan_remark.",".$row->e_pelunasan_remark.",".$row->e_remark;
                        $objPHPExcel->getActiveSheet()->setCellValue('L'.$k, $epelunasan_remark);
                     }*/
                  }
                  $nonota=$row->i_nota;
               }
               $x=$i-1;
            }
            $objPHPExcel->getActiveSheet()->getStyle('K6:L'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
            $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $nama='OPN+TOPIntern'.$dto.'.xls';
            #$area='00';
            if(file_exists('mimin/'.$nama))
            {
               @chmod('mimin/'.$nama, 0777);
               @unlink('mimin/'.$nama);
            }
            $objWriter->save('mimin/'.$nama);
            @chmod('mimin/'.$nama, 0777);

               $sess    = $this->session->userdata('session_id');
               $id      = $this->session->userdata('user_id');
               $sql     = "select * from dgu_session where session_id='$sess' and not user_data isnull";
               $rs      = pg_query($sql);
               if(pg_num_rows($rs)>0){
                  while($row=pg_fetch_assoc($rs)){
                     $ip_address   = $row['ip_address'];
                     break;
                  }
               }else{
                  $ip_address='kosong';
               }
               $query   = pg_query("SELECT current_timestamp as c");
               while($row=pg_fetch_assoc($query)){
               $now    = $row['c'];
               }
               $pesan='Export Opname Nota+TOP Intern sampai tanggal:'.$dto;
               $this->load->model('logger');
               $this->logger->write($id, $ip_address, $now , $pesan );

            $data['sukses']   = true;
            $data['inomor']   = "Export Opname Nota+TOP Intern berhasil ke file ".$nama;
            $this->load->view('nomor',$data);
         }
         else
         {
            $this->load->view('awal/index.php');
      }
   }
}
?>
