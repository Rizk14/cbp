<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu311')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$iarea1	= $this->session->userdata('i_area');
			$dsjbr 	= $this->input->post('dsjbr', TRUE);
			if($dsjbr!=''){
				$tmp=explode("-",$dsjbr);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsjbr=$th."-".$bl."-".$hr;
#   			$thbl=substr($th,2,2).$bl;
   			$thbl=$th.$bl;
			}
			$iarea	= $this->input->post('iarea', TRUE);
			$isjpbr	= $this->input->post('isjpbr', TRUE);
			$dsjpbr	= $this->input->post('dsjpbr', TRUE);
			if($dsjpbr!=''){
				$tmp=explode("-",$dsjpbr);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsjpbr=$th."-".$bl."-".$hr;
			}
			$eareaname	= $this->input->post('eareaname', TRUE);
	    $ispg	= $this->input->post('ispg',TRUE);
			$icustomer	= $this->input->post('icustomer',TRUE);
			$vsjbr	= $this->input->post('vsjbr',TRUE);
			$vsjbr	= str_replace(',','',$vsjbr);
			$jml	  = $this->input->post('jml', TRUE);
			if($dsjbr!='' && $eareaname!='')
			{
				$gaono=true;
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					if($cek=='on'){
						$gaono=false;
					}
					if(!$gaono) break;
				}
				if(!$gaono){
					$this->db->trans_begin();
					$this->load->model('sjbretur/mmaster');
					$istore	= $this->input->post('istore', TRUE);
          if($istore=='AA'){
					  $istorelocation		= '01';
				  }elseif($istore=='PB'){
            $istorelocation		= '00';
					}else{
            $istorelocation		= 'PB';		
					}
					$istorelocationbin	= '00';
					$isjbr = $this->mmaster->runningnumbersj($iarea,$thbl);
					$this->mmaster->insertsjheader($isjbr,$dsjbr,$iarea,$vsjbr);
					$this->mmaster->updatesjpbr($isjpbr,$iarea,$isjbr,$dsjbr);
					for($i=1;$i<=$jml;$i++){
						$cek=$this->input->post('chk'.$i, TRUE);
						if($cek=='on'){
						  $iproduct		  = $this->input->post('iproduct'.$i, TRUE);
						  $iproductgrade= 'A';
						  $iproductmotif= $this->input->post('motif'.$i, TRUE);
						  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
						  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
						  $vunitprice		= str_replace(',','',$vunitprice);
						  $nretur	     	= $this->input->post('nretur'.$i, TRUE);
						  $nretur		    = str_replace(',','',$nretur);
						  $nreceive  		= $this->input->post('nreceive'.$i, TRUE);
						  $nreceive     = str_replace(',','',$nreceive);
						  if($nretur>0){
							  $this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$nreceive,$nretur,
			                      $vunitprice,$isjbr,$dsjbr,$iarea,$istore,$istorelocation,$istorelocationbin,$i);
							  $this->mmaster->updatesjpbr($isjpbr,$dsjpbr,$isjbr,$iarea);
###
                $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                if(isset($trans)){
                  foreach($trans as $itrans)
                  {
                    $q_aw =$itrans->n_quantity_awal;
                    $q_ak =$itrans->n_quantity_akhir;
                    $q_in =$itrans->n_quantity_in;
                    $q_out=$itrans->n_quantity_out;
                    break;
                  }
                }else{
                  $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
                  if(isset($trans)){
                    foreach($trans as $itrans)
                    {
                      $q_aw =$itrans->n_quantity_stock;
                      $q_ak =$itrans->n_quantity_stock;
                      $q_in =0;
                      $q_out=0;
                      break;
                    }
                  }else{
                    $q_aw=0;
                    $q_ak=0;
                    $q_in=0;
                    $q_out=0;
                  }
                }
                $this->mmaster->inserttrans04($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isjbr,$q_in,$q_out,$nretur,$q_aw,$q_ak);
                $th=substr($dsjbr,0,4);
                $bl=substr($dsjbr,5,2);
                $emutasiperiode=$th.$bl;
		
                if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
                {
                  $this->mmaster->updatemutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nretur,$emutasiperiode);
                }else{
                  $this->mmaster->insertmutasi4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nretur,$emutasiperiode,$q_aw);
                }
                if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
                {
                  $this->mmaster->updateic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nretur,$q_ak);
                }else{
                  $this->mmaster->insertic4($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nretur,$q_aw);
                }
###              
						  }else{
                #$this->mmaster->updatespbitem($ispb,$iproduct,$iproductgrade,$iproductmotif,$ndeliver,$iarea);
              }
						}else{
						  $iproduct		= $this->input->post('iproduct'.$i, TRUE);
						  $iproductgrade	= 'A';
						  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
              //$this->mmaster->updatesjpbritem($ispb,$iproduct,$iproductgrade,$iproductmotif,0,$iarea);
            }
					}
					if ( ($this->db->trans_status() === FALSE) )
					{
						$this->db->trans_rollback();
					}else{
#						$this->db->trans_rollback();
						$this->db->trans_commit();
						$sess=$this->session->userdata('session_id');
						$id=$this->session->userdata('user_id');
						$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
						$rs		= pg_query($sql);
						if(pg_num_rows($rs)>0){
							while($row=pg_fetch_assoc($rs)){
								$ip_address	  = $row['ip_address'];
								break;
							}
						}else{
							$ip_address='kosong';
						}
						$query 	= pg_query("SELECT current_timestamp as c");
						while($row=pg_fetch_assoc($query)){
							$now	  = $row['c'];
						}
						$pesan='Input SJBR No:'.$isjbr;
						$this->load->model('logger');
						$this->logger->write($id, $ip_address, $now , $pesan );
						$data['sukses']			= true;
						$data['inomor']			= $isjbr;
						$this->load->view('nomor',$data);
					}
				}
			}else{
				$data['page_title'] = $this->lang->line('sjbr');
				$data['isjbr']='';
				$data['detail']="";
				$data['jmlitem']="";
				$data['dsjbr']='';
				$data['isjpbr']='';
				$data['dsjpbr']='';
        if($this->uri->segment(5)!='')
        {
				  $data['iarea']=$this->uri->segment(5);
          $iarea=$this->uri->segment(5);
				  $data['istore']=$this->uri->segment(6);
				  $data['eareaname'] =str_replace('%20',' ' ,$this->uri->segment(4));
        }else{
				  $data['iarea']     ='';
          $iarea='';
				  $data['istore']    ='';
				  $data['eareaname'] ='';
        }
				$data['ispg']      ='';
				$data['vsjbr']    ='';
				$data['icustomer']   ='';
				$data['ecustomername']   ='';
        $data['dsjbr']        =date('Y-m-d');
				$this->load->view('sjbretur/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu311')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('sjbr');
			$this->load->view('sjbretur/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			($this->session->userdata('logged_in'))
		){
			$data['page_title'] = $this->lang->line('sjbr')." update";
			if($this->uri->segment(4)!=''){
				$isjbr	= $this->uri->segment(4);
				$iarea= $this->uri->segment(5);
				$dfrom= $this->uri->segment(6);
				$dto 	= $this->uri->segment(7);
				$data['isjbr'] 	= $isjbr;
				$data['iarea']= $iarea;
				$data['dfrom']= $dfrom;
				$data['dto']	= $dto;
				$query 	= $this->db->query("select * from tm_sjbr_item where i_sjbr = '$isjbr' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('sjbretur/mmaster');
				$data['isi']=$this->mmaster->baca($isjbr,$iarea);
				$data['detail']=$this->mmaster->bacadetail($isjbr,$iarea);
		 		$this->load->view('sjbretur/vmainform',$data);
			}else{
				$this->load->view('sjbretur/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			($this->session->userdata('logged_in'))
		){
			$isj	= $this->input->post('isj', TRUE);
			$dsj 	= $this->input->post('dsj', TRUE);
			if($dsj!=''){
				$tmp=explode("-",$dsj);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dsj=$th."-".$bl."-".$hr;
				$thbl	= substr($th,2,2).$bl;
				$tmpsj	= explode("-",$isj);
				$firstsj= $tmpsj[0];
				$lastsj	= $tmpsj[2];
				$newsj	= $firstsj."-".$thbl."-".$lastsj;				
			}
			$iarea		= $this->input->post('iarea', TRUE);
			$eareaname	= $this->input->post('eareaname', TRUE);
			$vspbnetto= $this->input->post('vsj', TRUE);
			$vspbnetto= str_replace(',','',$vspbnetto);
			$jml	  = $this->input->post('jml', TRUE);
			$gaono=true;
			for($i=1;$i<=$jml;$i++){
				$cek=$this->input->post('chk'.$i, TRUE);
				if($cek=='on'){
					$gaono=false;
				}
				if(!$gaono) break;
			}
			if(!$gaono){
				$this->db->trans_begin();
				$this->load->model('sjbretur/mmaster');
				$istore	  			= $this->input->post('istore', TRUE);
				if($istore=='AA'){
					$istorelocation		= '01';
				}elseif($istore=='PB'){
          $istorelocation		= '00';
				}else{
          $istorelocation		= 'PB';		
				}
				$istorelocationbin	= '00';
#				$Qseachsjdaer	= $this->mmaster->searchsjheader($isj,$iarea);
#				$nserachsjdaer	= $Qseachsjdaer->num_rows();
				$this->mmaster->updatesjheader($isj,$iarea,$dsj,$vspbnetto);
				for($i=1;$i<=$jml;$i++){
					$cek=$this->input->post('chk'.$i, TRUE);
					$iproduct		= $this->input->post('iproduct'.$i, TRUE);
					$iproductgrade	= 'A';
					$iproductmotif	= $this->input->post('motif'.$i, TRUE);
					$nretur		= $this->input->post('nretur'.$i, TRUE);
					$nretur		= str_replace(',','',$nretur);
          $nreceive	= $this->input->post('nreceive'.$i, TRUE);
					$nreceive	= str_replace(',','',$nreceive);
          $nasal  	= $this->input->post('nasal'.$i, TRUE);
					$nasal  	= str_replace(',','',$nasal);

					$this->mmaster->deletesjdetail( $isj, $iarea, $iproduct, $iproductgrade, $iproductmotif);

					$th=substr($dsj,0,4);
					$bl=substr($dsj,5,2);
					$emutasiperiode=$th.$bl;
          $tra=$this->mmaster->deletetrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$isj);
				  $this->mmaster->updatemutasi01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nasal,$emutasiperiode);
				  $this->mmaster->updateic01($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nasal);
					if($cek=='on'){
					  $eproductname	= $this->input->post('eproductname'.$i, TRUE);
					  $vunitprice		= $this->input->post('vproductmill'.$i, TRUE);
					  $vunitprice		= str_replace(',','',$vunitprice);
					  $nreceive		 	= $this->input->post('nreceive'.$i, TRUE);
					  $nreceive		  = str_replace(',','',$nreceive);
					  $nretur		  	= $this->input->post('nretur'.$i, TRUE);
					  $nretur			  = str_replace(',','',$nretur);
					  $eremark  		= $this->input->post('eremark'.$i, TRUE);
					  if($eremark=='')$eremark=null;
					  if($nretur>0){
						  $this->mmaster->insertsjdetail($iproduct,$iproductgrade,$iproductmotif,$eproductname,$nreceive,$nretur,
                                             $vunitprice,$isj,$dsj,$iarea,$istore,$istorelocation,
                                             $istorelocationbin,$eremark,$i);                      
					    $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
					    if(isset($trans)){
					      foreach($trans as $itrans)
					      {
						      $q_aw =$itrans->n_quantity_stock;
						      $q_ak =$itrans->n_quantity_stock;
						      $q_in =0;
						      $q_out=0;
						      break;
					      }
					    }else{
					      $q_aw=0;
					      $q_ak=0;
					      $q_in=0;
					      $q_out=0;
					    }
					    $this->mmaster->inserttrans1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$isj,$q_in,$q_out,$nretur,$q_aw,$q_ak,$tra);
					    $th=substr($dsj,0,4);
					    $bl=substr($dsj,5,2);
					    $emutasiperiode=$th.$bl;
					    $ada=$this->mmaster->cekmutasi2($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode);
					    if($ada=='ada')
					    {
						    $this->mmaster->updatemutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nretur,$emutasiperiode);
					    }else{
						    $this->mmaster->insertmutasi1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nretur,$emutasiperiode,$q_aw,$q_ak);
					    }
					    if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
					    {
						    $this->mmaster->updateic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nretur,$q_ak);
					    }else{
						    $this->mmaster->insertic1($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nretur);
					    }
					  }
					}
				}				
				if ( ($this->db->trans_status() === FALSE) )
				{
					$this->db->trans_rollback();
				}else{
#					$this->db->trans_rollback();
					$this->db->trans_commit();
						$sess=$this->session->userdata('session_id');
						$id=$this->session->userdata('user_id');
						$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
						$rs		= pg_query($sql);
						if(pg_num_rows($rs)>0){
							while($row=pg_fetch_assoc($rs)){
								$ip_address	  = $row['ip_address'];
								break;
							}
						}else{
							$ip_address='kosong';
						}
						$query 	= pg_query("SELECT current_timestamp as c");
						while($row=pg_fetch_assoc($query)){
							$now	  = $row['c'];
						}
						$pesan='Edit SJ Retur No:'.$isj;
						$this->load->model('logger');
						$this->logger->write($id, $ip_address, $now , $pesan );
					$data['sukses']			= true;
					$data['inomor']			= $newsj;
					$this->load->view('nomor',$data);
				}
			}
    }
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu311')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj	= $this->input->post('isjdelete', TRUE);
			$this->load->model('sjbretur/mmaster');
			$this->mmaster->delete($isj);
			$data['page_title'] = $this->lang->line('sjbr');
			$data['isj']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('sjbretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu311')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$isj			= $this->input->post('isjdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
  			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$this->db->trans_begin();
			$this->load->model('sjbretur/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $isj, $iproductmotif);
			if ($this->db->trans_status() === FALSE)
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();
				$data['page_title'] = $this->lang->line('sjbr')." Update";
				$query = $this->db->query("select * from tm_spmb_item where i_spmb = '$isj'");
				$data['jmlitem'] = $query->num_rows(); 				
				$data['isj'] = $isj;
				$data['isi']=$this->mmaster->baca($isj);
				$data['detail']=$this->mmaster->bacadetail($isj);
				$this->load->view('sjbretur/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu311')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$data['sjpbr']=$this->uri->segment(5);
			$sjpbr=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/sjbretur/cform/product/'.$baris.'/'.$sjpbr.'/';
			$query = $this->db->query(" select a.i_product as kode, a.i_product_motif as motif,
							                    a.e_product_motifname as namamotif, 
							                    c.e_product_name as nama,b.v_unit_price as harga
							                    from tr_product_motif a,tr_product c, tm_sjpbr_item b
							                    where a.i_product=c.i_product and b.i_product_motif=a.i_product_motif and c.i_product=b.i_product
							                    and b.i_sjpbr='$sjpbr' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('sjbretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($sjpbr,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$data['sjpbr']=$sjpbr;
			$this->load->view('sjbretur/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu311')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/sjbretur/cform/product/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										              c.e_product_name as nama,c.v_product_mill as harga
										              from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product
										              and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('sjbretur/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('sjbretur/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu311')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/sjbretur/cform/area/'.$baris.'/sikasep/';
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area
										   where i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$data['baris']=$baris;
         	$data['cari']='';
			$this->load->model('sjbretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(6));
			$this->load->view('sjbretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu311')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			/*$config['base_url'] = base_url().'index.php/sjbretur/cform/area/index/';
			$cari = $this->input->post('cari', FALSE)?$this->input->post('cari', FALSE):$this->input->get_post('cari', FALSE);
			$cari=strtoupper($cari);*/

			$baris   = $this->input->post('baris', FALSE);
        	if($baris=='')$baris=$this->uri->segment(4);
	        $cari   = ($this->input->post('cari', FALSE));
	        if($cari=='' && $this->uri->segment(5)!='sikasep')$cari=$this->uri->segment(5);
	        if($cari!='sikasep')
	        $config['base_url'] = base_url().'index.php/sjbretur/cform/cariarea/'.$baris.'/'.$cari.'/';
	          else
	        $config['base_url'] = base_url().'index.php/sjbretur/cform/cariarea/'.$baris.'/sikasep/';

			if($area1=='00'){
			 	$stquery = " select i_area, e_area_name, i_store from tr_area 
							  where (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
							  order by i_area ";
		  	}else{
			 	$stquery = " select	i_area, e_area_name, i_store from tr_area 
							  where (i_area='$area1' or i_area='$area2' or i_area='$area3' or i_area='$area4' or i_area='$area5')
							  and (upper(e_area_name) ilike '%$cari%' or upper(i_area) ilike '%$cari%')
							  order by i_area ";
		  	}

			$query = $this->db->query($stquery,false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('sjbretur/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['baris']=$baris;
         	$data['cari']=$cari;
			$data['isi']=$this->mmaster->cariarea($area1,$area2,$area3,$area4,$area5,$cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('sjbretur/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function sjpbr()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu311')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/sjbretur/cform/sjpbr/'.$area.'/';
			$query = $this->db->query(" select a.*, b.e_customer_name, c.e_area_name, c.i_store
                                  from tm_sjpbr a, tr_customer b, tr_area c
                                  where a.i_customer=b.i_customer and a.i_area=c.i_area
                                  and a.f_sjpbr_cancel = 'f' 
                                  and a.i_area='$area'
                                  and (a.i_sjbr is null or trim(a.i_sjbr)='')
                                  order by a.i_sjpbr",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sjbretur/mmaster');
			$data['page_title'] = $this->lang->line('listsjpbr');
			$data['area']=$area;
			$data['isi']=$this->mmaster->bacasjpbr($area,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sjbretur/vlistsjpbr', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisjpbr()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu311')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$area = $this->input->post('iarea', FALSE);
			$config['base_url'] = base_url().'index.php/sjbretur/cform/sjpbr/'.$area.'/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.*, b.e_customer_name, c.e_area_name, c.i_store
                                  from tm_sjpbr a, tr_customer b, tr_area c
                                  where a.i_customer=b.i_customer and a.i_area=c.i_area
                                  and a.f_sjpbr_cancel = 'f' 
                                  and a.i_area='$area'
                                  and (a.i_sjbr is null or trim(a.i_sjbr)='')
                                  and (upper(a.i_sjpbr) like '%$cari%')
                                  order by a.i_sjpbr",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('sjbretur/mmaster');
			$data['area']=$area;
			$data['page_title'] = $this->lang->line('listsjpbr');
			$data['isi']=$this->mmaster->carisjpbr($cari,$area,$config['per_page'],$this->uri->segment(5));
			$this->load->view('sjbretur/vlistsjpbr', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function hitung()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu311')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('sjbretur/mmaster');
			$dsjbr 	= $this->uri->segment(8);
			if($dsjbr!=''){
				$tmp=explode("-",$dsjbr);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
        $tglsjbaru=$th.$bl.$hr;
				$dsjbr=$th."-".$bl."-".$hr;
			}
			$isjpbr		    = $this->uri->segment(4);
			$dsjpbr		    = $this->uri->segment(5);
			$iarea		    = $this->uri->segment(6);
      $eareaname	  = str_replace('%20',' ',$this->uri->segment(7));
			$istore		    = $this->uri->segment(9);
			$ispg 		    = $this->uri->segment(12);
			$ecustomername= $this->uri->segment(10);
      $ecustomername= str_replace('%20',' ',$ecustomername);
      $ecustomername= str_replace("tandakoma",",",$ecustomername);
      $ecustomername= str_replace("tandapetiksatukebalik","`",$ecustomername);
      $ecustomername= str_replace("tandapetiksatu","'",$ecustomername);
      $ecustomername= str_replace("tandakurungbuka","(",$ecustomername);
      $ecustomername= str_replace("tandakurungtutup",")",$ecustomername);
      $ecustomername= str_replace("tandadan","&",$ecustomername);
			$icustomer		= $this->uri->segment(11);
			$query        = $this->db->query("  select a.i_product as kode
                                          from tr_product_motif a,tr_product c, tm_sjpbr_item b
                                          where a.i_product=c.i_product 
                                          and b.i_product_motif=a.i_product_motif
                                          and c.i_product=b.i_product
                                          and b.i_sjpbr='$isjpbr' and b.i_area='$iarea'",false);
			$data['jmlitem'] = $query->num_rows(); 
			$data['page_title'] = $this->lang->line('sjbr');
			$data['isjbr']='';
			$data['isi']="xxxxx";
			$data['dsjbr']=$dsjbr;
			$data['isjpbr']=$isjpbr;
			$data['dsjpbr']=$dsjpbr;
			$data['iarea']=$iarea;
			$data['istore']=$istore;
			$data['ispg']=$ispg;
			$data['eareaname']=$eareaname;
			$data['ecustomername']=$ecustomername;
			$data['icustomer']=$icustomer;
			$query=$this->db->query("	select v_sjpbr, i_customer, i_spg
                                from tm_sjpbr where i_sjpbr='$isjpbr' and i_area='$iarea' ",false);
			if ($query->num_rows() > 0){
				foreach($query->result() as $row){
					$vsjbr        = $row->v_sjpbr;
					$icustomer	  = $row->i_customer;
					$ispg     	  = $row->i_spg;
				}
			}
			$data['vsjbr']	=$vsjbr;
			$data['icustomer']=$icustomer;
			$data['ispg']=$ispg;
			$data['detail']=$this->mmaster->product($isjpbr,$iarea);
			$this->load->view('sjbretur/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
