<?php 
include ("php/fungsi.php");
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('rrkh');
			$data['drrkh']='';
			$this->load->model('rrkh/mmaster');
			$data['isi']=$this->mmaster->bacasemua();
			$data['detail']="";
			$data['jmlitem']="";
			$this->load->view('rrkh/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('rrkh');
			$this->load->view('rrkh/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('rrkh')." update";
			if($this->uri->segment(4)!=''){
				$drrkh 		= $this->uri->segment(4);
				$isalesman= $this->uri->segment(5);
				$iarea 		= $this->uri->segment(6);
				$dfrom		= $this->uri->segment(7);
				$dto 		= $this->uri->segment(8);
				$data['drrkh'] 	= $drrkh;
				$data['isalesman']= $isalesman;
				$data['iarea'] 	= $iarea;
				$data['dfrom'] 	= $dfrom;
				$data['dto']	= $dto;
				$tmp=explode("-",$drrkh);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$data['hari']	= dinten($hr,$bl,$th);
				$query = $this->db->query("select * from tm_rrkh_item where d_rrkh='$drrkh' and i_salesman='$isalesman' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('rrkh/mmaster');
				$data['isi']	 = $this->mmaster->baca($drrkh,$isalesman,$iarea);
				$data['detail']	 = $this->mmaster->bacadetail($drrkh,$isalesman,$iarea);
		 		$this->load->view('rrkh/vmainform',$data);
			}else{
				$this->load->view('rrkh/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		$isalesman 	= $this->input->post('isalesman', TRUE);
		$esalesmanname	= $this->input->post('esalesmanname', TRUE);
		$dtmp 		= $this->input->post('drrkh', TRUE);
		$dreceive1	= $this->input->post('dreceive1', TRUE);
		if($dtmp!=''){
			$tmp=explode("-",$dtmp);
			$th=$tmp[2];
			$bl=$tmp[1];
			$hr=$tmp[0];
			$drrkh=$th."-".$bl."-".$hr;
		}
		if($dreceive1!=''){
			$tmp=explode("-",$dreceive1);
			$th=$tmp[2];
			$bl=$tmp[1];
			$hr=$tmp[0];
			$drec1=$th."-".$bl."-".$hr;
		}else{
			$drec1='';
		}
		$iarea		= $this->input->post('iarea', TRUE);
		$eareaname	= $this->input->post('eareaname', TRUE);
		$jml		= $this->input->post('jml', TRUE);
		if($drrkh!='' && $eareaname!='' && $esalesmanname!='')
		{
			$this->db->trans_begin();
			$this->load->model('rrkh/mmaster');
			$this->mmaster->insertheader($isalesman, $drrkh, $iarea,$drec1);
			for($i=1;$i<=$jml;$i++){
			  $icustomer		= $this->input->post('icustomer'.$i, TRUE);
			  $ikunjungantype	= $this->input->post('ikunjungantype'.$i, TRUE);
			  $icity		= $this->input->post('icity'.$i, TRUE);
			  $fkunjunganrealisasi	= $this->input->post('fkunjunganrealisasi'.$i, TRUE);
				if($fkunjunganrealisasi=='on') 
					$fkunjunganrealisasi='t'; 
				else
					$fkunjunganrealisasi='f';
			  $fkunjunganvalid	= $this->input->post('fkunjunganvalid'.$i, TRUE);
				if($fkunjunganvalid=='on')
					$fkunjunganvalid='t';
				else
					$fkunjunganvalid='f';
			  $eremark		= $this->input->post('eremark'.$i, TRUE);
		    $this->mmaster->insertdetail($isalesman,$drrkh,$iarea,$icustomer,$ikunjungantype,$icity,$fkunjunganrealisasi,$fkunjunganvalid,$eremark,$i);
			}
			if ( ($this->db->trans_status() === FALSE) )
			{
		    $this->db->trans_rollback();
			}else{
#		    $this->db->trans_rollback();
		    $this->db->trans_commit();

		    $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		= pg_query($sql);
		    if(pg_num_rows($rs)>0){
			    while($row=pg_fetch_assoc($rs)){
				    $ip_address	  = $row['ip_address'];
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }
		    $query 	= pg_query("SELECT current_timestamp as c");
		    while($row=pg_fetch_assoc($query)){
			    $now	  = $row['c'];
		    }
		    $pesan='Input RRKH Salesman:'.$isalesman.' Area:'.$iarea;
		    $this->load->model('logger');
		    $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']	= true;
				$data['inomor']	= "Tanggal ".$dtmp." / Area ".$eareaname." / Salesman ".$esalesmanname;
				$this->load->view('nomor',$data);
			}
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		$isalesman 	= $this->input->post('isalesman', TRUE);
		$esalesmanname	= $this->input->post('esalesmanname', TRUE);
		$dtmp 		= $this->input->post('drrkh', TRUE);
		$dreceive1	= $this->input->post('dreceive1', TRUE);
		if($dtmp!=''){
			$tmp=explode("-",$dtmp);
			$th=$tmp[2];
			$bl=$tmp[1];
			$hr=$tmp[0];
			$drrkh=$th."-".$bl."-".$hr;
		}
		if($dreceive1!=''){
			$tmp=explode("-",$dreceive1);
			$th=$tmp[2];
			$bl=$tmp[1];
			$hr=$tmp[0];
			$drec1=$th."-".$bl."-".$hr;
		}else{
			$drec1='';
		}
		$iarea		= $this->input->post('iarea', TRUE);
		$eareaname	= $this->input->post('eareaname', TRUE);
		$jml		= $this->input->post('jml', TRUE);
		if($drrkh!='' && $eareaname!='' && $esalesmanname!='')
		{
			$this->db->trans_begin();
			$this->load->model('rrkh/mmaster');
			for($i=1;$i<=$jml;$i++){
			  $icustomer		= $this->input->post('icustomer'.$i, TRUE);
			  $ikunjungantype	= $this->input->post('ikunjungantype'.$i, TRUE);
			  $icity		= $this->input->post('icity'.$i, TRUE);
			  $fkunjunganrealisasi	= $this->input->post('fkunjunganrealisasi'.$i, TRUE);
				if($fkunjunganrealisasi=='on') 
					$fkunjunganrealisasi='t'; 
				else
					$fkunjunganrealisasi='f';
			  $fkunjunganvalid	= $this->input->post('fkunjunganvalid'.$i, TRUE);
				if($fkunjunganvalid=='on')
					$fkunjunganvalid='t';
				else
					$fkunjunganvalid='f';
			  $eremark	= $this->input->post('eremark'.$i, TRUE);
        #$this->mmaster->updateheader($drrkh,$isalesman,$iarea,$dreceive1);
				$this->mmaster->deleteheader($drrkh,$isalesman,$iarea);
        $this->mmaster->insertheader($isalesman, $drrkh, $iarea,$drec1);
				$this->mmaster->deletedetail($icustomer, $isalesman, $drrkh, $iarea);
    		$this->mmaster->insertdetail($isalesman,$drrkh,$iarea,$icustomer,$ikunjungantype,$icity,$fkunjunganrealisasi,$fkunjunganvalid,$eremark,$i);
			}
			if(($this->db->trans_status()===FALSE))
			{
			    $this->db->trans_rollback();
			}else{
			    $this->db->trans_commit();

		    $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		= pg_query($sql);
		    if(pg_num_rows($rs)>0){
			    while($row=pg_fetch_assoc($rs)){
				    $ip_address	  = $row['ip_address'];
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }
		    $query 	= pg_query("SELECT current_timestamp as c");
		    while($row=pg_fetch_assoc($query)){
			    $now	  = $row['c'];
		    }
		    $pesan='Update RRKH salesman:'.$isalesman.' Area:'.$iarea;
		    $this->load->model('logger');
		    $this->logger->write($id, $ip_address, $now , $pesan );

				$data['sukses']	= true;
				$data['inomor']	= "Tanggal ".$dtmp." / Area ".$eareaname." / Salesman ".$esalesmanname;
				$this->load->view('nomor',$data);
			}
		}
	}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$irrkh	= $this->input->post('irrkhdelete', TRUE);
			$this->load->model('rrkh/mmaster');
			$this->mmaster->delete($irrkh);

	    $sess=$this->session->userdata('session_id');
	    $id=$this->session->userdata('user_id');
	    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	    $rs		= pg_query($sql);
	    if(pg_num_rows($rs)>0){
		    while($row=pg_fetch_assoc($rs)){
			    $ip_address	  = $row['ip_address'];
			    break;
		    }
	    }else{
		    $ip_address='kosong';
	    }
	    $query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
		    $now	  = $row['c'];
	    }
	    $pesan='Hapus RRKH Salesman:'.$isalesman.' Area:'.$iarea;
	    $this->load->model('logger');
	    $this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('rrkh');
			$data['irrkh']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('rrkh/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$drrkh			= $this->uri->segment(4);
			$isalesman	= $this->uri->segment(5);
  		$iarea			= $this->uri->segment(6);
			$icustomer	= $this->uri->segment(7);
			$dfrom			= $this->uri->segment(8);
			$dto				= $this->uri->segment(9);
			$this->db->trans_begin();
			$this->load->model('rrkh/mmaster');
			$this->mmaster->deletedetail($icustomer, $isalesman, $drrkh, $iarea);
			if ($this->db->trans_status() === FALSE)
			{
		    $this->db->trans_rollback();
			}else{
		    $this->db->trans_commit();
				$data['page_title'] = $this->lang->line('rrkh')." Update";
				$data['drrkh'] 		= $drrkh;
				$data['isalesman']= $isalesman;
				$data['iarea'] 		= $iarea;
				$data['dfrom'] 		= $dfrom;
				$data['dto']	 		= $dto;
				$tmp=explode("-",$drrkh);
				$th=$tmp[0];
				$bl=$tmp[1];
				$hr=$tmp[2];
				$data['hari']			= dinten($hr,$bl,$th);
				$query = $this->db->query("select * from tm_rrkh_item where d_rrkh = '$drrkh' and i_salesman='$isalesman' and i_area='$iarea'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('rrkh/mmaster');
				$data['isi']=$this->mmaster->baca($drrkh,$isalesman,$iarea);
				$data['detail']=$this->mmaster->bacadetail($drrkh,$isalesman,$iarea);
		 		$this->load->view('rrkh/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']= $this->uri->segment(4);
			$data['area']	= $this->uri->segment(5);
			$data['cari']	= '';
			$baris= $this->uri->segment(4);
			$area	= $this->uri->segment(5);
			$cari	= '';
			$config['base_url'] = base_url().'index.php/rrkh/cform/customer/'.$baris.'/'.$area.'/';
			$query = $this->db->query(" select * from(
                                    select a.i_customer from tr_customer a, tr_city b where a.i_area='$area'
                                    and a.i_area=b.i_area and a.i_city=b.i_city
                                    union all
                                    select a.i_customer from tr_customer_tmp a 
                                    left join tr_city b on (a.i_area=b.i_area and a.i_city=b.i_city)
                                    where a.i_area='$area' and a.i_customer like '%000'
                                  ) as a
                                  order by a.i_customer",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('rrkh/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(6),$area);
			$this->load->view('rrkh/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){

			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$area 	= strtoupper($this->input->post('iarea', FALSE));
			$baris 	= strtoupper($this->input->post('baris', FALSE));
			if($cari=='') $cari=$this->uri->segment(6);
			if($area=='') $area=$this->uri->segment(5);
			if($baris=='') $baris=$this->uri->segment(4);
			if($cari=='index') $cari='';
			if($cari!='') $cari=strtoupper($cari);
			$data['baris']= $baris;
			$data['area']	= $area;
			$data['cari']	= $cari;
			$config['base_url'] = base_url().'index.php/rrkh/cform/caricustomer/'.$baris.'/'.$area.'/'.$cari.'/index/';
			$query 	= $this->db->query("	select * from(
                                    select a.i_customer from tr_customer a, tr_city b where a.i_area='$area'
                                    and a.i_area=b.i_area and a.i_city=b.i_city 
                                    and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%')
                                    union all
                                    select a.i_customer from tr_customer_tmp a 
                                    left join tr_city b on (a.i_area=b.i_area and a.i_city=b.i_city)
                                    where a.i_area='$area' and (a.i_customer like '%000' or upper(a.e_customer_name) like '%$cari%')
                                    ) as a ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$this->load->model('rrkh/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caricustomer($cari,$config['per_page'],$this->uri->segment(8),$area);
			$this->load->view('rrkh/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/rrkh/cform/area/index/';
      $allarea= $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      if($allarea=='t')
      {
        $query = $this->db->query(" select * from tr_area order by i_area", false);
      }
      else
      {
        $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
      }

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('rrkh/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('rrkh/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/rrkh/cform/area/index/';
      $allarea = $this->session->userdata('allarea');
      $iuser   = $this->session->userdata('user_id');
      $cari    = $this->input->post('cari', FALSE);
      $cari = strtoupper($cari);

      if($allarea=='t'){
        $query = $this->db->query(" select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
      else{
        $query = $this->db->query(" select * from tr_area where (i_area in ( select i_area from tm_user_area where i_user='$iuser') ) and (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%') order by i_area", false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('rrkh/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
      $data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$allarea,$iuser);
			$this->load->view('rrkh/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/rrkh/cform/index/';
			$query = $this->db->query("select * from tm_rrkh
						   where upper(i_rrkh) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('rrkh/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_rrkh');
			$data['irrkh']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('rrkh/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function salesman()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$area	= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/rrkh/cform/salesman/'.$area.'/';
//			$query = $this->db->query("	select * from tr_salesman where i_area='$area'",false);
			$query = $this->db->query("	select distinct(i_salesman),e_salesman_name from tr_customer_salesman where i_area='$area'",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('rrkh/mmaster');
			$data['page_title'] = $this->lang->line('list_salesman');
			$data['area']=$area;
			$data['isi']=$this->mmaster->bacasalesman($config['per_page'],$this->uri->segment(5),$area);
			$this->load->view('rrkh/vlistsalesman', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisalesman()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/rrkh/cform/salesman/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area = $this->input->post('iarea', FALSE);
//			$query = $this->db->query("	select * from tr_salesman
//							where upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%' ",false);
			$query = $this->db->query("	select distinct(i_salesman),e_salesman_name from tr_customer_salesman where i_area='$area' and 
																	(upper(e_salesman_name) like '%$cari%' or upper(i_salesman) like '%$cari%') ",false);

			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('rrkh/mmaster');
			$data['page_title'] = $this->lang->line('list_salesman');
			$data['area']=$area;
			$data['isi']=$this->mmaster->carisalesman($cari,$config['per_page'],$this->uri->segment(5),$area);
			$this->load->view('rrkh/vlistsalesman', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function city()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$baris= $this->uri->segment(4);
			$area	= $this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/rrkh/cform/city/'.$baris.'/'.$area.'/';
			$query = $this->db->query("	select * from tr_city where i_area='$area'",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('rrkh/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['area']=$area;
			$data['baris']=$baris;
			$data['isi']=$this->mmaster->bacacity($config['per_page'],$this->uri->segment(6),$area);
			$this->load->view('rrkh/vlistcity', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricity()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$baris= $this->input->post('baris', FALSE);
			$area = $this->input->post('iarea', FALSE);
			$config['base_url'] = base_url().'index.php/rrkh/cform/city/'.$baris.'/'.$area.'/';
			$query = $this->db->query("	select * from tr_city where (upper(e_city_name) like '%$cari%' or upper(i_city) like '%$cari%') and i_area='$area' ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('rrkh/mmaster');
			$data['page_title'] = $this->lang->line('list_city');
			$data['area']=$area;
			$data['baris']=$baris;
			$data['isi']=$this->mmaster->caricity($cari,$config['per_page'],$this->uri->segment(6),$area);
			$this->load->view('rrkh/vlistcity', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kunjungan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$baris= $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/rrkh/cform/kunjungan/'.$baris.'/';
			$query = $this->db->query("	select * from tr_kunjungan_type",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('rrkh/mmaster');
			$data['page_title'] = $this->lang->line('list_kunjungan');
			$data['baris']=$baris;
			$data['isi']=$this->mmaster->bacakunjungan($config['per_page'],$this->uri->segment(5));
			$this->load->view('rrkh/vlistkunjungan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carikunjungan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu168')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$baris= $this->input->post('baris', FALSE);
			$config['base_url'] = base_url().'index.php/rrkh/cform/kunjungan/'.$baris.'/';
			$query = $this->db->query("	select * from tr_kunjungan_type where upper(e_kunjungan_typename) like '%$cari%' or upper(i_kunjungan) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows();				
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('rrkh/mmaster');
			$data['page_title'] = $this->lang->line('list_kunjungan');
			$data['baris']=$baris;
			$data['isi']=$this->mmaster->carikunjungan($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('rrkh/vlistkunjungan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
