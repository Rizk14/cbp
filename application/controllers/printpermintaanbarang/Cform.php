<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{

			$no = $this->input->post('no');
			$iproductgroup	= $this->input->post('iproductgroup');
			$data['page_title'] = $this->lang->line('printpermintaanbarang');
			$this->load->model('printpermintaanbarang/mmaster');
			$data['no']			= '';
			$data['iproductgroup'] = $this->mmaster->bacaproductgroup();
			$this->load->view('printpermintaanbarang/vform2', $data);
		}else{
				$this->load->view('awal/index.php');
		 }
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$dfrom = $this->uri->segment(4);
			$dto = $this->uri->segment(5);
			$nopermintaan	= $this->uri->segment(6);
			$iproductgroup	= $this->uri->segment(7);
		
			$this->load->model('printpermintaanbarang/mmaster');
			$data['isi']		= $this->mmaster->baca($dfrom,$dto,$iproductgroup);
			$data['page_title'] = $this->lang->line('printpermintaanbarang');
			$data['no']			   = $nopermintaan;
			$data['iproductgroup'] = $iproductgroup;
	 		$this->load->view('printpermintaanbarang/vformrpt',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$config['base_url'] = base_url().'index.php/printpermintaanbarang/cform/area/index/';
      		$iuser   			= $this->session->userdata('user_id');

      		$query 				= $this->db->query("select * from tr_area where i_area in ( select i_area from tm_user_area 
      												where i_user='$iuser') 
      												order by i_area", false);

			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link']	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('printpermintaanbarang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']		= $this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);

			$this->load->view('printpermintaanbarang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printpermintaanbarang/cform/area/index/';
			$cari	= strtoupper($this->input->post('cari'));
			$iuser  = $this->session->userdata('user_id');
			
			$query 	= $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page']	 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printpermintaanbarang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			
			$this->load->view('printpermintaanbarang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	/*function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$ispmb = $this->uri->segment(4);
			$iarea = $this->input->post('iarea');
			$this->load->model('printpermintaanbarang/mmaster');
			$data['ispmb']=$ispmb;
			$data['page_title'] = $this->lang->line('printpermintaanbarang');
			$data['isi']=$this->mmaster->baca($ispmb);
			$data['detail']=$this->mmaster->bacadetail($ispmb);
			
			$sess 	= $this->session->userdata('session_id');
		  	$id 	= $this->session->userdata('user_id');
		  	$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  	$rs		= $this->db->query($sql);
		 	 if($rs->num_rows>0)
		 		{
				  foreach($rs->result() as $tes)
				  	{
					  	$ip_address	= $tes->ip_address;
					  	break;
			  		}
		 	 	}else{
			  			$ip_address = 'kosong';
		  			}

			$data['user']	= $this->session->userdata('user_id');
		  	$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query))
				{
					$now	= $row['c'];
				}
			$pesan='Cetak SPMB No:'.$ispmb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printpermintaanbarang/vmainform', $data);
		}else{
				$this->load->view('awal/index.php');
			}
	}*/
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispmb 					= $this->uri->segment(4);
			$iarea 					= $this->uri->segment(5);
			$dfrom					= $this->input->post('dfrom');
			$dto					= $this->input->post('dto');
			$iarea					= $this->input->post('iarea');
      		if($iarea=='')$iarea 	= $this->uri->segment(5);
      		if($dfrom=='')$dfrom 	= $this->uri->segment(6);
      		if($dto=='')$dto 		= $this->uri->segment(7);
			$this->load->model('printpermintaanbarang/mmaster');
      		$data['dfrom']			= $dfrom;
			$data['dto']  			= $dto;
			$data['iarea']			= $iarea;
			$data['ispmb']			= $ispmb;
			$data['page_title'] 	= $this->lang->line('printpermintaanbarang');
			$data['isi']			= $this->mmaster->baca($ispmb,$iarea);
     		$data['detail']			= $this->mmaster->bacadetail($ispmb,$iarea);

      		$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak SPMB No:'.$ispmb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printpermintaanbarang/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   )
		{
			$data['page_title'] = $this->lang->line('printpermintaanbarang');
			$this->load->view('printpermintaanbarang/vinsert_fail',$data);
		}else{
				$this->load->view('awal/index.php');
			}
	}
	
/*DICOMMENT TANGGAL 14-07-2017
function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ) {

			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			
			$config['base_url'] = base_url().'index.php/printpermintaanbarang/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			
			if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') {
				$fltr_area	= " ";
			} else {
				$fltr_area	= " and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5') ";
			}
			
			$query = $this->db->query(" select a.*, b.e_area_name from tm_spmb a, tr_area b
										where a.i_area=b.i_area ".$fltr_area."
										and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										or upper(a.i_spmb) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printpermintaanbarang');
			$this->load->model('printpermintaanbarang/mmaster');
			$data['ispmb']='';
			$data['cari']=$cari;
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('printpermintaanbarang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ispmb = $this->uri->segment(4);
			$this->load->model('printpermintaanbarang/mmaster');
			$data['ispmb']=$ispmb;
			$data['page_title'] = $this->lang->line('printpermintaanbarang');
			$data['isi']=$this->mmaster->baca($ispmb);
			$data['detail']=$this->mmaster->bacadetail($ispmb);
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak SPMB No:'.$ispmb;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printpermintaanbarang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printpermintaanbarang');
			$this->load->view('printpermintaanbarang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu69')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
						   
			$config['base_url'] = base_url().'index.php/printpermintaanbarang/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);

			if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') {
				$fltr_area	= " ";
			} else {
				$fltr_area	= " and (a.i_area='$area1' or a.i_area='$area2' or a.i_area='$area3' or a.i_area='$area4' or a.i_area='$area5') ";
			}
						
			$query = $this->db->query(" select a.*, b.e_area_name from tm_spmb a, tr_area b
										where a.i_area=b.i_area ".$fltr_area."
										and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										or upper(a.i_spmb) like '%$cari%')",false);
										
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('printpermintaanbarang/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(4),$area1,$area2,$area3,$area4,$area5);
			$data['page_title'] = $this->lang->line('printpermintaanbarang');
			$data['ispmb']='';
			$data['cari']=$cari;
			$data['detail']='';
	 		$this->load->view('printpermintaanbarang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}*/	
}
?>
