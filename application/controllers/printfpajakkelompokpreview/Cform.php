<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
    $this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu192')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/printfpajakkelompokpreview/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$data['page_title'] = $this->lang->line('printfpajakpreview');
			$data['cari']       = '';
      $data['dfrom']      = '';
			$data['dto']        = '';
			$data['iarea']      = '';
			$data['isi']        = '';
			$data['fakturfrom'] = '';
			$data['fakturto']   = '';
      $data['iseri']      = '';
			$this->load->view('printfpajakkelompokpreview/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu192')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printfpajakkelompokpreview/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('printfpajakkelompokpreview/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printfpajakkelompokpreview/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu192')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printfpajakkelompokpreview/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);
			$this->load->model('printfpajakkelompokpreview/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('printfpajakkelompokpreview/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		        ($this->session->userdata('menu192')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	  = strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	  = $this->input->post('dto');
			$iarea	= $this->input->post('iarea');
			$nama 	= $this->input->post('nama');
      $data['faktur'] = $this->input->post('ifkom');
      if($dfrom=='')$dfrom=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
      if($iarea=='')$iarea=$this->uri->segment(6);
      if($nama=='')$nama=$this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/printfpajakkelompokpreview/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$nama.'/';
			$iarea1	= $this->session->userdata('i_area');
			$iarea2	= $this->session->userdata('i_area2');
			$iarea3	= $this->session->userdata('i_area3');
			$iarea4	= $this->session->userdata('i_area4');
			$iarea5	= $this->session->userdata('i_area5');
      if($iarea1=='00'){
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
						                      where a.i_customer=b.i_customer 
						                      and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						                      or upper(a.i_faktur_komersial) like '%$cari%') and a.n_faktur_komersialprint>0
                                  and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                                  and a.i_area='$iarea' and not a.i_faktur_komersial isnull",false);
#and (a.n_print=0 or a.n_print isnull)
      }else{
			$query = $this->db->query(" select a.*, b.e_customer_name from tm_nota a, tr_customer b
						                      where a.i_customer=b.i_customer 
						                      and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
						                      or upper(a.i_faktur_komersial) like '%$cari%') and a.n_faktur_komersialprint>0
						                      and (a.i_area='$iarea1' or a.i_area='$iarea2' or a.i_area='$iarea3' or a.i_area='$iarea4' 
                                  or a.i_area='$iarea5')
                                  and a.d_nota >= to_date('$dfrom','dd-mm-yyyy') and a.d_nota <= to_date('$dto','dd-mm-yyyy')
                                  and a.i_area='$iarea' and not a.i_faktur_komersial isnull",false);
      }
#and (a.n_print=0 or a.n_print isnull)
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printfpajakpreview');
			$this->load->model('printfpajakkelompokpreview/mmaster');
      $data['dfrom']= $dfrom;
			$data['dto']  = $dto;
			$data['iarea']= $iarea;
			$data['nama']= $nama;
			$data['isi']=$this->mmaster->bacasemua($iarea,$iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$cari,$config['per_page'],$this->uri->segment(8),$dfrom,$dto);
			$this->load->view('printfpajakkelompokpreview/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu192')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$nama   = $this->input->post('nama');
			$jabatan= $this->input->post('jabatan');
			$iarea	= $this->input->post('iarea');
			$inota  = $this->input->post('inotafrom');
			$fakturfrom	= $this->input->post('fakturfrom');
			$fakturto	  = $this->input->post('fakturto');
			$sjfrom	= $this->input->post('isjfrom');
			$sjto	  = $this->input->post('isjto');
			$iseri	= $this->input->post('iseripajak');
      if( ($nama!='')&&($jabatan!='')&&($iarea!='')&&($inota!='')&&($fakturfrom!='')&&($fakturto!='')&&($iseri!='') )
      {
			  $this->load->model('printfpajakkelompokpreview/mmaster');
        $data['nama']   = $nama;
        $data['jabatan']= $jabatan;
			  $data['iarea']  = $iarea;
			  $data['inota']  = $inota;
			  $data['fakturfrom'] = $fakturfrom;
			  $data['fakturto'] = $fakturto;
			  $data['sjfrom'] = $sjfrom;
			  $data['sjto'] = $sjto;
			  $data['iseri']  = $iseri;
			  $data['page_title'] = $this->lang->line('printfpajakpreview');
			  $data['master']=$this->mmaster->baca($sjfrom,$sjto,$fakturfrom,$fakturto,$iarea);
			  $this->load->view('printfpajakkelompokpreview/vformrptprev', $data);
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu192')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printfpajakpreview');
			$this->load->view('printfpajakkelompokpreview/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu192')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printfpajakkelompokpreview/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$iarea1	= $this->session->userdata('i_area');
			$iarea2	= $this->session->userdata('i_area2');
			$iarea3	= $this->session->userdata('i_area3');
			$iarea4	= $this->session->userdata('i_area4');
			$iarea5	= $this->session->userdata('i_area5');
			$query = $this->db->query(" 	select a.*, b.e_customer_name from tm_spb a, tr_customer b
							where a.i_customer=b.i_customer
							and (upper(a.i_customer) like '%$cari%' or upper(b.e_customer_name) like '%$cari%'
							or upper(a.i_spb) like '%$cari%')
							and (a.i_area='$iarea1' or a.i_area='$iarea2' or a.i_area='$iarea3' or a.i_area='$iarea4' or a.i_area='$iarea5')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('printfpajakkelompokpreview/mmaster');
			$data['isi']=$this->mmaster->cari($iarea1,$iarea2,$iarea3,$iarea4,$iarea5,$cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('printfpajakpreview');
			$data['cari']=$cari;
			$data['inota']='';
			$data['detail']='';
	 		$this->load->view('printfpajakkelompokpreview/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function fakturfrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu192')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari=strtoupper($this->input->post("cari"));
      $dfrom=strtoupper($this->input->post("dfrom"));
      $dto=strtoupper($this->input->post("dto"));
      $area=strtoupper($this->input->post("iarea"));
      $to=strtoupper($this->input->post("fakturto"));
      if($dfrom=='') $dfrom=$this->uri->segment(4);
      if($area=='') $area=$this->uri->segment(5);
#      if($to=='') $to=$this->uri->segment(7);
#			$config['base_url'] = base_url().'index.php/printfpajakkelompokpreview/cform/fakturfrom/'.$dfrom.'/'.$dto.'/'.$area.'/';#.$to.'/';
			$config['base_url'] = base_url().'index.php/printfpajakkelompokpreview/cform/fakturfrom/'.$dfrom.'/'.$area.'/';
      if($area=='NA'){
        if($to==''){
			    $query = $this->db->query(" select a.i_faktur_komersial from tm_nota a
				                              where (upper(a.i_customer) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%')
                                      and a.d_nota = to_date('$dfrom','dd-mm-yyyy')
                                      and not a.i_faktur_komersial isnull",false);
        }else{
			    $query = $this->db->query(" select a.i_faktur_komersial from tm_nota a
				                              where (upper(a.i_customer) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%')
                                      and a.d_nota = to_date('$dfrom','dd-mm-yyyy')
                                      and not a.i_faktur_komersial isnull and a.i_faktur_komersial<='$to'",false);
        }
      }else{
        if($to==''){
			    $query = $this->db->query(" select a.i_faktur_komersial from tm_nota a
				                              where (upper(a.i_customer) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%')
                                      and a.d_nota = to_date('$dfrom','dd-mm-yyyy')
                                      and a.i_area='$area' and not a.i_faktur_komersial isnull",false);
        }else{
			    $query = $this->db->query(" select a.i_faktur_komersial from tm_nota a
				                              where (upper(a.i_customer) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%')
                                      and a.d_nota = to_date('$dfrom','dd-mm-yyyy')
                                      and a.i_area='$area' and not a.i_faktur_komersial isnull and a.i_faktur_komersial<='$to'",false);
        }
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->paginationxx->initialize($config);

			$this->load->model('printfpajakkelompokpreview/mmaster');
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['area']=$area;
			$data['page_title'] = $this->lang->line('list_fkom');
			$data['isi']=$this->mmaster->bacafakturfrom($config['per_page'],$this->uri->segment(6),$dfrom,$dto,$area,$cari,$to);
			$this->load->view('printfpajakkelompokpreview/vlistfakturfrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function fakturto()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu192')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
      $cari=strtoupper($this->input->post("cari"));
      $dfrom=strtoupper($this->input->post("dfrom"));
      $dto=strtoupper($this->input->post("dto"));
      $area=strtoupper($this->input->post("iarea"));
      $from=strtoupper($this->input->post("fakturfrom"));
      if($dfrom=='') $dfrom=$this->uri->segment(4);
      if($area=='') $area=$this->uri->segment(5);
      if($from=='') $from=$this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/printfpajakkelompokpreview/cform/fakturto/'.$dfrom.'/'.$area.'/'.$from.'/';
      if($area=='NA'){
			  $query = $this->db->query(" select a.i_faktur_komersial from tm_nota a
				                            where (upper(a.i_customer) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%')
                                    and a.d_nota = to_date('$dfrom','dd-mm-yyyy')
                                    and not a.i_faktur_komersial isnull and a.i_faktur_komersial>='$from'",false);
      }else{
			  $query = $this->db->query(" select a.i_faktur_komersial from tm_nota a
				                            where (upper(a.i_customer) like '%$cari%' or upper(a.i_faktur_komersial) like '%$cari%')
                                    and a.d_nota = to_date('$dfrom','dd-mm-yyyy')
                                    and a.i_area='$area' and not a.i_faktur_komersial isnull and a.i_faktur_komersial>='$from'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);

			$this->load->model('printfpajakkelompokpreview/mmaster');
      $data['dfrom']=$dfrom;
      $data['dto']=$dto;
      $data['area']=$area;
			$data['page_title'] = $this->lang->line('list_fkom');
			$data['isi']=$this->mmaster->bacafakturto($config['per_page'],$this->uri->segment(7),$dfrom,$dto,$area,$cari,$from);
			$this->load->view('printfpajakkelompokpreview/vlistfakturto', $data);

		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
