<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('list_pelunasan');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listpelunasan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	= $this->session->userdata('i_area');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listpelunasan/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_pelunasan a 
										              inner join tr_area on(a.i_area=tr_area.i_area)
										              inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										              where (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                                  or upper(a.i_pelunasan) like '%$cari%')
										              and a.i_area='$iarea' and
										              a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
										              a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_pelunasan');
			$this->load->model('listpelunasan/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['area1']  = $area1;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Pelunasan Area '.$iarea.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listpelunasan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listpelunasan');
			$this->load->view('listpelunasan/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	= $this->session->userdata('i_area');
			$ipl		= $this->uri->segment(4);
			$iarea		= $this->uri->segment(5);
			$idt		= $this->uri->segment(6);
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(7);
			if($dto=='') $dto=$this->uri->segment(8);
			$this->load->model('listpelunasan/mmaster');
      $this->db->trans_begin();
			$this->mmaster->delete($ipl,$iarea,$idt);
      if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
			}else{
#				$this->db->trans_rollback();
				$this->db->trans_commit();
		    $sess=$this->session->userdata('session_id');
		    $id=$this->session->userdata('user_id');
		    $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		    $rs		= pg_query($sql);
		    if(pg_num_rows($rs)>0){
			    while($row=pg_fetch_assoc($rs)){
				    $ip_address	  = $row['ip_address'];
				    break;
			    }
		    }else{
			    $ip_address='kosong';
		    }
		    $query 	= pg_query("SELECT current_timestamp as c");
        while($row=pg_fetch_assoc($query)){
        	$now	  = $row['c'];
		    }
		    $pesan='Menghapus Pelunasan Area:'.$iarea.' No:'.$ipl;
		    $this->load->model('logger');
		    $this->logger->write($id, $ip_address, $now , $pesan );  

		    $cari	= strtoupper($this->input->post('cari'));
			  $config['base_url'] = base_url().'index.php/listpelunasan/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			  $query = $this->db->query(" select * from tm_pelunasan a 
										                inner join tr_area on(a.i_area=tr_area.i_area)
										                inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										                where (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%'
                                    or upper(a.i_pelunasan) like '%$cari%')
										                and a.i_area='$iarea' and
										                a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
										                a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
			  $config['total_rows'] = $query->num_rows(); 
			  $config['per_page'] = '10';
			  $config['first_link'] = 'Awal';
			  $config['last_link'] = 'Akhir';
			  $config['next_link'] = 'Selanjutnya';
			  $config['prev_link'] = 'Sebelumnya';
			  $config['cur_page'] = $this->uri->segment(8);
			  $this->pagination->initialize($config);
			  $data['page_title'] = $this->lang->line('list_pelunasan');
			  $this->load->model('listpelunasan/mmaster');
			  $data['cari']		= $cari;
			  $data['dfrom']		= $dfrom;
			  $data['dto']		= $dto;
			  $data['iarea']		= $iarea;
			  $data['area1']		= $area1;
			  $data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			  $this->load->view('listpelunasan/vmainform', $data);
      }
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $area1	= $this->session->userdata('i_area');
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listpelunasan/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_pelunasan a 
										              inner join tr_area on(a.i_area=tr_area.i_area)
										              inner join tr_customer on(a.i_customer=tr_customer.i_customer)
										              where (upper(a.i_dt) like '%$cari%' or upper(a.i_giro) like '%$cari%' or upper(a.i_customer) like '%$cari%')
										              and a.i_area='$iarea' and
										              a.d_bukti >= to_date('$dfrom','dd-mm-yyyy') AND
										              a.d_bukti <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('list_pelunasan');
			$this->load->model('listpelunasan/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']	= $iarea;
      $data['area1']  = $area1;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listpelunasan/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listpelunasan/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listpelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listpelunasan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu108')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listpelunasan/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listpelunasan/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listpelunasan/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
