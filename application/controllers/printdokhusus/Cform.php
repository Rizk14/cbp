<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu162')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printdokhusus');
			$data['dfrom']	= '';
			$data['dto']	= '';
			$data['isupplier']	= '';
			$this->load->view('printdokhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu157')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printdokhusus/cform/supplier/index/';
			$config['per_page'] = '10';
			$query = $this->db->query("select * from tr_supplier",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$this->load->model('opvsdo/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('printdokhusus/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu157')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
     	 $cari	  		= strtoupper($this->input->post('cari'));
		 $dfrom			= $this->input->post('dfrom');
		 $dto	 		= $this->input->post('dto');
		 $isupplier		= $this->input->post('isupplier');
     		$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/printdokhusus/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select * from v_list_do a
										              left join tm_spb b on (b.i_spb=a.i_spb and b.i_area=a.i_spb_area)
										              left join tm_spmb c on (c.i_spmb=a.i_spmb and c.i_area=a.i_spmb_area)
										              where 
										              a.d_do >= to_date('$dfrom','dd-mm-yyyy') and a.d_do <= to_date('$dto','dd-mm-yyyy') and
										              a.i_supplier='$isupplier' and
										              upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
										              or upper(i_do) like '%$cari%' or upper(i_op) like '%$cari%'
										              or upper(a.i_spmb) like '%$cari%' or upper(a.i_spb) like '%$cari%'
										              or upper(c.i_spmb_old) like '%$cari%' or upper(b.i_spb_old) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('printdokhusus');
			$this->load->model('printdokhusus/mmaster');
			$data['dfrom']= $dfrom;
			$data['dto']  = $dto;
			$data['isupplier']= $isupplier;

			$data['isi']=$this->mmaster->bacasemua($dfrom,$dto,$isupplier,$cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('printdokhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu162')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$ido  = $this->uri->segment(4);
			$area = $this->uri->segment(5);
			$this->load->model('printdokhusus/mmaster');
			$data['ido']=$ido;
			$data['page_title'] = $this->lang->line('printdokhusus');
			$data['isi']=$this->mmaster->baca($ido,$area);
			$data['detail']=$this->mmaster->bacadetail($ido,$area);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak DO Area '.$area.' No:'.$ido;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printdokhusus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu162')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printdokhusus');
			$this->load->view('printdokhusus/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu162')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/printdokhusus/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from v_list_do a
										              left join tm_spb b on (b.i_spb=a.i_spb and b.i_area=a.i_spb_area)
										              left join tm_spmb c on (c.i_spmb=a.i_spmb and c.i_area=a.i_spmb_area)
										              where 
										              upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'
										              or upper(i_do) like '%$cari%' or upper(i_op) like '%$cari%'
										              or upper(a.i_spmb) like '%$cari%' or upper(a.i_spb) like '%$cari%'
										              or upper(c.i_spmb_old) like '%$cari%' or upper(b.i_spb_old) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('printdokhusus/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('printdokhusus');
			$data['cari']=$cari;
			$data['ido']='';
			$data['detail']='';
	 		$this->load->view('printdokhusus/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
