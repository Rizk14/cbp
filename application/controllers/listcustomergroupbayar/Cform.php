<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');

	}
/*	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu464')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomergroupbayar/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select a.i_customer, a.i_customer_groupbayar as i_group
                                  from tr_customer_groupbayar a
                                  where upper(a.i_customer_groupbayar) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                                  order by a.i_customer",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listcustomergroupbayar');
			$data['icustomer']='';
			$this->load->model('listcustomergroupbayar/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$data['cari'] = $cari;
			$this->load->view('listcustomergroupbayar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}*/
	function cari()
	{
		$cari = strtoupper($this->input->post('cari', FALSE));
		if ($cari == '')
			$cari= $this->uri->segment(4);
		
		/*if($cari==''){
		  $config['base_url'] = base_url().'index.php/listcustomergroupbayar/cform/cari/xsigantengteax/';
		}else{
		  $config['base_url'] = base_url().'index.php/listcustomergroupbayar/cform/cari/'.$cari.'/';
		} */
		
		if ($cari == '')
			$cari = "xsigantengteax";
		
		if ($cari != "xsigantengteax")
			$query	= $this->db->query("select a.i_customer, a.i_customer_groupbayar as i_group
                                  from tr_customer_groupbayar a
                                  where upper(a.i_customer_groupbayar) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                                  order by a.i_customer",false);
        else
			$query	= $this->db->query("select a.i_customer, a.i_customer_groupbayar as i_group
                                  from tr_customer_groupbayar a
                                  order by a.i_customer",false);
                                  
	$config['base_url'] = base_url().'index.php/listcustomergroupbayar/cform/cari/'.$cari.'/';
    $config['total_rows'] 	= $query->num_rows();
    $config['per_page'] 	  = '10';
    $config['first_link'] 	= 'Awal';
    $config['last_link'] 	  = 'Akhir';
    $config['next_link'] 	  = 'Selanjutnya';
    $config['prev_link'] 	  = 'Sebelumnya';
    $config['cur_page'] 	  = $this->uri->segment(5);
    $this->pagination->initialize($config);
		$data['page_title'] = $this->lang->line('listcustomergroupbayar');
		$this->load->model('listcustomergroupbayar/mmaster');
		$hasil=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(5));
		
		$data['isi'] = $hasil;
		
		if ($cari == "xsigantengteax")
			$data['cari'] = '';
		else
			$data['cari'] = $cari;
		
		//$data['cari'] = $cari;
		$this->load->view('listcustomergroupbayar/vmainform', $data);
	}
	
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer 		= $this->input->post('icustomer', TRUE);
			$ecustomername 	= $this->input->post('ecustomername', TRUE);
			$igroup 	= $this->input->post('igroup', TRUE);

			if ((isset($icustomer) && $icustomer != '') && (isset($ecustomername) && $igroup != ''))
			{
				$this->load->model('listcustomergroupbayar/mmaster');
				$this->mmaster->insert($icustomer,$ecustomername,$igroup);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listcustomergroupbayar');
			$this->load->view('listcustomergroupbayar/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('listcustomergroupbayar')." update";
			if($this->uri->segment(4)){
				$icustomer = $this->uri->segment(4);
				$igroup = $this->uri->segment(5);
				$data['icustomer'] = $icustomer;
				$data['igroup'] = $igroup;
				$this->load->model('listcustomergroupbayar/mmaster');
				$data['isi']=$this->mmaster->baca($icustomer);
		 		$this->load->view('listcustomergroupbayar/vmainform',$data);
			}else{
				$this->load->view('listcustomergroupbayar/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomername 	= $this->input->post('ecustomername', TRUE);
			$igroup	= $this->input->post('igroup', TRUE);
			
			$this->load->model('listcustomergroupbayar/mmaster');
			$this->mmaster->update($icustomer,$ecustomername,$igroup);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$icustomer	= $this->uri->segment(4);
			$this->load->model('listcustomergroupbayar/mmaster');
			$this->mmaster->delete($icustomer);

			$config['base_url'] = base_url().'index.php/listcustomergroupbayar/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select a.i_customer, b.e_customer_name, a.i_customer_groupbayar as i_group
                                  from tr_customer_groupbayar a, tr_customer b
                                  where a.i_customer=b.i_customer and
                                  upper(b.e_customer_name) like '%$cari%' or upper(a.i_customer) like '%$cari%' 
                                  order by a.i_customer",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listcustomergroupbayar');
			$data['icustomer']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('listcustomergroupbayar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function pelanggan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){				
			$this->load->model('listcustomergroupbayar/mmaster');
			
			$config['base_url'] = base_url().'index.php/listcustomergroupbayar/cform/pelanggan/index/';	
			$query = $this->db->query("select * from tr_customer order by e_customer_name asc",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacapelanggan($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomergroupbayar/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caripelanggan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('listcustomergroupbayar/mmaster');
			
			$cari = $this->input->post('cari', FALSE)?$this->input->post('cari', FALSE):$this->input->get_post('cari', FALSE);
			$keywordcari	= $cari==''?'null':$cari;
			$config['base_url'] = base_url().'index.php/listcustomergroupbayar/cform/caripelanggan/index/'.$keywordcari.'/';
			
			$cari=strtoupper($cari);
			$stquery = " select * from tr_customer
						where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') order by e_customer_name asc ";

			$query = $this->db->query($stquery,false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->caripelanggan($cari,$config['per_page'],$this->uri->segment(6));
			$this->load->view('listcustomergroupbayar/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function group()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu437')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){				
			$this->load->model('listcustomergroupbayar/mmaster');
			
			$config['base_url'] = base_url().'index.php/listcustomergroupbayar/cform/group/index/';	
			$query = $this->db->query("select i_customer as i_group from tr_customer order by i_area, e_customer_name asc",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacagroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('listcustomergroupbayar/vlistgroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carigroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu464')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listcustomergroupbayar/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select i_customer as i_group from tr_customer
						                      where (upper(i_customer) like '%$cari%' or upper(e_customer_name) 
						                      like '%$cari%') order by i_area, e_customer_name asc",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('listcustomergroupbayar');
			$data['icustomer']='';
			$this->load->model('listcustomergroupbayar/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('listcustomergroupbayar/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}		
}
?>
