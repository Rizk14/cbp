<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('transplap');
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$this->load->view('transplap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iperiode	= $this->input->post('iperiode');
      $iarea = $this->input->post('iarea');
			$data['page_title'] = $this->lang->line('transplap');
			$data['iperiode']	= $iperiode;
			$data['iarea']	= $iarea;
################################################################
			$this->load->model('transplap/mmaster');
			$isi = $this->mmaster->bacaperiode($iperiode);
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Customer Card")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
      if(count($isi)>0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(24);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(14);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:K1'
				);
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'NO SUPPLIER');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'NAMA SUPPLIER');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'NO PELUNASAN');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('D1', 'AREA');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'BULAN');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'TAHUN');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'NO NOTA');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'TGL NOTA');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'TGL PELUNASAN');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'JUMLAH');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
			  $objPHPExcel->getActiveSheet()->setCellValue('K1', 'SISA');
			  $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					  array(
						  'borders' => array(
							  'top' 	=> array('style' => Style_Border::BORDER_THIN)
						  ),
					  )
				  );
        $totalnota = 0;
        $totalsisa = 0;
        $totalsupplier=0;
        $totalsisasupplier=0;
        $gtotalnota = 0;
        $gtotalsisa = 0;
        $i=1;
        $isupplier ='';
        $i_pelunasanap ='';
        foreach($isi as $row){
        $i++;
        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			  array(
				  'font' => array(
					  'name'	=> 'Arial',
					  'bold'  => false,
					  'italic'=> false,
					  'size'  => 10
				  )
			  ),
			  'A'.$i.':K'.$i
			  );
			  if($isupplier=='' || ($isupplier==$row->i_supplier)){
          if($i_pelunasanap=='' || ($i_pelunasanap==$row->i_pelunasanap)){
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_supplier, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_pelunasanap, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->bulan, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->tahun, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->i_dtap, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->d_bukti, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->d_dtap, Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->v_jumlah, Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->v_sisa, Cell_DataType::TYPE_NUMERIC);
            $i_pelunasanap = $row->i_pelunasanap;
            $totalnota  = $totalnota + $row->v_jumlah;
            $totalsisa = $totalsisa + $row->v_sisa;
            $isupplier= $row->i_supplier;
            $esupplier=$row->e_supplier_name;           
          }
           elseif($i_pelunasanap!='' && $i_pelunasanap!=$row->i_pelunasanap){
              $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				      array(
					      'font' => array(
						      'name'	=> 'Arial',
						      'bold'  => true,
						      'italic'=> false,
						      'size'  => 10
					      ),
					      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => true
					      )
				      ),
				      'A'.$i.':K'.$i
				      );
              
		          $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'TOTAL NOTA  '.$i_pelunasanap);
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,8,$i);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $totalnota, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $totalsisa, Cell_DataType::TYPE_NUMERIC);
              $i++;
              $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			        array(
				        'font' => array(
					        'name'	=> 'Arial',
					        'bold'  => false,
					        'italic'=> false,
					        'size'  => 10
				        )
			        ),
			        'A'.$i.':K'.$i
			        );
              $totalsupplier = $totalsupplier + $totalnota;
              $totalsisasupplier = $totalsisasupplier + $totalsisa;
              $totalnota=0;
              $totalsisa=0;       
              $totalnota  = $totalnota + $row->v_jumlah;
              $totalsisa = $totalsisa + $row->v_sisa;

			        
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_supplier, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_pelunasanap, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->bulan, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->tahun, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->i_dtap, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->d_bukti, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->d_dtap, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->v_jumlah, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->v_sisa, Cell_DataType::TYPE_NUMERIC);
              $i_pelunasanap = $row->i_pelunasanap;
      
            }
              $isupplier= $row->i_supplier;
              $esupplier=$row->e_supplier_name;    
          }elseif($isupplier!='' && ($isupplier!=$row->i_supplier)){
              $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				      array(
					      'font' => array(
						      'name'	=> 'Arial',
						      'bold'  => true,
						      'italic'=> false,
						      'size'  => 10
					      ),
					      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => true
					      )
				      ),
				      'A'.$i.':K'.$i
				      );
		          $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'TOTAL NOTA  '.$i_pelunasanap);
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,8,$i);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $totalnota, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $totalsisa, Cell_DataType::TYPE_NUMERIC);
              $i++;

              $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				      array(
					      'font' => array(
						      'name'	=> 'Arial',
						      'bold'  => true,
						      'italic'=> false,
						      'size'  => 10
					      ),
					      'alignment' => array(
						      'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						      'vertical'  => Style_Alignment::VERTICAL_CENTER,
						      'wrap'      => true
					      )
				      ),
				      'A'.$i.':K'.$i
				      );
              $totalsupplier = $totalsupplier + $totalnota;
              $totalsisasupplier = $totalsisasupplier + $totalsisa;
              $totalnota=0;
              $totalsisa=0;       
	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'TOTAL '.$esupplier);
              $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,8,$i);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $totalsupplier, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $totalsisasupplier, Cell_DataType::TYPE_NUMERIC);
              $gtotalnota = $gtotalnota + $totalsupplier;
              $gtotalsisa = $gtotalsisa + $totalsisasupplier;
              $totalsupplier=0;
              $totalsisasupplier=0;
              $totalnota=0;
              $totalsisa=0;
              $i++;
              
              $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			        array(
				        'font' => array(
					        'name'	=> 'Arial',
					        'bold'  => false,
					        'italic'=> false,
					        'size'  => 10
				        )
			        ),
			        'A'.$i.':K'.$i
			        );
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_supplier, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->e_supplier_name, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_pelunasanap, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->i_area, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->bulan, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->tahun, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->i_dtap, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $row->d_bukti, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $row->d_dtap, Cell_DataType::TYPE_STRING);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $row->v_jumlah, Cell_DataType::TYPE_NUMERIC);
              $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $row->v_sisa, Cell_DataType::TYPE_NUMERIC);
              $i_pelunasanap = $row->i_pelunasanap;
              $totalnota  = $totalnota + $row->v_jumlah;
              $totalsisa = $totalsisa + $row->v_sisa;
              $i_pelunasanap = $row->i_pelunasanap;
              $isupplier= $row->i_supplier;
          }
        }
        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A'.$i.':K'.$i
				);
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'TOTAL '.$esupplier);
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,8,$i);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $totalsupplier, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $totalsisasupplier, Cell_DataType::TYPE_NUMERIC);
        $gtotalnota = $gtotalnota + $totalsupplier;
        $gtotalsisa = $gtotalsisa + $totalsisasupplier;

        $i++;
		    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'GRAND TOTAL');
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,$i,8,$i);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $gtotalnota, Cell_DataType::TYPE_NUMERIC);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $gtotalsisa, Cell_DataType::TYPE_NUMERIC);
        $gtotalnota = $gtotalnota + $totalsupplier;
        $gtotalsisa = $gtotalsisa + $totalsisasupplier;
        
			  $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $nama='Data Pelunasan Hutang '.$iperiode.'.xls';
        if(file_exists('excel/'.$nama)){
          @chmod('excel/'.$nama, 0777);
          @unlink('excel/'.$nama);
        }
			  $objWriter->save('excel/00/'.$nama);
			  $sess=$this->session->userdata('session_id');
			  $id=$this->session->userdata('user_id');
			  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			  $rs		= pg_query($sql);
			  if(pg_num_rows($rs)>0){
				  while($row=pg_fetch_assoc($rs)){
					  $ip_address	  = $row['ip_address'];
					  break;
				  }
			  }else{
				  $ip_address='kosong';
			  }
			  $query 	= pg_query("SELECT current_timestamp as c");
			  while($row=pg_fetch_assoc($query)){
				  $now	  = $row['c'];
			  }
			  $pesan='Transfer Pelunasan Hutang '.' Periode:'.$iperiode;
			  $this->load->model('logger');
			  $this->logger->write($id, $ip_address, $now , $pesan );
			  $data['sukses']			= true;
			  $data['inomor']			= $pesan;
			  $this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
		
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/transplap/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('transplap/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('transplap/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu135')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/transplap/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('transplap/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('transplap/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
