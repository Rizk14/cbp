<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listagingpiutangnasional');
      $data['djt']='';
			$this->load->view('listagingpiutangnasional/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
	    $djt	= $this->input->post('djt');
			if($djt=='') $djt	= $this->uri->segment(4);
 			$config['base_url'] = base_url().'index.php/listagingpiutangnasional/cform/view/'.$djt.'/index/';
      $query = $this->db->query(" select a.v_sisa from tm_nota a, tr_customer b, tr_customer_groupar c
															    where a.i_customer=b.i_customer and a.i_customer=c.i_customer 
															    and a.f_ttb_tolak='f'
															    and not a.i_nota isnull
                                  and a.v_sisa>0
                                  and a.f_nota_cancel='f'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('listagingpiutangnasional/mmaster');
  		$data['page_title'] = $this->lang->line('listagingpiutangnasional');
			$data['cari']		= $cari;
			$data['djt']	= $djt;
			$data['isi']		= $this->mmaster->bacaperiode($djt,$config['per_page'],$this->uri->segment(6));

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Buka Aging piutang jatuh tempo:'.$djt;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('listagingpiutangnasional/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listagingpiutangnasional');
			$this->load->view('listagingpiutangnasional/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listagingpiutangnasional/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
											or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listagingpiutangnasional/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listagingpiutangnasional/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
 			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/listagingpiutangnasional/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			  $query 	= $this->db->query("select * from tr_area
								                   	where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              										  and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
								                   	or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listagingpiutangnasional/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('listagingpiutangnasional/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
     	 ($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dto	    = $this->input->post('dto');
			$iarea= $this->input->post('iarea');
      if($iarea=='')$icustomer=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$this->load->model('listagingpiutangnasional/mmaster');
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$data['dto']  = $dto;
			$data['iarea']= $iarea;
			$data['page_title'] = $this->lang->line('printopnnotaarea');
			$data['isi']=$this->mmaster->baca($iarea,$dto);
			$data['user']	= $this->session->userdata('user_id');
      $sess = $this->session->userdata('session_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Buka Aging piutang jatuh tempo:'.$dto.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('listagingpiutangnasional/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$icustomergroupar= $this->input->post('icustomergroupar');
			$ecustomername= $this->input->post('ecustomername');
      $djt	= $this->input->post('djt');
			if($icustomergroupar=='') $icustomergroupar	= $this->uri->segment(4);
			if($djt=='') $djt	= $this->uri->segment(5);
			if($ecustomername=='') $ecustomername	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listagingpiutangnasional/cform/detail/'.$icustomergroupar.'/'.$djt.'/'.$ecustomername.'/';
      $ecustomername=str_replace('%20',' ',$ecustomername);
      $ecustomername=str_replace('tandakurungbuka','(',$ecustomername);
      $ecustomername=str_replace('tandakurungtutup',')',$ecustomername);
      $ecustomername=str_replace('tandadan','&',$ecustomername);
      $ecustomername=str_replace('tandatitik','.',$ecustomername);
      $ecustomername=str_replace('tandakoma',',',$ecustomername);
      $ecustomername=str_replace('tandagaring','/',$ecustomername);

      $query = $this->db->query(" select a.v_sisa from tm_nota a, tr_customer b, tr_customer_groupar c
																	  where a.i_customer=b.i_customer and a.i_customer=c.i_customer
																	  and a.f_ttb_tolak='f'
																	  and a.f_nota_koreksi='f'
																	  and not a.i_nota isnull
																	  and c.i_customer_groupar='$icustomergroupar' 
                                    and a.v_sisa>0
                                    and a.f_nota_cancel='f'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);
			$this->load->model('listagingpiutangnasional/mmaster');
  		$data['page_title'] = $this->lang->line('listagingpiutangnasional');
			$data['icustomergroupar']	= $icustomergroupar;
			$data['ecustomername']	= $ecustomername;
			$data['djt']	= $djt;
			$data['isi']		= $this->mmaster->bacadetail($icustomergroupar,$djt,$config['per_page'],$this->uri->segment(7));
			$this->load->view('listagingpiutangnasional/vformdetail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
