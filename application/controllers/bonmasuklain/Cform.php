<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu453')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$inota	= $this->input->post('inota', TRUE);
			$dbm 		= $this->input->post('dbm', TRUE);
			if($dbm!=''){
				$tmp=explode("-",$dbm);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbm=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
			}
			$eremark= $this->input->post('eremark', TRUE);
			$jml		= $this->input->post('jml', TRUE);
 			$user		= $this->session->userdata('user_id');
			if($dbm!='' && $inota!='')# && $eremark!='')
			{
				$this->db->trans_begin();
				$this->load->model('bonmasuklain/mmaster');
        $istore				    = 'AA';
				$istorelocation		= '01';
				$istorelocationbin= '00';
				$ibm	=$this->mmaster->runningnumber($thbl);
				$this->mmaster->insertheader($ibm, $inota, $dbm, $eremark);
				$this->mmaster->updatenotabatal($inota, $user);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= 'A';
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $nquantity   		= $this->input->post('nquantity'.$i, TRUE);
				  if($nquantity>0){
				    $this->mmaster->insertdetail($ibm,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$eremark,$i);
############

            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttransbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibm,$q_in,$q_out,$nquantity,$q_aw,$q_ak);
            $th=substr($dbm,0,4);
            $bl=substr($dbm,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasibmelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasibmelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateicbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$q_ak);
            }else{
              $this->mmaster->inserticbm($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nquantity);
            }

############
				  }
				}
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
			    $this->db->trans_commit();
		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Input Bon Masuk No:'.$ibm;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ibm;
					$this->load->view('nomor',$data);
#			    $this->db->trans_rollback();
				}
			}else{
				$data['page_title'] = $this->lang->line('bonmasuk').' Lain-lain';
				$data['ibm']='';
				$data['inota']='';
        $data['cari']='';
        $data['eremark']='';
				$data['isi']="";
				$data['detail']="";
				$data['jmlitem']="";
				$data['tgl']=date('d-m-Y');
				$this->load->view('bonmasuklain/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu453')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bonmasuk').' Lain-lain';
			$this->load->view('bonmasuklain/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function nota()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu453')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari=strtoupper($this->input->post("cari"));
			if($cari=='')$cari=$this->uri->segment(4);
			if($cari=='' or $cari=='sikasep'){
			  $config['base_url'] = base_url().'index.php/bonmasuklain/cform/nota/sikasep/';
			  $config['total_rows'] = 0;
			}else{
			  $config['base_url'] = base_url().'index.php/bonmasuklain/cform/nota/'.$cari.'/';
/*
			  $query = $this->db->query(" select i_nota from tm_nota where f_nota_cancel='t' and i_nota like '%$cari%'
			                              and not i_nota isnull",false);
*/
			  $query = $this->db->query(" select i_nota from tm_notabatal where i_nota like '%$cari%'",false);
			  $config['total_rows'] = $query->num_rows(); 
			}
		  $config['per_page'] = '10';
		  $config['first_link'] = 'Awal';
		  $config['last_link'] = 'Akhir';
		  $config['next_link'] = 'Selanjutnya';
		  $config['prev_link'] = 'Sebelumnya';
		  $config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bonmasuklain/mmaster');
			$data['page_title'] = $this->lang->line('list_nota');
			$data['isi']=$this->mmaster->bacanota($config['per_page'],$this->uri->segment(5),$cari);
			$data['cari']=$cari;
			$this->load->view('bonmasuklain/vlistnota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function copy()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu453')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$this->load->model('bonmasuklain/mmaster');
			$cari         = $this->uri->segment(4);
			$inota	      = $this->uri->segment(5);
			$tgl  	      = $this->uri->segment(6);
			$eremark      = $this->uri->segment(7);
			$data['detail']=$this->mmaster->bacadetail($inota);
  		$data['cari']  =$cari;
			$data['tgl']  =$tgl;
			$data['eremark']=$eremark;
			$data['ibm']='';
			$data['inota']=$inota;
			$data['page_title'] = $this->lang->line('bonmasuk').' Lain-lain';
			$data['isi']=true;
			$this->load->view('bonmasuklain/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
