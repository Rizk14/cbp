<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if ($this->session->userdata('logged_in')){
			$iproduct 	= $this->input->post('iproduct', TRUE);
			$eproductname 	= $this->input->post('eproductname', TRUE);
			$iproductbase 	= $this->input->post('iproductbase', TRUE);
			$iproductmotif 	= $this->input->post('iproductmotif', TRUE);

			if ((isset($iproduct) && $iproduct != '') && (isset($eproductname) && $eproductname != '') && (isset($iproductbase) && $iproductbase != '') && (isset($iproductmotif) && $iproductmotif != ''))
			{
				$this->load->model('product/mmaster');
				$this->mmaster->insert($iproduct,$eproductname,$iproductbase,$iproductmotif);
			}
			else
			{
				$config['base_url'] = base_url().'index.php/product/cform/index/';
				$config['total_rows'] = $this->db->count_all('tr_product');
				$config['per_page'] = '10';
				$config['first_link'] = 'Awal';
				$config['last_link'] = 'Akhir';
				$config['next_link'] = 'Selanjutnya';
				$config['prev_link'] = 'Sebelumnya';
				$config['cur_page'] = $this->uri->segment(4);
				$this->pagination->initialize($config);

				$data['page_title'] = $this->lang->line('master_product');
				$data['iproduct']='';
				$this->load->model('product/mmaster');
				$cari = $this->input->post('cari', FALSE);
				$cari = strtoupper($cari);
				$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
				$this->load->view('product/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('master_product');
			$this->load->view('product/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if ($this->session->userdata('logged_in')){
			$data['page_title'] = $this->lang->line('master_product')." update";
			if($this->input->post('iproductedit')){
				$iproduct = $this->input->post('iproductedit');
				$data['iproduct'] = $iproduct;
				$this->load->model('product/mmaster');
				$data['isi']=$this->mmaster->baca($iproduct);
		 		$this->load->view('product/vmainform',$data);
			}else{
				$this->load->view('product/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if ($this->session->userdata('logged_in')){
			$iproduct = $this->input->post('iproduct', TRUE);
			$eproductname 	= $this->input->post('eproductname', TRUE);
			$iproductbase = $this->input->post('iproductbase', TRUE);
			$eproductbasename = $this->input->post('eproductbasename', TRUE);
			$iproductmotif 	= $this->input->post('iproductmotif', TRUE);
			$this->load->model('product/mmaster');
			$this->mmaster->update($iproduct,$eproductname,$iproductbase,$iproductmotif);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if ($this->session->userdata('logged_in')){
			$iproduct = $this->input->post('iproductdelete', TRUE);
			$this->load->model('product/mmaster');
			$this->mmaster->delete($iproduct);
			
			$config['base_url'] = base_url().'index.php/product/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_product');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);

			$data['page_title'] = $this->lang->line('master_product');
			$data['iproduct']='';
			$this->load->model('product/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('product/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productbase()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/product/cform/productbase/index/';
			$config['total_rows'] = $this->db->count_all('tr_product_base');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('product/mmaster');
			$data['page_title'] = $this->lang->line('list_productbase');
			$data['isi']=$this->mmaster->bacaproductbase($config['per_page'],$this->uri->segment(5));
			$this->load->view('product/vlistproductbase', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productmotif()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/product/cform/productmotif/index/';
			$config['total_rows'] = $this->db->count_all('tr_product_motif');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('product/mmaster');
			$data['page_title'] = $this->lang->line('list_productmotif');
			$data['isi']=$this->mmaster->bacaproductmotif($config['per_page'],$this->uri->segment(5));
			$this->load->view('product/vlistproductmotif', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if ($this->session->userdata('logged_in')){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/product/cform/index/';
			$query = $this->db->query("	select i_product from tr_product 
										where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('product/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_product');
			$data['iproduct']='';
	 		$this->load->view('product/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductbase()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/product/cform/productbase/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select i_product_base from tr_product_base 
										where upper(i_product_base) like '%$cari%' or upper(e_product_basename) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('product/mmaster');
			$data['page_title'] = $this->lang->line('list_productbase');
			$data['isi']=$this->mmaster->cariproductbase($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('product/vlistproductbase', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductmotif()
	{
		if ($this->session->userdata('logged_in')){
			$config['base_url'] = base_url().'index.php/product/cform/productmotif/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select i_product_motif from tr_product_motif 
										where upper(i_product_motif) like '%$cari%' or upper(e_product_motifname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('product/mmaster');
			$data['page_title'] = $this->lang->line('list_productmotif');
			$data['isi']=$this->mmaster->cariproductmotif($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('product/vlistproductmotif', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
