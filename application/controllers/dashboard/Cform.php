<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
    $this->load->helper(array('file','directory','fusioncharts'));
		require_once("php/fungsi.php");
	}
  function index(){
    $tgl=date('Y-m-d');
    $graph_swfFile      = base_url().'flash/FCF_MSColumn3D.swf';
    $graph_caption      = 'DASHBOARD '.NmPerusahaan.' Periode : '.date('d-m-Y');
    $graph_numberPrefix = 'Rp.' ;
    $graph_title        = 'DASHBOARD '.NmPerusahaan.' Periode : '.date('d-m-Y');
    $graph_width        = 954;
    $graph_height       = 500;
    $this->load->model('dashboard/mmaster');

    // Area
    $i=0;
    $category[$i] = '';

    // data set
#    $dataset[0] = 'Kemarin';
    $dataset[0] = 'Penjualan Hari Kemarin';
    $dataset[1] = 'Penjualan Hari Ini';
    $dataset[2] = 'Penjualan Minggu Lalu';
    $dataset[3] = 'Penjualan Minggu Ini';
    $dataset[4] = 'Penjualan Bulan Lalu';
    $dataset[5] = 'Penjualan Bulan Ini';
    $dataset[6] = 'Penjualan Bulan Ini Tahun Lalu';
    $dataset[7] = 'Penjualan Tahun Lalu';
    $dataset[8] = 'Penjualan Tahun Ini';
    $dataset[9] = 'Retur Tahun Lalu';
    $dataset[10] = 'Retur Tahun Ini';
    $dataset[11] = 'Piutang Saat Ini';
    $dataset[12] = 'Uang Masuk Bulan Ini';
    $dataset[13] = 'Alokasi Uang Masuk Bulan Ini';
    $dataset[14] = 'Hutang Yang Jatuh Tempo Bulan Ini';
    $dataset[15] = 'Saldo Hutang Saat Ini';
    $dataset[16] = 'Pembayaran Hutang Saat Ini';
    $dataset[17] = 'Biaya Bulan Ini';

    $arrData['Penjualan Hari Kemarin'][0]='0';
    $arrData['Penjualan Hari Ini'][0]='0';		            
    $arrData['Penjualan Minggu Lalu'][0]='0';
    $arrData['Penjualan Minggu Ini'][0]='0';		            
    $arrData['Penjualan Bulan Lalu'][0]='0';
    $arrData['Penjualan Bulan Ini'][0]='0';		            
    $arrData['Penjualan Bulan Ini Tahun Lalu'][0]='0';
    $arrData['Penjualan Tahun Lalu'][0]='0';		            
    $arrData['Penjualan Tahun Ini'][0]='0';
    $arrData['Retur Tahun Lalu'][0]='0';		            
    $arrData['Retur Tahun Ini'][0]='0';
    $arrData['Piutang Saat Ini'][0]='0';		            
    $arrData['Uang Masuk Bulan Ini'][0]='0';
    $arrData['Alokasi Uang Masuk Bulan Ini'][0]='0';		            
    $arrData['Hutang Yang Jatuh Tempo Bulan Ini'][0]='0';
    $arrData['Saldo Hutang Saat Ini'][0]='0';		            
    $arrData['Pembayaran Hutang Saat Ini'][0]='0';
    $arrData['Biaya Bulan Ini'][0]='0';		            

    //data 1
    $i=0;
    $result = $this->mmaster->baca($tgl);
    foreach($result as $row){
      switch(substr($row->jenis,0,2)){
      case '01':
        $arrData['Penjualan Hari Kemarin'][$i] = floatval($row->jumlah);
        break;
      case '02':
        $arrData['Penjualan Hari Ini'][$i] = floatval($row->jumlah);
        break;
      case '03':
        $arrData['Penjualan Minggu Lalu'][$i] = floatval($row->jumlah);
        break;
      case '04':
        $arrData['Penjualan Minggu Ini'][$i] = floatval($row->jumlah);
        break;
      case '05':
        $arrData['Penjualan Bulan Lalu'][$i] = floatval($row->jumlah);
        break;
      case '06':
        $arrData['Penjualan Bulan Ini'][$i] = floatval($row->jumlah);
        break;
      case '07':
        $arrData['Penjualan Bulan Ini Tahun Lalu'][$i] = floatval($row->jumlah);
        break;
      case '08':
        $arrData['Penjualan Tahun Lalu'][$i] = floatval($row->jumlah);
        break;
      case '09':
        $arrData['Penjualan Tahun Ini'][$i] = floatval($row->jumlah);
        break;
      case '10':
        $arrData['Retur Tahun Lalu'][$i] = floatval($row->jumlah);
        break;
      case '11':
        $arrData['Retur Tahun Ini'][$i] = floatval($row->jumlah);
        break;
      case '12':
        $arrData['Piutang Saat Ini'][$i] = floatval($row->jumlah);
        break;
      case '13':
        $arrData['Uang Masuk Bulan Ini'][$i] = floatval($row->jumlah);
        break;
      case '14':
        $arrData['Alokasi Uang Masuk Bulan Ini'][$i] = floatval($row->jumlah);
        break;
      case '15':
        $arrData['Hutang Yang Jatuh Tempo Bulan Ini'][$i] = floatval($row->jumlah);
        break;
      case '16':
        $arrData['Saldo Hutang Saat Ini'][$i] = floatval($row->jumlah);
        break;
      case '17':
        $arrData['Pembayaran Hutang Saat Ini'][$i] = floatval($row->jumlah);
        break;
      case '18':
        $arrData['Biaya Bulan Ini'][$i] = floatval($row->jumlah);
        break;
      }
    }

    $strXML = "<graph hovercapbg='DEDEBE' hovercapborder='889E6D' rotateNames='0' yAxisMaxValue='100' numdivlines='9' divLineColor='CCCCCC' divLineAlpha='80' decimalPrecision='0' showAlternateHGridColor='1' AlternateHGridAlpha='30' AlternateHGridColor='CCCCCC' caption='".$graph_caption."' numberPrefix='".$graph_numberPrefix."' showValues='0'>" ;

    //Convert category to XML and append
    $strXML .= "<categories font='Arial' fontSize='11' fontColor='000000'>" ;
    foreach ($category as $c) {
        $strXML .= "<category name='".$c."'/>" ;
    }
    $strXML .= "</categories>" ;

    //Convert dataset and data to XML and append
    foreach ($dataset as $set) {
        $strXML .= "<dataset seriesname='".$set."' color='".  getFCColor()."'>" ;
        foreach ($arrData[$set] as $d) {
            $strXML .= "<set value='".$d."'/>" ;
        }
        $strXML .= "</dataset>" ;
    }

//Close <chart> element
$strXML .= "</graph>";

    $data['graph']  = renderChart($graph_swfFile, $graph_title, $strXML, "div" , $graph_width, $graph_height);
    $data['tgl']=$tgl;
    $data['modul']='dashboard';
		$data['isi']= directory_map('./flash/');
		$data['file']='';

    $this->load->view('dashboard/chart_view',$data) ;
  }
}
?>
