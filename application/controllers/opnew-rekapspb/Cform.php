<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu381')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opnew-rekapspb/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query = $this->db->query(" 	
						            select distinct(b.i_spmb) as no, a.d_spmb as tgl, a.i_spmb_old as asal, a.i_area as i_area, c.e_area_name as e_area_name, 
                        '' as e_customer_name, e.i_op, e.d_op, d.i_spb, d.d_spb
						            from tm_spmb_item b, tm_spmb a
                        left join tm_op e on (a.i_spmb=e.i_reff and a.i_area=e.i_area and e.f_op_close='f')
                        inner join tm_spb d on(not d.i_spmb isnull and a.i_spmb=d.i_spmb and a.i_area=d.i_area and d.f_spb_stockdaerah='t'
                                   and d.f_spb_cancel='f' and d.i_store isnull and not d.i_approve1 isnull and not d.i_approve2 isnull)
                        , tr_area c
                        where not a.i_approve2 isnull and not a.i_store isnull and not a.i_store_location isnull 
                        and a.f_op = 't' and a.f_spmb_pemenuhan='f'
                        and (b.n_deliver<b.n_acc and b.n_acc>0 and b.n_saldo>0)
                        and a.i_spmb=b.i_spmb 
                        and (
                        upper(c.i_area) like '%$cari%' or upper(c.e_area_name) like '%$cari%' or 
                        upper(a.i_spmb) like '%$cari%')
                        and a.i_area=c.i_area 
                        and a.f_spmb_opclose='f'
						            order by tgl, no",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			
			$data['page_title'] = $this->lang->line('op-rekapspb');
			$data['ispb']='';
			$data['iop']	= '';
			$data['jmlitem'] = ''; 				
			$data['detail']='';
			$this->load->model('opnew-rekapspb/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('opnew-rekapspb/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu381')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_spb');
			$this->load->view('opnew-rekapspb/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
		    (($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu381')=='t')) ||
		    (($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('op-rekapspb');
			if($this->uri->segment(5)){
				$ispb 			= $this->uri->segment(4);
				$iarea			= $this->uri->segment(5);
				$tmp=explode('-',$ispb);
				if($tmp[0]=='SPB'){
					$query 	= $this->db->query("select a.* from tm_spb_item a, tr_product b
												where a.i_spb = '$ispb'
												and a.i_area='$iarea' and b.i_product_status<>'4'
												and a.i_product=b.i_product
												and a.n_deliver<a.n_order
												and a.i_op isnull");
				}else if($tmp[0]=='SPMB'){
					$query 	= $this->db->query("select a.* from tm_spmb_item a, tr_product b
												where a.i_spmb = '$ispb' and b.i_product_status<>'4'
												and a.i_product=b.i_product
												and a.n_deliver<a.n_acc
                        and a.n_acc>0
                        and a.n_saldo>0
                        and a.n_stock<a.n_acc");
					$data['ispmbold'] 	  = $ispb;
				}
				$data['jmlitem']  = $query->num_rows(); 				
				$data['iop']	  = '';
				$data['iopold']	  = '';
				$data['ispb'] 	  = $ispb;
				$this->load->model('opnew-rekapspb/mmaster');
				$data['isi']=$this->mmaster->baca($ispb,$iarea);
				$data['detail']=$this->mmaster->bacadetail($ispb,$iarea);
        $data['tgl']=date('d-m-Y');
		 		$this->load->view('opnew-rekapspb/vmainform',$data);
			}else{
				$this->load->view('opnew-rekapspb/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function editop()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		  (($this->session->userdata('menu381')=='t') || ($this->session->userdata('menu58')=='t'))) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('op-rekapspb')." Update";
			if(	
				($this->uri->segment(4)) && ($this->uri->segment(5)) 
			  )
			{
				$ispb   	= $this->uri->segment(4);
				$iop		= $this->uri->segment(5);
				$isupplier	= $this->uri->segment(6);
				$area		= $this->uri->segment(7);
				$dfrom	= $this->uri->segment(8);
				$dto		= $this->uri->segment(9);
				$query 		= $this->db->query("select * from tm_op_item where i_op = '$iop'");
				$data['jmlitem'] 	= $query->num_rows(); 				
				$data['ispb'] 		= $ispb;
				$data['iop'] 		= $iop;
				$data['supplier']	= $isupplier;
				$this->load->model('opnew-rekapspb/mmaster');
				$data['isi']=$this->mmaster->bacaop($iop,$area);
				$data['detail']=$this->mmaster->bacadetailop($iop,$area);
        $data['dfrom']=$dfrom;
        $data['dto']=$dto;
		 		$this->load->view('opnew-rekapspb/vmainform',$data);
			}else{
				$this->load->view('opnew-rekapspb/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu381')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
#			$iop		    = $this->input->post('iop', TRUE);
			$iopold		  = $this->input->post('iopold', TRUE);
#			$isupplier 	= $this->input->post('isupplier', TRUE);
			$iarea 		  = $this->input->post('iarea', TRUE);
			$iopstatus 	= $this->input->post('iopstatus', TRUE);
			$ireff		  = $this->input->post('ispb', TRUE);
			$dop    		= $this->input->post('dop', TRUE);
			$old		    = $this->input->post('asal', TRUE);
			if($dop!=''){
				$tmp=explode("-",$dop);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dop=$th."-".$bl."-".$hr;
#        $thbl=substr($th,2,2).$bl;
        $thbl=$th.$bl;
			}
			$dreff		= $this->input->post('dspb', TRUE);
			if($dreff!=''){
				$tmp=explode("-",$dreff);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dreff=$th."-".$bl."-".$hr;
			}
			$eopremark		= $this->input->post('eopremark', TRUE);
			if($eopremark=='')
				$eopremark=null;
			$ndeliverylimit	= $this->input->post('ndeliverylimit', TRUE);
			$ntoplength		  = $this->input->post('ntoplength', TRUE);
			$jml			      = $this->input->post('jml');
      $i=0;
			if(($iopstatus!='') && ($dop!=''))
			{
###
		    $this->load->model('opnew-rekapspb/mmaster');
 				$this->db->trans_begin();
			  $tmp=explode('-',$ireff);
			  if($tmp[0]=='SPB'){
				  $quer = $this->db->query(" select distinct(a.i_supplier),b.* from tr_product a, tr_supplier b
											  where a.i_product in (select i_product from tm_spb_item
											  where i_spb='$ireff' and i_area='$iarea' and i_op isnull and n_deliver<n_order)
											  and a.i_supplier=b.i_supplier order by b.n_supplier_urut",false);
			  }else if($tmp[0]=='SPMB'){
				  $quer = $this->db->query(" select distinct(a.i_supplier),b.* from tr_product a, tr_supplier b
											  where a.i_product in (select i_product from tm_spmb_item
											  where i_spmb='$ireff' and i_area='$iarea'and n_stock<n_saldo and n_saldo>0)
											  and a.i_supplier=b.i_supplier order by b.n_supplier_urut",false);
			  }
###
       if($quer->num_rows() > 0){
 
          $iopx='';
          $iopz='';
          foreach($quer->result() as $supp){
            $isupplier=$supp->i_supplier;
            $ndeliverylimit=1;
            $ntoplength=$supp->n_supplier_toplength;
            $iop='';
				    for($i=1;$i<=$jml;$i++){
				      $norder		= $this->input->post('norder'.$i, TRUE);
              $iproduct	= $this->input->post('iproduct'.$i, TRUE);
              $rp=$this->mmaster->cekproduct($iproduct);
              $pemasok=$rp->i_supplier;
				      if( ($norder!='0') && ($pemasok==$isupplier) ){
                if($iop==''){
				          $iop	= $this->mmaster->runningnumber($thbl);
				          $this->mmaster->insertheader( $iop, $dop, $isupplier, $iarea, $iopstatus, $ireff, $eopremark, 
                                                $ndeliverylimit, $ntoplength, $dreff, $old,$iopold);
                }
					      $iproductgrade	= 'A';
					      $iproductmotif	= $this->input->post('motif'.$i, TRUE);
					      $eproductname		= $this->input->post('eproductname'.$i, TRUE);
					      $vproductmill		= $this->input->post('vproductmill'.$i, TRUE);
					      $vproductmill		= str_replace(',','',$vproductmill);
					      $norder				  = $this->input->post('norder'.$i, TRUE);
					      $nquantitystock	= $this->input->post('nquantitystock'.$i, TRUE);
					      $this->mmaster->insertdetail( $iop,$iproduct,$iproductgrade,$eproductname,$norder,
												      $vproductmill,$iproductmotif,$i);
					      $this->mmaster->updatespb($ireff,$iop,$iproduct,$iproductgrade,$iproductmotif,$iarea,$norder);
              }
    			  }
            if( ($iopx!=$iop) && ($iop!='') ){
              $iopx=$iop;
              if($iopz==''){
                $iopz=$iop.' - '.$supp->e_supplier_name.'<br>';
              }else{
                $iopz=$iopz.$iop.' - '.$supp->e_supplier_name.'<br>';
              }
            }
          }
				}else{
          $iopz= 'ga ada OP-nya';
        }
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
#					$this->db->trans_rollback();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input OP No:'.$iop;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $iopz;
					$this->load->view('opew',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function updateop()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu381')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iop		= $this->input->post('iop', TRUE);
			$iopold		= $this->input->post('iopold', TRUE);
			$isupplier 	= $this->input->post('isupplier', TRUE);
			$iarea 		= $this->input->post('iarea', TRUE);
			$iopstatus 	= $this->input->post('iopstatus', TRUE);
			$ireff		= $this->input->post('ispb', TRUE);
			$dop		= $this->input->post('dop', TRUE);
			$old		= $this->input->post('asal', TRUE);
			if($dop!=''){
				$tmp=explode("-",$dop);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dop=$th."-".$bl."-".$hr;
			}
			$dreff		= $this->input->post('dspb', TRUE);
			if($dreff!=''){
				$tmp=explode("-",$dreff);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dreff=$th."-".$bl."-".$hr;
			}
			$eopremark		= $this->input->post('eopremark', TRUE);
			if($eopremark=='')
				$eopremark=null;
			$ndeliverylimit	= $this->input->post('ndeliverylimit', TRUE);
			$ntoplength		= $this->input->post('ntoplength', TRUE);
			$jml			= $this->input->post('jml', TRUE);
			if(($isupplier!='') && ($iopstatus!='') && ($dop!=''))
				$benar			= 'false';
				$this->db->trans_begin();
				$this->load->model('opnew-rekapspb/mmaster');
#				$iop	= $this->mmaster->runningnumber();
				$this->mmaster->updateheader($iop, $dop, $isupplier, $iarea, $iopstatus, $ireff, 
											 $eopremark, $ndeliverylimit, $ntoplength, $dreff, $old, $iopold);
				for($i=1;$i<=$jml;$i++){
				  $norder					=$this->input->post('norder'.$i, TRUE);
				  if($norder!='0'){
					$iproduct				=$this->input->post('iproduct'.$i, TRUE);
					$iproductgrade			='A';
					$iproductmotif			=$this->input->post('motif'.$i, TRUE);
					$eproductname			=$this->input->post('eproductname'.$i, TRUE);
					$vproductmill			=$this->input->post('vproductmill'.$i, TRUE);
					$vproductmill			=str_replace(',','',$vproductmill);
					$norder					=$this->input->post('norder'.$i, TRUE);
					$data['iproduct']		=$iproduct;
					$data['iproductgrade']	=$iproductgrade;
					$data['iproductmotif']	=$iproductmotif;
					$data['eproductname']	=$eproductname;
					$data['vproductmill']	=$vproductmill;
					$data['norder']			=$norder;
					$this->mmaster->deletedetail($iproduct, $iproductgrade, $iop, $iproductmotif);
					$this->mmaster->insertdetail( $iop,$iproduct,$iproductgrade,$eproductname,$norder,
												$vproductmill,$iproductmotif,$i);
					$this->mmaster->updatespb($ireff,$iop,$iproduct,$iproductgrade,$iproductmotif,$iarea,$norder);
				  }
				}
#				$this->mmaster->updatespb($ireff,$iop);
				$benar='true';
				if ($this->db->trans_status() === FALSE)
				{
					$this->db->trans_rollback();
				}else{
					$this->db->trans_commit();
#					$this->db->trans_rollback();

					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Update OP No:'.$iop;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );
	
  				$data['sukses']			= true;
					$data['inomor']			= $iop;
					$this->load->view('nomor',$data);
				}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu381')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$spb=$this->uri->segment(4);
			$data['spb']=$this->uri->segment(4);
			$area=$this->uri->segment(5);
			$data['area']=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/opnew-rekapspb/cform/supplier/'.$spb.'/'.$area.'/';
			$tmp=explode('-',$spb);
			if($tmp[0]=='SPB'){
				$query = $this->db->query(" select distinct(a.i_supplier),b.* from tr_product a, tr_supplier b
											where a.i_product in (select i_product from tm_spb_item
											where i_spb='$spb' and i_area='$area' and i_op isnull and n_deliver<n_order)
											and a.i_supplier=b.i_supplier",false);
			}else if($tmp[0]=='SPMB'){
				$query = $this->db->query(" select distinct(a.i_supplier),b.* from tr_product a, tr_supplier b
											where a.i_product in (select i_product from tm_spmb_item
											where i_spmb='$spb' and i_area='$area'and n_stock<n_saldo and n_saldo>0)
											and a.i_supplier=b.i_supplier",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('opnew-rekapspb/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(6),$spb,$area);
			$this->load->view('opnew-rekapspb/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu381')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opnew-rekapspb/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("select * from tr_supplier
									   where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('opnew-rekapspb/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('opnew-rekapspb/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu381')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/opnew-rekapspb/cform/index/';
			$query = $this->db->query(" 	select distinct(b.i_spb) as no
					              from tm_spb_item b, tm_spb a left join tm_op e on (a.i_spb=e.i_reff and a.i_area=e.i_area and e.f_op_close='f'),
                        tr_customer_area c, tr_customer d
					              where not a.i_approve1 isnull
					              and not a.i_approve2 isnull
					              and not a.i_store isnull
					              and not a.i_store_location isnull
					              and a.f_spb_op = 't'
					              and b.i_op isnull
												and a.f_spb_cancel = 'f'
					              and a.f_spb_stockdaerah='f'
					              and a.i_nota isnull
					              and (upper(a.i_spb) like '%$cari%' or upper(a.i_spb_old) like '%$cari%' 
					                or upper(d.i_customer) like '%$cari' or  upper(d.e_customer_name) like '%$cari%')
					              and a.i_spb=b.i_spb and a.i_area=b.i_area and a.f_spb_pemenuhan='f' and b.n_deliver<b.n_order
					              and d.i_customer=c.i_customer and d.i_customer=a.i_customer
					              and a.i_customer=c.i_customer
      						union all
						            select distinct(b.i_spmb) as no
						            from tm_spmb_item b, tm_spmb a left join tm_op e on (a.i_spmb=e.i_reff and a.i_area=e.i_area and e.f_op_close='f'), tr_area c
						            where not a.i_approve2 isnull
						            and not a.i_store isnull
						            and not a.i_store_location isnull
						            and a.f_op = 't' and a.f_spmb_pemenuhan='f'
						            and (b.n_deliver<b.n_acc and b.n_acc>0 and b.n_saldo>0)
						            and a.i_spmb=b.i_spmb 
						            and (upper(c.i_area) like '%$cari%' or upper(a.i_spmb) like '%$cari%'
						            or upper(c.e_area_name) like '%$cari%')
						            and a.i_area=c.i_area 
						            and a.f_spmb_opclose='f' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('opnew-rekapspb/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$data['page_title'] = $this->lang->line('op-rekapspb');
			$data['ispb']='';
			$data['iop']	= '';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('opnew-rekapspb/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function status()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu381')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/opnew-rekapspb/cform/status/index/';
			$query = $this->db->query("select * from tr_op_status",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('opnew-rekapspb/mmaster');
			$data['page_title'] = $this->lang->line('list_opstatus');
			$data['isi']=$this->mmaster->bacastatus($config['per_page'],$this->uri->segment(5));
			$this->load->view('opnew-rekapspb/vliststatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu381')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris=$this->uri->segment(4);
			$spb=$this->uri->segment(5);
			$area =$this->uri->segment(6);
      $cari=strtoupper($this->input->post("cari",false));
			$config['base_url'] = base_url().'index.php/opnew-rekapspb/cform/product/'.$baris.'/'.$spb.'/'.$area.'/';
      $tmp=explode('-',$spb);
			if($tmp[0]=='SPB'){
			  $query = $this->db->query(" select a.*, b.i_store, b.i_store_location, b.i_price_group, c.e_product_motifname, d.v_product_mill
									                  from tm_spb_item a, tm_spb b, tr_product_motif c, tr_product d
									                  where b.i_spb = '$spb' and b.i_area='$area' and b.i_spb=a.i_spb and b.i_area=a.i_area
									                  and a.i_product=d.i_product and d.i_product_status<>'4'
									                  and a.i_product_motif=c.i_product_motif and a.i_product=c.i_product
									                  order by a.n_item_no",false);
      }elseif($tmp[0]=='SPMB'){
        $query = $this->db->query(" select a.*, b.i_store, b.i_store_location, c.e_product_motifname, d.v_product_mill
									                  from tm_spmb_item a, tm_spmb b, tr_product_motif c, tr_product d
									                  where b.i_spmb = '$spb' and b.i_spmb=a.i_spmb and a.i_product=d.i_product
									                  and b.i_area='$area' and d.i_product_status<>'4'
									                  and a.i_product_motif=c.i_product_motif and a.i_product=c.i_product
									                  order by a.n_item_no",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('opnew-rekapspb/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($cari,$spb,$area,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$data['spb']=$spb;
			$data['area']=$area;
			$data['cari']=$cari;
			$this->load->view('opnew-rekapspb/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu381')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris=strtoupper($this->input->post("baris",false));
			$spb=strtoupper($this->input->post("spb",false));
			$area =strtoupper($this->input->post("area",false));
      $cari=strtoupper($this->input->post("cari",false));
			$config['base_url'] = base_url().'index.php/opnew-rekapspb/cform/product/'.$baris.'/'.$spb.'/'.$area.'/';
      $tmp=explode('-',$spb);
			if($tmp[0]=='SPB'){
			  $query = $this->db->query(" select a.*, b.i_store, b.i_store_location, b.i_price_group, c.e_product_motifname, d.v_product_mill
									                  from tm_spb_item a, tm_spb b, tr_product_motif c, tr_product d
									                  where b.i_spb = '$ispb' and b.i_area='$iarea' and b.i_spb=a.i_spb and b.i_area=a.i_area
									                  and a.i_product=d.i_product and d.i_product_status<>'4'
									                  and a.i_product_motif=c.i_product_motif and a.i_product=c.i_product
                                    and (upper(a.i_product) like '%$cari%' or upper(d.e_product_name) like '%$cari%')
									                  order by a.n_item_no",false);
      }elseif($tmp[0]=='SPMB'){
        $query = $this->db->query(" select a.*, b.i_store, b.i_store_location, c.e_product_motifname, d.v_product_mill
									                  from tm_spmb_item a, tm_spmb b, tr_product_motif c, tr_product d
									                  where b.i_spmb = '$ispb' and b.i_spmb=a.i_spmb and a.i_product=d.i_product
									                  and b.i_area='$iarea' and d.i_product_status<>'4'
									                  and a.i_product_motif=c.i_product_motif and a.i_product=c.i_product
                                    and (upper(a.i_product) like '%$cari%' or upper(d.e_product_name) like '%$cari%')
									                  order by a.n_item_no",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('opnew-rekapspb/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($cari,$spb,$area,$config['per_page'],$this->uri->segment(7));
			$data['baris']=$baris;
			$data['spb']=$spb;
			$data['area']=$area;
			$data['cari']=$cari;
			$this->load->view('opnew-rekapspb/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
