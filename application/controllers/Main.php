<?php 
class Main extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if ($this->session->userdata('logged_in')){
			if($this->session->userdata('status_update')=='t'){
			    $data['page_title'] = $this->lang->line('updatepassword');
	        	$this->load->view('updatepassword/index.php',$data);
		  	}else{
        		$data['username'] = $this->get_menu($this->session->userdata('user_id'));
				$this->load->view('main/index.php',$data);
		  	}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	private function get_menu($username){
		$this->load->model("mmaster");

		$navigationmenu = $this->mmaster->getmenu($username);

		$echo = '';
		$echo .="<ul>\n";
		foreach($navigationmenu as $parent){
			if($parent->e_menu_url == '#' && $parent->i_parent == '0'){
				$echo .="<li>\n";
				$echo .="<a href='#'>$parent->e_menu</a>\n";
				$echo .=($parent->e_menu_url == '#') ? "<ul>\n" : "";
				foreach($navigationmenu as $ch){
					if($parent->i_menu == $ch->i_parent){
						$echo .="<li>\n";
						$echo .=($ch->e_menu_url == '#') ? "<a href='#'>$ch->e_menu</a>\n" :
						$this->pquery->link_to_remote($ch->e_menu, array('url' => site_url($ch->e_menu_url) , 'update' => '#main'));
						$echo .=($ch->e_menu_url == '#') ? "<ul>\n" : "";
						foreach($navigationmenu as $ch2){
							if($ch->i_menu == $ch2->i_parent){
								$echo .="<li>\n";
								$echo .=($ch2->e_menu_url == '#') ? "<a href='#'>$ch2->e_menu</a>\n" :
								$this->pquery->link_to_remote($ch2->e_menu, array('url' => site_url($ch2->e_menu_url) , 'update' => '#main'));
								$echo .=($ch2->e_menu_url == '#') ? "<ul>\n" : "";
								foreach($navigationmenu as $ch3){
									if($ch2->i_menu == $ch3->i_parent){
										$echo .="<li>\n";
										$echo .=($ch3->e_menu_url == '#') ? "<a href='#'>$ch3->e_menu</a>\n" :
										$this->pquery->link_to_remote($ch3->e_menu, array('url' => site_url($ch3->e_menu_url) , 'update' => '#main'));
										$echo .=($ch3->e_menu_url == '#') ? "<ul>\n" : "";
										foreach($navigationmenu as $ch4){
											if($ch3->i_menu == $ch4->i_parent){
												$echo .="<li>\n";
												$echo .=($ch4->e_menu_url == '#') ? "<a href='#'>$ch4->e_menu</a>\n" :
												$this->pquery->link_to_remote($ch4->e_menu, array('url' => site_url($ch4->e_menu_url) , 'update' => '#main'));
												$echo .="</li>\n";
											}
										}
										$echo .=($ch3->e_menu_url == '#') ? "</ul>\n" : "";
										$echo .="</li>\n";
									}
								}
								$echo .=($ch2->e_menu_url == '#') ? "</ul>\n" : "";
								$echo .="</li>\n";
							}
						}
						$echo .=($ch->e_menu_url == '#') ? "</ul>\n" : "";
						$echo .="</li>\n";
					}
				}
				$echo .=($parent->e_menu_url == '#') ? "</ul>\n" : "";
				$echo .="</li>\n";
			}
		}

		$echo .="</ul>\n</li>\n";
		$echo .="</ul>\n";

		return $echo;
	}
	function taxvalue($dreference){
		$this->load->model("mmaster");

		echo $this->mmaster->_taxvalue(date('Y-m-d',strtotime(($dreference))));
	}
}
?>
