<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		        ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productbase/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query=$this->db->query("select * from tr_product where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%' ",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_product');
			$data['iproduct']='';
			$this->load->model('productbase/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('productbase/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct			    = $this->input->post('iproduct', TRUE);
			$iproductsupplier	= $this->input->post('iproductsupplier', TRUE);
			$isupplier			  = $this->input->post('isupplier', TRUE);
			$iproductstatus		= $this->input->post('iproductstatus', TRUE);
			$iproducttype			= $this->input->post('iproducttype', TRUE);
			$iproductcategory	= $this->input->post('iproductcategory', TRUE);
			$iproductclass		= $this->input->post('iproductclass', TRUE);
			$iproductgroup		= $this->input->post('iproductgroup', TRUE);
			$eproductname			= $this->input->post('eproductname', TRUE);
      $nproductmargin		= $this->input->post('nproductmargin', TRUE);
			$eproductsuppliername	= $this->input->post('eproductsuppliername', TRUE);
			$vproductretail		= str_replace(",","",$this->input->post('vproductretail', TRUE));
			$vproductmill			= str_replace(",","",$this->input->post('vproductmill', TRUE));
			$fproductpricelist= $this->input->post('fproductpricelist', TRUE);
			$dproductstopproduction		= $this->input->post('dproductstopproduction', TRUE);
			$dproductregister	= $this->input->post('dproductregister', TRUE);
			if($vproductretail=='')
				$vproductretail=0;
			if($vproductmill=='')
				$vproductmill=0;
			if ($iproduct != '' && $eproductname != '')
			{
				$this->load->model('productbase/mmaster');
				$this->mmaster->insert
					(
					$iproduct, $iproductsupplier, $isupplier, $iproductstatus, 
					$iproducttype, $iproductcategory, $iproductclass, $iproductgroup, 
					$eproductname, $eproductsuppliername, $vproductretail, $vproductmill,
					$fproductpricelist, $dproductstopproduction, $dproductregister
					);
        $this->mmaster->insertmotif('00','ST',$iproduct,$eproductname);
        $this->mmaster->insertprice($iproduct,$eproductname,'A',$nproductmargin,$vproductmill);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_product');
			$this->load->view('productbase/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_product')." update";
			if($this->uri->segment(4)){
				$iproduct = $this->uri->segment(4);
				$data['iproduct'] = $iproduct;
				$this->load->model('productbase/mmaster');
				$data['isi']=$this->mmaster->baca($iproduct);
		 		$this->load->view('productbase/vmainform',$data);
			}else{
				$this->load->view('productbase/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct			= $this->input->post('iproduct', TRUE);
			$iproductsupplier		= $this->input->post('iproductsupplier', TRUE);
			$isupplier				= $this->input->post('isupplier', TRUE);
			$iproductstatus			= $this->input->post('iproductstatus', TRUE);
			$iproducttype			= $this->input->post('iproducttype', TRUE);
			$iproductcategory		= $this->input->post('iproductcategory', TRUE);
			$iproductclass			= $this->input->post('iproductclass', TRUE);
			$iproductgroup			= $this->input->post('iproductgroup', TRUE);
			$eproductname			= $this->input->post('eproductname', TRUE);
			$eproductsuppliername		= $this->input->post('eproductsuppliername', TRUE);
			$vproductretail			= str_replace(",","",$this->input->post('vproductretail', TRUE));
			$vproductmill			= str_replace(",","",$this->input->post('vproductmill', TRUE));
			$fproductpricelist		= $this->input->post('fproductpricelist', TRUE);
			$dproductstopproduction		= $this->input->post('dproductstopproduction', TRUE);
			$dproductregister		= $this->input->post('dproductregister', TRUE);
			if($vproductretail=='')
				$vproductretail=0;
			if($vproductmill=='')
				$vproductmill=0;
			$this->load->model('productbase/mmaster');
			$this->mmaster->update(
						$iproduct, $iproductsupplier, $isupplier, $iproductstatus, 
						$iproducttype, $iproductcategory, $iproductclass, 
						$iproductgroup, $eproductname,
						$eproductsuppliername, $vproductretail, $vproductmill, 
						$fproductpricelist, $dproductstopproduction, $dproductregister
					      );
      $config['base_url'] = base_url().'index.php/productbase/cform/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$query=$this->db->query("select * from tr_product where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%' ",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_product');
			$data['iproduct']='';
			$this->load->model('productbase/mmaster');
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('productbase/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct	= $this->uri->segment(4);
			$this->load->model('productbase/mmaster');
			$this->mmaster->delete($iproduct);

			$config['base_url'] = base_url().'index.php/productbase/cform/index/';
			$config['total_rows'] = $this->db->count_all('tr_product');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['page_title'] = $this->lang->line('master_product');
			$data['iproduct']='';
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('productbase/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productbase/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query=$this->db->query("select * from tr_product where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%' ",FALSE);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('productbase/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_product');
			$data['iproduct']='';
	 		$this->load->view('productbase/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function productgroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productbase/cform/productgroup/index/';
			$config['total_rows'] = $this->db->count_all('tr_product_group');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('productbase/mmaster');
			$data['page_title'] = $this->lang->line('list_productgroup');
			$data['isi']=$this->mmaster->bacagroup($config['per_page'],$this->uri->segment(5));
			$this->load->view('productbase/vlistproductgroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductgroup()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productbase/cform/productgroup/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_product_group 
										where upper(i_product_group) like '%$cari%' 
										or upper(e_product_groupname) like '%$cari%'",false);
			$config['total_rows'] 	= $query->num_rows(); 
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productbase/mmaster');
			$data['page_title'] = $this->lang->line('list_productgroup');
			$data['isi']=$this->mmaster->carigroup($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productbase/vlistproductgroup', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productbase/cform/supplier/index/';
			$config['total_rows'] = $this->db->count_all('tr_supplier');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('productbase/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('productbase/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productbase/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_supplier 
										where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productbase/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productbase/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function productstatus()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productbase/cform/productstatus/index/';
			$config['total_rows'] = $this->db->count_all('tr_product_status');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('productbase/mmaster');
			$data['page_title'] = $this->lang->line('list_productstatus');
			$data['isi']=$this->mmaster->bacaproductstatus($config['per_page'],$this->uri->segment(5));
			$this->load->view('productbase/vlistproductstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductstatus()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productbase/cform/productstatus/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_product_status 
										where upper(i_product_status) like '%$cari%' or upper(e_product_statusname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productbase/mmaster');
			$data['page_title'] = $this->lang->line('list_productstatus');
			$data['isi']=$this->mmaster->cariproductstatus($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productbase/vlistproductstatus', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function producttype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproductgroup = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/productbase/cform/producttype/index/';
			$query = $this->db->query("select * from tr_product_type where i_product_type = '$iproductgroup'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('productbase/mmaster');
			$data['page_title'] = $this->lang->line('list_producttype');
			$data['iproductgroup'] = $iproductgroup;
			$data['isi']=$this->mmaster->bacaproducttype($iproductgroup,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productbase/vlistproducttype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproducttype()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productbase/cform/producttype/index/';
			$iproductgroup = $this->input->post('iproductgroup', FALSE);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_product_type where upper(i_product_type) like '%$cari%' 
										or upper(e_product_typename) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productbase/mmaster');
			$data['page_title'] = $this->lang->line('list_producttype');
			$data['iproductgroup'] = $iproductgroup;
			$data['isi']=$this->mmaster->cariproducttype($iproductgroup,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productbase/vlistproducttype', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productcategory()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproductclass = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/productbase/cform/productcategory/'.$iproductclass.'/';
			$query = $this->db->query("select * from tr_product_category where i_product_class = '$iproductclass'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('productbase/mmaster');
			$data['page_title'] = $this->lang->line('list_productcategory');
			$data['iproductclass'] = $iproductclass;
			$data['isi']=$this->mmaster->bacaproductcategory($iproductclass,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productbase/vlistproductcategory', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductcategory()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productbase/cform/productcategory/index/';
			$iproductclass = $this->input->post('iproductclass', FALSE);
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_product_category where (upper(i_product_category) like '%$cari%' 
										or upper(e_product_categoryname) like '%$cari%')
										and i_product_class = '$iproductclass'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productbase/mmaster');
			$data['page_title'] = $this->lang->line('list_productcategory');
			$data['iproductclass'] = $iproductclass;
			$data['isi']=$this->mmaster->cariproductcategory($iproductclass,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productbase/vlistproductcategory', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productclass()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productbase/cform/productclass/index/';
			$config['total_rows'] = $this->db->count_all('tr_product_class');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('productbase/mmaster');
			$data['page_title'] = $this->lang->line('list_productclass');
			$data['isi']=$this->mmaster->bacaproductclass($config['per_page'],$this->uri->segment(5));
			$this->load->view('productbase/vlistproductclass', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductclass()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu14')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productbase/cform/productclass/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_product_class where upper(i_product_class) like '%$cari%' 
										or upper(e_product_classname) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productbase/mmaster');
			$data['page_title'] = $this->lang->line('list_productclass');
			$data['isi']=$this->mmaster->cariproductclass($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productbase/vlistproductclass', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari_barang() {
		$nbrg	= $this->input->post('nbrg')?$this->input->post('nbrg'):$this->input->get_post('nbrg');
		$this->load->model('productbase/mmaster');
		$qnbrg	= $this->mmaster->cari_brg($nbrg);
		if($qnbrg->num_rows()>0) {
					$data['konfirm']	= true;
					$data['message']	= "Maaf,Kode Brg sudah ada.";
					$this->load->view('konfirm',$data);
		}
	}	
}
?>
