<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu529')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('infostatus');
			$data['dfrom']	= '';
			$data['dto']	= '';
			$this->load->view('infostatus/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
	}
}



function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu529')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){

			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/infostatus/cform/view/'.$dfrom.'/'.$dto.'/';
		



		//	$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '50';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('infostatus/mmaster');
			$data['page_title'] = $this->lang->line('infostatus');
			$data['cari']	= $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	= $dto;
			$data['isi']	= $this->mmaster->bacaperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(6),$cari);
			

      $sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		=  $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}

			$data['user']	= $this->session->userdata('user_id');
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Membuka Data SPB Per Area tanggal:'.$dfrom.' sampai:'.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('infostatus/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detailperdata()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu465')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			
			
			$listdata = array();
				$sqlnya	= $this->db->query("  
								  select b.e_product_name,sum(b.n_order) as qty  from tm_spb a, tm_spb_item b, tr_area c
where a.f_spb_stockdaerah='t'
 and (a.d_spb >= ('2016-09-01') AND a.d_spb <= ('2016-10-30')) 
 and a.f_spb_cancel='f'
 and a.i_area=b.i_area
 and a.i_spb=b.i_spb
 and a.i_area=c.i_area
 and not a.i_approve1 isnull 
 and not a.i_approve2 isnull 
 and a.i_store isnull
group by   b.e_product_name
order by e_product_name asc ");
								  
								  
			
						
						$listdata[] = array(	'productname'=> $product,	
												'qty'=> $qty
												);
				
			
			// -----------------------------------------------------------------------------------------------------
			//print_r($listnota); die();
			$data['page_title'] = $this->lang->line('realisasispbvssj');
			$data['iarea']		= $iarea;
			$data['namaarea']	= $namaarea;
			$data['isalesman']		= $isalesman;
			$data['namasalesman']	= $namasalesman;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['listdata']	= $listdata;

			$this->load->view('infostatus/detail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}

?>
