<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu133')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('labarugi');
			$data['periode']	= '';
			$this->load->view('akt-lr/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu133')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$periode	= $this->uri->segment(4);
			if(
				($periode!='')
			  )
			{
				$this->load->model('akt-lr/mmaster');
				$data['page_title'] = $this->lang->line('labarugi');
				$tmp 	= explode("-", $periode);
				$tahun	= $tmp[2];
				$bulan	= $tmp[1];
				$tanggal= $tmp[0];
				$periode=$tahun.$bulan;
				$kiwari		= $tahun."/".$bulan."/01";
				$namabulan	= $this->mmaster->NamaBulan($bulan);
				$data['periode']	= $periode;
				$dfrom				= $tahun."-".$bulan."-01";
				$dto				= $tahun."-".$bulan."-".$tanggal;
				$data['dfrom']		= $dfrom;
				$data['dto']		= $tahun."-".$bulan."-".$tanggal;
				$data['tanggal']	= $tanggal;
				$data['namabulan']	= $namabulan;
				$data['tahun']		= $tahun;
				$data['pendapatan']	= $this->mmaster->bacapendapatan($periode);
				$data['pendapatanlain']	= $this->mmaster->bacapendapatanlain($periode);
				$data['pembelian']	= $this->mmaster->bacapembelian($periode);
				$data['bebanadm']	= $this->mmaster->bacabebanadministrasi($periode);
				$data['bebanope']	= $this->mmaster->bacabebanoperasional($periode);
				$data['bebanlai']	= $this->mmaster->bacabebanlainnya($periode);

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='Buka Laporan Laba-Rugi Periode:'.$periode;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan );

				$this->load->view('akt-lr/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
