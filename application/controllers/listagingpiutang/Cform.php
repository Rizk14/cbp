<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->library('fungsi');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listagingpiutang');
      $data['djt']='';
			$this->load->view('listagingpiutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari	= strtoupper($this->input->post('cari'));
	    $djt	= $this->input->post('djt');
	    $iarea	= $this->input->post('iarea');
			if($djt=='') $djt	= $this->uri->segment(4);
			if($iarea=='') $iarea	= $this->uri->segment(5);
      if($cari=='' && ($this->uri->segment(6)=='index' or $this->uri->segment(6)=='')){
  			$config['base_url'] = base_url().'index.php/listagingpiutang/cform/view/'.$djt.'/'.$iarea.'/index/';
      }else{
   			if($cari=='') $cari	= $this->uri->segment(6);
        $config['base_url'] = base_url().'index.php/listagingpiutang/cform/view/'.$djt.'/'.$iarea.'/'.$cari.'/';
      }
      if($iarea=='NA'){
        $query = $this->db->query(" select a.v_sisa from tm_nota a, tr_customer b, tr_customer_groupar c
																    where a.i_customer=b.i_customer and a.i_customer=c.i_customer 
																    and a.f_ttb_tolak='f'
																    and not a.i_nota isnull
																    and (upper(a.i_nota) like '%$cari%' 
																	    or upper(a.i_spb) like '%$cari%' 
																	    or upper(a.i_customer) like '%$cari%'
                                      or upper(c.i_customer_groupar) like '%$cari%'  
																	    or upper(b.e_customer_name) like '%$cari%')
                                    and a.v_sisa>0
                                    and a.f_nota_cancel='f'",false);
      }else{
        $query = $this->db->query(" select a.v_sisa from tm_nota a, tr_customer b, tr_customer_groupar c, tr_area d
																    where a.i_customer=b.i_customer and a.i_customer=c.i_customer 
																    and a.f_ttb_tolak='f' and a.i_area=d.i_area and d.i_area_parent='$iarea'
																    and not a.i_nota isnull
																    and (upper(a.i_nota) like '%$cari%' 
																	    or upper(a.i_spb) like '%$cari%' 
																	    or upper(a.i_customer) like '%$cari%'
                                      or upper(c.i_customer_groupar) like '%$cari%'  
																	    or upper(b.e_customer_name) like '%$cari%')
                                    and a.v_sisa>0
                                    and a.f_nota_cancel='f'",false);
      }
#                                    and a.d_jatuh_tempo >= to_date('$djt','dd-mm-yyyy')
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('listagingpiutang/mmaster');
  		$data['page_title'] = $this->lang->line('listagingpiutang');
			$data['cari']		= $cari;
			$data['djt']	= $djt;
			$data['isi']		= $this->mmaster->bacaperiode($djt,$iarea,$config['per_page'],$this->uri->segment(7),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Buka Aging piutang jatuh tempo:'.$djt;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('listagingpiutang/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listagingpiutang');
			$this->load->view('listagingpiutang/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listagingpiutang/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listagingpiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listagingpiutang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listagingpiutang/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listagingpiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('listagingpiutang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
     	 ($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$dto	    = $this->input->post('dto');
			$iarea= $this->input->post('iarea');
      if($iarea=='')$icustomer=$this->uri->segment(4);
      if($dto=='')$dto=$this->uri->segment(5);
			$this->load->model('listagingpiutang/mmaster');
      if($dto!=''){
				$tmp=explode("-",$dto);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dto=$th."-".$bl."-".$hr;
			}
			$data['dto']  = $dto;
			$data['iarea']= $iarea;
			$data['page_title'] = $this->lang->line('printopnnotaarea');
			$data['isi']=$this->mmaster->baca($iarea,$dto);
			$data['user']	= $this->session->userdata('user_id');
      $sess = $this->session->userdata('session_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= $this->db->query($sql);
			if($rs->num_rows>0){
				foreach($rs->result() as $tes){
					$ip_address	  = $tes->ip_address;
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Buka Aging piutang jatuh tempo:'.$dto.' Area:'.$iarea;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('listagingpiutang/vformrpt', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function detail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu388')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$icustomergroupar= $this->input->post('icustomergroupar');
			$ecustomername= $this->input->post('ecustomername');
      $djt	= $this->input->post('djt');
			if($icustomergroupar=='') $icustomergroupar	= $this->uri->segment(4);
			if($djt=='') $djt	= $this->uri->segment(5);
			if($ecustomername=='') $ecustomername	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listagingpiutang/cform/detail/'.$icustomergroupar.'/'.$djt.'/'.$ecustomername.'/';
      $ecustomername=str_replace('%20',' ',$ecustomername);
      $ecustomername=str_replace('tandakurungbuka','(',$ecustomername);
      $ecustomername=str_replace('tandakurungtutup',')',$ecustomername);
      $ecustomername=str_replace('tandadan','&',$ecustomername);
      $ecustomername=str_replace('tandatitik','.',$ecustomername);
      $ecustomername=str_replace('tandakoma',',',$ecustomername);
      $ecustomername=str_replace('tandagaring','/',$ecustomername);

      $query = $this->db->query(" select a.v_sisa from tm_nota a, tr_customer b, tr_customer_groupar c
																	  where a.i_customer=b.i_customer and a.i_customer=c.i_customer
																	  and a.f_ttb_tolak='f'
																	  and a.f_nota_koreksi='f'
																	  and not a.i_nota isnull
																	  and c.i_customer_groupar='$icustomergroupar' 
                                    and a.v_sisa>0
                                    and a.f_nota_cancel='f'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->paginationxx->initialize($config);
			$this->load->model('listagingpiutang/mmaster');
  		$data['page_title'] = $this->lang->line('listagingpiutang');
			$data['icustomergroupar']	= $icustomergroupar;
			$data['ecustomername']	= $ecustomername;
			$data['djt']	= $djt;
			$data['isi']		= $this->mmaster->bacadetail($icustomergroupar,$djt,$config['per_page'],$this->uri->segment(7));
			$this->load->view('listagingpiutang/vformdetail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
