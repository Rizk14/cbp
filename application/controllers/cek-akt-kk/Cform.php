<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu191')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listkk');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('cek-akt-kk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu146')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('kk')." update";
			if(
				$this->uri->segment(4) && $this->uri->segment(5) && $this->uri->segment(6)
			  ){
				$ikk 		= $this->uri->segment(4);
				$iperiode	= $this->uri->segment(5);
				$iarea		= $this->uri->segment(6);
				$dfrom		= $this->uri->segment(7);
				$dto		= $this->uri->segment(8);
				$this->load->model("cek-akt-kk/mmaster");
				$data['isi']=$this->mmaster->baca($ikk,$iperiode,$iarea);
				$data['iarea'] = $iarea;
				$data['dfrom'] = $dfrom;
				$data['dto']   = $dto;
		 		$this->load->view('cek-akt-kk/vformupdate',$data);
			}else{
				$this->load->view('cek-akt-kk/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if(
  		(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu146')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		  )
    {
/*
			$ikk					= $this->input->post('ikk', TRUE);
			$iarea				= $this->input->post('iarea', TRUE);
			$iperiode			= $this->input->post('iperiodeth', TRUE).$this->input->post('iperiodebl', TRUE);
			$tah					= substr($this->input->post('iperiodeth', TRUE),2,2);
			$bul					= $this->input->post('iperiodebl', TRUE);
			$ikendaraan		= $this->input->post('ikendaraan', TRUE);
			$vkk					= $this->input->post('vkk', TRUE);
			$vkk					= str_replace(',','',$vkk);
			$dkk					= $this->input->post('dkk', TRUE);
			$dbukti				= $this->input->post('dbukti', TRUE);
			$etempat			= $this->input->post('etempat', TRUE);
			$epengguna		= $this->input->post('epengguna', TRUE);
			$icoa					= $this->input->post('icoa', TRUE);
			$ecoaname			= $this->input->post('ecoaname', TRUE);
			$enamatoko		= $this->input->post('enamatoko', TRUE);
			$edescription	= $this->input->post('edescription', TRUE);
			if($edescription=="") $edescription=null;
			$ejamin			= $this->input->post('ejamin', TRUE);
			if($ejamin=="") $ejamin=null;
			$ejamout		= $this->input->post('ejamout', TRUE);
			if($ejamout=="") $ejamout=null;
			$nkm			= $this->input->post('nkm', TRUE);
			$nkm			= str_replace(',','',$nkm);
			$ecek	= $this->input->post('ecek',TRUE);
			if($ecek=='')
				$ecek=null;
			$user	= $this->session->userdata('user_id');

			if($nkm=="") $nkm=null;
			if($dkk!=''){
				$tmp=explode("-",$dkk);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dkk=$th."-".$bl."-".$hr;
			}
			if($dbukti!=''){
				$tmp=explode("-",$dbukti);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbukti=$th."-".$bl."-".$hr;
			}
			$fdebet='t';
			if (
				(isset($ikk) && $ikk != '') &&
				(isset($iperiode) && $iperiode != '') && 
				(isset($iarea) && $iarea != '') &&
				(isset($vkk) && (($vkk != 0) || ($vkk != ''))) &&
				(isset($dkk) && $dkk != '') &&
				(isset($icoa) && $icoa != '')
			   )
			{
*/
      $dfrom		= $this->input->post('dfrom');
	    $dto		  = $this->input->post('dto');
	    $iarea		= $this->input->post('iarea');
	    if($dfrom=='') $dfrom=$this->uri->segment(4);
	    if($dto=='') $dto=$this->uri->segment(5);
	    if($iarea=='') $iarea	= $this->uri->segment(6);
      $user	= $this->session->userdata('user_id');

			$this->load->model('cek-akt-kk/mmaster');
			$this->db->trans_begin();
			$this->mmaster->update($iarea,$dfrom,$dto,$user);
			$nomor=$dfrom.' - '.$dto;
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();
			}else{
	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='Cek Kas Kecil from:'.$dfrom.'s/d '.$dto.' Area:'.$iarea;
#        $this->load->model('logger');
#        $this->logger->write($id, $ip_address, $now , $pesan );
	      
#	      $this->load->model('logger');
#	      $this->logger->write($id, $ip_address, $now , $pesan );
#        $dat['user_id'] 	= $id;
#		    $dat['ip_address']= $ip_address;
#		    $dat['waktu'] = $now;
#		    $dat['activity'] 	= $pesan;
#		    $this->db->insert('dgu_logs',$dat);
        $sql	= "INSERT INTO dgu_log (user_id, ip_address, waktu, activity) VALUES ('$id', '$ip_address', '$now', '$pesan')";
        $rs		= pg_query($sql);
				$data['sukses']			= true;
				$data['inomor']			= $nomor;
				$this->db->trans_commit();
#				$this->db->trans_rollback();
				$this->load->view('nomor',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu191')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cek-akt-kk/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_kk
					                        where (upper(i_kk) like '%$cari%') and f_close='f' 
					                        and i_area='$iarea' and f_kk_cancel='f' and
					                        d_kk >= to_date('$dfrom','dd-mm-yyyy') AND
					                        d_kk <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('listkk');
			$this->load->model('cek-akt-kk/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
        while($row=pg_fetch_assoc($rs)){
	        $ip_address	  = $row['ip_address'];
	        break;
        }
      }else{
        $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
        $now	  = $row['c'];
      }
      $pesan='Buka Cetak Kas Kecil Periode:'.$dfrom.' s/d '.$dto.' Area:'.$iarea;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$this->load->view('cek-akt-kk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu191')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listgj');
			$this->load->view('cek-akt-kk/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	/*
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu191')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ikk		= $this->uri->segment(4);
			$iperiode	= $this->uri->segment(5);
			$iarea		= $this->uri->segment(6);
			$dfrom	= $this->uri->segment(7);
			$dto		= $this->uri->segment(8);
			$this->load->model('cek-akt-kk/mmaster');
			$this->mmaster->delete($iperiode,$iarea,$ikk);
			$cari		= strtoupper($this->input->post('cari'));
			if($dfrom=='') $dfrom=$this->uri->segment(7);
			if($dto=='') $dto=$this->uri->segment(8);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cek-akt-kk/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_kk
								where (upper(i_kk) like '%$cari%') and f_close='f' 
								and i_area='$iarea' and f_kk_cancel='f' and
								d_kk >= to_date('$dfrom','dd-mm-yyyy') AND
								d_kk <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('listkk');
			$data['cari']		  = $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		  = $dto;
			$data['iarea']		= $iarea;
			$data['isi']		  = $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('cek-akt-kk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	*/
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu147')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$iarea		= $this->input->post('iarea');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($iarea=='') $iarea	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/cek-akt-kk/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" select * from tm_kk
					where (upper(i_kk) like '%$cari%') and f_close='f' 
					and i_area='$iarea' and f_kk_cancel='f' and
					d_kk >= to_date('$dfrom','dd-mm-yyyy') AND
					d_kk <= to_date('$dto','dd-mm-yyyy')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('listkk');
			$this->load->model('cek-akt-kk/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['iarea']		= $iarea;
			$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('cek-akt-kk/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu191')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/cek-akt-kk/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00'){
  			$query = $this->db->query("select * from tr_area",false);
      }else{
  			$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
	                        		or i_area = '$area4' or i_area = '$area5'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('cek-akt-kk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('cek-akt-kk/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu147')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/cek-akt-kk/cform/area/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
      if($area1=='00'){
  			$query = $this->db->query("select * from tr_area where (upper(e_area_name) like '%$cari%' or upper(i_area) like '%$cari%')",false);
      }else{
			$query 	= $this->db->query("select * from tr_area
					where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')
              				and (i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
					or i_area = '$area4' or i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('cek-akt-kk/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('cek-akt-kk/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function coa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu146')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/cek-akt-kk/cform/coa/index/';
			$query = $this->db->query("select * from tr_coa where i_coa like '6%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('cek-akt-kk/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->bacacoa($config['per_page'],$this->uri->segment(5));
			$this->load->view('cek-akt-kk/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caricoa()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu146')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/cek-akt-kk/cform/coa/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("select * from tr_coa
						    									where (upper(i_coa) like '6%$cari%' or (upper(e_coa_name) like '%$cari%' 
																	and upper(i_coa) like '6%'))",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('cek-akt-kk/mmaster');
			$data['page_title'] = $this->lang->line('list_coa');
			$data['isi']=$this->mmaster->caricoa($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('cek-akt-kk/vlistcoa', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function kendaraan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu146')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	=$this->uri->segment(4);
			$periode=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/cek-akt-kk/cform/kendaraan/'.$area.'/'.$periode.'/';
			$query = $this->db->query("	select * from tr_kendaraan a
							inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							where a.i_area='$area' and a.i_periode='$periode'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('cek-akt-kk/mmaster');
			$data['page_title'] = $this->lang->line('list_kendaraan');
			$data['area']	=$area;
			$data['periode']=$periode;
			$data['isi']=$this->mmaster->bacakendaraan($area,$periode,$config['per_page'],$this->uri->segment(6));
			$this->load->view('cek-akt-kk/vlistkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carikendaraan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu146')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	=$this->input->post('area', TRUE);
			$periode=$this->input->post('periode', TRUE);
			$config['base_url'] = base_url().'index.php/cek-akt-kk/cform/kendaraan/'.$area.'/'.$periode.'/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$query 	= $this->db->query("	select * from tr_kendaraan a
							inner join tr_kendaraan_jenis b on (a.i_kendaraan_jenis=b.i_kendaraan_jenis)
							inner join tr_kendaraan_bbm c on(a.i_kendaraan_bbm=c.i_kendaraan_bbm)
							where (upper(a.i_kendaraan) like '%$cari%' or upper(a.e_pengguna) like '%$cari%')
							and a.i_area='$area'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('akt-kk/mmaster');
			$data['area']=$area;
			$data['periode']=$periode;
			$data['page_title'] = $this->lang->line('list_kendaraan');
			$data['isi']=$this->mmaster->carikendaraan($area,$periode,$cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('cek-akt-kk/vlistkendaraan', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
