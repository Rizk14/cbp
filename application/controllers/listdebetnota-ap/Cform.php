<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu522')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdebetnota-ap');
			$data['dfrom']='';
			$data['dto']='';
			$data['isupplier'] = '';
			$this->load->view('listdebetnota-ap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu522')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier  = $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listdebetnota-ap/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';

			/*if ($isupplier == 'all') {
	    		$where = '';
	    	} else {
	    		$where = " AND a.i_supplier='$isupplier'";
	    	}*/
			$query = $this->db->query(" 
				select 
					a.*,
					b.e_supplier_name
				from 
					tm_dn_ap a 
					left join tr_supplier b on (a.i_supplier=b.i_supplier)
				where 
					a.i_supplier >= '$isupplier'
					and a.d_dn_ap >= to_date('$dfrom','dd-mm-yyyy') 
					and a.d_dn_ap <= to_date('$dto','dd-mm-yyyy')
					and (a.i_supplier like '%$cari%' 
					or upper(b.e_supplier_name) like '%$cari%' 
					or upper(a.i_dn_ap) like '%$cari%' 
					or upper(a.i_refference) like '%$cari%')
				order by 
					a.d_dn_ap",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdebetnota-ap');
			$this->load->model('listdebetnota-ap/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']  = $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Debet Nota A/P Supplier '.$isupplier.' Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  
			$this->load->view('listdebetnota-ap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu522')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listdebetnota-ap');
			$this->load->view('listdebetnota-ap/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu522')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$idnap		= $this->uri->segment(4);
			$ndnyear	= $this->uri->segment(5);
			$isupplier	= $this->uri->segment(6);
			$isupplier  = str_replace('%20','',$isupplier);
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			if($dfrom==''){
				$dfrom=$this->uri->segment(7);
			}
			if($dto==''){
				$dto=$this->uri->segment(8);
			}

			$cari = $this->input->post('cari', FALSE);
			$cari= strtoupper($cari);
			$this->load->model('listdebetnota-ap/mmaster');
			$this->mmaster->delete($idnap,$ndnyear,$isupplier);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Menghapus Debet Nota A/P Supplier '.$isupplier.' No:'.$idnap;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$config['base_url'] = base_url().'index.php/listdebetnota-ap/cform/view/'.$dfrom.'/'.$dto.'/'.$isupplier.'/index/';
			$query = $this->db->query(" select 
					a.*,
					b.e_supplier_name
				from 
					tm_dn_ap a 
					left join tr_supplier b on (a.i_supplier=b.i_supplier)
				where 
					a.i_supplier >= '$isupplier'
					and a.d_dn_ap >= to_date('$dfrom','dd-mm-yyyy') 
					and a.d_dn_ap <= to_date('$dto','dd-mm-yyyy')
					and (a.i_supplier like '%$cari%' 
					or upper(b.e_supplier_name) like '%$cari%' 
					or upper(a.i_dn_ap) like '%$cari%' 
					or upper(a.i_refference) like '%$cari%')
				order by 
					a.d_dn_ap",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(9);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdebetnota-ap');
			$data['cari']		= $cari;
			$data['dfrom']	    = $dfrom;
			$data['idnap']		= $idnap;	
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$cari);
			$this->load->view('listdebetnota-ap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu522')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			$isupplier  = $this->input->post('isupplier');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(6);
			$config['base_url'] = base_url().'index.php/listdebetnota-ap/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/index/';
			$query = $this->db->query(" 
				select 
					a.*,
					b.e_supplier_name
				from 
					tm_dn_ap a 
					left join tr_supplier b on (a.i_supplier=b.i_supplier)
				where 
					a.i_supplier >= '$isupplier'
					and a.d_dn_ap >= to_date('$dfrom','dd-mm-yyyy') 
					and a.d_dn_ap <= to_date('$dto','dd-mm-yyyy')
					and (a.i_supplier like '%$cari%' 
					or upper(b.e_supplier_name) like '%$cari%' 
					or upper(a.i_dn_ap) like '%$cari%' 
					or upper(a.i_refference) like '%$cari%')
				order by 
					a.d_dn_ap",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['page_title'] = $this->lang->line('listdebetnota-ap');
			$this->load->model('listdebetnota-ap/mmaster');
			$data['cari']		= $cari;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
			$data['isupplier']	= $isupplier;
			$data['isi']		= $this->mmaster->bacaperiode($isupplier,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
			$this->load->view('listdebetnota-ap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu522')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listdebetnota-ap/cform/supplier/index/';
      		#$iuser   = $this->session->userdata('user_id');
      		$query = $this->db->query("select * from tr_supplier order by i_supplier", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listdebetnota-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('listdebetnota-ap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu522')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listdebetnota-ap/cform/supplier/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      		#$iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_supplier where (upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%') order by i_supplier",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listdebetnota-ap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listdebetnota-ap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
