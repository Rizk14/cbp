<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu386')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listlebihbayarap');
			//$data['dfrom']='';
			//$data['dto']='';
        $data['isupplier']='';
			$this->load->view('listlebihbayarap/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu386')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			//$dfrom		= $this->input->post('dfrom');
			//$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			//if($dfrom=='') $dfrom=$this->uri->segment(4);
			//if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(4);
			//$config['base_url'] = base_url().'index.php/listlebihbayarap/cform/view/'.$dfrom.'/'.$dto.'/'.$iarea.'/';
			$config['base_url'] = base_url().'index.php/listlebihbayarap/cform/view/'.$isupplier.'/';
			$query = $this->db->query(" select a.i_pelunasanap
                                  from tr_supplier b, tm_pelunasanap_lebih a left join tr_jenis_bayar c on(a.i_jenis_bayar=c.i_jenis_bayar)
                                  where a.i_supplier=b.i_supplier and a.f_pelunasanap_cancel='f' and a.v_lebih>0
                                  and (upper(a.i_pelunasanap) like '%$cari%'
                                  or upper(a.i_supplier) like '%$cari%' 
                                  or upper(b.e_supplier_name) like '%$cari%')
                                  and a.i_supplier='$isupplier' order by a.i_pelunasanap",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('listlebihbayarap/mmaster');
			$data['page_title'] = $this->lang->line('listlebihbayarap');
			$data['cari']		= $cari;
			$data['keyword']	= '';	
			//$data['dfrom']		= $dfrom;
			//$data['dto']		= $dto;
			$data['isupplier']		= $isupplier;
			//$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);
      $data['isi']		= $this->mmaster->bacaperiode($isupplier,$config['per_page'],$this->uri->segment(5),$cari);

			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Lebih Bayar Hutang Dagang Supplier '.$isupplier;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listlebihbayarap/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu386')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('listlebihbayarap');
			$this->load->view('listlebihbayarap/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu386')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			//$dfrom		= $this->input->post('dfrom');
			//$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			//if($dfrom=='') $dfrom=$this->uri->segment(4);
			//if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $iarea	= $this->uri->segment(4);
			if($cari=='') $cari	= $this->uri->segment(5);
			//$config['base_url'] = base_url().'index.php/listlebihbayarap/cform/cari/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$cari.'/';
      $config['base_url'] = base_url().'index.php/listlebihbayarap/cform/cari/'.$isupplier.'/'.$cari.'/';
			$query = $this->db->query(" select a.i_pelunasanap
                                  from tr_supplier b, tm_pelunasanap_lebih a left join tr_jenis_bayar c on(a.i_jenis_bayar=c.i_jenis_bayar)
                                  where a.i_supplier=b.i_supplier and a.f_pelunasanap_cancel='f' and a.v_lebih>0
                                  and (upper(a.i_pelunasanap) like '%$cari%'
                                  or upper(a.i_supplier) like '%$cari%' 
                                  or upper(b.e_supplier_name) like '%$cari%')
                                  and a.i_supplier='$isupplier' order by a.i_pelunasanap",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);

			$this->load->model('listlebihbayarap/mmaster');
			$data['page_title'] = $this->lang->line('listlebihbayarap');
			$data['keyword']	= $cari;
			$data['cari']		= '';
			//$data['dfrom']		= $dfrom;
			//$data['dto']		= $dto;
			$data['isupplier']		= $isupplier;
			//$data['isi']		= $this->mmaster->bacaperiode($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(8),$cari);
      $data['isi']		= $this->mmaster->bacaperiode($isupplier,$config['per_page'],$this->uri->segment(6),$cari);
			$this->load->view('listlebihbayarap/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariperpages()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu386')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			//$dfrom		= $this->input->post('dfrom');
			//$dto		= $this->input->post('dto');
			$isupplier		= $this->input->post('isupplier');
			//if($dfrom=='') $dfrom=$this->uri->segment(4);
			//if($dto=='') $dto=$this->uri->segment(5);
			if($isupplier=='') $isupplier	= $this->uri->segment(4);
			$keyword = strtoupper($this->uri->segment(5));	
			//$config['base_url'] = base_url().'index.php/listlebihbayarap/cform/cariperpages/'.$dfrom.'/'.$dto.'/'.$iarea.'/'.$keyword.'/index/';
      $config['base_url'] = base_url().'index.php/listlebihbayarap/cform/cariperpages/'.$isupplier.'/'.$keyword.'/index/';
			$query = $this->db->query(" select a.i_pelunasanap
                                  from tr_supplier b, tm_pelunasanap_lebih a left join tr_jenis_bayar c on(a.i_jenis_bayar=c.i_jenis_bayar)
                                  where a.i_supplier=b.i_supplier and a.f_pelunasanap_cancel='f' and a.v_lebih>0
                                  and (upper(a.i_pelunasanap) like '%$keyword%'
                                  or upper(a.i_supplier) like '%$keyword%' 
                                  or upper(b.e_supplier_name) like '%$keyword%')
                                  and a.i_supplier='$isupplier' order by a.i_pelunasanap",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('listlebihbayarap/mmaster');
			$data['page_title'] = $this->lang->line('listlebihbayarap');
			$data['cari']		= '';
			$data['keyword']	= $keyword;
			//$data['dfrom']		= $dfrom;
			//$data['dto']		= $dto;
			$data['isupplier']		= $isupplier;
			//$data['isi']		= $this->mmaster->bacaperiodeperpages($iarea,$dfrom,$dto,$config['per_page'],$this->uri->segment(9),$keyword);
      $data['isi']		= $this->mmaster->bacaperiodeperpages($isupplier,$config['per_page'],$this->uri->segment(7),$keyword);
			$this->load->view('listlebihbayarap/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu386')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listlebihbayarap/cform/supplier/index/';
			$query = $this->db->query("select * from tr_supplier",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page']   = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listlebihbayarap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('listlebihbayarap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu386')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/listlebihbayarap/cform/supplier/index/';
			$cari 	= strtoupper($this->input->post('cari', FALSE));
  		$query = $this->db->query("select * from tr_supplier where (upper(e_supplier_name) like '%$cari%' or upper(i_supplier) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listlebihbayarap/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('listlebihbayarap/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
