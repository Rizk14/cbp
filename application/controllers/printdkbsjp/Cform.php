<?php
class Cform extends CI_Controller
{
	public $title 	= "Cetak DKB-SJP";
	public $folder 	= "printdkbsjp";

	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		$this->load->model($this->folder . '/mmaster');
	}

	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu164') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {

			$data = [
				'page_title' 	=> $this->title,
				'folder' 		=> $this->folder,
				'dfrom' 		=> '',
				'iarea' 		=> '',
				'dto' 			=> '',
			];

			$this->logger->writenew("Membuka Menu " . $this->title);

			$this->load->view($this->folder . '/vform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu164') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/area/index/';
			$iuser   = $this->session->userdata('user_id');

			$query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('list_area');
			$data['isi'] 		= $this->mmaster->bacaarea($config['per_page'], $this->uri->segment(5), $iuser);

			$this->load->view($this->folder . '/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu164') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$iuser   = $this->session->userdata('user_id');

			$query = $this->db->query("	select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
										and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )", false);
			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(5);
			$this->pagination->initialize($config);

			$data['page_title'] 	= $this->lang->line('list_area');
			$data['isi'] 			= $this->mmaster->cariarea($cari, $config['per_page'], $this->uri->segment(5), $iuser);

			$this->load->view($this->folder . '/vlistarea', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu164') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari 	= strtoupper($this->input->post('cari', FALSE));
			$iarea  = $this->input->post('iarea');
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');

			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea	= $this->uri->segment(6);

			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/view/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';

			$query = $this->db->query(" SELECT a.*, b.e_area_name
										from tm_dkb_sjp a, tr_area b
										where a.i_area=b.i_area 
										and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										or upper(a.i_dkb) like '%$cari%')
										and substr(a.i_dkb,11,2)='$iarea' and 
										a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_dkb <= to_date('$dto','dd-mm-yyyy')
										order by a.i_dkb desc", false);

			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data = [
				'page_title' => $this->title,
				'cari'		 => $cari,
				'dfrom'		 => $dfrom,
				'dto'		 => $dto,
				'iarea'		 => $iarea,
				'folder'	 => $this->folder,
				'isi'		 => $this->mmaster->bacasemua($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari)
			];

			$this->load->view($this->folder . '/vformview', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu164') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$idkb 	= $this->uri->segment(4);
			$iarea	= $this->uri->segment(5);
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea	= $this->input->post('iarea');

			if ($iarea == '') $iarea = $this->uri->segment(5);
			if ($dfrom == '') $dfrom = $this->uri->segment(6);
			if ($dto == '') $dto = $this->uri->segment(7);

			$data = [
				'idkb'			=> $idkb,
				'iarea'			=> $iarea,
				'dfrom'			=> $dfrom,
				'dto' 			=> $dto,
				'page_title'	=> $this->title,
				'folder'		=> $this->folder,
				'isi'			=> $this->mmaster->baca($idkb, $iarea),
				'detail'		=> $this->mmaster->bacadetail($idkb, $iarea),
			];

			$this->logger->writenew('Cetak DKB Area ' . $iarea . ' No:' . $idkb);

			$this->load->view($this->folder . '/vformrpt', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu164') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('printdkbkhusus');
			$this->load->view($this->folder . '/vinsert_fail', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu164') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$cari	= strtoupper($this->input->post('cari'));
			$dfrom	= $this->input->post('dfrom');
			$dto	= $this->input->post('dto');
			$iarea  = $this->session->userdata('i_area');

			if ($dfrom == '') $dfrom = $this->uri->segment(4);
			if ($dto == '') $dto = $this->uri->segment(5);
			if ($iarea == '') $iarea	= $this->uri->segment(6);

			$config['base_url'] = base_url() . 'index.php/' . $this->folder . '/cform/view/' . $dfrom . '/' . $dto . '/' . $iarea . '/index/';

			$query = $this->db->query(" SELECT a.*, b.e_area_name
										from tm_dkb_sjp a, tr_area b
										where a.i_area=b.i_area 
										and (upper(a.i_area) like '%$cari%' or upper(b.e_area_name) like '%$cari%'
										or upper(a.i_dkb) like '%$cari%')
										and substr(a.i_dkb,11,2)='$iarea' and 
										a.d_dkb >= to_date('$dfrom','dd-mm-yyyy') AND
										a.d_dkb <= to_date('$dto','dd-mm-yyyy')
										order by a.i_dkb desc", false);

			$config['total_rows'] 	= $query->num_rows();
			$config['per_page'] 	= '10';
			$config['first_link'] 	= 'Awal';
			$config['last_link'] 	= 'Akhir';
			$config['next_link'] 	= 'Selanjutnya';
			$config['prev_link'] 	= 'Sebelumnya';
			$config['cur_page'] 	= $this->uri->segment(8);
			$this->pagination->initialize($config);

			$data = [
				'page_title'	=> $this->title,
				'folder'		=> $this->folder,
				'cari' 			=> $cari,
				'dfrom' 		=> $dfrom,
				'dto' 			=> $dto,
				'iarea' 		=> $iarea,
				'isi' 			=> $this->mmaster->bacaperiode($iarea, $dfrom, $dto, $config['per_page'], $this->uri->segment(8), $cari),
			];

			$this->load->view($this->folder . '/vformview', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}

	function update()
	{
		$idkb  = $this->input->post('idkb');

		$this->db->trans_begin();

		$this->mmaster->updatedkb($idkb);

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			echo 'fail';
		} else {
			$this->db->trans_commit();
			$this->logger->writenew($this->title . ' No DKB : ' . $idkb);
			echo $idkb;
		}
		redirect($this->folder . '/cform');
	}

	public function undo()
	{
		$idkb 		= $this->uri->segment(4);
		$iarea 		= $this->uri->segment(5);
		$dfrom 		= $this->uri->segment(6);
		$dto 		= $this->uri->segment(7);
		$iareax		= $this->uri->segment(8);

		$this->db->trans_begin();

		$this->mmaster->reprint($idkb, $iarea, $dfrom, $dto, $iareax, $this->folder);

		if ($this->db->trans_status() === FALSE || $this->db->trans_status() === false) {
			$this->db->trans_rollback();
		} else {
			$this->db->trans_commit();
			$this->logger->writenew('Buka Ulang Cetak DKB No. DKB : ' . $idkb);
		}
	}
}
