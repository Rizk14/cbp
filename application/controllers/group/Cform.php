<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu9')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('master_group');
			$data['igroup']='';
			$this->load->model('group/mmaster');
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('group/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu9')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$igroup 	= $this->input->post('igroup', TRUE);
			$egroupname = $this->input->post('egroupname', TRUE);

			if ($igroup != '' && $egroupname != '')
			{
				$this->load->model('group/mmaster');
				$this->mmaster->insert($igroup,$egroupname);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu9')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('master_group');
			$this->load->view('group/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu9')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('master_group')." update";
			if($this->uri->segment(4)){
				$igroup = $this->uri->segment(4);
				$data['igroup'] = $igroup;
				$this->load->model('group/mmaster');
				$data['isi']=$this->mmaster->baca($igroup);
		 		$this->load->view('group/vmainform',$data);
			}else{
				$this->load->view('group/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu9')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$igroup		= $this->input->post('igroup', TRUE);
			$egroupname 	= $this->input->post('egroupname', TRUE);
			$this->load->model('group/mmaster');
			$this->mmaster->update($igroup,$egroupname);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu9')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$igroup		= $this->uri->segment(4);
			$this->load->model('group/mmaster');
			$this->mmaster->delete($igroup);
			$data['page_title'] = $this->lang->line('master_group');
			$data['igroup']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('group/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
