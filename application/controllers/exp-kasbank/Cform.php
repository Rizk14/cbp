<?php
class Cform extends CI_Controller
{
    public $title = "Export Buku Kas dan Bank";

    public function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');
        $this->load->helper('file');
        require_once "php/fungsi.php";
        $this->load->model('exp-kasbank/mmaster');
    }
    public function index()
    {
        $data['page_title'] = $this->title;
        $data['dfrom']      = '';
        $data['dto']        = '';

        $this->load->view('exp-kasbank/vform', $data);
    }

    function export()
    {
        $dfrom  = $this->uri->segment(4);
        $dto    = $this->uri->segment(5);

        $tmp = explode("-", date('Y-m-d', strtotime($dfrom)));
        $det = $tmp[2];
        $mon = $tmp[1];
        $yir = $tmp[0];
        $dsaldo = $yir . "/" . $mon . "/" . $det;
        $periode = $yir . $mon;

        $dtos = $this->mmaster->dateAdd("d", -1, $dsaldo);

        $tmp  = explode("-", $dtos);
        $det1 = $tmp[2];
        $mon1 = $tmp[1];
        $yir1 = $tmp[0];
        $dtos = $yir1 . "-" . $mon1 . "-" . $det1;

        $this->load->library('PHPExcel');
        $this->load->library('PHPExcel/IOFactory');

        $ObjPHPExcel = new PHPExcel();

        $query  = $this->mmaster->bacaperiode($dfrom, $dto); /* BACA BANK */
        $query2 = $this->mmaster->bacakb($dfrom, $dto); /* BACA KB */
        $query3 = $this->mmaster->bacakk($periode, $dfrom, $dto); /* BACA KK */

        if ($query) {
            $ObjPHPExcel->getActiveSheet()->getProtection()->setSheet(false);
            $ObjPHPExcel->getProperties()->setCreator("M.I.S Dept");
            $ObjPHPExcel->getProperties()->setLastModifiedBy("M.I.S Dept");

            $ObjPHPExcel->getProperties()
                ->setTitle("Buku Kas dan Bank")
                ->setSubject("Buku Kas dan Bank")
                ->setDescription("Buku Kas dan Bank")
                ->setKeywords("kasbank")
                ->setCategory("Laporan");

            $ObjPHPExcel->setActiveSheetIndex(0);

            $ObjPHPExcel->getActiveSheet()->duplicateStyleArray(
                array(
                    'font' => array(
                        'name'    => 'Times New Roman',
                        'bold'  => true,
                        'italic' => false,
                        'size'  => 12
                    ),
                    'alignment' => array(
                        'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
                        'vertical'  => Style_Alignment::VERTICAL_CENTER,
                        'wrap'      => true
                    )
                ),
                'A2:A4'
            );

            $style_col = array(
                'font' => array(
                    'name'    => 'Times New Roman',
                    'bold'  => true,
                    'italic' => false,
                    'size'  => 10
                ),
                'borders' => array(
                    'top'     => array('style' => Style_Border::BORDER_THIN),
                    'bottom' => array('style' => Style_Border::BORDER_THIN),
                    'left'  => array('style' => Style_Border::BORDER_THIN),
                    'right' => array('style' => Style_Border::BORDER_THIN)
                ),
                'alignment' => array(
                    'horizontal' => Style_Alignment::HORIZONTAL_CENTER,
                    'vertical'  => Style_Alignment::VERTICAL_CENTER
                )
            );

            $style_col1 = array(
                'font' => array(
                    'name'    => 'Times New Roman',
                    'bold'  => true,
                    'italic' => false,
                    'size'  => 10
                ),
                'borders' => array(
                    'top'     => array('style' => Style_Border::BORDER_THIN),
                    'bottom' => array('style' => Style_Border::BORDER_THIN),
                    'left'  => array('style' => Style_Border::BORDER_THIN),
                    'right' => array('style' => Style_Border::BORDER_THIN)
                ),
                'alignment' => array(
                    'vertical'  => Style_Alignment::VERTICAL_CENTER
                )
            );

            $style_row1 = array(
                'borders' => array(
                    'top'     => array('style' => Style_Border::BORDER_THIN),
                    'bottom' => array('style' => Style_Border::BORDER_THIN),
                    'left'  => array('style' => Style_Border::BORDER_THIN),
                    'right' => array('style' => Style_Border::BORDER_THIN)

                ),
                'font' => array(
                    'name'    => 'Times New Roman',
                    'bold'  => false,
                    'italic' => false,
                    'size'  => 10
                ),
            );

            $style_row = array(
                'font' => array(
                    'name'    => 'Times New Roman',
                    'bold'  => false,
                    'italic' => false,
                    'size'  => 9
                ),
                'borders' => array(
                    'top'     => array('style' => Style_Border::BORDER_THIN),
                    'bottom' => array('style' => Style_Border::BORDER_THIN),
                    'left'  => array('style' => Style_Border::BORDER_THIN),
                    'right' => array('style' => Style_Border::BORDER_THIN)
                ),
                'alignment' => array(
                    // 'horizontal'=> Style_Alignment::HORIZONTAL_CENTER,
                    'vertical'  => Style_Alignment::VERTICAL_CENTER
                )
            );

            $ObjPHPExcel->getActiveSheet()->mergeCells('A2:J2');
            $ObjPHPExcel->getActiveSheet()->setCellValue('A2', 'BUKU KAS DAN BANK');
            $ObjPHPExcel->getActiveSheet()->mergeCells('A3:J3');
            $ObjPHPExcel->getActiveSheet()->setCellValue('A3', NmPerusahaan);
            $ObjPHPExcel->getActiveSheet()->mergeCells('A4:J4');
            $ObjPHPExcel->getActiveSheet()->setCellValue('A4', "Periode : " . $dfrom . " s/d " . $dto);

            $ObjPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12); //POST
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20); //AREA
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15); //NO BUKTI
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10); //TANGGAL
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30); //KETERANGAN
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10); //NO. PERK
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20); //NAMA PERK
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15); //DEBET
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15); //KREDIT
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20); //SALDO

            $ObjPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(35); //POST
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20); //SALDO AWAL 
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(15); //DEBIT
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15); //KREDIT
            $ObjPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20); //SALDO AKHIR

            $ObjPHPExcel->getActiveSheet()->setCellValue('A6', 'POST');
            // $ObjPHPExcel->getActiveSheet()->getStyle('A6')->applyFromArray($style_col);
            $ObjPHPExcel->getActiveSheet()->setCellValue('B6', 'AREA');
            // $ObjPHPExcel->getActiveSheet()->getStyle('B6')->applyFromArray($style_col);
            $ObjPHPExcel->getActiveSheet()->setCellValue('C6', 'NO BUKTI');
            // $ObjPHPExcel->getActiveSheet()->getStyle('C6')->applyFromArray($style_col);
            $ObjPHPExcel->getActiveSheet()->setCellValue('D6', 'TANGGAL');
            // $ObjPHPExcel->getActiveSheet()->getStyle('D6')->applyFromArray($style_col);
            $ObjPHPExcel->getActiveSheet()->setCellValue('E6', 'KETERANGAN');
            // $ObjPHPExcel->getActiveSheet()->getStyle('E6')->applyFromArray($style_col);
            $ObjPHPExcel->getActiveSheet()->setCellValue('F6', 'NO. PERK');
            // $ObjPHPExcel->getActiveSheet()->getStyle('F6')->applyFromArray($style_col);
            $ObjPHPExcel->getActiveSheet()->setCellValue('G6', 'NAMA PERK');
            // $ObjPHPExcel->getActiveSheet()->getStyle('G6')->applyFromArray($style_col);
            $ObjPHPExcel->getActiveSheet()->setCellValue('H6', 'DEBET');
            // $ObjPHPExcel->getActiveSheet()->getStyle('H6')->applyFromArray($style_col);
            $ObjPHPExcel->getActiveSheet()->setCellValue('I6', 'KREDIT');
            // $ObjPHPExcel->getActiveSheet()->getStyle('I6')->applyFromArray($style_col);
            $ObjPHPExcel->getActiveSheet()->setCellValue('J6', 'SALDO');
            // $ObjPHPExcel->getActiveSheet()->getStyle('J6')->applyFromArray($style_col);

            /* REKAP */
            $ObjPHPExcel->getActiveSheet()->setCellValue('L6', 'POST');
            $ObjPHPExcel->getActiveSheet()->setCellValue('M6', 'SALDO AWAL');
            $ObjPHPExcel->getActiveSheet()->setCellValue('N6', 'DEBIT');
            $ObjPHPExcel->getActiveSheet()->setCellValue('O6', 'KREDIT');
            $ObjPHPExcel->getActiveSheet()->setCellValue('P6', 'SALDO AKHIR');
            /*  */

            $ObjPHPExcel->getActiveSheet()->getStyle('A6:J6')->applyFromArray($style_col);
            $ObjPHPExcel->getActiveSheet()->getStyle('L6:P6')->applyFromArray($style_col);

            $ObjPHPExcel->getActiveSheet()->getStyle('A6:J6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DFF1D0');
            $ObjPHPExcel->getActiveSheet()->getStyle('L6:P6')->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DFF1D0');

            /* *********************************** */

            $i          = 1;
            $no         = 7;
            $j          = 7;
            $namabank   = '';
            // $vtsawal    = 0;
            // $vtdebet    = 0;
            // $vtkredit   = 0;
            // $vtsahir    = 0;

            /* *************** PROSES BANK ****************** */

            /* DISINI BACA BANK MASUK & BANK KELUAR (ALL) */
            foreach ($query as $row) {
                $saldoawal = $this->mmaster->bacasaldo($dtos, $row->i_coa_bank);

                if ($namabank == '' || $namabank != $row->e_bank_name) {

                    $isi = $this->mmaster->baca_dkbank($dfrom, $dto, $row->e_bank_name);

                    $sahir = ($saldoawal + $isi->v_debet) - $isi->v_kredit;

                    // $ObjPHPExcel->getActiveSheet()->setCellValue('A' . $j, $row->e_bank_name);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($style_col);

                    // $ObjPHPExcel->getActiveSheet()->setCellValue('J' . $j, $saldoawal);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('J' . $j)->applyFromArray($style_row);

                    $ObjPHPExcel->getActiveSheet()->setCellValue('L' . $no, $row->e_bank_name);
                    $ObjPHPExcel->getActiveSheet()->getStyle('L' . $no)->applyFromArray($style_row);

                    $ObjPHPExcel->getActiveSheet()->setCellValue('M' . $no, $saldoawal);
                    $ObjPHPExcel->getActiveSheet()->getStyle('M' . $no)->applyFromArray($style_row);

                    $ObjPHPExcel->getActiveSheet()->setCellValue('N' . $no, $isi->v_debet);
                    $ObjPHPExcel->getActiveSheet()->getStyle('N' . $no)->applyFromArray($style_row);

                    $ObjPHPExcel->getActiveSheet()->setCellValue('O' . $no, $isi->v_kredit);
                    $ObjPHPExcel->getActiveSheet()->getStyle('O' . $no)->applyFromArray($style_row);

                    $ObjPHPExcel->getActiveSheet()->setCellValue('P' . $no, $sahir);
                    $ObjPHPExcel->getActiveSheet()->getStyle('P' . $no)->applyFromArray($style_row);

                    // $ObjPHPExcel->getActiveSheet()->mergeCells('A' . $j . ":I" . $j);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('A' . $j . ":J" . $j)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DFF1D0');

                    $i = 1;
                    // $j++;
                    $no++;
                }
                $namabank = $row->e_bank_name;

                if ($i == 1) {
                    $saldo = $saldoawal;
                }

                $saldo = ($saldo + $row->v_debet) - $row->v_kredit;

                $ObjPHPExcel->getActiveSheet()->setCellValue('A' . $j, $row->e_bank_name);
                // $ObjPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($style_row);
                $ObjPHPExcel->getActiveSheet()->setCellValue('B' . $j, $row->i_area . " - " . $row->e_area_name);
                // $ObjPHPExcel->getActiveSheet()->getStyle('B' . $j)->applyFromArray($style_row);
                $ObjPHPExcel->getActiveSheet()->setCellValue('C' . $j, $row->i_reff);
                // $ObjPHPExcel->getActiveSheet()->getStyle('C' . $j)->applyFromArray($style_row);
                $ObjPHPExcel->getActiveSheet()->setCellValue('D' . $j, date('d-m-Y', strtotime($row->d_bank)));
                // $ObjPHPExcel->getActiveSheet()->getStyle('D' . $j)->applyFromArray($style_row);
                $ObjPHPExcel->getActiveSheet()->setCellValue('E' . $j, $row->e_description);
                // $ObjPHPExcel->getActiveSheet()->getStyle('E' . $j)->applyFromArray($style_row);
                $ObjPHPExcel->getActiveSheet()->setCellValue('F' . $j, $row->i_coa);
                // $ObjPHPExcel->getActiveSheet()->getStyle('F' . $j)->applyFromArray($style_row);
                $ObjPHPExcel->getActiveSheet()->setCellValue('G' . $j, $row->e_coa_name);
                // $ObjPHPExcel->getActiveSheet()->getStyle('G' . $j)->applyFromArray($style_row);
                $ObjPHPExcel->getActiveSheet()->setCellValue('H' . $j, $row->v_debet);
                // $ObjPHPExcel->getActiveSheet()->getStyle('H' . $j)->applyFromArray($style_row);
                $ObjPHPExcel->getActiveSheet()->setCellValue('I' . $j, $row->v_kredit);
                // $ObjPHPExcel->getActiveSheet()->getStyle('I' . $j)->applyFromArray($style_row);
                $ObjPHPExcel->getActiveSheet()->setCellValue('J' . $j, $saldo);
                // $ObjPHPExcel->getActiveSheet()->getStyle('J' . $j)->applyFromArray($style_row);

                $i++;
                $j++;
            }



            /* ************************************ */

            /* *************** PROSES KAS BESAR ****************** */

            /* BACA SALDO KAS BESAR */
            $saldoawal  = $this->mmaster->bacasaldokas($periode, $dfrom, $dto);

            /* DISINI BACA KAS BESAR MASUK & KAS BESAR KELUAR */
            if ($query2) {
                foreach ($query2 as $row) {
                    if ($namabank == '' || $namabank != $row->e_bank_name) {

                        $isi = $this->mmaster->baca_dkkb($dfrom, $dto);

                        $sahir = ($saldoawal + $isi->v_debet) - $isi->v_kredit;

                        // $ObjPHPExcel->getActiveSheet()->setCellValue('A' . $j, $row->e_bank_name);
                        // $ObjPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($style_col);

                        // $ObjPHPExcel->getActiveSheet()->setCellValue('J' . $j, $saldoawal);
                        // $ObjPHPExcel->getActiveSheet()->getStyle('J' . $j)->applyFromArray($style_row);

                        $ObjPHPExcel->getActiveSheet()->setCellValue('L' . $no, $row->e_bank_name);
                        $ObjPHPExcel->getActiveSheet()->getStyle('L' . $no)->applyFromArray($style_row);

                        $ObjPHPExcel->getActiveSheet()->setCellValue('M' . $no, $saldoawal);
                        $ObjPHPExcel->getActiveSheet()->getStyle('M' . $no)->applyFromArray($style_row);

                        $ObjPHPExcel->getActiveSheet()->setCellValue('N' . $no, $isi->v_debet);
                        $ObjPHPExcel->getActiveSheet()->getStyle('N' . $no)->applyFromArray($style_row);

                        $ObjPHPExcel->getActiveSheet()->setCellValue('O' . $no, $isi->v_kredit);
                        $ObjPHPExcel->getActiveSheet()->getStyle('O' . $no)->applyFromArray($style_row);

                        $ObjPHPExcel->getActiveSheet()->setCellValue('P' . $no, $sahir);
                        $ObjPHPExcel->getActiveSheet()->getStyle('P' . $no)->applyFromArray($style_row);

                        // $ObjPHPExcel->getActiveSheet()->mergeCells('A' . $j . ":I" . $j);

                        // $ObjPHPExcel->getActiveSheet()->getStyle('A' . $j . ":J" . $j)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DFF1D0');

                        $i = 1;
                        // $j++;
                        $no++;
                    }
                    $namabank = $row->e_bank_name;

                    if ($i == 1) {
                        $saldo = $saldoawal;
                    }

                    $saldo = ($saldo + $row->v_debet) - $row->v_kredit;

                    $ObjPHPExcel->getActiveSheet()->setCellValue('A' . $j, $row->e_bank_name);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('B' . $j, $row->i_area . " - " . $row->e_area_name);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('B' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('C' . $j, $row->i_reff);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('C' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('D' . $j, date('d-m-Y', strtotime($row->d_bank)));
                    // $ObjPHPExcel->getActiveSheet()->getStyle('D' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('E' . $j, $row->e_description);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('E' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('F' . $j, $row->i_coa);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('F' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('G' . $j, $row->e_coa_name);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('G' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('H' . $j, $row->v_debet);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('H' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('I' . $j, $row->v_kredit);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('I' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('J' . $j, $saldo);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('J' . $j)->applyFromArray($style_row);

                    $i++;
                    $j++;
                }
            }
            /* ************************************ */

            /* *************** PROSES KAS KECIL ****************** */
            /* DISINI BACA KAS KECIL MASUK & KAS KECIL KELUAR */
            if ($query3) {
                foreach ($query3 as $row) {
                    $saldoawal = $this->mmaster->bacasaldokk($row->i_area, $periode, $dfrom);

                    if ($namabank == '' || $namabank != $row->e_bank_name) {

                        $isi = $this->mmaster->baca_dkkk($periode, $dfrom, $dto, $row->e_bank_name);

                        $sahir = ($saldoawal + $isi->v_debet) - $isi->v_kredit;

                        // $ObjPHPExcel->getActiveSheet()->setCellValue('A' . $j, $row->e_bank_name);
                        // $ObjPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($style_col);

                        // $ObjPHPExcel->getActiveSheet()->setCellValue('J' . $j, $saldoawal);
                        // $ObjPHPExcel->getActiveSheet()->getStyle('J' . $j)->applyFromArray($style_row);

                        $ObjPHPExcel->getActiveSheet()->setCellValue('L' . $no, $row->e_bank_name);
                        $ObjPHPExcel->getActiveSheet()->getStyle('L' . $no)->applyFromArray($style_row);

                        $ObjPHPExcel->getActiveSheet()->setCellValue('M' . $no, $saldoawal);
                        $ObjPHPExcel->getActiveSheet()->getStyle('M' . $no)->applyFromArray($style_row);

                        $ObjPHPExcel->getActiveSheet()->setCellValue('N' . $no, $isi->v_debet);
                        $ObjPHPExcel->getActiveSheet()->getStyle('N' . $no)->applyFromArray($style_row);

                        $ObjPHPExcel->getActiveSheet()->setCellValue('O' . $no, $isi->v_kredit);
                        $ObjPHPExcel->getActiveSheet()->getStyle('O' . $no)->applyFromArray($style_row);

                        $ObjPHPExcel->getActiveSheet()->setCellValue('P' . $no, $sahir);
                        $ObjPHPExcel->getActiveSheet()->getStyle('P' . $no)->applyFromArray($style_row);

                        // $ObjPHPExcel->getActiveSheet()->mergeCells('A' . $j . ":I" . $j);

                        // $ObjPHPExcel->getActiveSheet()->getStyle('A' . $j . ":J" . $j)->getFill()->setFillType(Style_Fill::FILL_SOLID)->getStartColor()->setRGB('DFF1D0');

                        $i = 1;
                        // $j++;
                        $no++;
                    }
                    $namabank = $row->e_bank_name;

                    if ($i == 1) {
                        $saldo = $saldoawal;
                    }

                    $saldo = ($saldo + $row->v_debet) - $row->v_kredit;

                    $ObjPHPExcel->getActiveSheet()->setCellValue('A' . $j, $row->e_bank_name);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('A' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('B' . $j, $row->i_area . " - " . $row->e_area_name);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('B' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('C' . $j, $row->i_kk);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('C' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('D' . $j, date('d-m-Y', strtotime($row->d_kk)));
                    // $ObjPHPExcel->getActiveSheet()->getStyle('D' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('E' . $j, $row->e_description);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('E' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('F' . $j, $row->i_coa);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('F' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('G' . $j, $row->e_coa_name);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('G' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('H' . $j, $row->v_debet);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('H' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('I' . $j, $row->v_kredit);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('I' . $j)->applyFromArray($style_row);
                    $ObjPHPExcel->getActiveSheet()->setCellValue('J' . $j, $saldo);
                    // $ObjPHPExcel->getActiveSheet()->getStyle('J' . $j)->applyFromArray($style_row);

                    $i++;
                    $j++;
                }
            }
            /* ************************************ */

            $ObjPHPExcel->getActiveSheet()->getStyle('A7:J' . $j)->applyFromArray($style_row);
            $ObjPHPExcel->getActiveSheet()->getStyle('H7:J' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
            $ObjPHPExcel->getActiveSheet()->getStyle('H7:J' . $j)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
            $ObjPHPExcel->getActiveSheet()->getStyle('L7:P' . $no)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);

            $nama_file = "Buku_Kas_Bank_" . $dfrom . "_" . $dto . ".xls";
            // Proses file excel    
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment; filename=' . $nama_file . ''); // Set nama file excel nya    
            header('Cache-Control: max-age=0');

            $ObjWriter = IOFactory::createWriter($ObjPHPExcel, 'Excel5');
            $ObjWriter->save('php://output', 'w');
        } else {
            echo "<script language='javascript' type='text/javascript'>alert ('Belum Ada Data !!!');self.close();</script>";
        }
    }
}
