<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu83')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('printnotakoreksi');
			$data['notakoreksifrom']='';
			$data['notakoreksito']	='';
			$this->load->view('printnotakoreksikelompok/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function notakoreksifrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu83')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	= strtoupper($this->input->post('cari'));
			$config['base_url'] = base_url().'index.php/printnotakoreksikelompok/cform/notakoreksifrom/index/';
			$query = $this->db->query("	select i_nota from tm_notakoreksi
              										where upper(i_nota) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printnotakoreksikelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_notakoreksi');
			$data['isi']=$this->mmaster->bacanotakoreksi($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printnotakoreksikelompok/vlistnotakoreksifrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carinotakoreksifrom()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu83')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	  = strtoupper($this->input->post('cari'));
			$config['base_url'] = base_url().'index.php/printnotakoreksikelompok/cform/notakoreksifrom/index/';
			$query = $this->db->query("	select i_nota from tm_notakoreksi
										where upper(i_nota) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printnotakoreksikelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_notakoreksi');
			$data['isi']=$this->mmaster->bacanotakoreksi($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('printnotakoreksikelompok/vlistnotakoreksifrom', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function notakoreksito()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu83')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	  = strtoupper($this->input->post('cari'));
      $area   = $this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/printnotakoreksikelompok/cform/notakoreksito/'.$area.'/';
			$query = $this->db->query("	select i_nota from tm_notakoreksi
              										where upper(i_nota) like '%$cari%' and i_area='$area'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printnotakoreksikelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_notakoreksi');
      $data['area']=$area;
			$data['isi']=$this->mmaster->bacanotakoreksito($cari,$config['per_page'],$this->uri->segment(5),$area);
			$this->load->view('printnotakoreksikelompok/vlistnotakoreksito', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carinotakoreksito()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu83')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$cari	  = strtoupper($this->input->post('cari'));
			$area	  = $this->input->post('iarea');
			$config['base_url'] = base_url().'index.php/printnotakoreksikelompok/cform/notakoreksito/'.$area.'/';
			$query = $this->db->query("	select i_nota from tm_notakoreksi
              										where upper(i_nota) like '%$cari%' and i_area='$area'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('printnotakoreksikelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_notakoreksi');
      $data['area']=$area;
			$data['isi']=$this->mmaster->bacanotakoreksito($cari,$config['per_page'],$this->uri->segment(5),$area);
			$this->load->view('printnotakoreksikelompok/vlistnotakoreksito', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cetak()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu83')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area	= $this->input->post('areafrom');
			$notakoreksifrom= $this->input->post('notakoreksifrom');
			$notakoreksito	= $this->input->post('notakoreksito');
			$this->load->model('printnotakoreksikelompok/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['master']=$this->mmaster->bacamaster($notakoreksifrom,$notakoreksito);
			$data['area']=$area;
			$sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
			$data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Cetak Nota Koreksi Area:'.$area.' No:'.$notakoreksifrom.' s/d No:'.$notakoreksito;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );
			$this->load->view('printnotakoreksikelompok/vformrpt',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
