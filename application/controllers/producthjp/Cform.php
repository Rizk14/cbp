<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu17')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productprice/cform/index/';
/*
			$query = $this->db->query("select distinct i_product, i_product_grade, v_product_mill 
						   from tr_product_price order by i_product",false);
*/
			$query = $this->db->query("select * from tr_product_price",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '20';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$data['page_title'] = $this->lang->line('master_productprice');
			$data['iproduct']='';
			$data['cari']='';
			$this->load->model('productprice/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('productprice/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu17')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	= $this->input->post('iproduct', TRUE);
			$eproductname 	= $this->input->post('eproductname', TRUE);
			$iproductgrade 	= $this->input->post('iproductgrade', TRUE);
			$nproductmargin	= $this->input->post('nproductmargin', TRUE);
			$vproductmill	= $this->input->post('vproductmill', TRUE);
			$nproductmargin = str_replace(",","",$this->input->post('nproductmargin', TRUE));
			$vproductmill	= str_replace(",","",$this->input->post('vproductmill', TRUE));
			if($vproductmill=='')
				$vproductmill=0;
			if ((isset($iproduct) && $iproduct != '') && (isset($eproductname) && $eproductname != '') && (isset($iproductgrade) && $iproductgrade != '') && (isset($nproductmargin) && $nproductmargin != ''))
			{
				$this->load->model('productprice/mmaster');
				$query = $this->db->query(" select distinct i_product, i_product_grade, v_product_mill 
										   	from tr_product_price where i_product='$iproduct' 
											and i_product_grade='$iproductgrade' ",false);
				if($query->num_rows()>0){ 
					$this->mmaster->update($iproduct,$eproductname,$iproductgrade,$nproductmargin,$vproductmill);
				}else{
					$this->mmaster->insert($iproduct,$eproductname,$iproductgrade,$nproductmargin,$vproductmill);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu17')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_productprice');
			$this->load->view('productprice/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu17')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('master_productprice')." update";
			if( ($this->uri->segment(4)) && ($this->uri->segment(5)) ){
				$iproduct = $this->uri->segment(4);
				$iproductgrade = $this->uri->segment(5);
				$data['iproduct'] = $iproduct;
				$data['iproductgrade'] = $iproductgrade;
				$this->load->model('productprice/mmaster');
				$data['isi']=$this->mmaster->baca($iproduct,$iproductgrade);
		 		$this->load->view('productprice/vmainform',$data);
			}else{
				$this->load->view('productprice/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu17')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	= $this->input->post('iproduct', TRUE);
			$eproductname 	= $this->input->post('eproductname', TRUE);
			$iproductgrade 	= $this->input->post('iproductgrade', TRUE);
			$nproductmargin	= $this->input->post('nproductmargin', TRUE);
			$vproductmill	= $this->input->post('vproductmill', TRUE);
			$nproductmargin	= str_replace(",","",$this->input->post('nproductmargin', TRUE));
			$vproductmill	= str_replace(",","",$this->input->post('vproductmill', TRUE));
			if($vproductmill=='')
				$vproductmill=0;			
			$this->load->model('productprice/mmaster');
			$this->mmaster->update($iproduct,$eproductname,$iproductgrade,$nproductmargin,$vproductmill);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu17')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iproduct 	= $this->uri->segment(4);
			$iproductgrade	= $this->uri->segment(5);
			$this->load->model('productprice/mmaster');
			$this->mmaster->delete($iproduct,$iproductgrade);
			$config['base_url'] = base_url().'index.php/productprice/cform/index/';
			$query = $this->db->query("select distinct i_product, i_product_grade, v_product_mill 
						   from tr_product_price order by i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '20';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);

			$data['page_title'] = $this->lang->line('master_productprice');
			$data['iproduct']='';
			$data['iproductgrade']='';
			$this->load->model('productprice/mmaster');
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$data['isi']=$this->mmaster->bacasemua($cari,$config['per_page'],$this->uri->segment(4));
			$this->load->view('productprice/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu17')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productprice/cform/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query(" select * from tr_product_price 
					                        where (upper(tr_product_price.i_product) like '%$cari%' 
					                        or upper(tr_product_price.e_product_name) = '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '20';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->paginationxx->initialize($config);
			$this->load->model('productprice/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('master_productprice');
			$data['iproduct']='';
			$data['iproductgrade']='';
	 		$this->load->view('productprice/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productgrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu17')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productprice/cform/productgrade/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_product_grade from tr_product_grade 
										where upper(i_product_grade) like '%$cari%' or upper(e_product_gradename) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productprice/mmaster');
			$data['page_title'] = $this->lang->line('list_productgrade');
			$data['isi']=$this->mmaster->bacaproductgrade($config['per_page'],$this->uri->segment(5));
			$this->load->view('productprice/vlistproductgrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductgrade()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu17')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/productprice/cform/productgrade/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_product_grade from tr_product_grade 
										where upper(i_product_grade) like '%$cari%' or upper(e_product_gradename) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productprice/mmaster');
			$data['page_title'] = $this->lang->line('list_productgrade');
			$data['isi']=$this->mmaster->cariproductgrade($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productprice/vlistproductgrade', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu17')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$baris=$this->uri->segment(4);
			$cari =$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/productprice/cform/product/'.$baris.'/'.$cari.'/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("select i_product from tr_product
										where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productprice/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($cari,$config['per_page'],$this->uri->segment(7));
			$this->load->view('productprice/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    	($this->session->userdata('menu17')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$config['base_url'] = base_url().'index.php/productprice/cform/product/'.$cari.'/'.'index/';
			$query = $this->db->query("select i_product from tr_product
						   where upper(i_product) like '%$cari%' or upper(e_product_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('productprice/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('productprice/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
