<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
    $this->load->dbutil();
    $this->load->helper('file');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu134')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exp-pelunasanpiutang');
			$data['page_title1'] = "Export Pelunasan Piutang / Tanggal";
			$data['iperiode']	= '';
			$data['iarea']	  = '';
			$data['iarea2']	  = '';
			$data['datefrom']	= '';
			$data['dateto']	= '';
			$this->load->view('exp-pelunasanpiutang/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu134')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-pelunasanpiutang/mmaster');
			$iarea  	= $this->input->post('iarea');
			$iperiode	= $this->input->post('iperiode');

      if($iarea=='NA'){
      $area='00';
      $que ="  a.i_pelunasan, a.i_dt, a.i_area, a.i_jenis_bayar, a.i_giro, a.i_customer, a.d_giro, 
                  a.d_bukti, a.d_cair,a.e_bank_name, a.v_jumlah, a.v_lebih, a.f_posting, a.f_close, a.f_pelunasan_cancel, 
                  a.i_cek, a.d_cek, a.e_cek,a.i_cek_ikhp, a.d_cek_ikhp, a.d_dt, b.i_nota, b.d_nota, b.v_jumlah as v_jumlah_item, 
                  b.v_sisa, c.e_jenis_bayarname, d.v_nota_netto
                  from tm_pelunasan a, tm_pelunasan_item b, tr_jenis_bayar c, tm_nota d
                  where a.i_pelunasan=b.i_pelunasan and a.i_area=b.i_area and a.i_dt=b.i_dt 
                  and to_char(a.d_bukti,'yyyymm')='$iperiode' and a.i_area like '%%' and a.f_pelunasan_cancel='0' 
                  and a.f_giro_tolak='0' and a.f_giro_batal='0'
                  and a.i_jenis_bayar=c.i_jenis_bayar and d.i_nota=b.i_nota 
                  order by a.i_pelunasan, b.n_item_no";
      }
      else{
      $area=$iarea;
      $que = " a.i_pelunasan, a.i_dt, a.i_area, a.i_jenis_bayar, a.i_giro, a.i_customer, a.d_giro, 
                  a.d_bukti, a.d_cair,a.e_bank_name, a.v_jumlah, a.v_lebih, a.f_posting, a.f_close, a.f_pelunasan_cancel, 
                  a.i_cek, a.d_cek, a.e_cek,a.i_cek_ikhp, a.d_cek_ikhp, a.d_dt, b.i_nota, b.d_nota, b.v_jumlah as v_jumlah_item, 
                  b.v_sisa, c.e_jenis_bayarname, d.v_nota_netto
                  from tm_pelunasan a, tm_pelunasan_item b, tr_jenis_bayar c, tm_nota d
                  where a.i_pelunasan=b.i_pelunasan and a.i_area=b.i_area and a.i_dt=b.i_dt 
                  and to_char(a.d_bukti,'yyyymm')='$iperiode' and a.i_area='$iarea' and a.f_pelunasan_cancel='0' 
                  and a.f_giro_tolak='0' and a.f_giro_batal='0'
                  and a.i_jenis_bayar=c.i_jenis_bayar and d.i_nota=b.i_nota 
                  order by a.i_pelunasan, b.n_item_no
                          ";
      }


      $this->db->select($que,false);
#and a.n_faktur_komersialprint>0
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Export Pelunasan Piutang")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14)->setVisible(false);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15)->setVisible(false);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(14)->setVisible(false);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(14)->setVisible(false);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(12);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:AC1'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'CEK');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'NOBUKTIBR');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOBUKTI');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'NOGIRO');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'GIRO');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'NMBANK');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'TGLGIRO');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Tanggal GIRO');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'JUMLAH');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'KODELANG');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K1', 'TGLBUKTI');
				$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Tanggal BUKTI');
				$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M1', 'TGLCAIR');
				$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Tanggal CAIR');
				$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O1', 'NODOK');
				$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P1', 'TGLDOK');
				$objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Tanggal DOK');
				$objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('R1', 'NILAI');
				$objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('S1', 'JUMBYR');
				$objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('T1', 'SISA');
				$objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('U1', 'LEBIH');
				$objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('V1', 'KET');
				$objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('W1', 'BATAL');
				$objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('X1', 'TGLBUAT');
				$objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Y1', 'JAMBUAT');
				$objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Z1', 'TGLUBAH');
				$objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AA1', 'JAMUBAH');
				$objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AB1', 'TGLTRANSF');
				$objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AC1', 'JAMTRANSF');
				$objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				
				$i=2;
				foreach($query->result() as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':AC'.$i
				  );
          
            $cek='';
          $jenis            = $row->i_jenis_bayar;
	      $nobuktidbase     = str_replace('-','/',substr($row->i_pelunasan,0,10)).$jenis;
	      if( (trim($row->i_giro)=='') || ($row->i_giro==null) ){
  	      $nogiro         = $row->i_jenis_bayar.$row->e_jenis_bayarname;
			}else{
			  $nogiro         = $row->i_jenis_bayar.$row->i_giro;
			}
			$giro             = '';
		  if($jenis=='D'){
          switch (substr($row->e_bank_name,10,1)) {
            case 'A':
              $jenislama='A';
              break;
            case 'B':
              $jenislama='G';
              break;
            case 'C':
              $jenislama='B';
              break;
            case 'D':
              $jenislama='C';
              break;
            case 'E':
              $jenislama='D';
              break;
          }
  	      $row->e_bank_name = str_replace('-','/',substr($row->e_bank_name,0,10)).$jenislama;
		  }
			$nmbank           = $row->e_bank_name;
		  if($row->i_jenis_bayar=='02' || $row->i_jenis_bayar=='04'){
            $tmp=explode('-',$row->d_bukti);
            $hr=$tmp[2];
            $bl=$tmp[1];
            $th=$tmp[0];
            //$row->d_bukti=$hr.'-'.$bl.'-'.$th;
			$tglgiro = $th.'-'.$bl.'-'.$hr;
			}else{
				if($row->d_giro=='' || $row->d_giro==null){
					$tglgiro='-';
				}else{
			$tmp=explode('-',$row->d_giro);
            $hr=$tmp[2];
            $bl=$tmp[1];
            $th=$tmp[0];
            //$row->d_giro=$hr.'-'.$bl.'-'.$th;
			$tglgiro = $th.'-'.$bl.'-'.$hr;
			}
		}
			$jumlah           = $row->v_jumlah;
			$kodelang         = $row->i_customer;
			$tmp=explode('-',$row->d_bukti);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				//$row->d_bukti=$hr.'-'.$bl.'-'.$th;
			$tglbukti = $th.'-'.$bl.'-'.$hr;
			/*if($bl=='01'){ $bul='Jan';}
			else if($bl=='02'){ $bul='Feb';}else if($bl=='03'){ $bul='Mar';}else if($bl=='04'){ $bul='Apr';}
			else if($bl=='05'){ $bul='May';}else if($bl=='06'){ $bul='Jun';}else if($bl=='07'){ $bul='Jul';}
			else if($bl=='08'){ $bul='Aug';}else if($bl=='09'){ $bul='Sep';}else if($bl=='10'){ $bul='Oct';}
			else if($bl=='11'){ $bul='Nov';}else if($bl=='12'){ $bul='Dec';}
			*/
			if($row->i_jenis_bayar=='02' || $row->i_jenis_bayar=='04'){
				$tmp=explode('-',$row->d_bukti);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				//$row->d_bukti=$hr.'-'.$bl.'-'.$th;
			    $tglcair = $th.'-'.$bl.'-'.$hr;
			}elseif($row->d_cair=='' || $row->d_cair==null){
				if($row->d_giro=='' || $row->d_giro==null){
					$tglcair='-';
				}else{
				$tmp=explode('-',$row->d_giro);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				//$row->d_giro=$hr.'-'.$bl.'-'.$th;
			  $tglcair = $th.'-'.$bl.'-'.$hr;
			}
			}else{
				$tmp=explode('-',$row->d_cair);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				//$row->d_cair=$hr.'-'.$bl.'-'.$th;
			  $tglcair = $th.'-'.$bl.'-'.$hr;
			}
			$nodok            = $row->i_nota;
			$tmp=explode('-',$row->d_nota);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				//$row->d_nota=$hr.'-'.$bl.'-'.$th;
			$tgldok = $th.'-'.$bl.'-'.$hr;

          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $cek, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_pelunasan, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $nobuktidbase , Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $nogiro, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $giro, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $nmbank, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $tglgiro, Cell_DataType::TYPE_STRING);
          //$objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, date('d. M. Y',strtotime($tglgiro)), Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '=IF(G'.$i.'="-","-",VALUE(G'.$i.'))');
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $jumlah, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $kodelang, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $tglbukti, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '=IF(K'.$i.'="-","-",VALUE(K'.$i.'))');
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $tglcair, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '=IF(M'.$i.'="-","-",VALUE(M'.$i.'))');
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $nodok, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $tgldok, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '=IF(P'.$i.'="-","-",VALUE(P'.$i.'))');
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $row->v_jumlah_item, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $row->v_sisa, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $row->v_lebih, Cell_DataType::TYPE_NUMERIC);
					$i++;
				}
        $x=$i-1;
			}
      $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='Pelunasan Piutang '.$iarea.' '.$iperiode.'.xls';
			$objWriter->save('excel/'.$area.'/'.$nama);
/*
      if(file_exists('excel/'.$area.'/'.$nama)){
        @chmod('excel/'.$area.'/'.$nama, 0777);
        unlink('excel/'.$area.'/'.$nama);
*/      

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
		  $data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			$pesan='Export Pelunasan Piutang '.$iarea.' Periode:'.$iperiode;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function view2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu134')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-pelunasanpiutang/mmaster');
			$datefrom = $this->input->post('datefrom');
			$dateto	  = $this->input->post('dateto');

			$iarea  	= $this->input->post('iarea2');

      if($iarea=='NA'){
      $area='00';
      $que ="  a.i_pelunasan, a.i_dt, a.i_area, a.i_jenis_bayar, a.i_giro, a.i_customer, a.d_giro, 
                  a.d_bukti, a.d_cair,a.e_bank_name, a.v_jumlah, a.v_lebih, a.f_posting, a.f_close, a.f_pelunasan_cancel, 
                  a.i_cek, a.d_cek, a.e_cek,a.i_cek_ikhp, a.d_cek_ikhp, a.d_dt, b.i_nota, b.d_nota, b.v_jumlah as v_jumlah_item, 
                  b.v_sisa, c.e_jenis_bayarname, d.v_nota_netto
                  from tm_pelunasan a, tm_pelunasan_item b, tr_jenis_bayar c, tm_nota d
                  where a.i_pelunasan=b.i_pelunasan and a.i_area=b.i_area and a.i_dt=b.i_dt 
                  and a.d_bukti >= to_date('$datefrom','dd-mm-yyyy') and a.d_bukti <= to_date('$dateto','dd-mm-yyyy') and a.i_area like '%%'
                  and a.f_pelunasan_cancel='0' 
                  and a.f_giro_tolak='0' and a.f_giro_batal='0'
                  and a.i_jenis_bayar=c.i_jenis_bayar and d.i_nota=b.i_nota 
                  order by a.i_pelunasan, b.n_item_no";
      }
      else{
      $area=$iarea;
      $que = " a.i_pelunasan, a.i_dt, a.i_area, a.i_jenis_bayar, a.i_giro, a.i_customer, a.d_giro, 
                  a.d_bukti, a.d_cair,a.e_bank_name, a.v_jumlah, a.v_lebih, a.f_posting, a.f_close, a.f_pelunasan_cancel, 
                  a.i_cek, a.d_cek, a.e_cek,a.i_cek_ikhp, a.d_cek_ikhp, a.d_dt, b.i_nota, b.d_nota, b.v_jumlah as v_jumlah_item, 
                  b.v_sisa, c.e_jenis_bayarname, d.v_nota_netto
                  from tm_pelunasan a, tm_pelunasan_item b, tr_jenis_bayar c, tm_nota d
                  where a.i_pelunasan=b.i_pelunasan and a.i_area=b.i_area and a.i_dt=b.i_dt 
                  and a.d_bukti >= to_date('$datefrom','dd-mm-yyyy') and a.d_bukti <= to_date('$dateto','dd-mm-yyyy') and a.i_area='$iarea'
                  and a.f_pelunasan_cancel='0' 
                  and a.f_giro_tolak='0' and a.f_giro_batal='0'
                  and a.i_jenis_bayar=c.i_jenis_bayar and d.i_nota=b.i_nota 
                  order by a.i_pelunasan, b.n_item_no
                          ";
      }


      $this->db->select($que,false);
#and a.n_faktur_komersialprint>0
		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Export Pelunasan Piutang")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(14)->setVisible(false);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15)->setVisible(false);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(14)->setVisible(false);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(14)->setVisible(false);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(11);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(12);
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A1:AC1'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A1', 'CEK');
				$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('B1', 'NOBUKTIBR');
				$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C1', 'NOBUKTI');
				$objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D1', 'NOGIRO');
				$objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E1', 'GIRO');
				$objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F1', 'NMBANK');
				$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G1', 'TGLGIRO');
				$objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Tanggal GIRO');
				$objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I1', 'JUMLAH');
				$objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('J1', 'KODELANG');
				$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('K1', 'TGLBUKTI');
				$objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('L1', 'Tanggal BUKTI');
				$objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('M1', 'TGLCAIR');
				$objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('N1', 'Tanggal CAIR');
				$objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('O1', 'NODOK');
				$objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('P1', 'TGLDOK');
				$objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Q1', 'Tanggal DOK');
				$objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('R1', 'NILAI');
				$objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('S1', 'JUMBYR');
				$objPHPExcel->getActiveSheet()->getStyle('S1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('T1', 'SISA');
				$objPHPExcel->getActiveSheet()->getStyle('T1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('U1', 'LEBIH');
				$objPHPExcel->getActiveSheet()->getStyle('U1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('V1', 'KET');
				$objPHPExcel->getActiveSheet()->getStyle('V1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('W1', 'BATAL');
				$objPHPExcel->getActiveSheet()->getStyle('W1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('X1', 'TGLBUAT');
				$objPHPExcel->getActiveSheet()->getStyle('X1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Y1', 'JAMBUAT');
				$objPHPExcel->getActiveSheet()->getStyle('Y1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('Z1', 'TGLUBAH');
				$objPHPExcel->getActiveSheet()->getStyle('Z1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AA1', 'JAMUBAH');
				$objPHPExcel->getActiveSheet()->getStyle('AA1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AB1', 'TGLTRANSF');
				$objPHPExcel->getActiveSheet()->getStyle('AB1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('AC1', 'JAMTRANSF');
				$objPHPExcel->getActiveSheet()->getStyle('AC1')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				
				$i=2;
				foreach($query->result() as $row){
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':AC'.$i
				  );
          
            $cek='';
          $jenis            = $row->i_jenis_bayar;
	      $nobuktidbase     = str_replace('-','/',substr($row->i_pelunasan,0,10)).$jenis;
	      if( (trim($row->i_giro)=='') || ($row->i_giro==null) ){
  	      $nogiro         = $row->i_jenis_bayar.$row->e_jenis_bayarname;
			}else{
			  $nogiro         = $row->i_jenis_bayar.$row->i_giro;
			}
			$giro             = '';
		  if($jenis=='D'){
          switch (substr($row->e_bank_name,10,1)) {
            case 'A':
              $jenislama='A';
              break;
            case 'B':
              $jenislama='G';
              break;
            case 'C':
              $jenislama='B';
              break;
            case 'D':
              $jenislama='C';
              break;
            case 'E':
              $jenislama='D';
              break;
          }
  	      $row->e_bank_name = str_replace('-','/',substr($row->e_bank_name,0,10)).$jenislama;
		  }
			$nmbank           = $row->e_bank_name;
		  if($row->i_jenis_bayar=='02' || $row->i_jenis_bayar=='04'){
            $tmp=explode('-',$row->d_bukti);
            $hr=$tmp[2];
            $bl=$tmp[1];
            $th=$tmp[0];
            //$row->d_bukti=$hr.'-'.$bl.'-'.$th;
			$tglgiro = $th.'-'.$bl.'-'.$hr;
			}else{
				if($row->d_giro=='' || $row->d_giro==null){
					$tglgiro='-';
				}else{
			$tmp=explode('-',$row->d_giro);
            $hr=$tmp[2];
            $bl=$tmp[1];
            $th=$tmp[0];
            //$row->d_giro=$hr.'-'.$bl.'-'.$th;
			$tglgiro = $th.'-'.$bl.'-'.$hr;
			}
		}
			$jumlah           = $row->v_jumlah;
			$kodelang         = $row->i_customer;
			$tmp=explode('-',$row->d_bukti);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				//$row->d_bukti=$hr.'-'.$bl.'-'.$th;
			$tglbukti = $th.'-'.$bl.'-'.$hr;
			/*if($bl=='01'){ $bul='Jan';}
			else if($bl=='02'){ $bul='Feb';}else if($bl=='03'){ $bul='Mar';}else if($bl=='04'){ $bul='Apr';}
			else if($bl=='05'){ $bul='May';}else if($bl=='06'){ $bul='Jun';}else if($bl=='07'){ $bul='Jul';}
			else if($bl=='08'){ $bul='Aug';}else if($bl=='09'){ $bul='Sep';}else if($bl=='10'){ $bul='Oct';}
			else if($bl=='11'){ $bul='Nov';}else if($bl=='12'){ $bul='Dec';}
			*/
			if($row->i_jenis_bayar=='02' || $row->i_jenis_bayar=='04'){
				$tmp=explode('-',$row->d_bukti);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				//$row->d_bukti=$hr.'-'.$bl.'-'.$th;
			    $tglcair = $th.'-'.$bl.'-'.$hr;
			}elseif($row->d_cair=='' || $row->d_cair==null){
				if($row->d_giro=='' || $row->d_giro==null){
					$tglcair='-';
				}else{
				$tmp=explode('-',$row->d_giro);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				//$row->d_giro=$hr.'-'.$bl.'-'.$th;
			  $tglcair = $th.'-'.$bl.'-'.$hr;
			}
			}else{
				$tmp=explode('-',$row->d_cair);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				//$row->d_cair=$hr.'-'.$bl.'-'.$th;
			  $tglcair = $th.'-'.$bl.'-'.$hr;
			}
			$nodok            = $row->i_nota;
			$tmp=explode('-',$row->d_nota);
				$hr=$tmp[2];
				$bl=$tmp[1];
				$th=$tmp[0];
				//$row->d_nota=$hr.'-'.$bl.'-'.$th;
			$tgldok = $th.'-'.$bl.'-'.$hr;

          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $cek, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->i_pelunasan, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $nobuktidbase , Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $nogiro, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $giro, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $nmbank, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $tglgiro, Cell_DataType::TYPE_STRING);
          //$objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, date('d. M. Y',strtotime($tglgiro)), Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, '=IF(G'.$i.'="-","-",VALUE(G'.$i.'))');
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$i, $jumlah, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$i, $kodelang, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$i, $tglbukti, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, '=IF(K'.$i.'="-","-",VALUE(K'.$i.'))');
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$i, $tglcair, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValue('N'.$i, '=IF(M'.$i.'="-","-",VALUE(M'.$i.'))');
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$i, $nodok, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$i, $tgldok, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, '=IF(P'.$i.'="-","-",VALUE(P'.$i.'))');
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$i, $row->v_nota_netto, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$i, $row->v_jumlah_item, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$i, $row->v_sisa, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$i, $row->v_lebih, Cell_DataType::TYPE_NUMERIC);
					$i++;
				}
        $x=$i-1;
			}
      $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      //$nama='Pelunasan Piutang '.$iarea.' '.$iperiode.'.xls';
      $nama="Pelunasan Piutang $iarea Tgl $datefrom - $dateto.xls";
			$objWriter->save('excel/'.$area.'/'.$nama);
/*
      if(file_exists('excel/'.$area.'/'.$nama)){
        @chmod('excel/'.$area.'/'.$nama, 0777);
        unlink('excel/'.$area.'/'.$nama);
*/      

		  $sess=$this->session->userdata('session_id');
		  $id=$this->session->userdata('user_id');
		  $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		  $rs		=  $this->db->query($sql);
		  if($rs->num_rows>0){
			  foreach($rs->result() as $tes){
				  $ip_address	  = $tes->ip_address;
				  break;
			  }
		  }else{
			  $ip_address='kosong';
		  }

		  $data['user']	= $this->session->userdata('user_id');
#			$data['host']	= $this->session->userdata('printerhost');
		  $data['host']	= $ip_address;
		  $data['uri']	= $this->session->userdata('printeruri');
			$query 	= pg_query("SELECT current_timestamp as c");
			while($row=pg_fetch_assoc($query)){
				$now	  = $row['c'];
			}
			//$pesan='Export Pelunasan Piutang '.$iarea.' Periode:'.$iperiode;
			$pesan="Export Pelunasan Piutang $iarea Tgl $datefrom - $dateto.xls";
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

		function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu243')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-pelunasanpiutang/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-pelunasanpiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('exp-pelunasanpiutang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function area2()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu243')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-pelunasanpiutang/cform/area/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area",false);
			}else{
				$query = $this->db->query("select * from tr_area where i_area = '$area1' or i_area = '$area2' or i_area = '$area3'
										   or i_area = '$area4' or i_area = '$area5'",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-pelunasanpiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('exp-pelunasanpiutang/vlistarea2', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu243')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			$config['base_url'] = base_url().'index.php/exp-pelunasanpiutang/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			if($area1=='00' or $area2=='00' or $area3=='00' or $area4=='00' or $area5=='00'){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')",false);
			}else{
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') and (i_area = '$area1' or i_area = '$area2' 
										   or i_area = '$area3' or i_area = '$area4' or i_area = '$area5')",false);
			}
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-pelunasanpiutang/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('exp-pelunasanpiutang/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
