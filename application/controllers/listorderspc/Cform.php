<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu307')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('orderkonsinyasi');
			$data['dfrom']='';
			$data['dto']='';
			$this->load->view('listorderspc/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function view()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu307')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$area = $this->session->userdata("i_area");
      $area2= $this->session->userdata('i_area2');
      $area3= $this->session->userdata('i_area3');
      $area4= $this->session->userdata('i_area4');
      $area5= $this->session->userdata('i_area5');

			$config['base_url'] = base_url().'index.php/listorderspc/cform/view/'.$dfrom.'/'.$dto.'/index/';
			if($area=='PB'){
			  $query = $this->db->query(" select a.i_orderpb from tm_orderpb a, tr_spg b, tr_customer c
                                    where a.i_spg=b.i_spg and a.f_orderpb_cancel='f'
                                    and (upper(a.i_orderpb) like '%$cari%')
                                    and a.i_customer=c.i_customer and
                                    a.d_orderpb >= to_date('$dfrom','dd-mm-yyyy') AND
                                    a.d_orderpb <= to_date('$dto','dd-mm-yyyy')
                                    order by a.i_orderpb",false);
      }else{
			  $query = $this->db->query(" select a.i_orderpb from tm_orderpb a, tr_spg b, tr_customer c
                                    where a.i_spg=b.i_spg and a.f_orderpb_cancel='f'
                                    and (upper(a.i_orderpb) like '%$cari%')
                                    and a.i_customer=c.i_customer and (a.i_area='$area' or a.i_area='$area2' or a.i_area='$area3' or 
                                                                       a.i_area='$area4' or a.i_area='$area5') and
                                    a.d_orderpb >= to_date('$dfrom','dd-mm-yyyy') AND
                                    a.d_orderpb <= to_date('$dto','dd-mm-yyyy')
                                    order by a.i_orderpb",false);
      }
			$config['total_rows']	= $query->num_rows(); 
			$config['per_page']	= '10';
			$config['first_link']	= 'Awal';
			$config['last_link']	= 'Akhir';
			$config['next_link']	= 'Selanjutnya';
			$config['prev_link']	= 'Sebelumnya';
			$config['cur_page']	= $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('listorderspc/mmaster');
			$data['page_title'] = $this->lang->line('orderkonsinyasi');
			$data['cari']	  = $cari;
			$data['dfrom']	= $dfrom;
			$data['dto']	  = $dto;
			$data['isi']	  = $this->mmaster->bacaperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari,$area,$area2,$area3,$area4,$area5);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Membuka Data Orderan SPC Periode:'.$dfrom.' s/d '.$dto;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan );  

			$this->load->view('listorderspc/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu307')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('orderkonsinyasi');
			$this->load->view('listorderspc/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu307')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari		= strtoupper($this->input->post('cari'));
			$dfrom		= $this->input->post('dfrom');
			$dto		= $this->input->post('dto');
			if($dfrom=='') $dfrom=$this->uri->segment(4);
			if($dto=='') $dto=$this->uri->segment(5);
			$config['base_url'] = base_url().'index.php/listorderspc/cform/view/'.$dfrom.'/'.$dto.'/index/';
		  $query = $this->db->query(" select a.i_orderpb from tm_orderpb a, tr_spg b, tr_customer c
                                where a.i_spg=b.i_spg and a.f_orderpb_cancel='f'
                                and (upper(a.i_orderpb) like '%$cari%')
                                and a.i_customer=c.i_customer and
                                a.d_orderpb >= to_date('$dfrom','dd-mm-yyyy') AND
                                a.d_orderpb <= to_date('$dto','dd-mm-yyyy')
                                order by a.i_orderpb",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);
			$this->load->model('listorderspc/mmaster');
			$data['page_title'] = $this->lang->line('orderkonsinyasi');
			$data['cari']		= $cari;
			$data['dfrom']  = $dfrom;
			$data['dto']		= $dto;
			$data['isi']		= $this->mmaster->bacaperiode($dfrom,$dto,$config['per_page'],$this->uri->segment(7),$cari);
			$this->load->view('listorderspc/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu307')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('orderkonsinyasi')." update";
			if($this->uri->segment(4)!=''){
				$iorderpb  = $this->uri->segment(4);
				$dfrom    = $this->uri->segment(5);
				$dto 	    = $this->uri->segment(6);
				$tgl= $this->uri->segment(7);
  			$icustomer= $this->uri->segment(8);
        $iarea=$this->session->userdata("i_area");
				$data['iorderpb']  = $iorderpb;
				$data['dfrom'] = $dfrom;
				$data['dto']	 = $dto;
				$data['icustomer']= $icustomer;
				$data['iarea'] = $iarea;
				$query = $this->db->query("select i_product from tm_orderpb_item where i_orderpb='$iorderpb' and i_customer='$icustomer'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('listorderspc/mmaster');
				$data['isi']=$this->mmaster->baca($iorderpb,$icustomer);
				$data['detail']=$this->mmaster->bacadetail($iorderpb,$icustomer);
		 		$this->load->view('listorderspc/vformupdate',$data);
			}else{
				$this->load->view('listorderspc/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu307')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$xiorderpb		= $this->input->post('xiorderpb', TRUE);
			$iorderpb 		= $this->input->post('iorderpb', TRUE);
			$dorderpb 		= $this->input->post('dorderpb', TRUE);
			if($dorderpb!=''){
				$tmp=explode("-",$dorderpb);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dorderpb=$th."-".$bl."-".$hr;
				$thbl=substr($th,2,2).$bl;
			}
			$iarea	= $this->input->post('iarea', TRUE);

			$ispg   = $this->input->post('ispg', TRUE);
			$icustomer= $this->input->post('icustomer', TRUE);
			$norderpbdiscount= $this->input->post('norderpbdiscount', TRUE);
		  $norderpbdiscount= str_replace(',','',$norderpbdiscount);
			$vorderpbdiscount= $this->input->post('vorderpbdiscount', TRUE);
		  $vorderpbdiscount= str_replace(',','',$vorderpbdiscount);
			$vorderpbgross= $this->input->post('vorderpbgross', TRUE);
		  $vorderpbgross	  	= str_replace(',','',$vorderpbgross);
			$jml		= $this->input->post('jml', TRUE);
			if($dorderpb!='' && $iorderpb!='' && $jml>0)
			{
				$this->db->trans_begin();
				$this->load->model('listorderspc/mmaster');
				$this->mmaster->deleteheader($xiorderpb, $iarea, $icustomer);
				$this->mmaster->insertheader($iorderpb, $dorderpb, $iarea, $ispg, $icustomer, $norderpbdiscount, $vorderpbdiscount, $vorderpbgross);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= 'A';
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		  = $this->input->post('vunitprice'.$i, TRUE);
				  $vunitprice	  	= str_replace(',','',$vunitprice);
				  $nquantity   		= $this->input->post('nquantityorder'.$i, TRUE);
				  $nquantity	  	= str_replace(',','',$nquantity);
				  $nstock      		= $this->input->post('nquantitystock'.$i, TRUE);
				  $nstock   	  	= str_replace(',','',$nstock);
          $ipricegroupco  = $this->input->post('ipricegroupco'.$i, TRUE);
          $eremark        = $this->input->post('eremark'.$i, TRUE);
          $this->mmaster->deletedetail($iproduct, $iproductgrade, $xiorderpb, $iarea, $icustomer, $iproductmotif);
				  if($nquantity>0){
				    $this->mmaster->insertdetail($iorderpb,$iarea,$icustomer,$dorderpb,$iproduct,$eproductname,$iproductmotif,$iproductgrade,$nquantity,$nstock,$eremark,$i);
				  }
				}
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
#			    $this->db->trans_rollback();
			    $this->db->trans_commit();
					$data['sukses']			= true;
					$data['inomor']			= $iorderpb;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu307')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
#			$data['baris']=$this->uri->segment(4);
#			$baris=$this->uri->segment(4);
      $cari=strtoupper($this->input->post("cari"));
      $baris=$this->input->post("baris");
      $peraw=$this->input->post("peraw");
      $perak=$this->input->post("perak");
      $area =$this->input->post("area");
			if($baris=='')$baris=$this->uri->segment(4);
      if($peraw=='')$peraw=$this->uri->segment(5);
      if($perak=='')$perak=$this->uri->segment(6);
      if($area=='')$area=$this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/productupdate/'.$baris.'/'.$peraw.'/'.$perak.'/'.$area.'/';

#			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/productupdate/'.$baris.'/index/';
			$query = $this->db->query(" select a.i_product from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('orderkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(8),$peraw,$perak,$cari);
			$data['baris']=$baris;
      $data['peraw']=$peraw;
      $data['perak']=$perak;
      $data['area']=$area;
			$this->load->view('orderkonsinyasi/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu307')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari=strtoupper($this->input->post("cari"));
      $baris=$this->input->post("baris");
			if($baris=='')$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/product/'.$baris.'/';
			$query = $this->db->query(" select a.i_product
										              from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product and upper(a.i_product) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('listorderspc/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$baris;
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('listorderspc/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cariproductupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu307')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/orderkonsinyasi/cform/productupdate/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_retail as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
										and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc "
									  ,false);
# c.v_product_mill as harga
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';

			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('orderkonsinyasi/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('orderkonsinyasi/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

}
?>
