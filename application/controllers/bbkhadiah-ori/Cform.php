<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbk 		= $this->input->post('ibbk', TRUE);
      $ibbktype = '03';
			$ibbkold 	= $this->input->post('ibbkold', TRUE);
			$dbbk 		= $this->input->post('dbbk', TRUE);
			if($dbbk!=''){
				$tmp=explode("-",$dbbk);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbbk=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
			}
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomername= $this->input->post('ecustomername', TRUE);
			$eremark	    = $this->input->post('eremark', TRUE);
			$jml		      = $this->input->post('jml', TRUE);
			if($dbbk!='' && $ecustomername!='' && $jml!='' && $jml!='0')
			{
				$this->db->trans_begin();
				$this->load->model('bbkhadiah/mmaster');
        $istore				    = 'AA';
				$istorelocation		= '01';
				$istorelocationbin= '00';
				$ibbk	=$this->mmaster->runningnumber($thbl);
				$this->mmaster->insertheader($ibbk, $ibbktype, $dbbk, $icustomer, $ibbkold, $eremark);
				for($i=1;$i<=$jml;$i++){
				  $iproduct			  = $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= 'A';
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice		  = $this->input->post('vunitprice'.$i, TRUE);
				  $vunitprice	  	= str_replace(',','',$vunitprice);
				  $nquantity   		= $this->input->post('nquantity'.$i, TRUE);
				  $eremark		  	= $this->input->post('eremark'.$i, TRUE);
				  if($nquantity>0){
				    $this->mmaster->insertdetail( $ibbk,$ibbktype,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$vunitprice,$eremark,$i,$thbl);
############
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttransbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibbk,$q_in,$q_out,$nquantity,$q_aw,$q_ak);
            $th=substr($dbbk,0,4);
            $bl=substr($dbbk,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateicbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$q_ak);
            }else{
              $this->mmaster->inserticbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nquantity);
            }
############
				  }
				}
				if (($this->db->trans_status() === FALSE))
				{
			    $this->db->trans_rollback();
				}else{
#			    $this->db->trans_rollback();
			    $this->db->trans_commit();

		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Input BBK-Hadiah No:'.$ibbk;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ibbk;
					$this->load->view('nomor',$data);
				}
			}else{
				$data['page_title'] = $this->lang->line('bbkhadiah');
				$data['ibbk']='';
        $data['cari']='';
				$this->load->model('bbkhadiah/mmaster');
				$data['isi']="";
				$data['detail']="";
				$data['jmlitem']="";
				$data['tgl']=date('d-m-Y');
				$this->load->view('bbkhadiah/vmainform', $data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bbkhadiah');
			$this->load->view('bbkhadiah/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('bbkhadiah')." update";
			if($this->uri->segment(4)!=''){
				$ibbk  = $this->uri->segment(4);
				$dfrom  = $this->uri->segment(5);
				$dto 	  = $this->uri->segment(6);
				$data['ibbk'] = $ibbk;
				$data['dfrom'] = $dfrom;
				$data['dto']	 = $dto;
				$query = $this->db->query("select i_product from tm_bbk_item where i_bbk='$ibbk' and i_bbk_type='03'");
				$data['jmlitem'] = $query->num_rows(); 				
				$this->load->model('bbkhadiah/mmaster');
				$data['isi']=$this->mmaster->baca($ibbk);
				$data['detail']=$this->mmaster->bacadetail($ibbk);
		 		$this->load->view('bbkhadiah/vmainform',$data);
			}else{
				$this->load->view('bbkhadiah/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbk 		= $this->input->post('ibbk', TRUE);
			$dbbk 		= $this->input->post('dbbk', TRUE);
 			$eremark	= $this->input->post('eremark', TRUE);
			if($dbbk!=''){
				$tmp=explode("-",$dbbk);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dbbk=$th."-".$bl."-".$hr;
        $thbl=$th.$bl;
			}
      $ibbktype='03';
      $istore				    = 'AA';
      $istorelocation		= '01';
      $istorelocationbin= '00';
			$icustomer		= $this->input->post('icustomer', TRUE);
			$ecustomername	= $this->input->post('ecustomername', TRUE);
			$jml		= $this->input->post('jml', TRUE);
			if($dbbk!='' && $ecustomername!='')
			{
				$this->db->trans_begin();
				$this->load->model('bbkhadiah/mmaster');
				$this->mmaster->updateheader($ibbk, $dbbk, $icustomer, $eremark);
				for($i=1;$i<=$jml;$i++){
				  $iproduct		  	= $this->input->post('iproduct'.$i, TRUE);
				  $iproductgrade	= 'A';
				  $iproductmotif	= $this->input->post('motif'.$i, TRUE);
				  $eproductname		= $this->input->post('eproductname'.$i, TRUE);
				  $vunitprice	  	= $this->input->post('vunitprice'.$i, TRUE);
				  $vunitprice 		= str_replace(',','',$vunitprice);
				  $nquantity 			= $this->input->post('nquantity'.$i, TRUE);
				  $nquantityx			= $this->input->post('nquantityx'.$i, TRUE);
				  $eremark		  	= $this->input->post('eremark'.$i, TRUE);
          if($nquantityx>0){
  				  $this->mmaster->deletedetail( $iproduct,$iproductgrade,$ibbk,$iproductmotif,$nquantityx, $istore,$istorelocation,$istorelocationbin);
          }
				  if($nquantity>0){
				    $this->mmaster->insertdetail( $ibbk,$ibbktype,$iproduct,$iproductmotif,$iproductgrade,$eproductname,$nquantity,$vunitprice,$eremark,$i,$thbl);
############
            $trans=$this->mmaster->lasttrans($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
            if(isset($trans)){
              foreach($trans as $itrans)
              {
                $q_aw =$itrans->n_quantity_awal;
                $q_ak =$itrans->n_quantity_akhir;
                $q_in =$itrans->n_quantity_in;
                $q_out=$itrans->n_quantity_out;
                break;
              }
            }else{
              $trans=$this->mmaster->qic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin);
              if(isset($trans)){
                foreach($trans as $itrans)
                {
                  $q_aw =$itrans->n_quantity_stock;
                  $q_ak =$itrans->n_quantity_stock;
                  $q_in =0;
                  $q_out=0;
                  break;
                }
              }else{
                $q_aw=0;
                $q_ak=0;
                $q_in=0;
                $q_out=0;
              }
            }
            $this->mmaster->inserttransbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$ibbk,$q_in,$q_out,$nquantity,$q_aw,$q_ak);
            $th=substr($dbbk,0,4);
            $bl=substr($dbbk,5,2);
            $emutasiperiode=$th.$bl;
            if($this->mmaster->cekmutasi($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$emutasiperiode))
            {
              $this->mmaster->updatemutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
            }else{
              $this->mmaster->insertmutasibbkelse($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$emutasiperiode);
            }
            if($this->mmaster->cekic($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin))
            {
              $this->mmaster->updateicbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$nquantity,$q_ak);
            }else{
              $this->mmaster->inserticbbk($iproduct,$iproductgrade,$iproductmotif,$istore,$istorelocation,$istorelocationbin,$eproductname,$nquantity);
            }
############
				  }
				}
				if ( ($this->db->trans_status() === FALSE) )
				{
			    $this->db->trans_rollback();
				}else{
  		    $this->db->trans_commit();
#			    $this->db->trans_rollback();

		      $sess=$this->session->userdata('session_id');
		      $id=$this->session->userdata('user_id');
		      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
		      $rs		= pg_query($sql);
		      if(pg_num_rows($rs)>0){
			      while($row=pg_fetch_assoc($rs)){
				      $ip_address	  = $row['ip_address'];
				      break;
			      }
		      }else{
			      $ip_address='kosong';
		      }
		      $query 	= pg_query("SELECT current_timestamp as c");
		      while($row=pg_fetch_assoc($query)){
			      $now	  = $row['c'];
		      }
		      $pesan='Update BBK-Hadiah No:'.$ibbk;
		      $this->load->model('logger');
		      $this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $ibbk;
					$this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function delete()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbk	= $this->input->post('ibbkdelete', TRUE);
			$this->load->model('bbkhadiah/mmaster');
			$this->mmaster->delete($ibbk);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
	      $now	  = $row['c'];
      }
      $pesan='Delete BBK-Hadiah No:'.$ibbk;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan );

			$data['page_title'] = $this->lang->line('bbkhadiah');
			$data['ibbk']='';
			$data['jmlitem']='';
			$data['detail']='';
			$data['isi']=$this->mmaster->bacasemua();
			$this->load->view('bbkhadiah/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$ibbk			= $this->input->post('ibbkdelete', TRUE);
			$iproduct		= $this->input->post('iproductdelete', TRUE);
			$iproductgrade	= $this->input->post('iproductgradedelete', TRUE);
			$iproductmotif	= $this->input->post('iproductmotifdelete', TRUE);
			$iarea = $this->input->post('iareadel', TRUE);
			$dfrom= $this->input->post('dfrom', TRUE);
			$dto 	= $this->input->post('dto', TRUE);

			$this->db->trans_begin();
			$this->load->model('bbkhadiah/mmaster');
			$this->mmaster->deletedetail($iproduct, $iproductgrade, $ibbk, $iproductmotif);
			if ($this->db->trans_status() === FALSE)
			{
			  $this->db->trans_rollback();
			}else{
			  $this->db->trans_commit();

	      $sess=$this->session->userdata('session_id');
	      $id=$this->session->userdata('user_id');
	      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
	      $rs		= pg_query($sql);
	      if(pg_num_rows($rs)>0){
		      while($row=pg_fetch_assoc($rs)){
			      $ip_address	  = $row['ip_address'];
			      break;
		      }
	      }else{
		      $ip_address='kosong';
	      }
	      $query 	= pg_query("SELECT current_timestamp as c");
	      while($row=pg_fetch_assoc($query)){
		      $now	  = $row['c'];
	      }
	      $pesan='Delete Item BBK-Hadiah No:'.$ibbk;
	      $this->load->model('logger');
	      $this->logger->write($id, $ip_address, $now , $pesan );

			  $data['ibbk'] = $ibbk;
			  $data['iarea'] = $iarea;
			  $data['dfrom'] = $dfrom;
			  $data['dto']	 = $dto;
			  $query = $this->db->query("select * from tm_spmb_item where i_spmb = '$ibbk'");
			  $data['jmlitem'] = $query->num_rows(); 				
			  $this->load->model('bbkhadiah/mmaster');
			  $data['isi']=$this->mmaster->baca($ibbk);
			  $data['detail']=$this->mmaster->bacadetail($ibbk);
	   		$this->load->view('bbkhadiah/vmainform',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function product()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari=strtoupper($this->input->post("cari"));
      $baris=$this->input->post("baris");
			if($baris=='')$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bbkhadiah/cform/product/'.$baris.'/';
			$query = $this->db->query(" select a.i_product
										              from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product and (upper(a.i_product) like '%$cari%' 
                                  or upper(c.e_product_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bbkhadiah/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['baris']=$baris;
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('bbkhadiah/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproduct()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bbkhadiah/cform/product/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
								c.e_product_name as nama,c.v_product_retail as harga
								from tr_product_motif a,tr_product c
								where a.i_product=c.i_product
								and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc ", false);
#c.v_product_mill as harga
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('bbkhadiah/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('bbkhadiah/vlistproduct', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
      $cari=strtoupper($this->input->post("cari"));
      if($cari==''){
        if($this->uri->segment(4)!='index'){
          $cari=$this->uri->segment(4);
          $config['base_url'] = base_url().'index.php/bbkhadiah/cform/customer/'.$cari.'/';
        }else{
    			$config['base_url'] = base_url().'index.php/bbkhadiah/cform/customer/index/';
        }
      }else{
        $config['base_url'] = base_url().'index.php/bbkhadiah/cform/customer/'.$cari.'/';
      }
			$query = $this->db->query("select i_customer from tr_customer 
                                 where (upper(i_customer) like '%$cari%' or upper(e_customer_name) like '%$cari%') ",false);				
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
      $data['cari']=$cari;
			$this->load->model('bbkhadiah/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($config['per_page'],$this->uri->segment(5),$cari);
			$this->load->view('bbkhadiah/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$config['base_url'] = base_url().'index.php/bbkhadiah/cform/area/index/';
			$cari = strtoupper($this->input->post('cari', FALSE));
			$allarea	= $this->session->userdata('allarea');
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
			if($allarea=='t') {
				$query = $this->db->query("select * from tr_area ",false);
			}elseif($allarea=='f' && ($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00') ){
				$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$area1%' or upper(i_area) like '%$area2%' or upper(i_area) like '%$area3%'
						or upper(i_area) like '%$area4%' or upper(i_area) like '%$area5%') ",false);				
			} else {
				$query = $this->db->query("select * from tr_area where i_area='$area1' or i_area='$area2' or i_area='$area3'
						or i_area='$area4' or i_area='$area5' ",false);				
			}				
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbkhadiah/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5,$allarea);
			$this->load->view('bbkhadiah/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function cari()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$config['base_url'] = base_url().'index.php/bbkhadiah/cform/index/';
			$query = $this->db->query("select * from tm_spmb
						   where upper(i_spmb) like '%$cari%' ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(4);
			$this->pagination->initialize($config);
			$this->load->model('bbkhadiah/mmaster');
			$data['isi']=$this->mmaster->cari($cari,$config['per_page'],$this->uri->segment(5));
			$data['page_title'] = $this->lang->line('trans_spmb');
			$data['ibbk']='';
			$data['jmlitem']='';
			$data['detail']='';
	 		$this->load->view('bbkhadiah/vmainform',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function productupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
#			$data['baris']=$this->uri->segment(4);
#			$baris=$this->uri->segment(4);
      $cari=strtoupper($this->input->post("cari"));
      $baris=$this->input->post("baris");
      $peraw=$this->input->post("peraw");
      $perak=$this->input->post("perak");
      $area =$this->input->post("area");
			if($baris=='')$baris=$this->uri->segment(4);
      if($peraw=='')$peraw=$this->uri->segment(5);
      if($perak=='')$perak=$this->uri->segment(6);
      if($area=='')$area=$this->uri->segment(7);
			$config['base_url'] = base_url().'index.php/bbkhadiah/cform/productupdate/'.$baris.'/'.$peraw.'/'.$perak.'/'.$area.'/';

#			$config['base_url'] = base_url().'index.php/bbkhadiah/cform/productupdate/'.$baris.'/index/';
			$query = $this->db->query(" select a.i_product from tr_product_motif a,tr_product c
										              where a.i_product=c.i_product",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);

			$this->load->model('bbkhadiah/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->bacaproduct($config['per_page'],$this->uri->segment(8),$peraw,$perak,$cari);
			$data['baris']=$baris;
      $data['peraw']=$peraw;
      $data['perak']=$perak;
      $data['area']=$area;
			$this->load->view('bbkhadiah/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariproductupdate()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['baris']=$this->uri->segment(4);
			$baris=$this->uri->segment(4);
			$config['base_url'] = base_url().'index.php/bbkhadiah/cform/productupdate/'.$baris.'/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari=strtoupper($cari);
			$query = $this->db->query("	select a.i_product||a.i_product_motif as kode, 
										c.e_product_name as nama,c.v_product_retail as harga
										from tr_product_motif a,tr_product c
										where a.i_product=c.i_product
										and (upper(a.i_product) like '%$cari%' or upper(c.e_product_name) like '%$cari%') order by c.e_product_name asc "
									  ,false);
# c.v_product_mill as harga
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(6);
			$this->pagination->initialize($config);
			$this->load->model('bbkhadiah/mmaster');
			$data['page_title'] = $this->lang->line('list_product');
			$data['isi']=$this->mmaster->cariproduct($cari,$config['per_page'],$this->uri->segment(6));
			$data['baris']=$baris;
			$this->load->view('bbkhadiah/vlistproductupdate', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function store()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/bbkhadiah/cform/store/index/';
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store");
			} else {
				$query = $this->db->query("select distinct(c.i_store) from tr_store_location a, tr_store b, tr_area c
                                  where a.i_store = b.i_store and b.i_store=c.i_store
                                  and (c.i_area = '$area1' or c.i_area = '$area2' or
                                  c.i_area = '$area3' or c.i_area = '$area4' or
                                  c.i_area = '$area5')");				
			}
			$config['total_rows'] = $query->num_rows(); 	
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('bbkhadiah/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->bacastore($config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('bbkhadiah/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function caristore()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu349')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/bbkhadiah/cform/store/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$area1	= $this->session->userdata('i_area');
			$area2	= $this->session->userdata('i_area2');
			$area3	= $this->session->userdata('i_area3');
			$area4	= $this->session->userdata('i_area4');
			$area5	= $this->session->userdata('i_area5');
      if($area1=='00' || $area2=='00' || $area3=='00' || $area4=='00' || $area5=='00'){
        $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')",false);
      }else{
			  $query = $this->db->query(" select distinct(c.i_store) from tr_store a, tr_store_location b, tr_area c
                                    where a.i_store=b.i_store and b.i_store=c.i_store
                                    and(  upper(a.i_store) like '%$cari%' 
                                    or upper(a.e_store_name) like '%$cari%'
                                    or upper(b.i_store_location) like '%$cari%'
                                    or upper(b.e_store_locationname) like '%$cari%')
                                    and (c.i_area = '$area1' or c.i_area = '$area2' or
                                    c.i_area = '$area3' or c.i_area = '$area4' or
                                    c.i_area = '$area5')",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('bbkhadiah/mmaster');
			$data['page_title'] = $this->lang->line('list_store');
			$data['isi']=$this->mmaster->caristore($cari,$config['per_page'],$this->uri->segment(5),$area1,$area2,$area3,$area4,$area5);
			$this->load->view('bbkhadiah/vliststore', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
