<?php 
class Cform extends CI_Controller
{
   function __construct()
   {
      parent::__construct();
      $this->load->library('pagination');
      require_once("php/fungsi.php");
   }

   function index()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu303')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
      {
         $data['page_title'] = $this->lang->line('exp-lebihbayar');
         $this->load->view('exp-lebihbayar/vmainform', $data);
      }
      else
      {
         $this->load->view('awal/index.php');
      }
   }

   function export()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu303')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         )
      {
          $this->load->model('exp-lebihbayar/mmaster');
          $iarea= $this->input->post('iarea');
		      $data['page_title'] = $this->lang->line('exp-lebihbayar');
			    $qareaname	= $this->mmaster->eareaname($iarea);
			    if($qareaname->num_rows()>0) {
				    $row_areaname	= $qareaname->row();
				    $aname	= $row_areaname->e_area_name;
			    } else {
				    $aname	= '';
			    }
		      $this->db->select("	a.*, b.e_customer_name, c.e_jenis_bayarname
                              from tr_customer b, tm_pelunasan_lebih a
                              left join tr_jenis_bayar c on(a.i_jenis_bayar=c.i_jenis_bayar)
                              where a.i_customer=b.i_customer
                              and a.f_pelunasan_cancel='f' and a.v_lebih>0
                              and a.f_giro_tolak='f' and a.f_giro_batal='f'
                              and a.i_area='$iarea' order by a.i_pelunasan",false);
	          $query = $this->db->get();
		        $this->load->library('PHPExcel');
		        $this->load->library('PHPExcel/IOFactory');
		        $objPHPExcel = new PHPExcel();
		        $objPHPExcel->getProperties()->setTitle("Daftar Pembayaran Lebih")->setDescription(NmPerusahaan);
		        $objPHPExcel->setActiveSheetIndex(0);
		        if ($query->num_rows() > 0){
			        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			        array(
				        'font' => array(
					        'name'	=> 'Arial',
					        'bold'  => true,
					        'italic'=> false,
					        'size'  => 10
				        ),
				        'alignment' => array(
					        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
					        'vertical'  => Style_Alignment::VERTICAL_CENTER,
					        'wrap'      => true
				        )
			        ),
			        'A2:A4'
			        );
			        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
			        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
			        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
			        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);

			        $objPHPExcel->getActiveSheet()->setCellValue('A2', 'Daftar Pembayaran Lebih');
			        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,9,2);
			        $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Area : '.$aname);
			        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,9,3);

			        $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			        array(
				        'font' => array(
					        'name'	=> 'Arial',
					        'bold'  => true,
					        'italic'=> false,
					        'size'  => 10
				        ),
				        'alignment' => array(
					        'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
					        'vertical'  => Style_Alignment::VERTICAL_CENTER,
					        'wrap'      => true
				        )
			        ),
			        'A5:G5'
			        );


			        $objPHPExcel->getActiveSheet()->setCellValue('A5', 'KdLang');
			        $objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
				        array(
					        'borders' => array(
						        'top' 	=> array('style' => Style_Border::BORDER_THIN)
					        )

				        )
			        );

              $objPHPExcel->getActiveSheet()->setCellValue('B5', 'Nama Cust');
			        $objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
				        array(
					        'borders' => array(
						        'top' 	=> array('style' => Style_Border::BORDER_THIN)
					        ),
				        )
			        );

			        $objPHPExcel->getActiveSheet()->setCellValue('C5', 'No Bukti');
			        $objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
				        array(
					        'borders' => array(
						        'top' 	=> array('style' => Style_Border::BORDER_THIN)
					        ),
				        )
			        );
			        $objPHPExcel->getActiveSheet()->setCellValue('D5', 'Tgl Bukti');
			        $objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
				        array(
					        'borders' => array(
						        'top' 	=> array('style' => Style_Border::BORDER_THIN)
					        ),
				        )
			        );

			        $objPHPExcel->getActiveSheet()->setCellValue('E5', 'Jumlah');
			        $objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
				        array(
					        'borders' => array(
						        'top' 	=> array('style' => Style_Border::BORDER_THIN)
					        ),
				        )
			        );

			        $objPHPExcel->getActiveSheet()->setCellValue('F5', 'Lebih Bayar');
			        $objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
				        array(
					        'borders' => array(
						        'top' 	=> array('style' => Style_Border::BORDER_THIN)
					        ),
				        )
			        );
			        $objPHPExcel->getActiveSheet()->setCellValue('G5', 'Keterangan');
			        $objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
				        array(
					        'borders' => array(
						        'top' 	=> array('style' => Style_Border::BORDER_THIN)
					        )
				        )
			        );

			        $i=6;
			        $j=6;
              $no=0;
             
			        foreach($query->result() as $row){
                $no++;
                $objPHPExcel->getActiveSheet()->duplicateStyleArray(
			          array(
				          'font' => array(
					          'name'	=> 'Arial',
					          'bold'  => false,
					          'italic'=> false,
					          'size'  => 10
				          )
			          ),
			          'A'.$i.':G'.$i
			          );
				      if($row->d_bukti!=''){
					      $tmp=explode("-",$row->d_bukti);
					      $th=$tmp[0];
					      $bl=$tmp[1];
					      $hr=$tmp[2];
					      $row->d_bukti=$hr."-".$bl."-".$th;
				      }

                $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_customer, Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $row->e_customer_name, Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $row->i_pelunasan, Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $row->d_bukti, Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $row->v_jumlah, Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $row->v_lebih, Cell_DataType::TYPE_STRING);
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $row->e_jenis_bayarname, Cell_DataType::TYPE_STRING);
          
				        $i++;
				        $j++;
			        }
			        $x=$i-1;
                $objPHPExcel->getActiveSheet()->getStyle('E5:F'.$x)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_CURRENCY_IDR);
		        }
		        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
            $nama='lebihbayar-'.$iarea.'.xls';
		        $objWriter->save('excel/'.$iarea.'/'.$nama); 

			      $sess=$this->session->userdata('session_id');
			      $id=$this->session->userdata('user_id');
			      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			      $rs		= pg_query($sql);
			      if(pg_num_rows($rs)>0){
				      while($row=pg_fetch_assoc($rs)){
					      $ip_address	  = $row['ip_address'];
					      break;
				      }
			      }else{
				      $ip_address='kosong';
			      }
			      $query 	= pg_query("SELECT current_timestamp as c");
	          while($row=pg_fetch_assoc($query)){
	          	$now	  = $row['c'];
			      }
			      $pesan='Export Lebih Bayar Area:'.$iarea;
			      $this->load->model('logger');
			      $this->logger->write($id, $ip_address, $now , $pesan ); 

		        $data['sukses'] = true;
		        $data['inomor']	= "Lebih Bayar - $iarea";
		        $this->load->view('nomor',$data);
	        }else{
		        $this->load->view('awal/index.php');
	        }
        }
   
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu303')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-lebihbayar/cform/area/index/';
      $iuser   = $this->session->userdata('user_id');
      $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') order by i_area", false);

			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-lebihbayar/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('exp-lebihbayar/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu303')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-lebihbayar/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
      $iuser   = $this->session->userdata('user_id');
			$query = $this->db->query("select * from tr_area where (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%') 
												and (i_area in ( select i_area from tm_user_area where i_user='$iuser') )",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-lebihbayar/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari,$config['per_page'],$this->uri->segment(5),$iuser);
			$this->load->view('exp-lebihbayar/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
