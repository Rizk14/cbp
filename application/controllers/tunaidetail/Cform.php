<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('tunaidetail');
			$this->load->model('tunaidetail/mmaster');
			$data['ikum']='';
			$this->load->view('tunaidetail/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function insert_fail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$data['page_title'] = $this->lang->line('tunaidetail');
			$this->load->view('tunaidetail/vinsert_fail',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function edit()
	{
		if (($this->session->userdata('logged_in'))
		){ 	
			$data['page_title'] = $this->lang->line('tunaidetail')." updatee";
			if(
				($this->uri->segment(4)!='')
			  ){
        $itunai          = $this->uri->segment(4);
				$iarea		      = $this->uri->segment(5);
				$dfrom		      = $this->uri->segment(6);
				$dto			      = $this->uri->segment(7);
				$data['itunai']     = $itunai;
				$data['iarea']		= $iarea;
				$data['dfrom']		= $dfrom;
				$data['dto']		= $dto;
        $data['pst']	= $this->session->userdata('i_area');
				$this->load->model("tunaidetail/mmaster");
				$data['isi']=$this->mmaster->baca($iarea,$itunai);
				$data['detail']=$this->mmaster->bacadetail($iarea,$itunai);
########
				$query  = $this->mmaster->baca($iarea,$itunai);
			  $dtunai=substr($query->d_tunai,0,4).substr($query->d_tunai,5,2);
				$data['bisaedit']=false;
				$query=$this->db->query("	select i_periode from tm_periode ",false);
			  if ($query->num_rows() > 0){
				  foreach($query->result() as $rw){
            $periode=$rw->i_periode;
				  }
				  if($periode<=$dtunai)$data['bisaedit']=true;
			  }
########
		 		$this->load->view('tunaidetail/vformupdate',$data);
			}else{
				$this->load->view('tunaidetail/vinsert_fail',$data);
			}
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function update()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		  $itunai  = $this->input->post('itunai', TRUE);
		   $dtunai	= $this->input->post('dtunai', TRUE);
			if($dtunai!=''){
				$tmp=explode("-",$dtunai);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dtunai=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
				$tahun=$th;
			}
			$iarea			      = $this->input->post('iarea', TRUE);
//			$inota			      = $this->input->post('inota', TRUE);
			$eremark      		= $this->input->post('eremark', TRUE);
			$vjumlah      		= $this->input->post('vjumlah', TRUE);
			$vjumlah      		= str_replace(',','',$vjumlah);
			$jml          		= $this->input->post('jml', TRUE);

			{
				$this->db->trans_begin();
				$this->load->model('tunaidetail/mmaster');
				$this->mmaster->update($itunai,$dtunai,$iarea,$eremark,$vjumlah);
				
				for($i=1;$i<=$jml;$i++){
					  $iarea		= $this->input->post('iarea'.$i, TRUE);
					  $inota       = $this->input->post('inota'.$i, TRUE);
					  $vjumlah		= $this->input->post('vjumlah'.$i, TRUE);
					  $vjumlah		= str_replace(',','',$vjumlah);
					  if($inota<>''){
  					  $this->mmaster->updatedetail($itunai,$iarea,$inota,$vjumlah,$i);
  					}
				//  $this->mmaster->updatetunai($irtunai,$iarea,$itunai,$iareatunai);
				}		
				if( ($this->db->trans_status() === FALSE) ) #&& ($cek) )
				{
					$this->db->trans_rollback();
				}else{
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input Setor Tunai Area '.$iarea.' No:'.$itunai;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $itunai;
					$this->db->trans_commit();
#					$this->db->trans_rollback();
				  $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function simpan()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
			$dtunai	= $this->input->post('dtunai', TRUE);
			if($dtunai!=''){
				$tmp=explode("-",$dtunai);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dtunai=$th."-".$bl."-".$hr;
			}
			$esalesmanname		= $this->input->post('esalesmanname', TRUE);
			$isalesman		    = $this->input->post('isalesman', TRUE);
			$iarea			      = $this->input->post('iarea', TRUE);
			$eareaname		    = $this->input->post('eareaname', TRUE);
			$icustomer    		= $this->input->post('icustomer', TRUE);
			$icustomergroupar	= $this->input->post('icustomergroupar', TRUE);
			$ecustomername		= $this->input->post('ecustomername', TRUE);
			$xeremark      		= $this->input->post('xeremark', TRUE);
			$vjumlah      		= $this->input->post('vjumlah', TRUE);
			$vjumlah      		  = str_replace(',','',$vjumlah);
			$jml          		  = $this->input->post('jml', TRUE);
			$jml          		  = str_replace(',','',$jml);
			$dtunai	= $this->input->post('dtunai', TRUE);
			if($dtunai!=''){
				$tmp=explode("-",$dtunai);
				$th=$tmp[2];
				$bl=$tmp[1];
				$hr=$tmp[0];
				$dtunai=$th."-".$bl."-".$hr;
				$thbl=$th.$bl;
				$tahun=$th;
			}
			if (
				($dtunai != '') && ($iarea!='') && ($vjumlah!='') && ($vjumlah!='0'))
			{
				$this->load->model('tunaidetail/mmaster');
				$this->db->trans_begin();
			    $itunai = $this->mmaster->runningnumber($iarea,$thbl);
				$this->mmaster->insert($itunai,$dtunai,$iarea,$icustomer,$icustomergroupar,$isalesman,$xeremark,$vjumlah,$vjumlah);
				for($i=1;$i<=$jml;$i++){
				  $iarea		= $this->input->post('iarea'.$i, TRUE);
				  $inota		= $this->input->post('inota'.$i, TRUE);
				  $vjumlah		= $this->input->post('vjumlah'.$i, TRUE);
				  $vjumlah		= str_replace(',','',$vjumlah);
				  $this->mmaster->insertdetail($itunai,$iarea,$inota,$vjumlah,$i);
				//  $this->mmaster->updatetunai($irtunai,$iarea,$itunai,$iareatunai);
				}	
				if( ($this->db->trans_status() === FALSE) && ($cek) )
				{
					$this->db->trans_rollback();
				}else{
					$sess=$this->session->userdata('session_id');
					$id=$this->session->userdata('user_id');
					$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
					$rs		= pg_query($sql);
					if(pg_num_rows($rs)>0){
						while($row=pg_fetch_assoc($rs)){
							$ip_address	  = $row['ip_address'];
							break;
						}
					}else{
						$ip_address='kosong';
					}
					$query 	= pg_query("SELECT current_timestamp as c");
					while($row=pg_fetch_assoc($query)){
						$now	  = $row['c'];
					}
					$pesan='Input Tunai Area '.$iarea.' No:'.$itunai;
					$this->load->model('logger');
					$this->logger->write($id, $ip_address, $now , $pesan );

					$data['sukses']			= true;
					$data['inomor']			= $itunai;
					$this->db->trans_commit();
#					$this->db->trans_rollback();
				  $this->load->view('nomor',$data);
				}
			}
		}else{
			$this->load->view('awal/index.php');

		}
	}
	function area()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/tunaidetail/cform/area/index/';
			$config['per_page'] = '10';
			$area	= $this->session->userdata('i_area');
			$iuser   = $this->session->userdata('user_id');
			if($area=='00'){
  			$query = $this->db->query("	select * from tr_area ",false);
  	  }else{
  	    $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser')", false);
  	  }
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('tunaidetail/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->bacaarea($config['per_page'],$this->uri->segment(5));
			$this->load->view('tunaidetail/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function cariarea()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/tunaidetail/cform/area/index/';
			$cari 	= $this->input->post('cari', FALSE);
			$cari	= strtoupper($cari);
			$area	= $this->session->userdata('i_area');
			$iuser   = $this->session->userdata('user_id');
			if($area=='00'){
			  $query = $this->db->query("select * from tr_area
									     	where upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%' ",false);
      }else{
  	    $query = $this->db->query(" select * from tr_area where i_area in ( select i_area from tm_user_area where i_user='$iuser') 
  	                                and (upper(i_area) like '%$cari%' or upper(e_area_name) like '%$cari%')", false);
  	  }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('tunaidetail/mmaster');
			$data['page_title'] = $this->lang->line('list_area');
			$data['isi']=$this->mmaster->cariarea($cari, $config['per_page'],$this->uri->segment(5));
			$this->load->view('tunaidetail/vlistarea', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

 function salesman()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu491')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
          $iarea = strtoupper($this->input->post('iarea', FALSE));
          $dtunai  = strtoupper($this->input->post('dtunai', FALSE));
          $icustomer  = strtoupper($this->input->post('icustomer', FALSE));
          if($iarea=='') $iarea=$this->uri->segment(4);
          if($dtunai=='') $dtunai=$this->uri->segment(5);
          if($icustomer=='') $icustomer=$this->uri->segment(6);
          $per='';
          if($dtunai!=''){
            $tmp=explode('-',$dtunai);
            $yy=$tmp[2];
            $bl=$tmp[1];
            $per=$yy.$bl;
          }

         $config['base_url'] = base_url().'index.php/tunaidetail/cform/salesman/'.$iarea.'/'.$dtunai.'/'.$icustomer.'/';
         $area1   = $this->session->userdata('i_area');
         $area2   = $this->session->userdata('i_area2');
         $area3   = $this->session->userdata('i_area3');
         $area4   = $this->session->userdata('i_area4');
         $area5   = $this->session->userdata('i_area5');
         $cari = strtoupper($this->input->post('cari', FALSE));
         $query = $this->db->query("select distinct a.i_salesman, a.e_salesman_name from tr_customer_salesman a, tr_salesman b
                                    where (upper(a.e_salesman_name) like '%$cari%' or upper(a.i_salesman) like '%$cari%')
                                    and a.i_area='$iarea' and a.i_salesman = b.i_salesman
                                    and b.f_salesman_aktif='true' and a.e_periode='$per'",false);

         $config['total_rows'] = $query->num_rows();
         $config['per_page'] = '10';
         $config['first_link'] = 'Awal';
         $config['last_link'] = 'Akhir';
         $config['next_link'] = 'Selanjutnya';
         $config['prev_link'] = 'Sebelumnya';
         $config['cur_page'] = $this->uri->segment(6);
         $this->pagination->initialize($config);
         $data['iarea']=$iarea;
         $data['dtunai']=$dtunai;
         $data['icustomer']=$icustomer;
         $this->load->model('tunaidetail/mmaster');
         $data['page_title'] = $this->lang->line('list_salesman');
         $data['isi']=$this->mmaster->bacasalesman($iarea,$per,$cari,$area1,$area2,$area3,$area4,$area5,$config['per_page'],$this->uri->segment(7));
         $this->load->view('tunaidetail/vlistsalesman', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }



	function bank()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/tunaidetail/cform/bank/index/';
			$config['per_page'] = '10';
 			$query = $this->db->query("	select * from tr_bank ",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('tunaidetail/mmaster');
			$data['page_title'] = $this->lang->line('list_bank');
			$data['isi']=$this->mmaster->bacabank($config['per_page'],$this->uri->segment(5));
			$this->load->view('tunaidetail/vlistbank', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}

	function customer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu491')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
		  $iarea = $this->input->post('iarea');
			if($iarea=='') $iarea 	= $this->uri->segment(4);
		  $tgl = $this->input->post('tgl');
			if($tgl=='') $tgl = $this->uri->segment(5);
			$tmp=explode('-',$tgl);
			$yy=$tmp[2];
			$mm=$tmp[1];
			$per=$yy.$mm;
		  $cari = strtoupper($this->input->post('cari'));
		  if($cari=='' && $this->uri->segment(6)=='sikasep'){
		    $cari = '';
		  }elseif($cari==''){
		    $cari=$this->uri->segment(6);
		  }
		  if($cari==''){
	  		$config['base_url'] = base_url().'index.php/tunaidetail/cform/customer/'.$iarea.'/'.$tgl.'/sikasep/';
      }else{			
  			$config['base_url'] = base_url().'index.php/tunaidetail/cform/customer/'.$iarea.'/'.$tgl.'/'.$cari.'/';
      }
			$config['per_page'] = '10';
			$query = $this->db->query("	select distinct on (a.i_customer, a.e_customer_name, c.e_salesman_name) a.*, b.i_customer_groupar, 
			              c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
										left join tr_customer_groupar b on(a.i_customer=b.i_customer)
										left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area and c.e_periode='$per')
										left join tr_customer_owner d on(a.i_customer=d.i_customer)
										where a.i_area = '$iarea'
										and (upper(a.i_customer) like '%$cari%' or upper(a.e_customer_name) like '%$cari%' or upper(c.e_salesman_name) like '%$cari%')",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(7);
			$this->pagination->initialize($config);

			$this->load->model('tunaidetail/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
			$data['isi']=$this->mmaster->bacacustomer($iarea,$per,$cari,$config['per_page'],$this->uri->segment(7));
			$data['iarea']=$iarea;
      $data['cari']=$cari;
      $data['tgl']=$tgl;
			$this->load->view('tunaidetail/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	
	function caricustomer()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu490')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$iarea 	= $this->uri->segment(4);
			$cari 	= strtoupper($this->input->post('cari', FALSE));
      if( ($cari=='') && ($this->uri->segment(6)!='') )$cari=$this->uri->segment(5);
      $cari=str_replace("%20"," ",$cari);
      if($cari!='')
  			$config['base_url'] = base_url().'index.php/tunaidetail/cform/caricustomer/'.$iarea.'/'.$cari.'/';
      else
  			$config['base_url'] = base_url().'index.php/tunaidetail/cform/caricustomer/'.$iarea.'/';
			$config['per_page'] = '10';
			$query = $this->db->query("	select a.*, b.i_customer_groupar, c.e_salesman_name, c.i_salesman, d.e_customer_setor from tr_customer a
								left join tr_customer_groupar b on(a.i_customer=b.i_customer)
								left join tr_customer_salesman c on(a.i_customer=c.i_customer and a.i_area=c.i_area)
								left join tr_customer_owner d on(a.i_customer=d.i_customer)

								where a.i_area='$iarea' and ( 
									upper(a.i_customer) like '%$cari%' or 
									upper(a.e_customer_name) like '%$cari%' or 
									upper(d.e_customer_setor) like '%$cari%' )",false);
			$config['total_rows'] = $query->num_rows();
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
      if($cari!='')
  			$config['cur_page'] = $this->uri->segment(6);
      else
   			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('tunaidetail/mmaster');
			$data['page_title'] = $this->lang->line('list_customer');
      if($cari!='')
  			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(6));
      else
  			$data['isi']=$this->mmaster->caricustomer($cari,$iarea,$config['per_page'],$this->uri->segment(5));
			$data['iarea']=$iarea;
      $data['cari']=$cari;
			$this->load->view('tunaidetail/vlistcustomer', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function nota()
	{
	if (
		(($this->session->userdata('logged_in')) &&
		($this->session->userdata('menu490')=='t')) ||
		(($this->session->userdata('logged_in')) &&
		($this->session->userdata('allmenu')=='t'))
		){
      $baris 				=$this->input->post('baris', FALSE);
      $area 				=$this->input->post('area', FALSE);
      $icustomer 			=$this->input->post('icustomer', FALSE);
      $dtunai 				=$this->input->post('dtunai', FALSE);
	  if($baris=='') 		$baris=$this->uri->segment(4);
	  $data['baris']=$baris;
      if($area=='')			$area=$this->uri->segment(5);
      if($dtunai=='')		$dtunai=$this->uri->segment(6);
      if($icustomer=='')	$icustomer=$this->uri->segment(7);
      
      $tmp=explode("-",$dtunai);
      $dd=$tmp[0];
      $mm=$tmp[1];
      $yy=$tmp[2];
      $dtunaix=$yy.'-'.$mm.'-'.$dd;

      $areax1 = $this->session->userdata('i_area');
	  $areax2	= $this->session->userdata('i_area2');
	  $areax3	= $this->session->userdata('i_area3');
	  $areax4	= $this->session->userdata('i_area4');
	  $areax5	= $this->session->userdata('i_area5');
      $cari 	= strtoupper($this->input->post('cari', FALSE));
			$config['base_url'] = base_url().'index.php/tunaidetail/cform/nota/'.$baris.'/'.$area.'/'.$dtunai.'/'.$icustomer.'/';
      if($areax1=='00'){
		    $query = $this->db->query("select a.i_nota,a.d_nota, a.v_sisa, a.i_area, b.e_area_name, a.i_customer, c.e_customer_name, a.e_remark  
		    						   from tm_nota a 
									   left join tr_area b using(i_area) 
									   left join tr_customer c using (i_customer) where a.d_nota<='$dtunaix' and a.v_sisa is not null 
									   and a.i_customer='$icustomer' and a.i_area='$area' and a.f_nota_cancel='f'",false);
      }else{
		    $query = $this->db->query(" select a.i_nota,a.d_nota, a.v_sisa, a.i_area, b.e_area_name, a.i_customer, c.e_customer_name , a.e_remark 
		    						   from tm_nota a 
									   left join tr_area b using(i_area) 
									   left join tr_customer c using (i_customer) where a.d_nota<='$dtunaix' and a.v_sisa is not null 
									   and a.i_customer='$icustomer' and a.i_area='$area' and a.f_nota_cancel='f'",false);
      }
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(8);
			$this->pagination->initialize($config);
			$data['area']=$area;
			$data['dtunai']=$dtunai;
			$data['baris']=$baris;
			$data['icustomer']=$icustomer;
			$this->load->model('tunaidetail/mmaster');
			$data['page_title'] = $this->lang->line('nota');
			$data['isi']=$this->mmaster->bacanota($dtunaix,$cari,$area,$config['per_page'],$this->uri->segment(8),$areax1,$areax2,$areax3,$areax4,$areax5,$icustomer);
			$this->load->view('tunaidetail/vlistnota', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}


 function carinota()
   {
      if (
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('menu490')=='t')) ||
         (($this->session->userdata('logged_in')) &&
         ($this->session->userdata('allmenu')=='t'))
         ){
         $cari             = strtoupper($this->input->post('cari', FALSE));
         $baris            = $this->input->post('baris', FALSE)?$this->input->post('baris', FALSE):$this->uri->segment(4);
         $area            = $this->input->post('area', FALSE)?$this->input->post('area', FALSE):$this->uri->segment(5);
         $dtunai 		   = $this->input->post('dtunai', FALSE)?$this->input->post('dtunai', FALSE):$this->uri->segment(6);
         $icustomer        = $this->input->post('icustomer', FALSE)?$this->input->post('icustomer', FALSE):$this->uri->segment(7);
         $data['baris']    = $baris;
         $data['area']    = $area;
         $data['dtunai']    = $dtunai;
         $data['icustomer']= $icustomer;

         $tmp=explode("-",$dtunai);
     	 $dd=$tmp[0];
         $mm=$tmp[1];
         $yy=$tmp[2];
         $dtunaix=$yy.'-'.$mm.'-'.$dd;
		     
         $config['base_url'] = base_url().'index.php/tunaidetail/cform/carinota/'.$baris.'/'.$area.'/'.$dtunai.'/'.$icustomer.'/index/';
      $group='xxx';
         $query = $this->db->query(" select i_customer_groupbayar from tr_customer_groupbayar where i_customer='$icustomer'" ,false);
      foreach($query->result() as $row){
        $group=$row->i_customer_groupbayar;
      }
         $query = $this->db->query(" select a.i_nota,a.d_nota, a.v_sisa, a.i_area, b.e_area_name, a.i_customer, c.e_customer_name, a.e_remark  
         								   from tm_nota a 
									  	   left join tr_area b using(i_area) 
									   	   left join tr_customer c using (i_customer)
									   	   left join tr_customer_groupbayar d on a.i_customer=d.i_customer
                                           where a.d_nota<='$dtunaix' and a.v_sisa is not null and a.v_sisa>0 
             							   and a.i_customer='$icustomer' and a.i_area='$area' and a.f_nota_cancel='f'
                                           and (upper(a.i_nota) like '%$cari%' or upper(a.i_customer) like '%$cari%')" ,false);
         $config['total_rows']   = $query->num_rows();
         $config['per_page']  = '10';
         $config['first_link']   = 'Awal';
         $config['last_link']    = 'Akhir';
         $config['next_link']    = 'Selanjutnya';
         $config['prev_link']    = 'Sebelumnya';
         $config['cur_page']  = $this->uri->segment(10);
         $this->pagination->initialize($config);
         $this->load->model('tunaidetail/mmaster');

         $data['page_title'] = $this->lang->line('list_nota');
         $data['isi']=$this->mmaster->carinota($cari,$area,$dtunaix,$icustomer,$config['per_page'],$this->uri->segment(10),$group);
         $data['baris']=$baris;
         $this->load->view('tunaidetail/vlistnota', $data);
      }else{
         $this->load->view('awal/index.php');
      }
   }

	function deletedetail()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu490')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		){
		  $data['page_title'] = $this->lang->line('tunaidetail')." update";
      		$itunai	  = $this->uri->segment(4);
			$iarea		  = $this->uri->segment(5);
      		$inota 	  = $this->uri->segment(6);
			$dfrom		  = $this->uri->segment(7);
			$dto			  = $this->uri->segment(8);
			$cari = strtoupper($this->input->post('cari', FALSE));
			$this->load->model('tunaidetail/mmaster');
			$this->mmaster->deletedetail($itunai,$iarea,$inota);
			$sess=$this->session->userdata('session_id');
			$id=$this->session->userdata('user_id');
			$sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			$rs		= pg_query($sql);
			if(pg_num_rows($rs)>0){
				while($row=pg_fetch_assoc($rs)){
					$ip_address	  = $row['ip_address'];
					break;
				}
			}else{
				$ip_address='kosong';
			}
			$query 	= pg_query("SELECT current_timestamp as c");
	    while($row=pg_fetch_assoc($query)){
	    	$now	  = $row['c'];
			}
			$pesan='Hapus Item Tunai Item Area '.$iarea.' No Tunai:'.$itunai.'  No Nota:'.$inota;
			$this->load->model('logger');
			$this->logger->write($id, $ip_address, $now , $pesan ); 

			$data['itunai']  = $itunai;
			$data['iarea']		= $iarea;
			$data['dfrom']		= $dfrom;
			$data['dto']		= $dto;
    		$data['pst']	= $this->session->userdata('i_area');
			$data['isi']=$this->mmaster->baca($iarea,$itunai);
			$data['detail']=$this->mmaster->bacadetail($iarea,$itunai);
	 		$this->load->view('tunaidetail/vformupdate',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
