<?php
class Cform extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu334') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$data['page_title'] = $this->lang->line('exp-opdo');
			$data['iperiode']	= '';
			$data['isupplier']	  = '';
			$this->load->view('exp-opdo/vmainform', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu334') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-opdo/cform/supplier/index/';
			$config['total_rows'] = $this->db->count_all('tr_supplier');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-opdo/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi'] = $this->mmaster->bacasupplier($config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-opdo/vlistsupplier', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu334') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$config['base_url'] = base_url() . 'index.php/exp-opdo/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_supplier 
										where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'", false);
			$config['total_rows'] = $query->num_rows();
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-opdo/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi'] = $this->mmaster->carisupplier($cari, $config['per_page'], $this->uri->segment(5));
			$this->load->view('exp-opdo/vlistsupplier', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('menu334') == 't')) ||
			(($this->session->userdata('logged_in')) &&
				($this->session->userdata('allmenu') == 't'))
		) {
			$this->load->model('exp-opdo/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
			$isupplier = $this->input->post('isupplier');
			if ($iperiode == '') {
				$iperiode = $this->uri->segment(4);
			}

			$a = substr($iperiode, 0, 4);
			$b = substr($iperiode, 4, 2);
			$peri = mbulan($b) . " - " . $a;

			if ($isupplier == '') $isupplier = $this->uri->segment(5);
			$qsupname	= $this->mmaster->esupname($isupplier);
			if ($qsupname->num_rows() > 0) {
				$row_supname	= $qsupname->row();
				$sname   = $row_supname->e_supplier_name;
			} else {
				$sname   = '';
			}
			/*$this->db->select("	b.i_product, b.e_product_name, sum(b.n_order) as n_order, sum((b.n_order * b.v_product_mill)) as rpop,
	  case when sum(b.n_delivery) isnull then 0 else sum(b.n_delivery) end as n_delivery, sum((n_delivery * b.v_product_mill)) as rpdo,
	  c.e_supplier_name,
	  case when b.n_delivery isnull then b.n_order else b.n_order-b.n_delivery end as pending from tm_op a 
	  left join tm_op_item b on (a.i_op=b.i_op)
	  inner join tr_supplier c on (a.i_supplier=c.i_supplier)
	  where 
      to_char(a.d_op::timestamp with time zone, 'yyyymm'::text)='$iperiode'
	  and a.f_op_cancel='f'
      and a.i_supplier = '$isupplier' 
	  group by b.i_product, b.e_product_name,c.e_supplier_name, b.n_delivery, b.n_order
	  order by b.i_product asc",false);*/
			$this->db->select("DISTINCT
                a.i_op,
              	a.i_area,
              	a.i_reff,
              	a.d_reff,
              	a.e_op_remark,
              	a.d_op,
              	b.i_product,
              	b.e_product_name,
              	b.v_product_mill,
              	b.n_order,
              	(b.n_order * b.v_product_mill) AS rpop,
              	e.e_supplier_name,
              	CASE WHEN 
              		b.n_delivery isnull THEN 0
              	ELSE
              		b.n_delivery
              	END,
              	(b.n_delivery * b.v_product_mill) AS rpdo,
              	CASE WHEN 
              		b.n_delivery isnull THEN b.n_order
              	ELSE
              		b.n_order - b.n_delivery 
              	END AS pending
              FROM 
              	tm_op a
              	LEFT JOIN tm_op_item b ON (a.i_op = b.i_op)
              	LEFT JOIN tm_do c ON (a.i_op = c.i_op AND a.i_area = c.i_area AND a.i_supplier = c.i_supplier)
              	LEFT JOIN tm_do_item d ON (c.i_do = d.i_do AND b.i_product = d.i_product)
              	INNER JOIN tr_supplier e ON (a.i_supplier = e.i_supplier AND a.i_supplier = c.i_supplier)
              WHERE 
              	to_char(a.d_op::timestamp with time zone, 'yyyymm'::text)='$iperiode'
              	AND to_char(c.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
              	AND a.i_supplier = '$isupplier'
              	AND a.f_op_cancel = 'f'
              	AND c.f_do_cancel = 'f'
              ORDER BY b.i_product", FALSE);

			$query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan OP vs DO ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0) {
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(6);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan OP vs DO');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 2, 9, 2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Bulan : ' . $peri . ' - Supplier : ' . $sname);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 3, 9, 3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
					array(
						'font' => array(
							'name'	=> 'Arial',
							'bold'  => true,
							'italic' => false,
							'size'  => 10
						),
						'alignment' => array(
							'horizontal' => Style_Alignment::HORIZONTAL_LEFT,
							'vertical'  => Style_Alignment::VERTICAL_CENTER,
							'wrap'      => true
						)
					),
					'A5:I5'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Pcs (OP)');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'RP (OP)');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Pcs (DO)');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'RP (DO)');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Pending');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Persen (Pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('I5', 'Persen (Rp)');
				$objPHPExcel->getActiveSheet()->getStyle('I5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

				$i = 6;
				$j = 6;
				$no = 0;
				foreach ($query->result() as $row) {
					$no++;
					$objPHPExcel->getActiveSheet()->duplicateStyleArray(
						array(
							'font' => array(
								'name'	=> 'Arial',
								'bold'  => false,
								'italic' => false,
								'size'  => 10
							)
						),
						'A' . $i . ':I' . $i
					);

					if ($row->n_order != 0 && $row->n_delivery != 0) {
						$persenpcs = ($row->n_delivery / $row->n_order) * 100;
					} else {
						$persenpcs = 0;
					}

					if ($row->rpop != 0 && $row->rpdo != 0) {
						$persenrp = ($row->rpdo / $row->rpop) * 100;
					} else {
						$persenrp = 0;
					}

					$objPHPExcel->getActiveSheet()->setCellValueExplicit('A' . $i, $row->i_product, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $row->e_product_name, Cell_DataType::TYPE_STRING);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('C' . $i, $row->n_order, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('D' . $i, $row->rpop, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('E' . $i, $row->n_delivery, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('F' . $i, $row->rpdo, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('G' . $i, $row->pending, Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('H' . $i, $persenpcs . "%", Cell_DataType::TYPE_NUMERIC);
					$objPHPExcel->getActiveSheet()->setCellValueExplicit('I' . $i, $persenrp . "%", Cell_DataType::TYPE_NUMERIC);

					$i++;
					$j++;
				}
				$x = $i - 1;
				$objPHPExcel->getActiveSheet()->getStyle('H6:I' . $i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
			$nama = 'LapOPvsDO-' . $isupplier . '-' . $iperiode . '.xls';
			$objWriter->save('beli/' . $nama);

			// $sess = $this->session->userdata('session_id');
			// $id = $this->session->userdata('user_id');
			// $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
			// $rs		= pg_query($sql);
			// if (pg_num_rows($rs) > 0) {
			// 	while ($row = pg_fetch_assoc($rs)) {
			// 		$ip_address	  = $row['ip_address'];
			// 		break;
			// 	}
			// } else {
			// 	$ip_address = 'kosong';
			// }
			// $query 	= pg_query("SELECT current_timestamp as c");
			// while ($row = pg_fetch_assoc($query)) {
			// 	$now	  = $row['c'];
			// }
			// $pesan = 'Export OP DO Periode:' . $iperiode . ' Supplier:' . $isupplier;
			// $this->load->model('logger');
			// $this->logger->write($id, $ip_address, $now, $pesan);

			$this->logger->writenew('Export OP DO Periode:' . $iperiode . ' Supplier:' . $isupplier);

			$data['sukses'] = true;
			$data['inomor']	= $nama;
			$data['folder']	= "beli/";

			$this->load->view('nomorurl', $data);
		} else {
			$this->load->view('awal/index.php');
		}
	}
}
