<?php 
class Cform extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('pagination');
		$this->load->library('paginationxx');
		require_once("php/fungsi.php");
	}
	function index()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu334')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$data['page_title'] = $this->lang->line('exp-opdo');
			$data['iperiode']	= '';
			$data['isupplier']	  = '';
			$this->load->view('exp-opdo/vmainform', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
 	function supplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu334')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-opdo/cform/supplier/index/';
			$config['total_rows'] = $this->db->count_all('tr_supplier');
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);

			$this->load->model('exp-opdo/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->bacasupplier($config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-opdo/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function carisupplier()
	{
		if (
			(($this->session->userdata('logged_in')) &&
		    ($this->session->userdata('menu334')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$config['base_url'] = base_url().'index.php/exp-opdo/cform/supplier/index/';
			$cari = $this->input->post('cari', FALSE);
			$cari = strtoupper($cari);
			$query = $this->db->query("	select * from tr_supplier 
										where upper(i_supplier) like '%$cari%' or upper(e_supplier_name) like '%$cari%'",false);
			$config['total_rows'] = $query->num_rows(); 
			$config['per_page'] = '10';
			$config['first_link'] = 'Awal';
			$config['last_link'] = 'Akhir';
			$config['next_link'] = 'Selanjutnya';
			$config['prev_link'] = 'Sebelumnya';
			$config['cur_page'] = $this->uri->segment(5);
			$this->pagination->initialize($config);
			$this->load->model('exp-opdo/mmaster');
			$data['page_title'] = $this->lang->line('list_supplier');
			$data['isi']=$this->mmaster->carisupplier($cari,$config['per_page'],$this->uri->segment(5));
			$this->load->view('exp-opdo/vlistsupplier', $data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
	function export()
	{
		if (
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('menu334')=='t')) ||
			(($this->session->userdata('logged_in')) &&
			($this->session->userdata('allmenu')=='t'))
		   ){
			$this->load->model('exp-opdo/mmaster');
			$cari		= strtoupper($this->input->post('cari'));
			$iperiode	= $this->input->post('iperiode');
      $isupplier = $this->input->post('isupplier');
			if($iperiode==''){
        $iperiode=$this->uri->segment(4);
      }
      $a=substr($iperiode,0,4);
	    $b=substr($iperiode,4,2);
		  $peri=mbulan($b)." - ".$a;

			if($isupplier=='') $isupplier=$this->uri->segment(5);
       $qsupname	= $this->mmaster->esupname($isupplier);
       if($qsupname->num_rows()>0)
          {
           $row_supname	= $qsupname->row();
           $sname   = $row_supname->e_supplier_name;
        }
          else
        {
           $sname   = '';
        }
      $this->db->select("	distinct(a.i_product), a.i_supplier, b.e_supplier_name
                          from tm_do_item a
                          inner join tr_supplier b on (a.i_supplier=b.i_supplier)
                          where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                          and a.i_supplier='$isupplier'
                          order by a.i_product ",false);

		  $query = $this->db->get();
			$this->load->library('PHPExcel');
			$this->load->library('PHPExcel/IOFactory');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setTitle("Laporan OP vs DO ")->setDescription(NmPerusahaan);
			$objPHPExcel->setActiveSheetIndex(0);
			if ($query->num_rows() > 0){
				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A2:A4'
				);
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(6);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(6);

				$objPHPExcel->getActiveSheet()->setCellValue('A2', 'Laporan OP vs DO');
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,2,9,2);
				$objPHPExcel->getActiveSheet()->setCellValue('A3', 'Bulan : '.$peri. ' - Supplier : '.$sname);
				$objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0,3,9,3);

				$objPHPExcel->getActiveSheet()->duplicateStyleArray(
				array(
					'font' => array(
						'name'	=> 'Arial',
						'bold'  => true,
						'italic'=> false,
						'size'  => 10
					),
					'alignment' => array(
						'horizontal'=> Style_Alignment::HORIZONTAL_LEFT,
						'vertical'  => Style_Alignment::VERTICAL_CENTER,
						'wrap'      => true
					)
				),
				'A5:H5'
				);


				$objPHPExcel->getActiveSheet()->setCellValue('A5', 'Kode');
				$objPHPExcel->getActiveSheet()->getStyle('A5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)

					)
				);

				$objPHPExcel->getActiveSheet()->setCellValue('B5', 'Nama');
				$objPHPExcel->getActiveSheet()->getStyle('B5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('C5', 'Pcs (OP)');
				$objPHPExcel->getActiveSheet()->getStyle('C5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('D5', 'RP (OP)');
				$objPHPExcel->getActiveSheet()->getStyle('D5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('E5', 'Pcs (DO)');
				$objPHPExcel->getActiveSheet()->getStyle('E5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('F5', 'RP (DO)');
				$objPHPExcel->getActiveSheet()->getStyle('F5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('G5', 'Persen (Pcs)');
				$objPHPExcel->getActiveSheet()->getStyle('G5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						),
					)
				);
				$objPHPExcel->getActiveSheet()->setCellValue('H5', 'Persen (Rp)');
				$objPHPExcel->getActiveSheet()->getStyle('H5')->applyFromArray(
					array(
						'borders' => array(
							'top' 	=> array('style' => Style_Border::BORDER_THIN)
						)
					)
				);

				$i=6;
				$j=6;
        $no=0;
				foreach($query->result() as $row){
          $no++;
          $objPHPExcel->getActiveSheet()->duplicateStyleArray(
				  array(
					  'font' => array(
						  'name'	=> 'Arial',
						  'bold'  => false,
						  'italic'=> false,
						  'size'  => 10
					  )
				  ),
				  'A'.$i.':H'.$i
				  );

          $product=$row->i_product;
          $query = $this->db->query(" select sum(a.n_deliver) as pcsdo, sum(a.n_deliver*a.v_product_mill) as rpdo,
                                      sum(b.n_order) as pcsop, sum(b.n_order*b.v_product_mill) as rpop,
                                      a.i_product, a.e_product_name, a.i_supplier
                                      from tm_op_item b
                                      left join tm_do c on (b.i_op=c.i_op)
                                      left join tm_do_item a on (a.i_product=b.i_product and c.i_do=a.i_do)
                                      where to_char(a.d_do::timestamp with time zone, 'yyyymm'::text)='$iperiode'
                                      and a.i_supplier='$isupplier' and a.i_product='$product'
                                      group by a.i_product, a.e_product_name, a.i_supplier
                                      order by a.i_product  ");

          if($query->num_rows()>0){
            foreach($query->result() as $raw){
              $pop=$raw->pcsop;
              $rop=$raw->rpop;
              $pdo=$raw->pcsdo;
              $rdo=$raw->rpdo;
              $nmbrg=$raw->e_product_name;
            }
          }

          if($pop!=0 || $pdo!=0){
            $persenpcs=($pdo/$pop)*100;
          } else{
            $persenpcs=0;
          }

          if($rop!=0 || $rdo!=0){
            $persenrp=($rdo/$rop)*100;
          } else{
            $persenrp=0;
          }

          $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$i, $row->i_product, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$i, $nmbrg, Cell_DataType::TYPE_STRING);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$i, $pop, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$i, $rop, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$i, $pdo, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$i, $rdo, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$i, $persenpcs, Cell_DataType::TYPE_NUMERIC);
          $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$i, $persenrp, Cell_DataType::TYPE_NUMERIC);

					$i++;
					$j++;
				}
				$x=$i-1;
        $objPHPExcel->getActiveSheet()->getStyle('G6:H'.$i)->getNumberFormat()->setFormatCode(Style_NumberFormat::FORMAT_TEXT_COA);
			}
			$objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
      $nama='LapOPvsDO-'.$isupplier.'-'.$iperiode.'.xls';
			$objWriter->save('beli/'.$nama);

      $sess=$this->session->userdata('session_id');
      $id=$this->session->userdata('user_id');
      $sql	= "select * from dgu_session where session_id='$sess' and not user_data isnull";
      $rs		= pg_query($sql);
      if(pg_num_rows($rs)>0){
	      while($row=pg_fetch_assoc($rs)){
		      $ip_address	  = $row['ip_address'];
		      break;
	      }
      }else{
	      $ip_address='kosong';
      }
      $query 	= pg_query("SELECT current_timestamp as c");
      while($row=pg_fetch_assoc($query)){
      	$now	  = $row['c'];
      }
      $pesan='Export OP DO Periode:'.$iperiode.' Supplier:'.$isupplier;
      $this->load->model('logger');
      $this->logger->write($id, $ip_address, $now , $pesan ); 
 
			$data['sukses'] = true;
			$data['inomor']	= "Export OP vs DO ".$peri;
			$this->load->view('nomor',$data);
		}else{
			$this->load->view('awal/index.php');
		}
	}
}
?>
