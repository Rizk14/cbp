<html>
<head>
	<title>Direktori</title>
	<link href="<?=$baseurl;?>external/img/explorer.png" rel="shortcut icon" type="image/ico">
	<style type="text/css">
	body {
		background-color: #f2f2f2;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}
	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 25px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}
	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 15px;
		background-color: #f2f2f2;
		border: 4px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}
	#body {
		margin: 0 15px 0 15px;
	}
	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
		font-size: 17px;
	}
	#container {
		margin-left:330px;
		width:600px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Login Dahulu</h1>

	<div id="body">
		<code>Login Program Sebelum Masuk Direktori!!</code>
		<code><a href="<?=$baseurl?>">->Link Program<-</a></code>
	</div>
</div>
</body>
</html>