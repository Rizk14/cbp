<?php
$folder = glob("controllers/*/*.php");
foreach($folder as $src){

	$files   = file_get_contents($src);
	$string  = <<< 'EOT'
	$this->session->userdata
	EOT;

	$replace  = <<< 'EOT'
	session
	EOT;

	$rep = str_replace($string, $replace, $files);

	file_put_contents($src,$rep);

}

?>