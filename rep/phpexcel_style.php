<?php
$folder = glob("controllers/*/*.php");
foreach($folder as $src){

	$files   = file_get_contents($src);
	$string  = <<< EOT
	Style_Alignment
	EOT;

	$replace  = <<< EOT
	PHPExcel_Style_Alignment
	EOT;

	$rep = str_replace($string, $replace, $files);

	file_put_contents($src,$rep);


	/*---------------------------------------------------------*/


	$files   = file_get_contents($src);
	$string  = <<< EOT
	Style_Border
	EOT;

	$replace  = <<< EOT
	PHPExcel_Style_Border
	EOT;

	$rep = str_replace($string, $replace, $files);

	file_put_contents($src,$rep);


	/*---------------------------------------------------------*/


	$files   = file_get_contents($src);
	$string  = <<< EOT
	Cell_DataType
	EOT;

	$replace  = <<< EOT
	PHPExcel_Cell_DataType
	EOT;

	$rep = str_replace($string, $replace, $files);

	file_put_contents($src,$rep);


	/*---------------------------------------------------------*/


	$files   = file_get_contents($src);
	$string  = <<< EOT
	Style_Fill
	EOT;

	$replace  = <<< EOT
	PHPExcel_Style_Fill
	EOT;

	$rep = str_replace($string, $replace, $files);

	file_put_contents($src,$rep);


	/*---------------------------------------------------------*/


	$files   = file_get_contents($src);
	$string  = <<< EOT
	Style_NumberFormat
	EOT;

	$replace  = <<< EOT
	PHPExcel_Style_NumberFormat
	EOT;

	$rep = str_replace($string, $replace, $files);

	file_put_contents($src,$rep);



}

?>